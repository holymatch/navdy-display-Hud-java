#!/system/bin/sh

if [[ -f /maps/Hud.apk ]]; then
	am force-stop com.navdy.hud.app
	#su -c 'pkill -15 -f com.navdy.hud'
	su -c 'rm /system/priv-app/Hud/Hud.apk'
	su -c 'ln -s /maps/Hud.apk /system/priv-app/Hud/'

	su -c 'rm -rf "/data/dalvik-cache/arm/system@priv-app@Hud@Hud.apk@classes.dex"'
	su -c 'rm -rf /data/dalvik-cache/profiles/com.navdy.hud.app'
	logcat -b all -c
	am start -n com.navdy.hud.app/.ui.activity.MainActivity

fi