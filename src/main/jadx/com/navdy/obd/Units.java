package com.navdy.obd;

import android.content.res.XmlResourceParser;
import com.here.android.mpa.customlocation2.CLE2Request.CLE2Error;
import com.navdy.service.library.network.http.services.JiraClient;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public enum Units {
    NONE,
    MILES_PER_HOUR(System.US),
    KILOMETERS_PER_HOUR(System.Metric);
    
    protected String abbreviation;
    protected String description;
    protected System system;

    enum System {
        private static final /* synthetic */ System[] $VALUES = null;
        public static final System Metric = null;
        public static final System None = null;
        public static final System US = null;

        private System(String str, int i) {
        }

        public static System valueOf(String name) {
            return (System) Enum.valueOf(System.class, name);
        }

        public static System[] values() {
            return (System[]) $VALUES.clone();
        }

        static {
            None = new System(CLE2Error.NONE, 0);
            Metric = new System("Metric", 1);
            US = new System("US", 2);
            $VALUES = new System[]{None, Metric, US};
        }
    }

    private Units(System system) {
        this(r7, r8, "", "", system);
    }

    private Units(String abbreviation, String description, System system) {
        this.abbreviation = abbreviation;
        this.description = description;
        this.system = system;
    }

    public String getAbbreviation() {
        return this.abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public System getSystem() {
        return this.system;
    }

    static void localize(XmlResourceParser xpp) throws XmlPullParserException, IOException {
        Units currentUnit = null;
        StringBuilder stringBuilder = new StringBuilder();
        int eventType = xpp.getEventType();
        while (eventType != 1) {
            String tagName = xpp.getName();
            if (eventType == 2) {
                if ("unit".equals(tagName)) {
                    currentUnit = valueOf(xpp.getAttributeValue(null, "id"));
                }
            } else if (eventType == 3) {
                if (currentUnit != null) {
                    String tagValue = stringBuilder.toString();
                    if ("abbreviation".equals(tagName)) {
                        currentUnit.setAbbreviation(tagValue);
                    } else if (JiraClient.DESCRIPTION.equals(tagName)) {
                        currentUnit.setDescription(tagValue);
                    }
                }
                if ("unit".equals(tagName)) {
                    currentUnit = null;
                }
                stringBuilder.setLength(0);
            } else if (eventType == 4) {
                stringBuilder.append(xpp.getText());
            }
            eventType = xpp.next();
        }
    }
}
