package com.navdy.service.library.util;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import com.navdy.service.library.log.Logger;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class SystemUtils {
    private static final String COMMA = ",";
    private static final String PERCENTAGE = "%";
    private static final String SPACE = " ";
    private static final Logger sLogger = new Logger(SystemUtils.class);

    public static class CpuInfo {
        private ArrayList<ProcessCpuInfo> plist;
        private int system;
        private int usr;

        public CpuInfo(int usr, int system, ArrayList<ProcessCpuInfo> plist) {
            this.usr = usr;
            this.system = system;
            this.plist = plist;
        }

        public int getCpuUser() {
            return this.usr;
        }

        public int getCpuSystem() {
            return this.system;
        }

        public ArrayList<ProcessCpuInfo> getList() {
            return this.plist;
        }
    }

    public static class ProcessCpuInfo {
        private int cpu;
        private String name;
        private int pid;
        private String thread;
        private int tid;

        public ProcessCpuInfo(int pid, int tid, String name, String thread, int cpu) {
            this.pid = pid;
            this.tid = tid;
            this.name = name;
            this.thread = thread;
            this.cpu = cpu;
        }

        public int getPid() {
            return this.pid;
        }

        public int getTid() {
            return this.tid;
        }

        public String getProcessName() {
            return this.name;
        }

        public String getThreadName() {
            return this.thread;
        }

        public int getCpu() {
            return this.cpu;
        }
    }

    public static String getProcessName(Context context, int pid) {
        List<RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (RunningAppProcessInfo processInfo : runningAppProcesses) {
                if (processInfo != null && processInfo.pid == pid) {
                    return processInfo.processName;
                }
            }
        }
        return "";
    }

    public static int getProcessId(Context context, String processName) {
        for (RunningAppProcessInfo processInfo : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
            if (TextUtils.equals(processInfo.processName, processName)) {
                return processInfo.pid;
            }
        }
        return -1;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int getNativeProcessId(String processName) {
        InputStream inputStream = null;
        int i;
        try {
            Process process = Runtime.getRuntime().exec("ps");
            process.waitFor();
            inputStream = process.getInputStream();
            BufferedReader reader = new BufferedReader(new StringReader(IOUtils.convertInputStreamToString(inputStream, "UTF-8")));
            while (true) {
                String line = reader.readLine();
                if (line != null) {
                    if (line.contains(processName)) {
                        break;
                    }
                }
                IOUtils.closeStream(inputStream);
                break;
            }
            return i;
        } catch (Throwable th) {
            i = th;
            return -1;
        } finally {
            IOUtils.closeStream(inputStream);
        }
    }

    public static boolean isConnectedToNetwork(Context context) {
        NetworkInfo activeNetwork = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static boolean isConnectedToWifi(Context context) {
        NetworkInfo wifiNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1);
        if (wifiNetworkInfo == null || !wifiNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public static boolean isConnectedToCellNetwork(Context context) {
        NetworkInfo wifiNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0);
        if (wifiNetworkInfo == null || !wifiNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public static CpuInfo getCpuUsage() {
        InputStream inputStream = null;
        try {
            Process process = Runtime.getRuntime().exec("top -m 5 -t -n 1");
            process.waitFor();
            inputStream = process.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new StringReader(IOUtils.convertInputStreamToString(inputStream, "UTF-8")));
            int lineCount = 1;
            String usr = null;
            String system = null;
            ArrayList<ProcessCpuInfo> plist = new ArrayList();
            while (true) {
                String line = bufferedReader.readLine();
                if (line == null) {
                    CpuInfo cpuInfo = new CpuInfo(extractNumberFromPercent(usr), extractNumberFromPercent(system), plist);
                    IOUtils.closeStream(inputStream);
                    return cpuInfo;
                } else if (line.trim().length() != 0) {
                    StringTokenizer stringTokenizer;
                    String element;
                    if (lineCount == 1) {
                        stringTokenizer = new StringTokenizer(line, ",");
                        while (stringTokenizer.hasMoreElements()) {
                            element = ((String) stringTokenizer.nextElement()).trim();
                            int index = element.indexOf(" ");
                            if (index >= 0) {
                                if (usr == null) {
                                    usr = element.substring(index + 1).trim();
                                } else {
                                    system = element.substring(index + 1).trim();
                                }
                            }
                        }
                    } else if (lineCount >= 4) {
                        String cpu = null;
                        String thread = null;
                        String pname = null;
                        stringTokenizer = new StringTokenizer(line, " ");
                        int item = 1;
                        int pid = 0;
                        int tid = 0;
                        while (stringTokenizer.hasMoreElements()) {
                            element = (String) stringTokenizer.nextElement();
                            switch (item) {
                                case 1:
                                    try {
                                        pid = Integer.parseInt(element);
                                        break;
                                    } catch (Throwable th) {
                                        IOUtils.closeStream(inputStream);
                                    }
                                case 2:
                                    try {
                                        tid = Integer.parseInt(element);
                                        break;
                                    } catch (Throwable th2) {
                                        IOUtils.closeStream(inputStream);
                                    }
                                case 4:
                                    cpu = element;
                                    break;
                                case 10:
                                    thread = element;
                                    break;
                                case 11:
                                    pname = element;
                                    break;
                                default:
                                    break;
                            }
                            item++;
                        }
                        plist.add(new ProcessCpuInfo(pid, tid, pname, thread, extractNumberFromPercent(cpu)));
                    }
                    lineCount++;
                }
            }
        } catch (Throwable th22) {
            IOUtils.closeStream(inputStream);
        }
    }

    private static int extractNumberFromPercent(String s) {
        if (s == null) {
            return 0;
        }
        try {
            s = s.trim();
            int index = s.indexOf(PERCENTAGE);
            if (index != -1) {
                s = s.substring(0, index).trim();
            }
            return Integer.parseInt(s);
        } catch (Throwable t) {
            sLogger.e(t);
            return 0;
        }
    }
}
