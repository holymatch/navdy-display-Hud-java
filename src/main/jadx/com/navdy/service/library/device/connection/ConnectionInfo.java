package com.navdy.service.library.device.connection;

import android.net.nsd.NsdServiceInfo;
import android.text.TextUtils;
import com.google.gson.annotations.SerializedName;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.RuntimeTypeAdapterFactory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class ConnectionInfo {
    private static final int DEVICE_ID_GROUP = 1;
    public static final RuntimeTypeAdapterFactory<ConnectionInfo> connectionInfoAdapter = RuntimeTypeAdapterFactory.of(ConnectionInfo.class).registerSubtype(BTConnectionInfo.class, "BT").registerSubtype(TCPConnectionInfo.class, "TCP").registerSubtypeAlias(BTConnectionInfo.class, "EA");
    private static final Pattern deviceIdFromServiceName = Pattern.compile("Navdy\\s(.*)");
    public static final Logger sLogger = new Logger(ConnectionInfo.class);
    private final ConnectionType connectionType;
    @SerializedName("deviceId")
    protected final NavdyDeviceId mDeviceId;

    public abstract ServiceAddress getAddress();

    public static boolean isValidNavdyServiceInfo(NsdServiceInfo serviceInfo) {
        if (!ConnectionType.getServiceTypes().contains(serviceInfo.getServiceType())) {
            sLogger.d("Unknown service type: " + serviceInfo.getServiceType());
            return false;
        } else if (deviceIdFromServiceName.matcher(serviceInfo.getServiceName()).matches()) {
            return true;
        } else {
            sLogger.d("Not a Navdy: " + serviceInfo.getServiceName());
            return false;
        }
    }

    protected ConnectionInfo(ConnectionType connectionType) {
        this.connectionType = connectionType;
        this.mDeviceId = NavdyDeviceId.UNKNOWN_ID;
    }

    public static ConnectionInfo fromConnection(Connection connection) {
        if (connection == null) {
            return null;
        }
        return connection.getConnectionInfo();
    }

    public ConnectionInfo(NavdyDeviceId id, ConnectionType connectionType) {
        if (id == null) {
            throw new IllegalArgumentException("Device id required.");
        }
        this.mDeviceId = id;
        this.connectionType = connectionType;
    }

    public ConnectionInfo(NsdServiceInfo serviceInfo) {
        if (serviceInfo == null) {
            throw new IllegalArgumentException("serviceInfo can't be null");
        }
        String deviceIdString = parseDeviceIdStringFromServiceInfo(serviceInfo);
        if (deviceIdString == null) {
            throw new IllegalArgumentException("Service info doesn't contain valid device id");
        }
        this.connectionType = ConnectionType.TCP_PROTOBUF;
        this.mDeviceId = new NavdyDeviceId(deviceIdString);
    }

    public static ConnectionInfo fromServiceInfo(NsdServiceInfo serviceInfo) {
        String serviceType = serviceInfo.getServiceType();
        if (serviceType.startsWith(GlanceConstants.PERIOD)) {
            serviceType = serviceType.substring(1);
        }
        if (!serviceType.endsWith(GlanceConstants.PERIOD)) {
            serviceType = serviceType + GlanceConstants.PERIOD;
        }
        ConnectionType type = ConnectionType.fromServiceType(serviceType);
        if (type == null) {
            return null;
        }
        switch (type) {
            case TCP_PROTOBUF:
                return new TCPConnectionInfo(serviceInfo);
            default:
                sLogger.e("No connectioninfo from mDNS info for type: " + type);
                return null;
        }
    }

    public NavdyDeviceId getDeviceId() {
        return this.mDeviceId;
    }

    public ConnectionType getType() {
        return this.connectionType;
    }

    public String toString() {
        return getType() + " id: " + this.mDeviceId + " " + getAddress();
    }

    protected String parseDeviceIdStringFromServiceInfo(NsdServiceInfo serviceInfo) {
        String serviceName = serviceInfo.getServiceName().replace("\\032", " ");
        if (TextUtils.isEmpty(serviceName)) {
            return null;
        }
        Matcher matcher = deviceIdFromServiceName.matcher(serviceName);
        if (!matcher.matches() || matcher.groupCount() < 1) {
            return null;
        }
        return matcher.group(1);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return this.mDeviceId.equals(((ConnectionInfo) o).mDeviceId);
    }

    public int hashCode() {
        return this.mDeviceId.hashCode();
    }
}
