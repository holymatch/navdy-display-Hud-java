package com.navdy.service.library.network.http;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;

public class HttpManager implements IHttpManager {
    protected OkHttpClient mHttpClient;

    public HttpManager() {
        initHttpClient();
    }

    private void initHttpClient() {
        this.mHttpClient = new Builder().retryOnConnectionFailure(false).build();
    }

    public OkHttpClient getClient() {
        return this.mHttpClient;
    }

    public Builder getClientCopy() {
        return this.mHttpClient.newBuilder();
    }
}
