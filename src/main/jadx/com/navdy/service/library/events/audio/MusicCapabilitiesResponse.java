package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class MusicCapabilitiesResponse extends Message {
    public static final List<MusicCapability> DEFAULT_CAPABILITIES = Collections.emptyList();
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REPEATED, messageType = MusicCapability.class, tag = 1)
    public final List<MusicCapability> capabilities;

    public static final class Builder extends com.squareup.wire.Message.Builder<MusicCapabilitiesResponse> {
        public List<MusicCapability> capabilities;

        public Builder(MusicCapabilitiesResponse message) {
            super(message);
            if (message != null) {
                this.capabilities = Message.copyOf(message.capabilities);
            }
        }

        public Builder capabilities(List<MusicCapability> capabilities) {
            this.capabilities = com.squareup.wire.Message.Builder.checkForNulls(capabilities);
            return this;
        }

        public MusicCapabilitiesResponse build() {
            return new MusicCapabilitiesResponse();
        }
    }

    public MusicCapabilitiesResponse(List<MusicCapability> capabilities) {
        this.capabilities = Message.immutableCopyOf(capabilities);
    }

    private MusicCapabilitiesResponse(Builder builder) {
        this(builder.capabilities);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof MusicCapabilitiesResponse) {
            return equals(this.capabilities, ((MusicCapabilitiesResponse) other).capabilities);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.capabilities != null ? this.capabilities.hashCode() : 1;
        this.hashCode = hashCode;
        return hashCode;
    }
}
