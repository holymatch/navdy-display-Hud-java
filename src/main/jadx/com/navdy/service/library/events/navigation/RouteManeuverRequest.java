package com.navdy.service.library.events.navigation;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class RouteManeuverRequest extends Message {
    public static final String DEFAULT_ROUTEID = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String routeId;

    public static final class Builder extends com.squareup.wire.Message.Builder<RouteManeuverRequest> {
        public String routeId;

        public Builder(RouteManeuverRequest message) {
            super(message);
            if (message != null) {
                this.routeId = message.routeId;
            }
        }

        public Builder routeId(String routeId) {
            this.routeId = routeId;
            return this;
        }

        public RouteManeuverRequest build() {
            checkRequiredFields();
            return new RouteManeuverRequest();
        }
    }

    public RouteManeuverRequest(String routeId) {
        this.routeId = routeId;
    }

    private RouteManeuverRequest(Builder builder) {
        this(builder.routeId);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof RouteManeuverRequest) {
            return equals( this.routeId,  ((RouteManeuverRequest) other).routeId);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.routeId != null ? this.routeId.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
