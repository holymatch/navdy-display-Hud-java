package com.navdy.service.library.events.navigation;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class NavigationManeuverEvent extends Message {
    public static final String DEFAULT_CURRENTROAD = "";
    public static final String DEFAULT_DISTANCE_TO_PENDING_TURN = "";
    public static final String DEFAULT_ETA = "";
    public static final Long DEFAULT_ETAUTCTIME = Long.valueOf(0);
    public static final NavigationSessionState DEFAULT_NAVIGATIONSTATE = NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    public static final String DEFAULT_PENDING_STREET = "";
    public static final NavigationTurn DEFAULT_PENDING_TURN = NavigationTurn.NAV_TURN_START;
    public static final String DEFAULT_SPEED = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String currentRoad;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.STRING)
    public final String distance_to_pending_turn;
    @ProtoField(label = Label.REQUIRED, tag = 5, type = Datatype.STRING)
    public final String eta;
    @ProtoField(tag = 8, type = Datatype.INT64)
    public final Long etaUtcTime;
    @ProtoField(tag = 7, type = Datatype.ENUM)
    public final NavigationSessionState navigationState;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.STRING)
    public final String pending_street;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final NavigationTurn pending_turn;
    @ProtoField(label = Label.REQUIRED, tag = 6, type = Datatype.STRING)
    public final String speed;

    public static final class Builder extends com.squareup.wire.Message.Builder<NavigationManeuverEvent> {
        public String currentRoad;
        public String distance_to_pending_turn;
        public String eta;
        public Long etaUtcTime;
        public NavigationSessionState navigationState;
        public String pending_street;
        public NavigationTurn pending_turn;
        public String speed;

        public Builder(NavigationManeuverEvent message) {
            super(message);
            if (message != null) {
                this.currentRoad = message.currentRoad;
                this.pending_turn = message.pending_turn;
                this.distance_to_pending_turn = message.distance_to_pending_turn;
                this.pending_street = message.pending_street;
                this.eta = message.eta;
                this.speed = message.speed;
                this.navigationState = message.navigationState;
                this.etaUtcTime = message.etaUtcTime;
            }
        }

        public Builder currentRoad(String currentRoad) {
            this.currentRoad = currentRoad;
            return this;
        }

        public Builder pending_turn(NavigationTurn pending_turn) {
            this.pending_turn = pending_turn;
            return this;
        }

        public Builder distance_to_pending_turn(String distance_to_pending_turn) {
            this.distance_to_pending_turn = distance_to_pending_turn;
            return this;
        }

        public Builder pending_street(String pending_street) {
            this.pending_street = pending_street;
            return this;
        }

        public Builder eta(String eta) {
            this.eta = eta;
            return this;
        }

        public Builder speed(String speed) {
            this.speed = speed;
            return this;
        }

        public Builder navigationState(NavigationSessionState navigationState) {
            this.navigationState = navigationState;
            return this;
        }

        public Builder etaUtcTime(Long etaUtcTime) {
            this.etaUtcTime = etaUtcTime;
            return this;
        }

        public NavigationManeuverEvent build() {
            checkRequiredFields();
            return new NavigationManeuverEvent();
        }
    }

    public NavigationManeuverEvent(String currentRoad, NavigationTurn pending_turn, String distance_to_pending_turn, String pending_street, String eta, String speed, NavigationSessionState navigationState, Long etaUtcTime) {
        this.currentRoad = currentRoad;
        this.pending_turn = pending_turn;
        this.distance_to_pending_turn = distance_to_pending_turn;
        this.pending_street = pending_street;
        this.eta = eta;
        this.speed = speed;
        this.navigationState = navigationState;
        this.etaUtcTime = etaUtcTime;
    }

    private NavigationManeuverEvent(Builder builder) {
        this(builder.currentRoad, builder.pending_turn, builder.distance_to_pending_turn, builder.pending_street, builder.eta, builder.speed, builder.navigationState, builder.etaUtcTime);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NavigationManeuverEvent)) {
            return false;
        }
        NavigationManeuverEvent o = (NavigationManeuverEvent) other;
        if (equals( this.currentRoad,  o.currentRoad) && equals( this.pending_turn,  o.pending_turn) && equals( this.distance_to_pending_turn,  o.distance_to_pending_turn) && equals( this.pending_street,  o.pending_street) && equals( this.eta,  o.eta) && equals( this.speed,  o.speed) && equals( this.navigationState,  o.navigationState) && equals( this.etaUtcTime,  o.etaUtcTime)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.currentRoad != null ? this.currentRoad.hashCode() : 0) * 37;
        if (this.pending_turn != null) {
            hashCode = this.pending_turn.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.distance_to_pending_turn != null) {
            hashCode = this.distance_to_pending_turn.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.pending_street != null) {
            hashCode = this.pending_street.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.eta != null) {
            hashCode = this.eta.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.speed != null) {
            hashCode = this.speed.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.navigationState != null) {
            hashCode = this.navigationState.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.etaUtcTime != null) {
            i = this.etaUtcTime.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
