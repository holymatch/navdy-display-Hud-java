package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoEnum;

public enum MusicCollectionSource implements ProtoEnum {
    COLLECTION_SOURCE_UNKNOWN(0),
    COLLECTION_SOURCE_ANDROID_LOCAL(1),
    COLLECTION_SOURCE_IOS_MEDIA_PLAYER(2),
    COLLECTION_SOURCE_ANDROID_GOOGLE_PLAY_MUSIC(3),
    COLLECTION_SOURCE_ANDROID_SPOTIFY(4);
    
    private final int value;

    private MusicCollectionSource(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
