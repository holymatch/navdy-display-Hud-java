package com.navdy.service.library.events;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class Notification extends Message {
    public static final String DEFAULT_MESSAGE_DATA = "";
    public static final String DEFAULT_ORIGIN = "";
    public static final String DEFAULT_SENDER = "";
    public static final Integer DEFAULT_SMS_CLASS = Integer.valueOf(0);
    public static final String DEFAULT_TARGET = "";
    public static final String DEFAULT_TYPE = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String message_data;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String origin;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String sender;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT32)
    public final Integer sms_class;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String target;
    @ProtoField(tag = 6, type = Datatype.STRING)
    public final String type;

    public static final class Builder extends com.squareup.wire.Message.Builder<Notification> {
        public String message_data;
        public String origin;
        public String sender;
        public Integer sms_class;
        public String target;
        public String type;

        public Builder(Notification message) {
            super(message);
            if (message != null) {
                this.sms_class = message.sms_class;
                this.origin = message.origin;
                this.sender = message.sender;
                this.target = message.target;
                this.message_data = message.message_data;
                this.type = message.type;
            }
        }

        public Builder sms_class(Integer sms_class) {
            this.sms_class = sms_class;
            return this;
        }

        public Builder origin(String origin) {
            this.origin = origin;
            return this;
        }

        public Builder sender(String sender) {
            this.sender = sender;
            return this;
        }

        public Builder target(String target) {
            this.target = target;
            return this;
        }

        public Builder message_data(String message_data) {
            this.message_data = message_data;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Notification build() {
            checkRequiredFields();
            return new Notification();
        }
    }

    public Notification(Integer sms_class, String origin, String sender, String target, String message_data, String type) {
        this.sms_class = sms_class;
        this.origin = origin;
        this.sender = sender;
        this.target = target;
        this.message_data = message_data;
        this.type = type;
    }

    private Notification(Builder builder) {
        this(builder.sms_class, builder.origin, builder.sender, builder.target, builder.message_data, builder.type);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof Notification)) {
            return false;
        }
        Notification o = (Notification) other;
        if (equals( this.sms_class,  o.sms_class) && equals( this.origin,  o.origin) && equals( this.sender,  o.sender) && equals( this.target,  o.target) && equals( this.message_data,  o.message_data) && equals( this.type,  o.type)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.sms_class != null ? this.sms_class.hashCode() : 0) * 37;
        if (this.origin != null) {
            hashCode = this.origin.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.sender != null) {
            hashCode = this.sender.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.target != null) {
            hashCode = this.target.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.message_data != null) {
            hashCode = this.message_data.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.type != null) {
            i = this.type.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
