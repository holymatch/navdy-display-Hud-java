package com.navdy.service.library.events.preferences;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class DriverProfilePreferences extends Message {
    public static final List<String> DEFAULT_ADDITIONALLOCALES = Collections.emptyList();
    public static final Boolean DEFAULT_AUTO_ON_ENABLED = Boolean.valueOf(true);
    public static final String DEFAULT_CAR_MAKE = "";
    public static final String DEFAULT_CAR_MODEL = "";
    public static final String DEFAULT_CAR_YEAR = "";
    public static final String DEFAULT_DEVICE_NAME = "";
    public static final DialLongPressAction DEFAULT_DIAL_LONG_PRESS_ACTION = DialLongPressAction.DIAL_LONG_PRESS_VOICE_ASSISTANT;
    public static final DisplayFormat DEFAULT_DISPLAY_FORMAT = DisplayFormat.DISPLAY_FORMAT_NORMAL;
    public static final String DEFAULT_DRIVER_EMAIL = "";
    public static final String DEFAULT_DRIVER_NAME = "";
    public static final FeatureMode DEFAULT_FEATURE_MODE = FeatureMode.FEATURE_MODE_RELEASE;
    public static final Boolean DEFAULT_LIMIT_BANDWIDTH = Boolean.valueOf(false);
    public static final String DEFAULT_LOCALE = "en_US";
    public static final Long DEFAULT_OBDBLACKLISTLASTMODIFIED = Long.valueOf(0);
    public static final ObdScanSetting DEFAULT_OBDSCANSETTING = ObdScanSetting.SCAN_DEFAULT_ON;
    public static final String DEFAULT_PHOTO_CHECKSUM = "";
    public static final Boolean DEFAULT_PROFILE_IS_PUBLIC = Boolean.valueOf(false);
    public static final Long DEFAULT_SERIAL_NUMBER = Long.valueOf(0);
    public static final UnitSystem DEFAULT_UNIT_SYSTEM = UnitSystem.UNIT_SYSTEM_IMPERIAL;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REPEATED, tag = 19, type = Datatype.STRING)
    public final List<String> additionalLocales;
    @ProtoField(tag = 10, type = Datatype.BOOL)
    public final Boolean auto_on_enabled;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String car_make;
    @ProtoField(tag = 8, type = Datatype.STRING)
    public final String car_model;
    @ProtoField(tag = 9, type = Datatype.STRING)
    public final String car_year;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String device_name;
    @ProtoField(tag = 18, type = Datatype.ENUM)
    public final DialLongPressAction dial_long_press_action;
    @ProtoField(tag = 11, type = Datatype.ENUM)
    public final DisplayFormat display_format;
    @ProtoField(tag = 6, type = Datatype.STRING)
    public final String driver_email;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String driver_name;
    @ProtoField(tag = 14, type = Datatype.ENUM)
    public final FeatureMode feature_mode;
    @ProtoField(tag = 16, type = Datatype.BOOL)
    public final Boolean limit_bandwidth;
    @ProtoField(tag = 12, type = Datatype.STRING)
    public final String locale;
    @ProtoField(tag = 17, type = Datatype.INT64)
    public final Long obdBlacklistLastModified;
    @ProtoField(tag = 15, type = Datatype.ENUM)
    public final ObdScanSetting obdScanSetting;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String photo_checksum;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean profile_is_public;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(tag = 13, type = Datatype.ENUM)
    public final UnitSystem unit_system;

    public static final class Builder extends com.squareup.wire.Message.Builder<DriverProfilePreferences> {
        public List<String> additionalLocales;
        public Boolean auto_on_enabled;
        public String car_make;
        public String car_model;
        public String car_year;
        public String device_name;
        public DialLongPressAction dial_long_press_action;
        public DisplayFormat display_format;
        public String driver_email;
        public String driver_name;
        public FeatureMode feature_mode;
        public Boolean limit_bandwidth;
        public String locale;
        public Long obdBlacklistLastModified;
        public ObdScanSetting obdScanSetting;
        public String photo_checksum;
        public Boolean profile_is_public;
        public Long serial_number;
        public UnitSystem unit_system;

        public Builder(DriverProfilePreferences message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
                this.driver_name = message.driver_name;
                this.device_name = message.device_name;
                this.profile_is_public = message.profile_is_public;
                this.photo_checksum = message.photo_checksum;
                this.driver_email = message.driver_email;
                this.car_make = message.car_make;
                this.car_model = message.car_model;
                this.car_year = message.car_year;
                this.auto_on_enabled = message.auto_on_enabled;
                this.display_format = message.display_format;
                this.locale = message.locale;
                this.unit_system = message.unit_system;
                this.feature_mode = message.feature_mode;
                this.obdScanSetting = message.obdScanSetting;
                this.limit_bandwidth = message.limit_bandwidth;
                this.obdBlacklistLastModified = message.obdBlacklistLastModified;
                this.dial_long_press_action = message.dial_long_press_action;
                this.additionalLocales = Message.copyOf(message.additionalLocales);
            }
        }

        public Builder serial_number(Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }

        public Builder driver_name(String driver_name) {
            this.driver_name = driver_name;
            return this;
        }

        public Builder device_name(String device_name) {
            this.device_name = device_name;
            return this;
        }

        public Builder profile_is_public(Boolean profile_is_public) {
            this.profile_is_public = profile_is_public;
            return this;
        }

        public Builder photo_checksum(String photo_checksum) {
            this.photo_checksum = photo_checksum;
            return this;
        }

        public Builder driver_email(String driver_email) {
            this.driver_email = driver_email;
            return this;
        }

        public Builder car_make(String car_make) {
            this.car_make = car_make;
            return this;
        }

        public Builder car_model(String car_model) {
            this.car_model = car_model;
            return this;
        }

        public Builder car_year(String car_year) {
            this.car_year = car_year;
            return this;
        }

        public Builder auto_on_enabled(Boolean auto_on_enabled) {
            this.auto_on_enabled = auto_on_enabled;
            return this;
        }

        public Builder display_format(DisplayFormat display_format) {
            this.display_format = display_format;
            return this;
        }

        public Builder locale(String locale) {
            this.locale = locale;
            return this;
        }

        public Builder unit_system(UnitSystem unit_system) {
            this.unit_system = unit_system;
            return this;
        }

        public Builder feature_mode(FeatureMode feature_mode) {
            this.feature_mode = feature_mode;
            return this;
        }

        public Builder obdScanSetting(ObdScanSetting obdScanSetting) {
            this.obdScanSetting = obdScanSetting;
            return this;
        }

        public Builder limit_bandwidth(Boolean limit_bandwidth) {
            this.limit_bandwidth = limit_bandwidth;
            return this;
        }

        public Builder obdBlacklistLastModified(Long obdBlacklistLastModified) {
            this.obdBlacklistLastModified = obdBlacklistLastModified;
            return this;
        }

        public Builder dial_long_press_action(DialLongPressAction dial_long_press_action) {
            this.dial_long_press_action = dial_long_press_action;
            return this;
        }

        public Builder additionalLocales(List<String> additionalLocales) {
            this.additionalLocales = com.squareup.wire.Message.Builder.checkForNulls(additionalLocales);
            return this;
        }

        public DriverProfilePreferences build() {
            checkRequiredFields();
            return new DriverProfilePreferences();
        }
    }

    public enum DialLongPressAction implements ProtoEnum {
        DIAL_LONG_PRESS_VOICE_ASSISTANT(0),
        DIAL_LONG_PRESS_PLACE_SEARCH(1);
        
        private final int value;

        private DialLongPressAction(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum DisplayFormat implements ProtoEnum {
        DISPLAY_FORMAT_NORMAL(0),
        DISPLAY_FORMAT_COMPACT(1);
        
        private final int value;

        private DisplayFormat(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum FeatureMode implements ProtoEnum {
        FEATURE_MODE_RELEASE(1),
        FEATURE_MODE_BETA(2),
        FEATURE_MODE_EXPERIMENTAL(3);
        
        private final int value;

        private FeatureMode(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum ObdScanSetting implements ProtoEnum {
        SCAN_DEFAULT_ON(1),
        SCAN_ON(2),
        SCAN_OFF(3),
        SCAN_DEFAULT_OFF(4);
        
        private final int value;

        private ObdScanSetting(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum UnitSystem implements ProtoEnum {
        UNIT_SYSTEM_METRIC(0),
        UNIT_SYSTEM_IMPERIAL(1);
        
        private final int value;

        private UnitSystem(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public DriverProfilePreferences(Long serial_number, String driver_name, String device_name, Boolean profile_is_public, String photo_checksum, String driver_email, String car_make, String car_model, String car_year, Boolean auto_on_enabled, DisplayFormat display_format, String locale, UnitSystem unit_system, FeatureMode feature_mode, ObdScanSetting obdScanSetting, Boolean limit_bandwidth, Long obdBlacklistLastModified, DialLongPressAction dial_long_press_action, List<String> additionalLocales) {
        this.serial_number = serial_number;
        this.driver_name = driver_name;
        this.device_name = device_name;
        this.profile_is_public = profile_is_public;
        this.photo_checksum = photo_checksum;
        this.driver_email = driver_email;
        this.car_make = car_make;
        this.car_model = car_model;
        this.car_year = car_year;
        this.auto_on_enabled = auto_on_enabled;
        this.display_format = display_format;
        this.locale = locale;
        this.unit_system = unit_system;
        this.feature_mode = feature_mode;
        this.obdScanSetting = obdScanSetting;
        this.limit_bandwidth = limit_bandwidth;
        this.obdBlacklistLastModified = obdBlacklistLastModified;
        this.dial_long_press_action = dial_long_press_action;
        this.additionalLocales = Message.immutableCopyOf(additionalLocales);
    }

    private DriverProfilePreferences(Builder builder) {
        this(builder.serial_number, builder.driver_name, builder.device_name, builder.profile_is_public, builder.photo_checksum, builder.driver_email, builder.car_make, builder.car_model, builder.car_year, builder.auto_on_enabled, builder.display_format, builder.locale, builder.unit_system, builder.feature_mode, builder.obdScanSetting, builder.limit_bandwidth, builder.obdBlacklistLastModified, builder.dial_long_press_action, builder.additionalLocales);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof DriverProfilePreferences)) {
            return false;
        }
        DriverProfilePreferences o = (DriverProfilePreferences) other;
        if (equals( this.serial_number,  o.serial_number) && equals( this.driver_name,  o.driver_name) && equals( this.device_name,  o.device_name) && equals( this.profile_is_public,  o.profile_is_public) && equals( this.photo_checksum,  o.photo_checksum) && equals( this.driver_email,  o.driver_email) && equals( this.car_make,  o.car_make) && equals( this.car_model,  o.car_model) && equals( this.car_year,  o.car_year) && equals( this.auto_on_enabled,  o.auto_on_enabled) && equals( this.display_format,  o.display_format) && equals( this.locale,  o.locale) && equals( this.unit_system,  o.unit_system) && equals( this.feature_mode,  o.feature_mode) && equals( this.obdScanSetting,  o.obdScanSetting) && equals( this.limit_bandwidth,  o.limit_bandwidth) && equals( this.obdBlacklistLastModified,  o.obdBlacklistLastModified) && equals( this.dial_long_press_action,  o.dial_long_press_action) && equals(this.additionalLocales, o.additionalLocales)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.serial_number != null ? this.serial_number.hashCode() : 0) * 37;
        if (this.driver_name != null) {
            hashCode = this.driver_name.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.device_name != null) {
            hashCode = this.device_name.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.profile_is_public != null) {
            hashCode = this.profile_is_public.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.photo_checksum != null) {
            hashCode = this.photo_checksum.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.driver_email != null) {
            hashCode = this.driver_email.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.car_make != null) {
            hashCode = this.car_make.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.car_model != null) {
            hashCode = this.car_model.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.car_year != null) {
            hashCode = this.car_year.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.auto_on_enabled != null) {
            hashCode = this.auto_on_enabled.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.display_format != null) {
            hashCode = this.display_format.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.locale != null) {
            hashCode = this.locale.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.unit_system != null) {
            hashCode = this.unit_system.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.feature_mode != null) {
            hashCode = this.feature_mode.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.obdScanSetting != null) {
            hashCode = this.obdScanSetting.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.limit_bandwidth != null) {
            hashCode = this.limit_bandwidth.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.obdBlacklistLastModified != null) {
            hashCode = this.obdBlacklistLastModified.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.dial_long_press_action != null) {
            i = this.dial_long_press_action.hashCode();
        }
        result = ((hashCode + i) * 37) + (this.additionalLocales != null ? this.additionalLocales.hashCode() : 1);
        this.hashCode = result;
        return result;
    }
}
