package com.navdy.service.library.events.input;

import com.squareup.wire.ProtoEnum;

public enum KeyEvent implements ProtoEnum {
    KEY_DOWN(1),
    KEY_UP(2);
    
    private final int value;

    private KeyEvent(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
