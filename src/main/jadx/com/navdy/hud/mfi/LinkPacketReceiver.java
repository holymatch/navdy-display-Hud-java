package com.navdy.hud.mfi;

public interface LinkPacketReceiver {
    void queue(LinkPacket linkPacket);
}
