package com.navdy.hud.mfi;

public interface SessionPacketReceiver {
    void queue(SessionPacket sessionPacket);
}
