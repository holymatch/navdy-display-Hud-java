package com.navdy.hud.app.framework.notifications;

import com.navdy.hud.app.ui.framework.UIStateManager;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class NotificationManager$$InjectAdapter extends Binding<NotificationManager> implements MembersInjector<NotificationManager> {
    private Binding<Bus> bus;
    private Binding<UIStateManager> uiStateManager;

    public NotificationManager$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.framework.notifications.NotificationManager", false, NotificationManager.class);
    }

    public void attach(Linker linker) {
        this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", NotificationManager.class, getClass().getClassLoader());
        this.bus = linker.requestBinding("com.squareup.otto.Bus", NotificationManager.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.uiStateManager);
        injectMembersBindings.add(this.bus);
    }

    public void injectMembers(NotificationManager object) {
        object.uiStateManager = (UIStateManager) this.uiStateManager.get();
        object.bus = (Bus) this.bus.get();
    }
}
