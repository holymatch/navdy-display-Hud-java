package com.navdy.hud.app.framework.music;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;
import com.navdy.service.library.log.Logger;
import java.lang.reflect.Field;

public class OutlineTextView extends TextView {
    private static final Logger sLogger = new Logger(OutlineTextView.class);
    Field currentTextColorField;

    public OutlineTextView(Context context) {
        this(context, null);
    }

    public OutlineTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OutlineTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        try {
            this.currentTextColorField = TextView.class.getDeclaredField("mCurTextColor");
            this.currentTextColorField.setAccessible(true);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public void onDraw(Canvas canvas) {
        int restoreColor = getCurrentTextColor();
        TextPaint paint = getPaint();
        paint.setStyle(Style.STROKE);
        paint.setStrokeJoin(Join.MITER);
        paint.setStrokeMiter(1.0f);
        setCurrentTextColor(Integer.valueOf(-16777216));
        paint.setStrokeWidth(4.0f);
        super.onDraw(canvas);
        paint.setStyle(Style.FILL);
        setCurrentTextColor(Integer.valueOf(restoreColor));
        super.onDraw(canvas);
    }

    private void setCurrentTextColor(Integer color) {
        try {
            if (this.currentTextColorField != null) {
                this.currentTextColorField.set(this, color);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
