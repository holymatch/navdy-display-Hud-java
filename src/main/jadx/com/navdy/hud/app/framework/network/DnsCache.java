package com.navdy.hud.app.framework.network;

import java.util.HashMap;

class DnsCache {
    private HashMap<String, String> IPtoHostnameMap = new HashMap();

    DnsCache() {
    }

    public synchronized void addEntry(String ip, String host) {
        this.IPtoHostnameMap.put(ip, host);
    }

    public synchronized String getHostnamefromIP(String ip) {
        return (String) this.IPtoHostnameMap.get(ip);
    }

    public synchronized void clear(String ip, String host) {
        this.IPtoHostnameMap.clear();
    }
}
