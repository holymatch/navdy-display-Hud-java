package com.navdy.hud.app.framework.connection;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.makeramen.RoundedTransformationBuilder;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.event.Disconnect;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader.Priority;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager.ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.profile.DriverProfile;
import com.navdy.hud.app.screen.WelcomeScreen;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.service.ConnectionHandler.ApplicationLaunchAttempted;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.hud.app.view.ToastView;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.RemoteDeviceRegistry;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.input.LaunchAppEvent;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.picasso.Transformation;
import java.io.File;
import java.util.ArrayList;

public class ConnectionNotification {
    private static final int CONNECTED_TIMEOUT = 2000;
    public static final String CONNECT_ID = "connection#toast";
    public static final String DISCONNECT_ID = "disconnection#toast";
    private static final String EMPTY = "";
    private static final int NOT_CONNECTED_TIMEOUT = 30000;
    private static final int TAG_CONNECT = 0;
    private static final int TAG_DISMISS = 1;
    private static final int TAG_DONT_LAUNCH = 3;
    private static final int TAG_LAUNCH_APP = 2;
    private static final ArrayList<Choice> appClosedChoices = new ArrayList(2);
    private static final ArrayList<Choice> appClosedLaunchFailedChoices = new ArrayList(1);
    private static final int appNotConnectedWidth;
    private static final Bus bus = RemoteDeviceManager.getInstance().getBus();
    private static final String connect;
    private static final String connectedTitle;
    private static final int contentWidth;
    private static final ArrayList<Choice> disconnectedChoices = new ArrayList(2);
    private static final String disconnectedTitle;
    private static final String dismiss;
    private static Handler handler = new Handler(Looper.getMainLooper());
    private static final String iphoneAppClosedLaunchFailedMessage;
    private static final String iphoneAppClosedMessage1;
    private static final String iphoneAppClosedMessage2;
    private static final String iphoneAppClosedTitle;
    private static final String launch;
    private static Transformation roundTransformation = new RoundedTransformationBuilder().oval(true).build();
    private static final Logger sLogger = new Logger(ConnectionNotification.class);

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        connectedTitle = resources.getString(R.string.phone_connected);
        disconnectedTitle = resources.getString(R.string.phone_disconnected);
        iphoneAppClosedTitle = resources.getString(R.string.app_disconnected);
        iphoneAppClosedMessage1 = resources.getString(R.string.app_disconnected_title);
        iphoneAppClosedMessage2 = resources.getString(R.string.app_disconnected_msg);
        iphoneAppClosedLaunchFailedMessage = resources.getString(R.string.app_disconnected_msg_launch_failed);
        connect = resources.getString(R.string.connect);
        dismiss = resources.getString(R.string.dismiss);
        launch = resources.getString(R.string.launch);
        disconnectedChoices.add(new Choice(connect, 0));
        disconnectedChoices.add(new Choice(dismiss, 1));
        appClosedChoices.add(new Choice(launch, 2));
        appClosedChoices.add(new Choice(dismiss, 3));
        appClosedLaunchFailedChoices.add(new Choice(dismiss, 3));
        contentWidth = (int) resources.getDimension(R.dimen.toast_phone_disconnected_width);
        appNotConnectedWidth = (int) resources.getDimension(R.dimen.toast_app_disconnected_width);
    }

    public static void showDisconnectedToast(boolean showAppLaunch) {
        RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
        ConnectionHandler connectionHandler = remoteDeviceManager.getConnectionHandler();
        DeviceInfo deviceInfo = remoteDeviceManager.getRemoteDeviceInfo();
        NavdyDeviceId deviceId = null;
        Bundle bundle = new Bundle();
        boolean load = false;
        File path = null;
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 30000);
        bundle.putInt(ToastPresenter.EXTRA_SIDE_IMAGE, R.drawable.icon_type_phone_disconnected);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_STYLE, R.style.ToastMainTitle);
        bundle.putBoolean(ToastPresenter.EXTRA_SUPPORTS_GESTURE, true);
        boolean setDisconnectedData = false;
        if (showAppLaunch || (deviceInfo != null && connectionHandler.isAppClosed())) {
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE, iphoneAppClosedTitle);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_navdyapp);
            bundle.putInt(ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, appNotConnectedWidth);
            if (connectionHandler.isAppLaunchAttempted()) {
                sLogger.d("App launch has already been attempted but app is not launched yet");
                bundle.putParcelableArrayList(ToastPresenter.EXTRA_CHOICE_LIST, appClosedLaunchFailedChoices);
                bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_3, iphoneAppClosedLaunchFailedMessage);
                bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, R.style.Toast_3);
            } else {
                sLogger.d("App launch is not attempted yet so showing the app launch prompt");
                bundle.putParcelableArrayList(ToastPresenter.EXTRA_CHOICE_LIST, appClosedChoices);
                bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_3, iphoneAppClosedMessage1);
                bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, R.style.Toast_3);
            }
        } else {
            DeviceInfo lastConnectedDeviceInfo = connectionHandler.getLastConnectedDeviceInfo();
            if (lastConnectedDeviceInfo == null) {
                sLogger.v("last connected device not found");
                ConnectionInfo connectionInfo = RemoteDeviceRegistry.getInstance(HudApplication.getAppContext()).getLastPairedDevice();
                if (connectionInfo == null) {
                    sLogger.v("no last paired device found");
                    bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_user_numberonly);
                } else {
                    deviceId = connectionInfo.getDeviceId();
                    sLogger.v("last paired device found:" + deviceId);
                }
            } else {
                sLogger.v("last connected device found");
                deviceId = new NavdyDeviceId(lastConnectedDeviceInfo.deviceId);
            }
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE, disconnectedTitle);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, disconnectedTitle);
            bundle.putParcelableArrayList(ToastPresenter.EXTRA_CHOICE_LIST, disconnectedChoices);
            bundle.putInt(ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, contentWidth);
            setDisconnectedData = true;
        }
        DriverProfile profile = null;
        if (deviceId != null) {
            profile = DriverProfileHelper.getInstance().getDriverProfileManager().getProfileForId(deviceId);
            String deviceName = deviceId.getDeviceName();
            String driverName = null;
            if (profile == null) {
                sLogger.v("driver profile not found:" + deviceId);
                bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_user_numberonly);
            } else {
                driverName = profile.getDriverName();
                sLogger.v("driver profile found:" + deviceId);
                File imagePath = profile.getDriverImageFile();
                boolean imageInCache = PicassoUtil.isImageAvailableInCache(imagePath);
                String name = getName(profile, deviceId);
                if (imageInCache) {
                    bundle.putString(ToastPresenter.EXTRA_MAIN_IMAGE_CACHE_KEY, imagePath.getAbsolutePath());
                } else {
                    int resId = ContactImageHelper.getInstance().getDriverImageResId(deviceName);
                    String initials = ContactUtil.getInitials(name);
                    bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, resId);
                    bundle.putString(ToastPresenter.EXTRA_MAIN_IMAGE_INITIALS, initials);
                    load = true;
                    path = imagePath;
                }
            }
            if (setDisconnectedData) {
                if (TextUtils.isEmpty(driverName)) {
                    bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, deviceName);
                    bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
                } else {
                    bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, driverName);
                    bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
                    bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_3, deviceName);
                    bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, R.style.Toast_1);
                }
            }
        }
        final boolean z = load;
        final File file = path;
        final DriverProfile driverProfile = profile;
        IToastCallback callback = new IToastCallback() {
            InitialsImageView driverImage;
            ToastView toastView;

            public void onStart(ToastView view) {
                this.toastView = view;
                this.driverImage = view.getMainLayout().screenImage;
                this.driverImage.setTag(ConnectionNotification.DISCONNECT_ID);
                if (z) {
                    ConnectionNotification.loadDriveImagefromProfile(this.driverImage, ConnectionNotification.DISCONNECT_ID, file, driverProfile);
                }
            }

            public void onStop() {
                this.toastView = null;
                this.driverImage.setTag(null);
                this.driverImage = null;
            }

            public boolean onKey(CustomKeyEvent event) {
                return false;
            }

            public void executeChoiceItem(int pos, int id) {
                if (this.toastView != null) {
                    switch (id) {
                        case 0:
                            this.toastView.dismissToast();
                            Bundle args = new Bundle();
                            args.putString(WelcomeScreen.ARG_ACTION, WelcomeScreen.ACTION_RECONNECT);
                            ConnectionNotification.bus.post(new ShowScreenWithArgs(Screen.SCREEN_WELCOME, args, false));
                            return;
                        case 1:
                            this.toastView.dismissToast();
                            return;
                        case 2:
                            this.toastView.dismissToast();
                            ConnectionNotification.bus.post(new ApplicationLaunchAttempted());
                            ConnectionNotification.bus.post(new RemoteEvent(new LaunchAppEvent(null)));
                            return;
                        case 3:
                            ConnectionNotification.bus.post(new Disconnect());
                            this.toastView.dismissToast();
                            return;
                        default:
                            return;
                    }
                }
            }
        };
        ToastManager toastManager = ToastManager.getInstance();
        toastManager.dismissCurrentToast(toastManager.getCurrentToastId());
        toastManager.clearAllPendingToast();
        toastManager.addToast(new ToastParams(DISCONNECT_ID, bundle, callback, true, true));
    }

    public static void showConnectedToast() {
        final DriverProfile profile = DriverProfileHelper.getInstance().getCurrentProfile();
        NavdyDeviceId deviceId = RemoteDeviceManager.getInstance().getDeviceId();
        if (deviceId != null) {
            final File imagePath = profile.getDriverImageFile();
            boolean imageInCache = PicassoUtil.isImageAvailableInCache(imagePath);
            String name = getName(profile, deviceId);
            String deviceName = deviceId.getDeviceName();
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 2000);
            if (imageInCache) {
                bundle.putString(ToastPresenter.EXTRA_MAIN_IMAGE_CACHE_KEY, imagePath.getAbsolutePath());
            } else {
                int resId = ContactImageHelper.getInstance().getDriverImageResId(deviceId.getDeviceName());
                String initials = ContactUtil.getInitials(name);
                bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, resId);
                bundle.putString(ToastPresenter.EXTRA_MAIN_IMAGE_INITIALS, initials);
            }
            bundle.putInt(ToastPresenter.EXTRA_SIDE_IMAGE, R.drawable.icon_alert_phone_connected);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE, connectedTitle);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_STYLE, R.style.ToastMainTitle);
            String driverName = profile.getDriverName();
            if (TextUtils.isEmpty(driverName)) {
                bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, deviceName);
                bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
            } else {
                bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, driverName);
                bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
                bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_3, deviceName);
                bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, R.style.Toast_1);
            }
            bundle.putInt(ToastPresenter.EXTRA_INFO_CONTAINER_MAX_WIDTH, contentWidth);
            IToastCallback callback = null;
            if (!imageInCache) {
                callback = new IToastCallback() {
                    InitialsImageView driverImage;

                    public void onStart(ToastView view) {
                        this.driverImage = view.getMainLayout().screenImage;
                        this.driverImage.setTag("connection#toast");
                        ConnectionNotification.loadDriveImagefromProfile(this.driverImage, "connection#toast", imagePath, profile);
                    }

                    public void onStop() {
                        this.driverImage.setTag(null);
                        this.driverImage = null;
                    }

                    public boolean onKey(CustomKeyEvent event) {
                        return false;
                    }

                    public void executeChoiceItem(int pos, int id) {
                    }
                };
            }
            ToastManager toastManager = ToastManager.getInstance();
            toastManager.dismissCurrentToast(DISCONNECT_ID);
            toastManager.clearPendingToast(DISCONNECT_ID);
            toastManager.addToast(new ToastParams("connection#toast", bundle, callback, true, true));
        }
    }

    private static String getName(DriverProfile profile, NavdyDeviceId deviceId) {
        String n = profile.getDriverName();
        if (TextUtils.isEmpty(n)) {
            n = deviceId.getDeviceName();
        }
        if (n == null) {
            return "";
        }
        return n;
    }

    private static void loadDriveImagefromProfile(final InitialsImageView driverImage, final String tag, final File imagePath, final DriverProfile profile) {
        Bitmap bitmap = PicassoUtil.getBitmapfromCache(imagePath);
        if (bitmap != null) {
            driverImage.setInitials(null, Style.DEFAULT);
            driverImage.setImageBitmap(bitmap);
            return;
        }
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (imagePath.exists()) {
                    ConnectionNotification.handler.post(new Runnable() {
                        public void run() {
                            if (driverImage.getTag() == tag) {
                                driverImage.setInitials(null, Style.DEFAULT);
                                PicassoUtil.getInstance().load(imagePath).fit().transform(ConnectionNotification.roundTransformation).into(driverImage);
                            }
                        }
                    });
                } else if (profile != null) {
                    File f = profile.getDriverImageFile();
                    if (f != null && !f.exists()) {
                        ConnectionNotification.sLogger.d("photo not available locally");
                        PhoneImageDownloader.getInstance().submitDownload(DriverProfile.DRIVER_PROFILE_IMAGE, Priority.NORMAL, PhotoType.PHOTO_DRIVER_PROFILE, null);
                    }
                }
            }
        }, 1);
    }
}
