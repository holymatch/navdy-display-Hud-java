package com.navdy.hud.app.framework.toast;

import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.view.ToastView;

public interface IToastCallback {
    void executeChoiceItem(int i, int i2);

    boolean onKey(CustomKeyEvent customKeyEvent);

    void onStart(ToastView toastView);

    void onStop();
}
