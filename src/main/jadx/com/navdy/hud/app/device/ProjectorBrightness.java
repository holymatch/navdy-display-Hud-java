package com.navdy.hud.app.device;

import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.util.SerialNumber;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProjectorBrightness {
    private static final List<String> AUTO_BRIGHTNESS_MAPPING = readAutoBrightnessMapping();
    private static final List<String> AUTO_BRIGHTNESS_MAPPING_FILES = new ArrayList<String>() {
        {
            add("/device/etc/calibration/dlpc_brightness");
            add("/system/etc/calibration/dlpc_brightness");
        }
    };
    private static final int BRIGHTNESS_SCALE_VALUES_COUNT = 256;
    private static final String DATA_FILE = "/sys/dlpc/RGB_Brightness";
    private static final Logger sLogger = new Logger(ProjectorBrightness.class);

    public static void init() {
    }

    private static List<String> readAutoBrightnessMapping() {
        FileInputStream fileIS;
        BufferedReader reader;
        IOException e;
        Throwable th;
        List<String> result = Collections.emptyList();
        String suffix = "_v4";
        if (SerialNumber.instance.revisionCode.equals(ToastPresenter.EXTRA_MAIN_TITLE_1_STYLE)) {
            suffix = "_v3";
        }
        for (String file : AUTO_BRIGHTNESS_MAPPING_FILES) {
            String file2 = file2 + suffix;
            result = new ArrayList(256);
            fileIS = null;
            reader = null;
            try {
                FileInputStream fileIS2 = new FileInputStream(file2);
                try {
                    BufferedReader reader2 = new BufferedReader(new InputStreamReader(fileIS2));
                    try {
                        for (String line = reader2.readLine(); line != null && result.size() < 256; line = reader2.readLine()) {
                            result.add(line.trim());
                        }
                        if (result.size() >= 256) {
                            sLogger.i("Successfully read mapping from file " + file2);
                            IOUtils.closeStream(reader2);
                            IOUtils.closeStream(fileIS2);
                            break;
                        }
                        sLogger.w("File " + file2 + " not complete - skipping it");
                        IOUtils.closeStream(reader2);
                        IOUtils.closeStream(fileIS2);
                        reader = reader2;
                        fileIS = fileIS2;
                    } catch (FileNotFoundException e2) {
                        reader = reader2;
                        fileIS = fileIS2;
                    } catch (IOException e3) {
                        e = e3;
                        reader = reader2;
                        fileIS = fileIS2;
                        sLogger.e("Cannot read auto-brightness mapping file " + file2 + " : " + e.getMessage());
                        IOUtils.closeStream(reader);
                        IOUtils.closeStream(fileIS);
                    } catch (Throwable th2) {
                        th = th2;
                        reader = reader2;
                        fileIS = fileIS2;
                    }
                } catch (FileNotFoundException e4) {
                    fileIS = fileIS2;
                    try {
                        sLogger.w(file2 + " not found - checking next file");
                        IOUtils.closeStream(reader);
                        IOUtils.closeStream(fileIS);
                    } catch (Throwable th3) {
                        th = th3;
                    }
                } catch (IOException e5) {
                    e = e5;
                    fileIS = fileIS2;
                    sLogger.e("Cannot read auto-brightness mapping file " + file2 + " : " + e.getMessage());
                    IOUtils.closeStream(reader);
                    IOUtils.closeStream(fileIS);
                } catch (Throwable th4) {
                    th = th4;
                    fileIS = fileIS2;
                }
            } catch (FileNotFoundException e6) {
                sLogger.w(file2 + " not found - checking next file");
                IOUtils.closeStream(reader);
                IOUtils.closeStream(fileIS);
            } catch (IOException e7) {
                e = e7;
                sLogger.e("Cannot read auto-brightness mapping file " + file2 + " : " + e.getMessage());
                IOUtils.closeStream(reader);
                IOUtils.closeStream(fileIS);
            }
        }
        if (result.size() >= 256) {
            return result;
        }
        sLogger.e("No auto-brightness mapping file found");
        return Collections.emptyList();
        IOUtils.closeStream(reader);
        IOUtils.closeStream(fileIS);
        throw th;
    }

    public static int getValue() throws IOException {
        try {
            return getValue(IOUtils.convertFileToString(DATA_FILE).trim());
        } catch (NumberFormatException e) {
            sLogger.e("Exception while reading auto-brightness value", e);
            throw new IOException("Corrupted data in file with auto-brightness value");
        }
    }

    protected static int getValue(String readLine) {
        int value = AUTO_BRIGHTNESS_MAPPING.indexOf(readLine);
        if (value >= 0) {
            return value;
        }
        sLogger.e("Cannot get brightness value for auto-brightness string '" + readLine + "'");
        return 0;
    }
}
