package com.navdy.hud.app.device.gps;

import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.config.BooleanSetting;
import com.navdy.hud.app.config.BooleanSetting.Scope;
import com.navdy.service.library.log.Logger;

public class GpsUtils {
    public static BooleanSetting SHOW_RAW_GPS = new BooleanSetting("Show Raw GPS", Scope.NEVER, "map.raw_gps", "Add map indicators showing raw and map matched GPS");
    private static final Logger sLogger = new Logger(GpsUtils.class);

    public static class GpsSatelliteData {
        public Bundle data;

        public GpsSatelliteData(Bundle data) {
            this.data = data;
        }
    }

    public static class GpsSwitch {
        public boolean usingPhone;
        public boolean usingUblox;

        public GpsSwitch(boolean usingPhone, boolean usingUblox) {
            this.usingPhone = usingPhone;
            this.usingUblox = usingUblox;
        }

        public String toString() {
            return "GpsSwitch [UsingPhone :" + this.usingPhone + ", UsingUblox : " + this.usingUblox + "]";
        }
    }

    public enum HeadingDirection {
        N,
        NE,
        E,
        SE,
        S,
        SW,
        W,
        NW
    }

    public static boolean isDebugRawGpsPosEnabled() {
        return SHOW_RAW_GPS.isEnabled();
    }

    public static HeadingDirection getHeadingDirection(double heading) {
        HeadingDirection headingDirection = HeadingDirection.N;
        if (heading < 0.0d || heading > 360.0d) {
            return headingDirection;
        }
        return HeadingDirection.values()[(int) (((float) ((22.5d + heading) % 360.0d)) / 45.0f)];
    }

    public static void sendEventBroadcast(String eventName, Bundle extras) {
        try {
            Intent intent = new Intent(eventName);
            if (extras != null) {
                intent.putExtras(extras);
            }
            HudApplication.getAppContext().sendBroadcastAsUser(intent, Process.myUserHandle());
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }
}
