package com.navdy.hud.app.device.dial;

import android.bluetooth.BluetoothDevice;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastManager.ToastParams;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;

public class DialNotification {
    private static final String[] BATTERY_TOASTS = new String[]{DIAL_LOW_BATTERY_ID, DIAL_VERY_LOW_BATTERY_ID, DIAL_EXTREMELY_LOW_BATTERY_ID};
    private static final int DIAL_BATTERY_TIMEOUT = 2000;
    private static final int DIAL_BATTERY_TIMEOUT_EXTREMELY_LOW = 20000;
    public static final int DIAL_CONNECTED_TIMEOUT = 5000;
    public static final String DIAL_CONNECT_ID = "dial-connect";
    private static final int DIAL_DISCONNECTED_TIMEOUT = 1000;
    public static final String DIAL_DISCONNECT_ID = "dial-disconnect";
    public static final String DIAL_EXTREMELY_LOW_BATTERY_ID = "dial-exlow-battery";
    public static final String DIAL_FORGOTTEN_ID = "dial-forgotten";
    private static final int DIAL_FORGOTTEN_TIMEOUT = 2000;
    public static final String DIAL_LOW_BATTERY_ID = "dial-low-battery";
    private static final String[] DIAL_TOASTS = new String[]{DIAL_CONNECT_ID, DIAL_DISCONNECT_ID, DIAL_FORGOTTEN_ID, DIAL_LOW_BATTERY_ID, DIAL_VERY_LOW_BATTERY_ID, DIAL_EXTREMELY_LOW_BATTERY_ID};
    public static final String DIAL_VERY_LOW_BATTERY_ID = "dial-vlow-battery";
    private static final String EMPTY = "";
    private static final String battery_unknown = resources.getString(R.string.question_mark);
    private static final String connected = resources.getString(R.string.dial_paired_text);
    private static final String disconnected = resources.getString(R.string.connection_status_disconnected);
    private static final String forgotten = resources.getString(R.string.dial_forgotten);
    private static final String forgotten_plural = resources.getString(R.string.dial_forgotten_multiple);
    private static Resources resources = HudApplication.getAppContext().getResources();
    private static final Logger sLogger = new Logger(DialNotification.class);

    public static void showConnectedToast() {
        Bundle bundle = new Bundle();
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 5000);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_dial_2);
        bundle.putInt(ToastPresenter.EXTRA_SIDE_IMAGE, R.drawable.icon_sm_success);
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(R.string.navdy_dial));
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1_bold);
        if (!TextUtils.isEmpty(getDialAddressPart(getDialName()))) {
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_3, resources.getString(R.string.navdy_dial_name, new Object[]{dialName}));
        }
        bundle.putString(ToastPresenter.EXTRA_TTS, TTSUtils.TTS_DIAL_CONNECTED);
        showDialToast(DIAL_CONNECT_ID, bundle, true);
    }

    public static void showDisconnectedToast(String dialName) {
        BaseScreen screen = RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
        if (screen == null || screen.getScreen() != Screen.SCREEN_DIAL_PAIRING) {
            Bundle bundle = new Bundle();
            bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 1000);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_dial_forgotten);
            bundle.putInt(ToastPresenter.EXTRA_SIDE_IMAGE, R.drawable.icon_sm_forgotten);
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, disconnected);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
            if (!TextUtils.isEmpty(dialName)) {
                bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_3, dialName);
            }
            bundle.putString(ToastPresenter.EXTRA_TTS, TTSUtils.TTS_DIAL_DISCONNECTED);
            showDialToast(DIAL_DISCONNECT_ID, bundle, true);
            return;
        }
        sLogger.v("not showing dial disconnected toast");
    }

    public static void showForgottenToast(boolean plural, String name) {
        Bundle bundle = new Bundle();
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 2000);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_dial_forgotten);
        bundle.putInt(ToastPresenter.EXTRA_SIDE_IMAGE, R.drawable.icon_sm_forgotten);
        if (plural) {
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, forgotten_plural);
        } else {
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, forgotten);
        }
        String dialName = name;
        if (!(plural || TextUtils.isEmpty(dialName))) {
            bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_3, dialName);
            bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_3_STYLE, R.style.title3_single);
        }
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
        bundle.putString(ToastPresenter.EXTRA_TTS, TTSUtils.TTS_DIAL_FORGOTTEN);
        showDialToast(DIAL_FORGOTTEN_ID, bundle, true);
    }

    public static void showLowBatteryToast() {
        Bundle bundle = new Bundle();
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 2000);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_toast_dial_battery_low);
        String level = getDialBatteryLevel();
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(R.string.dial_low_battery, new Object[]{level}));
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
        bundle.putString(ToastPresenter.EXTRA_TTS, TTSUtils.TTS_DIAL_BATTERY_LOW);
        showDialToast(DIAL_LOW_BATTERY_ID, bundle, true);
    }

    public static void showVeryLowBatteryToast() {
        Bundle bundle = new Bundle();
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, 2000);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_toast_dial_battery_low);
        String level = getDialBatteryLevel();
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(R.string.dial_low_battery, new Object[]{level}));
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
        bundle.putString(ToastPresenter.EXTRA_TTS, TTSUtils.TTS_DIAL_BATTERY_VERY_LOW);
        showDialToast(DIAL_VERY_LOW_BATTERY_ID, bundle, false);
    }

    public static void showExtremelyLowBatteryToast() {
        Bundle bundle = new Bundle();
        bundle.putInt(ToastPresenter.EXTRA_TIMEOUT, DIAL_BATTERY_TIMEOUT_EXTREMELY_LOW);
        bundle.putInt(ToastPresenter.EXTRA_MAIN_IMAGE, R.drawable.icon_toast_dial_battery_very_low);
        String level = getDialBatteryLevel();
        bundle.putString(ToastPresenter.EXTRA_MAIN_TITLE_2, resources.getString(R.string.dial_ex_low_battery, new Object[]{level}));
        bundle.putInt(ToastPresenter.EXTRA_MAIN_TITLE_2_STYLE, R.style.Glances_1);
        bundle.putBoolean(ToastPresenter.EXTRA_DEFAULT_CHOICE, true);
        bundle.putString(ToastPresenter.EXTRA_TTS, TTSUtils.TTS_DIAL_BATTERY_EXTREMELY_LOW);
        showDialToast(DIAL_EXTREMELY_LOW_BATTERY_ID, bundle, false);
    }

    private static void showDialToast(String toastId, Bundle bundle, boolean removeOnDisable) {
        ToastManager toastManager = ToastManager.getInstance();
        String currentToastId = toastManager.getCurrentToastId();
        toastManager.clearPendingToast(DIAL_TOASTS);
        for (String s : DIAL_TOASTS) {
            if (!TextUtils.equals(s, toastId)) {
                toastManager.dismissCurrentToast(s);
            }
        }
        if (!TextUtils.equals(toastId, currentToastId)) {
            ToastManager.getInstance().addToast(new ToastParams(toastId, bundle, null, removeOnDisable, false));
        }
    }

    public static void dismissAllBatteryToasts() {
        ToastManager toastManager = ToastManager.getInstance();
        toastManager.dismissCurrentToast(BATTERY_TOASTS);
        toastManager.clearPendingToast(BATTERY_TOASTS);
    }

    public static String getDialName() {
        BluetoothDevice device = DialManager.getInstance().getDialDevice();
        if (device == null) {
            return null;
        }
        String dialName = device.getName();
        if (!TextUtils.isEmpty(dialName)) {
            return dialName;
        }
        dialName = device.getAddress();
        int len = dialName.length();
        if (len > 4) {
            dialName = dialName.substring(len - 4);
        }
        return HudApplication.getAppContext().getString(R.string.dial_str, new Object[]{dialName});
    }

    public static String getDialAddressPart(String dialName) {
        if (TextUtils.isEmpty(dialName)) {
            return dialName;
        }
        int index = dialName.indexOf(HereManeuverDisplayBuilder.OPEN_BRACKET);
        if (index < 0) {
            return dialName;
        }
        dialName = dialName.substring(index + 1);
        index = dialName.indexOf(HereManeuverDisplayBuilder.CLOSE_BRACKET);
        if (index >= 0) {
            return dialName.substring(0, index);
        }
        return dialName;
    }

    public static String getDialBatteryLevel() {
        int level = DialManager.getInstance().getLastKnownBatteryLevel();
        if (level == -1) {
            return battery_unknown;
        }
        return String.valueOf(DialManager.getDisplayBatteryLevel(level));
    }
}
