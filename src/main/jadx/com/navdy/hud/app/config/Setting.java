package com.navdy.hud.app.config;

public class Setting {
    private static int MAX_PROP_LEN = 31;
    private String description;
    private String name;
    private IObserver observer;
    private String path;
    private String property;

    interface IObserver {
        void onChanged(Setting setting);
    }

    public Setting(String name, String path, String description) {
        this(name, path, description, "persist.sys." + path);
    }

    public Setting(String name, String path, String description, String property) {
        this.name = name;
        this.path = path;
        this.description = description;
        if (property == null || property.length() <= MAX_PROP_LEN) {
            this.property = property;
            return;
        }
        throw new IllegalArgumentException("property name (" + property + ") too long");
    }

    public String getName() {
        return this.name;
    }

    public String getPath() {
        return this.path;
    }

    public String getDescription() {
        return this.description;
    }

    public String getProperty() {
        return this.property;
    }

    public boolean isEnabled() {
        return true;
    }

    protected void changed() {
        if (this.observer != null) {
            this.observer.onChanged(this);
        }
    }

    public void setObserver(IObserver observer) {
        this.observer = observer;
    }
}
