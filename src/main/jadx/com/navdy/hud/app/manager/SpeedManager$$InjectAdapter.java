package com.navdy.hud.app.manager;

import com.navdy.hud.app.profile.DriverProfileManager;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class SpeedManager$$InjectAdapter extends Binding<SpeedManager> implements Provider<SpeedManager>, MembersInjector<SpeedManager> {
    private Binding<Bus> bus;
    private Binding<DriverProfileManager> driverProfileManager;

    public SpeedManager$$InjectAdapter() {
        super("com.navdy.hud.app.manager.SpeedManager", "members/com.navdy.hud.app.manager.SpeedManager", false, SpeedManager.class);
    }

    public void attach(Linker linker) {
        this.driverProfileManager = linker.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", SpeedManager.class, getClass().getClassLoader());
        this.bus = linker.requestBinding("com.squareup.otto.Bus", SpeedManager.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.driverProfileManager);
        injectMembersBindings.add(this.bus);
    }

    public SpeedManager get() {
        SpeedManager result = new SpeedManager();
        injectMembers(result);
        return result;
    }

    public void injectMembers(SpeedManager object) {
        object.driverProfileManager = (DriverProfileManager) this.driverProfileManager.get();
        object.bus = (Bus) this.bus.get();
    }
}
