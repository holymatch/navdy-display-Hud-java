package com.navdy.hud.app.manager;

import android.content.Context;
import android.content.SharedPreferences;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.analytics.TelemetryDataManager;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.debug.DriveRecorder;
import com.navdy.hud.app.framework.calendar.CalendarManager;
import com.navdy.hud.app.framework.phonecall.CallManager;
import com.navdy.hud.app.framework.trips.TripManager;
import com.navdy.hud.app.framework.voice.VoiceSearchHandler;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.FeatureUtil;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.LegacyCapability;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.http.IHttpManager;
import com.squareup.otto.Bus;
import javax.inject.Inject;
import mortar.Mortar;

public final class RemoteDeviceManager {
    private static final Logger sLogger = new Logger(RemoteDeviceManager.class);
    private static final RemoteDeviceManager singleton = new RemoteDeviceManager();
    @Inject
    Bus bus;
    @Inject
    CalendarManager calendarManager;
    @Inject
    CallManager callManager;
    @Inject
    ConnectionHandler connectionHandler;
    @Inject
    DriveRecorder driveRecorder;
    @Inject
    FeatureUtil featureUtil;
    @Inject
    GestureServiceConnector gestureServiceConnector;
    @Inject
    IHttpManager httpManager;
    @Inject
    InputManager inputManager;
    @Inject
    MusicManager musicManager;
    @Inject
    SharedPreferences preferences;
    @Inject
    TelemetryDataManager telemetryDataManager;
    @Inject
    TimeHelper timeHelper;
    @Inject
    TripManager tripManager;
    @Inject
    UIStateManager uiStateManager;
    @Inject
    VoiceSearchHandler voiceSearchHandler;

    public static RemoteDeviceManager getInstance() {
        return singleton;
    }

    private RemoteDeviceManager() {
        Context appContext = HudApplication.getAppContext();
        if (appContext != null) {
            Mortar.inject(appContext, this);
        }
    }

    public DeviceInfo getRemoteDeviceInfo() {
        RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
        if (remoteDevice != null) {
            return remoteDevice.getDeviceInfo();
        }
        return null;
    }

    public boolean doesRemoteDeviceHasCapability(LegacyCapability capability) {
        RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
        if (remoteDevice == null) {
            return false;
        }
        DeviceInfo deviceInfo = remoteDevice.getDeviceInfo();
        if (deviceInfo == null || deviceInfo.legacyCapabilities == null || !deviceInfo.legacyCapabilities.contains(capability)) {
            return false;
        }
        return true;
    }

    public NavdyDeviceId getDeviceId() {
        RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
        if (remoteDevice != null) {
            return remoteDevice.getDeviceId();
        }
        return null;
    }

    public boolean isRemoteDeviceConnected() {
        return getRemoteDeviceInfo() != null;
    }

    public boolean isAppConnected() {
        if (!isRemoteDeviceConnected() || this.connectionHandler.isAppClosed()) {
            return false;
        }
        return true;
    }

    public Bus getBus() {
        return this.bus;
    }

    public TimeHelper getTimeHelper() {
        return this.timeHelper;
    }

    public Platform getRemoteDevicePlatform() {
        RemoteDevice remoteDevice = this.connectionHandler.getRemoteDevice();
        if (remoteDevice == null) {
            return null;
        }
        DeviceInfo deviceInfo = remoteDevice.getDeviceInfo();
        if (deviceInfo != null) {
            return deviceInfo.platform;
        }
        return Platform.PLATFORM_iOS;
    }

    public boolean isRemoteDeviceIos() {
        return Platform.PLATFORM_iOS.equals(getRemoteDevicePlatform());
    }

    public boolean isNetworkLinkReady() {
        return this.connectionHandler.isNetworkLinkReady();
    }

    public ConnectionHandler getConnectionHandler() {
        return this.connectionHandler;
    }

    public UIStateManager getUiStateManager() {
        return this.uiStateManager;
    }

    public CallManager getCallManager() {
        return this.callManager;
    }

    public SharedPreferences getSharedPreferences() {
        return this.preferences;
    }

    public InputManager getInputManager() {
        return this.inputManager;
    }

    public GestureServiceConnector getGestureServiceConnector() {
        return this.gestureServiceConnector;
    }

    public CalendarManager getCalendarManager() {
        return this.calendarManager;
    }

    public TripManager getTripManager() {
        return this.tripManager;
    }

    public DriveRecorder getDriveRecorder() {
        return this.driveRecorder;
    }

    public MusicManager getMusicManager() {
        return this.musicManager;
    }

    public VoiceSearchHandler getVoiceSearchHandler() {
        return this.voiceSearchHandler;
    }

    public FeatureUtil getFeatureUtil() {
        return this.featureUtil;
    }

    public IHttpManager getHttpManager() {
        return this.httpManager;
    }

    public TelemetryDataManager getTelemetryDataManager() {
        return this.telemetryDataManager;
    }
}
