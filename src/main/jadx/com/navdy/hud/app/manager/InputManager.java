package com.navdy.hud.app.manager;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewParent;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.analytics.AnalyticsSupport.WakeupReason;
import com.navdy.hud.app.bluetooth.obex.HeaderSet;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialManager;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.device.light.LightManager;
import com.navdy.hud.app.service.ShutdownMonitor;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class InputManager {
    private static final Logger sLogger = new Logger(InputManager.class);
    private Bus bus;
    private IInputHandler defaultHandler;
    private DialManager dialManager;
    private Handler handler = new Handler(Looper.getMainLooper());
    private IInputHandler inputHandler;
    private boolean isLongPressDetected;
    private long lastInputEventTime = 0;
    private PowerManager powerManager;
    private UIStateManager uiStateManager;

    public interface IInputHandler {
        IInputHandler nextHandler();

        boolean onGesture(GestureEvent gestureEvent);

        boolean onKey(CustomKeyEvent customKeyEvent);
    }

    public enum CustomKeyEvent {
        LEFT,
        RIGHT,
        SELECT,
        LONG_PRESS,
        POWER_BUTTON_CLICK,
        POWER_BUTTON_DOUBLE_CLICK,
        POWER_BUTTON_LONG_PRESS
    }

    public InputManager(Bus bus, PowerManager powerManager, UIStateManager uiStateManager) {
        this.powerManager = powerManager;
        this.bus = bus;
        this.bus.register(this);
        this.uiStateManager = uiStateManager;
    }

    public void setFocus(IInputHandler inputHandler) {
        this.inputHandler = inputHandler;
    }

    public IInputHandler getFocus() {
        return this.inputHandler;
    }

    public long getLastInputEventTime() {
        return this.lastInputEventTime;
    }

    @Subscribe
    public void onGestureEvent(GestureEvent event) {
        invokeHandler(event, null);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (sLogger.isLoggable(3)) {
            sLogger.v("onKeyDown:" + keyCode + " isLongPress:" + event.isLongPress() + " " + event);
        }
        this.bus.post(event);
        if (this.dialManager == null) {
            this.dialManager = DialManager.getInstance();
        }
        if (this.dialManager.reportInputEvent(event)) {
            HUDLightUtils.dialActionDetected(HudApplication.getAppContext(), LightManager.getInstance());
        }
        if (event.isLongPress() || this.isLongPressDetected) {
            return true;
        }
        CustomKeyEvent customKeyEvent = convertKeyToCustomEvent(keyCode, event);
        if (customKeyEvent == null) {
            return false;
        }
        if (customKeyEvent != CustomKeyEvent.SELECT) {
            return invokeHandler(null, customKeyEvent);
        }
        event.startTracking();
        return true;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (sLogger.isLoggable(3)) {
            sLogger.v("onKeyUp:" + keyCode + " isLongPress:" + event.isLongPress() + " " + event);
        }
        this.bus.post(event);
        if (this.isLongPressDetected) {
            this.isLongPressDetected = false;
            return true;
        }
        CustomKeyEvent customKeyEvent = convertKeyToCustomEvent(keyCode, event);
        if (customKeyEvent == null) {
            return false;
        }
        switch (customKeyEvent) {
            case SELECT:
            case LONG_PRESS:
                return invokeHandler(null, customKeyEvent);
            default:
                return false;
        }
    }

    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (sLogger.isLoggable(3)) {
            sLogger.v("onKeyLongPress:" + keyCode + " " + event);
        }
        this.isLongPressDetected = true;
        return invokeHandler(null, convertKeyToCustomEvent(keyCode, event));
    }

    private CustomKeyEvent convertKeyToCustomEvent(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case 8:
            case 21:
            case 29:
            case 38:
            case 45:
            case 54:
                return CustomKeyEvent.LEFT;
            case 9:
            case 20:
            case 39:
            case 47:
            case 51:
            case 52:
            case HeaderSet.TYPE /*66*/:
                if (event.isLongPress()) {
                    return CustomKeyEvent.LONG_PRESS;
                }
                return CustomKeyEvent.SELECT;
            case 10:
            case 22:
            case 31:
            case 32:
            case 33:
            case 40:
                return CustomKeyEvent.RIGHT;
            default:
                return null;
        }
    }

    public static boolean isCenterKey(int keyCode) {
        return keyCode == 47 || keyCode == 52 || keyCode == 51 || keyCode == 66 || keyCode == 20 || keyCode == 39 || keyCode == 9;
    }

    public static boolean isLeftKey(int keyCode) {
        return keyCode == 29 || keyCode == 54 || keyCode == 45 || keyCode == 21 || keyCode == 38 || keyCode == 8;
    }

    public static boolean isRightKey(int keyCode) {
        return keyCode == 32 || keyCode == 31 || keyCode == 33 || keyCode == 22 || keyCode == 40 || keyCode == 10;
    }

    private boolean invokeHandler(GestureEvent gesture, CustomKeyEvent keyEvent) {
        if (gesture == null && keyEvent == null) {
            return false;
        }
        boolean result = false;
        if (this.inputHandler != null) {
            IInputHandler handler = this.inputHandler;
            while (handler != null) {
                if (gesture == null) {
                    if (handler.onKey(keyEvent)) {
                        result = true;
                        break;
                    }
                    handler = handler.nextHandler();
                } else if (handler.onGesture(gesture)) {
                    HUDLightUtils.showGestureDetected(HudApplication.getAppContext(), LightManager.getInstance());
                    result = true;
                    break;
                } else {
                    handler = handler.nextHandler();
                }
            }
        }
        if (!(result || keyEvent == null)) {
            switch (keyEvent) {
                case LONG_PRESS:
                    sLogger.v("long press not handled");
                    if (this.defaultHandler == null) {
                        Main main = this.uiStateManager.getRootScreen();
                        if (main != null) {
                            this.defaultHandler = main.getInputHandler();
                        }
                    }
                    if (this.defaultHandler != null) {
                        result = this.defaultHandler.onKey(keyEvent);
                        break;
                    }
                    break;
            }
        }
        if (!result) {
            return result;
        }
        this.lastInputEventTime = SystemClock.elapsedRealtime();
        ShutdownMonitor.getInstance().recordInputEvent();
        return result;
    }

    public static IInputHandler nextContainingHandler(View v) {
        ViewParent parent = v.getParent();
        while (parent != null && !(parent instanceof IInputHandler)) {
            parent = parent.getParent();
        }
        if (parent != null) {
            return (IInputHandler) parent;
        }
        return null;
    }

    public void injectKey(final CustomKeyEvent event) {
        this.powerManager.wakeUp(WakeupReason.POWERBUTTON);
        this.handler.post(new Runnable() {
            public void run() {
                if (event != null) {
                    InputManager.sLogger.v("inject key event:" + event);
                    if (!InputManager.this.invokeHandler(null, event)) {
                        Main main = InputManager.this.uiStateManager.getRootScreen();
                        if (main != null) {
                            main.handleKey(event);
                        }
                    }
                }
            }
        });
    }
}
