package com.navdy.hud.app.manager;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.KeyEvent;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.event.LocalSpeechRequest;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.DriverProfileHelper;
import com.navdy.hud.app.framework.glance.GlanceHelper;
import com.navdy.hud.app.framework.music.MusicNotification;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.service.pandora.PandoraManager;
import com.navdy.hud.app.storage.cache.MessageCache;
import com.navdy.hud.app.ui.component.mainmenu.MusicMenu2;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.MusicArtworkCache;
import com.navdy.hud.app.util.MusicArtworkCache.Callback;
import com.navdy.service.library.events.Capabilities;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.DeviceInfo.Platform;
import com.navdy.service.library.events.audio.MusicArtworkRequest;
import com.navdy.service.library.events.audio.MusicArtworkResponse;
import com.navdy.service.library.events.audio.MusicCapabilitiesRequest;
import com.navdy.service.library.events.audio.MusicCapabilitiesResponse;
import com.navdy.service.library.events.audio.MusicCapability;
import com.navdy.service.library.events.audio.MusicCollectionResponse;
import com.navdy.service.library.events.audio.MusicCollectionSource;
import com.navdy.service.library.events.audio.MusicCollectionSourceUpdate;
import com.navdy.service.library.events.audio.MusicCollectionType;
import com.navdy.service.library.events.audio.MusicDataSource;
import com.navdy.service.library.events.audio.MusicEvent;
import com.navdy.service.library.events.audio.MusicEvent.Action;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import com.navdy.service.library.events.audio.MusicShuffleMode;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.audio.MusicTrackInfo.Builder;
import com.navdy.service.library.events.audio.MusicTrackInfoRequest;
import com.navdy.service.library.events.audio.ResumeMusicRequest;
import com.navdy.service.library.events.callcontrol.PhoneEvent;
import com.navdy.service.library.events.callcontrol.PhoneStatus;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStateChange.ConnectionState;
import com.navdy.service.library.events.input.MediaRemoteKey;
import com.navdy.service.library.events.input.MediaRemoteKeyEvent;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.events.photo.PhotoUpdate;
import com.navdy.service.library.events.photo.PhotoUpdatesRequest;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.MusicDataUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Wire;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import okio.ByteString;

public class MusicManager {
    public static final MediaControl[] CONTROLS = MediaControl.values();
    public static final int DEFAULT_ARTWORK_SIZE = 200;
    public static final MusicTrackInfo EMPTY_TRACK = new Builder().playbackState(MusicPlaybackState.PLAYBACK_NONE).name(HudApplication.getAppContext().getString(R.string.music_init_title)).author("").album("").isPreviousAllowed(Boolean.valueOf(true)).isNextAllowed(Boolean.valueOf(true)).build();
    public static final long PAUSED_DUE_TO_HFP_EXPIRY_TIME = TimeUnit.MINUTES.toMillis(10);
    public static final String PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE = "PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE";
    public static final String PREFERENCE_MUSIC_CACHE_PROFILE = "PREFERENCE_MUSIC_CACHE_PROFILE";
    public static final String PREFERENCE_MUSIC_CACHE_SERIAL = "PREFERENCE_MUSIC_CACHE_SERIAL";
    private static final int PROGRESS_BAR_UPDATE_ON_ANDROID = 2000;
    public static final int THRESHOLD_TIME_BETWEEN_PAUSE_AND_POSSIBLE_CAUSE_FOR_HFP = 10000;
    private static final Logger sLogger = new Logger(MusicManager.class);
    private boolean acceptingResumes = false;
    private MusicArtworkCache artworkCache;
    private Bus bus;
    private boolean clientSupportsArtworkCaching;
    private Set<MediaControl> currentControls;
    private ByteString currentPhoto;
    private int currentPosition;
    @NonNull
    private MusicTrackInfo currentTrack;
    private Handler handler = new Handler();
    private volatile long interestingEventTime;
    private AtomicBoolean isLongPress = new AtomicBoolean(false);
    private boolean isMusicLibraryCachingEnabled = false;
    private volatile long lastPausedTime;
    private long lastTrackUpdateTime;
    private MusicCapabilitiesResponse musicCapabilities;
    private Map<MusicCollectionSource, List<MusicCollectionType>> musicCapabilityTypeMap;
    private MessageCache<MusicCollectionResponse> musicCollectionResponseMessageCache;
    private String musicMenuPath = null;
    private MusicNotification musicNotification;
    private Set<MusicUpdateListener> musicUpdateListeners = new HashSet();
    private final Runnable openNotificationRunnable = new Runnable() {
        public void run() {
            NotificationManager.getInstance().addNotification(MusicManager.this.musicNotification, MusicManager.this.uiStateManager.getMainScreensSet());
        }
    };
    private PandoraManager pandoraManager;
    private boolean pausedByUs = false;
    private volatile long pausedDueToHfpDetectedTime = 0;
    @NonNull
    private MusicDataSource previousDataSource = MusicDataSource.MUSIC_SOURCE_NONE;
    private Runnable progressBarUpdater = new Runnable() {
        public void run() {
            if (MusicPlaybackState.PLAYBACK_PLAYING.equals(MusicManager.this.currentTrack.playbackState) && MusicManager.this.currentTrack.currentPosition != null) {
                MusicManager.this.currentPosition = MusicManager.this.currentTrack.currentPosition.intValue() + ((int) (SystemClock.elapsedRealtime() - MusicManager.this.lastTrackUpdateTime));
                MusicManager.this.musicNotification.updateProgressBar(MusicManager.this.currentPosition);
                MusicManager.this.handler.postDelayed(MusicManager.this.progressBarUpdater, 2000);
            }
        }
    };
    private UIStateManager uiStateManager;
    private boolean updateListeners = true;
    private volatile boolean wasPossiblyPausedDueToHFP = false;

    public interface MusicUpdateListener {
        void onAlbumArtUpdate(@Nullable ByteString byteString, boolean z);

        void onTrackUpdated(MusicTrackInfo musicTrackInfo, Set<MediaControl> set, boolean z);
    }

    public enum MediaControl {
        PLAY,
        PAUSE,
        NEXT,
        PREVIOUS,
        MUSIC_MENU,
        MUSIC_MENU_DEEP
    }

    public MusicManager(MessageCache<MusicCollectionResponse> musicCollectionResponseMessageCache, Bus bus, Resources resources, UIStateManager uiStateManager, PandoraManager pandoraManager, MusicArtworkCache artworkCache) {
        this.musicCollectionResponseMessageCache = musicCollectionResponseMessageCache;
        this.bus = bus;
        this.uiStateManager = uiStateManager;
        this.pandoraManager = pandoraManager;
        this.currentTrack = EMPTY_TRACK;
        this.currentControls = new HashSet();
        this.currentControls.add(MediaControl.PLAY);
        this.musicNotification = new MusicNotification(this, bus);
        this.artworkCache = artworkCache;
    }

    public MusicTrackInfo getCurrentTrack() {
        return this.currentTrack;
    }

    public Set<MediaControl> getCurrentControls() {
        return this.currentControls;
    }

    public int getCurrentPosition() {
        return this.currentPosition;
    }

    public static boolean tryingToPlay(MusicPlaybackState playbackState) {
        return (MusicPlaybackState.PLAYBACK_NONE.equals(playbackState) || MusicPlaybackState.PLAYBACK_CONNECTING.equals(playbackState) || MusicPlaybackState.PLAYBACK_ERROR.equals(playbackState) || MusicPlaybackState.PLAYBACK_PAUSED.equals(playbackState) || MusicPlaybackState.PLAYBACK_STOPPED.equals(playbackState)) ? false : true;
    }

    @Subscribe
    public void onTrackInfo(MusicTrackInfo trackInfo) {
        sLogger.d("updateState " + trackInfo);
        boolean isPlaying = MusicPlaybackState.PLAYBACK_PLAYING.equals(trackInfo.playbackState);
        boolean isOldStatePlaying = MusicPlaybackState.PLAYBACK_PLAYING.equals(this.currentTrack.playbackState);
        if (trackInfo.collectionSource == MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN) {
            setMusicMenuPath(null);
        }
        if (isPlaying || !isOldStatePlaying || MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(this.currentTrack.dataSource) || !MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(trackInfo.dataSource)) {
            boolean trackChanged = !TextUtils.equals(MusicDataUtils.songIdentifierFromTrackInfo(trackInfo), MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack));
            if (trackChanged && this.clientSupportsArtworkCaching) {
                requestArtwork(trackInfo);
            }
            boolean isNewSongPlaying = isPlaying && trackChanged;
            boolean isNewStatePlaying = isPlaying && !isOldStatePlaying;
            boolean isPaused = MusicPlaybackState.PLAYBACK_PAUSED.equals(trackInfo.playbackState);
            boolean isOldStatePaused = MusicPlaybackState.PLAYBACK_PAUSED.equals(this.currentTrack.playbackState);
            boolean isNewStatePaused = isPaused && !isOldStatePaused;
            if ((isNewStatePlaying || isNewStatePaused) && Platform.PLATFORM_iOS.equals(RemoteDeviceManager.getInstance().getRemoteDevicePlatform())) {
                this.bus.post(new RemoteEvent(trackInfo));
            }
            if (isNewStatePlaying) {
                sLogger.d("Music started playing");
            }
            if (isNewStatePlaying && this.pausedByUs) {
                sLogger.d("User started playback after we paused - setting pauseByUs = false");
                this.pausedByUs = false;
            }
            if (isNewStatePaused) {
                sLogger.d("New state is paused");
                long pausedTime = SystemClock.elapsedRealtime();
                if (!this.wasPossiblyPausedDueToHFP) {
                    if (pausedTime - this.interestingEventTime < 10000) {
                        sLogger.d("Interesting event happened just before the song was paused, possibly playing through HFP");
                        this.wasPossiblyPausedDueToHFP = true;
                        this.pausedDueToHfpDetectedTime = pausedTime;
                    } else {
                        this.lastPausedTime = pausedTime;
                    }
                }
            }
            this.lastTrackUpdateTime = SystemClock.elapsedRealtime();
            boolean shouldSuppressNotification = isNewStatePlaying && this.wasPossiblyPausedDueToHFP && this.lastTrackUpdateTime - this.pausedDueToHfpDetectedTime < PAUSED_DUE_TO_HFP_EXPIRY_TIME;
            if (shouldSuppressNotification) {
                sLogger.d("Should suppress Notification");
            }
            if (shouldSuppressNotification || this.lastTrackUpdateTime - this.pausedDueToHfpDetectedTime > PAUSED_DUE_TO_HFP_EXPIRY_TIME) {
                this.wasPossiblyPausedDueToHFP = false;
                this.pausedDueToHfpDetectedTime = 0;
            }
            if (!(trackInfo.dataSource == null || MusicDataSource.MUSIC_SOURCE_NONE.equals(trackInfo.dataSource))) {
                this.previousDataSource = trackInfo.dataSource;
            }
            updateControls(trackInfo);
            boolean hasProgressInfo = (trackInfo.duration == null || trackInfo.duration.intValue() <= 0 || trackInfo.currentPosition == null) ? false : true;
            if (hasProgressInfo) {
                this.currentPosition = trackInfo.currentPosition.intValue();
                this.musicNotification.updateProgressBar(this.currentPosition);
            }
            if (Platform.PLATFORM_Android.equals(RemoteDeviceManager.getInstance().getRemoteDevicePlatform())) {
                this.handler.removeCallbacks(this.progressBarUpdater);
                if (MusicPlaybackState.PLAYBACK_PLAYING.equals(trackInfo.playbackState) && hasProgressInfo) {
                    this.handler.postDelayed(this.progressBarUpdater, 2000);
                }
            }
            boolean isOldStateStoppedOrNone = MusicPlaybackState.PLAYBACK_PAUSED.equals(this.currentTrack.playbackState) || MusicPlaybackState.PLAYBACK_NONE.equals(this.currentTrack.playbackState);
            boolean pausedExistingTrack = isNewStatePaused && !trackChanged;
            boolean trackEnd = hasProgressInfo && trackInfo.currentPosition.equals(trackInfo.duration);
            boolean interestingPauseTransition = isPaused && !trackEnd && (pausedExistingTrack || ((isOldStatePaused && trackChanged) || isOldStateStoppedOrNone));
            boolean shuffleModeChange = this.currentTrack.shuffleMode != trackInfo.shuffleMode;
            boolean z = isPlaying || interestingPauseTransition || shuffleModeChange;
            this.updateListeners = z;
            if (this.updateListeners) {
                this.currentTrack = trackInfo;
                boolean interestingPlayTransition = isNewSongPlaying || isNewStatePlaying;
                boolean willShowMusicNotification = !shouldSuppressNotification && GlanceHelper.isMusicNotificationEnabled() && interestingPlayTransition;
                if (interestingPlayTransition || interestingPauseTransition || shuffleModeChange) {
                    callTrackUpdateCallbacks(willShowMusicNotification);
                    if (!this.clientSupportsArtworkCaching) {
                        callAlbumArtCallbacks();
                    }
                }
                if (willShowMusicNotification) {
                    sLogger.d("Showing notification New Song ? : " + isNewSongPlaying + ", New state playing : " + isNewStatePlaying);
                    this.handler.removeCallbacks(this.openNotificationRunnable);
                    this.handler.post(this.openNotificationRunnable);
                    return;
                }
                return;
            }
            sLogger.d("Ignoring updates");
            return;
        }
        sLogger.d("Ignoring update from non playing Pandora");
    }

    public boolean isShuffling() {
        return (this.currentTrack.shuffleMode == null || this.currentTrack.shuffleMode == MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN || this.currentTrack.shuffleMode == MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF) ? false : true;
    }

    private void requestArtwork(final MusicTrackInfo trackInfo) {
        sLogger.d("requestArtwork: " + trackInfo);
        if (TextUtils.isEmpty(trackInfo.author) && TextUtils.isEmpty(trackInfo.album) && TextUtils.isEmpty(trackInfo.name)) {
            this.currentPhoto = null;
            callAlbumArtCallbacks();
            sLogger.d("Empty track, returning");
            return;
        }
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                MusicManager.this.artworkCache.getArtwork(trackInfo.author, trackInfo.album, trackInfo.name, new Callback() {
                    public void onHit(@NonNull byte[] artwork) {
                        MusicManager.sLogger.d("CACHE HIT");
                        MusicManager.this.currentPhoto = ByteString.of(artwork);
                        MusicManager.this.callAlbumArtCallbacks();
                    }

                    public void onMiss() {
                        MusicManager.sLogger.d("CACHE MISS");
                        MusicArtworkRequest musicArtworkRequest = new MusicArtworkRequest.Builder().name(trackInfo.name).album(trackInfo.album).author(trackInfo.author).size(Integer.valueOf(200)).build();
                        MusicManager.sLogger.d("requesting artwork: " + musicArtworkRequest);
                        MusicManager.this.bus.post(new RemoteEvent(musicArtworkRequest));
                    }
                });
            }
        }, 22);
    }

    @Subscribe
    public void onMusicArtworkResponse(MusicArtworkResponse musicArtworkResponse) {
        sLogger.d("onMusicArtworkResponse " + musicArtworkResponse);
        if (musicArtworkResponse == null || musicArtworkResponse.photo == null) {
            this.currentPhoto = null;
        } else {
            cacheAlbumArt(musicArtworkResponse);
            if (TextUtils.equals(MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack), MusicDataUtils.songIdentifierFromArtworkResponse(musicArtworkResponse))) {
                this.currentPhoto = musicArtworkResponse.photo;
            }
        }
        callAlbumArtCallbacks();
    }

    private void updateControls(MusicTrackInfo trackInfo) {
        this.currentControls = new HashSet();
        if (Boolean.TRUE.equals(trackInfo.isPreviousAllowed)) {
            this.currentControls.add(MediaControl.PREVIOUS);
        }
        if (tryingToPlay(trackInfo.playbackState)) {
            this.currentControls.add(MediaControl.PAUSE);
        } else {
            this.currentControls.add(MediaControl.PLAY);
        }
        if (Boolean.TRUE.equals(trackInfo.isNextAllowed)) {
            this.currentControls.add(MediaControl.NEXT);
        }
    }

    public void handleKeyEvent(KeyEvent event, MediaControl control, boolean isKeyPressedDown) {
        Platform remotePlatform = RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        if (remotePlatform != null && InputManager.isCenterKey(event.getKeyCode())) {
            recordAnalytics(event, control, isKeyPressedDown);
            switch (remotePlatform) {
                case PLATFORM_Android:
                    switch (event.getAction()) {
                        case 0:
                            if (event.isLongPress() && this.isLongPress.compareAndSet(false, true)) {
                                switch (control) {
                                    case NEXT:
                                        if (canSeekBackward()) {
                                            runAndroidAction(Action.MUSIC_ACTION_FAST_FORWARD_START);
                                            return;
                                        } else {
                                            sLogger.w("Fast-forward is not allowed");
                                            return;
                                        }
                                    case PREVIOUS:
                                        if (canSeekForward()) {
                                            runAndroidAction(Action.MUSIC_ACTION_REWIND_START);
                                            return;
                                        } else {
                                            sLogger.w("Rewind is not allowed");
                                            return;
                                        }
                                    default:
                                        return;
                                }
                            }
                            return;
                        case 1:
                            if (!isKeyPressedDown) {
                                return;
                            }
                            if (this.isLongPress.compareAndSet(true, false)) {
                                switch (control) {
                                    case NEXT:
                                        runAndroidAction(Action.MUSIC_ACTION_FAST_FORWARD_STOP);
                                        return;
                                    case PREVIOUS:
                                        runAndroidAction(Action.MUSIC_ACTION_REWIND_STOP);
                                        return;
                                    default:
                                        return;
                                }
                            }
                            switch (control) {
                                case NEXT:
                                    if (Boolean.TRUE.equals(this.currentTrack.isNextAllowed)) {
                                        runAndroidAction(Action.MUSIC_ACTION_NEXT);
                                        return;
                                    } else {
                                        sLogger.w("Next song action is not allowed");
                                        return;
                                    }
                                case PREVIOUS:
                                    if (Boolean.TRUE.equals(this.currentTrack.isPreviousAllowed)) {
                                        runAndroidAction(Action.MUSIC_ACTION_PREVIOUS);
                                        return;
                                    } else {
                                        sLogger.w("Previous song action is not allowed");
                                        return;
                                    }
                                default:
                                    return;
                            }
                        default:
                            return;
                    }
                case PLATFORM_iOS:
                    switch (event.getAction()) {
                        case 0:
                            if (!isKeyPressedDown) {
                                if (control == MediaControl.PLAY || control == MediaControl.PAUSE) {
                                    sendMediaKeyEvent(control, true);
                                    sendMediaKeyEvent(control, false);
                                    return;
                                }
                                sendMediaKeyEvent(control, true);
                                return;
                            }
                            return;
                        case 1:
                            if (isKeyPressedDown) {
                                sendMediaKeyEvent(control, false);
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                default:
                    return;
            }
        }
    }

    public void executeMediaControl(MediaControl control, boolean isKeyPressedDown) {
        Platform remotePlatform = RemoteDeviceManager.getInstance().getRemoteDevicePlatform();
        if (remotePlatform != null) {
            recordAnalytics(null, control, isKeyPressedDown);
            switch (remotePlatform) {
                case PLATFORM_Android:
                    switch (control) {
                        case PLAY:
                            runAndroidAction(Action.MUSIC_ACTION_PLAY);
                            return;
                        case PAUSE:
                            runAndroidAction(Action.MUSIC_ACTION_PAUSE);
                            return;
                        default:
                            return;
                    }
                case PLATFORM_iOS:
                    sendMediaKeyEvent(control, true);
                    sendMediaKeyEvent(control, false);
                    return;
                default:
                    return;
            }
        }
    }

    private void recordAnalytics(@Nullable KeyEvent event, MediaControl control, boolean isKeyPressedDown) {
        if (event == null) {
            switch (control) {
                case PLAY:
                    AnalyticsSupport.recordMusicAction("Play");
                    return;
                case PAUSE:
                    AnalyticsSupport.recordMusicAction("Pause");
                    return;
                default:
                    return;
            }
        }
        switch (event.getAction()) {
            case 0:
                if (event.isLongPress() && !this.isLongPress.get()) {
                    switch (control) {
                        case NEXT:
                            AnalyticsSupport.recordMusicAction("Fast-forward");
                            return;
                        case PREVIOUS:
                            AnalyticsSupport.recordMusicAction("Rewind");
                            return;
                        default:
                            return;
                    }
                }
                return;
            case 1:
                if (isKeyPressedDown && !this.isLongPress.get()) {
                    switch (control) {
                        case NEXT:
                            AnalyticsSupport.recordMusicAction("Next");
                            return;
                        case PREVIOUS:
                            AnalyticsSupport.recordMusicAction("Previous");
                            return;
                        default:
                            return;
                    }
                }
                return;
            default:
                return;
        }
    }

    private boolean canSeekForward() {
        return Boolean.TRUE.equals(this.currentTrack.isNextAllowed) && !Boolean.TRUE.equals(this.currentTrack.isOnlineStream);
    }

    private boolean canSeekBackward() {
        return Boolean.TRUE.equals(this.currentTrack.isPreviousAllowed) && !Boolean.TRUE.equals(this.currentTrack.isOnlineStream);
    }

    private boolean runAndroidAction(Action action) {
        MusicDataSource targetDataSource = dataSourceToUse();
        if (Action.MUSIC_ACTION_PLAY.equals(action) && MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(targetDataSource)) {
            this.pandoraManager.startAndPlay();
        } else {
            sLogger.d("Media action: " + action);
            this.bus.post(new RemoteEvent(new MusicEvent.Builder().action(action).dataSource(targetDataSource).build()));
        }
        return true;
    }

    private MusicDataSource dataSourceToUse() {
        MusicTrackInfo currentTrackAvailable = this.currentTrack;
        if (currentTrackAvailable == null || currentTrackAvailable.dataSource == null || MusicDataSource.MUSIC_SOURCE_NONE.equals(currentTrackAvailable.dataSource)) {
            return this.previousDataSource;
        }
        return currentTrackAvailable.dataSource;
    }

    private boolean sendMediaKeyEvent(MediaControl control, boolean down) {
        MediaRemoteKey key = null;
        switch (control) {
            case NEXT:
                key = MediaRemoteKey.MEDIA_REMOTE_KEY_NEXT;
                break;
            case PREVIOUS:
                key = MediaRemoteKey.MEDIA_REMOTE_KEY_PREV;
                break;
            case PLAY:
            case PAUSE:
                key = MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY;
                break;
        }
        this.bus.post(new RemoteEvent(new MediaRemoteKeyEvent.Builder().key(key).action(down ? com.navdy.service.library.events.input.KeyEvent.KEY_DOWN : com.navdy.service.library.events.input.KeyEvent.KEY_UP).build()));
        return true;
    }

    public void initialize() {
        this.bus.post(new RemoteEvent(new MusicTrackInfoRequest()));
        sLogger.d("Request for music track info sent to the client.");
    }

    public void addMusicUpdateListener(@NonNull final MusicUpdateListener listener) {
        sLogger.d("addMusicUpdateListener");
        if (this.musicUpdateListeners.contains(listener)) {
            sLogger.w("Tried to add a listener that's already listening");
        } else {
            if (this.musicUpdateListeners.size() == 0) {
                sLogger.d("Enabling album art updates (PhotoUpdate)");
                requestPhotoUpdates(true);
            }
            this.musicUpdateListeners.add(listener);
        }
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                MusicManager.this.handler.post(new Runnable() {
                    public void run() {
                        listener.onAlbumArtUpdate(MusicManager.this.currentPhoto, false);
                        listener.onTrackUpdated(MusicManager.this.getCurrentTrack(), MusicManager.this.getCurrentControls(), false);
                    }
                });
                MusicManager.sLogger.v("album art listeners:");
                for (MusicUpdateListener l : MusicManager.this.musicUpdateListeners) {
                    MusicManager.sLogger.d("- " + l);
                }
            }
        }, 1);
    }

    public void removeMusicUpdateListener(@NonNull MusicUpdateListener listener) {
        sLogger.d("removeMusicUpdateListener");
        if (this.musicUpdateListeners.contains(listener)) {
            this.musicUpdateListeners.remove(listener);
            if (this.musicUpdateListeners.size() == 0) {
                sLogger.d("Disabling album art updates");
                requestPhotoUpdates(false);
            }
        } else {
            sLogger.w("Tried to remove a non-existent listener");
        }
        sLogger.d("album art listeners:");
        for (MusicUpdateListener l : this.musicUpdateListeners) {
            sLogger.v("- " + l);
        }
    }

    private void requestPhotoUpdates(boolean start) {
        if (start && this.clientSupportsArtworkCaching) {
            sLogger.d("client supports caching, no need to request photo updates");
        } else {
            this.bus.post(new RemoteEvent(new PhotoUpdatesRequest.Builder().start(Boolean.valueOf(start)).photoType(PhotoType.PHOTO_ALBUM_ART).build()));
        }
    }

    @Subscribe
    public void onPhotoUpdate(PhotoUpdate photoUpdate) {
        sLogger.d("onPhotoUpdate " + photoUpdate);
        if (photoUpdate == null || photoUpdate.photo == null) {
            this.currentPhoto = null;
        } else {
            this.currentPhoto = photoUpdate.photo;
            String currentTrackPhotoId = MusicDataUtils.songIdentifierFromTrackInfo(this.currentTrack);
            if (TextUtils.equals(currentTrackPhotoId, photoUpdate.identifier)) {
                cacheAlbumArt(this.currentPhoto.toByteArray(), this.currentTrack);
            } else {
                sLogger.w("Received PhotoUpdate with wrong identifier: " + currentTrackPhotoId + " != " + photoUpdate.identifier);
            }
        }
        callAlbumArtCallbacks();
    }

    private void callTrackUpdateCallbacks(boolean willOpenNotification) {
        if (this.updateListeners) {
            sLogger.d("callTrackUpdateCallbacks");
            for (MusicUpdateListener listener : this.musicUpdateListeners) {
                listener.onTrackUpdated(this.currentTrack, this.currentControls, willOpenNotification);
            }
            return;
        }
        sLogger.w("callTrackUpdateCallbacks but updateListeners was false");
    }

    private void callAlbumArtCallbacks() {
        if (this.updateListeners) {
            sLogger.d("callAlbumArtCallbacks");
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    MusicManager.this.handler.post(new Runnable() {
                        public void run() {
                            for (MusicUpdateListener listener : MusicManager.this.musicUpdateListeners) {
                                listener.onAlbumArtUpdate(MusicManager.this.currentPhoto, true);
                            }
                        }
                    });
                }
            }, 1);
            return;
        }
        sLogger.w("callAlbumArtCallbacks but updateListeners was false");
    }

    public void showMusicNotification() {
        NotificationManager.getInstance().addNotification(this.musicNotification);
    }

    public boolean isPandoraDataSource() {
        return MusicDataSource.MUSIC_SOURCE_PANDORA_API.equals(this.currentTrack.dataSource);
    }

    @Subscribe
    public void onPhoneEvent(PhoneEvent phoneEvent) {
        if (phoneEvent != null && phoneEvent.status != null) {
            sLogger.d("PhoneEvent : new status " + phoneEvent.status);
            if (phoneEvent.status == PhoneStatus.PHONE_DIALING || phoneEvent.status == PhoneStatus.PHONE_RINGING || phoneEvent.status == PhoneStatus.PHONE_OFFHOOK) {
                long currentTime = SystemClock.elapsedRealtime();
                if (!this.wasPossiblyPausedDueToHFP) {
                    if (this.currentTrack == null || !this.currentTrack.playbackState.equals(MusicPlaybackState.PLAYBACK_PAUSED) || this.lastPausedTime - currentTime >= 10000) {
                        sLogger.d("Recording the phone event as interesting event");
                        this.interestingEventTime = currentTime;
                        return;
                    }
                    sLogger.d("Current state is paused recently, possibly due to call");
                    this.wasPossiblyPausedDueToHFP = true;
                    this.pausedDueToHfpDetectedTime = currentTime;
                }
            }
        }
    }

    @Subscribe
    public void onLocalSpeechRequest(LocalSpeechRequest localSpeechRequest) {
        long currentTime = SystemClock.elapsedRealtime();
        if (!this.wasPossiblyPausedDueToHFP) {
            sLogger.d("Recording the speech request as an interesting event");
            this.interestingEventTime = currentTime;
        }
    }

    @Subscribe
    public void onDriverProfileChanged(DriverProfileChanged driverProfileChanged) {
        sLogger.d("onDriverProfileChanged - " + driverProfileChanged);
        if (!DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isDefaultProfile() && this.musicUpdateListeners.size() > 0) {
            requestPhotoUpdates(true);
        }
    }

    @Subscribe
    public void onConnectionStateChange(ConnectionStateChange event) {
        if (event.state == ConnectionState.CONNECTION_DISCONNECTED) {
            NotificationManager.getInstance().removeNotification(this.musicNotification.getId());
            this.currentTrack = EMPTY_TRACK;
            this.currentPhoto = null;
            this.currentControls = null;
            this.updateListeners = true;
            callAlbumArtCallbacks();
            callTrackUpdateCallbacks(false);
            this.musicCapabilities = null;
            this.musicCapabilityTypeMap = null;
            this.musicMenuPath = null;
            MusicMenu2.clearMenuData();
        }
    }

    private void cacheAlbumArt(MusicArtworkResponse musicArtworkResponse) {
        if (musicArtworkResponse.author == null && musicArtworkResponse.album == null && musicArtworkResponse.name == null) {
            sLogger.d("Not a now playing update, returning");
        } else {
            this.artworkCache.putArtwork(musicArtworkResponse.author, musicArtworkResponse.album, musicArtworkResponse.name, musicArtworkResponse.photo.toByteArray());
        }
    }

    private void cacheAlbumArt(byte[] photo, MusicTrackInfo musicTrackInfo) {
        if (musicTrackInfo != null) {
            this.artworkCache.putArtwork(musicTrackInfo.author, musicTrackInfo.album, musicTrackInfo.name, photo);
        }
    }

    @Subscribe
    public void onDeviceInfo(DeviceInfo deviceInfo) {
        sLogger.d("onDeviceInfo - " + deviceInfo);
        this.bus.post(new RemoteEvent(new MusicCapabilitiesRequest.Builder().build()));
        Capabilities capabilities = deviceInfo.capabilities;
        if (deviceInfo.platform == Platform.PLATFORM_iOS || (capabilities != null && ((Boolean) Wire.get(capabilities.musicArtworkCache, Boolean.valueOf(false))).booleanValue())) {
            this.clientSupportsArtworkCaching = true;
        }
    }

    @Subscribe
    public void onMusicCollectionSourceUpdate(MusicCollectionSourceUpdate musicCollectionSourceUpdate) {
        sLogger.d("onMusicCollectionSourceUpdate " + musicCollectionSourceUpdate);
        if (musicCollectionSourceUpdate.collectionSource != null && musicCollectionSourceUpdate.serial_number != null) {
            checkCache(musicCollectionSourceUpdate.collectionSource.name(), musicCollectionSourceUpdate.serial_number.longValue());
        }
    }

    private void checkCache(String musicCollectionSource, long serialNumber) {
        SharedPreferences sharedPreferences = RemoteDeviceManager.getInstance().getSharedPreferences();
        String profileName = DriverProfileHelper.getInstance().getCurrentProfile().getProfileName();
        String cacheProfileName = sharedPreferences.getString(PREFERENCE_MUSIC_CACHE_PROFILE, null);
        long cacheSerialNumber = sharedPreferences.getLong(PREFERENCE_MUSIC_CACHE_SERIAL, 0);
        String cachedMusicCollectionSource = sharedPreferences.getString(PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE, null);
        sLogger.d("Current Cache details " + cacheProfileName + ", Source : " + cachedMusicCollectionSource + ", Serial : " + cacheSerialNumber);
        sLogger.d("Current Profile details " + profileName + ", Source : " + musicCollectionSource + ", Serial : " + serialNumber);
        if (!TextUtils.equals(profileName, cacheProfileName) || cacheSerialNumber != serialNumber || !TextUtils.equals(musicCollectionSource, cachedMusicCollectionSource)) {
            sLogger.d("Clearing the Music data cache");
            this.musicCollectionResponseMessageCache.clear();
            sharedPreferences.edit().putString(PREFERENCE_MUSIC_CACHE_PROFILE, profileName).putLong(PREFERENCE_MUSIC_CACHE_SERIAL, serialNumber).putString(PREFERENCE_MUSIC_CACHE_COLLECTION_SOURCE, musicCollectionSource).apply();
        }
    }

    public boolean hasMusicCapabilities() {
        return (this.musicCapabilities == null || this.musicCapabilities.capabilities == null || this.musicCapabilities.capabilities.size() == 0) ? false : true;
    }

    public MusicCapabilitiesResponse getMusicCapabilities() {
        return this.musicCapabilities;
    }

    public List<MusicCollectionType> getCollectionTypesForSource(MusicCollectionSource collectionSource) {
        return (List) this.musicCapabilityTypeMap.get(collectionSource);
    }

    @Subscribe
    public void onMusicCapabilitiesResponse(MusicCapabilitiesResponse musicCapabilitiesResponse) {
        sLogger.d("onMusicCapabilitiesResponse " + musicCapabilitiesResponse);
        this.musicCapabilities = musicCapabilitiesResponse;
        String musicCollectionSource = null;
        this.isMusicLibraryCachingEnabled = false;
        long serialNumber = -1;
        if (hasMusicCapabilities()) {
            this.musicCapabilityTypeMap = new HashMap();
            for (MusicCapability capability : this.musicCapabilities.capabilities) {
                this.musicCapabilityTypeMap.put(capability.collectionSource, capability.collectionTypes);
                if (!(this.isMusicLibraryCachingEnabled || capability.serial_number == null)) {
                    this.isMusicLibraryCachingEnabled = true;
                    musicCollectionSource = capability.collectionSource.name();
                    serialNumber = capability.serial_number.longValue();
                    sLogger.d("Enabling caching with Source " + musicCollectionSource + ", Serial :" + serialNumber);
                }
            }
        }
        if (this.isMusicLibraryCachingEnabled) {
            checkCache(musicCollectionSource, serialNumber);
        } else {
            sLogger.d("Cache is not enabled");
        }
    }

    @Subscribe
    public void onMediaKeyEventFromClient(MediaRemoteKeyEvent mediaRemoteKeyEvent) {
        this.bus.post(new RemoteEvent(mediaRemoteKeyEvent));
    }

    public String getMusicMenuPath() {
        return this.musicMenuPath;
    }

    public void setMusicMenuPath(String musicMenuPath) {
        sLogger.d("setMusicMenuPath: " + musicMenuPath);
        this.musicMenuPath = musicMenuPath;
    }

    public void softPause() {
        if (this.currentTrack.playbackState == MusicPlaybackState.PLAYBACK_PLAYING) {
            sLogger.d("Pausing music");
            this.pausedByUs = true;
            this.acceptingResumes = false;
            postKeyDownUp(MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY);
        }
    }

    public void acceptResumes() {
        this.acceptingResumes = true;
        sLogger.d("Now accepting music resume events");
    }

    @Subscribe
    public void onResumeMusicRequest(ResumeMusicRequest resumeMusicRequest) {
        softResume();
    }

    public void softResume() {
        if (this.pausedByUs && this.acceptingResumes) {
            postKeyDownUp(MediaRemoteKey.MEDIA_REMOTE_KEY_PLAY);
            this.pausedByUs = false;
            sLogger.d("Unpausing music (resuming)");
        }
    }

    public boolean isMusicLibraryCachingEnabled() {
        return this.isMusicLibraryCachingEnabled;
    }

    private void postKeyDownUp(final MediaRemoteKey key) {
        this.bus.post(new RemoteEvent(new MediaRemoteKeyEvent(key, com.navdy.service.library.events.input.KeyEvent.KEY_DOWN)));
        this.handler.postDelayed(new Runnable() {
            public void run() {
                MusicManager.this.bus.post(new RemoteEvent(new MediaRemoteKeyEvent(key, com.navdy.service.library.events.input.KeyEvent.KEY_UP)));
            }
        }, 100);
    }
}
