package com.navdy.hud.app.manager;

import android.content.SharedPreferences;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class UpdateReminderManager$$InjectAdapter extends Binding<UpdateReminderManager> implements MembersInjector<UpdateReminderManager> {
    private Binding<Bus> bus;
    private Binding<SharedPreferences> sharedPreferences;

    public UpdateReminderManager$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.manager.UpdateReminderManager", false, UpdateReminderManager.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", UpdateReminderManager.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", UpdateReminderManager.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
        injectMembersBindings.add(this.sharedPreferences);
    }

    public void injectMembers(UpdateReminderManager object) {
        object.bus = (Bus) this.bus.get();
        object.sharedPreferences = (SharedPreferences) this.sharedPreferences.get();
    }
}
