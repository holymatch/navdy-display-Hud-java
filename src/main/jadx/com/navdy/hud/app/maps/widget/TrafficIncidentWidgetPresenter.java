package com.navdy.hud.app.maps.widget;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.widget.TextView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.DisplayTrafficIncident;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.maps.here.HereMapsManager;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.hud.app.view.DashboardWidgetView;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TrafficIncidentWidgetPresenter extends DashboardWidgetPresenter {
    private static Handler handler = new Handler(Looper.getMainLooper());
    private static final Logger sLogger = new Logger("TrafficIncidentWidget");
    private Bus bus = RemoteDeviceManager.getInstance().getBus();
    private boolean registered;
    private TextView textView;

    public Drawable getDrawable() {
        return null;
    }

    protected void updateGauge() {
    }

    public String getWidgetIdentifier() {
        return SmartDashWidgetManager.TRAFFIC_INCIDENT_GAUGE_ID;
    }

    public String getWidgetName() {
        return null;
    }

    public void setView(DashboardWidgetView dashboardWidgetView, Bundle arguments) {
        if (dashboardWidgetView != null) {
            boolean isActive = false;
            if (arguments != null) {
                isActive = arguments.getBoolean(DashboardWidgetPresenter.EXTRA_IS_ACTIVE, false);
            }
            dashboardWidgetView.setContentView((int) R.layout.maps_traffic_incident_widget);
            this.textView = (TextView) dashboardWidgetView.findViewById(R.id.incidentInfo);
            setText();
            super.setView(dashboardWidgetView, arguments);
            if (!isActive) {
                sLogger.v("not active");
                unregister();
                return;
            } else if (this.registered) {
                sLogger.v("already register");
                return;
            } else {
                this.bus.register(this);
                this.registered = true;
                sLogger.v("register");
                return;
            }
        }
        unregister();
        this.textView = null;
        super.setView(dashboardWidgetView, arguments);
    }

    @Subscribe
    public void onDisplayTrafficIncident(final DisplayTrafficIncident event) {
        try {
            if (this.textView != null) {
                sLogger.v("DisplayTrafficIncident type:" + event.type + ", category:" + event.category + ", title:" + event.title + ", description:" + event.description + ", affectedStreet:" + event.affectedStreet + ", distanceToIncident:" + event.distanceToIncident + ", reported:" + event.reported + ", updated:" + event.updated + ", icon:" + event.icon);
                switch (event.type) {
                    case NORMAL:
                        this.textView.setTextAppearance(HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText("NORMAL");
                        return;
                    case INACTIVE:
                        this.textView.setTextAppearance(HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText("INACTIVE");
                        return;
                    case FAILED:
                        this.textView.setTextAppearance(HudApplication.getAppContext(), R.style.incident_1);
                        this.textView.setText("FAILED");
                        return;
                    default:
                        TaskManager.getInstance().execute(new Runnable() {
                            public void run() {
                                final SpannableStringBuilder builder = new SpannableStringBuilder();
                                Bitmap bitmap = null;
                                if (event.icon != null) {
                                    bitmap = event.icon.getBitmap(40, 40);
                                }
                                builder.append(event.type.name());
                                builder.setSpan(new StyleSpan(1), 0, builder.length(), 33);
                                builder.append(" ");
                                int start = builder.length();
                                if (bitmap != null) {
                                    ImageSpan imageSpan = new ImageSpan(HudApplication.getAppContext(), bitmap);
                                    builder.append("IMG");
                                    builder.setSpan(imageSpan, start, builder.length(), 33);
                                    builder.append(" ");
                                }
                                if (!TextUtils.isEmpty(event.title)) {
                                    builder.append(HereManeuverDisplayBuilder.OPEN_BRACKET);
                                    builder.append(event.title);
                                    builder.append(") ,");
                                }
                                if (!TextUtils.isEmpty(event.description)) {
                                    builder.append(event.description);
                                    builder.append(" , ");
                                }
                                if (!TextUtils.isEmpty(event.affectedStreet)) {
                                    builder.append("street=");
                                    builder.append(event.affectedStreet);
                                    builder.append(" , ");
                                }
                                if (event.distanceToIncident != -1) {
                                    builder.append("dist=");
                                    builder.append(String.valueOf(event.distanceToIncident));
                                    builder.append(" , ");
                                }
                                if (event.updated != null) {
                                    builder.append(String.valueOf(TimeUnit.MILLISECONDS.toMinutes(new Date().getTime() - event.updated.getTime())) + " minutes ago");
                                }
                                TrafficIncidentWidgetPresenter.handler.post(new Runnable() {
                                    public void run() {
                                        if (TrafficIncidentWidgetPresenter.this.textView != null) {
                                            TrafficIncidentWidgetPresenter.this.textView.setTextAppearance(HudApplication.getAppContext(), R.style.incident_2);
                                            TrafficIncidentWidgetPresenter.this.textView.setText(builder);
                                        }
                                    }
                                });
                            }
                        }, 1);
                        return;
                }
                sLogger.e(t);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private void setText() {
        this.textView.setTextAppearance(HudApplication.getAppContext(), R.style.incident_1);
        if (HereMapsManager.getInstance().isInitialized() && HereNavigationManager.getInstance().isTrafficUpdaterRunning()) {
            this.textView.setText("NORMAL");
        } else {
            this.textView.setText("INACTIVE");
        }
    }

    private void unregister() {
        if (this.registered) {
            sLogger.v("unregister");
            this.bus.unregister(this);
            this.registered = false;
        }
    }
}
