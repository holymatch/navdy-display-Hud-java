package com.navdy.hud.app.maps.here;

import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class HereRegionManager$$InjectAdapter extends Binding<HereRegionManager> implements MembersInjector<HereRegionManager> {
    private Binding<Bus> bus;

    public HereRegionManager$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.maps.here.HereRegionManager", false, HereRegionManager.class);
    }

    public void attach(Linker linker) {
        this.bus = linker.requestBinding("com.squareup.otto.Bus", HereRegionManager.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.bus);
    }

    public void injectMembers(HereRegionManager object) {
        object.bus = (Bus) this.bus.get();
    }
}
