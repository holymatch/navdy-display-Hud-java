package com.navdy.hud.app.maps.util;

import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnit;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.service.library.events.navigation.DistanceUnit;

public class DistanceConverter {

    public static class Distance {
        public DistanceUnit unit;
        public float value;

        void clear() {
            this.value = 0.0f;
            this.unit = null;
        }

        public String getFormattedExtendedDistanceUnit() {
            Resources resources = HudApplication.getAppContext().getResources();
            switch (this.unit) {
                case DISTANCE_METERS:
                    if (this.value == 1.0f) {
                        return resources.getString(R.string.unit_meters_ext_singular);
                    }
                    return resources.getString(R.string.unit_meters_ext);
                case DISTANCE_FEET:
                    if (this.value == 1.0f) {
                        return resources.getString(R.string.unit_feet_ext_singular);
                    }
                    return resources.getString(R.string.unit_feet_ext);
                case DISTANCE_KMS:
                    if (this.value == 1.0f) {
                        return resources.getString(R.string.unit_kilometers_ext_singular);
                    }
                    return resources.getString(R.string.unit_kilometers_ext);
                case DISTANCE_MILES:
                    if (this.value == 1.0f) {
                        return resources.getString(R.string.unit_miles_ext_singular);
                    }
                    return resources.getString(R.string.unit_miles_ext);
                default:
                    return "";
            }
        }
    }

    public static void convertToDistance(SpeedUnit unit, float value, Distance distance) {
        distance.clear();
        switch (unit) {
            case KILOMETERS_PER_HOUR:
                if (value >= 400.0f) {
                    distance.value = value / 1000.0f;
                    distance.value = (float) HereMapUtil.roundToN((double) distance.value, 10);
                    distance.unit = DistanceUnit.DISTANCE_KMS;
                    return;
                }
                distance.value = value;
                distance.unit = DistanceUnit.DISTANCE_METERS;
                return;
            default:
                if (value >= 160.934f) {
                    distance.value = value / 1609.34f;
                    distance.value = (float) HereMapUtil.roundToN((double) distance.value, 10);
                    distance.unit = DistanceUnit.DISTANCE_MILES;
                    return;
                }
                distance.value = (float) ((int) (3.28084f * value));
                distance.unit = DistanceUnit.DISTANCE_FEET;
                return;
        }
    }

    public static float convertToMeters(float value, DistanceUnit unit) {
        switch (unit) {
            case DISTANCE_FEET:
                return value / 3.28084f;
            case DISTANCE_KMS:
                return value * 1000.0f;
            case DISTANCE_MILES:
                return value * 1609.34f;
            default:
                return value;
        }
    }
}
