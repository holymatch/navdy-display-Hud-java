package com.navdy.hud.app.maps.here;

import android.os.Handler;
import android.text.TextUtils;
import com.here.android.mpa.common.RoadElement;
import com.here.android.mpa.guidance.NavigationManager;
import com.here.android.mpa.guidance.TrafficUpdater;
import com.here.android.mpa.guidance.TrafficUpdater.Error;
import com.here.android.mpa.guidance.TrafficUpdater.GetEventsListener;
import com.here.android.mpa.guidance.TrafficUpdater.Listener;
import com.here.android.mpa.guidance.TrafficUpdater.RequestInfo;
import com.here.android.mpa.guidance.TrafficUpdater.RequestState;
import com.here.android.mpa.mapping.TrafficEvent;
import com.here.android.mpa.mapping.TrafficEvent.Severity;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteElement;
import com.here.android.mpa.routing.RouteElements;
import com.navdy.hud.app.maps.MapEvents.LiveTrafficDismissEvent;
import com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent;
import com.navdy.hud.app.maps.MapEvents.LiveTrafficEvent.Type;
import com.navdy.hud.app.maps.MapEvents.TrafficDelayDismissEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficJamDismissEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficJamProgressEvent;
import com.navdy.hud.app.maps.here.HerePositionUpdateListener.MapMatchEvent;
import com.navdy.hud.app.maps.util.MapUtils;
import com.navdy.hud.app.util.CrashReporter;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HereTrafficUpdater {
    private static final int MAX_DISTANCE_NOTIFICATION_METERS = 5000;
    private static final int MIN_DISTANCE_NOTIFICATION_METERS = 200;
    private static final int MIN_JAM_DURATION_SECS = 180;
    private static final int ONE_MINUTE_SECS = 60;
    private static final long REFRESH_INTERVAL_MILLIS = 30000;
    public static final boolean TRAFFIC_DEBUG_ENABLED = false;
    private final Bus bus;
    private TrafficEvent currentEvent;
    private int currentRemainingTimeInJam;
    private RequestInfo currentRequestInfo;
    private Route currentRoute;
    private State currentState;
    private Runnable dismissEtaUpdate = new Runnable() {
        public void run() {
            HereTrafficUpdater.this.bus.post(new TrafficDelayDismissEvent());
        }
    };
    private final Handler handler;
    private final Logger logger;
    private final NavigationManager navigationManager;
    private GetEventsListener onGetEventsListener = new GetEventsListener() {
        public void onComplete(final List<TrafficEvent> list, Error error) {
            if (HereTrafficUpdater.this.currentRoute == null) {
                HereTrafficUpdater.this.logger.w(HereTrafficUpdater.this.tag + "onGetEventsListener triggered and currentRoute is null");
                return;
            }
            if (error != Error.NONE) {
                HereTrafficUpdater.this.logger.i(HereTrafficUpdater.this.tag + " error " + error.name() + " while getting TrafficEvents.");
            } else if (HereTrafficUpdater.this.currentState == State.ACTIVE || HereTrafficUpdater.this.currentState == State.TRACK_DISTANCE) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        HereTrafficUpdater.this.processEvents(HereTrafficUpdater.this.currentRoute, list);
                    }
                }, 2);
            }
            HereTrafficUpdater.this.refresh();
        }
    };
    private Listener onRequestListener = new Listener() {
        public void onStatusChanged(RequestState requestState) {
            if (HereTrafficUpdater.this.currentRoute == null) {
                HereTrafficUpdater.this.logger.w(HereTrafficUpdater.this.tag + "onRequestListener triggered and currentRoute is null");
            } else if (requestState == RequestState.DONE) {
                HereTrafficUpdater.this.trafficUpdater.getEvents(HereTrafficUpdater.this.currentRoute, HereTrafficUpdater.this.onGetEventsListener);
            } else {
                HereTrafficUpdater.this.logger.i(HereTrafficUpdater.this.tag + " error on completion of TrafficUpdater.request");
                HereTrafficUpdater.this.refresh();
            }
        }
    };
    private Runnable refreshRunnable = new Runnable() {
        public void run() {
            HereTrafficUpdater.this.startRequest();
        }
    };
    private final String tag;
    private final TrafficUpdater trafficUpdater;
    private final boolean verbose;

    private class DistanceTrackEvent {
        public final Route route;
        public final TrafficEvent trafficEvent;

        public DistanceTrackEvent(Route route, TrafficEvent trafficEvent) {
            this.route = route;
            this.trafficEvent = trafficEvent;
        }
    }

    private class JamTrackEvent {
        public final int remainingTimeInJam;
        public final Route route;
        public final TrafficEvent trafficEvent;

        public JamTrackEvent(Route route, TrafficEvent trafficEvent, int remainingTimeInJam) {
            this.route = route;
            this.trafficEvent = trafficEvent;
            this.remainingTimeInJam = remainingTimeInJam;
        }
    }

    public enum State {
        STOPPED,
        ACTIVE,
        TRACK_DISTANCE,
        TRACK_JAM
    }

    private class TrackDismissEvent {
        public final Route route;

        public TrackDismissEvent(Route route) {
            this.route = route;
        }
    }

    @Subscribe
    public void onTrackingUpdate(MapMatchEvent mapMatchEvent) {
        try {
            if (this.currentState == State.TRACK_DISTANCE) {
                onDistanceTrackingUpdate(mapMatchEvent);
            } else if (this.currentState == State.TRACK_JAM) {
                onJamTrackingUpdate();
            }
        } catch (Throwable t) {
            this.logger.e(t);
            CrashReporter.getInstance().reportNonFatalException(t);
        }
    }

    @Subscribe
    public void onDistanceTrackEvent(DistanceTrackEvent event) {
        if (isCurrentRoute(event.route)) {
            trackEvent(event.trafficEvent);
        }
    }

    @Subscribe
    public void onJamTrackEvent(JamTrackEvent event) {
        if (isCurrentRoute(event.route)) {
            this.currentEvent = event.trafficEvent;
            trackJam(event.remainingTimeInJam);
        }
    }

    @Subscribe
    public void onTrackDismissEvent(TrackDismissEvent event) {
        if (isCurrentRoute(event.route)) {
            changeState(State.ACTIVE);
        }
    }

    public HereTrafficUpdater(Logger logger, String tag, boolean verbose, NavigationManager navigationManager, Bus bus) {
        this.logger = logger;
        this.tag = tag;
        this.verbose = verbose;
        this.navigationManager = navigationManager;
        this.bus = bus;
        this.trafficUpdater = TrafficUpdater.getInstance();
        this.trafficUpdater.enableUpdate(true);
        this.handler = new Handler();
        this.currentState = State.STOPPED;
        this.bus.register(this);
    }

    public void activate(Route newRoute) {
        this.currentRoute = newRoute;
        changeState(State.ACTIVE);
    }

    public void stop() {
        changeState(State.STOPPED);
    }

    private void trackEvent(TrafficEvent newTrafficEvent) {
        this.currentEvent = newTrafficEvent;
        changeState(State.TRACK_DISTANCE);
    }

    private void trackJam(int newRemainingTimeInJam) {
        this.currentRemainingTimeInJam = newRemainingTimeInJam;
        changeState(State.TRACK_JAM);
    }

    private void changeState(State newState) {
        if (newState != this.currentState) {
            if (newState == State.STOPPED) {
                if (this.currentState == State.TRACK_DISTANCE) {
                    stopTrackingDistance();
                } else if (this.currentState == State.TRACK_JAM) {
                    stopTrackingJamTime();
                }
                stopUpdates();
            } else if (newState == State.ACTIVE) {
                if (this.currentState == State.STOPPED) {
                    startUpdates();
                } else if (this.currentState == State.TRACK_DISTANCE) {
                    stopTrackingDistance();
                } else if (this.currentState == State.TRACK_JAM) {
                    stopTrackingJamTime();
                }
            } else if (newState == State.TRACK_DISTANCE) {
                if (this.currentState == State.ACTIVE) {
                    startTrackingDistance();
                } else {
                    this.logger.w(this.tag + " invalid state change from " + this.currentState.name() + " to " + newState.name());
                    return;
                }
            } else if (newState == State.TRACK_JAM) {
                if (this.currentState == State.ACTIVE) {
                    startTrackingJamTime();
                } else if (this.currentState == State.TRACK_DISTANCE) {
                    stopTrackingDistance();
                    startTrackingJamTime();
                } else {
                    this.logger.w(this.tag + " invalid state change from " + this.currentState.name() + " to " + newState.name());
                    return;
                }
            }
            this.currentState = newState;
        }
    }

    private void processEvents(Route route, List<TrafficEvent> events) {
        TrafficEvent event;
        GenericUtil.checkNotOnMainThread();
        TrafficEvent firstEvent = null;
        TrafficEvent backupEvent = null;
        Map<String, TrafficEvent> roadElementIdToTrafficEventMap = new HashMap();
        for (TrafficEvent event2 : events) {
            List<RoadElement> affectedRoadElements = event2.getAffectedRoadElements();
            if (affectedRoadElements.size() > 0) {
                roadElementIdToTrafficEventMap.put(((RoadElement) affectedRoadElements.get(0)).getIdentifier().toString(), event2);
            }
        }
        int distanceToLocation = 0;
        for (RouteElement element : route.getRouteElementsFromLength((int) this.navigationManager.getElapsedDistance(), route.getLength()).getElements()) {
            RoadElement roadElement = element.getRoadElement();
            String roadId = roadElement.getIdentifier().toString();
            this.logger.v(this.tag + "route element id=" + roadId);
            if (roadElementIdToTrafficEventMap.containsKey(roadId)) {
                event2 = (TrafficEvent) roadElementIdToTrafficEventMap.get(roadId);
                boolean isVerySevereEvent = event2.getSeverity().getValue() > Severity.HIGH.getValue();
                boolean isSevereEvent = event2.getSeverity().getValue() > Severity.NORMAL.getValue();
                this.logger.v(this.tag + "event inside route: isVerySevereEvent=" + isVerySevereEvent + "; isSevereEvent=" + isSevereEvent + "; distanceToLocation=" + distanceToLocation);
                if (!isVerySevereEvent || distanceToLocation >= 200) {
                    if (isVerySevereEvent && distanceToLocation > 200) {
                        firstEvent = event2;
                        break;
                    } else if (isSevereEvent && backupEvent == null && distanceToLocation > 200) {
                        backupEvent = event2;
                    }
                } else {
                    int remainingTimeInJam = getRemainingTimeInJam(route, event2);
                    this.logger.v(this.tag + "triggering jam, remaining time=" + remainingTimeInJam + " sec");
                    if (remainingTimeInJam >= 180) {
                        this.bus.post(new JamTrackEvent(route, event2, remainingTimeInJam));
                        return;
                    }
                }
            }
            distanceToLocation = (int) (((double) distanceToLocation) + roadElement.getGeometryLength());
            if (distanceToLocation > 5000) {
                break;
            }
        }
        if (firstEvent != null) {
            this.bus.post(new DistanceTrackEvent(route, firstEvent));
        } else if (backupEvent != null) {
            this.bus.post(new DistanceTrackEvent(route, backupEvent));
        } else {
            this.bus.post(new TrackDismissEvent(route));
        }
    }

    private boolean isCurrentRoute(Route route) {
        return route.equals(this.currentRoute);
    }

    private int getRemainingTimeInJam(Route route, TrafficEvent event) {
        double remainingTime = 0.0d;
        List<RoadElement> roadElements = event.getAffectedRoadElements();
        RoadElement lastRoadElementInEvent = (RoadElement) roadElements.get(roadElements.size() - 1);
        RouteElements remainingRouteElementsInRoute = route.getRouteElementsFromLength((int) this.navigationManager.getElapsedDistance(), route.getLength());
        String lastRoadElementInEventId = lastRoadElementInEvent.getIdentifier().toString();
        boolean foundRoadElement = false;
        for (RouteElement routeElement : remainingRouteElementsInRoute.getElements()) {
            RoadElement roadElement = routeElement.getRoadElement();
            if (roadElement.getIdentifier().toString().equals(lastRoadElementInEventId)) {
                foundRoadElement = true;
                break;
            }
            remainingTime += roadElement.getGeometryLength() / ((double) roadElement.getDefaultSpeed());
        }
        if (foundRoadElement) {
            return (int) remainingTime;
        }
        return 0;
    }

    private LiveTrafficEvent transformToLiveTrafficEvent(TrafficEvent event) {
        LiveTrafficEvent.Severity severity;
        Type type = event.isFlow() ? Type.CONGESTION : Type.INCIDENT;
        if (event.getSeverity().getValue() > Severity.HIGH.getValue()) {
            severity = LiveTrafficEvent.Severity.VERY_HIGH;
        } else {
            severity = LiveTrafficEvent.Severity.HIGH;
        }
        return new LiveTrafficEvent(type, severity);
    }

    private void startRequest() {
        if (this.currentRoute == null) {
            this.logger.w(this.tag + "startRequest: currentRoute must not be null.");
            return;
        }
        this.currentRequestInfo = this.trafficUpdater.request(this.currentRoute, this.onRequestListener);
        switch (this.currentRequestInfo.getError()) {
            case NONE:
                return;
            case UNKNOWN:
            case OUT_OF_MEMORY:
            case REQUEST_FAILED:
                this.logger.i(this.tag + " recovering from error before TrafficUpdater.request");
                refresh();
                return;
            default:
                this.logger.i(this.tag + " got error before TrafficUpdater.request. Invalid/malformed request.");
                changeState(State.STOPPED);
                return;
        }
    }

    private void cancelRequest() {
        if (this.currentRequestInfo == null) {
            this.logger.w(this.tag + "cancelRequest: currentRequestInfo must not be null.");
            return;
        }
        if (this.currentRequestInfo.getError() == Error.NONE) {
            this.trafficUpdater.cancelRequest(this.currentRequestInfo.getRequestId());
        }
        this.currentRequestInfo = null;
    }

    private void refresh() {
        this.logger.v(this.tag + "posting refresh in 30 sec...");
        cancelRequest();
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.postDelayed(this.refreshRunnable, 30000);
    }

    private void onDistanceTrackingUpdate(MapMatchEvent mapMatchEvent) {
        if (this.currentEvent == null) {
            this.logger.w(this.tag + "onDistanceTrackingUpdate triggered while currentEvent is null.");
            return;
        }
        boolean isVerySevereEvent;
        if (this.currentEvent.getSeverity().getValue() >= Severity.VERY_HIGH.getValue()) {
            isVerySevereEvent = true;
        } else {
            isVerySevereEvent = false;
        }
        String currentRoadId = null;
        RoadElement roadElement = mapMatchEvent.mapMatch.getRoadElement();
        if (roadElement != null) {
            currentRoadId = roadElement.getIdentifier().toString();
        }
        String eventFirstRoadId = ((RoadElement) this.currentEvent.getAffectedRoadElements().get(0)).getIdentifier().toString();
        if (this.verbose) {
            this.logger.v(this.tag + "Position update: currentRoadId=" + currentRoadId + "; eventFirstRoadId=" + eventFirstRoadId);
        }
        if (TextUtils.equals(eventFirstRoadId, currentRoadId)) {
            if (isVerySevereEvent) {
                int remainingTimeInJam = getRemainingTimeInJam(this.currentRoute, this.currentEvent);
                this.logger.v(this.tag + "triggering jam, remaining time=" + remainingTimeInJam + " sec");
                if (remainingTimeInJam >= 180) {
                    this.bus.post(new JamTrackEvent(this.currentRoute, this.currentEvent, remainingTimeInJam));
                    return;
                }
            }
            this.currentEvent = null;
            changeState(State.ACTIVE);
        }
    }

    private void onJamTrackingUpdate() {
        if (this.currentRoute == null) {
            this.logger.w(this.tag + "onJamTrackingUpdate triggered while currentRoute is null.");
        } else if (this.currentEvent == null) {
            this.logger.w(this.tag + "onJamTrackingUpdate triggered while currentEvent is null.");
        } else if (this.currentRemainingTimeInJam == 0) {
            this.logger.w(this.tag + "onJamTrackingUpdate triggered while currentRemainingTimeInJam is zero.");
        } else {
            int remainingTimeInJam = getRemainingTimeInJam(this.currentRoute, this.currentEvent);
            if (this.verbose) {
                this.logger.v(this.tag + "Jam time update: remainingTimeInJam=" + remainingTimeInJam);
            }
            if (remainingTimeInJam > 0) {
                int currentMinuteCeiling = MapUtils.getMinuteCeiling(this.currentRemainingTimeInJam);
                int minuteCeiling = MapUtils.getMinuteCeiling(remainingTimeInJam);
                if (Math.abs(currentMinuteCeiling - minuteCeiling) >= 60) {
                    this.currentRemainingTimeInJam = remainingTimeInJam;
                    this.bus.post(new TrafficJamProgressEvent(minuteCeiling));
                    return;
                }
                return;
            }
            this.currentRemainingTimeInJam = 0;
            changeState(State.ACTIVE);
        }
    }

    private void startUpdates() {
        if (this.currentRoute == null) {
            this.logger.e(this.tag + " route must not be null.");
        }
    }

    private void stopUpdates() {
        cancelRequest();
        this.currentEvent = null;
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.removeCallbacks(this.dismissEtaUpdate);
        this.currentRoute = null;
        this.bus.post(new TrafficJamDismissEvent());
        this.bus.post(new TrafficDelayDismissEvent());
    }

    private void startTrackingDistance() {
        this.bus.post(transformToLiveTrafficEvent(this.currentEvent));
    }

    private void stopTrackingDistance() {
        this.bus.post(new LiveTrafficDismissEvent());
    }

    private void startTrackingJamTime() {
        this.bus.post(new TrafficJamProgressEvent(MapUtils.getMinuteCeiling(this.currentRemainingTimeInJam)));
    }

    private void stopTrackingJamTime() {
        this.bus.post(new TrafficJamDismissEvent());
    }
}
