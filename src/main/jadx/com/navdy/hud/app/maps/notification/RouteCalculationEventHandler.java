package com.navdy.hud.app.maps.notification;

import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.hud.app.framework.destinations.Destination;
import com.navdy.hud.app.framework.destinations.Destination.DestinationType;
import com.navdy.hud.app.framework.destinations.Destination.FavoriteDestinationType;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import com.navdy.hud.app.framework.notifications.NotificationId;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.ManeuverEvent;
import com.navdy.hud.app.maps.MapEvents.NavigationModeChange;
import com.navdy.hud.app.maps.MapEvents.RouteCalculationEvent;
import com.navdy.hud.app.maps.MapEvents.RouteCalculationState;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereRouteManager;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.ui.component.mainmenu.PlacesMenu;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelType;
import com.navdy.hud.app.ui.framework.INotificationAnimationListener;
import com.navdy.hud.app.ui.framework.UIStateManager.Mode;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.navigation.NavigationRouteCancelRequest;
import com.navdy.service.library.events.navigation.NavigationRouteResponse.Builder;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.navigation.NavigationSessionRequest;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.concurrent.TimeUnit;

public class RouteCalculationEventHandler {
    private static final int MIN_ROUTE_CALC_DISPLAY_TIME = ((int) TimeUnit.SECONDS.toMillis(2));
    private static final int ROUTE_CALC_DELAY_TIME = ((int) TimeUnit.SECONDS.toMillis(1));
    private static final Logger logger = new Logger(RouteCalculationEventHandler.class);
    private INotificationAnimationListener animationListener = new INotificationAnimationListener() {
        public void onStart(String id, NotificationType type, Mode mode) {
            if (mode == Mode.COLLAPSE && NotificationId.ROUTE_CALC_NOTIFICATION_ID.equals(id)) {
                RouteCalculationNotification notif = (RouteCalculationNotification) NotificationManager.getInstance().getNotification(NotificationId.ROUTE_CALC_NOTIFICATION_ID);
                if (notif != null) {
                    RouteCalculationEventHandler.logger.v("hideStartTrip");
                    notif.hideStartTrip();
                }
            }
        }

        public void onStop(String id, NotificationType type, Mode mode) {
            if (mode == Mode.EXPAND && NotificationId.ROUTE_CALC_NOTIFICATION_ID.equals(id)) {
                RouteCalculationEventHandler.logger.v("showStartTrip: check");
                RouteCalculationNotification notif = (RouteCalculationNotification) NotificationManager.getInstance().getNotification(NotificationId.ROUTE_CALC_NOTIFICATION_ID);
                if (notif != null && notif.isStarting()) {
                    RouteCalculationEventHandler.logger.v("showStartTrip");
                    notif.showStartTrip();
                }
            }
        }
    };
    private Bus bus;
    private RouteCalculationEvent currentRouteCalcEvent;
    private Handler handler = new Handler(Looper.getMainLooper());
    private long routeCalcStartTime;
    private RouteCalculationEvent showRoutesCalcEvent;
    private Runnable startRouteRunnable = new Runnable() {
        public void run() {
            RouteCalculationEventHandler.this.startRoute();
        }
    };
    private final String startTrip;

    public RouteCalculationEventHandler(Bus bus) {
        logger.v("ctor");
        Resources resources = HudApplication.getAppContext().getResources();
        this.bus = bus;
        this.startTrip = resources.getString(R.string.start_trip);
        RemoteDeviceManager.getInstance().getUiStateManager().addNotificationAnimationListener(this.animationListener);
        bus.register(this);
    }

    @Subscribe
    public void onFirstManeuver(ManeuverEvent event) {
        logger.v("onFirstManeuver");
    }

    @Subscribe
    public void onNavigationModeChanged(NavigationModeChange event) {
        boolean switching = HereNavigationManager.getInstance().isSwitchingToNewRoute();
        logger.v("onNavigationModeChanged event[" + event.navigationMode + "] switching=" + switching);
        if (event.navigationMode == NavigationMode.MAP && !switching) {
            this.showRoutesCalcEvent = null;
            dismissNotification();
        }
    }

    @Subscribe
    public void onRouteCalculation(RouteCalculationEvent event) {
        logger.v("onRouteCalculation event[" + event.state + "]");
        NotificationManager notificationManager = NotificationManager.getInstance();
        RouteCalculationNotification notif;
        switch (event.state) {
            case STARTED:
            case FINDING_NAV_COORDINATES:
                String label;
                String destinationLabel;
                Destination transformedDestination;
                BaseScreen screen = RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
                if (screen != null && screen.getScreen() == Screen.SCREEN_DESTINATION_PICKER) {
                    logger.v("onRouteCalculation: destination picker on");
                    notificationManager.enableNotifications(true);
                }
                this.handler.removeCallbacks(this.startRouteRunnable);
                this.showRoutesCalcEvent = null;
                this.currentRouteCalcEvent = event;
                this.routeCalcStartTime = SystemClock.elapsedRealtime();
                DestinationsManager.getInstance().clearSuggestedDestination();
                HomeScreenView homeScreenView = RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
                if (homeScreenView != null) {
                    homeScreenView.setShowCollapsedNotification(false);
                }
                notif = (RouteCalculationNotification) notificationManager.getNotification(NotificationId.ROUTE_CALC_NOTIFICATION_ID);
                if (notif == null) {
                    notif = new RouteCalculationNotification(this, this.bus);
                }
                String destinationAddress = null;
                String destinationDistance = null;
                if (event.state == RouteCalculationState.STARTED) {
                    if (!TextUtils.isEmpty(event.request.label)) {
                        label = event.request.label;
                        destinationLabel = label;
                        destinationAddress = event.request.streetAddress;
                    } else if (TextUtils.isEmpty(event.request.streetAddress)) {
                        label = "";
                        destinationLabel = label;
                    } else {
                        label = event.request.streetAddress;
                        destinationLabel = label;
                    }
                    transformedDestination = DestinationsManager.getInstance().transformToInternalDestination(event.request.requestDestination);
                } else {
                    if (event.lookupDestination != null && !TextUtils.isEmpty(event.lookupDestination.destinationTitle)) {
                        label = event.lookupDestination.destinationTitle;
                        destinationLabel = label;
                        destinationAddress = event.lookupDestination.fullAddress;
                    } else if (event.lookupDestination == null || TextUtils.isEmpty(event.lookupDestination.fullAddress)) {
                        label = "";
                        destinationLabel = label;
                    } else {
                        label = event.lookupDestination.fullAddress;
                        destinationLabel = label;
                    }
                    transformedDestination = event.lookupDestination;
                }
                String initials = null;
                Model model = PlacesMenu.getPlaceModel(transformedDestination, 0, null, null);
                if (model.type == ModelType.TWO_ICONS) {
                    model.iconSelectedColor = 0;
                } else if (transformedDestination == null) {
                    model.iconSelectedColor = RouteCalculationNotification.notifBkColor;
                } else if (!(transformedDestination == null || transformedDestination.destinationType == null || transformedDestination.destinationType != DestinationType.DEFAULT || transformedDestination.favoriteDestinationType == null || transformedDestination.favoriteDestinationType != FavoriteDestinationType.FAVORITE_NONE)) {
                    model.icon = R.drawable.icon_mm_places_2;
                    model.iconSelectedColor = RouteCalculationNotification.notifBkColor;
                }
                int icon = model.icon;
                int color = model.iconSelectedColor;
                if (transformedDestination != null) {
                    initials = DestinationsManager.getInitials(transformedDestination.destinationTitle, transformedDestination.favoriteDestinationType);
                    if (!(transformedDestination.destinationIcon == 0 || transformedDestination.destinationIconBkColor == 0)) {
                        icon = transformedDestination.destinationIcon;
                        color = transformedDestination.destinationIconBkColor;
                    }
                    destinationDistance = transformedDestination.distanceStr;
                }
                Resources res = HudApplication.getAppContext().getResources();
                String tts = null;
                switch (event.routeOptions.getRouteType()) {
                    case SHORTEST:
                        tts = res.getString(R.string.finding_route, new Object[]{RouteCalculationNotification.shortestRoute, label});
                        break;
                    case FASTEST:
                        tts = res.getString(R.string.finding_route, new Object[]{RouteCalculationNotification.fastestRoute, label});
                        break;
                }
                com.navdy.service.library.events.destination.Destination destination = null;
                if (event.lookupDestination != null) {
                    destination = DestinationsManager.getInstance().transformToProtoDestination(event.lookupDestination);
                }
                String requestRouteId = null;
                if (event.request != null) {
                    requestRouteId = event.request.requestId;
                }
                int notifColor = 0;
                if (color == 0 && model.iconFluctuatorColor != -1) {
                    notifColor = model.iconFluctuatorColor;
                }
                notif.showRouteSearch(this.startTrip, label, icon, color, notifColor, initials, tts, destination, requestRouteId, destinationLabel, destinationAddress, destinationDistance);
                notificationManager.addNotification(notif);
                return;
            case STOPPED:
                if (!event.stopped && this.currentRouteCalcEvent != null && this.currentRouteCalcEvent.response != null) {
                    event.stopped = true;
                    if (this.currentRouteCalcEvent.response.status == RequestStatus.REQUEST_SUCCESS) {
                        RouteCalculationNotification routeCNotif = (RouteCalculationNotification) NotificationManager.getInstance().getNotification(NotificationId.ROUTE_CALC_NOTIFICATION_ID);
                        if (routeCNotif != null) {
                            logger.v("hideStartTrip");
                            routeCNotif.hideStartTrip();
                        }
                        long diff = SystemClock.elapsedRealtime() - this.routeCalcStartTime;
                        if (diff < ((long) MIN_ROUTE_CALC_DISPLAY_TIME)) {
                            logger.v("route calc too fast[" + diff + "] wait");
                            this.handler.postDelayed(this.startRouteRunnable, (long) ROUTE_CALC_DELAY_TIME);
                            return;
                        }
                        logger.v("route calc took[" + diff + "]");
                        startRoute();
                        return;
                    }
                    logger.e("routing calculation not successful:" + this.currentRouteCalcEvent.response.status);
                    if (this.currentRouteCalcEvent.response.status == RequestStatus.REQUEST_CANCELLED) {
                        logger.v("request cancelled");
                        this.currentRouteCalcEvent = null;
                        dismissNotification();
                        return;
                    }
                    logger.v("show error");
                    notif = (RouteCalculationNotification) notificationManager.getNotification(NotificationId.ROUTE_CALC_NOTIFICATION_ID);
                    if (notif == null) {
                        notif = new RouteCalculationNotification(this, this.bus);
                    }
                    notif.showError();
                    notificationManager.addNotification(notif);
                    return;
                }
                return;
            case ABORT_NAVIGATION:
                logger.v("abort-nav routeCalc event[ABORT_NAVIGATION] " + event.pendingNavigationRequestId);
                if (this.currentRouteCalcEvent == null || this.currentRouteCalcEvent.request == null) {
                    logger.v("abort-nav:no current route calc event");
                    return;
                } else if (TextUtils.equals(this.currentRouteCalcEvent.request.requestId, event.pendingNavigationRequestId)) {
                    logger.v("abort-nav: found current route id, cancel");
                    this.currentRouteCalcEvent = null;
                    if (event.abortOriginDisplay) {
                        this.bus.post(new RemoteEvent(new NavigationRouteCancelRequest(event.pendingNavigationRequestId)));
                        dismissNotification();
                        return;
                    }
                    logger.v("abort-nav: sent navrouteresponse cancel [" + event.pendingNavigationRequestId + "]");
                    this.bus.post(new RemoteEvent(new Builder().requestId(event.pendingNavigationRequestId).status(RequestStatus.REQUEST_CANCELLED).destination(new Coordinate.Builder().latitude(Double.valueOf(0.0d)).longitude(Double.valueOf(0.0d)).build()).build()));
                    HereRouteManager.clearActiveRouteCalc(event.pendingNavigationRequestId);
                    dismissNotification();
                    return;
                } else {
                    logger.v("abort-nav:does not match current request:" + this.currentRouteCalcEvent.request.requestId);
                    return;
                }
            default:
                return;
        }
    }

    private void navigateToRoute(NavigationRouteResult result) {
        NavigationSessionRequest navigationSessionRequest = new NavigationSessionRequest.Builder().newState(NavigationSessionState.NAV_SESSION_STARTED).label(this.currentRouteCalcEvent.response.label).routeId(result.routeId).simulationSpeed(Integer.valueOf(0)).originDisplay(Boolean.valueOf(true)).build();
        this.bus.post(navigationSessionRequest);
        this.bus.post(new RemoteEvent(navigationSessionRequest));
    }

    public RouteCalculationEvent getCurrentRouteCalcEvent() {
        return this.currentRouteCalcEvent;
    }

    public void clearCurrentRouteCalcEvent() {
        this.currentRouteCalcEvent = null;
        this.handler.removeCallbacks(this.startRouteRunnable);
    }

    private void dismissNotification() {
        NotificationManager.getInstance().removeNotification(NotificationId.ROUTE_CALC_NOTIFICATION_ID);
    }

    public boolean hasMoreRoutes() {
        return this.showRoutesCalcEvent != null;
    }

    private void startRoute() {
        if (this.currentRouteCalcEvent == null || this.currentRouteCalcEvent.request == null || this.currentRouteCalcEvent.response == null || this.currentRouteCalcEvent.response.results == null) {
            logger.v("invalid state");
            dismissNotification();
            return;
        }
        logger.v("put user on route");
        navigateToRoute((NavigationRouteResult) this.currentRouteCalcEvent.response.results.get(0));
        int routes = this.currentRouteCalcEvent.response.results.size();
        if (routes > 1) {
            logger.v("more route available:" + routes);
            this.showRoutesCalcEvent = this.currentRouteCalcEvent;
            showMoreRoutes();
            return;
        }
        logger.v("more route NOT available:" + routes);
        this.currentRouteCalcEvent = null;
        dismissNotification();
    }

    private void showMoreRoutes() {
        logger.v("showMoreRoutes");
        if (this.showRoutesCalcEvent != null) {
            NotificationManager notificationManager = NotificationManager.getInstance();
            RouteCalculationNotification notif = (RouteCalculationNotification) notificationManager.getNotification(NotificationId.ROUTE_CALC_NOTIFICATION_ID);
            if (notif == null) {
                notif = new RouteCalculationNotification(this, this.bus);
            }
            notif.showRoutes(this.showRoutesCalcEvent);
            this.showRoutesCalcEvent = null;
            logger.v("showMoreRoutes: posted notif");
            notificationManager.addNotification(notif);
        }
    }
}
