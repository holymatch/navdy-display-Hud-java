package com.navdy.hud.app.storage.db.table;

import android.database.sqlite.SQLiteDatabase;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.storage.db.DatabaseUtil;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.MusicDataUtils;

public class FavoriteContactsTable {
    public static final String DEFAULT_IMAGE_INDEX = "def_image";
    public static final String DRIVER_ID = "device_id";
    public static final String NAME = "name";
    public static final String NUMBER = "number";
    public static final String NUMBER_NUMERIC = "number_numeric";
    public static final String NUMBER_TYPE = "number_type";
    public static final String TABLE_NAME = "fav_contacts";
    private static final Logger sLogger = new Logger(FavoriteContactsTable.class);

    public static void createTable(SQLiteDatabase db) {
        createTable_1(db);
    }

    public static void createTable_1(SQLiteDatabase db) {
        String tableName = TABLE_NAME;
        db.execSQL("CREATE TABLE IF NOT EXISTS " + tableName + " (" + "device_id" + " TEXT NOT NULL," + "name" + " TEXT," + "number" + " TEXT NOT NULL," + "number_type" + " INTEGER NOT NULL," + "def_image" + " INTEGER," + "number_numeric" + " INTEGER" + ");");
        sLogger.v("createdTable:" + tableName);
        String indexName = tableName + MusicDataUtils.ALTERNATE_SEPARATOR + "device_id";
        db.execSQL("CREATE INDEX IF NOT EXISTS " + indexName + " ON " + tableName + HereManeuverDisplayBuilder.OPEN_BRACKET + "device_id" + ");");
        sLogger.v("createdIndex:" + indexName);
        indexName = tableName + MusicDataUtils.ALTERNATE_SEPARATOR + "number";
        db.execSQL("CREATE INDEX IF NOT EXISTS " + indexName + " ON " + tableName + HereManeuverDisplayBuilder.OPEN_BRACKET + "number" + ");");
        sLogger.v("createdIndex:" + indexName);
        indexName = tableName + MusicDataUtils.ALTERNATE_SEPARATOR + "number_type";
        db.execSQL("CREATE INDEX IF NOT EXISTS " + indexName + " ON " + tableName + HereManeuverDisplayBuilder.OPEN_BRACKET + "number_type" + ");");
        sLogger.v("createdIndex:" + indexName);
        indexName = tableName + MusicDataUtils.ALTERNATE_SEPARATOR + "def_image";
        db.execSQL("CREATE INDEX IF NOT EXISTS " + indexName + " ON " + tableName + HereManeuverDisplayBuilder.OPEN_BRACKET + "def_image" + ");");
        sLogger.v("createdIndex:" + indexName);
        indexName = tableName + MusicDataUtils.ALTERNATE_SEPARATOR + "number_numeric";
        db.execSQL("CREATE INDEX IF NOT EXISTS " + indexName + " ON " + tableName + HereManeuverDisplayBuilder.OPEN_BRACKET + "number_numeric" + ");");
        sLogger.v("createdIndex:" + indexName);
    }

    public static void upgradeDatabase_1(SQLiteDatabase db) {
        DatabaseUtil.dropTable(db, TABLE_NAME, sLogger);
        createTable_1(db);
    }
}
