package com.navdy.hud.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

public class ObservableScrollView extends ScrollView {
    private View child;
    private IScrollListener listener;

    public interface IScrollListener {
        void onBottom();

        void onScroll(int i, int i2, int i3, int i4);

        void onTop();
    }

    public ObservableScrollView(Context context) {
        super(context);
    }

    public ObservableScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ObservableScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setScrollListener(IScrollListener listener) {
        this.listener = listener;
    }

    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        if (this.child == null) {
            this.child = getChildAt(0);
        }
        if (this.child == null) {
            super.onScrollChanged(l, t, oldl, oldt);
        } else if (t <= 0) {
            if (this.listener != null) {
                this.listener.onTop();
            }
            super.onScrollChanged(l, t, oldl, oldt);
        } else if (this.child.getBottom() - (getHeight() + getScrollY()) == 0) {
            if (this.listener != null) {
                this.listener.onBottom();
            }
            super.onScrollChanged(l, t, oldl, oldt);
        } else {
            if (this.listener != null) {
                this.listener.onScroll(l, t, oldl, oldt);
            }
            super.onScrollChanged(l, t, oldl, oldt);
        }
    }
}
