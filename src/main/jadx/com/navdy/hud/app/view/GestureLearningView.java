package com.navdy.hud.app.view;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.gesture.GestureServiceConnector.GestureDirection;
import com.navdy.hud.app.gesture.GestureServiceConnector.GestureProgress;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.screen.GestureLearningScreen.Presenter;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.hud.app.ui.component.ChoiceLayout.Choice;
import com.navdy.hud.app.ui.component.ChoiceLayout.IListener;
import com.navdy.hud.app.ui.component.ChoiceLayout.Mode;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.events.input.GestureEvent;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import mortar.Mortar;

public class GestureLearningView extends RelativeLayout implements IListener, IInputHandler {
    public static final int MOVE_IN_DURATION = 600;
    public static final int MOVE_OUT_DURATION = 250;
    private static final int PROGRESS_INDICATOR_HIDE_INTERVAL = 100;
    private static final int SCROLL_DURATION = 1000;
    private static final int SCROLL_INTERVAL = 4000;
    private static final int SENSOR_BLOCKED_MESSAGE_DELAY = 30000;
    private static final String SMOOTHING_COEFF_KEY = "persist.sys.gesture_smooth";
    public static final int TAG_CAPTURE = 2;
    public static final int TAG_DONE = 0;
    public static final int TAG_TIPS = 1;
    private String[] TIPS;
    @Inject
    Bus mBus;
    @InjectView(R.id.center_image)
    ImageView mCenterImage;
    @InjectView(R.id.choiceLayout)
    ChoiceLayout mChoiceLayout;
    private int mCurrentSmallTipIndex;
    private float mGestureIndicatorHalfWidth;
    private float mGestureIndicatorProgressSpan;
    @InjectView(R.id.gesture_progress_indicator)
    View mGestureProgressIndicator;
    private float mHandIconMargin;
    private Handler mHandler;
    private AnimatorSet mLeftGestureAnimation;
    @InjectView(R.id.lyt_left_swipe)
    LinearLayout mLeftSwipeLayout;
    private final float mLowPassCoeff;
    private float mNeutralX;
    @Inject
    Presenter mPresenter;
    private Runnable mProgressIndicatorRunnable;
    private AnimatorSet mRightGestureAnimation;
    @InjectView(R.id.lyt_right_swipe)
    LinearLayout mRightSwipeLayout;
    private ValueAnimator mScrollAnimator;
    private Runnable mShowSensorBlockedMessageRunnable;
    private float mSmoothedProgress;
    private boolean mStopAnimation;
    @InjectView(R.id.tips_scroller)
    LinearLayout mTipsScroller;
    int mTipsScrollerHeight;
    private Runnable mTipsScrollerRunnable;
    @InjectView(R.id.tipsText1)
    TextView mTipsTextView1;
    @InjectView(R.id.tipsText2)
    TextView mTipsTextView2;

    public GestureLearningView(Context context) {
        this(context, null);
    }

    public GestureLearningView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GestureLearningView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mCurrentSmallTipIndex = 0;
        this.mStopAnimation = false;
        this.mLowPassCoeff = Float.parseFloat(SystemProperties.get(SMOOTHING_COEFF_KEY, "1.0"));
        this.mSmoothedProgress = 0.0f;
        if (!isInEditMode()) {
            Mortar.inject(context, this);
        }
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        this.mChoiceLayout.setHighlightPersistent(true);
        Resources resources = getResources();
        this.mTipsScrollerHeight = resources.getDimensionPixelSize(R.dimen.gesture_learning_tips_text_scroll_height);
        this.mHandIconMargin = resources.getDimension(R.dimen.gesture_icon_margin_from_center_image);
        this.mLeftGestureAnimation = prepareGestureAnimation(false, this.mLeftSwipeLayout);
        this.mRightGestureAnimation = prepareGestureAnimation(true, this.mRightSwipeLayout);
        this.TIPS = resources.getStringArray(R.array.gesture_small_tips);
        this.mHandler = new Handler();
        this.mScrollAnimator = new ValueAnimator();
        this.mScrollAnimator.setIntValues(new int[]{0, -this.mTipsScrollerHeight});
        this.mScrollAnimator.setDuration(1000);
        getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                GestureLearningView.this.mNeutralX = (float) (GestureLearningView.this.getWidth() / 2);
                GestureLearningView.this.mGestureProgressIndicator.setY((GestureLearningView.this.mCenterImage.getY() + ((float) (GestureLearningView.this.mCenterImage.getHeight() / 2))) - ((float) (GestureLearningView.this.mGestureProgressIndicator.getHeight() / 2)));
                GestureLearningView.this.mGestureIndicatorProgressSpan = (GestureLearningView.this.mHandIconMargin + ((float) (GestureLearningView.this.mCenterImage.getWidth() / 2))) - ((float) (GestureLearningView.this.mGestureProgressIndicator.getWidth() / 2));
                GestureLearningView.this.mGestureIndicatorHalfWidth = (float) (GestureLearningView.this.mGestureProgressIndicator.getWidth() / 2);
                GestureLearningView.this.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
        this.mScrollAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                MarginLayoutParams marginLayoutParams = (MarginLayoutParams) GestureLearningView.this.mTipsScroller.getLayoutParams();
                marginLayoutParams.topMargin = ((Integer) animation.getAnimatedValue()).intValue();
                GestureLearningView.this.mTipsScroller.setLayoutParams(marginLayoutParams);
            }
        });
        this.mScrollAnimator.addListener(new AnimatorListener() {
            public void onAnimationStart(Animator animation) {
            }

            public void onAnimationEnd(Animator animation) {
                GestureLearningView.this.mCurrentSmallTipIndex = (GestureLearningView.this.mCurrentSmallTipIndex + 1) % GestureLearningView.this.TIPS.length;
                String tipText1 = GestureLearningView.this.TIPS[GestureLearningView.this.mCurrentSmallTipIndex];
                String tipText2 = GestureLearningView.this.TIPS[(GestureLearningView.this.mCurrentSmallTipIndex + 1) % GestureLearningView.this.TIPS.length];
                GestureLearningView.this.mTipsTextView1.setText(tipText1);
                MarginLayoutParams marginLayoutParams = (MarginLayoutParams) GestureLearningView.this.mTipsScroller.getLayoutParams();
                marginLayoutParams.topMargin = 0;
                GestureLearningView.this.mTipsScroller.setLayoutParams(marginLayoutParams);
                GestureLearningView.this.mTipsTextView2.setText(tipText2);
                if (!GestureLearningView.this.mStopAnimation) {
                    GestureLearningView.this.mHandler.postDelayed(GestureLearningView.this.mTipsScrollerRunnable, 4000);
                }
            }

            public void onAnimationCancel(Animator animation) {
            }

            public void onAnimationRepeat(Animator animation) {
            }
        });
        String tipText1 = this.TIPS[this.mCurrentSmallTipIndex];
        String tipText2 = this.TIPS[(this.mCurrentSmallTipIndex + 1) % this.TIPS.length];
        this.mTipsTextView1.setText(tipText1);
        this.mTipsTextView2.setText(tipText2);
        this.mTipsScrollerRunnable = new Runnable() {
            public void run() {
                GestureLearningView.this.mScrollAnimator.start();
            }
        };
        this.mProgressIndicatorRunnable = new Runnable() {
            public void run() {
                GestureLearningView.this.mGestureProgressIndicator.setVisibility(4);
            }
        };
        this.mShowSensorBlockedMessageRunnable = new Runnable() {
            public void run() {
                GestureLearningView.this.mPresenter.showCameraSensorBlocked();
            }
        };
        this.mHandler.postDelayed(this.mTipsScrollerRunnable, 4000);
        List<Choice> list = new ArrayList();
        list.add(new Choice(getContext().getString(R.string.done), 0));
        list.add(new Choice(getContext().getString(R.string.tips), 1));
        this.mChoiceLayout.setChoices(Mode.LABEL, list, 0, this);
        if (!isInEditMode()) {
            this.mBus.register(this);
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mBus.unregister(this);
        this.mStopAnimation = true;
        this.mScrollAnimator.cancel();
        this.mHandler.removeCallbacks(this.mTipsScrollerRunnable);
        this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
    }

    public boolean onGesture(GestureEvent event) {
        this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
        switch (event.gesture) {
            case GESTURE_SWIPE_LEFT:
                this.mLeftGestureAnimation.start();
                break;
            case GESTURE_SWIPE_RIGHT:
                this.mRightGestureAnimation.start();
                break;
        }
        return true;
    }

    public boolean onKey(CustomKeyEvent event) {
        switch (event) {
            case LEFT:
                this.mChoiceLayout.moveSelectionLeft();
                break;
            case RIGHT:
                this.mChoiceLayout.moveSelectionRight();
                break;
            case SELECT:
                this.mChoiceLayout.executeSelectedItem(true);
                break;
        }
        return false;
    }

    public IInputHandler nextHandler() {
        return null;
    }

    public void executeItem(int pos, int id) {
        switch (id) {
            case 0:
                this.mPresenter.finish();
                return;
            case 1:
                this.mPresenter.showTips();
                return;
            case 2:
                this.mPresenter.showCaptureView();
                return;
            default:
                return;
        }
    }

    public void itemSelected(int pos, int id) {
    }

    private AnimatorSet prepareGestureAnimation(final boolean rightSide, final ViewGroup view) {
        TextView directionText;
        ImageView overLay;
        if (rightSide) {
            directionText = (TextView) view.findViewById(R.id.txt_right);
            overLay = (ImageView) view.findViewById(R.id.img_right_hand_overlay);
        } else {
            directionText = (TextView) view.findViewById(R.id.txt_left);
            overLay = (ImageView) view.findViewById(R.id.img_left_hand_overlay);
        }
        AnimatorSet animatorSet = new AnimatorSet();
        ValueAnimator moveOutAnimator = new ValueAnimator();
        moveOutAnimator.setDuration(250);
        int marginDistance = getResources().getDimensionPixelSize(R.dimen.gesture_icon_animate_distance);
        moveOutAnimator.setIntValues(new int[]{(int) this.mHandIconMargin, ((int) this.mHandIconMargin) + marginDistance});
        moveOutAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                MarginLayoutParams marginLayoutParams = (MarginLayoutParams) view.getLayoutParams();
                int animatedValue = ((Integer) animation.getAnimatedValue()).intValue();
                if (rightSide) {
                    marginLayoutParams.leftMargin = animatedValue;
                } else {
                    marginLayoutParams.rightMargin = animatedValue;
                }
                view.setLayoutParams(marginLayoutParams);
            }
        });
        ValueAnimator moveRightAnimator = new ValueAnimator();
        moveRightAnimator.setDuration(600);
        moveRightAnimator.setIntValues(new int[]{((int) this.mHandIconMargin) + marginDistance, (int) this.mHandIconMargin});
        moveRightAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                MarginLayoutParams marginLayoutParams = (MarginLayoutParams) view.getLayoutParams();
                int animatedValue = ((Integer) animation.getAnimatedValue()).intValue();
                if (rightSide) {
                    marginLayoutParams.leftMargin = animatedValue;
                } else {
                    marginLayoutParams.rightMargin = animatedValue;
                }
                view.setLayoutParams(marginLayoutParams);
            }
        });
        ValueAnimator alphaAnimator = new ValueAnimator();
        alphaAnimator.setDuration(600);
        alphaAnimator.setFloatValues(new float[]{1.0f, 0.0f});
        alphaAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                directionText.setAlpha(((Float) animation.getAnimatedValue()).floatValue());
                overLay.setAlpha(((Float) animation.getAnimatedValue()).floatValue());
            }
        });
        animatorSet.play(moveOutAnimator).before(alphaAnimator).before(moveRightAnimator);
        animatorSet.addListener(new AnimatorListener() {
            public void onAnimationStart(Animator animation) {
                overLay.setAlpha(1.0f);
                directionText.setAlpha(1.0f);
            }

            public void onAnimationEnd(Animator animation) {
                overLay.setAlpha(0.0f);
                directionText.setAlpha(0.0f);
            }

            public void onAnimationCancel(Animator animation) {
            }

            public void onAnimationRepeat(Animator animation) {
            }
        });
        return animatorSet;
    }

    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == 8) {
            this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
        } else if (visibility == 0) {
            this.mHandler.removeCallbacks(this.mShowSensorBlockedMessageRunnable);
            this.mHandler.postDelayed(this.mShowSensorBlockedMessageRunnable, 30000);
        }
    }

    @Subscribe
    public void onGestureProgress(GestureProgress gestureProgress) {
        this.mHandler.removeCallbacks(this.mProgressIndicatorRunnable);
        float progress = gestureProgress.progress;
        if (gestureProgress.direction == GestureDirection.LEFT) {
            progress *= -1.0f;
        }
        float newSmoothedProgress = (this.mLowPassCoeff * progress) + (this.mSmoothedProgress * (1.0f - this.mLowPassCoeff));
        if (newSmoothedProgress != this.mSmoothedProgress) {
            this.mGestureProgressIndicator.setVisibility(0);
            this.mSmoothedProgress = newSmoothedProgress;
            this.mGestureProgressIndicator.setX((this.mNeutralX + (this.mGestureIndicatorProgressSpan * this.mSmoothedProgress)) - this.mGestureIndicatorHalfWidth);
        }
        this.mHandler.postDelayed(this.mProgressIndicatorRunnable, 100);
    }
}
