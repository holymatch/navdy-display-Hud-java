package com.navdy.hud.app.view;

import com.navdy.hud.app.screen.FactoryResetScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class FactoryResetView$$InjectAdapter extends Binding<FactoryResetView> implements MembersInjector<FactoryResetView> {
    private Binding<Presenter> presenter;

    public FactoryResetView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.FactoryResetView", false, FactoryResetView.class);
    }

    public void attach(Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.screen.FactoryResetScreen$Presenter", FactoryResetView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
    }

    public void injectMembers(FactoryResetView object) {
        object.presenter = (Presenter) this.presenter.get();
    }
}
