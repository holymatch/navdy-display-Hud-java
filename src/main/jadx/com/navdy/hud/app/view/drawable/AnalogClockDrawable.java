package com.navdy.hud.app.view.drawable;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import com.navdy.hud.app.R;
import com.navdy.hud.app.util.DateUtil;

public class AnalogClockDrawable extends CustomDrawable {
    private int centerPointWidth;
    private int dateTextMargin;
    private int dateTextSize;
    private int dayOfMonth;
    private int frameColor;
    private int hour;
    private int hourHandColor;
    private float hourHandLengthFraction = 0.75f;
    private int hourHandStrokeWidth;
    private int minute;
    private int minuteHandColor;
    private float minuteHandLengthFraction = 0.8f;
    private int minuteHandStrokeWidth;
    private int seconds;

    public AnalogClockDrawable(Context context) {
        this.frameColor = context.getResources().getColor(R.color.analog_clock_frame_color);
        this.hourHandColor = context.getResources().getColor(R.color.cyan);
        this.hourHandStrokeWidth = context.getResources().getDimensionPixelSize(R.dimen.analog_clock_hour_hand_width);
        this.minuteHandStrokeWidth = context.getResources().getDimensionPixelSize(R.dimen.analog_clock_minute_hand_width);
        this.centerPointWidth = context.getResources().getDimensionPixelSize(R.dimen.analog_clock_center_point_width);
        this.minuteHandColor = -1;
        this.dateTextSize = context.getResources().getDimensionPixelSize(R.dimen.analog_clock_date_text_size);
        this.dateTextMargin = context.getResources().getDimensionPixelSize(R.dimen.analog_clock_date_text_margin);
    }

    public void setTime(int day, int hour, int minute, int seconds) {
        this.dayOfMonth = day;
        this.hour = hour;
        this.minute = minute;
        this.seconds = seconds;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        Rect bounds = getBounds();
        RectF boundsRectF = new RectF(bounds);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(Style.FILL);
        this.mPaint.setColor(this.frameColor);
        canvas.drawArc(boundsRectF, 0.0f, 360.0f, true, this.mPaint);
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setColor(this.hourHandColor);
        this.mPaint.setStrokeCap(Cap.ROUND);
        this.mPaint.setStrokeWidth((float) this.hourHandStrokeWidth);
        this.mPaint.setShadowLayer(10.0f, 10.0f, 10.0f, -16777216);
        float hourAngle = DateUtil.getClockAngleForHour(this.hour, this.minute);
        float radius = (float) ((bounds.width() / 2) - 15);
        Canvas canvas2 = canvas;
        canvas2.drawLine((float) bounds.centerX(), (float) bounds.centerY(), (float) ((int) (((double) bounds.centerX()) + (((double) radius) * Math.cos(Math.toRadians((double) hourAngle))))), (float) ((int) (((double) bounds.centerY()) + (((double) radius) * Math.sin(Math.toRadians((double) hourAngle))))), this.mPaint);
        this.mPaint.setShadowLayer(0.0f, 0.0f, 0.0f, this.mPaint.getColor());
        this.mPaint.setStyle(Style.STROKE);
        this.mPaint.setColor(this.minuteHandColor);
        this.mPaint.setStrokeCap(Cap.ROUND);
        this.mPaint.setStrokeWidth((float) this.minuteHandStrokeWidth);
        float minuteAngle = DateUtil.getClockAngleForMinutes(this.minute);
        radius = (float) ((bounds.width() / 2) - 10);
        canvas2 = canvas;
        canvas2.drawLine((float) bounds.centerX(), (float) bounds.centerY(), (float) ((int) (((double) bounds.centerX()) + (((double) radius) * Math.cos(Math.toRadians((double) minuteAngle))))), (float) ((int) (((double) bounds.centerY()) + (((double) radius) * Math.sin(Math.toRadians((double) minuteAngle))))), this.mPaint);
        this.mPaint.setStyle(Style.FILL);
        this.mPaint.setStrokeWidth(1.0f);
        this.mPaint.setColor(this.minuteHandColor);
        canvas.drawArc((float) (bounds.centerX() - this.centerPointWidth), (float) (bounds.centerY() - this.centerPointWidth), (float) (bounds.centerX() + this.centerPointWidth), (float) (bounds.centerY() + this.centerPointWidth), 0.0f, 360.0f, true, this.mPaint);
        radius = (float) ((bounds.width() / 2) - this.dateTextMargin);
        this.mPaint.setStyle(Style.FILL);
        this.mPaint.setTypeface(Typeface.create(Typeface.DEFAULT, 1));
        this.mPaint.setTextSize((float) this.dateTextSize);
        String text = Integer.toString(this.dayOfMonth);
        Rect rect = new Rect();
        this.mPaint.getTextBounds(text, 0, text.length(), rect);
        int hour12Format = this.hour % 12;
        if ((hour12Format < 7 || hour12Format >= 11) && (this.minute < 35 || this.minute >= 55)) {
            canvas.drawText(text, (float) ((int) (((double) bounds.centerX()) + (((double) radius) * Math.cos(Math.toRadians((double) 1127481344))))), (float) ((rect.height() / 2) + ((int) (((double) bounds.centerY()) + (((double) radius) * Math.sin(Math.toRadians((double) 1127481344)))))), this.mPaint);
        } else if ((hour12Format < 1 || hour12Format >= 4) && (this.minute < 5 || this.minute >= 20)) {
            canvas.drawText(text, (float) (((int) (((double) bounds.centerX()) + (((double) radius) * Math.cos(Math.toRadians((double) null))))) - rect.width()), (float) ((rect.height() / 2) + ((int) (((double) bounds.centerY()) + (((double) radius) * Math.sin(Math.toRadians((double) null)))))), this.mPaint);
        } else if (hour12Format >= 4 && hour12Format < 7) {
        } else {
            if (this.minute < 20 || this.minute >= 35) {
                canvas.drawText(text, (float) (((int) (((double) bounds.centerX()) + (((double) radius) * Math.cos(Math.toRadians((double) 1119092736))))) - (rect.width() / 2)), (float) ((int) (((double) bounds.centerY()) + (((double) radius) * Math.sin(Math.toRadians((double) 1119092736))))), this.mPaint);
            }
        }
    }
}
