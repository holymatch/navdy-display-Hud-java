package com.navdy.hud.app.view;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ChoiceLayout;

public class GestureLearningView$$ViewInjector {
    public static void inject(Finder finder, GestureLearningView target, Object source) {
        target.mChoiceLayout = (ChoiceLayout) finder.findRequiredView(source, R.id.choiceLayout, "field 'mChoiceLayout'");
        target.mTipsScroller = (LinearLayout) finder.findRequiredView(source, R.id.tips_scroller, "field 'mTipsScroller'");
        target.mTipsTextView1 = (TextView) finder.findRequiredView(source, R.id.tipsText1, "field 'mTipsTextView1'");
        target.mTipsTextView2 = (TextView) finder.findRequiredView(source, R.id.tipsText2, "field 'mTipsTextView2'");
        target.mCenterImage = (ImageView) finder.findRequiredView(source, R.id.center_image, "field 'mCenterImage'");
        target.mGestureProgressIndicator = finder.findRequiredView(source, R.id.gesture_progress_indicator, "field 'mGestureProgressIndicator'");
        target.mLeftSwipeLayout = (LinearLayout) finder.findRequiredView(source, R.id.lyt_left_swipe, "field 'mLeftSwipeLayout'");
        target.mRightSwipeLayout = (LinearLayout) finder.findRequiredView(source, R.id.lyt_right_swipe, "field 'mRightSwipeLayout'");
    }

    public static void reset(GestureLearningView target) {
        target.mChoiceLayout = null;
        target.mTipsScroller = null;
        target.mTipsTextView1 = null;
        target.mTipsTextView2 = null;
        target.mCenterImage = null;
        target.mGestureProgressIndicator = null;
        target.mLeftSwipeLayout = null;
        target.mRightSwipeLayout = null;
    }
}
