package com.navdy.hud.app.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.common.TimeHelper.UpdateClock;
import com.navdy.hud.app.framework.calendar.CalendarManager;
import com.navdy.hud.app.framework.calendar.CalendarManager.CalendarManagerEvent;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.navdy.service.library.events.calendars.CalendarEvent;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Wire;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class CalendarWidgetPresenter extends DashboardWidgetPresenter {
    private static final long MILLISECONDS_IN_A_DAY = 86400000;
    private static final int REFRESH_INTERVAL = ((int) TimeUnit.MINUTES.toMillis(5));
    private static final Logger sLogger = new Logger(CalendarWidgetPresenter.class);
    private String calendarGaugeName;
    CalendarManager calendarManager;
    private Context context;
    private String formattedText;
    private Handler handler;
    private boolean nextEventExists;
    private int paddingLeft;
    private Runnable refreshRunnable;
    private TimeHelper timeHelper = RemoteDeviceManager.getInstance().getTimeHelper();
    private String title;

    public CalendarWidgetPresenter(Context context) {
        this.context = context;
        this.paddingLeft = context.getResources().getDimensionPixelSize(R.dimen.calendar_widget_left_padding);
        this.calendarGaugeName = context.getResources().getString(R.string.widget_calendar);
        this.calendarManager = RemoteDeviceManager.getInstance().getCalendarManager();
        this.handler = new Handler();
        this.refreshRunnable = new Runnable() {
            public void run() {
                CalendarWidgetPresenter.this.refreshCalendarEvent();
            }
        };
    }

    public void setView(DashboardWidgetView dashboardWidgetView, Bundle arguments) {
        if (dashboardWidgetView != null) {
            int paddingLeft;
            int gravity = arguments != null ? arguments.getInt(DashboardWidgetPresenter.EXTRA_GRAVITY, 0) : 0;
            dashboardWidgetView.setContentView((int) R.layout.calendar_widget);
            if (gravity == 2) {
                paddingLeft = this.paddingLeft;
            } else {
                paddingLeft = 0;
            }
            dashboardWidgetView.getChildAt(0).setPadding(paddingLeft, 0, 0, 0);
            this.handler.removeCallbacks(this.refreshRunnable);
            refreshCalendarEvent();
        } else {
            this.handler.removeCallbacks(this.refreshRunnable);
        }
        super.setView(dashboardWidgetView, arguments);
    }

    protected boolean isRegisteringToBusRequired() {
        return true;
    }

    public void refreshCalendarEvent() {
        List<CalendarEvent> calendarEvents = this.calendarManager.getCalendarEvents();
        CalendarEvent nextEvent = null;
        long currentTime = System.currentTimeMillis();
        TimeZone timeZone = this.timeHelper.getTimeZone();
        Calendar currentTimeCalendar = Calendar.getInstance(timeZone);
        currentTimeCalendar.setTime(new Date(currentTime));
        if (calendarEvents != null) {
            for (CalendarEvent event : calendarEvents) {
                sLogger.d("Event :" + event.display_name + ", From : " + event.start_time + ", To : " + event.end_time + ", All day : " + event.all_day);
                long eventEndTime = ((Long) Wire.get(event.end_time, Long.valueOf(0))).longValue();
                long eventStartTime = ((Long) Wire.get(event.start_time, Long.valueOf(0))).longValue();
                Calendar eventStartTimeCalendar = Calendar.getInstance(timeZone);
                eventStartTimeCalendar.setTime(new Date(eventStartTime));
                if (eventEndTime > currentTime && currentTimeCalendar.get(1) == eventStartTimeCalendar.get(1) && currentTimeCalendar.get(6) == eventStartTimeCalendar.get(6)) {
                    if (!event.all_day.booleanValue()) {
                        nextEvent = event;
                        break;
                    } else if (nextEvent == null) {
                        nextEvent = event;
                    }
                }
            }
        }
        if (nextEvent != null) {
            sLogger.d("Next event shown : " + nextEvent.display_name);
            this.nextEventExists = true;
            this.formattedText = formatTime(nextEvent.start_time.longValue());
            this.title = nextEvent.display_name;
        } else {
            sLogger.d("No event shown");
            this.nextEventExists = false;
            this.formattedText = "";
            this.title = "";
        }
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.postDelayed(this.refreshRunnable, (long) REFRESH_INTERVAL);
        reDraw();
    }

    private String formatTime(long time) {
        StringBuilder amPmMarker = new StringBuilder();
        String formattedTime = this.timeHelper.formatTime(new Date(time), amPmMarker);
        return amPmMarker.length() > 0 ? formattedTime + " " + amPmMarker.toString() : formattedTime;
    }

    public Drawable getDrawable() {
        return null;
    }

    protected void updateGauge() {
        if (this.mWidgetView != null) {
            TextView dateTextView = (TextView) this.mWidgetView.findViewById(R.id.txt_unit);
            TextView titleTextView = (TextView) this.mWidgetView.findViewById(R.id.txt_value);
            if (this.nextEventExists) {
                dateTextView.setText(this.formattedText);
                titleTextView.setText(this.title);
                return;
            }
            dateTextView.setText(R.string.today);
            titleTextView.setText(R.string.no_more_events);
        }
    }

    public String getWidgetIdentifier() {
        return SmartDashWidgetManager.CALENDAR_WIDGET_ID;
    }

    public String getWidgetName() {
        return this.calendarGaugeName;
    }

    @Subscribe
    public void onClockChanged(UpdateClock updateClock) {
        refreshCalendarEvent();
    }

    @Subscribe
    public void onCalendarManagerEvent(CalendarManagerEvent event) {
        switch (event) {
            case UPDATED:
                refreshCalendarEvent();
                return;
            default:
                return;
        }
    }
}
