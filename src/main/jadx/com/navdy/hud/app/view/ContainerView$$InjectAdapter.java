package com.navdy.hud.app.view;

import com.navdy.hud.app.ui.framework.UIStateManager;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class ContainerView$$InjectAdapter extends Binding<ContainerView> implements MembersInjector<ContainerView> {
    private Binding<UIStateManager> uiStateManager;

    public ContainerView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.ContainerView", false, ContainerView.class);
    }

    public void attach(Linker linker) {
        this.uiStateManager = linker.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", ContainerView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.uiStateManager);
    }

    public void injectMembers(ContainerView object) {
        object.uiStateManager = (UIStateManager) this.uiStateManager.get();
    }
}
