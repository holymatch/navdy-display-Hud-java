package com.navdy.hud.app.view;

import android.content.SharedPreferences;
import com.navdy.hud.app.screen.DialManagerScreen.Presenter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class DialManagerView$$InjectAdapter extends Binding<DialManagerView> implements MembersInjector<DialManagerView> {
    private Binding<Presenter> presenter;
    private Binding<SharedPreferences> sharedPreferences;

    public DialManagerView$$InjectAdapter() {
        super(null, "members/com.navdy.hud.app.view.DialManagerView", false, DialManagerView.class);
    }

    public void attach(Linker linker) {
        this.presenter = linker.requestBinding("com.navdy.hud.app.screen.DialManagerScreen$Presenter", DialManagerView.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", DialManagerView.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.presenter);
        injectMembersBindings.add(this.sharedPreferences);
    }

    public void injectMembers(DialManagerView object) {
        object.presenter = (Presenter) this.presenter.get();
        object.sharedPreferences = (SharedPreferences) this.sharedPreferences.get();
    }
}
