package com.navdy.hud.app.ui.component.homescreen;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.common.TimeHelper;
import com.navdy.hud.app.common.TimeHelper.UpdateClock;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.Date;

public class TimeView extends RelativeLayout {
    public static final int CLOCK_UPDATE_INTERVAL = 30000;
    @InjectView(R.id.txt_ampm)
    TextView ampmTextView;
    private Bus bus;
    @InjectView(R.id.txt_day)
    TextView dayTextView;
    private Handler handler;
    private Logger logger;
    private Runnable runnable;
    private StringBuilder stringBuilder;
    private TimeHelper timeHelper;
    @InjectView(R.id.txt_time)
    TextView timeTextView;

    public TimeView(Context context) {
        this(context, null);
    }

    public TimeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TimeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.stringBuilder = new StringBuilder();
    }

    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        if (!isInEditMode()) {
            this.timeHelper = RemoteDeviceManager.getInstance().getTimeHelper();
            updateTime();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
            HudApplication.getAppContext().registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if ("android.intent.action.TIMEZONE_CHANGED".equals(intent.getAction())) {
                        TimeView.this.logger.v("timezone change broadcast");
                        TimeView.this.timeHelper.updateLocale();
                    }
                }
            }, intentFilter);
            this.runnable = new Runnable() {
                public void run() {
                    TimeView.this.updateTime();
                    TimeView.this.postDelayed(TimeView.this.runnable, 30000);
                }
            };
            postDelayed(this.runnable, 30000);
            this.bus = RemoteDeviceManager.getInstance().getBus();
            this.bus.register(this);
        }
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.handler != null) {
            this.handler.removeCallbacks(this.runnable);
        }
    }

    @Subscribe
    public void onTimeSettingsChange(UpdateClock event) {
        updateTime();
    }

    private void updateTime() {
        this.dayTextView.setText(this.timeHelper.getDay());
        this.timeTextView.setText(this.timeHelper.formatTime(new Date(), this.stringBuilder));
        this.ampmTextView.setText(this.stringBuilder.toString());
    }

    public void setView(CustomAnimationMode mode) {
        switch (mode) {
            case EXPAND:
                setX((float) HomeScreenResourceValues.timeX);
                return;
            case SHRINK_LEFT:
                setX((float) HomeScreenResourceValues.timeShrinkLeftX);
                return;
            default:
                return;
        }
    }
}
