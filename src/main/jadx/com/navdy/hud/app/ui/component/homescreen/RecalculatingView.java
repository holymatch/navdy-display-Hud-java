package com.navdy.hud.app.ui.component.homescreen;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.RerouteEvent;
import com.navdy.hud.app.maps.MapEvents.RouteEventType;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.ui.component.FluctuatorAnimatorView;
import com.navdy.hud.app.ui.component.image.ColorImageView;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class RecalculatingView extends FrameLayout implements IHomeScreenLifecycle {
    private Bus bus;
    private HomeScreenView homeScreenView;
    private RerouteEvent lastRerouteEvent;
    private Logger logger;
    private boolean paused;
    @InjectView(R.id.recalcAnimator)
    FluctuatorAnimatorView recalcAnimator;
    @InjectView(R.id.recalcImageView)
    ColorImageView recalcImageView;

    public RecalculatingView(Context context) {
        this(context, null);
    }

    public RecalculatingView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RecalculatingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        this.recalcImageView.setColor(HomeScreenResourceValues.reCalcAnimColor);
        this.bus = RemoteDeviceManager.getInstance().getBus();
    }

    public void init(HomeScreenView homeScreenView) {
        this.homeScreenView = homeScreenView;
        this.bus.register(this);
    }

    @Subscribe
    public void onRerouteEvent(RerouteEvent event) {
        this.logger.v("rerouteEvent:" + event.routeEventType);
        if (!this.homeScreenView.isNavigationActive() || HereNavigationManager.getInstance().hasArrived()) {
            this.logger.w("rerouteEvent:navigation not active|hasArrived|routeCalcOn");
            return;
        }
        this.lastRerouteEvent = event;
        if (event.routeEventType == RouteEventType.STARTED) {
            this.homeScreenView.setRecalculating(true);
            if (!this.paused) {
                showRecalculating();
                return;
            }
            return;
        }
        this.homeScreenView.setRecalculating(false);
        if (!this.paused || getVisibility() == 0) {
            hideRecalculating();
            this.homeScreenView.getTbtView().setVisibility(0);
        }
    }

    public void showRecalculating() {
        this.homeScreenView.getTbtView().setVisibility(8);
        setVisibility(0);
        this.recalcAnimator.start();
    }

    public void hideRecalculating() {
        setVisibility(8);
        this.recalcAnimator.stop();
        this.lastRerouteEvent = null;
    }

    public void setView(CustomAnimationMode mode) {
        switch (mode) {
            case EXPAND:
                setX((float) HomeScreenResourceValues.recalcX);
                return;
            case SHRINK_LEFT:
                setX((float) HomeScreenResourceValues.recalcShrinkLeftX);
                return;
            default:
                return;
        }
    }

    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:recalc");
        }
    }

    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:recalc");
            RerouteEvent event = this.lastRerouteEvent;
            if (event != null) {
                this.lastRerouteEvent = null;
                this.logger.v("::onResume:recalc set last reroute event");
                onRerouteEvent(event);
            }
        }
    }
}
