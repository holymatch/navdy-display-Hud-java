package com.navdy.hud.app.ui.component.homescreen;

import android.animation.AnimatorSet.Builder;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.DisplayTrafficLaneInfo;
import com.navdy.hud.app.maps.MapEvents.HideTrafficLaneInfo;
import com.navdy.hud.app.maps.MapEvents.LaneData;
import com.navdy.hud.app.ui.activity.Main;
import com.navdy.hud.app.ui.component.homescreen.HomeScreen.DisplayMode;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

public class LaneGuidanceView extends LinearLayout implements IHomeScreenLifecycle {
    private Bus bus;
    private int iconH;
    private int iconW;
    private DisplayTrafficLaneInfo lastEvent;
    private Logger logger;
    private int mapIconIndicatorBottomMarginWithLane;
    private boolean needsMeasurement;
    private boolean paused;
    private int separatorColor;
    private int separatorH;
    private int separatorMargin;
    private int separatorW;
    private UIStateManager uiStateManager;

    public LaneGuidanceView(Context context) {
        this(context, null);
    }

    public LaneGuidanceView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LaneGuidanceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (this.needsMeasurement) {
            int w = getMeasuredWidth();
            if (w > 0) {
                setX((float) (((HomeScreenResourceValues.transformCenterIconWidth / 2) + getMapIconX(null)) - (w / 2)));
                this.needsMeasurement = false;
            }
        }
    }

    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        setY((float) HomeScreenResourceValues.laneGuidanceY);
        RemoteDeviceManager remoteDeviceManager = RemoteDeviceManager.getInstance();
        this.bus = remoteDeviceManager.getBus();
        this.uiStateManager = remoteDeviceManager.getUiStateManager();
        Resources resources = getContext().getResources();
        this.iconH = resources.getDimensionPixelSize(R.dimen.lane_guidance_icon_h);
        this.iconW = resources.getDimensionPixelSize(R.dimen.lane_guidance_icon_w);
        this.separatorH = resources.getDimensionPixelSize(R.dimen.lane_guidance_separator_h);
        this.separatorW = resources.getDimensionPixelSize(R.dimen.lane_guidance_separator_w);
        this.separatorMargin = resources.getDimensionPixelSize(R.dimen.lane_guidance_separator_margin);
        this.separatorColor = resources.getColor(17170443);
        this.mapIconIndicatorBottomMarginWithLane = resources.getDimensionPixelSize(R.dimen.map_icon_indicator_bottom_margin_with_lane);
    }

    public void init(HomeScreenView homeScreenView) {
        this.bus.register(this);
    }

    public void setView(CustomAnimationMode mode) {
    }

    public void getCustomAnimator(CustomAnimationMode mode, Builder mainBuilder) {
        if (getVisibility() == 0 && getMeasuredWidth() != 0) {
            int w = getMeasuredWidth();
            int mapIconX = getMapIconX(mode);
            mainBuilder.with(HomeScreenUtils.getXPositionAnimator(this, (float) (((HomeScreenResourceValues.transformCenterIconWidth / 2) + mapIconX) - (w / 2))));
            mainBuilder.with(HomeScreenUtils.getXPositionAnimator(this.uiStateManager.getHomescreenView().getLaneGuidanceIconIndicator(), (float) mapIconX));
        }
    }

    @Subscribe
    public void onLaneInfoShow(DisplayTrafficLaneInfo event) {
        if (this.uiStateManager.getHomescreenView().getDisplayMode() != DisplayMode.MAP) {
            onHideLaneInfo(null);
            this.lastEvent = event;
            return;
        }
        this.logger.v("LaneGuidanceView:showLanes");
        this.lastEvent = event;
        if (!this.paused) {
            removeAllViews();
            Context context = getContext();
            int size = event.laneData.size();
            addSeparator(context, false, true);
            for (int i = 0; i < size; i++) {
                ImageView imageView = new ImageView(context);
                imageView.setLayoutParams(new LayoutParams(this.iconW, this.iconH));
                Drawable[] icons = ((LaneData) event.laneData.get(i)).icons;
                if (icons == null || icons.length <= 0) {
                    imageView.setImageResource(0);
                } else {
                    imageView.setImageDrawable(new LayerDrawable(icons));
                }
                addView(imageView);
                if (i < size - 1) {
                    addSeparator(context, true, true);
                }
            }
            addSeparator(context, true, false);
            showMapIconIndicator();
            this.needsMeasurement = true;
            setVisibility(0);
        }
    }

    @Subscribe
    public void onHideLaneInfo(HideTrafficLaneInfo event) {
        this.lastEvent = null;
        this.needsMeasurement = false;
        if (getVisibility() == 0) {
            this.logger.v("LaneGuidanceView:hideLanes");
            hideMapIconIndicator();
            setVisibility(8);
            removeAllViews();
        }
    }

    private void addSeparator(Context context, boolean leftMargin, boolean rightMargin) {
        View separator = new View(context);
        LayoutParams lytParams = new LayoutParams(this.separatorW, this.separatorH);
        lytParams.gravity = 80;
        if (leftMargin) {
            lytParams.leftMargin = this.separatorMargin;
        }
        if (rightMargin) {
            lytParams.rightMargin = this.separatorMargin;
        }
        separator.setLayoutParams(lytParams);
        separator.setBackgroundColor(this.separatorColor);
        addView(separator, lytParams);
    }

    private void showMapIconIndicator() {
        ImageView view = this.uiStateManager.getHomescreenView().getLaneGuidanceIconIndicator();
        view.setX((float) getMapIconX(null));
        ((MarginLayoutParams) view.getLayoutParams()).bottomMargin = this.mapIconIndicatorBottomMarginWithLane;
        view.setVisibility(0);
    }

    private void hideMapIconIndicator() {
        this.uiStateManager.getHomescreenView().getLaneGuidanceIconIndicator().setVisibility(8);
    }

    private int getMapIconX(CustomAnimationMode mode) {
        if (mode != null) {
            switch (mode) {
                case SHRINK_LEFT:
                    return HomeScreenResourceValues.iconIndicatorShrinkLeft_X;
                case EXPAND:
                    return HomeScreenResourceValues.iconIndicatorX;
                default:
                    return 0;
            }
        }
        Main rootScreen = this.uiStateManager.getRootScreen();
        if (rootScreen.isNotificationViewShowing() || rootScreen.isNotificationExpanding()) {
            return HomeScreenResourceValues.iconIndicatorShrinkLeft_X;
        }
        return HomeScreenResourceValues.iconIndicatorX;
    }

    public void showLastEvent() {
        if (this.lastEvent != null) {
            onLaneInfoShow(this.lastEvent);
        }
    }

    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.logger.v("::onPause:lane");
        }
    }

    public void onResume() {
        if (this.paused) {
            this.paused = false;
            this.logger.v("::onResume:lane");
            if (this.uiStateManager.getHomescreenView().getDisplayMode() == DisplayMode.MAP) {
                DisplayTrafficLaneInfo event = this.lastEvent;
                if (event != null) {
                    this.lastEvent = null;
                    onLaneInfoShow(event);
                    this.logger.v("::onResume:shown last lane");
                }
            }
        }
    }
}
