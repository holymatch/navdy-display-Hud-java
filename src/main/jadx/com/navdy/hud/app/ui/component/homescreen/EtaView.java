package com.navdy.hud.app.ui.component.homescreen;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.maps.MapEvents.TrafficDelayDismissEvent;
import com.navdy.hud.app.maps.MapEvents.TrafficDelayEvent;
import com.navdy.hud.app.maps.here.HereMapUtil;
import com.navdy.hud.app.maps.util.RouteUtils;
import com.navdy.hud.app.view.MainView.CustomAnimationMode;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.concurrent.TimeUnit;

public class EtaView extends RelativeLayout {
    private Bus bus;
    @InjectView(R.id.etaTextView)
    TextView etaTextView;
    @InjectView(R.id.etaTimeAmPm)
    TextView etaTimeAmPm;
    @InjectView(R.id.etaTimeLeft)
    TextView etaTimeLeft;
    private HomeScreenView homeScreenView;
    private String lastETA;
    private String lastETADate;
    private String lastETA_AmPm;
    private Logger logger;

    public EtaView(Context context) {
        this(context, null);
    }

    public EtaView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EtaView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View) this);
        if (!isInEditMode()) {
            if (HomeScreenView.showDriveScoreEventsOnMap()) {
                this.etaTimeLeft.setVisibility(8);
            }
            this.bus = RemoteDeviceManager.getInstance().getBus();
        }
    }

    public void init(HomeScreenView homeScreenView) {
        this.homeScreenView = homeScreenView;
        this.bus.register(this);
    }

    @Subscribe
    public void onTrafficDelayEvent(TrafficDelayEvent event) {
        setEtaTextColor(HomeScreenResourceValues.badTrafficColor);
    }

    @Subscribe
    public void onTrafficDelayDismissEvent(TrafficDelayDismissEvent event) {
        setEtaTextColor(-1);
    }

    private void setEtaTextColor(int color) {
        this.etaTextView.setTextColor(color);
        this.etaTimeLeft.setTextColor(color);
        this.etaTimeAmPm.setTextColor(color);
    }

    public void clearState() {
        this.lastETA = null;
        this.lastETA_AmPm = null;
        this.lastETADate = null;
        this.etaTextView.setText("");
        this.etaTimeLeft.setText("");
        this.etaTimeAmPm.setText("");
        setEtaTextColor(-1);
    }

    @Subscribe
    public void updateETA(ManeuverDisplay event) {
        if (!this.homeScreenView.isNavigationActive()) {
            return;
        }
        if (event.etaDate == null) {
            this.logger.w("etaDate is not set");
            return;
        }
        long time = event.etaDate.getTime() - System.currentTimeMillis();
        if (time < 0) {
            time = 0;
        } else {
            time = TimeUnit.MILLISECONDS.toMinutes(time);
        }
        this.etaTimeLeft.setText(RouteUtils.formatEtaMinutes(getResources(), (int) time));
        if (!TextUtils.equals(event.eta, this.lastETA)) {
            this.lastETA = event.eta;
            this.lastETADate = HereMapUtil.convertDateToEta(event.etaDate);
            this.etaTextView.setText(this.lastETA);
        }
        if (!TextUtils.equals(event.etaAmPm, this.lastETA_AmPm)) {
            this.lastETA_AmPm = event.etaAmPm;
            this.etaTimeAmPm.setText(this.lastETA_AmPm);
        }
    }

    private String getEtaTimeLeft(int minutes) {
        Resources resources = getResources();
        if (minutes < 60) {
            return resources.getString(R.string.eta_time_min, new Object[]{Integer.valueOf(minutes)});
        } else if (minutes < HomeScreenConstants.MINUTES_DAY) {
            if (minutes - ((minutes / 60) * 60) > 0) {
                return resources.getString(R.string.eta_time_hour, new Object[]{Integer.valueOf(minutes / 60), Integer.valueOf(minutes - ((minutes / 60) * 60))});
            }
            return resources.getString(R.string.eta_time_hour_no_min, new Object[]{Integer.valueOf(minutes / 60)});
        } else {
            if ((minutes - ((minutes / HomeScreenConstants.MINUTES_DAY) * HomeScreenConstants.MINUTES_DAY)) / 60 > 0) {
                return resources.getString(R.string.eta_time_day, new Object[]{Integer.valueOf(minutes / HomeScreenConstants.MINUTES_DAY), Integer.valueOf((minutes - ((minutes / HomeScreenConstants.MINUTES_DAY) * HomeScreenConstants.MINUTES_DAY)) / 60)});
            }
            return resources.getString(R.string.eta_time_day_no_hour, new Object[]{Integer.valueOf(minutes / HomeScreenConstants.MINUTES_DAY)});
        }
    }

    public void setView(CustomAnimationMode mode) {
        switch (mode) {
            case EXPAND:
                setX((float) HomeScreenResourceValues.etaX);
                return;
            case SHRINK_LEFT:
                setX((float) HomeScreenResourceValues.etaShrinkLeftX);
                return;
            default:
                return;
        }
    }

    public String getLastEtaDate() {
        return this.lastETADate;
    }
}
