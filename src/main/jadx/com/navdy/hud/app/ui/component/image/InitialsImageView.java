package com.navdy.hud.app.ui.component.image;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.navdy.hud.app.R;

public class InitialsImageView extends ImageView {
    private static int greyColor;
    private static int largeStyleTextSize;
    private static int mediumStyleTextSize;
    private static int smallStyleTextSize;
    private static int tinyStyleTextSize;
    private static Typeface typefaceLarge;
    private static Typeface typefaceSmall;
    private static boolean valuesSet;
    private static int whiteColor;
    private int bkColor;
    private String initials;
    private Paint paint;
    private boolean scaled;
    private Style style;

    public enum Style {
        TINY,
        SMALL,
        LARGE,
        MEDIUM,
        DEFAULT
    }

    public InitialsImageView(Context context) {
        this(context, null, 0);
    }

    public InitialsImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InitialsImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.style = Style.DEFAULT;
        this.bkColor = greyColor;
        init();
    }

    private void init() {
        if (!valuesSet) {
            valuesSet = true;
            Resources resources = getResources();
            greyColor = resources.getColor(R.color.grey_4a);
            whiteColor = resources.getColor(17170443);
            tinyStyleTextSize = (int) resources.getDimension(R.dimen.contact_image_tiny_text);
            smallStyleTextSize = (int) resources.getDimension(R.dimen.contact_image_small_text);
            mediumStyleTextSize = (int) resources.getDimension(R.dimen.contact_image_medium_text);
            largeStyleTextSize = (int) resources.getDimension(R.dimen.contact_image_large_text);
            typefaceSmall = Typeface.create("sans-serif-medium", 0);
            typefaceLarge = Typeface.create("sans-serif", 1);
        }
        this.paint = new Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }

    public void setInitials(String initials, Style style) {
        this.initials = initials;
        this.style = style;
        invalidate();
    }

    public void setImage(int resourceId, String initials, Style style) {
        setImageResource(resourceId);
        setInitials(initials, style);
    }

    public Style getStyle() {
        return this.style;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.style != Style.DEFAULT) {
            int width = getWidth();
            int height = getHeight();
            switch (this.style) {
                case SMALL:
                case TINY:
                    canvas.drawColor(0);
                    if (this.bkColor != 0) {
                        this.paint.setColor(this.bkColor);
                        canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) (width / 2), this.paint);
                        break;
                    }
                    break;
            }
            Style s = this.style;
            if (this.scaled) {
                s = Style.SMALL;
            }
            if (this.initials != null) {
                this.paint.setColor(whiteColor);
                switch (s) {
                    case SMALL:
                        this.paint.setTextSize((float) smallStyleTextSize);
                        this.paint.setTypeface(typefaceSmall);
                        break;
                    case TINY:
                        this.paint.setTextSize((float) tinyStyleTextSize);
                        this.paint.setTypeface(typefaceSmall);
                        break;
                    case MEDIUM:
                        this.paint.setTextSize((float) mediumStyleTextSize);
                        this.paint.setTypeface(typefaceLarge);
                        break;
                    case LARGE:
                        this.paint.setTextSize((float) largeStyleTextSize);
                        this.paint.setTypeface(typefaceLarge);
                        break;
                }
                this.paint.setTextAlign(Align.CENTER);
                canvas.drawText(this.initials, (float) (width / 2), (float) ((int) (((float) (height / 2)) - ((this.paint.descent() + this.paint.ascent()) / 2.0f))), this.paint);
            }
        }
    }

    public void setScaled(boolean val) {
        this.scaled = val;
    }

    public void setBkColor(int color) {
        this.bkColor = color;
    }
}
