package com.navdy.hud.app.ui.component.homescreen;

import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;

public class SpeedView$$ViewInjector {
    public static void inject(Finder finder, SpeedView target, Object source) {
        target.speedView = (TextView) finder.findRequiredView(source, R.id.speedView, "field 'speedView'");
        target.speedUnitView = (TextView) finder.findRequiredView(source, R.id.speedUnitView, "field 'speedUnitView'");
    }

    public static void reset(SpeedView target) {
        target.speedView = null;
        target.speedUnitView = null;
    }
}
