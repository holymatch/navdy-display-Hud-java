package com.navdy.hud.app.ui.component.mainmenu;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import com.makeramen.RoundedTransformationBuilder;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.navdy.hud.app.framework.contacts.FavoriteContactsManager;
import com.navdy.hud.app.framework.contacts.NumberType;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.framework.recentcall.RecentCall;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView.Style;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.VerticalModelCache;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class ContactsMenu implements IMenu {
    private static final float CONTACT_PHOTO_OPACITY = 0.6f;
    private static final Model back;
    private static final int backColor = resources.getColor(R.color.mm_back);
    private static final int contactColor = resources.getColor(R.color.mm_contacts);
    private static final String contactStr = resources.getString(R.string.carousel_menu_contacts);
    private static final Handler handler = new Handler(Looper.getMainLooper());
    private static final int noContactColor = resources.getColor(R.color.icon_user_bg_4);
    private static final String recentContactStr = resources.getString(R.string.carousel_menu_recent_contacts_title);
    private static final Model recentContacts;
    private static final Resources resources = HudApplication.getAppContext().getResources();
    private static final Transformation roundTransformation = new RoundedTransformationBuilder().oval(true).build();
    private static final Logger sLogger = new Logger(ContactsMenu.class);
    private int backSelection;
    private int backSelectionId;
    private Bus bus;
    private List<Model> cachedList;
    private IMenu parent;
    private Presenter presenter;
    private RecentContactsMenu recentContactsMenu;
    private List<Model> returnToCacheList;
    private VerticalMenuComponent vscrollComponent;

    static {
        String title = resources.getString(R.string.back);
        int fluctuatorColor = backColor;
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
        title = recentContactStr;
        fluctuatorColor = contactColor;
        recentContacts = IconBkColorViewHolder.buildModel(R.id.contacts_menu_recent, R.drawable.icon_place_recent_2, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, title, null);
    }

    ContactsMenu(Bus bus, VerticalMenuComponent vscrollComponent, Presenter presenter, IMenu parent) {
        this.bus = bus;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
    }

    public List<Model> getItems() {
        if (this.cachedList != null) {
            return this.cachedList;
        }
        List<Model> list = new ArrayList();
        this.returnToCacheList = new ArrayList();
        FavoriteContactsManager favoriteContactsManager = FavoriteContactsManager.getInstance();
        if (favoriteContactsManager.isAllowedToReceiveContacts()) {
            list.add(back);
            list.add(recentContacts);
            int counter = 0;
            List<Contact> favContacts = favoriteContactsManager.getFavoriteContacts();
            if (favContacts != null) {
                sLogger.v("favorite contacts:" + favContacts.size());
                for (Contact contact : favContacts) {
                    int counter2 = counter + 1;
                    Model model = buildModel(counter, contact.name, contact.formattedNumber, contact.numberTypeStr, contact.defaultImageIndex, contact.numberType, contact.initials);
                    model.state = contact;
                    list.add(model);
                    this.returnToCacheList.add(model);
                    counter = counter2;
                }
            } else {
                sLogger.v("no favorite contacts");
            }
            this.cachedList = list;
            return list;
        }
        list.add(getContactsNotAllowed());
        return list;
    }

    public int getInitialSelection() {
        if (this.cachedList == null) {
            return 0;
        }
        if (this.cachedList.size() >= 3) {
            return 2;
        }
        return 1;
    }

    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_mm_contacts_2, contactColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText(contactStr);
    }

    public Model getModelfromPos(int pos) {
        if (this.cachedList == null || this.cachedList.size() <= pos) {
            return null;
        }
        return (Model) this.cachedList.get(pos);
    }

    public boolean isBindCallsEnabled() {
        return true;
    }

    public void onBindToView(Model model, View view, int pos, ModelState state) {
        if (model.state != null) {
            String number;
            final InitialsImageView imageView = (InitialsImageView) VerticalList.findImageView(view);
            InitialsImageView smallImageView = (InitialsImageView) VerticalList.findSmallImageView(view);
            CrossFadeImageView crossFadeImageView = (CrossFadeImageView) VerticalList.findCrossFadeImageView(view);
            imageView.setTag(null);
            if (model.state instanceof RecentCall) {
                number = ((RecentCall) model.state).number;
            } else {
                number = ((Contact) model.state).number;
            }
            final File imagePath = PhoneImageDownloader.getInstance().getImagePath(number, PhotoType.PHOTO_CONTACT);
            Bitmap bitmap = PicassoUtil.getBitmapfromCache(imagePath);
            if (bitmap != null) {
                imageView.setTag(null);
                imageView.setInitials(null, Style.DEFAULT);
                imageView.setImageBitmap(bitmap);
                smallImageView.setInitials(null, Style.DEFAULT);
                smallImageView.setImageBitmap(bitmap);
                smallImageView.setAlpha(0.6f);
                crossFadeImageView.setSmallAlpha(0.6f);
                state.updateImage = false;
                state.updateSmallImage = false;
                return;
            }
            imageView.setTag(model.state);
            final Model model2 = model;
            final int i = pos;
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    if (imagePath.exists()) {
                        ContactsMenu.handler.post(new Runnable() {
                            public void run() {
                                if (imageView.getTag() == model2.state) {
                                    ContactsMenu.this.setImage(imagePath, VerticalViewHolder.selectedIconSize, VerticalViewHolder.selectedIconSize, i);
                                }
                            }
                        });
                    }
                }
            }, 1);
        }
    }

    public IMenu getChildMenu(IMenu parent, String args, String path) {
        return null;
    }

    public void onUnload(MenuLevel level) {
        switch (level) {
            case CLOSE:
                if (this.returnToCacheList != null) {
                    sLogger.v("cm:unload add to cache");
                    VerticalModelCache.addToCache(this.returnToCacheList);
                    this.returnToCacheList = null;
                }
                if (this.recentContactsMenu != null) {
                    this.recentContactsMenu.onUnload(MenuLevel.CLOSE);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onItemSelected(ItemSelectionState selection) {
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return true;
    }

    public boolean selectItem(ItemSelectionState selection) {
        sLogger.v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case R.id.contacts_menu_recent:
                sLogger.v("recent contacts");
                AnalyticsSupport.recordMenuSelection("recent_contacts");
                if (this.recentContactsMenu == null) {
                    this.recentContactsMenu = new RecentContactsMenu(this.bus, this.vscrollComponent, this.presenter, this);
                }
                this.presenter.loadMenu(this.recentContactsMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
            case R.id.menu_back:
                sLogger.v("back");
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            default:
                sLogger.v("contact options");
                AnalyticsSupport.recordMenuSelection("contact_options");
                Contact contact = ((Model) this.cachedList.get(selection.pos)).state;
                List<Contact> list = new ArrayList(1);
                list.add(contact);
                this.presenter.loadMenu(new ContactOptionsMenu(list, this.vscrollComponent, this.presenter, this, this.bus), MenuLevel.SUB_LEVEL, selection.pos, 0);
                break;
        }
        return true;
    }

    public Menu getType() {
        return Menu.CONTACTS;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    Model buildModel(int id, String contactName, String formattedNumber, String numberTypeStr, int defaultImageIndex, NumberType numberType, String initials) {
        Model model;
        ContactImageHelper contactImageHelper = ContactImageHelper.getInstance();
        if (TextUtils.isEmpty(contactName)) {
            model = IconsTwoViewHolder.buildModel(id, R.drawable.icon_user_bg_4, R.drawable.icon_user_numberonly, noContactColor, -1, formattedNumber, null);
        } else {
            String subTitle;
            int largeImageRes = contactImageHelper.getResourceId(defaultImageIndex);
            int fluctuator = contactImageHelper.getResourceColor(defaultImageIndex);
            if (numberType != NumberType.OTHER) {
                subTitle = numberTypeStr;
            } else {
                subTitle = formattedNumber;
            }
            model = IconsTwoViewHolder.buildModel(id, largeImageRes, R.drawable.icon_user_grey, fluctuator, -1, contactName, subTitle);
        }
        model.extras = new HashMap();
        model.extras.put(Model.INITIALS, initials);
        return model;
    }

    private void setImage(File path, int width, int height, final int position) {
        PicassoUtil.getInstance().load(path).resize(width, height).transform(roundTransformation).into(new Target() {
            public void onBitmapLoaded(Bitmap bitmap, LoadedFrom from) {
                ContactsMenu.this.presenter.refreshDataforPos(position);
            }

            public void onBitmapFailed(Drawable errorDrawable) {
            }

            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
    }

    private Model getContactsNotAllowed() {
        String title;
        Resources resources = HudApplication.getAppContext().getResources();
        if (RemoteDeviceManager.getInstance().isRemoteDeviceIos()) {
            title = resources.getString(R.string.allow_contact_access_iphone_title);
        } else {
            title = resources.getString(R.string.allow_contact_access_android_title);
        }
        return IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, contactColor, MainMenu.bkColorUnselected, contactColor, title, null);
    }
}
