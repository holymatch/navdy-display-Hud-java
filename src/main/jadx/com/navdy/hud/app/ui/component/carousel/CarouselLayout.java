package com.navdy.hud.app.ui.component.carousel;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.AnimatorSet.Builder;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import com.navdy.hud.app.R;
import com.navdy.hud.app.audio.SoundUtils;
import com.navdy.hud.app.audio.SoundUtils.Sound;
import com.navdy.hud.app.gesture.GestureDetector;
import com.navdy.hud.app.gesture.GestureDetector.GestureListener;
import com.navdy.hud.app.gesture.MultipleClickGestureDetector;
import com.navdy.hud.app.gesture.MultipleClickGestureDetector.IMultipleClickKeyGesture;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.ui.component.carousel.AnimationStrategy.Direction;
import com.navdy.hud.app.ui.component.carousel.Carousel.InitParams;
import com.navdy.hud.app.ui.component.carousel.Carousel.Listener;
import com.navdy.hud.app.ui.component.carousel.Carousel.Model;
import com.navdy.hud.app.ui.component.carousel.Carousel.ViewCacheManager;
import com.navdy.hud.app.ui.component.carousel.Carousel.ViewProcessor;
import com.navdy.hud.app.ui.component.carousel.Carousel.ViewType;
import com.navdy.hud.app.ui.component.image.CrossFadeImageView;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import com.navdy.hud.app.ui.framework.DefaultAnimationListener;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class CarouselLayout extends FrameLayout implements IInputHandler, GestureListener {
    static final /* synthetic */ boolean $assertionsDisabled = (!CarouselLayout.class.desiredAssertionStatus());
    private static final int DEFAULT_ANIM_DURATION = 250;
    private static final Logger sLogger = new Logger(CarouselLayout.class);
    private int animationDuration;
    boolean animationRunning;
    private AnimationStrategy animator;
    CarouselAdapter carouselAdapter;
    CarouselIndicator carouselIndicator;
    int currentItem = -1;
    protected GestureDetector detector = new GestureDetector(this);
    private boolean exitOnDoubleClick;
    private boolean fastScrollAnimation;
    private Handler handler = new Handler(Looper.getMainLooper());
    int imageLytResourceId;
    LayoutInflater inflater;
    int infoLayoutResourceId;
    Interpolator interpolator;
    Listener itemChangeListener;
    Operation lastScrollAnimationOperation;
    private int lastTouchX;
    private int lastTouchY;
    View leftView;
    int mainImageSize;
    int mainLeftPadding;
    int mainRightPadding;
    int mainViewDividerPadding;
    View middleLeftView;
    View middleRightView;
    List<Model> model;
    private MultipleClickGestureDetector multipleClickGestureDetector;
    View newLeftView;
    View newMiddleLeftView;
    View newMiddleRightView;
    View newRightView;
    private Queue<OperationInfo> operationQueue = new LinkedList();
    int rightImageStart;
    int rightSectionHeight;
    int rightSectionWidth;
    View rightView;
    View rootContainer;
    View selectedItemView;
    int sideImageSize;
    private ViewCacheManager viewCacheManager = new ViewCacheManager(3);
    int viewPadding;
    ViewProcessor viewProcessor;
    boolean viewsScaled;

    enum Operation {
        FORWARD,
        BACK,
        SELECT,
        OTHER
    }

    static class OperationInfo {
        int animationDuration = -1;
        int count = 1;
        Runnable runnable;
        Operation type;

        OperationInfo(Operation type, Runnable runnable) {
            this.type = type;
            this.runnable = runnable;
        }

        OperationInfo(Operation type, Runnable runnable, int animationDuration) {
            this.type = type;
            this.runnable = runnable;
            this.animationDuration = animationDuration;
        }
    }

    public CarouselLayout(Context context) {
        super(context, null);
    }

    public CarouselLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.inflater = LayoutInflater.from(context);
        initFromAttributes(context, attrs);
        setWillNotDraw(false);
        if (!DeviceUtil.isNavdyDevice()) {
            setOnTouchListener(new OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == 1) {
                        CarouselLayout.this.lastTouchX = (int) event.getX();
                        CarouselLayout.this.lastTouchY = (int) event.getY();
                    }
                    return false;
                }
            });
            setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    Rect hitRect = new Rect();
                    int[] myPos = new int[2];
                    int[] parentPos = new int[2];
                    CarouselLayout.this.getLocationInWindow(myPos);
                    View[] views = new View[]{CarouselLayout.this.leftView, CarouselLayout.this.middleLeftView, CarouselLayout.this.rightView};
                    int i = 0;
                    while (i < views.length) {
                        View childView = views[i];
                        ((View) childView.getParent()).getLocationInWindow(parentPos);
                        int touchX = CarouselLayout.this.lastTouchX - (parentPos[0] - myPos[0]);
                        int touchY = CarouselLayout.this.lastTouchY - (parentPos[1] - myPos[1]);
                        childView.getHitRect(hitRect);
                        if (!hitRect.contains(touchX, touchY)) {
                            i++;
                        } else if (i == 1) {
                            CarouselLayout.this.selectItem();
                            return;
                        } else {
                            CarouselLayout.this.move(null, i > 1, true, false, CarouselLayout.this.animationDuration);
                            return;
                        }
                    }
                }
            });
        }
    }

    private void initFromAttributes(Context context, AttributeSet attrs) {
        boolean z = true;
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.Carousel, 0, 0);
        if ($assertionsDisabled || a != null) {
            Resources resources = getResources();
            try {
                this.animationDuration = a.getInt(0, 250);
                this.interpolator = AnimationUtils.loadInterpolator(getContext(), a.getResourceId(1, 17432580));
                this.viewPadding = (int) a.getDimension(2, resources.getDimension(R.dimen.carousel_side_margin));
                this.sideImageSize = (int) a.getDimension(3, resources.getDimension(R.dimen.carousel_side_image_size));
                this.mainImageSize = (int) a.getDimension(4, resources.getDimension(R.dimen.carousel_main_image_size));
                this.rightImageStart = (int) a.getDimension(7, resources.getDimension(R.dimen.carousel_main_right_section_start));
                this.mainLeftPadding = (int) a.getDimension(5, resources.getDimension(R.dimen.carousel_main_left_padding));
                this.mainRightPadding = (int) a.getDimension(6, resources.getDimension(R.dimen.carousel_main_right_padding));
                this.rightSectionWidth = (int) a.getDimension(9, resources.getDimension(R.dimen.carousel_main_right_section_width));
                if (this.mainImageSize == this.sideImageSize) {
                    z = false;
                }
                this.viewsScaled = z;
                sLogger.v("view scaled=" + this.viewsScaled);
                this.rightSectionHeight = (int) getResources().getDimension(R.dimen.dashboard_box_height);
            } finally {
                a.recycle();
            }
        } else {
            throw new AssertionError();
        }
    }

    public void init(InitParams params) {
        if (this.model != null) {
            throw new IllegalStateException();
        } else if (params.model == null || params.rootContainer == null || params.infoLayoutResourceId == 0 || params.imageLytResourceId == 0) {
            throw new IllegalArgumentException();
        } else {
            this.model = params.model;
            this.infoLayoutResourceId = params.infoLayoutResourceId;
            this.viewProcessor = params.viewProcessor;
            this.rootContainer = params.rootContainer;
            this.carouselIndicator = params.carouselIndicator;
            this.carouselAdapter = new CarouselAdapter(this);
            this.imageLytResourceId = params.imageLytResourceId;
            this.fastScrollAnimation = params.fastScrollAnimation;
            this.exitOnDoubleClick = params.exitOnDoubleClick;
            AnimationStrategy fastScrollAnimator = params.animator != null ? params.animator : this.fastScrollAnimation ? new FastScrollAnimator(this) : new CarouselAnimator();
            this.animator = fastScrollAnimator;
            if (this.exitOnDoubleClick) {
                this.multipleClickGestureDetector = new MultipleClickGestureDetector(2, new IMultipleClickKeyGesture() {
                    public void onMultipleClick(int count) {
                        if (CarouselLayout.this.itemChangeListener != null) {
                            CarouselLayout.this.itemChangeListener.onExit();
                        }
                    }

                    public boolean onGesture(GestureEvent event) {
                        return false;
                    }

                    public boolean onKey(CustomKeyEvent event) {
                        return CarouselLayout.this.handleKey(event);
                    }

                    public IInputHandler nextHandler() {
                        return null;
                    }
                });
            }
        }
    }

    public void setListener(Listener carouselListener) {
        this.itemChangeListener = carouselListener;
    }

    public Listener getListener() {
        return this.itemChangeListener;
    }

    public int getCurrentItem() {
        return this.currentItem;
    }

    public Model getCurrentModel() {
        return (Model) this.model.get(this.currentItem);
    }

    public Model getModel(int item) {
        if (item < 0 || item >= this.model.size()) {
            return null;
        }
        return (Model) this.model.get(item);
    }

    public int getCount() {
        return this.model == null ? 0 : this.model.size();
    }

    public void setModel(final List<Model> newModel, final int position, final boolean animated) {
        if (this.animationRunning) {
            clearOperationQueue();
            this.operationQueue.add(new OperationInfo(Operation.OTHER, new Runnable() {
                public void run() {
                    CarouselLayout.this.setModelInternal(newModel, position, animated);
                }
            }));
            return;
        }
        setModelInternal(newModel, position, animated);
    }

    private void setModelInternal(final List<Model> newModel, final int position, boolean animated) {
        if (this.currentItem == -1 || !animated) {
            this.model = newModel;
            setCurrentItem(position, false);
            runQueuedOperation();
            return;
        }
        AnimatorSet animatorSet = new AnimatorSet();
        Builder builder = animatorSet.play(ObjectAnimator.ofFloat(this.middleLeftView, View.ALPHA, new float[]{0.0f}));
        if (this.middleRightView != null) {
            builder.with(ObjectAnimator.ofFloat(this.middleRightView, View.ALPHA, new float[]{0.0f}));
        }
        if (this.leftView != null) {
            builder.with(ObjectAnimator.ofFloat(this.leftView, View.ALPHA, new float[]{0.0f}));
        }
        if (this.rightView != null) {
            builder.with(ObjectAnimator.ofFloat(this.rightView, View.ALPHA, new float[]{0.0f}));
        }
        animatorSet.setDuration(250);
        animatorSet.addListener(new DefaultAnimationListener() {
            public void onAnimationStart(Animator animator) {
                CarouselLayout.this.model = newModel;
                if (CarouselLayout.this.itemChangeListener != null) {
                    CarouselLayout.this.itemChangeListener.onCurrentItemChanging(CarouselLayout.this.currentItem, position, ((Model) CarouselLayout.this.model.get(position)).id);
                }
            }

            public void onAnimationEnd(Animator animation) {
                CarouselLayout.this.setCurrentItem(position, false);
                CarouselLayout.this.runQueuedOperation();
            }
        });
        this.animationRunning = true;
        animatorSet.start();
    }

    private void setCurrentItem(int position, boolean callOnCurrentItemChanging) {
        boolean z = true;
        if (position >= 0 && position < this.model.size()) {
            if (this.currentItem != -1) {
                recycleViews();
            }
            this.leftView = addSideView(position - 1, this.viewPadding, position > 0);
            this.middleLeftView = addMiddleLeftView(position, (this.viewPadding + this.sideImageSize) + this.mainLeftPadding, true);
            this.middleRightView = addMiddleRightView(position, ((this.viewPadding + this.sideImageSize) + this.mainLeftPadding) + this.mainImageSize, true);
            int i = position + 1;
            int i2 = (this.viewPadding + this.sideImageSize) + this.rightImageStart;
            if (position + 1 >= this.model.size()) {
                z = false;
            }
            this.rightView = addSideView(i, i2, z);
            if (callOnCurrentItemChanging && this.itemChangeListener != null) {
                this.itemChangeListener.onCurrentItemChanging(this.currentItem, position, ((Model) this.model.get(position)).id);
            }
            this.currentItem = position;
            if (this.carouselIndicator != null) {
                this.carouselIndicator.setCurrentItem(this.currentItem);
            }
            if (this.itemChangeListener != null) {
                this.itemChangeListener.onCurrentItemChanged(this.currentItem, ((Model) this.model.get(position)).id);
            }
            if (this.middleLeftView != null && this.middleLeftView.getVisibility() == 0) {
                this.selectedItemView = this.middleLeftView;
            }
        }
    }

    public void setCurrentItem(int position) {
        setCurrentItem(position, true);
    }

    public void movePrevious(AnimatorListener listener) {
        move(listener, false, false, false, this.animationDuration);
    }

    public void moveNext(AnimatorListener listener) {
        move(listener, true, false, false, this.animationDuration);
    }

    private void move(AnimatorListener listener, boolean next, boolean keyPress, boolean ignoreAnimationRunning, int duration) {
        Operation operation;
        if (next) {
            operation = Operation.FORWARD;
        } else {
            operation = Operation.BACK;
        }
        if (ignoreAnimationRunning || !this.animationRunning) {
            int newPos = this.currentItem + (next ? 1 : -1);
            if (this.currentItem < 0 || newPos < 0 || newPos >= getCount()) {
                if (sLogger.isLoggable(2)) {
                    sLogger.v("cannot go " + (next ? "next" : "prev"));
                }
                runQueuedOperation();
                return;
            }
            this.lastScrollAnimationOperation = operation;
            AnimatorSet animatorSet;
            if (this.animator instanceof FastScrollAnimator) {
                animatorSet = this.animator.buildLayoutAnimation(listener, this, this.currentItem, newPos);
                this.animationRunning = true;
                if (sLogger.isLoggable(2)) {
                    sLogger.v("move:" + operation.name() + " duration =" + animatorSet.getDuration());
                }
                animatorSet.start();
            } else {
                animatorSet = buildLayoutAnimation(listener, this.currentItem, newPos);
                animatorSet.setDuration((long) duration);
                this.animationRunning = true;
                if (sLogger.isLoggable(2)) {
                    sLogger.v("move:" + operation.name() + " duration =" + animatorSet.getDuration());
                }
                animatorSet.start();
            }
            SoundUtils.playSound(Sound.MENU_MOVE);
        } else if (this.fastScrollAnimation || this.operationQueue.size() < 3) {
            if (this.fastScrollAnimation) {
                if (this.operationQueue.size() > 0) {
                    OperationInfo last = (OperationInfo) this.operationQueue.peek();
                    if (last.type == operation) {
                        last.count++;
                        return;
                    }
                } else if (this.lastScrollAnimationOperation == operation) {
                    duration = 50;
                }
            }
            final OperationInfo operationInfo = new OperationInfo(operation, null, duration);
            final AnimatorListener animatorListener = listener;
            final boolean z = next;
            final boolean z2 = keyPress;
            operationInfo.runnable = new Runnable() {
                public void run() {
                    CarouselLayout.this.move(animatorListener, z, z2, true, operationInfo.animationDuration);
                }
            };
            this.operationQueue.add(operationInfo);
        }
    }

    public void insert(AnimatorListener listener, int pos, Model item, boolean preserveCurrentItem) {
        if (this.animationRunning) {
            final AnimatorListener animatorListener = listener;
            final int i = pos;
            final Model model = item;
            final boolean z = preserveCurrentItem;
            this.operationQueue.add(new OperationInfo(Operation.OTHER, new Runnable() {
                public void run() {
                    CarouselLayout.this.doInsert(animatorListener, i, model, z);
                }
            }));
            return;
        }
        doInsert(listener, pos, item, preserveCurrentItem);
    }

    private void doInsert(AnimatorListener listener, int pos, Model item, boolean preserveCurrentItem) {
        this.model.add(pos, item);
        AnimatorSet animatorSet = new AnimatorSet();
        Collection<Animator> animatorCollection = new ArrayList();
        int previous = this.currentItem;
        Direction direction = Direction.RIGHT;
        int leftX = this.viewPadding;
        int rightX = (this.viewPadding + this.sideImageSize) + this.rightImageStart;
        int middleLeftX = (this.viewPadding + this.sideImageSize) + this.mainLeftPadding;
        int middleRightX = ((this.viewPadding + this.sideImageSize) + this.mainLeftPadding) + this.mainImageSize;
        if (pos <= this.currentItem + 1) {
            if (pos == this.currentItem + 1) {
                this.newRightView = addSideView(pos, rightX, false);
                direction = Direction.RIGHT;
                animatorCollection.add(this.animator.createViewOutAnimation(this, direction));
            } else if (pos == this.currentItem) {
                if (preserveCurrentItem) {
                    this.newLeftView = addSideView(pos, leftX, false);
                    direction = Direction.LEFT;
                    animatorCollection.add(this.animator.createViewOutAnimation(this, direction));
                    this.currentItem++;
                } else {
                    this.newMiddleLeftView = addMiddleLeftView(pos, middleLeftX, false);
                    this.newMiddleRightView = addMiddleRightView(pos, middleRightX, false);
                    direction = Direction.RIGHT;
                    if (this.rightView != null) {
                        animatorCollection.add(this.animator.createViewOutAnimation(this, direction));
                    }
                    animatorCollection.add(this.animator.createMiddleLeftViewAnimation(this, direction));
                    animatorCollection.add(this.animator.createMiddleRightViewAnimation(this, direction));
                }
            } else if (pos <= this.currentItem - 1) {
                if (preserveCurrentItem) {
                    this.currentItem++;
                } else {
                    move(listener, true, true, false, this.animationDuration);
                    return;
                }
            }
        }
        animatorSet.addListener(buildListener(listener, direction == Direction.LEFT, previous, this.currentItem));
        animatorSet.playTogether(animatorCollection);
        this.animationRunning = true;
        animatorSet.start();
    }

    private AnimatorSet buildLayoutAnimation(AnimatorListener listener, int pos, int newPos) {
        AnimatorSet animatorSet = new AnimatorSet();
        boolean next = newPos > pos;
        Direction direction = next ? Direction.LEFT : Direction.RIGHT;
        int offset = this.mainViewDividerPadding + this.sideImageSize;
        if (next) {
            this.newMiddleRightView = addMiddleRightView(newPos, ((int) this.rightView.getX()) + offset, false);
            this.newRightView = addHiddenView(newPos + 1, Direction.RIGHT, newPos + 1 < this.model.size());
        } else {
            this.newLeftView = addHiddenView(newPos - 1, Direction.LEFT, newPos > 0);
            this.newMiddleRightView = addMiddleRightView(newPos, ((int) this.leftView.getX()) + offset, false);
            this.newMiddleRightView.setAlpha(0.0f);
        }
        Builder builder = animatorSet.play(this.animator.createViewOutAnimation(this, direction));
        builder.with(this.animator.createMiddleLeftViewAnimation(this, direction));
        builder.with(this.animator.createMiddleRightViewAnimation(this, direction));
        builder.with(this.animator.createSideViewToMiddleAnimation(this, direction));
        builder.with(this.animator.createNewMiddleRightViewAnimation(this, direction));
        builder.with(this.animator.createHiddenViewAnimation(this, direction));
        if (this.carouselIndicator != null) {
            AnimatorSet indicatorAnimator = this.carouselIndicator.getItemMoveAnimator(newPos, -1);
            if (indicatorAnimator != null) {
                builder.with(indicatorAnimator);
            }
        }
        animatorSet.setInterpolator(this.interpolator);
        animatorSet.addListener(buildListener(listener, next, this.currentItem, newPos));
        return animatorSet;
    }

    private AnimatorListener buildListener(AnimatorListener listener, boolean next, int oldPos, int newPos) {
        final AnimatorListener animatorListener = listener;
        final int i = oldPos;
        final int i2 = newPos;
        final boolean z = next;
        return new DefaultAnimationListener() {
            public void onAnimationStart(Animator animation) {
                if (animatorListener != null) {
                    animatorListener.onAnimationStart(animation);
                }
                if (CarouselLayout.this.newMiddleRightView != null) {
                    CarouselLayout.this.newMiddleRightView.setVisibility(0);
                }
                if (CarouselLayout.this.itemChangeListener != null && i != i2) {
                    CarouselLayout.this.itemChangeListener.onCurrentItemChanging(i, i2, ((Model) CarouselLayout.this.model.get(i2)).id);
                }
            }

            public void onAnimationEnd(Animator animation) {
                CarouselLayout.this.currentItem = i2;
                if (animatorListener != null) {
                    animatorListener.onAnimationEnd(animation);
                }
                CarouselLayout.this.changeSelectedItem(z, true);
                if (z) {
                    if (CarouselLayout.this.leftView != null) {
                        CarouselLayout.this.recycleView(ViewType.SIDE, CarouselLayout.this.leftView);
                        CarouselLayout.this.leftView = null;
                    }
                    CarouselLayout.this.leftView = CarouselLayout.this.middleLeftView;
                    CarouselLayout.this.middleLeftView = CarouselLayout.this.rightView;
                    CarouselLayout.this.rightView = CarouselLayout.this.newRightView;
                } else {
                    if (CarouselLayout.this.rightView != null) {
                        CarouselLayout.this.recycleView(ViewType.SIDE, CarouselLayout.this.rightView);
                        CarouselLayout.this.rightView = null;
                    }
                    CarouselLayout.this.rightView = CarouselLayout.this.middleLeftView;
                    CarouselLayout.this.middleLeftView = CarouselLayout.this.newMiddleLeftView != null ? CarouselLayout.this.newMiddleLeftView : CarouselLayout.this.leftView;
                    CarouselLayout.this.leftView = CarouselLayout.this.newLeftView;
                }
                if (CarouselLayout.this.newMiddleRightView != null) {
                    CarouselLayout.this.recycleView(ViewType.MIDDLE_RIGHT, CarouselLayout.this.middleRightView);
                    CarouselLayout.this.middleRightView = CarouselLayout.this.newMiddleRightView;
                    CarouselLayout.this.newMiddleRightView = null;
                }
                CarouselLayout.this.newMiddleLeftView = null;
                CarouselLayout.this.newLeftView = null;
                CarouselLayout.this.newRightView = null;
                if (CarouselLayout.this.leftView == null) {
                    CarouselLayout.this.leftView = CarouselLayout.this.addSideView(i2 - 1, CarouselLayout.this.viewPadding, false);
                }
                if (CarouselLayout.this.rightView == null) {
                    CarouselLayout.this.rightView = CarouselLayout.this.addSideView(i2 + 1, (CarouselLayout.this.viewPadding + CarouselLayout.this.sideImageSize) + CarouselLayout.this.rightImageStart, false);
                }
                if (CarouselLayout.this.carouselIndicator != null) {
                    CarouselLayout.this.carouselIndicator.setCurrentItem(CarouselLayout.this.currentItem);
                }
                if (CarouselLayout.this.itemChangeListener != null) {
                    CarouselLayout.this.itemChangeListener.onCurrentItemChanged(CarouselLayout.this.currentItem, ((Model) CarouselLayout.this.model.get(CarouselLayout.this.currentItem)).id);
                }
                CarouselLayout.this.runQueuedOperation();
            }
        };
    }

    private void recycleViews() {
        recycleView(ViewType.SIDE, this.leftView);
        this.leftView = null;
        recycleView(ViewType.SIDE, this.rightView);
        this.rightView = null;
        recycleView(ViewType.MIDDLE_LEFT, this.middleLeftView);
        this.middleLeftView = null;
        recycleView(ViewType.MIDDLE_RIGHT, this.middleRightView);
        this.middleRightView = null;
    }

    private void recycleView(ViewType viewType, View view) {
        if (view != null) {
            removeView(view);
            view.setScaleX(1.0f);
            view.setScaleY(1.0f);
            view.setVisibility(0);
            view.setTranslationX(0.0f);
            view.setTranslationY(0.0f);
            view.setAlpha(1.0f);
            if (view instanceof CrossFadeImageView) {
                CrossFadeImageView crossfadeImageView = (CrossFadeImageView) view;
                ImageView imageView = (ImageView) crossfadeImageView.getBig();
                imageView.setImageResource(0);
                imageView.setPivotX(0.0f);
                imageView.setPivotY(0.0f);
                if (this.viewsScaled && (imageView instanceof InitialsImageView)) {
                    ((InitialsImageView) imageView).setScaled(true);
                }
                imageView = (ImageView) crossfadeImageView.getSmall();
                imageView.setImageResource(0);
                imageView.setPivotX(0.0f);
                imageView.setPivotY(0.0f);
                if (this.viewsScaled && (imageView instanceof InitialsImageView)) {
                    ((InitialsImageView) imageView).setScaled(true);
                }
            }
            this.viewCacheManager.putView(viewType, view);
        }
    }

    private void changeSelectedItem(boolean next, boolean keyPress) {
        if (this.selectedItemView != null) {
            if (next) {
                this.selectedItemView = this.rightView;
            } else {
                this.selectedItemView = this.leftView;
            }
        }
    }

    public void selectItem() {
        if (this.animationRunning) {
            Runnable runnable = new Runnable() {
                public void run() {
                    CarouselLayout.this.animationRunning = false;
                    CarouselLayout.this.selectItem();
                }
            };
            sLogger.w("queueing up select event");
            this.operationQueue.add(new OperationInfo(Operation.SELECT, runnable));
            return;
        }
        clearOperationQueue();
        if (this.selectedItemView == null) {
            sLogger.v("no item selected");
            runQueuedOperation();
        } else if (this.selectedItemView == this.middleLeftView) {
            int id = ((Integer) this.selectedItemView.getTag(R.id.item_id)).intValue();
            SoundUtils.playSound(Sound.MENU_SELECT);
            if (this.itemChangeListener != null) {
                sLogger.v("execute item:" + id);
                this.itemChangeListener.onExecuteItem(id, this.currentItem);
                return;
            }
            sLogger.v("no carousel listener");
            runQueuedOperation();
        } else {
            sLogger.v("no match");
            runQueuedOperation();
        }
    }

    View buildView(int item, int xPos, ViewType viewType, boolean visible) {
        int w = 0;
        int h = 0;
        int layoutId = 0;
        switch (viewType) {
            case SIDE:
                h = this.sideImageSize;
                w = this.sideImageSize;
                layoutId = this.imageLytResourceId;
                break;
            case MIDDLE_LEFT:
                h = this.mainImageSize;
                w = this.mainImageSize;
                layoutId = this.imageLytResourceId;
                break;
            case MIDDLE_RIGHT:
                h = this.rightSectionHeight;
                w = this.rightSectionWidth;
                layoutId = this.infoLayoutResourceId;
                break;
        }
        LayoutParams lytParams = new LayoutParams(w, h);
        lytParams.gravity = 16;
        View convertView = this.viewCacheManager.getView(viewType);
        View view = this.carouselAdapter.getView(item, convertView, viewType, layoutId, w);
        if (convertView == null && this.viewsScaled && viewType == ViewType.SIDE && (view instanceof CrossFadeImageView)) {
            CrossFadeImageView crossFadeImageView = (CrossFadeImageView) view;
            View big = crossFadeImageView.getBig();
            if (big instanceof InitialsImageView) {
                ((InitialsImageView) big).setScaled(true);
            }
            if (crossFadeImageView.getSmall() instanceof InitialsImageView) {
                ((InitialsImageView) big).setScaled(true);
            }
        }
        view.setX((float) xPos);
        view.setVisibility(visible ? 0 : 4);
        addView(view, lytParams);
        return view;
    }

    View addSideView(int item, int xPos, boolean visible) {
        return buildView(item, xPos, ViewType.SIDE, visible);
    }

    View addMiddleLeftView(int item, int xPos, boolean visible) {
        return buildView(item, xPos, ViewType.MIDDLE_LEFT, visible);
    }

    View addMiddleRightView(int item, int xPos, boolean visible) {
        return buildView(item, xPos, ViewType.MIDDLE_RIGHT, visible);
    }

    View addHiddenView(int item, Direction direction, boolean visible) {
        float x;
        if (direction == Direction.RIGHT) {
            x = (((this.rightView.getX() + ((float) this.mainImageSize)) + ((float) this.mainViewDividerPadding)) + ((float) this.rightSectionWidth)) + ((float) this.mainRightPadding);
        } else {
            x = -(this.leftView.getX() + ((float) this.leftView.getMeasuredWidth()));
        }
        return addSideView(item, (int) x, visible);
    }

    public void onClick() {
    }

    public void onTrackHand(float normalized) {
    }

    public boolean onGesture(GestureEvent event) {
        if (this.currentItem == -1) {
            return false;
        }
        this.detector.onGesture(event);
        switch (event.gesture) {
        }
        return true;
    }

    public boolean onKey(CustomKeyEvent event) {
        if (this.multipleClickGestureDetector != null) {
            return this.multipleClickGestureDetector.onKey(event);
        }
        return handleKey(event);
    }

    private boolean handleKey(CustomKeyEvent event) {
        if (this.currentItem == -1) {
            return true;
        }
        switch (event) {
            case LEFT:
                move(null, false, true, false, this.animationDuration);
                return true;
            case RIGHT:
                move(null, true, true, false, this.animationDuration);
                return true;
            case SELECT:
                selectItem();
                return true;
            default:
                return false;
        }
    }

    public IInputHandler nextHandler() {
        return null;
    }

    void runQueuedOperation() {
        if (this.operationQueue.size() > 0) {
            OperationInfo operationInfo = (OperationInfo) this.operationQueue.peek();
            if (operationInfo.count == 1) {
                this.operationQueue.remove();
            } else {
                operationInfo.count--;
            }
            this.handler.post(operationInfo.runnable);
            return;
        }
        if (this.animator instanceof FastScrollAnimator) {
            FastScrollAnimator fastScrollAnimator = this.animator;
            if (fastScrollAnimator.isEndPending()) {
                fastScrollAnimator.endAnimation();
                return;
            }
        }
        this.lastScrollAnimationOperation = null;
        this.animationRunning = false;
    }

    public void clearOperationQueue() {
        this.operationQueue.clear();
    }

    public void notifyDatasetChanged(List<Model> list, int selectedItem) {
        this.model = list;
        setCurrentItem(selectedItem, true);
    }

    public void reload() {
        setCurrentItem(this.currentItem, true);
    }

    boolean isAnimationPending() {
        if (this.operationQueue.size() <= 0 || ((OperationInfo) this.operationQueue.peek()).type != this.lastScrollAnimationOperation) {
            return false;
        }
        return true;
    }

    public boolean isAnimationEndPending() {
        if (this.animator instanceof FastScrollAnimator) {
            return this.animator.isEndPending();
        }
        return false;
    }
}
