package com.navdy.hud.app.ui.component.destination;

import butterknife.ButterKnife.Finder;
import com.navdy.hud.app.R;
import com.navdy.hud.app.ui.component.ConfirmationLayout;

public class DestinationPickerView$$ViewInjector {
    public static void inject(Finder finder, DestinationPickerView target, Object source) {
        target.rightBackground = finder.findRequiredView(source, R.id.rightBackground, "field 'rightBackground'");
        target.confirmationLayout = (ConfirmationLayout) finder.findRequiredView(source, R.id.confirmationLayout, "field 'confirmationLayout'");
    }

    public static void reset(DestinationPickerView target) {
        target.rightBackground = null;
        target.confirmationLayout = null;
    }
}
