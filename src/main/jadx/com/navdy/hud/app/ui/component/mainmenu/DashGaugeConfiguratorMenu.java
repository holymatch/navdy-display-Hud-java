package com.navdy.hud.app.ui.component.mainmenu;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import com.glympse.android.hal.NotificationListener;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.TelemetryDataManager.DriveScoreUpdated;
import com.navdy.hud.app.event.DriverProfileChanged;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.manager.SpeedManager;
import com.navdy.hud.app.manager.SpeedManager.SpeedUnit;
import com.navdy.hud.app.maps.MapEvents.LocationFix;
import com.navdy.hud.app.maps.MapEvents.ManeuverDisplay;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.obd.ObdManager.ObdPidChangeEvent;
import com.navdy.hud.app.ui.component.homescreen.SmartDashView;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.Reload;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.SmartDashWidgetCache;
import com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager.UserPreferenceChanged;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.Menu;
import com.navdy.hud.app.ui.component.mainmenu.IMenu.MenuLevel;
import com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2.Presenter;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ItemSelectionState;
import com.navdy.hud.app.ui.component.vlist.VerticalList.Model;
import com.navdy.hud.app.ui.component.vlist.VerticalList.ModelState;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import com.navdy.hud.app.ui.component.vlist.viewholder.SwitchViewHolder;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.util.HeadingDataUtil;
import com.navdy.hud.app.view.DashboardWidgetPresenter;
import com.navdy.hud.app.view.DashboardWidgetView;
import com.navdy.obd.PidSet;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u00d2\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 r2\u00020\u0001:\u0002rsB-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u000bJ\u0010\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020<H\u0007J\"\u0010=\u001a\u0004\u0018\u00010\u00012\u0006\u0010\n\u001a\u00020\u00012\u0006\u0010>\u001a\u00020?2\u0006\u0010@\u001a\u00020?H\u0016J\b\u0010A\u001a\u00020\rH\u0016J\u0010\u0010B\u001a\n\u0012\u0004\u0012\u00020D\u0018\u00010CH\u0016J\u0012\u0010E\u001a\u0004\u0018\u00010D2\u0006\u0010F\u001a\u00020\rH\u0016J\n\u0010G\u001a\u0004\u0018\u00010HH\u0016J\n\u0010I\u001a\u0004\u0018\u00010JH\u0016J\b\u0010K\u001a\u00020'H\u0016J\b\u0010L\u001a\u00020'H\u0016J\u0018\u0010M\u001a\u00020'2\u0006\u0010N\u001a\u00020\r2\u0006\u0010F\u001a\u00020\rH\u0016J(\u0010O\u001a\u00020:2\u0006\u0010P\u001a\u00020D2\u0006\u0010Q\u001a\u00020R2\u0006\u0010F\u001a\u00020\r2\u0006\u0010S\u001a\u00020TH\u0016J\u0010\u0010U\u001a\u00020:2\u0006\u0010V\u001a\u00020WH\u0007J\u0010\u0010X\u001a\u00020:2\u0006\u0010Y\u001a\u00020ZH\u0007J\b\u0010[\u001a\u00020:H\u0016J\b\u0010\\\u001a\u00020:H\u0016J\u0010\u0010]\u001a\u00020:2\u0006\u0010^\u001a\u00020_H\u0007J\u0010\u0010`\u001a\u00020:2\u0006\u0010a\u001a\u00020bH\u0016J\u0010\u0010c\u001a\u00020:2\u0006\u0010;\u001a\u00020dH\u0007J\u0010\u0010e\u001a\u00020:2\u0006\u0010;\u001a\u00020fH\u0007J\b\u0010g\u001a\u00020:H\u0016J\u0010\u0010h\u001a\u00020:2\u0006\u0010i\u001a\u00020jH\u0016J\u0010\u0010k\u001a\u00020'2\u0006\u0010a\u001a\u00020bH\u0016J\u0010\u0010l\u001a\u00020:2\u0006\u0010N\u001a\u00020\rH\u0016J\u0010\u0010m\u001a\u00020:2\u0006\u0010n\u001a\u00020\rH\u0016J\b\u0010o\u001a\u00020:H\u0016J\u000e\u0010p\u001a\u00020:2\u0006\u0010F\u001a\u00020\rJ\b\u0010q\u001a\u00020:H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u0010@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0016\u001a\u00020\u0017X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR\u0011\u0010\u001c\u001a\u00020\u001d\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u001a\u0010 \u001a\u00020!X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\u000e\u0010\n\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010&\u001a\u00020'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010,\u001a\b\u0018\u00010-R\u00020.X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R\u0011\u00103\u001a\u00020.\u00a2\u0006\b\n\u0000\u001a\u0004\b4\u00105R\u001a\u00106\u001a\u00020'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u0010)\"\u0004\b8\u0010+R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006t"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;", "bus", "Lcom/squareup/otto/Bus;", "sharedPreferences", "Landroid/content/SharedPreferences;", "vscrollComponent", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;", "presenter", "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;", "parent", "(Lcom/squareup/otto/Bus;Landroid/content/SharedPreferences;Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;)V", "backSelection", "", "backSelectionId", "value", "Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;", "gaugeType", "getGaugeType", "()Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;", "setGaugeType", "(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;)V", "gaugeView", "Lcom/navdy/hud/app/view/DashboardWidgetView;", "getGaugeView", "()Lcom/navdy/hud/app/view/DashboardWidgetView;", "setGaugeView", "(Lcom/navdy/hud/app/view/DashboardWidgetView;)V", "gaugeViewContainer", "Landroid/widget/FrameLayout;", "getGaugeViewContainer", "()Landroid/widget/FrameLayout;", "headingDataUtil", "Lcom/navdy/hud/app/util/HeadingDataUtil;", "getHeadingDataUtil", "()Lcom/navdy/hud/app/util/HeadingDataUtil;", "setHeadingDataUtil", "(Lcom/navdy/hud/app/util/HeadingDataUtil;)V", "registered", "", "getRegistered", "()Z", "setRegistered", "(Z)V", "smartDashWidgetCache", "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;", "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;", "getSmartDashWidgetCache", "()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;", "setSmartDashWidgetCache", "(Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$SmartDashWidgetCache;)V", "smartDashWidgetManager", "getSmartDashWidgetManager", "()Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager;", "userPreferenceChanged", "getUserPreferenceChanged", "setUserPreferenceChanged", "ObdPidChangeEvent", "", "event", "Lcom/navdy/hud/app/obd/ObdManager$ObdPidChangeEvent;", "getChildMenu", "args", "", "path", "getInitialSelection", "getItems", "", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getModelfromPos", "pos", "getScrollIndex", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;", "getType", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;", "isBindCallsEnabled", "isFirstItemEmpty", "isItemClickable", "id", "onBindToView", "model", "view", "Landroid/view/View;", "state", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "onDriveScoreUpdated", "driveScoreUpdated", "Lcom/navdy/hud/app/analytics/TelemetryDataManager$DriveScoreUpdated;", "onDriverProfileChanged", "profileChanged", "Lcom/navdy/hud/app/event/DriverProfileChanged;", "onFastScrollEnd", "onFastScrollStart", "onGpsLocationChanged", "location", "Landroid/location/Location;", "onItemSelected", "selection", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;", "onLocationFixChangeEvent", "Lcom/navdy/hud/app/maps/MapEvents$LocationFix;", "onMapEvent", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "onScrollIdle", "onUnload", "level", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;", "selectItem", "setBackSelectionId", "setBackSelectionPos", "n", "setSelectedIcon", "showGauge", "showToolTip", "Companion", "GaugeType", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
/* compiled from: DashGaugeConfiguratorMenu.kt */
public final class DashGaugeConfiguratorMenu implements IMenu {
    public static final Companion Companion = new Companion();
    private static final Model back;
    private static final int backColor;
    private static final int bkColorUnselected;
    private static final int centerGaugeBackgroundColor;
    private static final ArrayList<Model> centerGaugeOptionsList = new ArrayList();
    private static final String centerGaugeTitle;
    private static final String offLabel;
    private static final String onLabel;
    private static final Logger sLogger = new Logger(Companion.getClass());
    private static final ArrayList<Model> sideGaugesOptionsList = new ArrayList();
    private static final String sideGaugesTitle;
    private static final Model speedoMeter;
    private static final Model tachoMeter;
    private static final UIStateManager uiStateManager;
    private static final String unavailableLabel;
    private int backSelection;
    private int backSelectionId;
    private final Bus bus;
    @NotNull
    private GaugeType gaugeType;
    @NotNull
    private DashboardWidgetView gaugeView;
    @NotNull
    private final FrameLayout gaugeViewContainer;
    @NotNull
    private HeadingDataUtil headingDataUtil = new HeadingDataUtil();
    private final IMenu parent;
    private final Presenter presenter;
    private boolean registered;
    private final SharedPreferences sharedPreferences;
    @Nullable
    private SmartDashWidgetCache smartDashWidgetCache;
    @NotNull
    private final SmartDashWidgetManager smartDashWidgetManager;
    private boolean userPreferenceChanged;
    private final VerticalMenuComponent vscrollComponent;

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000\u0019\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"}, d2 = {"com/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$2", "", "(Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu;)V", "onReload", "", "reload", "Lcom/navdy/hud/app/ui/component/homescreen/SmartDashWidgetManager$Reload;", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: DashGaugeConfiguratorMenu.kt */
    /* renamed from: com.navdy.hud.app.ui.component.mainmenu.DashGaugeConfiguratorMenu$2 */
    public static final class AnonymousClass2 {
        final /* synthetic */ DashGaugeConfiguratorMenu this$0;

        AnonymousClass2(DashGaugeConfiguratorMenu $outer) {
            this.this$0 = $outer;
        }

        @Subscribe
        public final void onReload(@NotNull Reload reload) {
            Intrinsics.checkParameterIsNotNull(reload, "reload");
            switch (WhenMappings.$EnumSwitchMapping$0[reload.ordinal()]) {
                case 1:
                    this.this$0.presenter.updateCurrentMenu(this.this$0);
                    return;
                case 2:
                    this.this$0.setSmartDashWidgetCache(this.this$0.getSmartDashWidgetManager().buildSmartDashWidgetCache(0));
                    this.this$0.presenter.updateCurrentMenu(this.this$0);
                    return;
                default:
                    return;
            }
        }
    }

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0014\u0010\r\u001a\u00020\bX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\nR\u001a\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0010X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0016R\u0014\u0010\u0019\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0016R\u0014\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0010X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u0012R\u0014\u0010!\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0016R\u0014\u0010#\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0006R\u0014\u0010%\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\u0006R\u0014\u0010'\u001a\u00020(X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010*R\u0014\u0010+\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b,\u0010\u0016\u00a8\u0006-"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$Companion;", "", "()V", "back", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "getBack", "()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "backColor", "", "getBackColor", "()I", "bkColorUnselected", "getBkColorUnselected", "centerGaugeBackgroundColor", "getCenterGaugeBackgroundColor", "centerGaugeOptionsList", "Ljava/util/ArrayList;", "getCenterGaugeOptionsList", "()Ljava/util/ArrayList;", "centerGaugeTitle", "", "getCenterGaugeTitle", "()Ljava/lang/String;", "offLabel", "getOffLabel", "onLabel", "getOnLabel", "sLogger", "Lcom/navdy/service/library/log/Logger;", "getSLogger", "()Lcom/navdy/service/library/log/Logger;", "sideGaugesOptionsList", "getSideGaugesOptionsList", "sideGaugesTitle", "getSideGaugesTitle", "speedoMeter", "getSpeedoMeter", "tachoMeter", "getTachoMeter", "uiStateManager", "Lcom/navdy/hud/app/ui/framework/UIStateManager;", "getUiStateManager", "()Lcom/navdy/hud/app/ui/framework/UIStateManager;", "unavailableLabel", "getUnavailableLabel", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: DashGaugeConfiguratorMenu.kt */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker $constructor_marker) {
            this();
        }

        private final Model getBack() {
            return DashGaugeConfiguratorMenu.back;
        }

        private final Model getTachoMeter() {
            return DashGaugeConfiguratorMenu.tachoMeter;
        }

        private final Model getSpeedoMeter() {
            return DashGaugeConfiguratorMenu.speedoMeter;
        }

        private final int getBackColor() {
            return DashGaugeConfiguratorMenu.backColor;
        }

        private final int getBkColorUnselected() {
            return DashGaugeConfiguratorMenu.bkColorUnselected;
        }

        private final ArrayList<Model> getCenterGaugeOptionsList() {
            return DashGaugeConfiguratorMenu.centerGaugeOptionsList;
        }

        private final ArrayList<Model> getSideGaugesOptionsList() {
            return DashGaugeConfiguratorMenu.sideGaugesOptionsList;
        }

        private final Logger getSLogger() {
            return DashGaugeConfiguratorMenu.sLogger;
        }

        private final String getCenterGaugeTitle() {
            return DashGaugeConfiguratorMenu.centerGaugeTitle;
        }

        private final String getSideGaugesTitle() {
            return DashGaugeConfiguratorMenu.sideGaugesTitle;
        }

        private final UIStateManager getUiStateManager() {
            return DashGaugeConfiguratorMenu.uiStateManager;
        }

        private final int getCenterGaugeBackgroundColor() {
            return DashGaugeConfiguratorMenu.centerGaugeBackgroundColor;
        }

        private final String getOnLabel() {
            return DashGaugeConfiguratorMenu.onLabel;
        }

        private final String getOffLabel() {
            return DashGaugeConfiguratorMenu.offLabel;
        }

        private final String getUnavailableLabel() {
            return DashGaugeConfiguratorMenu.unavailableLabel;
        }
    }

    @Metadata(bv = {1, 0, 1}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/navdy/hud/app/ui/component/mainmenu/DashGaugeConfiguratorMenu$GaugeType;", "", "(Ljava/lang/String;I)V", "CENTER", "SIDE", "app_hudRelease"}, k = 1, mv = {1, 1, 6})
    /* compiled from: DashGaugeConfiguratorMenu.kt */
    public enum GaugeType {
    }

    @Metadata(bv = {1, 0, 1}, k = 3, mv = {1, 1, 6})
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0 = new int[Reload.values().length];

        static {
            $EnumSwitchMapping$0[Reload.RELOADED.ordinal()] = 1;
            $EnumSwitchMapping$0[Reload.RELOAD_CACHE.ordinal()] = 2;
            $EnumSwitchMapping$1 = new int[GaugeType.values().length];
            $EnumSwitchMapping$1[GaugeType.CENTER.ordinal()] = 1;
            $EnumSwitchMapping$1[GaugeType.SIDE.ordinal()] = 2;
            $EnumSwitchMapping$2 = new int[GaugeType.values().length];
            $EnumSwitchMapping$2[GaugeType.CENTER.ordinal()] = 1;
            $EnumSwitchMapping$2[GaugeType.SIDE.ordinal()] = 2;
            $EnumSwitchMapping$3 = new int[GaugeType.values().length];
            $EnumSwitchMapping$3[GaugeType.CENTER.ordinal()] = 1;
            $EnumSwitchMapping$3[GaugeType.SIDE.ordinal()] = 2;
            $EnumSwitchMapping$4 = new int[GaugeType.values().length];
            $EnumSwitchMapping$4[GaugeType.CENTER.ordinal()] = 1;
            $EnumSwitchMapping$4[GaugeType.SIDE.ordinal()] = 2;
        }
    }

    public DashGaugeConfiguratorMenu(@NotNull Bus bus, @NotNull SharedPreferences sharedPreferences, @NotNull VerticalMenuComponent vscrollComponent, @NotNull Presenter presenter, @NotNull IMenu parent) {
        Intrinsics.checkParameterIsNotNull(bus, "bus");
        Intrinsics.checkParameterIsNotNull(sharedPreferences, "sharedPreferences");
        Intrinsics.checkParameterIsNotNull(vscrollComponent, "vscrollComponent");
        Intrinsics.checkParameterIsNotNull(presenter, "presenter");
        Intrinsics.checkParameterIsNotNull(parent, "parent");
        this.bus = bus;
        this.sharedPreferences = sharedPreferences;
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
        FrameLayout frameLayout = this.vscrollComponent.selectedCustomView;
        Intrinsics.checkExpressionValueIsNotNull(frameLayout, "vscrollComponent.selectedCustomView");
        this.gaugeViewContainer = frameLayout;
        this.gaugeView = new DashboardWidgetView(this.gaugeViewContainer.getContext());
        this.gaugeView.setLayoutParams(new LayoutParams(-1, -1));
        this.gaugeViewContainer.removeAllViews();
        this.gaugeViewContainer.addView(this.gaugeView);
        this.smartDashWidgetManager = new SmartDashWidgetManager(this.sharedPreferences, HudApplication.getAppContext());
        this.smartDashWidgetManager.onResume();
        this.smartDashWidgetManager.setFilter(AnonymousClass1.INSTANCE);
        this.smartDashWidgetManager.reLoadAvailableWidgets(true);
        this.smartDashWidgetManager.registerForChanges(new AnonymousClass2(this));
        this.gaugeType = GaugeType.CENTER;
    }

    @NotNull
    public final SmartDashWidgetManager getSmartDashWidgetManager() {
        return this.smartDashWidgetManager;
    }

    @Nullable
    public final SmartDashWidgetCache getSmartDashWidgetCache() {
        return this.smartDashWidgetCache;
    }

    public final void setSmartDashWidgetCache(@Nullable SmartDashWidgetCache <set-?>) {
        this.smartDashWidgetCache = <set-?>;
    }

    @NotNull
    public final FrameLayout getGaugeViewContainer() {
        return this.gaugeViewContainer;
    }

    @NotNull
    public final DashboardWidgetView getGaugeView() {
        return this.gaugeView;
    }

    public final void setGaugeView(@NotNull DashboardWidgetView <set-?>) {
        Intrinsics.checkParameterIsNotNull(<set-?>, "<set-?>");
        this.gaugeView = <set-?>;
    }

    @NotNull
    public final HeadingDataUtil getHeadingDataUtil() {
        return this.headingDataUtil;
    }

    public final void setHeadingDataUtil(@NotNull HeadingDataUtil <set-?>) {
        Intrinsics.checkParameterIsNotNull(<set-?>, "<set-?>");
        this.headingDataUtil = <set-?>;
    }

    public final boolean getUserPreferenceChanged() {
        return this.userPreferenceChanged;
    }

    public final void setUserPreferenceChanged(boolean <set-?>) {
        this.userPreferenceChanged = <set-?>;
    }

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        backColor = resources.getColor(R.color.mm_back);
        bkColorUnselected = resources.getColor(R.color.icon_bk_color_unselected);
        CharSequence text = resources.getText(R.string.carousel_menu_smartdash_select_center_gauge);
        if (text == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        centerGaugeTitle = (String) text;
        text = resources.getText(R.string.carousel_menu_smartdash_side_gauges);
        if (text == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        sideGaugesTitle = (String) text;
        text = resources.getText(R.string.on);
        if (text == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        onLabel = (String) text;
        text = resources.getText(R.string.off);
        if (text == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        offLabel = (String) text;
        text = resources.getText(R.string.unavailable);
        if (text == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
        }
        unavailableLabel = (String) text;
        UIStateManager uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
        Intrinsics.checkExpressionValueIsNotNull(uiStateManager, "remoteDeviceManager.uiStateManager");
        uiStateManager = uiStateManager;
        centerGaugeBackgroundColor = resources.getColor(R.color.mm_options_scroll_gauge);
        int fluctuatorColor = Companion.getBackColor();
        Model buildModel = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, fluctuatorColor, MainMenu.bkColorUnselected, fluctuatorColor, resources.getString(R.string.back), null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)");
        back = buildModel;
        String title = resources.getString(R.string.tachometer);
        buildModel = IconBkColorViewHolder.buildModel(R.id.main_menu_options_tachometer, R.drawable.icon_options_dash_tachometer_2, -16777216, MainMenu.bkColorUnselected, resources.getColor(R.color.mm_options_tachometer), title, null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)");
        tachoMeter = buildModel;
        title = resources.getString(R.string.speedometer);
        buildModel = IconBkColorViewHolder.buildModel(R.id.main_menu_options_speedometer, R.drawable.icon_options_dash_speedometer_2, -16777216, MainMenu.bkColorUnselected, resources.getColor(R.color.mm_options_speedometer), title, null);
        Intrinsics.checkExpressionValueIsNotNull(buildModel, "IconBkColorViewHolder.bu\u2026tuatorColor, title, null)");
        speedoMeter = buildModel;
        Companion.getCenterGaugeOptionsList().add(Companion.getBack());
        Companion.getCenterGaugeOptionsList().add(Companion.getTachoMeter());
        Companion.getCenterGaugeOptionsList().add(Companion.getSpeedoMeter());
    }

    public final boolean getRegistered() {
        return this.registered;
    }

    public final void setRegistered(boolean <set-?>) {
        this.registered = <set-?>;
    }

    @NotNull
    public final GaugeType getGaugeType() {
        return this.gaugeType;
    }

    public final void setGaugeType(@NotNull GaugeType value) {
        Intrinsics.checkParameterIsNotNull(value, NotificationListener.INTENT_EXTRA_VALUE);
        this.gaugeType = value;
        if (Intrinsics.areEqual( value, GaugeType.SIDE)) {
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
            }
        } else if (this.registered) {
            this.bus.unregister(this);
            this.registered = false;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    @Nullable
    public List<Model> getItems() {
        switch (this.gaugeType) {
            case CENTER:
                return Companion.getCenterGaugeOptionsList();
            case SIDE:
                this.smartDashWidgetCache = this.smartDashWidgetManager.buildSmartDashWidgetCache(0);
                Companion.getSideGaugesOptionsList().clear();
                Companion.getSideGaugesOptionsList().add(Companion.getBack());
                if (this.smartDashWidgetCache != null) {
                    SmartDashWidgetCache $receiver = this.smartDashWidgetCache;
                    if ($receiver == null) {
                        Intrinsics.throwNpe();
                    }
                    int fluctuatorColor = HudApplication.getAppContext().getResources().getColor(R.color.mm_options_side_gauges_halo);
                    int i = 0;
                    int widgetsCount = $receiver.getWidgetsCount() - 1;
                    if (0 <= widgetsCount) {
                        while (true) {
                            String title;
                            boolean isGaugeEnabled;
                            String subtitle;
                            DashboardWidgetPresenter widgetPresenter = $receiver.getWidgetPresenter(i);
                            if (widgetPresenter != null) {
                                title = widgetPresenter.getWidgetName();
                                break;
                            }
                            title = "";
                            widgetPresenter = $receiver.getWidgetPresenter(i);
                            String identifier = widgetPresenter != null ? widgetPresenter.getWidgetIdentifier() : null;
                            boolean isGaugeOn = this.smartDashWidgetManager.isGaugeOn(identifier);
                            PidSet pidSet = ObdManager.getInstance().getSupportedPids();
                            if (identifier != null) {
                                boolean z;
                                switch (identifier.hashCode()) {
                                    case -1158963060:
                                        if (identifier.equals(SmartDashWidgetManager.MPG_AVG_WIDGET_ID)) {
                                            z = pidSet != null && pidSet.contains(256);
                                            isGaugeEnabled = z;
                                            break;
                                        }
                                        break;
                                    case -131933527:
                                        if (identifier.equals(SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID)) {
                                            z = pidSet != null && pidSet.contains(5);
                                            isGaugeEnabled = z;
                                            break;
                                        }
                                        break;
                                    case 1168400042:
                                        if (identifier.equals(SmartDashWidgetManager.FUEL_GAUGE_ID)) {
                                            z = pidSet != null && pidSet.contains(47);
                                            isGaugeEnabled = z;
                                            break;
                                        }
                                        break;
                                }
                            }
                            isGaugeEnabled = true;
                            if (isGaugeEnabled) {
                                subtitle = isGaugeOn ? Companion.getOnLabel() : Companion.getOffLabel();
                            } else {
                                subtitle = Companion.getUnavailableLabel();
                            }
                            Companion.getSideGaugesOptionsList().add(SwitchViewHolder.Companion.buildModel(i, fluctuatorColor, title, subtitle, isGaugeOn, isGaugeEnabled));
                            if (i != widgetsCount) {
                                i++;
                            }
                        }
                    }
                }
                return Companion.getSideGaugesOptionsList();
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    public int getInitialSelection() {
        return 1;
    }

    @Nullable
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }

    public void setBackSelectionPos(int n) {
        this.backSelection = n;
    }

    public void setBackSelectionId(int id) {
        this.backSelectionId = id;
    }

    public void setSelectedIcon() {
        switch (this.gaugeType) {
            case CENTER:
                this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_center_gauge, Companion.getCenterGaugeBackgroundColor(), null, 1.0f);
                this.vscrollComponent.selectedText.setText(Companion.getCenterGaugeTitle());
                return;
            case SIDE:
                this.vscrollComponent.showSelectedCustomView();
                this.vscrollComponent.selectedText.setText(null);
                if (this.smartDashWidgetCache == null) {
                    getItems();
                }
                showGauge(1);
                return;
            default:
                return;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean selectItem(@NotNull ItemSelectionState selection) {
        Intrinsics.checkParameterIsNotNull(selection, "selection");
        Companion.getSLogger().v("select id:" + selection.id + " pos:" + selection.pos);
        switch (this.gaugeType) {
            case CENTER:
                SmartDashView smartDashView;
                switch (selection.id) {
                    case R.id.main_menu_options_speedometer:
                        this.presenter.close();
                        smartDashView = Companion.getUiStateManager().getSmartDashView();
                        if (smartDashView != null) {
                            smartDashView.onSpeedoMeterSelected();
                            break;
                        }
                        break;
                    case R.id.main_menu_options_tachometer:
                        this.presenter.close();
                        smartDashView = Companion.getUiStateManager().getSmartDashView();
                        if (smartDashView != null) {
                            smartDashView.onTachoMeterSelected();
                            break;
                        }
                        break;
                    case R.id.menu_back:
                        this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                        break;
                }
                break;
            case SIDE:
                switch (selection.id) {
                    case R.id.menu_back:
                        this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
                        break;
                    default:
                        Model model = selection.model;
                        int id = selection.id;
                        SmartDashWidgetCache smartDashWidgetCache = this.smartDashWidgetCache;
                        DashboardWidgetPresenter widgetPresenter = smartDashWidgetCache != null ? smartDashWidgetCache.getWidgetPresenter(id) : null;
                        SmartDashWidgetManager smartDashWidgetManager;
                        String widgetIdentifier;
                        if (model.isOn) {
                            model.isOn = false;
                            model.subTitle = Companion.getOffLabel();
                            smartDashWidgetManager = this.smartDashWidgetManager;
                            if (widgetPresenter != null) {
                                widgetIdentifier = widgetPresenter.getWidgetIdentifier();
                                break;
                            }
                            widgetIdentifier = "";
                            smartDashWidgetManager.setGaugeOn(widgetIdentifier, false);
                        } else {
                            model.isOn = true;
                            model.subTitle = Companion.getOnLabel();
                            smartDashWidgetManager = this.smartDashWidgetManager;
                            if (widgetPresenter != null) {
                                widgetIdentifier = widgetPresenter.getWidgetIdentifier();
                                break;
                            }
                            widgetIdentifier = "";
                            smartDashWidgetManager.setGaugeOn(widgetIdentifier, true);
                        }
                        this.userPreferenceChanged = true;
                        this.presenter.refreshDataforPos(selection.pos);
                        break;
                }
        }
        return false;
    }

    @Nullable
    public Menu getType() {
        return Menu.MAIN_OPTIONS;
    }

    public boolean isItemClickable(int id, int pos) {
        return true;
    }

    @Nullable
    public Model getModelfromPos(int pos) {
        switch (this.gaugeType) {
            case CENTER:
                return (Model) Companion.getCenterGaugeOptionsList().get(pos);
            case SIDE:
                return (Model) Companion.getSideGaugesOptionsList().get(pos);
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    public boolean isBindCallsEnabled() {
        return false;
    }

    public void onBindToView(@NotNull Model model, @NotNull View view, int pos, @NotNull ModelState state) {
        Intrinsics.checkParameterIsNotNull(model, "model");
        Intrinsics.checkParameterIsNotNull(view, "view");
        Intrinsics.checkParameterIsNotNull(state, TransferTable.COLUMN_STATE);
    }

    @Nullable
    public IMenu getChildMenu(@NotNull IMenu parent, @NotNull String args, @NotNull String path) {
        Intrinsics.checkParameterIsNotNull(parent, "parent");
        Intrinsics.checkParameterIsNotNull(args, "args");
        Intrinsics.checkParameterIsNotNull(path, "path");
        return null;
    }

    public void onUnload(@NotNull MenuLevel level) {
        Intrinsics.checkParameterIsNotNull(level, "level");
        DashboardWidgetView dashboardWidgetView = this.gaugeView;
        Object tag = dashboardWidgetView != null ? dashboardWidgetView.getTag() : null;
        if (!(tag instanceof DashboardWidgetPresenter)) {
            tag = null;
        }
        DashboardWidgetPresenter dashboardWidgetPresenter = (DashboardWidgetPresenter) tag;
        if (dashboardWidgetPresenter != null) {
            dashboardWidgetPresenter.setView(null, null);
        }
        if (this.userPreferenceChanged) {
            Companion.getSLogger().d("User preference has changed " + this.userPreferenceChanged);
            this.bus.post(new UserPreferenceChanged());
        } else {
            Companion.getSLogger().d("User preference has not been changed " + this.userPreferenceChanged);
        }
        if (this.registered) {
            this.bus.unregister(this);
            this.registered = false;
        }
    }

    public void onItemSelected(@NotNull ItemSelectionState selection) {
        Intrinsics.checkParameterIsNotNull(selection, "selection");
        if (Intrinsics.areEqual(this.gaugeType, GaugeType.SIDE)) {
            showGauge(selection.pos);
        }
    }

    public final void showGauge(int pos) {
        if (pos > 0) {
            DashboardWidgetPresenter newPresenter;
            this.vscrollComponent.showSelectedCustomView();
            this.vscrollComponent.selectedText.setText(null);
            DashboardWidgetPresenter oldPresenter = this.gaugeView.getTag();
            if (!(oldPresenter instanceof DashboardWidgetPresenter)) {
                oldPresenter = null;
            }
            oldPresenter = oldPresenter;
            SmartDashWidgetCache smartDashWidgetCache = this.smartDashWidgetCache;
            if (smartDashWidgetCache != null) {
                newPresenter = smartDashWidgetCache.getWidgetPresenter(pos - 1);
            } else {
                newPresenter = null;
            }
            if (!(oldPresenter == null || oldPresenter == this.presenter || oldPresenter.getWidgetView() != this.gaugeView)) {
                oldPresenter.setView(null, null);
            }
            if (newPresenter != null) {
                Bundle args = new Bundle();
                args.putInt(DashboardWidgetPresenter.EXTRA_GRAVITY, 0);
                args.putBoolean(DashboardWidgetPresenter.EXTRA_IS_ACTIVE, true);
                newPresenter.setView(this.gaugeView, args);
                newPresenter.setWidgetVisibleToUser(true);
            }
            this.gaugeView.setTag(newPresenter);
            return;
        }
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_side_gauges, Companion.getCenterGaugeBackgroundColor(), null, 1.0f);
        this.vscrollComponent.selectedText.setText(Companion.getSideGaugesTitle());
    }

    @Subscribe
    public final void onMapEvent(@NotNull ManeuverDisplay event) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        this.smartDashWidgetManager.updateWidget(SmartDashWidgetManager.SPEED_LIMIT_SIGN_GAUGE_ID, Integer.valueOf(Math.round((float) SpeedManager.convert((double) event.currentSpeedLimit, SpeedUnit.METERS_PER_SECOND, SpeedManager.getInstance().getSpeedUnit()))));
    }

    @Subscribe
    public final void ObdPidChangeEvent(@NotNull ObdPidChangeEvent event) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        switch (event.pid.getId()) {
            case 5:
                this.smartDashWidgetManager.updateWidget(SmartDashWidgetManager.ENGINE_TEMPERATURE_GAUGE_ID, Double.valueOf(event.pid.getValue()));
                return;
            case 47:
                this.smartDashWidgetManager.updateWidget(SmartDashWidgetManager.FUEL_GAUGE_ID, Double.valueOf(event.pid.getValue()));
                return;
            case 256:
                this.smartDashWidgetManager.updateWidget(SmartDashWidgetManager.MPG_AVG_WIDGET_ID, Double.valueOf(event.pid.getValue()));
                return;
            default:
                return;
        }
    }

    @Subscribe
    public final void onGpsLocationChanged(@NotNull Location location) {
        Intrinsics.checkParameterIsNotNull(location, "location");
        this.headingDataUtil.setHeading((double) location.getBearing());
        this.smartDashWidgetManager.updateWidget(SmartDashWidgetManager.COMPASS_WIDGET_ID, Double.valueOf(this.headingDataUtil.getHeading()));
    }

    @Subscribe
    public final void onLocationFixChangeEvent(@NotNull LocationFix event) {
        Intrinsics.checkParameterIsNotNull(event, "event");
        if (!event.locationAvailable) {
            this.headingDataUtil.reset();
            this.smartDashWidgetManager.updateWidget(SmartDashWidgetManager.COMPASS_WIDGET_ID, Integer.valueOf(0));
        }
    }

    @Subscribe
    public final void onDriverProfileChanged(@NotNull DriverProfileChanged profileChanged) {
        Intrinsics.checkParameterIsNotNull(profileChanged, "profileChanged");
        Companion.getSLogger().d("onDriverProfileChanged, reloading the widgets");
        SmartDashWidgetManager smartDashWidgetManager = this.smartDashWidgetManager;
        if (smartDashWidgetManager != null) {
            smartDashWidgetManager.reLoadAvailableWidgets(false);
        }
    }

    @Subscribe
    public final void onDriveScoreUpdated(@NotNull DriveScoreUpdated driveScoreUpdated) {
        Intrinsics.checkParameterIsNotNull(driveScoreUpdated, "driveScoreUpdated");
        this.smartDashWidgetManager.updateWidget(SmartDashWidgetManager.DRIVE_SCORE_GAUGE_ID, driveScoreUpdated);
    }

    public void onScrollIdle() {
    }

    public void onFastScrollStart() {
    }

    public void onFastScrollEnd() {
    }

    public void showToolTip() {
    }

    public boolean isFirstItemEmpty() {
        return false;
    }
}
