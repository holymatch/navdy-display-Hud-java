package com.navdy.hud.app.event;

import com.navdy.hud.app.analytics.AnalyticsSupport.WakeupReason;

public class Wakeup {
    public WakeupReason reason;

    public Wakeup(WakeupReason reason) {
        this.reason = reason;
    }
}
