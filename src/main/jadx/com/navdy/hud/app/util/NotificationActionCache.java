package com.navdy.hud.app.util;

import com.navdy.service.library.events.notification.NotificationAction;

public class NotificationActionCache {
    private String handler;
    private int lastId = 1;

    public NotificationActionCache(Class handler) {
        this.handler = handler.getName();
    }

    public boolean validAction(NotificationAction action) {
        return action.handler.equals(this.handler);
    }

    public void markComplete(NotificationAction action) {
    }

    public NotificationAction buildAction(int notificationId, int actionId, int labelId) {
        return buildAction(notificationId, actionId, labelId, null);
    }

    public NotificationAction buildAction(int notificationId, int actionId, int labelId, String label) {
        String str = this.handler;
        int i = this.lastId;
        this.lastId = i + 1;
        return new NotificationAction(str, Integer.valueOf(i), Integer.valueOf(notificationId), Integer.valueOf(actionId), Integer.valueOf(labelId), label, null, null);
    }

    public NotificationAction buildAction(int notificationId, int actionId, int iconId, int focusedIconId) {
        String str = this.handler;
        int i = this.lastId;
        this.lastId = i + 1;
        return new NotificationAction(str, Integer.valueOf(i), Integer.valueOf(notificationId), Integer.valueOf(actionId), null, null, Integer.valueOf(iconId), Integer.valueOf(focusedIconId));
    }
}
