package com.navdy.hud.app.util;

import android.os.Build;
import android.util.Log;

public class SerialNumber {
    public static SerialNumber instance = parse(Build.SERIAL);
    public final String configurationCode;
    public final int dayOfWeek;
    public final String factory;
    public final String revisionCode;
    public final String serialNo;
    public final int week;
    public final int year;

    public SerialNumber(String factory, int year, int week, int dayOfWeek, String serialNo, String configuration, String revisionCode) {
        this.factory = factory;
        this.year = year;
        this.week = week;
        this.dayOfWeek = dayOfWeek;
        this.serialNo = serialNo;
        this.configurationCode = configuration;
        this.revisionCode = revisionCode;
    }

    public static SerialNumber parse(String serial) {
        if (serial == null || serial.length() != 16) {
            return new SerialNumber("NDY", 0, 0, 0, "0", "0", "0");
        }
        String factory = serial.substring(0, 3);
        int year = parseNumber(serial.substring(3, 4), 0, "year");
        int week = parseNumber(serial.substring(4, 6), 1, "week");
        if (week < 1 || week > 52) {
            week = 1;
        }
        int dayOfWeek = parseNumber(serial.substring(6, 7), 1, "dayOfWeek");
        if (dayOfWeek < 1 || dayOfWeek > 7) {
            dayOfWeek = 1;
        }
        return new SerialNumber(factory, year, week, dayOfWeek, serial.substring(7, 10), serial.substring(10, 14), serial.substring(14, 15));
    }

    private static int parseNumber(String string, int defaultValue, String fieldName) {
        int result = defaultValue;
        try {
            return Integer.parseInt(string);
        } catch (NumberFormatException e) {
            Log.e("SERIAL", "Invalid serial number field " + fieldName + " value:" + string);
            return result;
        }
    }
}
