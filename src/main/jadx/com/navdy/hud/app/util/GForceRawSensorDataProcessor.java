package com.navdy.hud.app.util;

import android.renderscript.Matrix3f;
import com.navdy.hud.app.device.gps.RawSensorData;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.service.library.log.Logger;
import java.util.Arrays;

public class GForceRawSensorDataProcessor {
    private static Logger logger = new Logger(GForceRawSensorDataProcessor.class);
    private boolean calibrated;
    private Matrix3f rotationMatrix;
    private float[] v = new float[3];
    public float xAccel;
    public float yAccel;
    public float zAccel;

    public void onRawData(RawSensorData sensorData) {
        this.v[0] = sensorData.x;
        this.v[1] = sensorData.y;
        this.v[2] = sensorData.z;
        if (!this.calibrated) {
            float m = magnitude(this.v);
            if (((double) m) > 0.9d && ((double) m) < 1.1d) {
                this.rotationMatrix = calculateRotationMatrix(this.v);
                this.calibrated = true;
            }
        }
        if (this.calibrated) {
            multiply(this.rotationMatrix, this.v);
        }
        this.xAccel = this.v[0];
        this.yAccel = this.v[1];
        this.zAccel = this.v[2];
    }

    private float magnitude(float[] v) {
        return (float) Math.sqrt((double) (((v[0] * v[0]) + (v[1] * v[1])) + (v[2] * v[2])));
    }

    private void multiply(Matrix3f matrix, float[] v) {
        float[] r = matrix.getArray();
        float x = v[0];
        float y = v[1];
        float z = v[2];
        float ry = ((r[3] * x) + (r[4] * y)) + (r[5] * z);
        float rz = ((r[6] * x) + (r[7] * y)) + (r[8] * z);
        v[0] = ((r[0] * x) + (r[1] * y)) + (r[2] * z);
        v[1] = ry;
        v[2] = rz;
    }

    private Matrix3f calculateRotationMatrix(float[] v) {
        Matrix3f result = new Matrix3f();
        float x = v[0];
        float y = v[1];
        float z = v[2];
        double roll = -Math.atan2((double) y, (double) z);
        double pitch = -Math.atan2((double) (-x), Math.sqrt((double) ((y * y) + (z * z))));
        float cr = (float) Math.cos(roll);
        float sr = (float) Math.sin(roll);
        float cp = (float) Math.cos(pitch);
        float sp = (float) Math.sin(pitch);
        float[] ry = new float[]{cp, 0.0f, -sp, 0.0f, 1.0f, 0.0f, sp, 0.0f, cp};
        result.loadMultiply(new Matrix3f(new float[]{1.0f, 0.0f, 0.0f, 0.0f, cr, sr, 0.0f, -sr, cr}), new Matrix3f(ry));
        logger.d("acceleration vector: " + x + HereManeuverDisplayBuilder.COMMA + y + HereManeuverDisplayBuilder.COMMA + z + " derived rotation matrix: " + Arrays.toString(result.getArray()));
        return result;
    }

    public void setCalibrated(boolean calibrated) {
        this.calibrated = calibrated;
    }

    public boolean isCalibrated() {
        return this.calibrated;
    }
}
