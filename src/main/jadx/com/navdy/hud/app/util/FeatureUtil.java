package com.navdy.hud.app.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.text.TextUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.settings.HUDSettings;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.util.HashSet;

public final class FeatureUtil {
    public static final String FEATURE_ACTION = "com.navdy.hud.app.feature";
    public static final String FEATURE_FUEL_ROUTING = "com.navdy.hud.app.feature.fuelRouting";
    public static final String FEATURE_GESTURE_COLLECTOR = "com.navdy.hud.app.feature.gestureCollector";
    public static final String FEATURE_GESTURE_ENGINE = "com.navdy.hud.app.feature.gestureEngine";
    public static final String FEATURE_VOICE_SEARCH_ADDITIONAL_RESULTS_PROPERTY = "persist.voice.search.additional.results";
    public static final String FEATURE_VOICE_SEARCH_LIST_PROPERTY = "persist.sys.voicesearch.list";
    private static final String GESTURE_CALIBRATED_PROPERTY = "persist.sys.swiped.calib";
    private static final Logger sLogger = new Logger(FeatureUtil.class);
    private HashSet<Feature> featuresEnabledMap = new HashSet();
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            try {
                if (FeatureUtil.FEATURE_ACTION.equalsIgnoreCase(intent.getAction())) {
                    final Bundle bundle = intent.getExtras();
                    if (bundle != null && bundle.size() == 1) {
                        TaskManager.getInstance().execute(new Runnable() {
                            public void run() {
                                try {
                                    String featureName = (String) bundle.keySet().iterator().next();
                                    boolean state = bundle.getBoolean(featureName);
                                    Feature feature = FeatureUtil.this.getFeatureFromName(featureName);
                                    if (feature == null) {
                                        FeatureUtil.sLogger.i("invalid feature:" + featureName);
                                        return;
                                    }
                                    if (feature == Feature.GESTURE_ENGINE) {
                                        FeatureUtil.this.sharedPreferences.edit().putBoolean(HUDSettings.GESTURE_ENGINE, state).apply();
                                        FeatureUtil.sLogger.v("gesture engine setting set to " + state);
                                        if (!DeviceUtil.supportsCamera()) {
                                            FeatureUtil.sLogger.v("gesture engine cannot be enabled/disabled, no camera");
                                            return;
                                        }
                                    } else if (feature == Feature.GESTURE_COLLECTOR) {
                                        FeatureUtil.sLogger.v("gesture collector setting set to " + state);
                                        if (!DeviceUtil.supportsCamera()) {
                                            FeatureUtil.sLogger.v("gesture collector cannot be enabled/disabled, no camera");
                                            return;
                                        } else if (!FeatureUtil.this.isGestureCollectorInstalled()) {
                                            return;
                                        }
                                    }
                                    FeatureUtil.this.setFeature(feature, state);
                                } catch (Throwable t) {
                                    FeatureUtil.sLogger.e(t);
                                }
                            }
                        }, 1);
                    }
                }
            } catch (Throwable t) {
                FeatureUtil.sLogger.e(t);
            }
        }
    };
    private SharedPreferences sharedPreferences;

    public enum Feature {
        GESTURE_ENGINE,
        GESTURE_COLLECTOR,
        FUEL_ROUTING,
        GESTURE_PROGRESS
    }

    public FeatureUtil(SharedPreferences sharedPreferences) {
        boolean enabled;
        this.sharedPreferences = sharedPreferences;
        boolean calibrated;
        if (TextUtils.isEmpty(SystemProperties.get(GESTURE_CALIBRATED_PROPERTY, ""))) {
            calibrated = false;
        } else {
            calibrated = true;
        }
        if (sharedPreferences.getBoolean(HUDSettings.GESTURE_ENGINE, true) || calibrated) {
            enabled = true;
        } else {
            enabled = false;
        }
        boolean voiceSearchListResultsEnabled = SystemProperties.getBoolean(FEATURE_VOICE_SEARCH_LIST_PROPERTY, false);
        boolean voiceSearchListAdditionalResultsEnabled = SystemProperties.getBoolean(FEATURE_VOICE_SEARCH_ADDITIONAL_RESULTS_PROPERTY, true);
        if (enabled) {
            this.featuresEnabledMap.add(Feature.GESTURE_ENGINE);
            sLogger.v("enabled gesture engine");
            if (isGestureCollectorInstalled()) {
                this.featuresEnabledMap.add(Feature.GESTURE_COLLECTOR);
                sLogger.v("enabled gesture collector");
            }
        } else {
            sLogger.v("not enabled gesture engine");
        }
        this.featuresEnabledMap.add(Feature.FUEL_ROUTING);
        HudApplication.getAppContext().registerReceiver(this.receiver, new IntentFilter(FEATURE_ACTION));
    }

    public synchronized boolean isFeatureEnabled(Feature feature) {
        return this.featuresEnabledMap.contains(feature);
    }

    private synchronized void setFeature(Feature feature, boolean enable) {
        if (enable) {
            this.featuresEnabledMap.add(feature);
            sLogger.v("enabled featured:" + feature);
        } else {
            this.featuresEnabledMap.remove(feature);
            sLogger.v("disabled featured:" + feature);
        }
    }

    private Feature getFeatureFromName(String name) {
        if (name == null) {
            return null;
        }
        Object obj = -1;
        switch (name.hashCode()) {
            case -1256323375:
                if (name.equals(FEATURE_FUEL_ROUTING)) {
                    obj = 2;
                    break;
                }
                break;
            case -1149811389:
                if (name.equals(FEATURE_GESTURE_COLLECTOR)) {
                    obj = null;
                    break;
                }
                break;
            case -23581364:
                if (name.equals(FEATURE_GESTURE_ENGINE)) {
                    obj = 1;
                    break;
                }
                break;
        }
        switch (obj) {
            case null:
                return Feature.GESTURE_COLLECTOR;
            case 1:
                return Feature.GESTURE_ENGINE;
            case 2:
                return Feature.FUEL_ROUTING;
            default:
                return null;
        }
    }

    private boolean isGestureCollectorInstalled() {
        try {
            HudApplication.getAppContext().getPackageManager().getPackageInfo("com.navdy.collector", 1);
            return true;
        } catch (Throwable e) {
            if (e instanceof NameNotFoundException) {
                sLogger.v("gesture collector not found");
            } else {
                sLogger.v("gesture collector not enabled", e);
            }
            return false;
        }
    }
}
