package com.navdy.hud.app.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;

public class ImageUtil {
    public static Bitmap hueShift(Bitmap bitmap, float hue) {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        Canvas canvasResult = new Canvas(newBitmap);
        Paint paint = new Paint();
        ColorMatrix colorMatrix = new ColorMatrix();
        float value = (Math.min(hue, Math.max(-hue, 180.0f)) / 180.0f) * 3.1415927f;
        if (value == 0.0f) {
            return null;
        }
        float cosVal = (float) Math.cos((double) value);
        float sinVal = (float) Math.sin((double) value);
        colorMatrix.postConcat(new ColorMatrix(new float[]{(((1.0f - 0.213f) * cosVal) + 0.213f) + ((-1046092972) * sinVal), (((-1060571709) * cosVal) + 0.715f) + ((-1060571709) * sinVal), (((-1033073852) * cosVal) + 0.072f) + ((1.0f - 0.072f) * sinVal), 0.0f, 0.0f, (((-1046092972) * cosVal) + 0.213f) + (0.143f * sinVal), (((1.0f - 0.715f) * cosVal) + 0.715f) + (0.14f * sinVal), (((-1033073852) * cosVal) + 0.072f) + (-0.283f * sinVal), 0.0f, 0.0f, (((-1046092972) * cosVal) + 0.213f) + ((-(1.0f - 0.213f)) * sinVal), (((-1060571709) * cosVal) + 0.715f) + (sinVal * 0.715f), (((1.0f - 0.072f) * cosVal) + 0.072f) + (sinVal * 0.072f), 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f}));
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvasResult.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        return newBitmap;
    }

    public static Bitmap applySaturation(Bitmap bitmap, float saturation) {
        GenericUtil.checkNotOnMainThread();
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        Canvas canvasResult = new Canvas(newBitmap);
        Paint paint = new Paint();
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(saturation);
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvasResult.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        return newBitmap;
    }

    public static Bitmap blend(Bitmap base, Bitmap blend) {
        GenericUtil.checkNotOnMainThread();
        Bitmap newBitmap = base.copy(base.getConfig(), true);
        Paint p = new Paint();
        p.setXfermode(new PorterDuffXfermode(Mode.MULTIPLY));
        new Canvas(newBitmap).drawBitmap(blend, 0.0f, 0.0f, p);
        return newBitmap;
    }
}
