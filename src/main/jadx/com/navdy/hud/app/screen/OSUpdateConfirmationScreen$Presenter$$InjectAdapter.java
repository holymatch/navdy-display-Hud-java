package com.navdy.hud.app.screen;

import android.content.SharedPreferences;
import com.navdy.hud.app.screen.OSUpdateConfirmationScreen.Presenter;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.squareup.otto.Bus;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class OSUpdateConfirmationScreen$Presenter$$InjectAdapter extends Binding<Presenter> implements Provider<Presenter>, MembersInjector<Presenter> {
    private Binding<Bus> mBus;
    private Binding<SharedPreferences> mPreferences;
    private Binding<BasePresenter> supertype;

    public OSUpdateConfirmationScreen$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.screen.OSUpdateConfirmationScreen$Presenter", "members/com.navdy.hud.app.screen.OSUpdateConfirmationScreen$Presenter", true, Presenter.class);
    }

    public void attach(Linker linker) {
        this.mBus = linker.requestBinding("com.squareup.otto.Bus", Presenter.class, getClass().getClassLoader());
        this.mPreferences = linker.requestBinding("android.content.SharedPreferences", Presenter.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", Presenter.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mBus);
        injectMembersBindings.add(this.mPreferences);
        injectMembersBindings.add(this.supertype);
    }

    public Presenter get() {
        Presenter result = new Presenter();
        injectMembers(result);
        return result;
    }

    public void injectMembers(Presenter object) {
        object.mBus = (Bus) this.mBus.get();
        object.mPreferences = (SharedPreferences) this.mPreferences.get();
        this.supertype.injectMembers(object);
    }
}
