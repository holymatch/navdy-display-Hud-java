package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.service.library.log.Logger;
import java.nio.ByteBuffer;

public class BaseIncomingOneIntMessage extends BaseIncomingMessage {
    private static int MESSAGE_LENGTH = 5;
    private static Logger sLogger = new Logger(BaseIncomingOneIntMessage.class);
    public int value;

    public BaseIncomingOneIntMessage(int value) {
        this.value = value;
    }

    protected static int parseIntValue(byte[] payload) throws MessageWrongWayException, CorruptedPayloadException {
        if (payload.length == MESSAGE_LENGTH) {
            return ByteBuffer.wrap(payload).getInt(1);
        }
        throw new CorruptedPayloadException();
    }

    public String toString() {
        sLogger.w("toString not overwritten in BaseIncomingOneIntMessage class");
        return "One int message received with value: " + this.value;
    }
}
