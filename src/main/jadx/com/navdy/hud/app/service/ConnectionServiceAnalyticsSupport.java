package com.navdy.hud.app.service;

import android.content.Intent;
import android.os.Process;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.device.gps.GpsConstants;
import com.navdy.hud.mfi.IAPListener;

public class ConnectionServiceAnalyticsSupport {

    public static class IAPListenerReceiver implements IAPListener {
        public void onCoprocessorStatusCheckFailed(String desc, String status, String errorCode) {
            ConnectionServiceAnalyticsSupport.sendEvent(AnalyticsSupport.ANALYTICS_EVENT_IAP_FAILURE, true, "Description", desc, "Status", status, "Error_Code", errorCode);
        }

        public void onDeviceAuthenticationSuccess(int retries) {
            if (retries > 0) {
                ConnectionServiceAnalyticsSupport.sendEvent(AnalyticsSupport.ANALYTICS_EVENT_IAP_RETRY_SUCCESS, true, "Retries", Integer.toString(retries));
            }
        }
    }

    public static void recordGpsAccuracy(String minimum, String maximum, String average) {
        sendEvent(AnalyticsSupport.ANALYTICS_EVENT_GPS_ACCURACY_STATISTICS, GpsConstants.GPS_EVENT_ACCURACY_MIN, minimum, GpsConstants.GPS_EVENT_ACCURACY_MAX, maximum, GpsConstants.GPS_EVENT_ACCURACY_AVERAGE, average);
    }

    public static void recordGpsAcquireLocation(String time, String accuracy) {
        sendEvent(AnalyticsSupport.ANALYTICS_EVENT_GPS_ACQUIRE_LOCATION, GpsConstants.GPS_EVENT_TIME, time, GpsConstants.GPS_EVENT_ACCURACY, accuracy);
    }

    public static void recordGpsAttemptAcquireLocation() {
        sendEvent(AnalyticsSupport.ANALYTICS_EVENT_GPS_ATTEMPT_ACQUIRE_LOCATION, new String[0]);
    }

    public static void recordGpsLostLocation(String time) {
        sendEvent(AnalyticsSupport.ANALYTICS_EVENT_GPS_LOST_LOCATION, GpsConstants.GPS_EVENT_TIME, time);
    }

    public static void recordNewDevice() {
        sendEvent(AnalyticsSupport.ANALYTICS_EVENT_NEW_DEVICE, new String[0]);
    }

    private static void sendEvent(String name, String... args) {
        sendEvent(name, false, args);
    }

    private static void sendEvent(String name, boolean includeDeviceInfo, String... args) {
        Intent intent = new Intent(AnalyticsSupport.ANALYTICS_INTENT);
        intent.putExtra(AnalyticsSupport.ANALYTICS_INTENT_EXTRA_TAG_NAME, name);
        intent.putExtra(AnalyticsSupport.ANALYTICS_INTENT_EXTRA_ARGUMENTS, args);
        if (includeDeviceInfo) {
            intent.putExtra(AnalyticsSupport.ANALYTICS_INTENT_EXTRA_INCLUDE_REMOTE_DEVICE_INFO, true);
        }
        HudApplication.getAppContext().sendBroadcastAsUser(intent, Process.myUserHandle());
    }
}
