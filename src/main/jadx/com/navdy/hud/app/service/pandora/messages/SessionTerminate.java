package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;
import com.navdy.hud.app.service.pandora.exceptions.StringOverflowException;
import com.navdy.hud.app.service.pandora.exceptions.UnexpectedEndOfStringException;
import java.io.IOException;

public class SessionTerminate extends BaseOutgoingEmptyMessage {
    public static final SessionTerminate INSTANCE = new SessionTerminate();

    public /* bridge */ /* synthetic */ byte[] buildPayload() throws IOException, StringOverflowException, UnexpectedEndOfStringException, MessageWrongWayException {
        return super.buildPayload();
    }

    private SessionTerminate() {
    }

    protected byte getMessageType() {
        return (byte) 5;
    }

    public String toString() {
        return "Session Terminate";
    }
}
