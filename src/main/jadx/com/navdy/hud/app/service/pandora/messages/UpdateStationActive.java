package com.navdy.hud.app.service.pandora.messages;

import com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException;
import com.navdy.hud.app.service.pandora.exceptions.MessageWrongWayException;

public class UpdateStationActive extends BaseIncomingOneIntMessage {
    public UpdateStationActive(int value) {
        super(value);
    }

    protected static BaseIncomingMessage innerBuildFromPayload(byte[] payload) throws MessageWrongWayException, CorruptedPayloadException {
        return new UpdateStationActive(BaseIncomingOneIntMessage.parseIntValue(payload));
    }

    public String toString() {
        return "New active station with token: " + this.value;
    }
}
