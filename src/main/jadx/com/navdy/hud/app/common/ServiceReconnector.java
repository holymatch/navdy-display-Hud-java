package com.navdy.hud.app.common;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import com.navdy.service.library.log.Logger;
import java.util.List;

public abstract class ServiceReconnector {
    private static final int RECONNECT_INTERVAL_MS = 15000;
    private static final int RETRY_INTERVAL_MS = 60000;
    private String action;
    private Runnable connectRunnable = new Runnable() {
        public void run() {
            try {
                ComponentName component = ServiceReconnector.this.serviceIntent.getComponent();
                if (component == null) {
                    component = ServiceReconnector.this.getServiceComponent(ServiceReconnector.this.serviceIntent);
                }
                if (component == null) {
                    ServiceReconnector.this.handler.postDelayed(this, 60000);
                    return;
                }
                ServiceReconnector.this.serviceIntent.setComponent(component);
                ServiceReconnector.this.logger.i("Trying to start service");
                if (ServiceReconnector.this.context.startService(ServiceReconnector.this.serviceIntent) != null) {
                    ServiceReconnector.this.serviceIntent.setAction(ServiceReconnector.this.action);
                    if (!ServiceReconnector.this.context.bindService(ServiceReconnector.this.serviceIntent, ServiceReconnector.this.serviceConnection, 0)) {
                        ServiceReconnector.this.logger.e("Unable to bind to service - aborting");
                        return;
                    }
                    return;
                }
                ServiceReconnector.this.logger.e("Service doesn't exist (uninstalled?) - retrying");
                ServiceReconnector.this.handler.postDelayed(this, 60000);
            } catch (SecurityException ex) {
                ServiceReconnector.this.logger.e("Security exception connecting to service - aborting", ex);
            }
        }
    };
    private Context context;
    private Handler handler;
    private final Logger logger = new Logger(getClass());
    private ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            ServiceReconnector.this.logger.i("ServiceConnection established with " + name);
            ServiceReconnector.this.onConnected(name, service);
        }

        public void onServiceDisconnected(ComponentName name) {
            ServiceReconnector.this.onDisconnected(name);
            if (!ServiceReconnector.this.shuttingDown) {
                ServiceReconnector.this.logger.i("Service disconnected - will try reconnecting");
                ServiceReconnector.this.handler.postDelayed(ServiceReconnector.this.connectRunnable, 15000);
            }
        }
    };
    protected Intent serviceIntent;
    boolean shuttingDown;

    protected abstract void onConnected(ComponentName componentName, IBinder iBinder);

    protected abstract void onDisconnected(ComponentName componentName);

    public ServiceReconnector(Context context, Intent serviceIntent, String action) {
        this.logger.i("Establishing service connection");
        this.handler = new Handler(Looper.getMainLooper());
        this.context = context;
        this.serviceIntent = serviceIntent;
        this.action = action;
        this.handler.post(this.connectRunnable);
    }

    public void shutdown() {
        this.logger.i("shutting down service");
        this.shuttingDown = true;
        this.handler.removeCallbacks(this.connectRunnable);
        this.context.stopService(this.serviceIntent);
    }

    public void restart() {
        this.context.stopService(this.serviceIntent);
    }

    private ComponentName getServiceComponent(Intent serviceIntent) {
        List<ResolveInfo> pkgAppsList = this.context.getPackageManager().queryIntentServices(serviceIntent, 0);
        if (pkgAppsList.isEmpty()) {
            return null;
        }
        ResolveInfo info = (ResolveInfo) pkgAppsList.get(0);
        return new ComponentName(info.serviceInfo.applicationInfo.packageName, info.serviceInfo.name);
    }
}
