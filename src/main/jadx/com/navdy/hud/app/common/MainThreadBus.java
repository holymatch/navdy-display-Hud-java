package com.navdy.hud.app.common;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.navdy.hud.app.debug.BusLeakDetector;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;

public class MainThreadBus extends Bus {
    private BusLeakDetector busLeakDetector;
    private boolean isEngBuild;
    private final Logger logger = new Logger(MainThreadBus.class);
    private final Handler mHandler = new Handler(Looper.getMainLooper());
    public int threshold;

    public MainThreadBus() {
        this.isEngBuild = !DeviceUtil.isUserBuild();
        if (this.isEngBuild) {
            this.busLeakDetector = BusLeakDetector.getInstance();
        }
    }

    public void post(final Object event) {
        if (event != null) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    long l1 = 0;
                    if (MainThreadBus.this.threshold > 0) {
                        l1 = SystemClock.elapsedRealtime();
                    }
                    super.post(event);
                    if (MainThreadBus.this.threshold > 0) {
                        long diff = SystemClock.elapsedRealtime() - l1;
                        if (diff >= ((long) MainThreadBus.this.threshold)) {
                            MainThreadBus.this.logger.v("[" + diff + "] MainThreadBus-event [" + event + "]");
                        }
                    }
                }
            });
        }
    }

    public void register(final Object object) {
        if (object != null) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    if (MainThreadBus.this.isEngBuild) {
                        MainThreadBus.this.busLeakDetector.addReference(object.getClass());
                    }
                    super.register(object);
                }
            });
        }
    }

    public void unregister(final Object object) {
        if (object != null) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    if (MainThreadBus.this.isEngBuild) {
                        MainThreadBus.this.busLeakDetector.removeReference(object.getClass());
                    }
                    super.unregister(object);
                }
            });
        }
    }
}
