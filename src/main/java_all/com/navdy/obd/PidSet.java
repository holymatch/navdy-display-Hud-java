package com.navdy.obd;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import android.os.Parcel;
import java.util.BitSet;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class PidSet implements Parcelable
{
    public static final Parcelable.Creator<PidSet> CREATOR;
    public static final int MAX_PID = 320;
    private BitSet pids;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<PidSet>() {
            public PidSet createFromParcel(final Parcel parcel) {
                return new PidSet(parcel);
            }
            
            public PidSet[] newArray(final int n) {
                return new PidSet[n];
            }
        };
    }
    
    public PidSet() {
        this.pids = new BitSet(320);
    }
    
    public PidSet(final long n, final int n2) {
        this();
        for (int i = 0; i < 32; ++i) {
            if ((n & 1 << 31 - i) != 0x0L) {
                this.pids.set(n2 + i + 1);
            }
        }
    }
    
    public PidSet(final Parcel parcel) {
        final long[] array = new long[parcel.readByte()];
        parcel.readLongArray(array);
        this.pids = BitSet.valueOf(array);
    }
    
    public PidSet(final ScanSchedule scanSchedule) {
        this();
        final Iterator<Map.Entry<Integer, ScanSchedule.Scan>> iterator = scanSchedule.schedule.entrySet().iterator();
        while (iterator.hasNext()) {
            this.pids.set(iterator.next().getValue().pid);
        }
    }
    
    public PidSet(final List<Pid> list) {
        this();
        final Iterator<Pid> iterator = list.iterator();
        while (iterator.hasNext()) {
            this.pids.set(iterator.next().getId());
        }
    }
    
    public void add(final int n) {
        this.pids.set(n);
    }
    
    public void add(final Pid pid) {
        this.add(pid.getId());
    }
    
    public List<Pid> asList() {
        final ArrayList<Pid> list = new ArrayList<Pid>(this.pids.cardinality());
        int nextSetBit;
        for (int n = 0; n < 320 && n != -1; n = nextSetBit + 1) {
            nextSetBit = this.pids.nextSetBit(n);
            if ((n = nextSetBit) != -1) {
                list.add(new Pid(nextSetBit));
            }
        }
        return list;
    }
    
    public void clear() {
        this.pids.clear();
    }
    
    public boolean contains(final int n) {
        return this.pids.get(n);
    }
    
    public boolean contains(final Pid pid) {
        return this.pids.get(pid.getId());
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof PidSet && this.pids.equals(((PidSet)o).pids);
    }
    
    public boolean isEmpty() {
        return this.pids.isEmpty();
    }
    
    public void merge(final PidSet set) {
        this.pids.or(set.pids);
    }
    
    public int nextPid(final int n) {
        return this.pids.nextSetBit(n);
    }
    
    public void remove(final int n) {
        this.pids.clear(n);
    }
    
    public int size() {
        return this.pids.cardinality();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final long[] longArray = this.pids.toLongArray();
        parcel.writeByte((byte)longArray.length);
        parcel.writeLongArray(longArray);
    }
}
