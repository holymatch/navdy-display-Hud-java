package com.navdy.service.library.events.navigation;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class NavigationSessionStatusEvent extends Message
{
    public static final String DEFAULT_DESTINATION_IDENTIFIER = "";
    public static final String DEFAULT_LABEL = "";
    public static final String DEFAULT_ROUTEID = "";
    public static final NavigationSessionState DEFAULT_SESSIONSTATE;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String destination_identifier;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String label;
    @ProtoField(tag = 4)
    public final NavigationRouteResult route;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String routeId;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final NavigationSessionState sessionState;
    
    static {
        DEFAULT_SESSIONSTATE = NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    }
    
    public NavigationSessionStatusEvent(final NavigationSessionState sessionState, final String label, final String routeId, final NavigationRouteResult route, final String destination_identifier) {
        this.sessionState = sessionState;
        this.label = label;
        this.routeId = routeId;
        this.route = route;
        this.destination_identifier = destination_identifier;
    }
    
    private NavigationSessionStatusEvent(final Builder builder) {
        this(builder.sessionState, builder.label, builder.routeId, builder.route, builder.destination_identifier);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NavigationSessionStatusEvent)) {
                b = false;
            }
            else {
                final NavigationSessionStatusEvent navigationSessionStatusEvent = (NavigationSessionStatusEvent)o;
                if (!this.equals(this.sessionState, navigationSessionStatusEvent.sessionState) || !this.equals(this.label, navigationSessionStatusEvent.label) || !this.equals(this.routeId, navigationSessionStatusEvent.routeId) || !this.equals(this.route, navigationSessionStatusEvent.route) || !this.equals(this.destination_identifier, navigationSessionStatusEvent.destination_identifier)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.sessionState != null) {
                hashCode3 = this.sessionState.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.label != null) {
                hashCode4 = this.label.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.routeId != null) {
                hashCode5 = this.routeId.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.route != null) {
                hashCode6 = this.route.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            if (this.destination_identifier != null) {
                hashCode = this.destination_identifier.hashCode();
            }
            hashCode2 = (((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NavigationSessionStatusEvent>
    {
        public String destination_identifier;
        public String label;
        public NavigationRouteResult route;
        public String routeId;
        public NavigationSessionState sessionState;
        
        public Builder() {
        }
        
        public Builder(final NavigationSessionStatusEvent navigationSessionStatusEvent) {
            super(navigationSessionStatusEvent);
            if (navigationSessionStatusEvent != null) {
                this.sessionState = navigationSessionStatusEvent.sessionState;
                this.label = navigationSessionStatusEvent.label;
                this.routeId = navigationSessionStatusEvent.routeId;
                this.route = navigationSessionStatusEvent.route;
                this.destination_identifier = navigationSessionStatusEvent.destination_identifier;
            }
        }
        
        public NavigationSessionStatusEvent build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NavigationSessionStatusEvent(this, null);
        }
        
        public Builder destination_identifier(final String destination_identifier) {
            this.destination_identifier = destination_identifier;
            return this;
        }
        
        public Builder label(final String label) {
            this.label = label;
            return this;
        }
        
        public Builder route(final NavigationRouteResult route) {
            this.route = route;
            return this;
        }
        
        public Builder routeId(final String routeId) {
            this.routeId = routeId;
            return this;
        }
        
        public Builder sessionState(final NavigationSessionState sessionState) {
            this.sessionState = sessionState;
            return this;
        }
    }
}
