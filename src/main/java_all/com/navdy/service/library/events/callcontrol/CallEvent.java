package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class CallEvent extends Message
{
    public static final Boolean DEFAULT_ANSWERED;
    public static final String DEFAULT_CONTACT_NAME = "";
    public static final Long DEFAULT_DURATION;
    public static final Boolean DEFAULT_INCOMING;
    public static final String DEFAULT_NUMBER = "";
    public static final Long DEFAULT_START_TIME;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.BOOL)
    public final Boolean answered;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String contact_name;
    @ProtoField(label = Label.REQUIRED, tag = 6, type = Datatype.INT64)
    public final Long duration;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.BOOL)
    public final Boolean incoming;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.STRING)
    public final String number;
    @ProtoField(label = Label.REQUIRED, tag = 5, type = Datatype.INT64)
    public final Long start_time;
    
    static {
        DEFAULT_INCOMING = false;
        DEFAULT_ANSWERED = false;
        DEFAULT_START_TIME = 0L;
        DEFAULT_DURATION = 0L;
    }
    
    private CallEvent(final Builder builder) {
        this(builder.incoming, builder.answered, builder.number, builder.contact_name, builder.start_time, builder.duration);
        this.setBuilder((Message.Builder)builder);
    }
    
    public CallEvent(final Boolean incoming, final Boolean answered, final String number, final String contact_name, final Long start_time, final Long duration) {
        this.incoming = incoming;
        this.answered = answered;
        this.number = number;
        this.contact_name = contact_name;
        this.start_time = start_time;
        this.duration = duration;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof CallEvent)) {
                b = false;
            }
            else {
                final CallEvent callEvent = (CallEvent)o;
                if (!this.equals(this.incoming, callEvent.incoming) || !this.equals(this.answered, callEvent.answered) || !this.equals(this.number, callEvent.number) || !this.equals(this.contact_name, callEvent.contact_name) || !this.equals(this.start_time, callEvent.start_time) || !this.equals(this.duration, callEvent.duration)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.incoming != null) {
                hashCode3 = this.incoming.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.answered != null) {
                hashCode4 = this.answered.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.number != null) {
                hashCode5 = this.number.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.contact_name != null) {
                hashCode6 = this.contact_name.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.start_time != null) {
                hashCode7 = this.start_time.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            if (this.duration != null) {
                hashCode = this.duration.hashCode();
            }
            hashCode2 = ((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<CallEvent>
    {
        public Boolean answered;
        public String contact_name;
        public Long duration;
        public Boolean incoming;
        public String number;
        public Long start_time;
        
        public Builder() {
        }
        
        public Builder(final CallEvent callEvent) {
            super(callEvent);
            if (callEvent != null) {
                this.incoming = callEvent.incoming;
                this.answered = callEvent.answered;
                this.number = callEvent.number;
                this.contact_name = callEvent.contact_name;
                this.start_time = callEvent.start_time;
                this.duration = callEvent.duration;
            }
        }
        
        public Builder answered(final Boolean answered) {
            this.answered = answered;
            return this;
        }
        
        public CallEvent build() {
            ((Message.Builder)this).checkRequiredFields();
            return new CallEvent(this, null);
        }
        
        public Builder contact_name(final String contact_name) {
            this.contact_name = contact_name;
            return this;
        }
        
        public Builder duration(final Long duration) {
            this.duration = duration;
            return this;
        }
        
        public Builder incoming(final Boolean incoming) {
            this.incoming = incoming;
            return this;
        }
        
        public Builder number(final String number) {
            this.number = number;
            return this;
        }
        
        public Builder start_time(final Long start_time) {
            this.start_time = start_time;
            return this;
        }
    }
}
