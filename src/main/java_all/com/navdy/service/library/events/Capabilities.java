package com.navdy.service.library.events;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class Capabilities extends Message
{
    public static final Boolean DEFAULT_CANNEDRESPONSETOSMS;
    public static final Boolean DEFAULT_COMPACTUI;
    public static final Boolean DEFAULT_CUSTOMDIALLONGPRESS;
    public static final Boolean DEFAULT_LOCALMUSICBROWSER;
    public static final Boolean DEFAULT_MUSICARTWORKCACHE;
    public static final Boolean DEFAULT_NAVCOORDSLOOKUP;
    public static final Boolean DEFAULT_PLACETYPESEARCH;
    public static final Boolean DEFAULT_SEARCHRESULTLIST;
    public static final Boolean DEFAULT_SUPPORTSPHONETICTTS;
    public static final Boolean DEFAULT_VOICESEARCH;
    public static final Boolean DEFAULT_VOICESEARCHNEWIOSPAUSEBEHAVIORS;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 9, type = Datatype.BOOL)
    public final Boolean cannedResponseToSms;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean compactUi;
    @ProtoField(tag = 10, type = Datatype.BOOL)
    public final Boolean customDialLongPress;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean localMusicBrowser;
    @ProtoField(tag = 7, type = Datatype.BOOL)
    public final Boolean musicArtworkCache;
    @ProtoField(tag = 5, type = Datatype.BOOL)
    public final Boolean navCoordsLookup;
    @ProtoField(tag = 2, type = Datatype.BOOL)
    public final Boolean placeTypeSearch;
    @ProtoField(tag = 6, type = Datatype.BOOL)
    public final Boolean searchResultList;
    @ProtoField(tag = 11, type = Datatype.BOOL)
    public final Boolean supportsPhoneticTts;
    @ProtoField(tag = 3, type = Datatype.BOOL)
    public final Boolean voiceSearch;
    @ProtoField(tag = 8, type = Datatype.BOOL)
    public final Boolean voiceSearchNewIOSPauseBehaviors;
    
    static {
        DEFAULT_COMPACTUI = false;
        DEFAULT_PLACETYPESEARCH = false;
        DEFAULT_VOICESEARCH = false;
        DEFAULT_LOCALMUSICBROWSER = false;
        DEFAULT_NAVCOORDSLOOKUP = false;
        DEFAULT_SEARCHRESULTLIST = false;
        DEFAULT_MUSICARTWORKCACHE = false;
        DEFAULT_VOICESEARCHNEWIOSPAUSEBEHAVIORS = false;
        DEFAULT_CANNEDRESPONSETOSMS = false;
        DEFAULT_CUSTOMDIALLONGPRESS = false;
        DEFAULT_SUPPORTSPHONETICTTS = false;
    }
    
    private Capabilities(final Builder builder) {
        this(builder.compactUi, builder.placeTypeSearch, builder.voiceSearch, builder.localMusicBrowser, builder.navCoordsLookup, builder.searchResultList, builder.musicArtworkCache, builder.voiceSearchNewIOSPauseBehaviors, builder.cannedResponseToSms, builder.customDialLongPress, builder.supportsPhoneticTts);
        this.setBuilder((Message.Builder)builder);
    }
    
    public Capabilities(final Boolean compactUi, final Boolean placeTypeSearch, final Boolean voiceSearch, final Boolean localMusicBrowser, final Boolean navCoordsLookup, final Boolean searchResultList, final Boolean musicArtworkCache, final Boolean voiceSearchNewIOSPauseBehaviors, final Boolean cannedResponseToSms, final Boolean customDialLongPress, final Boolean supportsPhoneticTts) {
        this.compactUi = compactUi;
        this.placeTypeSearch = placeTypeSearch;
        this.voiceSearch = voiceSearch;
        this.localMusicBrowser = localMusicBrowser;
        this.navCoordsLookup = navCoordsLookup;
        this.searchResultList = searchResultList;
        this.musicArtworkCache = musicArtworkCache;
        this.voiceSearchNewIOSPauseBehaviors = voiceSearchNewIOSPauseBehaviors;
        this.cannedResponseToSms = cannedResponseToSms;
        this.customDialLongPress = customDialLongPress;
        this.supportsPhoneticTts = supportsPhoneticTts;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof Capabilities)) {
                b = false;
            }
            else {
                final Capabilities capabilities = (Capabilities)o;
                if (!this.equals(this.compactUi, capabilities.compactUi) || !this.equals(this.placeTypeSearch, capabilities.placeTypeSearch) || !this.equals(this.voiceSearch, capabilities.voiceSearch) || !this.equals(this.localMusicBrowser, capabilities.localMusicBrowser) || !this.equals(this.navCoordsLookup, capabilities.navCoordsLookup) || !this.equals(this.searchResultList, capabilities.searchResultList) || !this.equals(this.musicArtworkCache, capabilities.musicArtworkCache) || !this.equals(this.voiceSearchNewIOSPauseBehaviors, capabilities.voiceSearchNewIOSPauseBehaviors) || !this.equals(this.cannedResponseToSms, capabilities.cannedResponseToSms) || !this.equals(this.customDialLongPress, capabilities.customDialLongPress) || !this.equals(this.supportsPhoneticTts, capabilities.supportsPhoneticTts)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.compactUi != null) {
                hashCode3 = this.compactUi.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.placeTypeSearch != null) {
                hashCode4 = this.placeTypeSearch.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.voiceSearch != null) {
                hashCode5 = this.voiceSearch.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.localMusicBrowser != null) {
                hashCode6 = this.localMusicBrowser.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.navCoordsLookup != null) {
                hashCode7 = this.navCoordsLookup.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.searchResultList != null) {
                hashCode8 = this.searchResultList.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.musicArtworkCache != null) {
                hashCode9 = this.musicArtworkCache.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            int hashCode10;
            if (this.voiceSearchNewIOSPauseBehaviors != null) {
                hashCode10 = this.voiceSearchNewIOSPauseBehaviors.hashCode();
            }
            else {
                hashCode10 = 0;
            }
            int hashCode11;
            if (this.cannedResponseToSms != null) {
                hashCode11 = this.cannedResponseToSms.hashCode();
            }
            else {
                hashCode11 = 0;
            }
            int hashCode12;
            if (this.customDialLongPress != null) {
                hashCode12 = this.customDialLongPress.hashCode();
            }
            else {
                hashCode12 = 0;
            }
            if (this.supportsPhoneticTts != null) {
                hashCode = this.supportsPhoneticTts.hashCode();
            }
            hashCode2 = (((((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode10) * 37 + hashCode11) * 37 + hashCode12) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<Capabilities>
    {
        public Boolean cannedResponseToSms;
        public Boolean compactUi;
        public Boolean customDialLongPress;
        public Boolean localMusicBrowser;
        public Boolean musicArtworkCache;
        public Boolean navCoordsLookup;
        public Boolean placeTypeSearch;
        public Boolean searchResultList;
        public Boolean supportsPhoneticTts;
        public Boolean voiceSearch;
        public Boolean voiceSearchNewIOSPauseBehaviors;
        
        public Builder() {
        }
        
        public Builder(final Capabilities capabilities) {
            super(capabilities);
            if (capabilities != null) {
                this.compactUi = capabilities.compactUi;
                this.placeTypeSearch = capabilities.placeTypeSearch;
                this.voiceSearch = capabilities.voiceSearch;
                this.localMusicBrowser = capabilities.localMusicBrowser;
                this.navCoordsLookup = capabilities.navCoordsLookup;
                this.searchResultList = capabilities.searchResultList;
                this.musicArtworkCache = capabilities.musicArtworkCache;
                this.voiceSearchNewIOSPauseBehaviors = capabilities.voiceSearchNewIOSPauseBehaviors;
                this.cannedResponseToSms = capabilities.cannedResponseToSms;
                this.customDialLongPress = capabilities.customDialLongPress;
                this.supportsPhoneticTts = capabilities.supportsPhoneticTts;
            }
        }
        
        public Capabilities build() {
            return new Capabilities(this, null);
        }
        
        public Builder cannedResponseToSms(final Boolean cannedResponseToSms) {
            this.cannedResponseToSms = cannedResponseToSms;
            return this;
        }
        
        public Builder compactUi(final Boolean compactUi) {
            this.compactUi = compactUi;
            return this;
        }
        
        public Builder customDialLongPress(final Boolean customDialLongPress) {
            this.customDialLongPress = customDialLongPress;
            return this;
        }
        
        public Builder localMusicBrowser(final Boolean localMusicBrowser) {
            this.localMusicBrowser = localMusicBrowser;
            return this;
        }
        
        public Builder musicArtworkCache(final Boolean musicArtworkCache) {
            this.musicArtworkCache = musicArtworkCache;
            return this;
        }
        
        public Builder navCoordsLookup(final Boolean navCoordsLookup) {
            this.navCoordsLookup = navCoordsLookup;
            return this;
        }
        
        public Builder placeTypeSearch(final Boolean placeTypeSearch) {
            this.placeTypeSearch = placeTypeSearch;
            return this;
        }
        
        public Builder searchResultList(final Boolean searchResultList) {
            this.searchResultList = searchResultList;
            return this;
        }
        
        public Builder supportsPhoneticTts(final Boolean supportsPhoneticTts) {
            this.supportsPhoneticTts = supportsPhoneticTts;
            return this;
        }
        
        public Builder voiceSearch(final Boolean voiceSearch) {
            this.voiceSearch = voiceSearch;
            return this;
        }
        
        public Builder voiceSearchNewIOSPauseBehaviors(final Boolean voiceSearchNewIOSPauseBehaviors) {
            this.voiceSearchNewIOSPauseBehaviors = voiceSearchNewIOSPauseBehaviors;
            return this;
        }
    }
}
