package com.navdy.service.library.events.photo;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class PhotoRequest extends Message
{
    public static final String DEFAULT_IDENTIFIER = "";
    public static final String DEFAULT_NAME = "";
    public static final String DEFAULT_PHOTOCHECKSUM = "";
    public static final PhotoType DEFAULT_PHOTOTYPE;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String identifier;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String photoChecksum;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final PhotoType photoType;
    
    static {
        DEFAULT_PHOTOTYPE = PhotoType.PHOTO_CONTACT;
    }
    
    private PhotoRequest(final Builder builder) {
        this(builder.identifier, builder.photoChecksum, builder.photoType, builder.name);
        this.setBuilder((Message.Builder)builder);
    }
    
    public PhotoRequest(final String identifier, final String photoChecksum, final PhotoType photoType, final String name) {
        this.identifier = identifier;
        this.photoChecksum = photoChecksum;
        this.photoType = photoType;
        this.name = name;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PhotoRequest)) {
                b = false;
            }
            else {
                final PhotoRequest photoRequest = (PhotoRequest)o;
                if (!this.equals(this.identifier, photoRequest.identifier) || !this.equals(this.photoChecksum, photoRequest.photoChecksum) || !this.equals(this.photoType, photoRequest.photoType) || !this.equals(this.name, photoRequest.name)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.identifier != null) {
                hashCode3 = this.identifier.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.photoChecksum != null) {
                hashCode4 = this.photoChecksum.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.photoType != null) {
                hashCode5 = this.photoType.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            if (this.name != null) {
                hashCode = this.name.hashCode();
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PhotoRequest>
    {
        public String identifier;
        public String name;
        public String photoChecksum;
        public PhotoType photoType;
        
        public Builder() {
        }
        
        public Builder(final PhotoRequest photoRequest) {
            super(photoRequest);
            if (photoRequest != null) {
                this.identifier = photoRequest.identifier;
                this.photoChecksum = photoRequest.photoChecksum;
                this.photoType = photoRequest.photoType;
                this.name = photoRequest.name;
            }
        }
        
        public PhotoRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new PhotoRequest(this, null);
        }
        
        public Builder identifier(final String identifier) {
            this.identifier = identifier;
            return this;
        }
        
        public Builder name(final String name) {
            this.name = name;
            return this;
        }
        
        public Builder photoChecksum(final String photoChecksum) {
            this.photoChecksum = photoChecksum;
            return this;
        }
        
        public Builder photoType(final PhotoType photoType) {
            this.photoType = photoType;
            return this;
        }
    }
}
