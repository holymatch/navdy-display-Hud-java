package com.navdy.service.library.events.glances;

import java.util.Collections;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import java.util.List;
import com.squareup.wire.Message;

public final class CannedMessagesUpdate extends Message
{
    public static final List<String> DEFAULT_CANNEDMESSAGE;
    public static final Long DEFAULT_SERIAL_NUMBER;
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REPEATED, tag = 4, type = Datatype.STRING)
    public final List<String> cannedMessage;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_SERIAL_NUMBER = 0L;
        DEFAULT_CANNEDMESSAGE = Collections.<String>emptyList();
    }
    
    public CannedMessagesUpdate(final RequestStatus status, final String statusDetail, final Long serial_number, final List<String> list) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.serial_number = serial_number;
        this.cannedMessage = Message.<String>immutableCopyOf(list);
    }
    
    private CannedMessagesUpdate(final Builder builder) {
        this(builder.status, builder.statusDetail, builder.serial_number, builder.cannedMessage);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof CannedMessagesUpdate)) {
                b = false;
            }
            else {
                final CannedMessagesUpdate cannedMessagesUpdate = (CannedMessagesUpdate)o;
                if (!this.equals(this.status, cannedMessagesUpdate.status) || !this.equals(this.statusDetail, cannedMessagesUpdate.statusDetail) || !this.equals(this.serial_number, cannedMessagesUpdate.serial_number) || !this.equals(this.cannedMessage, cannedMessagesUpdate.cannedMessage)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.statusDetail != null) {
                hashCode4 = this.statusDetail.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.serial_number != null) {
                hashCode = this.serial_number.hashCode();
            }
            int hashCode5;
            if (this.cannedMessage != null) {
                hashCode5 = this.cannedMessage.hashCode();
            }
            else {
                hashCode5 = 1;
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode) * 37 + hashCode5;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<CannedMessagesUpdate>
    {
        public List<String> cannedMessage;
        public Long serial_number;
        public RequestStatus status;
        public String statusDetail;
        
        public Builder() {
        }
        
        public Builder(final CannedMessagesUpdate cannedMessagesUpdate) {
            super(cannedMessagesUpdate);
            if (cannedMessagesUpdate != null) {
                this.status = cannedMessagesUpdate.status;
                this.statusDetail = cannedMessagesUpdate.statusDetail;
                this.serial_number = cannedMessagesUpdate.serial_number;
                this.cannedMessage = (List<String>)Message.<Object>copyOf((List<Object>)cannedMessagesUpdate.cannedMessage);
            }
        }
        
        public CannedMessagesUpdate build() {
            ((Message.Builder)this).checkRequiredFields();
            return new CannedMessagesUpdate(this, null);
        }
        
        public Builder cannedMessage(final List<String> list) {
            this.cannedMessage = Message.Builder.<String>checkForNulls(list);
            return this;
        }
        
        public Builder serial_number(final Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder statusDetail(final String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }
    }
}
