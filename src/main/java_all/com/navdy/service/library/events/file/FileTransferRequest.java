package com.navdy.service.library.events.file;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class FileTransferRequest extends Message
{
    public static final String DEFAULT_DESTINATIONFILENAME = "";
    public static final String DEFAULT_FILEDATACHECKSUM = "";
    public static final Long DEFAULT_FILESIZE;
    public static final FileType DEFAULT_FILETYPE;
    public static final Long DEFAULT_OFFSET;
    public static final Boolean DEFAULT_OVERRIDE;
    public static final Boolean DEFAULT_SUPPORTSACKS;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String destinationFileName;
    @ProtoField(tag = 6, type = Datatype.STRING)
    public final String fileDataChecksum;
    @ProtoField(tag = 4, type = Datatype.INT64)
    public final Long fileSize;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final FileType fileType;
    @ProtoField(tag = 5, type = Datatype.INT64)
    public final Long offset;
    @ProtoField(tag = 7, type = Datatype.BOOL)
    public final Boolean override;
    @ProtoField(tag = 8, type = Datatype.BOOL)
    public final Boolean supportsAcks;
    
    static {
        DEFAULT_FILETYPE = FileType.FILE_TYPE_OTA;
        DEFAULT_FILESIZE = 0L;
        DEFAULT_OFFSET = 0L;
        DEFAULT_OVERRIDE = false;
        DEFAULT_SUPPORTSACKS = false;
    }
    
    private FileTransferRequest(final Builder builder) {
        this(builder.fileType, builder.destinationFileName, builder.fileSize, builder.offset, builder.fileDataChecksum, builder.override, builder.supportsAcks);
        this.setBuilder((Message.Builder)builder);
    }
    
    public FileTransferRequest(final FileType fileType, final String destinationFileName, final Long fileSize, final Long offset, final String fileDataChecksum, final Boolean override, final Boolean supportsAcks) {
        this.fileType = fileType;
        this.destinationFileName = destinationFileName;
        this.fileSize = fileSize;
        this.offset = offset;
        this.fileDataChecksum = fileDataChecksum;
        this.override = override;
        this.supportsAcks = supportsAcks;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof FileTransferRequest)) {
                b = false;
            }
            else {
                final FileTransferRequest fileTransferRequest = (FileTransferRequest)o;
                if (!this.equals(this.fileType, fileTransferRequest.fileType) || !this.equals(this.destinationFileName, fileTransferRequest.destinationFileName) || !this.equals(this.fileSize, fileTransferRequest.fileSize) || !this.equals(this.offset, fileTransferRequest.offset) || !this.equals(this.fileDataChecksum, fileTransferRequest.fileDataChecksum) || !this.equals(this.override, fileTransferRequest.override) || !this.equals(this.supportsAcks, fileTransferRequest.supportsAcks)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.fileType != null) {
                hashCode3 = this.fileType.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.destinationFileName != null) {
                hashCode4 = this.destinationFileName.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.fileSize != null) {
                hashCode5 = this.fileSize.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.offset != null) {
                hashCode6 = this.offset.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.fileDataChecksum != null) {
                hashCode7 = this.fileDataChecksum.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.override != null) {
                hashCode8 = this.override.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            if (this.supportsAcks != null) {
                hashCode = this.supportsAcks.hashCode();
            }
            hashCode2 = (((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<FileTransferRequest>
    {
        public String destinationFileName;
        public String fileDataChecksum;
        public Long fileSize;
        public FileType fileType;
        public Long offset;
        public Boolean override;
        public Boolean supportsAcks;
        
        public Builder() {
        }
        
        public Builder(final FileTransferRequest fileTransferRequest) {
            super(fileTransferRequest);
            if (fileTransferRequest != null) {
                this.fileType = fileTransferRequest.fileType;
                this.destinationFileName = fileTransferRequest.destinationFileName;
                this.fileSize = fileTransferRequest.fileSize;
                this.offset = fileTransferRequest.offset;
                this.fileDataChecksum = fileTransferRequest.fileDataChecksum;
                this.override = fileTransferRequest.override;
                this.supportsAcks = fileTransferRequest.supportsAcks;
            }
        }
        
        public FileTransferRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new FileTransferRequest(this, null);
        }
        
        public Builder destinationFileName(final String destinationFileName) {
            this.destinationFileName = destinationFileName;
            return this;
        }
        
        public Builder fileDataChecksum(final String fileDataChecksum) {
            this.fileDataChecksum = fileDataChecksum;
            return this;
        }
        
        public Builder fileSize(final Long fileSize) {
            this.fileSize = fileSize;
            return this;
        }
        
        public Builder fileType(final FileType fileType) {
            this.fileType = fileType;
            return this;
        }
        
        public Builder offset(final Long offset) {
            this.offset = offset;
            return this;
        }
        
        public Builder override(final Boolean override) {
            this.override = override;
            return this;
        }
        
        public Builder supportsAcks(final Boolean supportsAcks) {
            this.supportsAcks = supportsAcks;
            return this;
        }
    }
}
