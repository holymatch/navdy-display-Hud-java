package com.navdy.service.library.events.dial;

import com.squareup.wire.Message;

public final class DialStatusRequest extends Message
{
    private static final long serialVersionUID = 0L;
    
    public DialStatusRequest() {
    }
    
    private DialStatusRequest(final Builder builder) {
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof DialStatusRequest;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    public static final class Builder extends Message.Builder<DialStatusRequest>
    {
        public Builder() {
        }
        
        public Builder(final DialStatusRequest dialStatusRequest) {
            super(dialStatusRequest);
        }
        
        public DialStatusRequest build() {
            return new DialStatusRequest(this, null);
        }
    }
}
