package com.navdy.service.library.events.file;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class PreviewFileRequest extends Message
{
    public static final String DEFAULT_FILENAME = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String filename;
    
    private PreviewFileRequest(final Builder builder) {
        this(builder.filename);
        this.setBuilder((Message.Builder)builder);
    }
    
    public PreviewFileRequest(final String filename) {
        this.filename = filename;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof PreviewFileRequest && this.equals(this.filename, ((PreviewFileRequest)o).filename));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.filename != null) {
                hashCode = this.filename.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<PreviewFileRequest>
    {
        public String filename;
        
        public Builder() {
        }
        
        public Builder(final PreviewFileRequest previewFileRequest) {
            super(previewFileRequest);
            if (previewFileRequest != null) {
                this.filename = previewFileRequest.filename;
            }
        }
        
        public PreviewFileRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new PreviewFileRequest(this, null);
        }
        
        public Builder filename(final String filename) {
            this.filename = filename;
            return this;
        }
    }
}
