package com.navdy.service.library.events.contacts;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class ContactRequest extends Message
{
    public static final String DEFAULT_IDENTIFIER = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String identifier;
    
    private ContactRequest(final Builder builder) {
        this(builder.identifier);
        this.setBuilder((Message.Builder)builder);
    }
    
    public ContactRequest(final String identifier) {
        this.identifier = identifier;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof ContactRequest && this.equals(this.identifier, ((ContactRequest)o).identifier));
    }
    
    @Override
    public int hashCode() {
        int hashCode = this.hashCode;
        if (hashCode == 0) {
            if (this.identifier != null) {
                hashCode = this.identifier.hashCode();
            }
            else {
                hashCode = 0;
            }
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    public static final class Builder extends Message.Builder<ContactRequest>
    {
        public String identifier;
        
        public Builder() {
        }
        
        public Builder(final ContactRequest contactRequest) {
            super(contactRequest);
            if (contactRequest != null) {
                this.identifier = contactRequest.identifier;
            }
        }
        
        public ContactRequest build() {
            return new ContactRequest(this, null);
        }
        
        public Builder identifier(final String identifier) {
            this.identifier = identifier;
            return this;
        }
    }
}
