package com.navdy.service.library.events.places;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.destination.Destination;
import com.squareup.wire.Message;

public final class SuggestedDestination extends Message
{
    public static final Integer DEFAULT_DURATION_TRAFFIC;
    public static final SuggestionType DEFAULT_TYPE;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1)
    public final Destination destination;
    @ProtoField(tag = 2, type = Datatype.UINT32)
    public final Integer duration_traffic;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final SuggestionType type;
    
    static {
        DEFAULT_DURATION_TRAFFIC = 0;
        DEFAULT_TYPE = SuggestionType.SUGGESTION_RECOMMENDATION;
    }
    
    public SuggestedDestination(final Destination destination, final Integer duration_traffic, final SuggestionType type) {
        this.destination = destination;
        this.duration_traffic = duration_traffic;
        this.type = type;
    }
    
    private SuggestedDestination(final Builder builder) {
        this(builder.destination, builder.duration_traffic, builder.type);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof SuggestedDestination)) {
                b = false;
            }
            else {
                final SuggestedDestination suggestedDestination = (SuggestedDestination)o;
                if (!this.equals(this.destination, suggestedDestination.destination) || !this.equals(this.duration_traffic, suggestedDestination.duration_traffic) || !this.equals(this.type, suggestedDestination.type)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.destination != null) {
                hashCode3 = this.destination.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.duration_traffic != null) {
                hashCode4 = this.duration_traffic.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.type != null) {
                hashCode = this.type.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<SuggestedDestination>
    {
        public Destination destination;
        public Integer duration_traffic;
        public SuggestionType type;
        
        public Builder() {
        }
        
        public Builder(final SuggestedDestination suggestedDestination) {
            super(suggestedDestination);
            if (suggestedDestination != null) {
                this.destination = suggestedDestination.destination;
                this.duration_traffic = suggestedDestination.duration_traffic;
                this.type = suggestedDestination.type;
            }
        }
        
        public SuggestedDestination build() {
            ((Message.Builder)this).checkRequiredFields();
            return new SuggestedDestination(this, null);
        }
        
        public Builder destination(final Destination destination) {
            this.destination = destination;
            return this;
        }
        
        public Builder duration_traffic(final Integer duration_traffic) {
            this.duration_traffic = duration_traffic;
            return this;
        }
        
        public Builder type(final SuggestionType type) {
            this.type = type;
            return this;
        }
    }
    
    public enum SuggestionType implements ProtoEnum
    {
        SUGGESTION_CALENDAR(1), 
        SUGGESTION_RECOMMENDATION(0);
        
        private final int value;
        
        private SuggestionType(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
