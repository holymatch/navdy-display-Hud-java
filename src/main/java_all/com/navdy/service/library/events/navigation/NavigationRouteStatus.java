package com.navdy.service.library.events.navigation;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class NavigationRouteStatus extends Message
{
    public static final String DEFAULT_HANDLE = "";
    public static final Integer DEFAULT_PROGRESS;
    public static final String DEFAULT_REQUESTID = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String handle;
    @ProtoField(tag = 2, type = Datatype.INT32)
    public final Integer progress;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String requestId;
    
    static {
        DEFAULT_PROGRESS = 0;
    }
    
    private NavigationRouteStatus(final Builder builder) {
        this(builder.handle, builder.progress, builder.requestId);
        this.setBuilder((Message.Builder)builder);
    }
    
    public NavigationRouteStatus(final String handle, final Integer progress, final String requestId) {
        this.handle = handle;
        this.progress = progress;
        this.requestId = requestId;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NavigationRouteStatus)) {
                b = false;
            }
            else {
                final NavigationRouteStatus navigationRouteStatus = (NavigationRouteStatus)o;
                if (!this.equals(this.handle, navigationRouteStatus.handle) || !this.equals(this.progress, navigationRouteStatus.progress) || !this.equals(this.requestId, navigationRouteStatus.requestId)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.handle != null) {
                hashCode3 = this.handle.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.progress != null) {
                hashCode4 = this.progress.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.requestId != null) {
                hashCode = this.requestId.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NavigationRouteStatus>
    {
        public String handle;
        public Integer progress;
        public String requestId;
        
        public Builder() {
        }
        
        public Builder(final NavigationRouteStatus navigationRouteStatus) {
            super(navigationRouteStatus);
            if (navigationRouteStatus != null) {
                this.handle = navigationRouteStatus.handle;
                this.progress = navigationRouteStatus.progress;
                this.requestId = navigationRouteStatus.requestId;
            }
        }
        
        public NavigationRouteStatus build() {
            return new NavigationRouteStatus(this, null);
        }
        
        public Builder handle(final String handle) {
            this.handle = handle;
            return this;
        }
        
        public Builder progress(final Integer progress) {
            this.progress = progress;
            return this;
        }
        
        public Builder requestId(final String requestId) {
            this.requestId = requestId;
            return this;
        }
    }
}
