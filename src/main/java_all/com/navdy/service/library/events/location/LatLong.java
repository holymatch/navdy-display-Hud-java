package com.navdy.service.library.events.location;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class LatLong extends Message
{
    public static final Double DEFAULT_LATITUDE;
    public static final Double DEFAULT_LONGITUDE;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.DOUBLE)
    public final Double latitude;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.DOUBLE)
    public final Double longitude;
    
    static {
        DEFAULT_LATITUDE = 0.0;
        DEFAULT_LONGITUDE = 0.0;
    }
    
    private LatLong(final Builder builder) {
        this(builder.latitude, builder.longitude);
        this.setBuilder((Message.Builder)builder);
    }
    
    public LatLong(final Double latitude, final Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof LatLong)) {
                b = false;
            }
            else {
                final LatLong latLong = (LatLong)o;
                if (!this.equals(this.latitude, latLong.latitude) || !this.equals(this.longitude, latLong.longitude)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.latitude != null) {
                hashCode3 = this.latitude.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.longitude != null) {
                hashCode = this.longitude.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<LatLong>
    {
        public Double latitude;
        public Double longitude;
        
        public Builder() {
        }
        
        public Builder(final LatLong latLong) {
            super(latLong);
            if (latLong != null) {
                this.latitude = latLong.latitude;
                this.longitude = latLong.longitude;
            }
        }
        
        public LatLong build() {
            ((Message.Builder)this).checkRequiredFields();
            return new LatLong(this, null);
        }
        
        public Builder latitude(final Double latitude) {
            this.latitude = latitude;
            return this;
        }
        
        public Builder longitude(final Double longitude) {
            this.longitude = longitude;
            return this;
        }
    }
}
