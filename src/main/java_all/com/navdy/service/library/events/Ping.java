package com.navdy.service.library.events;

import com.squareup.wire.Message;

public final class Ping extends Message
{
    private static final long serialVersionUID = 0L;
    
    public Ping() {
    }
    
    private Ping(final Builder builder) {
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof Ping;
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    public static final class Builder extends Message.Builder<Ping>
    {
        public Builder() {
        }
        
        public Builder(final Ping ping) {
            super(ping);
        }
        
        public Ping build() {
            return new Ping(this, null);
        }
    }
}
