package com.navdy.service.library.events.navigation;

import java.util.Collections;
import com.navdy.service.library.events.location.Coordinate;
import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import java.util.List;
import com.squareup.wire.Message;

public final class NavigationRouteResponse extends Message
{
    public static final Boolean DEFAULT_CONSIDEREDTRAFFIC;
    public static final String DEFAULT_LABEL = "";
    public static final String DEFAULT_REQUESTID = "";
    public static final List<NavigationRouteResult> DEFAULT_RESULTS;
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 6, type = Datatype.BOOL)
    public final Boolean consideredTraffic;
    @ProtoField(label = Label.REQUIRED, tag = 3)
    public final Coordinate destination;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String label;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String requestId;
    @ProtoField(label = Label.REPEATED, messageType = NavigationRouteResult.class, tag = 5)
    public final List<NavigationRouteResult> results;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_RESULTS = Collections.<NavigationRouteResult>emptyList();
        DEFAULT_CONSIDEREDTRAFFIC = false;
    }
    
    public NavigationRouteResponse(final RequestStatus status, final String statusDetail, final Coordinate destination, final String label, final List<NavigationRouteResult> list, final Boolean consideredTraffic, final String requestId) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.destination = destination;
        this.label = label;
        this.results = Message.<NavigationRouteResult>immutableCopyOf(list);
        this.consideredTraffic = consideredTraffic;
        this.requestId = requestId;
    }
    
    private NavigationRouteResponse(final Builder builder) {
        this(builder.status, builder.statusDetail, builder.destination, builder.label, builder.results, builder.consideredTraffic, builder.requestId);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NavigationRouteResponse)) {
                b = false;
            }
            else {
                final NavigationRouteResponse navigationRouteResponse = (NavigationRouteResponse)o;
                if (!this.equals(this.status, navigationRouteResponse.status) || !this.equals(this.statusDetail, navigationRouteResponse.statusDetail) || !this.equals(this.destination, navigationRouteResponse.destination) || !this.equals(this.label, navigationRouteResponse.label) || !this.equals(this.results, navigationRouteResponse.results) || !this.equals(this.consideredTraffic, navigationRouteResponse.consideredTraffic) || !this.equals(this.requestId, navigationRouteResponse.requestId)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.statusDetail != null) {
                hashCode4 = this.statusDetail.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.destination != null) {
                hashCode5 = this.destination.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.label != null) {
                hashCode6 = this.label.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.results != null) {
                hashCode7 = this.results.hashCode();
            }
            else {
                hashCode7 = 1;
            }
            int hashCode8;
            if (this.consideredTraffic != null) {
                hashCode8 = this.consideredTraffic.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            if (this.requestId != null) {
                hashCode = this.requestId.hashCode();
            }
            hashCode2 = (((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NavigationRouteResponse>
    {
        public Boolean consideredTraffic;
        public Coordinate destination;
        public String label;
        public String requestId;
        public List<NavigationRouteResult> results;
        public RequestStatus status;
        public String statusDetail;
        
        public Builder() {
        }
        
        public Builder(final NavigationRouteResponse navigationRouteResponse) {
            super(navigationRouteResponse);
            if (navigationRouteResponse != null) {
                this.status = navigationRouteResponse.status;
                this.statusDetail = navigationRouteResponse.statusDetail;
                this.destination = navigationRouteResponse.destination;
                this.label = navigationRouteResponse.label;
                this.results = (List<NavigationRouteResult>)Message.<Object>copyOf((List<Object>)navigationRouteResponse.results);
                this.consideredTraffic = navigationRouteResponse.consideredTraffic;
                this.requestId = navigationRouteResponse.requestId;
            }
        }
        
        public NavigationRouteResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NavigationRouteResponse(this, null);
        }
        
        public Builder consideredTraffic(final Boolean consideredTraffic) {
            this.consideredTraffic = consideredTraffic;
            return this;
        }
        
        public Builder destination(final Coordinate destination) {
            this.destination = destination;
            return this;
        }
        
        public Builder label(final String label) {
            this.label = label;
            return this;
        }
        
        public Builder requestId(final String requestId) {
            this.requestId = requestId;
            return this;
        }
        
        public Builder results(final List<NavigationRouteResult> list) {
            this.results = Message.Builder.<NavigationRouteResult>checkForNulls(list);
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder statusDetail(final String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }
    }
}
