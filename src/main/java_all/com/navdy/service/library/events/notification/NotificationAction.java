package com.navdy.service.library.events.notification;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class NotificationAction extends Message
{
    public static final Integer DEFAULT_ACTIONID;
    public static final String DEFAULT_HANDLER = "";
    public static final Integer DEFAULT_ICONRESOURCE;
    public static final Integer DEFAULT_ICONRESOURCEFOCUSED;
    public static final Integer DEFAULT_ID;
    public static final String DEFAULT_LABEL = "";
    public static final Integer DEFAULT_LABELID;
    public static final Integer DEFAULT_NOTIFICATIONID;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.INT32)
    public final Integer actionId;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String handler;
    @ProtoField(tag = 7, type = Datatype.INT32)
    public final Integer iconResource;
    @ProtoField(tag = 8, type = Datatype.INT32)
    public final Integer iconResourceFocused;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.INT32)
    public final Integer id;
    @ProtoField(tag = 6, type = Datatype.STRING)
    public final String label;
    @ProtoField(label = Label.REQUIRED, tag = 5, type = Datatype.INT32)
    public final Integer labelId;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.INT32)
    public final Integer notificationId;
    
    static {
        DEFAULT_ID = 0;
        DEFAULT_NOTIFICATIONID = 0;
        DEFAULT_ACTIONID = 0;
        DEFAULT_LABELID = 0;
        DEFAULT_ICONRESOURCE = 0;
        DEFAULT_ICONRESOURCEFOCUSED = 0;
    }
    
    private NotificationAction(final Builder builder) {
        this(builder.handler, builder.id, builder.notificationId, builder.actionId, builder.labelId, builder.label, builder.iconResource, builder.iconResourceFocused);
        this.setBuilder((Message.Builder)builder);
    }
    
    public NotificationAction(final String handler, final Integer id, final Integer notificationId, final Integer actionId, final Integer labelId, final String label, final Integer iconResource, final Integer iconResourceFocused) {
        this.handler = handler;
        this.id = id;
        this.notificationId = notificationId;
        this.actionId = actionId;
        this.labelId = labelId;
        this.label = label;
        this.iconResource = iconResource;
        this.iconResourceFocused = iconResourceFocused;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NotificationAction)) {
                b = false;
            }
            else {
                final NotificationAction notificationAction = (NotificationAction)o;
                if (!this.equals(this.handler, notificationAction.handler) || !this.equals(this.id, notificationAction.id) || !this.equals(this.notificationId, notificationAction.notificationId) || !this.equals(this.actionId, notificationAction.actionId) || !this.equals(this.labelId, notificationAction.labelId) || !this.equals(this.label, notificationAction.label) || !this.equals(this.iconResource, notificationAction.iconResource) || !this.equals(this.iconResourceFocused, notificationAction.iconResourceFocused)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.handler != null) {
                hashCode3 = this.handler.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.id != null) {
                hashCode4 = this.id.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.notificationId != null) {
                hashCode5 = this.notificationId.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.actionId != null) {
                hashCode6 = this.actionId.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.labelId != null) {
                hashCode7 = this.labelId.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.label != null) {
                hashCode8 = this.label.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            int hashCode9;
            if (this.iconResource != null) {
                hashCode9 = this.iconResource.hashCode();
            }
            else {
                hashCode9 = 0;
            }
            if (this.iconResourceFocused != null) {
                hashCode = this.iconResourceFocused.hashCode();
            }
            hashCode2 = ((((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode9) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NotificationAction>
    {
        public Integer actionId;
        public String handler;
        public Integer iconResource;
        public Integer iconResourceFocused;
        public Integer id;
        public String label;
        public Integer labelId;
        public Integer notificationId;
        
        public Builder() {
        }
        
        public Builder(final NotificationAction notificationAction) {
            super(notificationAction);
            if (notificationAction != null) {
                this.handler = notificationAction.handler;
                this.id = notificationAction.id;
                this.notificationId = notificationAction.notificationId;
                this.actionId = notificationAction.actionId;
                this.labelId = notificationAction.labelId;
                this.label = notificationAction.label;
                this.iconResource = notificationAction.iconResource;
                this.iconResourceFocused = notificationAction.iconResourceFocused;
            }
        }
        
        public Builder actionId(final Integer actionId) {
            this.actionId = actionId;
            return this;
        }
        
        public NotificationAction build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NotificationAction(this, null);
        }
        
        public Builder handler(final String handler) {
            this.handler = handler;
            return this;
        }
        
        public Builder iconResource(final Integer iconResource) {
            this.iconResource = iconResource;
            return this;
        }
        
        public Builder iconResourceFocused(final Integer iconResourceFocused) {
            this.iconResourceFocused = iconResourceFocused;
            return this;
        }
        
        public Builder id(final Integer id) {
            this.id = id;
            return this;
        }
        
        public Builder label(final String label) {
            this.label = label;
            return this;
        }
        
        public Builder labelId(final Integer labelId) {
            this.labelId = labelId;
            return this;
        }
        
        public Builder notificationId(final Integer notificationId) {
            this.notificationId = notificationId;
            return this;
        }
    }
}
