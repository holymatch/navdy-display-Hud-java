package com.navdy.service.library.events.obd;

import com.squareup.wire.ProtoField;
import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;

public final class ObdStatusResponse extends Message
{
    public static final Boolean DEFAULT_ISCONNECTED;
    public static final Integer DEFAULT_SPEED;
    public static final RequestStatus DEFAULT_STATUS;
    public static final String DEFAULT_VIN = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.BOOL)
    public final Boolean isConnected;
    @ProtoField(tag = 4, type = Datatype.INT32)
    public final Integer speed;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String vin;
    
    static {
        DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
        DEFAULT_ISCONNECTED = false;
        DEFAULT_SPEED = 0;
    }
    
    public ObdStatusResponse(final RequestStatus status, final Boolean isConnected, final String vin, final Integer speed) {
        this.status = status;
        this.isConnected = isConnected;
        this.vin = vin;
        this.speed = speed;
    }
    
    private ObdStatusResponse(final Builder builder) {
        this(builder.status, builder.isConnected, builder.vin, builder.speed);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof ObdStatusResponse)) {
                b = false;
            }
            else {
                final ObdStatusResponse obdStatusResponse = (ObdStatusResponse)o;
                if (!this.equals(this.status, obdStatusResponse.status) || !this.equals(this.isConnected, obdStatusResponse.isConnected) || !this.equals(this.vin, obdStatusResponse.vin) || !this.equals(this.speed, obdStatusResponse.speed)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.status != null) {
                hashCode3 = this.status.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.isConnected != null) {
                hashCode4 = this.isConnected.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.vin != null) {
                hashCode5 = this.vin.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            if (this.speed != null) {
                hashCode = this.speed.hashCode();
            }
            hashCode2 = ((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<ObdStatusResponse>
    {
        public Boolean isConnected;
        public Integer speed;
        public RequestStatus status;
        public String vin;
        
        public Builder() {
        }
        
        public Builder(final ObdStatusResponse obdStatusResponse) {
            super(obdStatusResponse);
            if (obdStatusResponse != null) {
                this.status = obdStatusResponse.status;
                this.isConnected = obdStatusResponse.isConnected;
                this.vin = obdStatusResponse.vin;
                this.speed = obdStatusResponse.speed;
            }
        }
        
        public ObdStatusResponse build() {
            ((Message.Builder)this).checkRequiredFields();
            return new ObdStatusResponse(this, null);
        }
        
        public Builder isConnected(final Boolean isConnected) {
            this.isConnected = isConnected;
            return this;
        }
        
        public Builder speed(final Integer speed) {
            this.speed = speed;
            return this;
        }
        
        public Builder status(final RequestStatus status) {
            this.status = status;
            return this;
        }
        
        public Builder vin(final String vin) {
            this.vin = vin;
            return this;
        }
    }
}
