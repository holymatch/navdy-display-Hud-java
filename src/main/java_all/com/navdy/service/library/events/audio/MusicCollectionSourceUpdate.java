package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class MusicCollectionSourceUpdate extends Message
{
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    public static final Long DEFAULT_SERIAL_NUMBER;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(tag = 2, type = Datatype.INT64)
    public final Long serial_number;
    
    static {
        DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_SERIAL_NUMBER = 0L;
    }
    
    public MusicCollectionSourceUpdate(final MusicCollectionSource collectionSource, final Long serial_number) {
        this.collectionSource = collectionSource;
        this.serial_number = serial_number;
    }
    
    private MusicCollectionSourceUpdate(final Builder builder) {
        this(builder.collectionSource, builder.serial_number);
        this.setBuilder((Message.Builder)builder);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof MusicCollectionSourceUpdate)) {
                b = false;
            }
            else {
                final MusicCollectionSourceUpdate musicCollectionSourceUpdate = (MusicCollectionSourceUpdate)o;
                if (!this.equals(this.collectionSource, musicCollectionSourceUpdate.collectionSource) || !this.equals(this.serial_number, musicCollectionSourceUpdate.serial_number)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.collectionSource != null) {
                hashCode3 = this.collectionSource.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            if (this.serial_number != null) {
                hashCode = this.serial_number.hashCode();
            }
            hashCode2 = hashCode3 * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<MusicCollectionSourceUpdate>
    {
        public MusicCollectionSource collectionSource;
        public Long serial_number;
        
        public Builder() {
        }
        
        public Builder(final MusicCollectionSourceUpdate musicCollectionSourceUpdate) {
            super(musicCollectionSourceUpdate);
            if (musicCollectionSourceUpdate != null) {
                this.collectionSource = musicCollectionSourceUpdate.collectionSource;
                this.serial_number = musicCollectionSourceUpdate.serial_number;
            }
        }
        
        public MusicCollectionSourceUpdate build() {
            return new MusicCollectionSourceUpdate(this, null);
        }
        
        public Builder collectionSource(final MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }
        
        public Builder serial_number(final Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }
    }
}
