package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class AudioStatus extends Message
{
    public static final ConnectionType DEFAULT_CONNECTIONTYPE;
    public static final Boolean DEFAULT_HASSCOCONNECTION;
    public static final Boolean DEFAULT_ISCONNECTED;
    public static final Boolean DEFAULT_ISPLAYING;
    public static final ProfileType DEFAULT_PROFILETYPE;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final ConnectionType connectionType;
    @ProtoField(tag = 5, type = Datatype.BOOL)
    public final Boolean hasSCOConnection;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean isConnected;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean isPlaying;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final ProfileType profileType;
    
    static {
        DEFAULT_ISCONNECTED = false;
        DEFAULT_CONNECTIONTYPE = ConnectionType.AUDIO_CONNECTION_NONE;
        DEFAULT_PROFILETYPE = ProfileType.AUDIO_PROFILE_NONE;
        DEFAULT_ISPLAYING = false;
        DEFAULT_HASSCOCONNECTION = false;
    }
    
    private AudioStatus(final Builder builder) {
        this(builder.isConnected, builder.connectionType, builder.profileType, builder.isPlaying, builder.hasSCOConnection);
        this.setBuilder((Message.Builder)builder);
    }
    
    public AudioStatus(final Boolean isConnected, final ConnectionType connectionType, final ProfileType profileType, final Boolean isPlaying, final Boolean hasSCOConnection) {
        this.isConnected = isConnected;
        this.connectionType = connectionType;
        this.profileType = profileType;
        this.isPlaying = isPlaying;
        this.hasSCOConnection = hasSCOConnection;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof AudioStatus)) {
                b = false;
            }
            else {
                final AudioStatus audioStatus = (AudioStatus)o;
                if (!this.equals(this.isConnected, audioStatus.isConnected) || !this.equals(this.connectionType, audioStatus.connectionType) || !this.equals(this.profileType, audioStatus.profileType) || !this.equals(this.isPlaying, audioStatus.isPlaying) || !this.equals(this.hasSCOConnection, audioStatus.hasSCOConnection)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.isConnected != null) {
                hashCode3 = this.isConnected.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.connectionType != null) {
                hashCode4 = this.connectionType.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.profileType != null) {
                hashCode5 = this.profileType.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.isPlaying != null) {
                hashCode6 = this.isPlaying.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            if (this.hasSCOConnection != null) {
                hashCode = this.hasSCOConnection.hashCode();
            }
            hashCode2 = (((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<AudioStatus>
    {
        public ConnectionType connectionType;
        public Boolean hasSCOConnection;
        public Boolean isConnected;
        public Boolean isPlaying;
        public ProfileType profileType;
        
        public Builder() {
        }
        
        public Builder(final AudioStatus audioStatus) {
            super(audioStatus);
            if (audioStatus != null) {
                this.isConnected = audioStatus.isConnected;
                this.connectionType = audioStatus.connectionType;
                this.profileType = audioStatus.profileType;
                this.isPlaying = audioStatus.isPlaying;
                this.hasSCOConnection = audioStatus.hasSCOConnection;
            }
        }
        
        public AudioStatus build() {
            return new AudioStatus(this, null);
        }
        
        public Builder connectionType(final ConnectionType connectionType) {
            this.connectionType = connectionType;
            return this;
        }
        
        public Builder hasSCOConnection(final Boolean hasSCOConnection) {
            this.hasSCOConnection = hasSCOConnection;
            return this;
        }
        
        public Builder isConnected(final Boolean isConnected) {
            this.isConnected = isConnected;
            return this;
        }
        
        public Builder isPlaying(final Boolean isPlaying) {
            this.isPlaying = isPlaying;
            return this;
        }
        
        public Builder profileType(final ProfileType profileType) {
            this.profileType = profileType;
            return this;
        }
    }
    
    public enum ConnectionType implements ProtoEnum
    {
        AUDIO_CONNECTION_AUX(4), 
        AUDIO_CONNECTION_BLUETOOTH(2), 
        AUDIO_CONNECTION_NONE(1), 
        AUDIO_CONNECTION_SPEAKER(3);
        
        private final int value;
        
        private ConnectionType(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
    
    public enum ProfileType implements ProtoEnum
    {
        AUDIO_PROFILE_A2DP(2), 
        AUDIO_PROFILE_HFP(3), 
        AUDIO_PROFILE_NONE(1);
        
        private final int value;
        
        private ProfileType(final int value) {
            this.value = value;
        }
        
        @Override
        public int getValue() {
            return this.value;
        }
    }
}
