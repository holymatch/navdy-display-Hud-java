package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class MusicArtworkRequest extends Message
{
    public static final String DEFAULT_ALBUM = "";
    public static final String DEFAULT_AUTHOR = "";
    public static final String DEFAULT_COLLECTIONID = "";
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    public static final MusicCollectionType DEFAULT_COLLECTIONTYPE;
    public static final String DEFAULT_NAME = "";
    public static final Integer DEFAULT_SIZE;
    private static final long serialVersionUID = 0L;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String album;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String author;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String collectionId;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(tag = 6, type = Datatype.ENUM)
    public final MusicCollectionType collectionType;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 5, type = Datatype.UINT32)
    public final Integer size;
    
    static {
        DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_SIZE = 0;
        DEFAULT_COLLECTIONTYPE = MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    }
    
    private MusicArtworkRequest(final Builder builder) {
        this(builder.collectionSource, builder.name, builder.album, builder.author, builder.size, builder.collectionType, builder.collectionId);
        this.setBuilder((Message.Builder)builder);
    }
    
    public MusicArtworkRequest(final MusicCollectionSource collectionSource, final String name, final String album, final String author, final Integer size, final MusicCollectionType collectionType, final String collectionId) {
        this.collectionSource = collectionSource;
        this.name = name;
        this.album = album;
        this.author = author;
        this.size = size;
        this.collectionType = collectionType;
        this.collectionId = collectionId;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof MusicArtworkRequest)) {
                b = false;
            }
            else {
                final MusicArtworkRequest musicArtworkRequest = (MusicArtworkRequest)o;
                if (!this.equals(this.collectionSource, musicArtworkRequest.collectionSource) || !this.equals(this.name, musicArtworkRequest.name) || !this.equals(this.album, musicArtworkRequest.album) || !this.equals(this.author, musicArtworkRequest.author) || !this.equals(this.size, musicArtworkRequest.size) || !this.equals(this.collectionType, musicArtworkRequest.collectionType) || !this.equals(this.collectionId, musicArtworkRequest.collectionId)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.collectionSource != null) {
                hashCode3 = this.collectionSource.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.name != null) {
                hashCode4 = this.name.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.album != null) {
                hashCode5 = this.album.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.author != null) {
                hashCode6 = this.author.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            int hashCode7;
            if (this.size != null) {
                hashCode7 = this.size.hashCode();
            }
            else {
                hashCode7 = 0;
            }
            int hashCode8;
            if (this.collectionType != null) {
                hashCode8 = this.collectionType.hashCode();
            }
            else {
                hashCode8 = 0;
            }
            if (this.collectionId != null) {
                hashCode = this.collectionId.hashCode();
            }
            hashCode2 = (((((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode7) * 37 + hashCode8) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<MusicArtworkRequest>
    {
        public String album;
        public String author;
        public String collectionId;
        public MusicCollectionSource collectionSource;
        public MusicCollectionType collectionType;
        public String name;
        public Integer size;
        
        public Builder() {
        }
        
        public Builder(final MusicArtworkRequest musicArtworkRequest) {
            super(musicArtworkRequest);
            if (musicArtworkRequest != null) {
                this.collectionSource = musicArtworkRequest.collectionSource;
                this.name = musicArtworkRequest.name;
                this.album = musicArtworkRequest.album;
                this.author = musicArtworkRequest.author;
                this.size = musicArtworkRequest.size;
                this.collectionType = musicArtworkRequest.collectionType;
                this.collectionId = musicArtworkRequest.collectionId;
            }
        }
        
        public Builder album(final String album) {
            this.album = album;
            return this;
        }
        
        public Builder author(final String author) {
            this.author = author;
            return this;
        }
        
        public MusicArtworkRequest build() {
            return new MusicArtworkRequest(this, null);
        }
        
        public Builder collectionId(final String collectionId) {
            this.collectionId = collectionId;
            return this;
        }
        
        public Builder collectionSource(final MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }
        
        public Builder collectionType(final MusicCollectionType collectionType) {
            this.collectionType = collectionType;
            return this;
        }
        
        public Builder name(final String name) {
            this.name = name;
            return this;
        }
        
        public Builder size(final Integer size) {
            this.size = size;
            return this;
        }
    }
}
