package com.navdy.service.library.events.photo;

import com.squareup.wire.ProtoField;
import okio.ByteString;
import com.squareup.wire.Message;

public final class PhotoUpdate extends Message
{
    public static final String DEFAULT_IDENTIFIER = "";
    public static final ByteString DEFAULT_PHOTO;
    public static final PhotoType DEFAULT_PHOTOTYPE;
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String identifier;
    @ProtoField(tag = 2, type = Datatype.BYTES)
    public final ByteString photo;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final PhotoType photoType;
    
    static {
        DEFAULT_PHOTO = ByteString.EMPTY;
        DEFAULT_PHOTOTYPE = PhotoType.PHOTO_ALBUM_ART;
    }
    
    private PhotoUpdate(final Builder builder) {
        this(builder.identifier, builder.photo, builder.photoType);
        this.setBuilder((Message.Builder)builder);
    }
    
    public PhotoUpdate(final String identifier, final ByteString photo, final PhotoType photoType) {
        this.identifier = identifier;
        this.photo = photo;
        this.photoType = photoType;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof PhotoUpdate)) {
                b = false;
            }
            else {
                final PhotoUpdate photoUpdate = (PhotoUpdate)o;
                if (!this.equals(this.identifier, photoUpdate.identifier) || !this.equals(this.photo, photoUpdate.photo) || !this.equals(this.photoType, photoUpdate.photoType)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.identifier != null) {
                hashCode3 = this.identifier.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.photo != null) {
                hashCode4 = this.photo.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            if (this.photoType != null) {
                hashCode = this.photoType.hashCode();
            }
            hashCode2 = (hashCode3 * 37 + hashCode4) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<PhotoUpdate>
    {
        public String identifier;
        public ByteString photo;
        public PhotoType photoType;
        
        public Builder() {
        }
        
        public Builder(final PhotoUpdate photoUpdate) {
            super(photoUpdate);
            if (photoUpdate != null) {
                this.identifier = photoUpdate.identifier;
                this.photo = photoUpdate.photo;
                this.photoType = photoUpdate.photoType;
            }
        }
        
        public PhotoUpdate build() {
            ((Message.Builder)this).checkRequiredFields();
            return new PhotoUpdate(this, null);
        }
        
        public Builder identifier(final String identifier) {
            this.identifier = identifier;
            return this;
        }
        
        public Builder photo(final ByteString photo) {
            this.photo = photo;
            return this;
        }
        
        public Builder photoType(final PhotoType photoType) {
            this.photoType = photoType;
            return this;
        }
    }
}
