package com.navdy.service.library.events.navigation;

import com.squareup.wire.ProtoField;
import com.squareup.wire.Message;

public final class NavigationSessionDeferRequest extends Message
{
    public static final Integer DEFAULT_DELAY;
    public static final Long DEFAULT_EXPIRETIMESTAMP;
    public static final Boolean DEFAULT_ORIGINDISPLAY;
    public static final String DEFAULT_REQUESTID = "";
    public static final String DEFAULT_ROUTEID = "";
    private static final long serialVersionUID = 0L;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.INT32)
    public final Integer delay;
    @ProtoField(tag = 5, type = Datatype.INT64)
    public final Long expireTimestamp;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean originDisplay;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String requestId;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String routeId;
    
    static {
        DEFAULT_DELAY = 0;
        DEFAULT_ORIGINDISPLAY = false;
        DEFAULT_EXPIRETIMESTAMP = 0L;
    }
    
    private NavigationSessionDeferRequest(final Builder builder) {
        this(builder.requestId, builder.routeId, builder.delay, builder.originDisplay, builder.expireTimestamp);
        this.setBuilder((Message.Builder)builder);
    }
    
    public NavigationSessionDeferRequest(final String requestId, final String routeId, final Integer delay, final Boolean originDisplay, final Long expireTimestamp) {
        this.requestId = requestId;
        this.routeId = routeId;
        this.delay = delay;
        this.originDisplay = originDisplay;
        this.expireTimestamp = expireTimestamp;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o != this) {
            if (!(o instanceof NavigationSessionDeferRequest)) {
                b = false;
            }
            else {
                final NavigationSessionDeferRequest navigationSessionDeferRequest = (NavigationSessionDeferRequest)o;
                if (!this.equals(this.requestId, navigationSessionDeferRequest.requestId) || !this.equals(this.routeId, navigationSessionDeferRequest.routeId) || !this.equals(this.delay, navigationSessionDeferRequest.delay) || !this.equals(this.originDisplay, navigationSessionDeferRequest.originDisplay) || !this.equals(this.expireTimestamp, navigationSessionDeferRequest.expireTimestamp)) {
                    b = false;
                }
            }
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        int hashCode = 0;
        int hashCode2;
        if ((hashCode2 = this.hashCode) == 0) {
            int hashCode3;
            if (this.requestId != null) {
                hashCode3 = this.requestId.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            int hashCode4;
            if (this.routeId != null) {
                hashCode4 = this.routeId.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            int hashCode5;
            if (this.delay != null) {
                hashCode5 = this.delay.hashCode();
            }
            else {
                hashCode5 = 0;
            }
            int hashCode6;
            if (this.originDisplay != null) {
                hashCode6 = this.originDisplay.hashCode();
            }
            else {
                hashCode6 = 0;
            }
            if (this.expireTimestamp != null) {
                hashCode = this.expireTimestamp.hashCode();
            }
            hashCode2 = (((hashCode3 * 37 + hashCode4) * 37 + hashCode5) * 37 + hashCode6) * 37 + hashCode;
            this.hashCode = hashCode2;
        }
        return hashCode2;
    }
    
    public static final class Builder extends Message.Builder<NavigationSessionDeferRequest>
    {
        public Integer delay;
        public Long expireTimestamp;
        public Boolean originDisplay;
        public String requestId;
        public String routeId;
        
        public Builder() {
        }
        
        public Builder(final NavigationSessionDeferRequest navigationSessionDeferRequest) {
            super(navigationSessionDeferRequest);
            if (navigationSessionDeferRequest != null) {
                this.requestId = navigationSessionDeferRequest.requestId;
                this.routeId = navigationSessionDeferRequest.routeId;
                this.delay = navigationSessionDeferRequest.delay;
                this.originDisplay = navigationSessionDeferRequest.originDisplay;
                this.expireTimestamp = navigationSessionDeferRequest.expireTimestamp;
            }
        }
        
        public NavigationSessionDeferRequest build() {
            ((Message.Builder)this).checkRequiredFields();
            return new NavigationSessionDeferRequest(this, null);
        }
        
        public Builder delay(final Integer delay) {
            this.delay = delay;
            return this;
        }
        
        public Builder expireTimestamp(final Long expireTimestamp) {
            this.expireTimestamp = expireTimestamp;
            return this;
        }
        
        public Builder originDisplay(final Boolean originDisplay) {
            this.originDisplay = originDisplay;
            return this;
        }
        
        public Builder requestId(final String requestId) {
            this.requestId = requestId;
            return this;
        }
        
        public Builder routeId(final String routeId) {
            this.routeId = routeId;
            return this;
        }
    }
}
