package com.navdy.service.library.log;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import android.support.annotation.NonNull;
import java.io.Serializable;
import java.io.IOException;
import java.util.ArrayList;
import java.io.PrintWriter;
import java.io.Writer;
import java.io.StringWriter;
import java.net.UnknownHostException;
import android.content.SharedPreferences;
import com.navdy.service.library.util.IOUtils;
import android.util.Log;
import java.io.File;
import com.navdy.service.library.util.SystemUtils;
import android.os.Process;
import android.text.TextUtils;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.nio.MappedByteBuffer;
import java.text.DateFormat;
import android.content.SharedPreferences;
import android.content.Context;

public class MemoryMapFileAppender implements LogAppender
{
    private static final String COLON = ":";
    private static final String CURRENT_POINTER_PREF = "pointer";
    private static final String CURRENT_POINTER_PREF_FILE_SUFFIX = "_current_log_pointer";
    private static final long MIN_FILE_SIZE = 16384L;
    private static final String NEWLINE = "\r\n";
    private static final byte[] ROLLOVER_MARKER;
    private static final int ROLLOVER_META_LEN;
    private static final String SLASH = "/";
    private static final String SPACE = " ";
    private static final String TAG = "MemoryMapFileAppender";
    private StringBuilder builder;
    private Context context;
    private SharedPreferences$Editor currentPointerPrefEditor;
    private String currentPointerPrefFileName;
    private DateFormat dateFormat;
    private String fileName;
    private long fileSize;
    private int maxFiles;
    private MappedByteBuffer memoryMap;
    
    static {
        ROLLOVER_MARKER = "\r\n<<<<rolling>>>>\r\n".getBytes();
        ROLLOVER_META_LEN = MemoryMapFileAppender.ROLLOVER_MARKER.length;
    }
    
    public MemoryMapFileAppender(final Context context, final String s, final String s2, final long n, final int n2) {
        this(context, s, s2, n, n2, true);
    }
    
    public MemoryMapFileAppender(final Context context, final String s, final String s2, final long n, int int1, final boolean b) {
        this.maxFiles = 0;
        this.builder = new StringBuilder(20480);
        this.dateFormat = new SimpleDateFormat("MM-dd HH:mm:ss.SSS", Locale.US);
        if (context == null || TextUtils.isEmpty((CharSequence)s2)) {
            throw new IllegalArgumentException();
        }
        this.maxFiles = int1;
        long fileSize = n;
        if (n < 16384L) {
            fileSize = 16384L;
        }
        String string = "";
        if (b) {
            string = "_" + SystemUtils.getProcessName(context, Process.myPid());
        }
        this.context = context;
        this.fileName = s + File.separator + s2 + string;
        this.fileSize = fileSize;
        this.currentPointerPrefFileName = string + "_current_log_pointer";
        try {
            Log.d("MemoryMapFileAppender", "MemoryMapFileAppender::ctor::start");
            IOUtils.createDirectory(s);
            final SharedPreferences sharedPreferences = context.getSharedPreferences(this.currentPointerPrefFileName, 0);
            int1 = sharedPreferences.getInt("pointer", 0);
            if (int1 > 0 && int1 < fileSize) {
                sharedPreferences.edit().remove("pointer").apply();
                Log.i("MemoryMapFileAppender", "MemoryMapFileAppender, pos set = " + int1 + " for " + this.fileName);
            }
            this.setMemoryMap(this.fileName + ".txt", int1, fileSize);
            Log.d("MemoryMapFileAppender", "MemoryMapFileAppender::ctor::end");
        }
        catch (Throwable t) {
            Log.e("MemoryMapFileAppender", "MemoryMapFileAppender.ctor()::", t);
        }
    }
    
    private String getFormatedNumber(final int n) {
        return String.format("%0" + (int)(Math.log10(this.maxFiles) + 1.0) + "d", n);
    }
    
    public static String getStackTraceString(final Throwable t) {
        String string;
        if (t == null) {
            string = "";
        }
        else {
            for (Throwable cause = t; cause != null; cause = cause.getCause()) {
                if (cause instanceof UnknownHostException) {
                    string = "";
                    return string;
                }
            }
            final StringWriter stringWriter = new StringWriter();
            final FastPrintWriter fastPrintWriter = new FastPrintWriter(stringWriter, false, 256);
            t.printStackTrace(fastPrintWriter);
            fastPrintWriter.flush();
            string = stringWriter.toString();
        }
        return string;
    }
    
    private void rollFiles() {
        if (this.maxFiles > 0) {
            IOUtils.deleteFile(this.context, this.fileName + "." + this.getFormatedNumber(0) + ".txt");
        }
        for (int i = 0; i < this.maxFiles; ++i) {
            File file;
            if (i == this.maxFiles - 1) {
                file = new File(this.fileName + ".txt");
            }
            else {
                file = new File(this.fileName + "." + this.getFormatedNumber(i + 1) + ".txt");
            }
            if (file.exists() && !file.renameTo(new File(this.fileName + "." + this.getFormatedNumber(i) + ".txt"))) {
                Log.w("MemoryMapFileAppender", "Unable to rename " + this.fileName + ".txt");
            }
        }
    }
    
    private void rollOver() {
        synchronized (this) {
            if (this.maxFiles == 0) {
                this.memoryMap.position(0);
                this.memoryMap.put(MemoryMapFileAppender.ROLLOVER_MARKER);
            }
            else {
                this.memoryMap.force();
                this.rollFiles();
                this.setMemoryMap(this.fileName + ".txt", 0, this.fileSize);
            }
        }
    }
    
    private void setMemoryMap(final String p0, final int p1, final long p2) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          5
        //     3: aconst_null    
        //     4: astore          6
        //     6: aload           6
        //     8: astore          7
        //    10: new             Ljava/io/File;
        //    13: astore          8
        //    15: aload           6
        //    17: astore          7
        //    19: aload           8
        //    21: aload_1        
        //    22: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    25: iload_2        
        //    26: ifne            49
        //    29: aload           6
        //    31: astore          7
        //    33: aload           8
        //    35: invokevirtual   java/io/File.exists:()Z
        //    38: ifeq            49
        //    41: aload           6
        //    43: astore          7
        //    45: aload_0        
        //    46: invokespecial   com/navdy/service/library/log/MemoryMapFileAppender.rollFiles:()V
        //    49: aload           6
        //    51: astore          7
        //    53: new             Ljava/io/RandomAccessFile;
        //    56: astore          8
        //    58: aload           6
        //    60: astore          7
        //    62: aload           8
        //    64: aload_1        
        //    65: ldc_w           "rw"
        //    68: invokespecial   java/io/RandomAccessFile.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //    71: aload           5
        //    73: astore_1       
        //    74: aload           8
        //    76: invokevirtual   java/io/RandomAccessFile.getChannel:()Ljava/nio/channels/FileChannel;
        //    79: astore          6
        //    81: aload           6
        //    83: astore_1       
        //    84: aload_0        
        //    85: aload           6
        //    87: getstatic       java/nio/channels/FileChannel$MapMode.READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;
        //    90: lconst_0       
        //    91: lconst_1       
        //    92: lload_3        
        //    93: ladd           
        //    94: invokevirtual   java/nio/channels/FileChannel.map:(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
        //    97: putfield        com/navdy/service/library/log/MemoryMapFileAppender.memoryMap:Ljava/nio/MappedByteBuffer;
        //   100: iload_2        
        //   101: ifle            116
        //   104: aload           6
        //   106: astore_1       
        //   107: aload_0        
        //   108: getfield        com/navdy/service/library/log/MemoryMapFileAppender.memoryMap:Ljava/nio/MappedByteBuffer;
        //   111: iload_2        
        //   112: invokevirtual   java/nio/MappedByteBuffer.position:(I)Ljava/nio/Buffer;
        //   115: pop            
        //   116: aload           6
        //   118: astore_1       
        //   119: aload           8
        //   121: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   124: aload           6
        //   126: astore          7
        //   128: aload           6
        //   130: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   133: return         
        //   134: aconst_null    
        //   135: astore          6
        //   137: astore          8
        //   139: aload           7
        //   141: astore_1       
        //   142: aload           6
        //   144: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   147: aload_1        
        //   148: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //   151: aload_0        
        //   152: aconst_null    
        //   153: putfield        com/navdy/service/library/log/MemoryMapFileAppender.memoryMap:Ljava/nio/MappedByteBuffer;
        //   156: ldc             "MemoryMapFileAppender"
        //   158: ldc_w           "setMemoryMap::"
        //   161: aload           8
        //   163: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   166: pop            
        //   167: goto            133
        //   170: astore          7
        //   172: aload           8
        //   174: astore          6
        //   176: aload           7
        //   178: astore          8
        //   180: goto            142
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  10     15     134    142    Ljava/lang/Throwable;
        //  19     25     134    142    Ljava/lang/Throwable;
        //  33     41     134    142    Ljava/lang/Throwable;
        //  45     49     134    142    Ljava/lang/Throwable;
        //  53     58     134    142    Ljava/lang/Throwable;
        //  62     71     134    142    Ljava/lang/Throwable;
        //  74     81     170    183    Ljava/lang/Throwable;
        //  84     100    170    183    Ljava/lang/Throwable;
        //  107    116    170    183    Ljava/lang/Throwable;
        //  119    124    170    183    Ljava/lang/Throwable;
        //  128    133    134    142    Ljava/lang/Throwable;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0116:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void truncateFileAtCurrentPosition(final String p0) {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/navdy/service/library/log/MemoryMapFileAppender.memoryMap:Ljava/nio/MappedByteBuffer;
        //     4: invokevirtual   java/nio/MappedByteBuffer.position:()I
        //     7: i2l            
        //     8: lstore_2       
        //     9: aconst_null    
        //    10: astore          4
        //    12: aconst_null    
        //    13: astore          5
        //    15: aconst_null    
        //    16: astore          6
        //    18: aload           5
        //    20: astore          7
        //    22: new             Ljava/io/RandomAccessFile;
        //    25: astore          8
        //    27: aload           5
        //    29: astore          7
        //    31: aload           8
        //    33: aload_1        
        //    34: ldc_w           "rws"
        //    37: invokespecial   java/io/RandomAccessFile.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //    40: aload           8
        //    42: lload_2        
        //    43: invokevirtual   java/io/RandomAccessFile.setLength:(J)V
        //    46: aload           8
        //    48: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    51: return         
        //    52: astore          8
        //    54: aload           6
        //    56: astore_1       
        //    57: aload_1        
        //    58: astore          7
        //    60: aload           8
        //    62: invokevirtual   java/io/FileNotFoundException.printStackTrace:()V
        //    65: aload_1        
        //    66: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    69: goto            51
        //    72: astore          8
        //    74: aload           4
        //    76: astore_1       
        //    77: aload_1        
        //    78: astore          7
        //    80: aload           8
        //    82: invokevirtual   java/io/IOException.printStackTrace:()V
        //    85: aload_1        
        //    86: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    89: goto            51
        //    92: astore_1       
        //    93: aload           7
        //    95: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
        //    98: aload_1        
        //    99: athrow         
        //   100: astore_1       
        //   101: aload           8
        //   103: astore          7
        //   105: goto            93
        //   108: astore          7
        //   110: aload           8
        //   112: astore_1       
        //   113: aload           7
        //   115: astore          8
        //   117: goto            77
        //   120: astore          7
        //   122: aload           8
        //   124: astore_1       
        //   125: aload           7
        //   127: astore          8
        //   129: goto            57
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  22     27     52     57     Ljava/io/FileNotFoundException;
        //  22     27     72     77     Ljava/io/IOException;
        //  22     27     92     93     Any
        //  31     40     52     57     Ljava/io/FileNotFoundException;
        //  31     40     72     77     Ljava/io/IOException;
        //  31     40     92     93     Any
        //  40     46     120    132    Ljava/io/FileNotFoundException;
        //  40     46     108    120    Ljava/io/IOException;
        //  40     46     100    108    Any
        //  60     65     92     93     Any
        //  80     85     92     93     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0051:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    protected void append(final String s) {
        try {
            if (this.memoryMap != null && s != null) {
                final byte[] bytes = s.getBytes();
                final int length = bytes.length;
                if (MemoryMapFileAppender.ROLLOVER_META_LEN + length < this.fileSize) {
                    if (this.memoryMap.position() + length >= this.fileSize) {
                        this.rollOver();
                    }
                    this.memoryMap.put(bytes);
                }
            }
        }
        catch (Throwable t) {
            Log.e("MemoryMapFileAppender", "MemoryMapFileAppender.append()::", t);
        }
    }
    
    protected void append(final String s, final String s2, final String s3, final Throwable t) {
        try {
            synchronized (this) {
                this.builder.setLength(0);
                this.builder.append(this.dateFormat.format(System.currentTimeMillis()));
                this.builder.append(" ");
                this.builder.append(s);
                this.builder.append("/");
                this.builder.append(s2);
                this.builder.append(":");
                this.builder.append(" ");
                if (s3 != null) {
                    this.builder.append(s3);
                }
                if (t != null) {
                    this.builder.append("\r\n");
                    this.builder.append(getStackTraceString(t));
                }
                this.builder.append("\r\n");
                this.append(this.builder.toString());
            }
        }
        catch (OutOfMemoryError outOfMemoryError) {
            outOfMemoryError.printStackTrace();
        }
    }
    
    @Override
    public void close() {
        synchronized (this) {
            Log.d("MemoryMapFileAppender", "MemoryMapFileAppender:closing");
            this.flush();
            this.memoryMap = null;
        }
    }
    
    @Override
    public void d(final String s, final String s2) {
        this.append("D", s, s2, null);
    }
    
    @Override
    public void d(final String s, final String s2, final Throwable t) {
        this.append("D", s, s2, t);
    }
    
    @Override
    public void e(final String s, final String s2) {
        this.append("W", s, s2, null);
    }
    
    @Override
    public void e(final String s, final String s2, final Throwable t) {
        this.append("W", s, s2, t);
    }
    
    @Override
    public void flush() {
        synchronized (this) {
            try {
                if (this.memoryMap != null) {
                    final int position = this.memoryMap.position();
                    this.memoryMap.force();
                    if (position > 0) {
                        if (this.currentPointerPrefEditor == null) {
                            this.currentPointerPrefEditor = this.context.getSharedPreferences(this.currentPointerPrefFileName, 0).edit();
                        }
                        this.currentPointerPrefEditor.putInt("pointer", position).commit();
                        Log.d("MemoryMapFileAppender", "MemoryMapFileAppender:stored pref pos:" + position);
                    }
                }
            }
            catch (Throwable t) {
                Log.d("MemoryMapFileAppender", "MemoryMapFileAppender:flush", t);
            }
        }
    }
    
    @NonNull
    public ArrayList<File> getLogFiles() {
        final ArrayList<String> list = (ArrayList<String>)new ArrayList<File>(this.maxFiles);
        int i = 0;
    Label_0106_Outer:
        while (i < this.maxFiles) {
            Serializable string = this.fileName + "." + this.getFormatedNumber(i) + ".txt";
            while (true) {
                if (i == this.maxFiles - 1) {
                    try {
                        IOUtils.copyFile(this.fileName + ".txt", (String)string);
                        this.truncateFileAtCurrentPosition((String)string);
                        string = new File((String)string);
                        if (((File)string).exists() && ((File)string).length() > 0L) {
                            list.add((File)string);
                        }
                        ++i;
                        continue Label_0106_Outer;
                    }
                    catch (IOException ex) {
                        ex.printStackTrace();
                        continue;
                    }
                    break;
                }
                continue;
            }
        }
        return (ArrayList<File>)list;
    }
    
    @Override
    public void i(final String s, final String s2) {
        this.append("I", s, s2, null);
    }
    
    @Override
    public void i(final String s, final String s2, final Throwable t) {
        this.append("I", s, s2, t);
    }
    
    @Override
    public void v(final String s, final String s2) {
        this.append("V", s, s2, null);
    }
    
    @Override
    public void v(final String s, final String s2, final Throwable t) {
        this.append("V", s, s2, t);
    }
    
    @Override
    public void w(final String s, final String s2) {
        this.append("W", s, s2, null);
    }
    
    @Override
    public void w(final String s, final String s2, final Throwable t) {
        this.append("W", s, s2, t);
    }
}
