package com.navdy.service.library.log;

import android.util.Log;

public final class Logger
{
    public static final String ACTION_RELOAD = "com.navdy.service.library.log.action.RELOAD";
    public static final String DEFAULT_TAG = "Navdy";
    private static volatile long logLevelChange;
    private static LogAppender[] sAppenders;
    private volatile boolean[] loggable;
    private long startTime;
    private final String tagName;
    private volatile long timestamp;
    
    static {
        Logger.sAppenders = new LogAppender[0];
        Logger.logLevelChange = -1L;
    }
    
    public Logger(final Class clazz) {
        this.timestamp = -2L;
        if (clazz != null) {
            final String simpleName = clazz.getSimpleName();
            this.tagName = simpleName.substring(0, Math.min(simpleName.length(), 23));
        }
        else {
            this.tagName = "Navdy";
        }
    }
    
    public Logger(final String s) {
        this.timestamp = -2L;
        if (s != null) {
            this.tagName = s.substring(0, Math.min(s.length(), 23));
        }
        else {
            this.tagName = "Navdy";
        }
    }
    
    public static void addAppender(final LogAppender logAppender) {
        // monitorenter(clazz = Logger.class)
        if (logAppender == null) {
            try {
                throw new IllegalArgumentException();
            }
            finally {
            }
            // monitorexit(clazz)
        }
        final LogAppender[] sAppenders = new LogAppender[Logger.sAppenders.length + 1];
        for (int i = 0; i < sAppenders.length; ++i) {
            if (i < sAppenders.length - 1) {
                sAppenders[i] = Logger.sAppenders[i];
            }
            else {
                sAppenders[i] = logAppender;
            }
        }
        Logger.sAppenders = sAppenders;
    }
    // monitorexit(clazz)
    
    public static void close() {
        for (int i = 0; i < Logger.sAppenders.length; ++i) {
            Logger.sAppenders[i].close();
        }
    }
    
    public static void flush() {
        for (int i = 0; i < Logger.sAppenders.length; ++i) {
            Logger.sAppenders[i].flush();
        }
    }
    
    public static void init(final LogAppender[] array) {
        if (array == null || array.length == 0) {
            throw new IllegalArgumentException();
        }
        Logger.sAppenders = array.clone();
    }
    
    public static void reloadLogLevels() {
        Logger.logLevelChange = System.currentTimeMillis();
    }
    
    public void d(final String s) {
        for (int i = 0; i < Logger.sAppenders.length; ++i) {
            Logger.sAppenders[i].d(this.tagName, s);
        }
    }
    
    public void d(final String s, final Throwable t) {
        for (int i = 0; i < Logger.sAppenders.length; ++i) {
            Logger.sAppenders[i].d(this.tagName, s, t);
        }
    }
    
    public void d(final Throwable t) {
        this.d("", t);
    }
    
    public void e(final String s) {
        for (int i = 0; i < Logger.sAppenders.length; ++i) {
            Logger.sAppenders[i].e(this.tagName, s);
        }
    }
    
    public void e(final String s, final Throwable t) {
        for (int i = 0; i < Logger.sAppenders.length; ++i) {
            Logger.sAppenders[i].e(this.tagName, s, t);
        }
    }
    
    public void e(final Throwable t) {
        this.e("", t);
    }
    
    public void i(final String s) {
        for (int i = 0; i < Logger.sAppenders.length; ++i) {
            Logger.sAppenders[i].i(this.tagName, s);
        }
    }
    
    public void i(final String s, final Throwable t) {
        for (int i = 0; i < Logger.sAppenders.length; ++i) {
            Logger.sAppenders[i].i(this.tagName, s, t);
        }
    }
    
    public void i(final Throwable t) {
        this.i("", t);
    }
    
    public boolean isLoggable(final int n) {
        if (this.timestamp < Logger.logLevelChange) {
            if (this.loggable == null) {
                this.loggable = new boolean[8];
            }
            for (int i = 2; i <= 7; ++i) {
                this.loggable[i] = Log.isLoggable(this.tagName, i);
            }
            this.timestamp = System.currentTimeMillis();
        }
        return this.loggable[n];
    }
    
    public void logTimeTaken(final String s) {
        this.d(s + " " + (System.currentTimeMillis() - this.startTime) + " ms");
    }
    
    public void recordStartTime() {
        this.startTime = System.currentTimeMillis();
    }
    
    public void v(final String s) {
        for (int i = 0; i < Logger.sAppenders.length; ++i) {
            Logger.sAppenders[i].v(this.tagName, s);
        }
    }
    
    public void v(final String s, final Throwable t) {
        for (int i = 0; i < Logger.sAppenders.length; ++i) {
            Logger.sAppenders[i].v(this.tagName, s, t);
        }
    }
    
    public void v(final Throwable t) {
        this.v("", t);
    }
    
    public void w(final String s) {
        for (int i = 0; i < Logger.sAppenders.length; ++i) {
            Logger.sAppenders[i].w(this.tagName, s);
        }
    }
    
    public void w(final String s, final Throwable t) {
        for (int i = 0; i < Logger.sAppenders.length; ++i) {
            Logger.sAppenders[i].w(this.tagName, s, t);
        }
    }
    
    public void w(final Throwable t) {
        this.w("", t);
    }
}
