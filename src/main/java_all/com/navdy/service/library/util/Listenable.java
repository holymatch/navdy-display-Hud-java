package com.navdy.service.library.util;

import java.util.Iterator;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import com.navdy.service.library.log.Logger;

public abstract class Listenable<T extends Listener>
{
    private final Object listenerLock;
    private final Logger logger;
    protected HashSet<WeakReference<T>> mListeners;
    
    public Listenable() {
        this.logger = new Logger(this.getClass());
        this.listenerLock = new Object();
        this.mListeners = new HashSet<WeakReference<T>>();
    }
    
    public boolean addListener(final T t) {
        boolean b = false;
        if (t == null) {
            this.logger.e("attempted to add null listener");
        }
        else {
            while (true) {
                final Object listenerLock = this.listenerLock;
                final T t2;
            Label_0100:
                while (true) {
                    Listener listener = null;
                    Label_0085: {
                        synchronized (listenerLock) {
                            final Iterator<WeakReference<T>> iterator = this.mListeners.iterator();
                            while (iterator.hasNext()) {
                                listener = iterator.next().get();
                                if (listener != null) {
                                    break Label_0085;
                                }
                                iterator.remove();
                            }
                            break Label_0100;
                        }
                    }
                    if (listener.equals(t2)) {
                        // monitorexit(o)
                        break;
                    }
                    continue;
                }
                this.mListeners.add(new WeakReference<T>(t2));
                // monitorexit(o)
                b = true;
                break;
            }
        }
        return b;
    }
    
    protected void dispatchToListeners(final EventDispatcher eventDispatcher) {
        while (true) {
            Object o = this.listenerLock;
            while (true) {
                Object o2 = null;
                Label_0074: {
                    synchronized (o) {
                        o2 = this.mListeners.clone();
                        // monitorexit(o)
                        o = ((HashSet<WeakReference<Listener>>)o2).iterator();
                        while (((Iterator)o).hasNext()) {
                            o2 = ((Iterator<WeakReference<Listener>>)o).next().get();
                            if (o2 != null) {
                                break Label_0074;
                            }
                            ((Iterator)o).remove();
                        }
                        break;
                    }
                }
                final EventDispatcher<Listenable, Listener> eventDispatcher2;
                eventDispatcher2.dispatchEvent(this, (Listener)o2);
                continue;
            }
        }
    }
    
    public boolean removeListener(final T t) {
        boolean b = false;
        if (t == null) {
            this.logger.e("attempted to remove null listener");
        }
        else {
            while (true) {
                final Object listenerLock = this.listenerLock;
            Label_0109:
                while (true) {
                    final Iterator<WeakReference<T>> iterator;
                    Listener listener = null;
                    Label_0085: {
                        synchronized (listenerLock) {
                            iterator = this.mListeners.iterator();
                            while (iterator.hasNext()) {
                                listener = iterator.next().get();
                                if (listener != null) {
                                    break Label_0085;
                                }
                                iterator.remove();
                            }
                            break Label_0109;
                        }
                    }
                    final Throwable t2;
                    if (listener.equals(t2)) {
                        iterator.remove();
                        b = true;
                        // monitorexit(o)
                        break;
                    }
                    continue;
                }
                // monitorexit(o)
                break;
            }
        }
        return b;
    }
    
    protected interface EventDispatcher<U extends Listenable, T extends Listener>
    {
        void dispatchEvent(final U p0, final T p1);
    }
    
    public interface Listener
    {
    }
}
