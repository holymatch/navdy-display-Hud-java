package com.navdy.service.library.device.discovery;

import java.util.Locale;
import android.bluetooth.BluetoothAdapter;
import com.navdy.service.library.log.Logger;
import java.util.regex.Pattern;

public class BTDeviceBroadcaster implements RemoteDeviceBroadcaster
{
    public static final int DISCOVERY_TIMEOUT = 300;
    public static final String DISPLAY_NAME = " Navdy Display";
    public static final Pattern DISPLAY_PATTERN;
    private static final Logger sLogger;
    private BluetoothAdapter adapter;
    private boolean started;
    
    static {
        sLogger = new Logger(BTDeviceBroadcaster.class);
        DISPLAY_PATTERN = Pattern.compile("navdy|hud");
    }
    
    public BTDeviceBroadcaster() {
        this.adapter = BluetoothAdapter.getDefaultAdapter();
    }
    
    private void ensureDisplayLike() {
        final String name = this.adapter.getName();
        if (!isDisplay(name)) {
            this.adapter.setName(name + " Navdy Display");
        }
    }
    
    public static boolean isDisplay(final String s) {
        return BTDeviceBroadcaster.DISPLAY_PATTERN.matcher(s.toLowerCase(Locale.US)).find();
    }
    
    private void setScanMode(final int n, final int n2) {
        try {
            Class.forName(this.adapter.getClass().getName()).getDeclaredMethod("setScanMode", Integer.TYPE, Integer.TYPE).invoke(this.adapter, n, n2);
        }
        catch (Exception ex) {
            BTDeviceBroadcaster.sLogger.d("enableDiscovery failed - " + ex.toString(), ex);
        }
    }
    
    @Override
    public boolean start() {
        if (!this.started) {
            this.started = true;
            this.setScanMode(23, 300);
        }
        return this.started;
    }
    
    @Override
    public boolean stop() {
        if (this.started) {
            this.started = false;
            this.setScanMode(20, 300);
        }
        return true;
    }
}
