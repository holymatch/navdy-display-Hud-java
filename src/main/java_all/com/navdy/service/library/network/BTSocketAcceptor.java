package com.navdy.service.library.network;

import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.connection.BTConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.ConnectionType;
import java.io.IOException;
import android.bluetooth.BluetoothAdapter;
import java.lang.reflect.Field;
import android.bluetooth.BluetoothSocket;
import android.os.Build;
import java.util.UUID;
import android.bluetooth.BluetoothServerSocket;
import com.navdy.service.library.log.Logger;

public class BTSocketAcceptor implements SocketAcceptor
{
    private static final boolean needsPatch;
    private static Logger sLogger;
    BluetoothServerSocket btServerSocket;
    private BTSocketAdapter internalSocket;
    private final String sdpName;
    private final boolean secure;
    private final UUID serviceUUID;
    
    static {
        BTSocketAcceptor.sLogger = new Logger(BTSocketAcceptor.class);
        needsPatch = (Build$VERSION.SDK_INT >= 17 && Build$VERSION.SDK_INT <= 20);
    }
    
    public BTSocketAcceptor(final String s, final UUID uuid) {
        this(s, uuid, true);
    }
    
    public BTSocketAcceptor(final String sdpName, final UUID serviceUUID, final boolean secure) {
        this.sdpName = sdpName;
        this.serviceUUID = serviceUUID;
        this.secure = secure;
    }
    
    private void storeSocket() {
        try {
            final Field declaredField = this.btServerSocket.getClass().getDeclaredField("mSocket");
            declaredField.setAccessible(true);
            this.internalSocket = new BTSocketAdapter((BluetoothSocket)declaredField.get(this.btServerSocket));
        }
        catch (Exception ex) {
            BTSocketAcceptor.sLogger.w("Failed to close server socket", ex);
        }
    }
    
    @Override
    public SocketAdapter accept() throws IOException {
        if (this.btServerSocket == null) {
            final BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
            if (defaultAdapter == null) {
                throw new IOException("Bluetooth unavailable");
            }
            if (this.secure) {
                this.btServerSocket = defaultAdapter.listenUsingRfcommWithServiceRecord(this.sdpName, this.serviceUUID);
            }
            else {
                this.btServerSocket = defaultAdapter.listenUsingInsecureRfcommWithServiceRecord(this.sdpName, this.serviceUUID);
            }
            if (BTSocketAcceptor.needsPatch) {
                this.storeSocket();
            }
        }
        return new BTSocketAdapter(this.btServerSocket.accept());
    }
    
    @Override
    public void close() throws IOException {
        if (this.btServerSocket != null) {
            this.btServerSocket.close();
            if (this.internalSocket != null) {
                this.internalSocket.close();
                this.internalSocket = null;
            }
            this.btServerSocket = null;
        }
    }
    
    @Override
    public ConnectionInfo getRemoteConnectionInfo(final SocketAdapter socketAdapter, final ConnectionType connectionType) {
        final NavdyDeviceId remoteDevice = socketAdapter.getRemoteDevice();
        BTConnectionInfo btConnectionInfo;
        if (remoteDevice != null) {
            btConnectionInfo = new BTConnectionInfo(remoteDevice, this.serviceUUID, connectionType);
        }
        else {
            btConnectionInfo = null;
        }
        return btConnectionInfo;
    }
}
