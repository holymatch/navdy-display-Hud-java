package com.navdy.service.library.file;

import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import java.util.Random;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import com.navdy.service.library.log.Logger;

public abstract class TransferDataSource
{
    public static final String TEST_DATA_NAME = "<TESTDATA>";
    private static Logger sLogger;
    protected long mCurOffset;
    
    static {
        TransferDataSource.sLogger = new Logger(TransferDataSource.class);
    }
    
    public static TransferDataSource fileSource(final File file) {
        return new FileTransferDataSource(file);
    }
    
    public static TransferDataSource testSource(final long n) {
        return new TestDataSource(n);
    }
    
    public abstract String checkSum();
    
    public abstract String checkSum(final long p0);
    
    public abstract File getFile();
    
    public abstract String getName();
    
    public abstract long length();
    
    public abstract int read(final byte[] p0) throws Exception;
    
    public void seek(final long mCurOffset) {
        this.mCurOffset = mCurOffset;
    }
    
    private static class FileTransferDataSource extends TransferDataSource
    {
        File mFile;
        
        FileTransferDataSource(final File mFile) {
            this.mFile = mFile;
        }
        
        @Override
        public String checkSum() {
            return IOUtils.hashForFile(this.mFile);
        }
        
        @Override
        public String checkSum(final long n) {
            return IOUtils.hashForFile(this.mFile, n);
        }
        
        @Override
        public File getFile() {
            return this.mFile;
        }
        
        @Override
        public String getName() {
            return this.mFile.getName();
        }
        
        @Override
        public long length() {
            return this.mFile.length();
        }
        
        @Override
        public int read(final byte[] p0) throws Exception {
            // 
            This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: astore_2       
            //     2: new             Ljava/io/RandomAccessFile;
            //     5: astore_3       
            //     6: aload_3        
            //     7: aload_0        
            //     8: getfield        com/navdy/service/library/file/TransferDataSource$FileTransferDataSource.mFile:Ljava/io/File;
            //    11: ldc             "r"
            //    13: invokespecial   java/io/RandomAccessFile.<init>:(Ljava/io/File;Ljava/lang/String;)V
            //    16: aload_3        
            //    17: aload_0        
            //    18: getfield        com/navdy/service/library/file/TransferDataSource$FileTransferDataSource.mCurOffset:J
            //    21: invokevirtual   java/io/RandomAccessFile.seek:(J)V
            //    24: aload_3        
            //    25: aload_1        
            //    26: invokevirtual   java/io/RandomAccessFile.read:([B)I
            //    29: istore          4
            //    31: aload_3        
            //    32: invokestatic    com/navdy/service/library/util/IOUtils.closeObject:(Ljava/io/Closeable;)V
            //    35: iload           4
            //    37: ireturn        
            //    38: astore_1       
            //    39: aload_2        
            //    40: astore_1       
            //    41: aload_1        
            //    42: invokestatic    com/navdy/service/library/util/IOUtils.closeObject:(Ljava/io/Closeable;)V
            //    45: iconst_m1      
            //    46: istore          4
            //    48: goto            35
            //    51: astore_1       
            //    52: aload_3        
            //    53: astore_1       
            //    54: goto            41
            //    Exceptions:
            //  throws java.lang.Exception
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type
            //  -----  -----  -----  -----  ----
            //  2      16     38     41     Any
            //  16     31     51     57     Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0035:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
            //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
            //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
            //     at java.lang.Thread.run(Unknown Source)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
    }
    
    private static class TestDataSource extends TransferDataSource
    {
        static final int DATA_BUFFER_LEN = 4096;
        byte[] mDataBuffer;
        long mLength;
        
        TestDataSource(final long mLength) {
            this.mLength = mLength;
            this.mDataBuffer = new byte[4096];
            new Random().nextBytes(this.mDataBuffer);
        }
        
        @Override
        public String checkSum() {
            return this.checkSum(this.mLength);
        }
        
        @Override
        public String checkSum(long n) {
            String bytesToHexString = null;
            final long n2 = 0L;
            long mLength = n;
            if (n > this.mLength) {
                mLength = this.mLength;
            }
            try {
                final MessageDigest instance = MessageDigest.getInstance("MD5");
                long n3;
                int n4;
                for (n = n2; n < mLength; n += n4) {
                    n3 = mLength - n;
                    if (n3 < 4096L) {
                        n4 = (int)n3;
                    }
                    else {
                        n4 = 4096;
                    }
                    instance.update(this.mDataBuffer, 0, n4);
                }
                bytesToHexString = IOUtils.bytesToHexString(instance.digest());
                return bytesToHexString;
            }
            catch (NoSuchAlgorithmException ex) {
                TransferDataSource.sLogger.e("unable to get MD5 algorithm");
                return bytesToHexString;
            }
        }
        
        @Override
        public File getFile() {
            return null;
        }
        
        @Override
        public String getName() {
            return "<TESTDATA>";
        }
        
        @Override
        public long length() {
            return this.mLength;
        }
        
        @Override
        public int read(final byte[] array) {
            int n = (int)(this.mLength % 4096L);
            final long n2 = this.mLength - this.mCurOffset;
            int length;
            if (n2 >= array.length) {
                length = array.length;
            }
            else {
                length = (int)n2;
            }
            for (int i = 0; i < length; ++i) {
                array[i] = this.mDataBuffer[n];
                n = (n + 1) % 4096;
            }
            return length;
        }
    }
}
