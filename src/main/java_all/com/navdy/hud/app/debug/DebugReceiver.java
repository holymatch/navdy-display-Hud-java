package com.navdy.hud.app.debug;

import android.util.Printer;
import android.content.Context;
import android.os.Handler;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.service.library.events.audio.SpeechRequest;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.notification.NotificationCategory;
import com.navdy.service.library.events.notification.NotificationEvent;
import java.io.OutputStream;
import java.util.Random;
import android.text.format.Formatter;
import java.io.InputStream;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.HttpURLConnection;
import java.net.URL;
import com.navdy.hud.app.obd.ObdManager;
import com.navdy.hud.app.device.light.HUDLightUtils;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.device.light.LightManager;
import java.util.TimerTask;
import java.util.Timer;
import com.navdy.service.library.events.input.MediaRemoteKeyEvent;
import com.navdy.service.library.events.input.KeyEvent;
import com.navdy.service.library.events.input.MediaRemoteKey;
import com.navdy.service.library.events.dial.DialBondRequest;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.service.library.events.places.SuggestedDestination;
import com.navdy.service.library.events.location.LatLong;
import com.navdy.service.library.events.destination.Destination;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.input.LaunchAppEvent;
import android.content.Intent;
import android.os.Looper;
import java.net.InetAddress;
import com.navdy.service.library.task.TaskManager;
import android.os.SystemClock;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import android.content.BroadcastReceiver;

public class DebugReceiver extends BroadcastReceiver
{
    public static final String ACTION_ANR = "com.navdy.app.debug.ANR";
    public static final String ACTION_BROADCAST_ANR = "com.navdy.app.debug.BROADCAST_ANR";
    public static final String ACTION_CHANGE_HEADING = "com.navdy.app.debug.CHANGE_HEADING";
    public static final String ACTION_CHECK_FOR_MAP_UPDATES = "com.navdy.app.debug.CHECK_FOR_MAP_DATA_UPDATE";
    public static final String ACTION_CLEAR_MANEUVER = "com.navdy.app.debug.CLEAR_MANEUVER";
    public static final String ACTION_CRASH = "com.navdy.app.debug.CRASH";
    public static final String ACTION_DNS_LOOKUP = "com.navdy.app.debug.DNS_LOOKUP";
    public static final String ACTION_DNS_LOOKUP_TEST = "com.navdy.app.debug.DNS_LOOKUP_TEST";
    public static final String ACTION_DUMP_THREAD_STACK = "com.navdy.hud.debug.DUMP_THREAD_STACK";
    public static final String ACTION_EJECT_JUNCTION_VIEW = "com.navdy.app.debug.EJECT_JUNC_VIEW";
    public static final String ACTION_GET_GOOGLE = "com.navdy.app.debug.GET_GOOGLE";
    public static final String ACTION_HIDE_LANE_INFO = "com.navdy.app.debug.HIDE_LANE_INFO";
    public static final String ACTION_HIDE_RECALC = "com.navdy.app.debug.HIDE_RECALC";
    public static final String ACTION_INJECT_JUNCTION_VIEW = "com.navdy.app.debug.INJECT_JUNC_VIEW";
    public static final String ACTION_LAUNCH_MUSIC_DETAILS = "com.navdy.app.debug.MUSIC_DETAILS";
    public static final String ACTION_LAUNCH_PICKER = "com.navdy.app.debug.LAUNCH_PICKER";
    public static final String ACTION_NATIVE_CRASH = "com.navdy.app.debug.NATIVE_CRASH";
    public static final String ACTION_NATIVE_CRASH_SEPARATE_THREAD = "com.navdy.app.debug.NATIVE_CRASH_SEPARATE_THREAD";
    public static final String ACTION_NETSTAT = "com.navdy.app.debug.NETSTAT";
    public static final String ACTION_PLAYBACK_ROUTE = "com.navdy.app.debug.PLAYBACK_ROUTE";
    public static final String ACTION_PLAYBACK_ROUTE_SECONDARY = "com.navdy.app.debug.PLAYBACK_ROUTE_SECONDARY";
    public static final String ACTION_PRINT_BUS_REGISTRATION = "com.navdy.app.debug.PRINT_BUS_REGISTRATION";
    public static final String ACTION_PRINT_MAP_NAV_MODE = "com.navdy.app.debug.PRINT_MAP_NAV_MODE";
    public static final String ACTION_PRINT_MAP_ZOOM = "com.navdy.app.debug.PRINT_MAP_ZOOM";
    public static final String ACTION_SCALE_DISPLAY = "com.navdy.app.debug.SCALE_DISPLAY";
    public static final String ACTION_SEND_NOW_MANEUVER = "com.navdy.app.debug.NOW_MANEUVER";
    public static final String ACTION_SEND_SOON_MANEUVER = "com.navdy.app.debug.SOON_MANEUVER";
    public static final String ACTION_SEND_STAY_MANEUVER = "com.navdy.app.debug.STAY_MANEUVER";
    public static final String ACTION_SET_DISTANCE_MANEUVER = "com.navdy.app.debug.DISTANCE_MANEUVER";
    public static final String ACTION_SET_ICON_MANEUVER = "com.navdy.app.debug.ICON_MANEUVER";
    public static final String ACTION_SET_INSTRUCTION_MANEUVER = "com.navdy.app.debug.INSTRUCTION_MANEUVER";
    public static final String ACTION_SET_SIMULATION_SPEED = "com.navdy.app.debug.SET_SIM_SPEED";
    public static final String ACTION_SET_START_ROUTE_CALC_POINT = "com.navdy.app.debug.START_ROUTE_POINT";
    public static final String ACTION_SET_THEN_MANEUVER = "com.navdy.app.debug.THEN_MANEUVER";
    public static final String ACTION_SHOW_LANE_INFO = "com.navdy.app.debug.SHOW_LANE_INFO";
    public static final String ACTION_SHOW_RECALC = "com.navdy.app.debug.SHOW_RECALC";
    public static final String ACTION_START_BUS_PROFILING = "com.navdy.app.debug.START_BUS_PROFILING";
    public static final String ACTION_START_CPU_HOG = "com.navdy.app.debug.START_CPU_HOG";
    public static final String ACTION_START_MAIN_THREAD_PROFILING = "com.navdy.app.debug.START_MAIN_THREAD_PROFILING";
    public static final String ACTION_START_MAP_RENDERING = "com.navdy.app.debug.START_MAP_RENDERING";
    public static final String ACTION_STOP_BUS_PROFILING = "com.navdy.app.debug.STOP_BUS_PROFILING";
    public static final String ACTION_STOP_CPU_HOG = "com.navdy.app.debug.STOP_CPU_HOG";
    public static final String ACTION_STOP_MAIN_THREAD_PROFILING = "com.navdy.app.debug.STOP_MAIN_THREAD_PROFILING";
    public static final String ACTION_STOP_MAP_RENDERING = "com.navdy.app.debug.STOP_MAP_RENDERING";
    public static final String ACTION_STOP_PLAYBACK_ROUTE = "com.navdy.app.debug.STOP_PLAYBACK_ROUTE";
    public static final String ACTION_TEST = "com.navdy.hud.debug.TEST";
    public static final String ACTION_TEST_TBT_TTS = "com.navdy.app.debug.TEST_TBT_TTS";
    public static final String ACTION_TEST_TBT_TTS_CANCEL = "com.navdy.app.debug.TEST_TBT_TTS_CANCEL";
    public static final String ACTION_UPDATE_MAP_DATA = "com.navdy.app.debug.UPDATE_MAP_DATA";
    public static final String ACTION_WAKEUP = "com.navdy.app.debug.WAKEUP";
    public static final int CONNECTION_TIMEOUT_MILLIS = 30000;
    public static final int DESTINATION_SUGGESTION_TYPE_FAVORITE = 2;
    public static final int DESTINATION_SUGGESTION_TYPE_HOME = 0;
    public static final int DESTINATION_SUGGESTION_TYPE_NEW_PLACE = 3;
    public static final int DESTINATION_SUGGESTION_TYPE_WORK = 1;
    public static final String DISABLE_HERE_LOC_DBG = "com.navdy.app.debug.DISABLE_HERE_LOC_DBG";
    public static final String DNS_LOOKUP_HOST_NAME = "DNS_LOOKUP_HOST_NAME";
    private static final boolean ENABLED = true;
    public static final String ENABLE_HERE_LOC_DBG = "com.navdy.app.debug.ENABLE_HERE_LOC_DBG";
    public static final String EXTRA_BANDWIDTH_LEVEL = "BANDWIDTH_LEVEL";
    public static final String EXTRA_COMMAND = "COMMAND";
    public static final String EXTRA_DEEP = "DEEP";
    public static final String EXTRA_DESTINATION_SUGGESTION_TYPE = "DESTINATION_SUGGESTION_TYPE";
    public static final String EXTRA_KEY = "KEY";
    public static final String EXTRA_LIGHT = "LIGHT";
    public static final String EXTRA_MANEUVER_DISTANCE = "EXTRA_MANEUVER_DISTANCE";
    public static final String EXTRA_MANEUVER_ICON = "EXTRA_MANEUVER_ICON";
    public static final String EXTRA_MANEUVER_INSTRUCTION = "EXTRA_MANEUVER_INSTRUCTION";
    public static final String EXTRA_MANEUVER_THEN_ICON = "EXTRA_MANEUVER_THEN_ICON";
    public static final String EXTRA_MODE = "MODE";
    public static final String EXTRA_NOTIFICATION_EVENT_APP_ID = "appId";
    public static final String EXTRA_NOTIFICATION_EVENT_APP_NAME = "appName";
    public static final String EXTRA_NOTIFICATION_EVENT_ID = "id";
    public static final String EXTRA_NOTIFICATION_EVENT_MESSAGE = "message";
    public static final String EXTRA_NOTIFICATION_EVENT_SUB_TITLE = "subtitle";
    public static final String EXTRA_NOTIFICATION_EVENT_TITLE = "title";
    public static final String EXTRA_OBD_CONFIG = "CONFIG";
    public static final String EXTRA_SIZE = "SIZE";
    public static final String EXTRA_TEST = "TEST";
    public static final String EXTRA_URL = "URL";
    public static final String HEADING = "HEADING";
    private static final int MAIN_THREAD_DEFAULT_PROFILING_THRESHOLD = 0;
    public static final String MAIN_THREAD_EXTRA_PROFILING_THRESHOLD = "PROFILING_THRESHOLD";
    public static final int READ_TIMEOUT_MILLIS = 30000;
    public static final String SPEED = "SPEED";
    public static final String TEST_DESTINATION_SUGGESTION = "TEST_DESTINATION_SUGGESTION";
    public static final String TEST_DIAL = "TEST_DIAL";
    public static final String TEST_DISABLE_OBD_PIDS_SCANNING = "TEST_DISABLE_OBD";
    public static final String TEST_ENABLE_OBD_PIDS_SCANNING = "TEST_ENABLE_OBD";
    public static final String TEST_HID = "HID";
    public static final String TEST_LAUNCH_APP = "LAUNCH_APP";
    public static final String TEST_LIGHT = "TEST_LIGHT";
    public static final String TEST_OBD_CONFIG = "TEST_OBD";
    public static final String TEST_OBD_FLASH_NEW_FIRMWARE = "TEST_OBD_FLASH_NEW_FIRMWARE";
    public static final String TEST_OBD_FLASH_OLD_FIRMWARE = "TEST_OBD_FLASH_OLD_FIRMWARE";
    public static final String TEST_OBD_RESET = "TEST_OBD_RESET";
    public static final String TEST_OBD_SET_MODE_J1939 = "TEST_OBD_SET_MODE_J1939";
    public static final String TEST_OBD_SET_MODE_OBD2 = "TEST_OBD_SET_MODE_OBD2";
    public static final String TEST_OBD_SLEEP = "TEST_OBD_SLEEP";
    public static final String TEST_PROXY_FILE_DOWNLOAD = "TEST_PROXY_FILE_DOWNLOAD";
    public static final String TEST_PROXY_FILE_UPLOAD = "TEST_PROXY_FILE_UPLOAD";
    public static final String TEST_SEND_NOTIFICATION_EVENT = "TEST_SEND_NOTIFICATION_EVENT";
    public static final String TEST_SHUT_DOWN = "SHUTDOWN";
    public static final String TEST_SWITCH_BANDWIDTH_LEVEL = "TEST_SWITCH_BANDWIDTH";
    public static final String TEST_TTS = "TEST_TTS";
    private static final String[] TTS_TEXTS;
    static final Logger sLogger;
    @Inject
    Bus mBus;
    private PrinterImpl mainThreadProfilingPrinter;
    
    static {
        TTS_TEXTS = new String[] { "Turn left after 100 feet", "Turn right after 500 feet. 1, 2, 3, 4, 5, 6, 7, 8,9,10", "There is a slight traffic ahead, be on the right lane.", "Hi how are you doing today? Testing a long long long long text", "Testing testing still testing." };
        sLogger = new Logger(DebugReceiver.class);
    }
    
    public DebugReceiver() {
        this.mainThreadProfilingPrinter = new PrinterImpl();
    }
    
    private void busywait(final int n) {
        for (long elapsedRealtime = SystemClock.elapsedRealtime(), n2 = SystemClock.elapsedRealtime(); n2 - elapsedRealtime < n; n2 = SystemClock.elapsedRealtime()) {}
    }
    
    private void dnsLookup(final String s) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    String val$host;
                    if ((val$host = s) == null) {
                        val$host = "analytics.localytics.com";
                    }
                    DebugReceiver.sLogger.v("dnslookup :" + val$host);
                    final InetAddress[] allByName = InetAddress.getAllByName(val$host);
                    if (allByName != null) {
                        DebugReceiver.sLogger.v("dnslookup total:" + allByName.length);
                        for (int i = 0; i < allByName.length; ++i) {
                            DebugReceiver.sLogger.v("dnslookup address :" + allByName[i].getHostAddress());
                        }
                    }
                    else {
                        DebugReceiver.sLogger.v("dnslookup no address");
                    }
                }
                catch (Throwable t) {
                    DebugReceiver.sLogger.e(t);
                }
            }
        }, 1);
    }
    
    private void dnsLookupTest() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    final String[] array = { "version.hybrid.api.here.com", "reverse.geocoder.api.here.com", "tpeg.traffic.api.here.com", "tpeg.hybrid.api.here.com", "download.hybrid.api.here.com", "v102-62-30-8.route.hybrid.api.here.com", "analytics.localytics.com", "profile.localytics.com", "sdk.hockeyapp.net", "navdyhud.atlassian.net" };
                    DebugReceiver.sLogger.v("dns_lookup_test");
                    int i = 0;
                    while (i < array.length) {
                        try {
                            final InetAddress[] allByName = InetAddress.getAllByName(array[i]);
                            if (allByName != null) {
                                DebugReceiver.sLogger.v("dns_lookup_test[" + array[i] + "] addresses = " + allByName.length);
                                for (int j = 0; j < allByName.length; ++j) {
                                    DebugReceiver.sLogger.v("dns_lookup_test[" + array[i] + "]  addr[" + allByName[j].getHostAddress() + "]");
                                }
                            }
                            else {
                                DebugReceiver.sLogger.v("dns_lookup_test[" + array[i] + "] no address");
                            }
                            ++i;
                        }
                        catch (Throwable t) {
                            DebugReceiver.sLogger.v("dns_lookup_test", t);
                        }
                    }
                }
                catch (Throwable t2) {
                    DebugReceiver.sLogger.e(t2);
                }
            }
        }, 1);
    }
    
    private void getGoogleHomePage() {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                // 
                This method could not be decompiled.
                // 
                // Original Bytecode:
                // 
                //     1: astore_1       
                //     2: aconst_null    
                //     3: astore_2       
                //     4: aconst_null    
                //     5: astore_3       
                //     6: aconst_null    
                //     7: astore          4
                //     9: aload_2        
                //    10: astore          5
                //    12: aload           4
                //    14: astore          6
                //    16: aload_1        
                //    17: astore          7
                //    19: aload_3        
                //    20: astore          8
                //    22: getstatic       com/navdy/hud/app/debug/DebugReceiver.sLogger:Lcom/navdy/service/library/log/Logger;
                //    25: ldc             "getGoogleHomePage"
                //    27: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
                //    30: aload_2        
                //    31: astore          5
                //    33: aload           4
                //    35: astore          6
                //    37: aload_1        
                //    38: astore          7
                //    40: aload_3        
                //    41: astore          8
                //    43: new             Ljava/net/URL;
                //    46: astore          9
                //    48: aload_2        
                //    49: astore          5
                //    51: aload           4
                //    53: astore          6
                //    55: aload_1        
                //    56: astore          7
                //    58: aload_3        
                //    59: astore          8
                //    61: aload           9
                //    63: ldc             "https://www.google.com"
                //    65: invokespecial   java/net/URL.<init>:(Ljava/lang/String;)V
                //    68: aload_2        
                //    69: astore          5
                //    71: aload           4
                //    73: astore          6
                //    75: aload_1        
                //    76: astore          7
                //    78: aload_3        
                //    79: astore          8
                //    81: aload           9
                //    83: invokevirtual   java/net/URL.openConnection:()Ljava/net/URLConnection;
                //    86: checkcast       Ljava/net/HttpURLConnection;
                //    89: astore_2       
                //    90: aload_2        
                //    91: astore          5
                //    93: aload           4
                //    95: astore          6
                //    97: aload_2        
                //    98: astore          7
                //   100: aload_3        
                //   101: astore          8
                //   103: aload_2        
                //   104: ldc             "GET"
                //   106: invokevirtual   java/net/HttpURLConnection.setRequestMethod:(Ljava/lang/String;)V
                //   109: aload_2        
                //   110: astore          5
                //   112: aload           4
                //   114: astore          6
                //   116: aload_2        
                //   117: astore          7
                //   119: aload_3        
                //   120: astore          8
                //   122: aload_2        
                //   123: invokevirtual   java/net/HttpURLConnection.getInputStream:()Ljava/io/InputStream;
                //   126: astore          4
                //   128: aload_2        
                //   129: astore          5
                //   131: aload           4
                //   133: astore          6
                //   135: aload_2        
                //   136: astore          7
                //   138: aload           4
                //   140: astore          8
                //   142: aload           4
                //   144: ldc             "UTF-8"
                //   146: invokestatic    com/navdy/service/library/util/IOUtils.convertInputStreamToString:(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
                //   149: astore_3       
                //   150: aload_2        
                //   151: astore          5
                //   153: aload           4
                //   155: astore          6
                //   157: aload_2        
                //   158: astore          7
                //   160: aload           4
                //   162: astore          8
                //   164: aload_2        
                //   165: invokevirtual   java/net/HttpURLConnection.getHeaderFields:()Ljava/util/Map;
                //   168: astore_1       
                //   169: aload_2        
                //   170: astore          5
                //   172: aload           4
                //   174: astore          6
                //   176: aload_2        
                //   177: astore          7
                //   179: aload           4
                //   181: astore          8
                //   183: aload_1        
                //   184: invokeinterface java/util/Map.keySet:()Ljava/util/Set;
                //   189: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
                //   194: astore          10
                //   196: aload_2        
                //   197: astore          5
                //   199: aload           4
                //   201: astore          6
                //   203: aload_2        
                //   204: astore          7
                //   206: aload           4
                //   208: astore          8
                //   210: aload           10
                //   212: invokeinterface java/util/Iterator.hasNext:()Z
                //   217: ifeq            563
                //   220: aload_2        
                //   221: astore          5
                //   223: aload           4
                //   225: astore          6
                //   227: aload_2        
                //   228: astore          7
                //   230: aload           4
                //   232: astore          8
                //   234: aload           10
                //   236: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
                //   241: checkcast       Ljava/lang/String;
                //   244: astore          11
                //   246: aload_2        
                //   247: astore          5
                //   249: aload           4
                //   251: astore          6
                //   253: aload_2        
                //   254: astore          7
                //   256: aload           4
                //   258: astore          8
                //   260: aload_1        
                //   261: aload           11
                //   263: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
                //   268: checkcast       Ljava/util/List;
                //   271: iconst_0       
                //   272: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
                //   277: checkcast       Ljava/lang/String;
                //   280: astore          9
                //   282: aload_2        
                //   283: astore          5
                //   285: aload           4
                //   287: astore          6
                //   289: aload_2        
                //   290: astore          7
                //   292: aload           4
                //   294: astore          8
                //   296: ldc             "null"
                //   298: aload           11
                //   300: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
                //   303: ifne            311
                //   306: aload           11
                //   308: ifnonnull       439
                //   311: aload_2        
                //   312: astore          5
                //   314: aload           4
                //   316: astore          6
                //   318: aload_2        
                //   319: astore          7
                //   321: aload           4
                //   323: astore          8
                //   325: getstatic       com/navdy/hud/app/debug/DebugReceiver.sLogger:Lcom/navdy/service/library/log/Logger;
                //   328: astore          11
                //   330: aload_2        
                //   331: astore          5
                //   333: aload           4
                //   335: astore          6
                //   337: aload_2        
                //   338: astore          7
                //   340: aload           4
                //   342: astore          8
                //   344: new             Ljava/lang/StringBuilder;
                //   347: astore          12
                //   349: aload_2        
                //   350: astore          5
                //   352: aload           4
                //   354: astore          6
                //   356: aload_2        
                //   357: astore          7
                //   359: aload           4
                //   361: astore          8
                //   363: aload           12
                //   365: invokespecial   java/lang/StringBuilder.<init>:()V
                //   368: aload_2        
                //   369: astore          5
                //   371: aload           4
                //   373: astore          6
                //   375: aload_2        
                //   376: astore          7
                //   378: aload           4
                //   380: astore          8
                //   382: aload           11
                //   384: aload           12
                //   386: ldc             "getGoogleHomePage "
                //   388: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   391: aload           9
                //   393: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   396: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   399: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
                //   402: goto            196
                //   405: astore_2       
                //   406: aload           5
                //   408: astore          7
                //   410: aload           6
                //   412: astore          8
                //   414: getstatic       com/navdy/hud/app/debug/DebugReceiver.sLogger:Lcom/navdy/service/library/log/Logger;
                //   417: ldc             "getGoogleHomePage"
                //   419: aload_2        
                //   420: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
                //   423: aload           6
                //   425: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                //   428: aload           5
                //   430: ifnull          438
                //   433: aload           5
                //   435: invokevirtual   java/net/HttpURLConnection.disconnect:()V
                //   438: return         
                //   439: aload_2        
                //   440: astore          5
                //   442: aload           4
                //   444: astore          6
                //   446: aload_2        
                //   447: astore          7
                //   449: aload           4
                //   451: astore          8
                //   453: getstatic       com/navdy/hud/app/debug/DebugReceiver.sLogger:Lcom/navdy/service/library/log/Logger;
                //   456: astore          12
                //   458: aload_2        
                //   459: astore          5
                //   461: aload           4
                //   463: astore          6
                //   465: aload_2        
                //   466: astore          7
                //   468: aload           4
                //   470: astore          8
                //   472: new             Ljava/lang/StringBuilder;
                //   475: astore          13
                //   477: aload_2        
                //   478: astore          5
                //   480: aload           4
                //   482: astore          6
                //   484: aload_2        
                //   485: astore          7
                //   487: aload           4
                //   489: astore          8
                //   491: aload           13
                //   493: invokespecial   java/lang/StringBuilder.<init>:()V
                //   496: aload_2        
                //   497: astore          5
                //   499: aload           4
                //   501: astore          6
                //   503: aload_2        
                //   504: astore          7
                //   506: aload           4
                //   508: astore          8
                //   510: aload           12
                //   512: aload           13
                //   514: ldc             "getGoogleHomePage "
                //   516: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   519: aload           11
                //   521: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   524: ldc             ":  "
                //   526: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   529: aload           9
                //   531: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   534: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   537: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
                //   540: goto            196
                //   543: astore          5
                //   545: aload           8
                //   547: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                //   550: aload           7
                //   552: ifnull          560
                //   555: aload           7
                //   557: invokevirtual   java/net/HttpURLConnection.disconnect:()V
                //   560: aload           5
                //   562: athrow         
                //   563: aload_2        
                //   564: astore          5
                //   566: aload           4
                //   568: astore          6
                //   570: aload_2        
                //   571: astore          7
                //   573: aload           4
                //   575: astore          8
                //   577: getstatic       com/navdy/hud/app/debug/DebugReceiver.sLogger:Lcom/navdy/service/library/log/Logger;
                //   580: astore          9
                //   582: aload_2        
                //   583: astore          5
                //   585: aload           4
                //   587: astore          6
                //   589: aload_2        
                //   590: astore          7
                //   592: aload           4
                //   594: astore          8
                //   596: new             Ljava/lang/StringBuilder;
                //   599: astore_1       
                //   600: aload_2        
                //   601: astore          5
                //   603: aload           4
                //   605: astore          6
                //   607: aload_2        
                //   608: astore          7
                //   610: aload           4
                //   612: astore          8
                //   614: aload_1        
                //   615: invokespecial   java/lang/StringBuilder.<init>:()V
                //   618: aload_2        
                //   619: astore          5
                //   621: aload           4
                //   623: astore          6
                //   625: aload_2        
                //   626: astore          7
                //   628: aload           4
                //   630: astore          8
                //   632: aload           9
                //   634: aload_1        
                //   635: ldc             "getGoogleHomePage "
                //   637: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   640: aload_3        
                //   641: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   644: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   647: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
                //   650: aload_2        
                //   651: astore          5
                //   653: aload           4
                //   655: astore          6
                //   657: aload_2        
                //   658: astore          7
                //   660: aload           4
                //   662: astore          8
                //   664: getstatic       com/navdy/hud/app/debug/DebugReceiver.sLogger:Lcom/navdy/service/library/log/Logger;
                //   667: astore          9
                //   669: aload_2        
                //   670: astore          5
                //   672: aload           4
                //   674: astore          6
                //   676: aload_2        
                //   677: astore          7
                //   679: aload           4
                //   681: astore          8
                //   683: new             Ljava/lang/StringBuilder;
                //   686: astore_1       
                //   687: aload_2        
                //   688: astore          5
                //   690: aload           4
                //   692: astore          6
                //   694: aload_2        
                //   695: astore          7
                //   697: aload           4
                //   699: astore          8
                //   701: aload_1        
                //   702: invokespecial   java/lang/StringBuilder.<init>:()V
                //   705: aload_2        
                //   706: astore          5
                //   708: aload           4
                //   710: astore          6
                //   712: aload_2        
                //   713: astore          7
                //   715: aload           4
                //   717: astore          8
                //   719: aload           9
                //   721: aload_1        
                //   722: ldc             "getGoogleHomePage LEN = "
                //   724: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                //   727: aload_3        
                //   728: invokevirtual   java/lang/String.length:()I
                //   731: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
                //   734: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                //   737: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
                //   740: aload           4
                //   742: invokestatic    com/navdy/service/library/util/IOUtils.closeStream:(Ljava/io/Closeable;)V
                //   745: aload_2        
                //   746: ifnull          438
                //   749: aload_2        
                //   750: invokevirtual   java/net/HttpURLConnection.disconnect:()V
                //   753: goto            438
                //   756: astore          7
                //   758: getstatic       com/navdy/hud/app/debug/DebugReceiver.sLogger:Lcom/navdy/service/library/log/Logger;
                //   761: aload           7
                //   763: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
                //   766: goto            438
                //   769: astore          7
                //   771: getstatic       com/navdy/hud/app/debug/DebugReceiver.sLogger:Lcom/navdy/service/library/log/Logger;
                //   774: aload           7
                //   776: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
                //   779: goto            438
                //   782: astore          7
                //   784: getstatic       com/navdy/hud/app/debug/DebugReceiver.sLogger:Lcom/navdy/service/library/log/Logger;
                //   787: aload           7
                //   789: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
                //   792: goto            560
                //    Exceptions:
                //  Try           Handler
                //  Start  End    Start  End    Type                 
                //  -----  -----  -----  -----  ---------------------
                //  22     30     405    438    Ljava/lang/Throwable;
                //  22     30     543    563    Any
                //  43     48     405    438    Ljava/lang/Throwable;
                //  43     48     543    563    Any
                //  61     68     405    438    Ljava/lang/Throwable;
                //  61     68     543    563    Any
                //  81     90     405    438    Ljava/lang/Throwable;
                //  81     90     543    563    Any
                //  103    109    405    438    Ljava/lang/Throwable;
                //  103    109    543    563    Any
                //  122    128    405    438    Ljava/lang/Throwable;
                //  122    128    543    563    Any
                //  142    150    405    438    Ljava/lang/Throwable;
                //  142    150    543    563    Any
                //  164    169    405    438    Ljava/lang/Throwable;
                //  164    169    543    563    Any
                //  183    196    405    438    Ljava/lang/Throwable;
                //  183    196    543    563    Any
                //  210    220    405    438    Ljava/lang/Throwable;
                //  210    220    543    563    Any
                //  234    246    405    438    Ljava/lang/Throwable;
                //  234    246    543    563    Any
                //  260    282    405    438    Ljava/lang/Throwable;
                //  260    282    543    563    Any
                //  296    306    405    438    Ljava/lang/Throwable;
                //  296    306    543    563    Any
                //  325    330    405    438    Ljava/lang/Throwable;
                //  325    330    543    563    Any
                //  344    349    405    438    Ljava/lang/Throwable;
                //  344    349    543    563    Any
                //  363    368    405    438    Ljava/lang/Throwable;
                //  363    368    543    563    Any
                //  382    402    405    438    Ljava/lang/Throwable;
                //  382    402    543    563    Any
                //  414    423    543    563    Any
                //  433    438    769    782    Ljava/lang/Throwable;
                //  453    458    405    438    Ljava/lang/Throwable;
                //  453    458    543    563    Any
                //  472    477    405    438    Ljava/lang/Throwable;
                //  472    477    543    563    Any
                //  491    496    405    438    Ljava/lang/Throwable;
                //  491    496    543    563    Any
                //  510    540    405    438    Ljava/lang/Throwable;
                //  510    540    543    563    Any
                //  555    560    782    795    Ljava/lang/Throwable;
                //  577    582    405    438    Ljava/lang/Throwable;
                //  577    582    543    563    Any
                //  596    600    405    438    Ljava/lang/Throwable;
                //  596    600    543    563    Any
                //  614    618    405    438    Ljava/lang/Throwable;
                //  614    618    543    563    Any
                //  632    650    405    438    Ljava/lang/Throwable;
                //  632    650    543    563    Any
                //  664    669    405    438    Ljava/lang/Throwable;
                //  664    669    543    563    Any
                //  683    687    405    438    Ljava/lang/Throwable;
                //  683    687    543    563    Any
                //  701    705    405    438    Ljava/lang/Throwable;
                //  701    705    543    563    Any
                //  719    740    405    438    Ljava/lang/Throwable;
                //  719    740    543    563    Any
                //  749    753    756    769    Ljava/lang/Throwable;
                // 
                // The error that occurred was:
                // 
                // java.lang.IndexOutOfBoundsException: Index: 400, Size: 400
                //     at java.util.ArrayList.rangeCheck(Unknown Source)
                //     at java.util.ArrayList.get(Unknown Source)
                //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
                //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1163)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1010)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
                //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
                //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
                //     at java.lang.Thread.run(Unknown Source)
                // 
                throw new IllegalStateException("An error occurred while decompiling this method.");
            }
        }, 1);
    }
    
    public static void setHereDebugLocation(final boolean b) {
        try {
            Looper.class.getDeclaredField("enableHereLocationDebugging").setBoolean(null, b);
            DebugReceiver.sLogger.v("setHereDebugLocation set to " + b);
        }
        catch (Throwable t) {
            DebugReceiver.sLogger.e("setHereDebugLocation", t);
        }
    }
    
    private void testAppLaunch(final Intent intent) {
        this.mBus.post(new RemoteEvent(new LaunchAppEvent((String)null)));
    }
    
    private void testDestinationSuggestion(final Intent intent) {
        final int intExtra = intent.getIntExtra("DESTINATION_SUGGESTION_TYPE", 0);
        Object o = null;
        switch (intExtra) {
            case 0:
                o = new SuggestedDestination(new Destination.Builder().navigation_position(new LatLong(Double.valueOf(37.7786444), Double.valueOf(-122.4462313))).display_position(new LatLong(Double.valueOf(37.7786444), Double.valueOf(-122.4462313))).full_address("201 8th St, San Francisco, CA 94103").destination_title("Home").destination_subtitle("").favorite_type(Destination.FavoriteType.FAVORITE_HOME).identifier("1").suggestion_type(Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(true).last_navigated_to(0L).build(), 600, SuggestedDestination.SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
            case 1:
                o = new SuggestedDestination(new Destination.Builder().navigation_position(new LatLong(Double.valueOf(37.7735078), Double.valueOf(-122.4055828))).display_position(new LatLong(Double.valueOf(37.7735078), Double.valueOf(-122.4055828))).full_address("575 7th St, San Francisco, CA 94103").destination_title("Work").destination_subtitle("").favorite_type(Destination.FavoriteType.FAVORITE_WORK).identifier("2").suggestion_type(Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(true).last_navigated_to(0L).build(), 3600, SuggestedDestination.SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
            case 2:
                o = new SuggestedDestination(new Destination.Builder().navigation_position(new LatLong(Double.valueOf(37.7879373), Double.valueOf(-122.4096868))).display_position(new LatLong(Double.valueOf(37.7879373), Double.valueOf(-122.4096868))).full_address("Union Square, San Francisco, CA 94108").destination_title("Union Square").destination_subtitle("").favorite_type(Destination.FavoriteType.FAVORITE_CUSTOM).identifier("3").suggestion_type(Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(true).last_navigated_to(0L).build(), 400, SuggestedDestination.SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
            case 3:
                o = new SuggestedDestination(new Destination.Builder().navigation_position(new LatLong(Double.valueOf(37.7766916), Double.valueOf(-122.3970457))).display_position(new LatLong(Double.valueOf(37.7766916), Double.valueOf(-122.3970457))).full_address("700 4th St, San Francisco, CA 94107").destination_title("").destination_subtitle("").favorite_type(Destination.FavoriteType.FAVORITE_NONE).identifier("4").suggestion_type(Destination.DEFAULT_SUGGESTION_TYPE).is_recommendation(true).last_navigated_to(0L).build(), -1, SuggestedDestination.SuggestionType.SUGGESTION_RECOMMENDATION);
                break;
        }
        this.mBus.post(o);
    }
    
    private void testDial(final Intent intent) {
        final String stringExtra = intent.getStringExtra("COMMAND");
        if (stringExtra.equals("PowerButtonDoubleClick")) {
            RemoteDeviceManager.getInstance().getInputManager().injectKey(InputManager.CustomKeyEvent.POWER_BUTTON_DOUBLE_CLICK);
        }
        else if (stringExtra.equals("rebond")) {
            this.mBus.post(new DialBondRequest(DialBondRequest.DialAction.DIAL_CLEAR_BOND));
            this.mBus.post(new DialBondRequest(DialBondRequest.DialAction.DIAL_BOND));
        }
    }
    
    private void testHID(final Intent intent) {
        try {
            final int intExtra = intent.getIntExtra("KEY", 0);
            this.mBus.post(new RemoteEvent(new MediaRemoteKeyEvent(MediaRemoteKey.values()[intExtra], KeyEvent.KEY_DOWN)));
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    DebugReceiver.this.mBus.post(new RemoteEvent(new MediaRemoteKeyEvent(MediaRemoteKey.values()[intExtra], KeyEvent.KEY_UP)));
                }
            }, 100L);
        }
        catch (Exception ex) {
            DebugReceiver.sLogger.e("Exception getting the extra");
        }
    }
    
    private void testLight(final Intent intent) {
        final int intExtra = intent.getIntExtra("LIGHT", 0);
        final LightManager instance = LightManager.getInstance();
        switch (intExtra) {
            case 0:
                HUDLightUtils.showPairing(HudApplication.getAppContext(), instance, true);
                break;
            case 1:
                HUDLightUtils.showShutDown(instance);
                break;
            case 2:
                HUDLightUtils.showGestureDetectionEnabled(HudApplication.getAppContext(), instance, "Debug");
                break;
            case 3:
                HUDLightUtils.showGestureNotRecognized(HudApplication.getAppContext(), instance);
                break;
            case 4:
                HUDLightUtils.showGestureDetected(HudApplication.getAppContext(), instance);
                break;
            case 5:
                HUDLightUtils.showUSBPowerOn(HudApplication.getAppContext(), instance);
                break;
            case 6:
                HUDLightUtils.showUSBPowerShutDown(HudApplication.getAppContext(), instance);
                break;
            case 7:
                HUDLightUtils.showUSBTransfer(HudApplication.getAppContext(), instance);
                break;
            case 8:
                HUDLightUtils.showError(HudApplication.getAppContext(), instance);
                break;
        }
    }
    
    private void testObdConfig(final Intent intent) {
        switch (intent.getIntExtra("CONFIG", 1)) {
            case 0:
                ObdManager.getInstance().getObdDeviceConfigurationManager().setConfigurationFile("debug");
                break;
            case 1:
                ObdManager.getInstance().getObdDeviceConfigurationManager().setConfigurationFile("default_on");
                break;
        }
    }
    
    private void testProxyFileDownload(final Intent intent) {
        final int intExtra = intent.getIntExtra("SIZE", 100);
        final String string = "http://test.mjpmz4phvm.us-west-2.elasticbeanstalk.com/file?size=" + intExtra;
        String stringExtra = intent.getStringExtra("URL");
        if (stringExtra == null) {
            stringExtra = string;
        }
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final Closeable closeable = null;
                final Closeable closeable2 = null;
                final Closeable closeable3 = null;
                final Closeable closeable4 = null;
                final Closeable closeable5 = null;
                final long n = 0L;
                long n2 = 0L;
                Closeable inputStream = closeable;
                long n3 = n;
                long n4 = n2;
                Closeable closeable6 = closeable2;
                long n5 = n;
                long n6 = n2;
                Closeable closeable7 = closeable3;
                long n7 = n;
                long n8 = n2;
                Closeable closeable8 = closeable4;
                long n9 = n;
                long n10 = n2;
                try {
                    final Logger sLogger = DebugReceiver.sLogger;
                    inputStream = closeable;
                    n3 = n;
                    n4 = n2;
                    closeable6 = closeable2;
                    n5 = n;
                    n6 = n2;
                    closeable7 = closeable3;
                    n7 = n;
                    n8 = n2;
                    closeable8 = closeable4;
                    n9 = n;
                    n10 = n2;
                    inputStream = closeable;
                    n3 = n;
                    n4 = n2;
                    closeable6 = closeable2;
                    n5 = n;
                    n6 = n2;
                    closeable7 = closeable3;
                    n7 = n;
                    n8 = n2;
                    closeable8 = closeable4;
                    n9 = n;
                    n10 = n2;
                    final StringBuilder sb = new StringBuilder();
                    inputStream = closeable;
                    n3 = n;
                    n4 = n2;
                    closeable6 = closeable2;
                    n5 = n;
                    n6 = n2;
                    closeable7 = closeable3;
                    n7 = n;
                    n8 = n2;
                    closeable8 = closeable4;
                    n9 = n;
                    n10 = n2;
                    sLogger.d(sb.append("Intended Size to be dowloaded is ").append(intExtra).toString());
                    inputStream = closeable;
                    n3 = n;
                    n4 = n2;
                    closeable6 = closeable2;
                    n5 = n;
                    n6 = n2;
                    closeable7 = closeable3;
                    n7 = n;
                    n8 = n2;
                    closeable8 = closeable4;
                    n9 = n;
                    n10 = n2;
                    inputStream = closeable;
                    n3 = n;
                    n4 = n2;
                    closeable6 = closeable2;
                    n5 = n;
                    n6 = n2;
                    closeable7 = closeable3;
                    n7 = n;
                    n8 = n2;
                    closeable8 = closeable4;
                    n9 = n;
                    n10 = n2;
                    final URL url = new URL(stringExtra);
                    inputStream = closeable;
                    n3 = n;
                    n4 = n2;
                    closeable6 = closeable2;
                    n5 = n;
                    n6 = n2;
                    closeable7 = closeable3;
                    n7 = n;
                    n8 = n2;
                    closeable8 = closeable4;
                    n9 = n;
                    n10 = n2;
                    final HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    inputStream = closeable;
                    n3 = n;
                    n4 = n2;
                    closeable6 = closeable2;
                    n5 = n;
                    n6 = n2;
                    closeable7 = closeable3;
                    n7 = n;
                    n8 = n2;
                    closeable8 = closeable4;
                    n9 = n;
                    n10 = n2;
                    httpURLConnection.setRequestMethod("GET");
                    inputStream = closeable;
                    n3 = n;
                    n4 = n2;
                    closeable6 = closeable2;
                    n5 = n;
                    n6 = n2;
                    closeable7 = closeable3;
                    n7 = n;
                    n8 = n2;
                    closeable8 = closeable4;
                    n9 = n;
                    n10 = n2;
                    httpURLConnection.setConnectTimeout(30000);
                    inputStream = closeable;
                    n3 = n;
                    n4 = n2;
                    closeable6 = closeable2;
                    n5 = n;
                    n6 = n2;
                    closeable7 = closeable3;
                    n7 = n;
                    n8 = n2;
                    closeable8 = closeable4;
                    n9 = n;
                    n10 = n2;
                    httpURLConnection.setReadTimeout(30000);
                    inputStream = closeable;
                    n3 = n;
                    n4 = n2;
                    closeable6 = closeable2;
                    n5 = n;
                    n6 = n2;
                    closeable7 = closeable3;
                    n7 = n;
                    n8 = n2;
                    closeable8 = closeable4;
                    n9 = n;
                    n10 = n2;
                    final int responseCode = httpURLConnection.getResponseCode();
                    if (responseCode == 200) {
                        inputStream = closeable;
                        n3 = n;
                        n4 = n2;
                        closeable6 = closeable2;
                        n5 = n;
                        n6 = n2;
                        closeable7 = closeable3;
                        n7 = n;
                        n8 = n2;
                        closeable8 = closeable4;
                        n9 = n;
                        n10 = n2;
                        DebugReceiver.sLogger.d("Starting to download");
                        inputStream = closeable;
                        n3 = n;
                        n4 = n2;
                        closeable6 = closeable2;
                        n5 = n;
                        n6 = n2;
                        closeable7 = closeable3;
                        n7 = n;
                        n8 = n2;
                        closeable8 = closeable4;
                        n9 = n;
                        n10 = n2;
                        final InputStream inputStream2 = (InputStream)(inputStream = httpURLConnection.getInputStream());
                        n3 = n;
                        n4 = n2;
                        closeable6 = inputStream2;
                        n5 = n;
                        n6 = n2;
                        closeable7 = inputStream2;
                        n7 = n;
                        n8 = n2;
                        closeable8 = inputStream2;
                        n9 = n;
                        n10 = n2;
                        final byte[] array = new byte[102400];
                        inputStream = inputStream2;
                        n3 = n;
                        n4 = n2;
                        closeable6 = inputStream2;
                        n5 = n;
                        n6 = n2;
                        closeable7 = inputStream2;
                        n7 = n;
                        n8 = n2;
                        closeable8 = inputStream2;
                        n9 = n;
                        n10 = n2;
                        final long elapsedRealtime = SystemClock.elapsedRealtime();
                        while (true) {
                            inputStream = inputStream2;
                            n3 = elapsedRealtime;
                            n4 = n2;
                            closeable6 = inputStream2;
                            n5 = elapsedRealtime;
                            n6 = n2;
                            closeable7 = inputStream2;
                            n7 = elapsedRealtime;
                            n8 = n2;
                            closeable8 = inputStream2;
                            n9 = elapsedRealtime;
                            n10 = n2;
                            final long n11 = inputStream2.read(array);
                            closeable8 = inputStream2;
                            n9 = elapsedRealtime;
                            n10 = n2;
                            if (n11 == -1L) {
                                break;
                            }
                            n2 += n11;
                        }
                    }
                    else {
                        inputStream = closeable;
                        n3 = n;
                        n4 = n2;
                        closeable6 = closeable2;
                        n5 = n;
                        n6 = n2;
                        closeable7 = closeable3;
                        n7 = n;
                        n8 = n2;
                        closeable8 = closeable4;
                        n9 = n;
                        n10 = n2;
                        final Logger sLogger2 = DebugReceiver.sLogger;
                        inputStream = closeable;
                        n3 = n;
                        n4 = n2;
                        closeable6 = closeable2;
                        n5 = n;
                        n6 = n2;
                        closeable7 = closeable3;
                        n7 = n;
                        n8 = n2;
                        closeable8 = closeable4;
                        n9 = n;
                        n10 = n2;
                        inputStream = closeable;
                        n3 = n;
                        n4 = n2;
                        closeable6 = closeable2;
                        n5 = n;
                        n6 = n2;
                        closeable7 = closeable3;
                        n7 = n;
                        n8 = n2;
                        closeable8 = closeable4;
                        n9 = n;
                        n10 = n2;
                        final StringBuilder sb2 = new StringBuilder();
                        inputStream = closeable;
                        n3 = n;
                        n4 = n2;
                        closeable6 = closeable2;
                        n5 = n;
                        n6 = n2;
                        closeable7 = closeable3;
                        n7 = n;
                        n8 = n2;
                        closeable8 = closeable4;
                        n9 = n;
                        n10 = n2;
                        sLogger2.e(sb2.append("Response code not OK ").append(responseCode).toString());
                        n10 = n2;
                        n9 = n;
                        closeable8 = closeable5;
                    }
                }
                catch (SocketTimeoutException ex) {
                    closeable8 = inputStream;
                    n9 = n3;
                    n10 = n4;
                    DebugReceiver.sLogger.e("Socket timed out Exception ", ex);
                }
                catch (IOException ex2) {
                    closeable8 = closeable6;
                    n9 = n5;
                    n10 = n6;
                    DebugReceiver.sLogger.e("IOException ", ex2);
                }
                catch (Exception ex3) {
                    closeable8 = closeable7;
                    n9 = n7;
                    n10 = n8;
                    DebugReceiver.sLogger.e("Exception ", ex3);
                }
                finally {
                    final double n12 = SystemClock.elapsedRealtime() - n9;
                    DebugReceiver.sLogger.e("Download finished, Size : " + n10 / 1000L + ", Time Taken :" + (long)n12 + " milliseconds , RawSpeed :" + (float)(n10 / 1000.0f / (n12 / 1000.0)) + " kBps");
                    IOUtils.closeStream(closeable8);
                }
            }
        }, 1);
    }
    
    private void testProxyFileUpload(final Intent intent) {
        TaskManager.getInstance().execute(new Runnable() {
            final /* synthetic */ int val$size = intent.getIntExtra("SIZE", 100);
            
            @Override
            public void run() {
                try {
                    final HttpURLConnection httpURLConnection = (HttpURLConnection)new URL("http://test.mjpmz4phvm.us-west-2.elasticbeanstalk.com/file").openConnection();
                    httpURLConnection.setConnectTimeout(30000);
                    httpURLConnection.setReadTimeout(30000);
                    httpURLConnection.setRequestMethod("POST");
                    final OutputStream outputStream = httpURLConnection.getOutputStream();
                    int n;
                    if (this.val$size < 0 || this.val$size > 2000) {
                        n = 100000;
                    }
                    else {
                        n = this.val$size * 1000;
                    }
                    DebugReceiver.sLogger.d("Uploading " + Formatter.formatFileSize(HudApplication.getAppContext(), (long)n));
                    final byte[] array = new byte[n];
                    new Random().nextBytes(array);
                    outputStream.write(array);
                    if (httpURLConnection.getResponseCode() == 200) {
                        DebugReceiver.sLogger.d("POST succeeded");
                    }
                }
                catch (SocketTimeoutException ex) {
                    DebugReceiver.sLogger.e("Socket timed out Exception ", ex);
                }
                catch (IOException ex2) {
                    DebugReceiver.sLogger.e("IOException ", ex2);
                }
                catch (Exception ex3) {
                    DebugReceiver.sLogger.e("Exception ", ex3);
                }
                finally {
                    final double n2 = SystemClock.elapsedRealtime() - 0L;
                    DebugReceiver.sLogger.e("Upload finished, Size : " + 0L / 1000L + ", Time Taken :" + (long)n2 + " milliseconds , RawSpeed :" + (float)(0L / 1000.0f / (n2 / 1000.0)) + " kBps");
                    IOUtils.closeStream(null);
                }
            }
        }, 1);
    }
    
    private void testSendNotificationEvent(final Intent intent) {
        try {
            final NotificationEvent.Builder builder = new NotificationEvent.Builder();
            if (intent != null) {
                builder.id(intent.getIntExtra("id", 1));
                builder.category(NotificationCategory.CATEGORY_OTHER);
                builder.title(intent.getStringExtra("title"));
                builder.subtitle(intent.getStringExtra("subtitle"));
                builder.message(intent.getStringExtra("message"));
                builder.appId(intent.getStringExtra("appId"));
                builder.appName(intent.getStringExtra("appName"));
                this.mBus.post(new RemoteEvent(builder.build()));
            }
        }
        catch (Exception ex) {
            DebugReceiver.sLogger.e("Error while sending test Notification event ", ex);
        }
    }
    
    private void testShutDown() {
        this.mBus.post(new ShowScreenWithArgs(Screen.SCREEN_SHUTDOWN_CONFIRMATION, Shutdown.Reason.POWER_BUTTON.asBundle(), false));
    }
    
    private void testSwitchBandwidthLevel(Intent intent) {
        final int intExtra = intent.getIntExtra("BANDWIDTH_LEVEL", 1);
        intent = new Intent();
        intent.setAction("LINK_BANDWIDTH_LEVEL_CHANGED");
        intent.addCategory("NAVDY_LINK");
        intent.putExtra("EXTRA_BANDWIDTH_MODE", intExtra);
        HudApplication.getAppContext().sendBroadcast(intent);
    }
    
    private void testTTS(final Intent intent) {
        TTSUtils.sendSpeechRequest(DebugReceiver.TTS_TEXTS[new Random().nextInt(DebugReceiver.TTS_TEXTS.length)], SpeechRequest.Category.SPEECH_TURN_BY_TURN, null);
    }
    
    private void triggerANR() {
        new Handler(Looper.getMainLooper()).post((Runnable)new Runnable() {
            @Override
            public void run() {
                DebugReceiver.this.busywait(10000);
            }
        });
    }
    
    private void triggerBroadcastANR() {
        this.busywait(12000);
    }
    
    private void triggerCrash() {
        throw new IllegalStateException("Oops");
    }
    
    public void onReceive(final Context context, final Intent intent) {
    }
    
    private static class PrinterImpl implements Printer
    {
        public void println(final String s) {
            DebugReceiver.sLogger.v(s);
        }
    }
}
