package com.navdy.hud.app.service;

import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.connection.ConnectionStatus;
import com.navdy.service.library.device.connection.Connection;
import com.squareup.wire.Message;
import android.bluetooth.BluetoothDevice;
import java.util.Iterator;
import java.util.List;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.RemoteDeviceRegistry;
import android.os.SystemClock;
import java.util.ArrayDeque;
import android.content.Context;
import com.navdy.service.library.log.Logger;
import android.os.Handler;
import java.util.Deque;
import com.navdy.service.library.device.RemoteDevice;

class DeviceSearch implements Listener
{
    private static final int SEARCH_TIMEOUT = 15000;
    private Deque<RemoteDevice> connectedDevices;
    private Deque<RemoteDevice> connectingDevices;
    private EventSink eventSink;
    private final Handler handler;
    protected final Logger logger;
    private Deque<RemoteDevice> remoteDevices;
    private long startTime;
    private boolean started;
    
    public DeviceSearch(final Context context, final EventSink eventSink, final boolean b) {
        this.logger = new Logger(this.getClass());
        this.remoteDevices = new ArrayDeque<RemoteDevice>();
        this.connectingDevices = new ArrayDeque<RemoteDevice>();
        this.connectedDevices = new ArrayDeque<RemoteDevice>();
        this.started = false;
        this.startTime = SystemClock.elapsedRealtime();
        this.handler = new Handler();
        this.eventSink = eventSink;
        final List<ConnectionInfo> pairedConnections = RemoteDeviceRegistry.getInstance(context).getPairedConnections();
        for (int i = 0; i < pairedConnections.size(); ++i) {
            final RemoteDevice remoteDevice = new RemoteDevice(context, pairedConnections.get(i), b);
            remoteDevice.addListener((RemoteDevice.Listener)this);
            this.remoteDevices.add(remoteDevice);
        }
    }
    
    private void closeAll(final Deque<RemoteDevice> deque) {
        for (final RemoteDevice remoteDevice : deque) {
            remoteDevice.removeListener((RemoteDevice.Listener)this);
            this.queueDisconnect(remoteDevice);
        }
    }
    
    private RemoteDevice findDevice(final BluetoothDevice bluetoothDevice, final Deque<RemoteDevice> deque) {
        return this.findDevice(bluetoothDevice.getAddress(), deque);
    }
    
    private RemoteDevice findDevice(final String s, final Deque<RemoteDevice> deque) {
        for (final RemoteDevice remoteDevice : deque) {
            if (remoteDevice.getDeviceId().getBluetoothAddress().equals(s)) {
                return remoteDevice;
            }
        }
        return null;
    }
    
    private void queueDisconnect(final RemoteDevice remoteDevice) {
        if (remoteDevice != null) {
            this.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    remoteDevice.disconnect();
                }
            });
        }
    }
    
    private void sendEvent(final Message message) {
        if (this.eventSink != null) {
            this.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    DeviceSearch.this.eventSink.onEvent(message);
                }
            });
        }
    }
    
    public void close() {
        synchronized (this) {
            this.stop();
            this.closeAll(this.connectingDevices);
            this.closeAll(this.connectedDevices);
        }
    }
    
    public boolean forgetDevice(final BluetoothDevice bluetoothDevice) {
        while (true) {
            boolean b = true;
            synchronized (this) {
                this.logger.i("DeviceSearch forgetting:" + bluetoothDevice);
                final RemoteDevice device = this.findDevice(bluetoothDevice, this.connectingDevices);
                if (device != null) {
                    this.logger.i("DeviceSearch removing device from connecting devices");
                    this.connectingDevices.remove(device);
                    this.queueDisconnect(device);
                }
                else {
                    final RemoteDevice device2 = this.findDevice(bluetoothDevice, this.remoteDevices);
                    if (device2 == null) {
                        return false;
                    }
                    this.logger.i("DeviceSearch removing device from remote devices");
                    this.remoteDevices.remove(device2);
                }
                return b;
            }
            b = false;
            return b;
        }
    }
    
    public void handleDisconnect(final BluetoothDevice bluetoothDevice) {
        synchronized (this) {
            this.queueDisconnect(this.findDevice(bluetoothDevice, this.connectedDevices));
        }
    }
    
    public boolean next() {
        while (true) {
            boolean b = false;
            Label_0099: {
                synchronized (this) {
                    if (SystemClock.elapsedRealtime() - this.startTime > 15000L) {
                        this.stop();
                    }
                    else {
                        if (this.remoteDevices.size() <= 0) {
                            break Label_0099;
                        }
                        this.start();
                        final RemoteDevice remoteDevice = this.remoteDevices.removeFirst();
                        if (remoteDevice.connect()) {
                            this.connectingDevices.add(remoteDevice);
                        }
                        else {
                            this.remoteDevices.addLast(remoteDevice);
                        }
                        b = true;
                    }
                    return b;
                }
            }
            if (this.connectingDevices.size() > 0) {
                b = true;
                return b;
            }
            this.stop();
            return b;
        }
    }
    
    @Override
    public void onDeviceConnectFailure(final RemoteDevice remoteDevice, final ConnectionFailureCause connectionFailureCause) {
        synchronized (this) {
            this.logger.i("DeviceSearch connect failure:" + remoteDevice.getDeviceId());
            if (this.connectingDevices.remove(remoteDevice)) {
                this.remoteDevices.addLast(remoteDevice);
            }
        }
    }
    
    @Override
    public void onDeviceConnected(final RemoteDevice remoteDevice) {
        synchronized (this) {
            this.logger.i("DeviceSearch connected:" + remoteDevice.getDeviceId());
            this.connectingDevices.remove(remoteDevice);
            this.connectedDevices.add(remoteDevice);
            this.sendEvent(new ConnectionStatus(ConnectionStatus.Status.CONNECTION_FOUND, remoteDevice.getDeviceId().toString()));
        }
    }
    
    @Override
    public void onDeviceConnecting(final RemoteDevice remoteDevice) {
    }
    
    @Override
    public void onDeviceDisconnected(final RemoteDevice remoteDevice, final DisconnectCause disconnectCause) {
        synchronized (this) {
            this.logger.i("DeviceSearch disconnected:" + remoteDevice.getDeviceId());
            this.connectedDevices.remove(remoteDevice);
            this.remoteDevices.addLast(remoteDevice);
            this.sendEvent(new ConnectionStatus(ConnectionStatus.Status.CONNECTION_LOST, remoteDevice.getDeviceId().toString()));
        }
    }
    
    @Override
    public void onNavdyEventReceived(final RemoteDevice remoteDevice, final NavdyEvent navdyEvent) {
    }
    
    @Override
    public void onNavdyEventReceived(final RemoteDevice remoteDevice, final byte[] array) {
    }
    
    public RemoteDevice select(final NavdyDeviceId navdyDeviceId) {
        synchronized (this) {
            final String bluetoothAddress = navdyDeviceId.getBluetoothAddress();
            RemoteDevice device = this.findDevice(bluetoothAddress, this.remoteDevices);
            final RemoteDevice device2 = this.findDevice(bluetoothAddress, this.connectingDevices);
            final RemoteDevice device3 = this.findDevice(bluetoothAddress, this.connectedDevices);
            if (device != null) {
                this.remoteDevices.remove(device);
                device.removeListener((RemoteDevice.Listener)this);
            }
            else if (device2 != null) {
                this.connectingDevices.remove(device2);
                device2.removeListener((RemoteDevice.Listener)this);
                device = device2;
            }
            else if (device3 != null) {
                this.connectedDevices.remove(device3);
                device3.removeListener((RemoteDevice.Listener)this);
                device = device3;
            }
            else {
                device = null;
            }
            return device;
        }
    }
    
    public void start() {
        if (!this.started) {
            this.started = true;
            this.sendEvent(new ConnectionStatus(ConnectionStatus.Status.CONNECTION_SEARCH_STARTED, null));
        }
    }
    
    public void stop() {
        if (this.started) {
            this.started = false;
            this.sendEvent(new ConnectionStatus(ConnectionStatus.Status.CONNECTION_SEARCH_FINISHED, null));
        }
    }
    
    public interface EventSink
    {
        void onEvent(final Message p0);
    }
}
