package com.navdy.hud.app.screen;

import mortar.Presenter;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.service.ShutdownMonitor;
import com.navdy.hud.app.event.Shutdown;
import com.navdy.hud.app.device.dial.DialManager;
import android.content.res.Resources;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.toast.ToastManager;
import android.os.Bundle;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.device.dial.DialFirmwareUpdater;
import android.content.SharedPreferences;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import dagger.Provides;
import com.navdy.hud.app.view.DialUpdateProgressView;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.service.library.events.ui.Screen;
import android.os.Looper;
import com.navdy.service.library.log.Logger;
import android.os.Handler;
import flow.Layout;

@Layout(R.layout.screen_dial_update_progress)
public class DialUpdateProgressScreen extends BaseScreen
{
    private static final String DIAL_UPDATE_ERROR_TOAST_ID = "dial-fw-update-err";
    public static final String EXTRA_PROGRESS_CAUSE = "PROGRESS_CAUSE";
    public static final int POST_OTA_PAIRING_DELAY = 6000;
    public static final int POST_OTA_SHUTDOWN_DELAY = 30000;
    public static final int SETTINGS_SCREEN = 1;
    public static final int SHUTDOWN_SCREEN = 2;
    private static Handler handler;
    private static final Logger sLogger;
    public static boolean updateStarted;
    
    static {
        sLogger = new Logger(DialUpdateProgressScreen.class);
        DialUpdateProgressScreen.handler = new Handler(Looper.getMainLooper());
        DialUpdateProgressScreen.updateStarted = false;
    }
    
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    @Override
    public String getMortarScopeName() {
        return DialUpdateProgressScreen.class.getName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_DIAL_UPDATE_PROGRESS;
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { DialUpdateProgressView.class })
    public class Module
    {
        @Provides
        DialUpdateProgressScreen provideScreen() {
            return DialUpdateProgressScreen.this;
        }
    }
    
    @Singleton
    public static class Presenter extends BasePresenter<DialUpdateProgressView>
    {
        private int cause;
        @Inject
        Bus mBus;
        @Inject
        SharedPreferences mPreferences;
        @Inject
        DialUpdateProgressScreen mScreen;
        
        private void showDialUpdateErrorToast(final DialFirmwareUpdater.Error error) {
            final Resources resources = HudApplication.getAppContext().getResources();
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 10000);
            bundle.putInt("8", R.drawable.icon_dial_update);
            bundle.putString("1", resources.getString(R.string.dial_update_err));
            bundle.putString("4", error.name());
            bundle.putInt("5", R.style.Glances_1);
            bundle.putBoolean("12", true);
            final ToastManager instance = ToastManager.getInstance();
            instance.dismissCurrentToast("dial-fw-update-err");
            instance.addToast(new ToastManager.ToastParams("dial-fw-update-err", bundle, null, true, false));
        }
        
        public void cancelUpdate() {
            DialManager.getInstance().getDialFirmwareUpdater().cancelUpdate();
        }
        
        public void finish(final DialFirmwareUpdater.Error error) {
            DialUpdateProgressScreen.updateStarted = false;
            switch (this.cause) {
                case 2:
                    DialUpdateProgressScreen.handler.postDelayed((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            Presenter.this.mBus.post(new Shutdown(Shutdown.Reason.DIAL_OTA));
                        }
                    }, 30000L);
                    break;
            }
            ShutdownMonitor.getInstance().disableScreenDim(false);
            if (error != DialFirmwareUpdater.Error.NONE) {
                ToastManager.getInstance().disableToasts(false);
                NotificationManager.getInstance().enableNotifications(true);
                this.mBus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BACK).build());
                this.cancelUpdate();
                this.showDialUpdateErrorToast(error);
            }
            else {
                DialUpdateProgressScreen.handler.postDelayed((Runnable)new Runnable() {
                    @Override
                    public void run() {
                        final Bundle bundle = new Bundle();
                        bundle.putString("OtaDialNameKey", DialManager.getInstance().getDialFirmwareUpdater().getDialName());
                        Presenter.this.mBus.post(new ShowScreenWithArgs(Screen.SCREEN_DIAL_PAIRING, bundle, false));
                    }
                }, 6000L);
            }
        }
        
        public DialFirmwareUpdater.Versions getVersions() {
            return DialManager.getInstance().getDialFirmwareUpdater().getVersions();
        }
        
        @Override
        public void onLoad(Bundle arguments) {
            super.onLoad(arguments);
            if (this.mScreen != null) {
                arguments = this.mScreen.arguments;
                if (arguments != null) {
                    this.cause = arguments.getInt("PROGRESS_CAUSE", 2);
                }
            }
        }
        
        public void startUpdate() {
            if (!DialUpdateProgressScreen.updateStarted) {
                DialUpdateProgressScreen.updateStarted = true;
                ShutdownMonitor.getInstance().disableScreenDim(true);
                DialManager.getInstance().getDialFirmwareUpdater().startUpdate((DialFirmwareUpdater.UpdateProgressListener)new DialFirmwareUpdater.UpdateProgressListener() {
                    @Override
                    public void onFinished(final Error error, final String s) {
                        DialUpdateProgressScreen.sLogger.v("onFinished:" + error + " , " + s);
                        DialUpdateProgressScreen.handler.post((Runnable)new Runnable() {
                            @Override
                            public void run() {
                                Presenter.this.finish(error);
                            }
                        });
                    }
                    
                    @Override
                    public void onProgress(final int n) {
                        DialUpdateProgressScreen.handler.post((Runnable)new Runnable() {
                            @Override
                            public void run() {
                                final DialUpdateProgressView dialUpdateProgressView = (DialUpdateProgressView)mortar.Presenter.this.getView();
                                if (dialUpdateProgressView != null) {
                                    dialUpdateProgressView.setProgress(n);
                                }
                            }
                        });
                    }
                });
            }
        }
    }
}
