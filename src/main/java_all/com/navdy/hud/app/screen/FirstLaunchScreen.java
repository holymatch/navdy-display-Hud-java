package com.navdy.hud.app.screen;

import com.squareup.otto.Subscribe;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.device.dial.DialConstants;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import android.os.Bundle;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.util.os.SystemProperties;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.device.PowerManager;
import com.navdy.hud.app.device.dial.DialManager;
import javax.inject.Inject;
import com.squareup.otto.Bus;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.FirstLaunchView;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.service.library.events.ui.Screen;
import flow.Flow;
import com.navdy.service.library.log.Logger;
import flow.Layout;

@Layout(R.layout.screen_first_launch)
public class FirstLaunchScreen extends BaseScreen
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(FirstLaunchScreen.class);
    }
    
    @Override
    public int getAnimationIn(final Flow.Direction direction) {
        return -1;
    }
    
    @Override
    public int getAnimationOut(final Flow.Direction direction) {
        return -1;
    }
    
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    @Override
    public String getMortarScopeName() {
        return this.getClass().getName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_FIRST_LAUNCH;
    }
    
    private static class MediaServerUp
    {
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { FirstLaunchView.class })
    public class Module
    {
    }
    
    @Singleton
    public static class Presenter extends BasePresenter<FirstLaunchView>
    {
        @Inject
        Bus bus;
        private DialManager dialManager;
        @Inject
        PowerManager powerManager;
        private boolean registered;
        private boolean stateChecked;
        @Inject
        UIStateManager uiStateManager;
        
        private void checkForMediaService() {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    // 
                    This method could not be decompiled.
                    // 
                    // Original Bytecode:
                    // 
                    //     1: istore_1       
                    //     2: aconst_null    
                    //     3: astore_2       
                    //     4: invokestatic    android/os/SystemClock.elapsedRealtime:()J
                    //     7: lstore_3       
                    //     8: invokestatic    com/navdy/hud/app/screen/FirstLaunchScreen.access$000:()Lcom/navdy/service/library/log/Logger;
                    //    11: astore          5
                    //    13: new             Ljava/lang/StringBuilder;
                    //    16: astore          6
                    //    18: aload           6
                    //    20: invokespecial   java/lang/StringBuilder.<init>:()V
                    //    23: aload           6
                    //    25: ldc             "creating media player counter="
                    //    27: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //    30: astore          6
                    //    32: iload_1        
                    //    33: iconst_1       
                    //    34: iadd           
                    //    35: istore          7
                    //    37: aload           5
                    //    39: aload           6
                    //    41: iload_1        
                    //    42: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
                    //    45: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                    //    48: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
                    //    51: new             Landroid/media/MediaPlayer;
                    //    54: astore_2       
                    //    55: aload_2        
                    //    56: invokespecial   android/media/MediaPlayer.<init>:()V
                    //    59: aload_2        
                    //    60: invokevirtual   android/media/MediaPlayer.release:()V
                    //    63: invokestatic    com/navdy/hud/app/screen/FirstLaunchScreen.access$000:()Lcom/navdy/service/library/log/Logger;
                    //    66: astore_2       
                    //    67: new             Ljava/lang/StringBuilder;
                    //    70: astore          5
                    //    72: aload           5
                    //    74: invokespecial   java/lang/StringBuilder.<init>:()V
                    //    77: aload_2        
                    //    78: aload           5
                    //    80: ldc             "created media player time="
                    //    82: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                    //    85: invokestatic    android/os/SystemClock.elapsedRealtime:()J
                    //    88: lload_3        
                    //    89: lsub           
                    //    90: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
                    //    93: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                    //    96: invokevirtual   com/navdy/service/library/log/Logger.v:(Ljava/lang/String;)V
                    //    99: aload_0        
                    //   100: getfield        com/navdy/hud/app/screen/FirstLaunchScreen$Presenter$2.this$0:Lcom/navdy/hud/app/screen/FirstLaunchScreen$Presenter;
                    //   103: getfield        com/navdy/hud/app/screen/FirstLaunchScreen$Presenter.bus:Lcom/squareup/otto/Bus;
                    //   106: astore_2       
                    //   107: new             Lcom/navdy/hud/app/screen/FirstLaunchScreen$MediaServerUp;
                    //   110: astore          5
                    //   112: aload           5
                    //   114: aconst_null    
                    //   115: invokespecial   com/navdy/hud/app/screen/FirstLaunchScreen$MediaServerUp.<init>:(Lcom/navdy/hud/app/screen/FirstLaunchScreen$1;)V
                    //   118: aload_2        
                    //   119: aload           5
                    //   121: invokevirtual   com/squareup/otto/Bus.post:(Ljava/lang/Object;)V
                    //   124: return         
                    //   125: astore          5
                    //   127: invokestatic    com/navdy/hud/app/screen/FirstLaunchScreen.access$000:()Lcom/navdy/service/library/log/Logger;
                    //   130: ldc             "checkForMediaService"
                    //   132: aload           5
                    //   134: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
                    //   137: aload_2        
                    //   138: ifnull          145
                    //   141: aload_2        
                    //   142: invokevirtual   android/media/MediaPlayer.release:()V
                    //   145: sipush          1000
                    //   148: invokestatic    com/navdy/hud/app/util/GenericUtil.sleep:(I)V
                    //   151: goto            2
                    //   154: astore_2       
                    //   155: invokestatic    com/navdy/hud/app/screen/FirstLaunchScreen.access$000:()Lcom/navdy/service/library/log/Logger;
                    //   158: aload_2        
                    //   159: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/Throwable;)V
                    //   162: goto            145
                    //   165: aconst_null    
                    //   166: astore_2       
                    //   167: astore          5
                    //   169: iload           7
                    //   171: istore_1       
                    //   172: goto            127
                    //   175: astore          5
                    //   177: iload           7
                    //   179: istore_1       
                    //   180: goto            127
                    //    Exceptions:
                    //  Try           Handler
                    //  Start  End    Start  End    Type                 
                    //  -----  -----  -----  -----  ---------------------
                    //  4      32     125    127    Ljava/lang/Throwable;
                    //  37     59     165    175    Ljava/lang/Throwable;
                    //  59     63     175    183    Ljava/lang/Throwable;
                    //  63     124    165    175    Ljava/lang/Throwable;
                    //  141    145    154    165    Ljava/lang/Throwable;
                    // 
                    // The error that occurred was:
                    // 
                    // java.lang.IndexOutOfBoundsException: Index: 88, Size: 88
                    //     at java.util.ArrayList.rangeCheck(Unknown Source)
                    //     at java.util.ArrayList.get(Unknown Source)
                    //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
                    //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1163)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1010)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
                    //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                    //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                    //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
                    //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
                    //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
                    //     at java.lang.Thread.run(Unknown Source)
                    // 
                    throw new IllegalStateException("An error occurred while decompiling this method.");
                }
            }, 1);
        }
        
        private void checkState() {
            if (this.stateChecked) {
                FirstLaunchScreen.sLogger.v("state already checked");
            }
            else {
                this.stateChecked = true;
                if (this.powerManager.inQuietMode()) {
                    SystemProperties.set("service.bootanim.exit", "1");
                }
                else {
                    final int bondedDialCount = this.dialManager.getBondedDialCount();
                    if (bondedDialCount > 0) {
                        FirstLaunchScreen.sLogger.v("bonded dial count=" + bondedDialCount + " , go to next screen, stop boot animation");
                        SystemProperties.set("service.bootanim.exit", "1");
                        this.exitScreen();
                    }
                    else {
                        FirstLaunchScreen.sLogger.v("checkForMediaService");
                        this.checkForMediaService();
                    }
                }
            }
        }
        
        private void exitScreen() {
            if (!RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
                FirstLaunchScreen.sLogger.v("go to welcome screen");
                final Bundle bundle = new Bundle();
                bundle.putString(WelcomeScreen.ARG_ACTION, WelcomeScreen.ACTION_RECONNECT);
                this.bus.post(new ShowScreenWithArgs(Screen.SCREEN_WELCOME, bundle, false));
            }
            else {
                FirstLaunchScreen.sLogger.v("go to default screen");
                this.bus.post(new ShowScreen.Builder().screen(RemoteDeviceManager.getInstance().getUiStateManager().getDefaultMainActiveScreen()).build());
            }
        }
        
        private void launchDialPairing() {
            SystemProperties.set("service.bootanim.exit", "1");
            FirstLaunchScreen.sLogger.v("bonded dial count=" + this.dialManager.getBondedDialCount() + " , go to dial pairing screen");
            this.bus.post(new ShowScreen.Builder().screen(Screen.SCREEN_DIAL_PAIRING).build());
        }
        
        @Subscribe
        public void onDialManagerInitEvent(final DialConstants.DialManagerInitEvent dialManagerInitEvent) {
            FirstLaunchScreen.sLogger.v("got dialmanager init event");
            if (this.registered) {
                final FirstLaunchView firstLaunchView = this.getView();
                if (firstLaunchView != null) {
                    firstLaunchView.firstLaunchLogo.animate().translationY((float)ToastPresenter.ANIMATION_TRANSLATION).alpha(0.0f).withEndAction((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            Presenter.this.checkState();
                        }
                    }).start();
                }
                else {
                    this.checkState();
                }
            }
            else {
                this.checkState();
            }
        }
        
        @Override
        public void onLoad(final Bundle bundle) {
            FirstLaunchScreen.sLogger.v("onLoad");
            this.bus.register(this);
            this.registered = true;
            this.dialManager = DialManager.getInstance();
            if (this.dialManager.isInitialized()) {
                FirstLaunchScreen.sLogger.v("already inititalized");
                this.checkState();
            }
            else {
                FirstLaunchScreen.sLogger.v("show view");
            }
            this.uiStateManager.enableSystemTray(false);
            this.uiStateManager.enableNotificationColor(false);
            FirstLaunchScreen.sLogger.v("systemtray:invisible");
        }
        
        @Subscribe
        public void onMediaServerUp(final MediaServerUp mediaServerUp) {
            FirstLaunchScreen.sLogger.v("media service is up, stop boot-animation");
            this.launchDialPairing();
        }
        
        @Override
        protected void onUnload() {
            FirstLaunchScreen.sLogger.v("onUnload");
            if (this.registered) {
                this.bus.unregister(this);
            }
            super.onUnload();
            this.uiStateManager.enableSystemTray(true);
            this.uiStateManager.enableNotificationColor(true);
            FirstLaunchScreen.sLogger.v("systemtray:visible");
        }
    }
}
