package com.navdy.hud.app.screen;

import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.hud.app.ui.framework.UIStateManager;
import android.os.Bundle;
import android.content.SharedPreferences;
import com.squareup.otto.Bus;
import javax.inject.Inject;
import com.navdy.hud.app.gesture.GestureServiceConnector;
import javax.inject.Singleton;
import com.navdy.hud.app.ui.framework.BasePresenter;
import com.navdy.hud.app.view.GestureVideoCaptureView;
import com.navdy.hud.app.view.ScrollableTextPresenterLayout;
import com.navdy.hud.app.view.GestureLearningView;
import com.navdy.hud.app.view.LearnGestureScreenLayout;
import com.navdy.hud.app.ui.activity.Main;
import dagger.Module;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.log.Logger;
import flow.Layout;

@Layout(R.layout.screen_learn_gesture_layout)
public class GestureLearningScreen extends BaseScreen
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(GestureLearningScreen.class);
    }
    
    @Override
    public Object getDaggerModule() {
        return new Module();
    }
    
    @Override
    public String getMortarScopeName() {
        return GestureLearningScreen.class.getSimpleName();
    }
    
    @Override
    public Screen getScreen() {
        return Screen.SCREEN_GESTURE_LEARNING;
    }
    
    @dagger.Module(addsTo = Main.Module.class, injects = { LearnGestureScreenLayout.class, GestureLearningView.class, ScrollableTextPresenterLayout.class, GestureVideoCaptureView.class })
    public class Module
    {
    }
    
    @Singleton
    public static class Presenter extends BasePresenter<LearnGestureScreenLayout>
    {
        @Inject
        GestureServiceConnector gestureServiceConnector;
        @Inject
        Bus mBus;
        @Inject
        SharedPreferences mPreferences;
        Bundle tipsBundle;
        @Inject
        UIStateManager uiStateManager;
        
        public void finish() {
            this.mBus.post(new ShowScreen.Builder().screen(Screen.SCREEN_BACK).build());
        }
        
        public void hideCameraSensorBlocked() {
            final LearnGestureScreenLayout learnGestureScreenLayout = this.getView();
            if (learnGestureScreenLayout != null) {
                learnGestureScreenLayout.hideSensorBlocked();
            }
        }
        
        public void hideCaptureView() {
            final LearnGestureScreenLayout learnGestureScreenLayout = this.getView();
            if (learnGestureScreenLayout != null) {
                learnGestureScreenLayout.hideCaptureGestureVideosView();
            }
        }
        
        public void hideTips() {
            final LearnGestureScreenLayout learnGestureScreenLayout = this.getView();
            if (learnGestureScreenLayout != null) {
                learnGestureScreenLayout.hideTips();
            }
        }
        
        @Override
        public void onLoad(final Bundle bundle) {
            GestureLearningScreen.sLogger.v("onLoad");
            super.onLoad(bundle);
            this.uiStateManager.enableSystemTray(false);
            NotificationManager.getInstance().enableNotifications(false);
            ToastManager.getInstance().disableToasts(true);
        }
        
        @Override
        protected void onUnload() {
            GestureLearningScreen.sLogger.v("onUnload");
            this.uiStateManager.enableSystemTray(true);
            NotificationManager.getInstance().enableNotifications(true);
            ToastManager.getInstance().disableToasts(false);
            super.onUnload();
        }
        
        public void showCameraSensorBlocked() {
            final LearnGestureScreenLayout learnGestureScreenLayout = this.getView();
            if (learnGestureScreenLayout != null) {
                learnGestureScreenLayout.showSensorBlocked();
            }
        }
        
        public void showCaptureView() {
            final LearnGestureScreenLayout learnGestureScreenLayout = this.getView();
            if (learnGestureScreenLayout != null) {
                learnGestureScreenLayout.showCaptureGestureVideosView();
            }
        }
        
        public void showTips() {
            final LearnGestureScreenLayout learnGestureScreenLayout = this.getView();
            if (learnGestureScreenLayout != null) {
                learnGestureScreenLayout.showTips();
            }
        }
    }
}
