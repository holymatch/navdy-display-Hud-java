package com.navdy.hud.app.analytics;

import org.jetbrains.annotations.NotNull;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015" }, d2 = { "Lcom/navdy/hud/app/analytics/RawSpeed;", "", "speed", "", "timeStamp", "", "(FJ)V", "getSpeed", "()F", "getTimeStamp", "()J", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class RawSpeed
{
    private final float speed;
    private final long timeStamp;
    
    public RawSpeed(final float speed, final long timeStamp) {
        this.speed = speed;
        this.timeStamp = timeStamp;
    }
    
    public final float component1() {
        return this.speed;
    }
    
    public final long component2() {
        return this.timeStamp;
    }
    
    @NotNull
    public final RawSpeed copy(final float n, final long n2) {
        return new RawSpeed(n, n2);
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = false;
        if (this != o) {
            boolean b2 = b;
            if (!(o instanceof RawSpeed)) {
                return b2;
            }
            final RawSpeed rawSpeed = (RawSpeed)o;
            b2 = b;
            if (Float.compare(this.speed, rawSpeed.speed) != 0) {
                return b2;
            }
            int n;
            if (this.timeStamp == rawSpeed.timeStamp) {
                n = 1;
            }
            else {
                n = 0;
            }
            b2 = b;
            if (n == 0) {
                return b2;
            }
        }
        return true;
    }
    
    public final float getSpeed() {
        return this.speed;
    }
    
    public final long getTimeStamp() {
        return this.timeStamp;
    }
    
    @Override
    public int hashCode() {
        final int floatToIntBits = Float.floatToIntBits(this.speed);
        final long timeStamp = this.timeStamp;
        return floatToIntBits * 31 + (int)(timeStamp ^ timeStamp >>> 32);
    }
    
    @Override
    public String toString() {
        return "RawSpeed(speed=" + this.speed + ", timeStamp=" + this.timeStamp + ")";
    }
}
