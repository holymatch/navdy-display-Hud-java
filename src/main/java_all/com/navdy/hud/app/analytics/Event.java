package com.navdy.hud.app.analytics;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import com.navdy.service.library.log.Logger;

class Event
{
    public static final String TAG = "Localytics";
    private static final Logger sLogger;
    public Map<String, String> argMap;
    public String tag;
    
    static {
        sLogger = new Logger("Localytics");
    }
    
    public Event(final String tag, final Map<String, String> argMap) {
        this.tag = tag;
        this.argMap = argMap;
    }
    
    public Event(final String tag, final String... array) {
        this.tag = tag;
        this.argMap = new HashMap<String, String>();
        int i;
        for (i = 0; i < array.length - 1; i += 2) {
            String s = array[i + 1];
            final Map<String, String> argMap = this.argMap;
            final String s2 = array[i];
            if (s == null) {
                s = "";
            }
            argMap.put(s2, s);
        }
        if (i < array.length) {
            Event.sLogger.e("Odd number of event arguments for tag " + tag);
        }
    }
    
    String getChangedFields(final Event event) {
        final StringBuilder sb = new StringBuilder();
        for (final Map.Entry<String, String> entry : this.argMap.entrySet()) {
            final String s = entry.getKey();
            final String s2 = event.argMap.get(s);
            final String s3 = entry.getValue();
            if ((s3 != null && !s3.equals(s2)) || (s3 == null && s2 != null)) {
                if (sb.length() == 0) {
                    sb.append("|");
                }
                sb.append(s);
                sb.append("|");
            }
        }
        return sb.toString();
    }
    
    @Override
    public String toString() {
        return "Event{tag='" + this.tag + '\'' + ", argMap=" + this.argMap + '}';
    }
}
