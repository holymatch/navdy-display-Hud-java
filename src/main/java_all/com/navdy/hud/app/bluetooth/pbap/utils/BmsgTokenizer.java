package com.navdy.hud.app.bluetooth.pbap.utils;

import android.util.Log;
import java.text.ParseException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public final class BmsgTokenizer
{
    private final Matcher mMatcher;
    private final int mOffset;
    private int mPos;
    private final String mStr;
    
    public BmsgTokenizer(final String s) {
        this(s, 0);
    }
    
    public BmsgTokenizer(final String mStr, final int mOffset) {
        this.mPos = 0;
        this.mStr = mStr;
        this.mOffset = mOffset;
        this.mMatcher = Pattern.compile("(([^:]*):(.*))?\r\n").matcher(mStr);
        this.mPos = this.mMatcher.regionStart();
    }
    
    public Property next() throws ParseException {
        return this.next(false);
    }
    
    public Property next(final boolean b) throws ParseException {
        int n = 0;
        int n2;
        do {
            this.mMatcher.region(this.mPos, this.mMatcher.regionEnd());
            if (!this.mMatcher.lookingAt()) {
                if (b) {
                    return null;
                }
                throw new ParseException("Property or empty line expected", this.pos());
            }
            else {
                this.mPos = this.mMatcher.end();
                n2 = n;
                if (this.mMatcher.group(1) == null) {
                    continue;
                }
                n2 = 1;
            }
        } while ((n = n2) == 0);
        return new Property(this.mMatcher.group(2), this.mMatcher.group(3));
    }
    
    public int pos() {
        return this.mPos + this.mOffset;
    }
    
    public String remaining() {
        return this.mStr.substring(this.mPos);
    }
    
    public static class Property
    {
        public final String name;
        public final String value;
        
        public Property(final String name, final String value) {
            if (name == null || value == null) {
                throw new IllegalArgumentException();
            }
            this.name = name;
            this.value = value;
            Log.v("BMSG >> ", this.toString());
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof Property && ((Property)o).name.equals(this.name) && ((Property)o).value.equals(this.value);
        }
        
        @Override
        public String toString() {
            return this.name + ":" + this.value;
        }
    }
}
