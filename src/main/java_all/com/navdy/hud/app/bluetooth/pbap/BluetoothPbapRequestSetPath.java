package com.navdy.hud.app.bluetooth.pbap;

import com.navdy.hud.app.bluetooth.obex.HeaderSet;
import java.io.IOException;
import android.util.Log;
import com.navdy.hud.app.bluetooth.obex.ClientSession;

final class BluetoothPbapRequestSetPath extends BluetoothPbapRequest
{
    private static final String TAG = "BTPbapReqSetPath";
    private SetPathDir mDir;
    
    public BluetoothPbapRequestSetPath(final String s) {
        this.mDir = SetPathDir.DOWN;
        this.mHeaderSet.setHeader(1, s);
    }
    
    public BluetoothPbapRequestSetPath(final boolean b) {
        this.mHeaderSet.setEmptyNameHeader();
        if (b) {
            this.mDir = SetPathDir.UP;
        }
        else {
            this.mDir = SetPathDir.ROOT;
        }
    }
    
    @Override
    public void execute(final ClientSession clientSession) {
        Log.v("BTPbapReqSetPath", "execute");
        final HeaderSet set = null;
        try {
            HeaderSet set2 = null;
            switch (this.mDir) {
                default:
                    set2 = set;
                    break;
                case ROOT:
                case DOWN:
                    set2 = clientSession.setPath(this.mHeaderSet, false, false);
                    break;
                case UP:
                    set2 = clientSession.setPath(this.mHeaderSet, true, false);
                    break;
            }
            this.mResponseCode = set2.getResponseCode();
        }
        catch (IOException ex) {
            this.mResponseCode = 208;
        }
    }
    
    private enum SetPathDir
    {
        DOWN, 
        ROOT, 
        UP;
    }
}
