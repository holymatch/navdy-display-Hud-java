package com.navdy.hud.app.bluetooth.obex;

public final class ApplicationParameter
{
    private byte[] mArray;
    private int mLength;
    private int mMaxLength;
    
    public ApplicationParameter() {
        this.mMaxLength = 1000;
        this.mArray = new byte[this.mMaxLength];
        this.mLength = 0;
    }
    
    public void addAPPHeader(final byte b, final byte b2, final byte[] array) {
        if (this.mLength + b2 + 2 > this.mMaxLength) {
            final byte[] mArray = new byte[this.mLength + b2 * 4];
            System.arraycopy(this.mArray, 0, mArray, 0, this.mLength);
            this.mArray = mArray;
            this.mMaxLength = this.mLength + b2 * 4;
        }
        this.mArray[this.mLength++] = b;
        this.mArray[this.mLength++] = b2;
        System.arraycopy(array, 0, this.mArray, this.mLength, b2);
        this.mLength += b2;
    }
    
    public byte[] getAPPparam() {
        final byte[] array = new byte[this.mLength];
        System.arraycopy(this.mArray, 0, array, 0, this.mLength);
        return array;
    }
    
    public static class TRIPLET_LENGTH
    {
        public static final byte FILTER_LENGTH = 8;
        public static final byte FORMAT_LENGTH = 1;
        public static final byte LISTSTARTOFFSET_LENGTH = 2;
        public static final byte MAXLISTCOUNT_LENGTH = 2;
        public static final byte NEWMISSEDCALLS_LENGTH = 1;
        public static final byte ORDER_LENGTH = 1;
        public static final byte PHONEBOOKSIZE_LENGTH = 2;
        public static final byte SEARCH_ATTRIBUTE_LENGTH = 1;
    }
    
    public static class TRIPLET_TAGID
    {
        public static final byte FILTER_TAGID = 6;
        public static final byte FORMAT_TAGID = 7;
        public static final byte LISTSTARTOFFSET_TAGID = 5;
        public static final byte MAXLISTCOUNT_TAGID = 4;
        public static final byte NEWMISSEDCALLS_TAGID = 9;
        public static final byte ORDER_TAGID = 1;
        public static final byte PHONEBOOKSIZE_TAGID = 8;
        public static final byte SEARCH_ATTRIBUTE_TAGID = 3;
        public static final byte SEARCH_VALUE_TAGID = 2;
    }
    
    public static class TRIPLET_VALUE
    {
        public static class FORMAT
        {
            public static final byte VCARD_VERSION_21 = 0;
            public static final byte VCARD_VERSION_30 = 1;
        }
        
        public static class ORDER
        {
            public static final byte ORDER_BY_ALPHANUMERIC = 1;
            public static final byte ORDER_BY_INDEX = 0;
            public static final byte ORDER_BY_PHONETIC = 2;
        }
        
        public static class SEARCHATTRIBUTE
        {
            public static final byte SEARCH_BY_NAME = 0;
            public static final byte SEARCH_BY_NUMBER = 1;
            public static final byte SEARCH_BY_SOUND = 2;
        }
    }
}
