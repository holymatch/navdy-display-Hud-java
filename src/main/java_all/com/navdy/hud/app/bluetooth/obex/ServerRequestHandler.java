package com.navdy.hud.app.bluetooth.obex;

public class ServerRequestHandler
{
    private long mConnectionId;
    
    protected ServerRequestHandler() {
        this.mConnectionId = -1L;
    }
    
    public long getConnectionId() {
        return this.mConnectionId;
    }
    
    public boolean isSrmSupported() {
        return false;
    }
    
    public int onAbort(final HeaderSet set, final HeaderSet set2) {
        return 209;
    }
    
    public void onAuthenticationFailure(final byte[] array) {
    }
    
    public void onClose() {
    }
    
    public int onConnect(final HeaderSet set, final HeaderSet set2) {
        return 160;
    }
    
    public int onDelete(final HeaderSet set, final HeaderSet set2) {
        return 209;
    }
    
    public void onDisconnect(final HeaderSet set, final HeaderSet set2) {
    }
    
    public int onGet(final Operation operation) {
        return 209;
    }
    
    public int onPut(final Operation operation) {
        return 209;
    }
    
    public int onSetPath(final HeaderSet set, final HeaderSet set2, final boolean b, final boolean b2) {
        return 209;
    }
    
    public void setConnectionId(final long mConnectionId) {
        if (mConnectionId < -1L || mConnectionId > 4294967295L) {
            throw new IllegalArgumentException("Illegal Connection ID");
        }
        this.mConnectionId = mConnectionId;
    }
    
    public void updateStatus(final String s) {
    }
}
