package com.navdy.hud.app.bluetooth.pbap;

import com.navdy.hud.app.bluetooth.obex.HeaderSet;
import java.io.IOException;
import android.util.Log;
import java.io.InputStream;
import java.util.ArrayList;
import com.navdy.hud.app.bluetooth.pbap.utils.ObexAppParameters;

final class BluetoothPbapRequestPullVcardListing extends BluetoothPbapRequest
{
    private static final String TAG = "BTPbapReqPullVcardL";
    private static final String TYPE = "x-bt/vcard-listing";
    private int mNewMissedCalls;
    private BluetoothPbapVcardListing mResponse;
    
    public BluetoothPbapRequestPullVcardListing(final String s, final byte b, final byte b2, final String s2, final int n, final int n2) {
        this.mResponse = null;
        this.mNewMissedCalls = -1;
        if (n < 0 || n > 65535) {
            throw new IllegalArgumentException("maxListCount should be [0..65535]");
        }
        if (n2 < 0 || n2 > 65535) {
            throw new IllegalArgumentException("listStartOffset should be [0..65535]");
        }
        String s3;
        if ((s3 = s) == null) {
            s3 = "";
        }
        this.mHeaderSet.setHeader(1, s3);
        this.mHeaderSet.setHeader(66, "x-bt/vcard-listing");
        final ObexAppParameters obexAppParameters = new ObexAppParameters();
        if (b >= 0) {
            obexAppParameters.add((byte)1, b);
        }
        if (s2 != null) {
            obexAppParameters.add((byte)3, b2);
            obexAppParameters.add((byte)2, s2);
        }
        if (n > 0) {
            obexAppParameters.add((byte)4, (short)n);
        }
        if (n2 > 0) {
            obexAppParameters.add((byte)5, (short)n2);
        }
        obexAppParameters.addToHeaderSet(this.mHeaderSet);
    }
    
    public ArrayList<BluetoothPbapCard> getList() {
        return this.mResponse.getList();
    }
    
    public int getNewMissedCalls() {
        return this.mNewMissedCalls;
    }
    
    @Override
    protected void readResponse(final InputStream inputStream) throws IOException {
        Log.v("BTPbapReqPullVcardL", "readResponse");
        this.mResponse = new BluetoothPbapVcardListing(inputStream);
    }
    
    @Override
    protected void readResponseHeaders(final HeaderSet set) {
        Log.v("BTPbapReqPullVcardL", "readResponseHeaders");
        final ObexAppParameters fromHeaderSet = ObexAppParameters.fromHeaderSet(set);
        if (fromHeaderSet.exists((byte)9)) {
            this.mNewMissedCalls = fromHeaderSet.getByte((byte)9);
        }
    }
}
