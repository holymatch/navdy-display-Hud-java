package com.navdy.hud.app.bluetooth.vcard.exception;

public class VCardNotSupportedException extends VCardException
{
    public VCardNotSupportedException() {
    }
    
    public VCardNotSupportedException(final String s) {
        super(s);
    }
}
