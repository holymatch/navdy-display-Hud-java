package com.navdy.hud.app.bluetooth.vcard;

import com.navdy.hud.app.bluetooth.vcard.exception.VCardException;
import java.io.IOException;
import java.io.InputStream;

public abstract class VCardParser
{
    public abstract void addInterpreter(final VCardInterpreter p0);
    
    public abstract void cancel();
    
    public abstract void parse(final InputStream p0) throws IOException, VCardException;
    
    @Deprecated
    public void parse(final InputStream inputStream, final VCardInterpreter vCardInterpreter) throws IOException, VCardException {
        this.addInterpreter(vCardInterpreter);
        this.parse(inputStream);
    }
    
    public abstract void parseOne(final InputStream p0) throws IOException, VCardException;
}
