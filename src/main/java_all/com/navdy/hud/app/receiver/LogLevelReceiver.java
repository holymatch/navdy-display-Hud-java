package com.navdy.hud.app.receiver;

import android.content.Intent;
import android.content.Context;
import com.navdy.service.library.log.Logger;
import android.content.BroadcastReceiver;

public class LogLevelReceiver extends BroadcastReceiver
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(LogLevelReceiver.class);
    }
    
    public void onReceive(final Context context, final Intent intent) {
        try {
            if (intent.getAction().equals("com.navdy.service.library.log.action.RELOAD")) {
                LogLevelReceiver.sLogger.i("Forcing loggers to reload cached log levels");
                Logger.reloadLogLevels();
            }
        }
        catch (Throwable t) {
            LogLevelReceiver.sLogger.e(t);
        }
    }
}
