package com.navdy.hud.app.maps.notification;

import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.ui.framework.UIStateManager;
import android.animation.AnimatorSet;
import android.view.LayoutInflater;
import com.navdy.hud.app.framework.notifications.NotificationType;
import android.view.View;
import android.content.Context;
import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import java.util.ArrayList;
import com.navdy.hud.app.maps.MapEvents;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.ViewGroup;
import com.squareup.otto.Bus;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import java.util.List;

public class TrafficEventNotification extends BaseTrafficNotification
{
    private static final int TAG_DISMISS = 1;
    private static String ahead;
    private static String dismiss;
    private static List<ChoiceLayout2.Choice> dismissChoices;
    private static String enableInternet;
    private static String noUpdatedTraffic;
    private static String slowTraffic;
    private static String stoppedTraffic;
    private static String trafficIncident;
    private static String unknownIncident;
    private Bus bus;
    private ChoiceLayout2.IListener choiceListener;
    private ViewGroup container;
    private ImageView image;
    private int notifColor;
    private TextView subTitle;
    private TextView title;
    private MapEvents.LiveTrafficEvent trafficEvent;
    
    static {
        TrafficEventNotification.dismissChoices = new ArrayList<ChoiceLayout2.Choice>(1);
    }
    
    public TrafficEventNotification(final Bus bus) {
        this.choiceListener = new ChoiceLayout2.IListener() {
            @Override
            public void executeItem(final Selection selection) {
                TrafficEventNotification.this.dismissNotification();
                TrafficEventNotification.this.trafficEvent = null;
            }
            
            @Override
            public void itemSelected(final Selection selection) {
            }
        };
        if (TrafficEventNotification.stoppedTraffic == null) {
            final Resources resources = HudApplication.getAppContext().getResources();
            TrafficEventNotification.stoppedTraffic = resources.getString(R.string.traffic_notification_text_stoppedtraffic);
            TrafficEventNotification.slowTraffic = resources.getString(R.string.traffic_notification_text_slowtraffic);
            TrafficEventNotification.trafficIncident = resources.getString(R.string.traffic_notification_text_incident);
            TrafficEventNotification.noUpdatedTraffic = resources.getString(R.string.traffic_no_updated);
            TrafficEventNotification.enableInternet = resources.getString(R.string.enable_internet);
            TrafficEventNotification.ahead = resources.getString(R.string.traffic_notification_ahead);
            TrafficEventNotification.dismiss = resources.getString(R.string.traffic_notification_dismiss);
            TrafficEventNotification.unknownIncident = resources.getString(R.string.traffic_notification_text_default);
            final int color = resources.getColor(R.color.glance_dismiss);
            TrafficEventNotification.dismissChoices.add(new ChoiceLayout2.Choice(1, R.drawable.icon_glances_dismiss, color, R.drawable.icon_glances_dismiss, -16777216, TrafficEventNotification.dismiss, color));
            this.notifColor = resources.getColor(R.color.traffic_bad);
        }
        this.bus = bus;
    }
    
    private int getLiveTrafficEventImage(final MapEvents.LiveTrafficEvent liveTrafficEvent) {
        int n;
        if (liveTrafficEvent.type == MapEvents.LiveTrafficEvent.Type.CONGESTION && liveTrafficEvent.severity.value > MapEvents.LiveTrafficEvent.Severity.VERY_HIGH.value) {
            n = R.drawable.icon_mm_stopped_traffic;
        }
        else if (liveTrafficEvent.type == MapEvents.LiveTrafficEvent.Type.INCIDENT) {
            n = R.drawable.icon_mm_incident;
        }
        else {
            n = R.drawable.icon_mm_slow_traffic;
        }
        return n;
    }
    
    private String getLiveTrafficEventText(final MapEvents.LiveTrafficEvent liveTrafficEvent) {
        String s;
        if (liveTrafficEvent.type == MapEvents.LiveTrafficEvent.Type.CONGESTION) {
            if (liveTrafficEvent.severity.value > MapEvents.LiveTrafficEvent.Severity.HIGH.value) {
                s = TrafficEventNotification.stoppedTraffic;
            }
            else {
                s = TrafficEventNotification.slowTraffic;
            }
        }
        else if (liveTrafficEvent.type == MapEvents.LiveTrafficEvent.Type.INCIDENT) {
            s = TrafficEventNotification.trafficIncident;
        }
        else {
            s = TrafficEventNotification.unknownIncident;
        }
        return s;
    }
    
    private void updateState() {
        if (this.trafficEvent != null) {
            this.title.setText((CharSequence)this.getLiveTrafficEventText(this.trafficEvent));
            this.subTitle.setText((CharSequence)TrafficEventNotification.ahead);
            this.image.setImageResource(this.getLiveTrafficEventImage(this.trafficEvent));
            this.choiceLayout.setChoices(TrafficEventNotification.dismissChoices, 0, this.choiceListener, 0.5f);
        }
    }
    
    @Override
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    @Override
    public boolean expandNotification() {
        return false;
    }
    
    @Override
    public int getColor() {
        return this.notifColor;
    }
    
    @Override
    public View getExpandedView(final Context context, final Object o) {
        return null;
    }
    
    @Override
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    @Override
    public String getId() {
        return "navdy#traffic#event#notif";
    }
    
    @Override
    public int getTimeout() {
        return 0;
    }
    
    @Override
    public NotificationType getType() {
        return NotificationType.TRAFFIC_EVENT;
    }
    
    @Override
    public View getView(final Context context) {
        if (this.container == null) {
            this.container = (ViewGroup)LayoutInflater.from(context).inflate(R.layout.notification_traffic_event, (ViewGroup)null);
            this.title = (TextView)this.container.findViewById(R.id.title);
            this.subTitle = (TextView)this.container.findViewById(R.id.subTitle);
            this.image = (ImageView)this.container.findViewById(R.id.image);
            this.choiceLayout = (ChoiceLayout2)this.container.findViewById(R.id.choiceLayout);
        }
        return (View)this.container;
    }
    
    @Override
    public AnimatorSet getViewSwitchAnimation(final boolean b) {
        return null;
    }
    
    @Override
    public boolean isAlive() {
        return true;
    }
    
    @Override
    public boolean isPurgeable() {
        return false;
    }
    
    @Override
    public void onExpandedNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onExpandedNotificationSwitched() {
    }
    
    @Override
    public void onNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onStart(final INotificationController notificationController) {
        super.onStart(notificationController);
        this.updateState();
    }
    
    @Override
    public void onStop() {
        super.onStop();
    }
    
    @Override
    public void onUpdate() {
        this.updateState();
    }
    
    public void setTrafficEvent(final MapEvents.LiveTrafficEvent trafficEvent) {
        this.trafficEvent = trafficEvent;
    }
}
