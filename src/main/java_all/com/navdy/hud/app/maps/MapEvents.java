package com.navdy.hud.app.maps;

import java.util.List;
import com.here.android.mpa.routing.RouteOptions;
import com.navdy.service.library.events.navigation.NavigationRouteResponse;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.hud.app.framework.destinations.Destination;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange;
import com.here.android.mpa.routing.Maneuver;
import java.io.Serializable;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.events.navigation.NavigationTurn;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.service.library.events.navigation.DistanceUnit;
import android.graphics.drawable.Drawable;
import java.util.ArrayList;
import java.util.Date;
import com.here.android.mpa.common.Image;
import com.navdy.service.library.log.Logger;

public class MapEvents
{
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(MapEvents.class);
    }
    
    public static class ArrivalEvent
    {
    }
    
    public enum DestinationDirection
    {
        LEFT, 
        RIGHT, 
        UNKNOWN;
    }
    
    public static class DialMapZoom
    {
        public final Type type;
        
        public DialMapZoom(final Type type) {
            this.type = type;
        }
        
        public enum Type
        {
            IN, 
            OUT;
        }
    }
    
    public static class DisplayJunction
    {
        public Image junction;
        public Image signpost;
        
        public DisplayJunction(final Image junction, final Image signpost) {
            this.junction = junction;
            this.signpost = signpost;
        }
    }
    
    public static class DisplayTrafficIncident
    {
        public static final String ACCIDENT = "ACCIDENT";
        public static final String CLOSURE = "CLOSURE";
        public static final String CONGESTION = "CONGESTION";
        public static final String FLOW = "FLOW";
        public static final String OTHER = "OTHER";
        public static final String ROADWORKS = "ROADWORKS";
        public static final String UNDEFINED = "UNDEFINED";
        public String affectedStreet;
        public Category category;
        public String description;
        public long distanceToIncident;
        public Image icon;
        public Date reported;
        public String title;
        public Type type;
        public Date updated;
        
        public DisplayTrafficIncident(final Type type, final Category category) {
            this.type = type;
            this.category = category;
        }
        
        public DisplayTrafficIncident(final Type type, final Category category, final String title, final String description, final String affectedStreet, final long distanceToIncident, final Date reported, final Date updated, final Image icon) {
            this.type = type;
            this.category = category;
            this.title = title;
            this.description = description;
            this.affectedStreet = affectedStreet;
            this.distanceToIncident = distanceToIncident;
            this.reported = reported;
            this.updated = updated;
            this.icon = icon;
        }
        
        public enum Category
        {
            ACCIDENT, 
            CLOSURE, 
            CONGESTION, 
            FLOW, 
            OTHER, 
            ROADWORKS, 
            UNDEFINED;
        }
        
        public enum Type
        {
            BLOCKING, 
            FAILED, 
            HIGH, 
            INACTIVE, 
            NORMAL, 
            VERY_HIGH;
        }
    }
    
    public static class DisplayTrafficLaneInfo
    {
        public ArrayList<LaneData> laneData;
        
        public DisplayTrafficLaneInfo(final ArrayList<LaneData> laneData) {
            this.laneData = laneData;
        }
    }
    
    public static class GPSSpeedEvent
    {
    }
    
    public static class GpsStatusChange
    {
        public boolean connected;
        
        public GpsStatusChange(final boolean connected) {
            this.connected = connected;
        }
    }
    
    public static class HideSignPostJunction
    {
    }
    
    public static class HideTrafficLaneInfo
    {
    }
    
    public static class LaneData
    {
        public Drawable[] icons;
        public Position position;
        
        public LaneData(final Position position, final Drawable[] icons) {
            this.position = position;
            this.icons = icons;
        }
        
        public enum Position
        {
            OFF_ROUTE, 
            ON_ROUTE;
        }
    }
    
    public static class LiveTrafficDismissEvent
    {
    }
    
    public static class LiveTrafficEvent
    {
        public final Severity severity;
        public final Type type;
        
        public LiveTrafficEvent(final Type type, final Severity severity) {
            this.type = type;
            this.severity = severity;
        }
        
        public enum Severity
        {
            HIGH(0), 
            VERY_HIGH(1);
            
            public final int value;
            
            private Severity(final int value) {
                this.value = value;
            }
        }
        
        public enum Type
        {
            CONGESTION, 
            INCIDENT;
        }
    }
    
    public static class LocationFix
    {
        public boolean locationAvailable;
        public boolean usingLocalGpsLocation;
        public boolean usingPhoneLocation;
        
        public LocationFix(final boolean locationAvailable, final boolean usingPhoneLocation, final boolean usingLocalGpsLocation) {
            this.locationAvailable = locationAvailable;
            this.usingPhoneLocation = usingPhoneLocation;
            this.usingLocalGpsLocation = usingLocalGpsLocation;
        }
    }
    
    public static class ManeuverDisplay
    {
        static final String NOT_AVAILABLE = "N/A";
        public String currentRoad;
        public float currentSpeedLimit;
        public int destinationIconId;
        public DestinationDirection direction;
        public float distance;
        public long distanceInMeters;
        public String distanceToPendingRoadText;
        public DistanceUnit distanceUnit;
        public boolean empty;
        public String eta;
        public String etaAmPm;
        public Date etaDate;
        public String maneuverId;
        public HereManeuverDisplayBuilder.ManeuverState maneuverState;
        public NavigationTurn navigationTurn;
        public int nextTurnIconId;
        public String pendingRoad;
        public String pendingTurn;
        public float totalDistance;
        public float totalDistanceRemaining;
        public DistanceUnit totalDistanceRemainingUnit;
        public DistanceUnit totalDistanceUnit;
        public int turnIconId;
        public int turnIconNowId;
        public int turnIconSoonId;
        
        public ManeuverDisplay() {
            this.turnIconId = -1;
            this.nextTurnIconId = -1;
        }
        
        public boolean isArrived() {
            return this.turnIconId == R.drawable.icon_tbt_arrive;
        }
        
        public boolean isEmpty() {
            return this.empty;
        }
        
        public boolean isNavigating() {
            return this.turnIconId != -1;
        }
        
        @Override
        public String toString() {
            Serializable resourceName;
            Serializable append = resourceName = "N/A";
        Label_0027:
            while (true) {
                if (this.turnIconId == -1) {
                    break Label_0027;
                }
                while (true) {
                    while (true) {
                        try {
                            resourceName = HudApplication.getAppContext().getResources().getResourceName(this.turnIconId);
                            append = new StringBuilder().append("TurnIcon [").append((String)resourceName).append("] ").append("TurnIcon [").append(this.turnIconId).append("] ").append("PendingTurn[").append(this.pendingTurn).append("] ").append("PendingRoad[").append(this.pendingRoad).append("] ").append("Distanceleft[").append(this.distanceToPendingRoadText).append("] ").append("DistanceleftInMeters[").append(this.distanceInMeters).append("] ").append("CurrentRoad[").append(this.currentRoad).append("] ").append("eta[").append(this.eta).append("] ").append("etaUtc[").append(this.etaDate).append("] ").append("currentSpeedLimit[").append(this.currentSpeedLimit).append("] ").append("earlyManeuver[");
                            if (this.nextTurnIconId == -1) {
                                final String s = "not_available] ";
                                return ((StringBuilder)append).append(s).append("destinationDirection[").append(this.direction).append("] ").append("totalDistance [").append(this.totalDistance).append(" ").append(this.totalDistanceUnit).append("] ").append("totalDistanceRemain [").append(this.totalDistanceRemaining).append(" ").append(this.totalDistanceRemainingUnit).append("] ").append("empty[").append(this.empty).append("] ").append("navigating[").append(this.isNavigating()).append("] ").append("arrived [").append(this.isArrived()).append("] ").toString();
                            }
                        }
                        catch (Throwable t) {
                            MapEvents.sLogger.e(t);
                            resourceName = append;
                            continue Label_0027;
                        }
                        final String s = "available] ";
                        continue;
                    }
                }
                break;
            }
        }
    }
    
    public static class ManeuverEvent
    {
        public Maneuver maneuver;
        public Type type;
        
        public ManeuverEvent(final Type type, final Maneuver maneuver) {
            this.type = type;
            this.maneuver = maneuver;
        }
        
        public enum Type
        {
            FIRST, 
            INTERMEDIATE, 
            LAST;
        }
    }
    
    public static class ManeuverSoonEvent
    {
        public final Maneuver maneuver;
        
        public ManeuverSoonEvent(final Maneuver maneuver) {
            this.maneuver = maneuver;
        }
    }
    
    public static class MapEngineInitialize
    {
        public boolean initialized;
        
        public MapEngineInitialize(final boolean initialized) {
            this.initialized = initialized;
        }
    }
    
    public static class MapEngineReady
    {
    }
    
    public static class MapUIReady
    {
    }
    
    public static class NavigationModeChange
    {
        public NavigationMode navigationMode;
        
        public NavigationModeChange(final NavigationMode navigationMode) {
            this.navigationMode = navigationMode;
        }
    }
    
    public static class NewRouteAdded
    {
        public final NavigationSessionRouteChange.RerouteReason rerouteReason;
        
        public NewRouteAdded(final NavigationSessionRouteChange.RerouteReason rerouteReason) {
            this.rerouteReason = rerouteReason;
        }
    }
    
    public static class RegionEvent
    {
        public final String country;
        public final String state;
        
        public RegionEvent(final String state, final String country) {
            this.state = state;
            this.country = country;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = true;
            boolean b2;
            if (o == this) {
                b2 = b;
            }
            else if (!(o instanceof RegionEvent)) {
                b2 = false;
            }
            else {
                final RegionEvent regionEvent = (RegionEvent)o;
                int equals;
                if (this.state == null) {
                    if (regionEvent.state == null) {
                        equals = 1;
                    }
                    else {
                        equals = 0;
                    }
                }
                else {
                    equals = (this.state.equals(regionEvent.state) ? 1 : 0);
                }
                if (equals != 0) {
                    if (this.country == null) {
                        b2 = b;
                        if (regionEvent.country == null) {
                            return b2;
                        }
                    }
                    else if (this.country.equals(regionEvent.country)) {
                        b2 = b;
                        return b2;
                    }
                }
                b2 = false;
            }
            return b2;
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (this.state == null) {
                hashCode = 0;
            }
            else {
                hashCode = this.state.hashCode();
            }
            int hashCode2;
            if (this.country == null) {
                hashCode2 = 0;
            }
            else {
                hashCode2 = this.country.hashCode();
            }
            return (hashCode + 527) * 31 + hashCode2;
        }
        
        @Override
        public String toString() {
            return "RegionEvent: " + this.state + ", " + this.country;
        }
    }
    
    public static class RerouteEvent
    {
        public RouteEventType routeEventType;
        
        public RerouteEvent(final RouteEventType routeEventType) {
            this.routeEventType = routeEventType;
        }
    }
    
    public static class RouteCalculationEvent
    {
        public boolean abortOriginDisplay;
        public Object end;
        public Destination lookupDestination;
        public String pendingNavigationRequestId;
        public int progress;
        public int progressIncrement;
        public NavigationRouteRequest request;
        public NavigationRouteResponse response;
        public RouteOptions routeOptions;
        public Object start;
        public RouteCalculationState state;
        public boolean stopped;
        public List<?> waypoints;
    }
    
    public enum RouteCalculationState
    {
        ABORT_NAVIGATION, 
        FINDING_NAV_COORDINATES, 
        IN_PROGRESS, 
        NONE, 
        STARTED, 
        STOPPED;
    }
    
    public enum RouteEventType
    {
        FAILED, 
        FINISHED, 
        STARTED;
    }
    
    public static class SpeedWarning
    {
        public boolean exceed;
        
        public SpeedWarning(final boolean exceed) {
            this.exceed = exceed;
        }
    }
    
    public static class TrafficDelayDismissEvent
    {
    }
    
    public static class TrafficDelayEvent
    {
        public final long etaDifference;
        
        public TrafficDelayEvent(final long etaDifference) {
            this.etaDifference = etaDifference;
        }
    }
    
    public static class TrafficJamDismissEvent
    {
    }
    
    public static class TrafficJamProgressEvent
    {
        public final int remainingTime;
        
        public TrafficJamProgressEvent(final int remainingTime) {
            this.remainingTime = remainingTime;
        }
    }
    
    public enum TrafficRerouteAction
    {
        DISMISS, 
        REROUTE;
    }
    
    public static class TrafficRerouteDismissEvent
    {
    }
    
    public static class TrafficRerouteEvent
    {
        public String additionalVia;
        public long currentEta;
        public long distanceDifference;
        public long etaDifference;
        public long newEta;
        public String via;
        
        public TrafficRerouteEvent(final String via, final String additionalVia, final long etaDifference, final long currentEta, final long distanceDifference, final long newEta) {
            this.via = via;
            this.additionalVia = additionalVia;
            this.etaDifference = etaDifference;
            this.currentEta = currentEta;
            this.distanceDifference = distanceDifference;
            this.newEta = newEta;
        }
    }
}
