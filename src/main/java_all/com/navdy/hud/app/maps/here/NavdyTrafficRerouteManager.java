package com.navdy.hud.app.maps.here;

import com.navdy.hud.app.util.CrashReporter;
import com.squareup.otto.Subscribe;
import android.os.SystemClock;
import com.here.android.mpa.common.GeoCoordinate;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import java.util.Date;
import com.here.android.mpa.routing.RouteTta;
import java.util.List;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import java.util.ArrayList;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.here.android.mpa.routing.RoutingError;
import com.here.android.mpa.routing.RouteOptions;
import com.navdy.hud.app.framework.network.NetworkBandwidthController;
import com.navdy.hud.app.framework.network.NetworkStateManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.service.library.task.TaskManager;
import android.os.Looper;
import android.os.Handler;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class NavdyTrafficRerouteManager
{
    private static final int KILL_ROUTE_CALC = 45000;
    private static final int STALE_PERIOD = 300000;
    private static final Logger sLogger;
    private final Bus bus;
    private Runnable fasterRouteCheck;
    private Runnable fasterRouteTrigger;
    private Handler handler;
    private final HereNavigationManager hereNavigationManager;
    private final HereTrafficRerouteListener hereTrafficRerouteListener;
    private Runnable killRouteCalc;
    private HereRouteCalculator routeCalculator;
    private volatile boolean running;
    private Runnable staleRoute;
    
    static {
        sLogger = new Logger("NavdyTrafficRerout");
    }
    
    NavdyTrafficRerouteManager(final HereNavigationManager hereNavigationManager, final HereTrafficRerouteListener hereTrafficRerouteListener, final Bus bus) {
        this.routeCalculator = new HereRouteCalculator(NavdyTrafficRerouteManager.sLogger, false);
        this.handler = new Handler(Looper.getMainLooper());
        this.staleRoute = new Runnable() {
            @Override
            public void run() {
                try {
                    NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                }
                catch (Throwable t) {
                    NavdyTrafficRerouteManager.sLogger.e(t);
                }
            }
        };
        this.killRouteCalc = new Runnable() {
            @Override
            public void run() {
                try {
                    NavdyTrafficRerouteManager.sLogger.v("stopping route calc");
                    NavdyTrafficRerouteManager.this.routeCalculator.cancel();
                }
                catch (Throwable t) {
                    NavdyTrafficRerouteManager.sLogger.e(t);
                }
            }
        };
        this.fasterRouteTrigger = new Runnable() {
            @Override
            public void run() {
                TaskManager.getInstance().execute(NavdyTrafficRerouteManager.this.fasterRouteCheck, 2);
            }
        };
        this.fasterRouteCheck = new Runnable() {
            @Override
            public void run() {
                try {
                    NavdyTrafficRerouteManager.this.handler.removeCallbacks(NavdyTrafficRerouteManager.this.killRouteCalc);
                    if (!NetworkStateManager.isConnectedToNetwork(HudApplication.getAppContext())) {
                        NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                        NavdyTrafficRerouteManager.sLogger.v("skip traffic check, no n/w");
                    }
                    else if (!NavdyTrafficRerouteManager.this.hereNavigationManager.isNavigationModeOn()) {
                        NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                        NavdyTrafficRerouteManager.sLogger.v("skip traffic check, not navigating");
                        if (NavdyTrafficRerouteManager.this.running) {
                            NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long)(NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                        }
                    }
                    else if (NavdyTrafficRerouteManager.this.hereNavigationManager.isRerouting()) {
                        NavdyTrafficRerouteManager.sLogger.v("skip traffic check, rerouting");
                        NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                        if (NavdyTrafficRerouteManager.this.running) {
                            NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long)(NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                        }
                    }
                    else if (NavdyTrafficRerouteManager.this.hereNavigationManager.hasArrived()) {
                        NavdyTrafficRerouteManager.sLogger.v("skip traffic check, arrival mode on");
                        NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                        if (NavdyTrafficRerouteManager.this.running) {
                            NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long)(NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                        }
                    }
                    else if (NavdyTrafficRerouteManager.this.hereNavigationManager.isOnGasRoute()) {
                        NavdyTrafficRerouteManager.sLogger.v("skip traffic check, on gas route");
                        NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                        if (NavdyTrafficRerouteManager.this.running) {
                            NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long)(NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                        }
                    }
                    else if (NetworkBandwidthController.getInstance().isLimitBandwidthModeOn() && NavdyTrafficRerouteManager.this.hereNavigationManager.hasTrafficRerouteOnce()) {
                        NavdyTrafficRerouteManager.sLogger.v("user has already selected faster route, no-op in limited b/w condition");
                        if (NavdyTrafficRerouteManager.this.running) {
                            NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long)(NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                        }
                    }
                    else {
                        final Route currentRoute = NavdyTrafficRerouteManager.this.hereNavigationManager.getCurrentRoute();
                        if (currentRoute == null) {
                            NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                            NavdyTrafficRerouteManager.sLogger.v("skip traffic check, no current route");
                            if (NavdyTrafficRerouteManager.this.running) {
                                NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long)(NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                            }
                        }
                        else {
                            final HereMapsManager instance = HereMapsManager.getInstance();
                            final GeoCoordinate lastGeoCoordinate = instance.getLocationFixManager().getLastGeoCoordinate();
                            if (lastGeoCoordinate == null) {
                                NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                NavdyTrafficRerouteManager.sLogger.e("skip traffic check, no start point");
                                if (NavdyTrafficRerouteManager.this.running) {
                                    NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long)(NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                                }
                            }
                            else {
                                final GeoCoordinate destination = currentRoute.getDestination();
                                if (destination == null) {
                                    NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                    NavdyTrafficRerouteManager.sLogger.e("skip traffic check, no end point");
                                    if (NavdyTrafficRerouteManager.this.running) {
                                        NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long)(NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                                    }
                                }
                                else if (!NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.shouldCalculateFasterRoute()) {
                                    NavdyTrafficRerouteManager.sLogger.e("skip traffic check, time not right");
                                    if (NavdyTrafficRerouteManager.this.running) {
                                        NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long)(NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                                    }
                                }
                                else {
                                    final RouteOptions routeOptions = instance.getRouteOptions();
                                    routeOptions.setRouteType(RouteOptions.Type.FASTEST);
                                    NavdyTrafficRerouteManager.this.routeCalculator.cancel();
                                    NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.killRouteCalc, 45000L);
                                    NavdyTrafficRerouteManager.this.routeCalculator.calculateRoute(null, lastGeoCoordinate, null, destination, true, (HereRouteCalculator.RouteCalculatorListener)new HereRouteCalculator.RouteCalculatorListener() {
                                        @Override
                                        public void error(final RoutingError routingError, final Throwable t) {
                                            NavdyTrafficRerouteManager.this.handler.removeCallbacks(NavdyTrafficRerouteManager.this.killRouteCalc);
                                            NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                            if (TTSUtils.isDebugTTSEnabled()) {
                                                String s = "";
                                                if (t != null) {
                                                    s = t.toString();
                                                }
                                                else if (routingError != null) {
                                                    s = routingError.name();
                                                }
                                                TTSUtils.debugShowFasterRouteToast("Faster route not found [Error]", s);
                                            }
                                            NavdyTrafficRerouteManager.sLogger.e("error occured:" + routingError, t);
                                        }
                                        
                                        @Override
                                        public void postSuccess(final ArrayList<NavigationRouteResult> list) {
                                            TaskManager.getInstance().execute(new Runnable() {
                                                @Override
                                                public void run() {
                                                    while (true) {
                                                        Label_0077: {
                                                            try {
                                                                if (!NavdyTrafficRerouteManager.this.running) {
                                                                    NavdyTrafficRerouteManager.sLogger.v("success,route manager stopped");
                                                                }
                                                                else {
                                                                    if (list != null && list.size() != 0) {
                                                                        break Label_0077;
                                                                    }
                                                                    NavdyTrafficRerouteManager.sLogger.v("success, but no result");
                                                                    NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                                                }
                                                                return;
                                                            }
                                                            catch (Throwable t) {
                                                                NavdyTrafficRerouteManager.sLogger.e(t);
                                                                return;
                                                            }
                                                        }
                                                        NavdyTrafficRerouteManager.sLogger.v("success");
                                                        final NavigationRouteResult navigationRouteResult = list.get(0);
                                                        final HereRouteCache instance = HereRouteCache.getInstance();
                                                        final HereRouteCache.RouteInfo route = instance.getRoute(navigationRouteResult.routeId);
                                                        if (route == null) {
                                                            NavdyTrafficRerouteManager.sLogger.v("did not get routeinfo");
                                                            NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                                            return;
                                                        }
                                                        instance.removeRoute(navigationRouteResult.routeId);
                                                        if (navigationRouteResult.duration_traffic == 0) {
                                                            NavdyTrafficRerouteManager.sLogger.v("did not get traffic duration");
                                                            NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                                            return;
                                                        }
                                                        final Route currentRoute = NavdyTrafficRerouteManager.this.hereNavigationManager.getCurrentRoute();
                                                        if (currentRoute == null) {
                                                            NavdyTrafficRerouteManager.sLogger.v("no current route");
                                                            NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                                            return;
                                                        }
                                                        final RouteTta tta = route.route.getTta(Route.TrafficPenaltyMode.OPTIMAL, 268435455);
                                                        final RouteTta tta2 = route.route.getTta(Route.TrafficPenaltyMode.DISABLED, 268435455);
                                                        if (tta != null && tta2 != null) {
                                                            NavdyTrafficRerouteManager.sLogger.v("route duration traffic[" + tta.getDuration() + "] no-traffic[" + tta2.getDuration() + "]");
                                                        }
                                                        final List<Maneuver> maneuvers = route.route.getManeuvers();
                                                        final List<Maneuver> maneuvers2 = currentRoute.getManeuvers();
                                                        final Maneuver nextManeuver = NavdyTrafficRerouteManager.this.hereNavigationManager.getNavController().getNextManeuver();
                                                        final int n = -1;
                                                        final int n2 = 0;
                                                        int n3 = 0;
                                                        int n4 = 0;
                                                        Label_0376: {
                                                            if (nextManeuver == null) {
                                                                NavdyTrafficRerouteManager.sLogger.i("no current maneuver");
                                                                n3 = 0;
                                                                n4 = n2;
                                                            }
                                                            else {
                                                                final int size = maneuvers2.size();
                                                                int n5 = 0;
                                                                int n6;
                                                                while (true) {
                                                                    n6 = n;
                                                                    if (n5 >= size) {
                                                                        break;
                                                                    }
                                                                    if (HereMapUtil.areManeuverEqual(maneuvers2.get(n5), nextManeuver)) {
                                                                        n6 = n5;
                                                                        break;
                                                                    }
                                                                    ++n5;
                                                                }
                                                                if (n6 == -1) {
                                                                    NavdyTrafficRerouteManager.sLogger.i("cannot find current maneuver in route");
                                                                    NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                                                    return;
                                                                }
                                                                final int size2 = maneuvers.size();
                                                                int n7 = 0;
                                                                while (true) {
                                                                    n3 = n6;
                                                                    n4 = n2;
                                                                    if (n7 >= size2) {
                                                                        break Label_0376;
                                                                    }
                                                                    if (HereMapUtil.areManeuverEqual(maneuvers.get(n7), nextManeuver)) {
                                                                        break;
                                                                    }
                                                                    ++n7;
                                                                }
                                                                n3 = n6;
                                                                n4 = n7;
                                                            }
                                                        }
                                                        NavdyTrafficRerouteManager.sLogger.i("firstOff = " + n3 + " secondOff = " + n4);
                                                        final boolean maneuversEqual = HereMapUtil.areManeuversEqual(maneuvers2, n3, maneuvers, n4, new ArrayList<Maneuver>(2));
                                                        final String viaString = HereRouteViaGenerator.getViaString(route.route);
                                                        String via = null;
                                                        if (HereRouteCache.getInstance().getRoute(NavdyTrafficRerouteManager.this.hereNavigationManager.getCurrentRouteId()) != null) {
                                                            via = route.routeResult.via;
                                                        }
                                                        if (maneuversEqual) {
                                                            NavdyTrafficRerouteManager.sLogger.v("fast route same as current route faster-via[" + viaString + "] current-via[" + via + "]");
                                                            if (TTSUtils.isDebugTTSEnabled()) {
                                                                TTSUtils.debugShowFasterRouteToast("Faster route not found", " current route via[" + via + "] is still fastest");
                                                            }
                                                            NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                                            return;
                                                        }
                                                        NavdyTrafficRerouteManager.sLogger.i("fast route is different than current route");
                                                        final String allRoadNames = HereMapUtil.getAllRoadNames(maneuvers2, 0);
                                                        final String allRoadNames2 = HereMapUtil.getAllRoadNames(maneuvers, 0);
                                                        NavdyTrafficRerouteManager.sLogger.v("[RoadNames-current] [offset=" + n3 + "] maneuverCount=" + maneuvers2.size());
                                                        NavdyTrafficRerouteManager.sLogger.v(allRoadNames);
                                                        NavdyTrafficRerouteManager.sLogger.v("[RoadNames-faster] [offset=" + n4 + "] maneuverCount=" + maneuvers.size());
                                                        NavdyTrafficRerouteManager.sLogger.v(allRoadNames2);
                                                        final Route currentProposedRoute = NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.getCurrentProposedRoute();
                                                        if (currentProposedRoute != null) {
                                                            NavdyTrafficRerouteManager.sLogger.i("check if current proposed route is same as new one");
                                                            final List<Maneuver> maneuvers3 = currentProposedRoute.getManeuvers();
                                                            if (HereMapUtil.areManeuversEqual(maneuvers3, 0, maneuvers, 0, null)) {
                                                                final Date eta = NavdyTrafficRerouteManager.this.hereNavigationManager.getNavController().getEta(true, Route.TrafficPenaltyMode.OPTIMAL);
                                                                if (!HereMapUtil.isValidEtaDate(eta)) {
                                                                    NavdyTrafficRerouteManager.sLogger.v("current eta is invalid:" + eta);
                                                                    NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                                                    return;
                                                                }
                                                                final long n8 = (eta.getTime() - System.currentTimeMillis()) / 1000L - tta.getDuration();
                                                                final int rerouteMinDuration = NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.getRerouteMinDuration();
                                                                NavdyTrafficRerouteManager.sLogger.i("current proposed route[" + via + "] is same as new one[" + viaString + "] new-diff[" + n8 + "] old-diff[" + NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.getFasterBy() + "] minDur[" + rerouteMinDuration + "]");
                                                                if (n8 < rerouteMinDuration) {
                                                                    NavdyTrafficRerouteManager.sLogger.i("route has dropped below threshold:" + n8);
                                                                    NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.dismissReroute();
                                                                    return;
                                                                }
                                                                NavdyTrafficRerouteManager.sLogger.i("route still above threshold, update");
                                                            }
                                                            else {
                                                                NavdyTrafficRerouteManager.sLogger.i("current proposed route[" + via + "] is different from new one[" + viaString + "]");
                                                                final String allRoadNames3 = HereMapUtil.getAllRoadNames(maneuvers3, 0);
                                                                final String allRoadNames4 = HereMapUtil.getAllRoadNames(maneuvers, 0);
                                                                NavdyTrafficRerouteManager.sLogger.v("[RoadNames-proposed-current] maneuverCount=" + maneuvers3.size());
                                                                NavdyTrafficRerouteManager.sLogger.v(allRoadNames3);
                                                                NavdyTrafficRerouteManager.sLogger.v("[RoadNames-proposed-faster] maneuverCount=" + maneuvers.size());
                                                                NavdyTrafficRerouteManager.sLogger.v(allRoadNames4);
                                                            }
                                                        }
                                                        NavdyTrafficRerouteManager.sLogger.i("calling here traffic reroute listener");
                                                        NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.handleFasterRoute(route.route, false);
                                                    }
                                                }
                                            }, 2);
                                        }
                                        
                                        @Override
                                        public void preSuccess() {
                                            NavdyTrafficRerouteManager.this.handler.removeCallbacks(NavdyTrafficRerouteManager.this.killRouteCalc);
                                        }
                                        
                                        @Override
                                        public void progress(final int n) {
                                        }
                                    }, 1, routeOptions, true, false, false);
                                    if (NavdyTrafficRerouteManager.this.running) {
                                        NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long)(NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Throwable t) {
                    NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                    NavdyTrafficRerouteManager.sLogger.e(t);
                    if (NavdyTrafficRerouteManager.this.running) {
                        NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long)(NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                    }
                }
                finally {
                    if (NavdyTrafficRerouteManager.this.running) {
                        NavdyTrafficRerouteManager.this.handler.postDelayed(NavdyTrafficRerouteManager.this.fasterRouteTrigger, (long)(NavdyTrafficRerouteManager.this.hereNavigationManager.getRerouteInterval() / 2));
                    }
                }
            }
        };
        NavdyTrafficRerouteManager.sLogger.v("NavdyTrafficRerouteManager:ctor");
        this.hereNavigationManager = hereNavigationManager;
        this.hereTrafficRerouteListener = hereTrafficRerouteListener;
        this.bus = bus;
    }
    
    private void cleanupRouteIfStale() {
        if (this.hereTrafficRerouteListener.getCurrentProposedRoute() != null) {
            final long n = SystemClock.elapsedRealtime() - this.hereTrafficRerouteListener.getCurrentProposedRouteTime();
            if (n > 300000L) {
                NavdyTrafficRerouteManager.sLogger.v("clear stale route:" + n);
                this.hereTrafficRerouteListener.dismissReroute();
            }
            else {
                NavdyTrafficRerouteManager.sLogger.v("route not stale:" + n);
            }
        }
        else {
            NavdyTrafficRerouteManager.sLogger.v("no proposed route");
        }
    }
    
    @Subscribe
    public void onBandwidthSettingChanged(final NetworkBandwidthController.UserBandwidthSettingChanged userBandwidthSettingChanged) {
        this.handler.removeCallbacks(this.fasterRouteTrigger);
        if (this.running) {
            final long n = this.hereNavigationManager.getRerouteInterval() / 2;
            NavdyTrafficRerouteManager.sLogger.v("faster route time changed to " + n);
            this.handler.postDelayed(this.fasterRouteTrigger, n);
        }
    }
    
    void reCalculateCurrentRoute(final Route route, final Route route2, final boolean b, final String s, final long n, final String s2, final long n2, final long n3, final String s3, final RouteTta routeTta) {
        while (true) {
            Label_0133: {
                try {
                    NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute:");
                    this.handler.removeCallbacks(this.killRouteCalc);
                    if (!NetworkStateManager.isConnectedToNetwork(HudApplication.getAppContext())) {
                        this.cleanupRouteIfStale();
                        this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                        NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute:skip no n/w");
                    }
                    else {
                        if (this.hereNavigationManager.isNavigationModeOn()) {
                            break Label_0133;
                        }
                        this.hereTrafficRerouteListener.dismissReroute();
                        this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                        NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: not navigating");
                    }
                    return;
                }
                catch (Throwable t) {
                    NavdyTrafficRerouteManager.sLogger.e(t);
                    this.cleanupRouteIfStale();
                    this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                    CrashReporter.getInstance().reportNonFatalException(t);
                    return;
                }
            }
            if (this.hereNavigationManager.isRerouting()) {
                NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: rerouting");
                this.hereTrafficRerouteListener.dismissReroute();
                this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                return;
            }
            if (this.hereNavigationManager.hasArrived()) {
                NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: arrival mode on");
                this.hereTrafficRerouteListener.dismissReroute();
                this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                return;
            }
            if (this.hereNavigationManager.isOnGasRoute()) {
                NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: on gas route");
                this.hereTrafficRerouteListener.dismissReroute();
                this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                return;
            }
            final HereMapsManager instance = HereMapsManager.getInstance();
            final GeoCoordinate lastGeoCoordinate = instance.getLocationFixManager().getLastGeoCoordinate();
            if (lastGeoCoordinate == null) {
                this.cleanupRouteIfStale();
                this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                NavdyTrafficRerouteManager.sLogger.e("reCalculateCurrentRoute: no start point");
                return;
            }
            final GeoCoordinate destination = route.getDestination();
            if (destination == null) {
                this.cleanupRouteIfStale();
                this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                NavdyTrafficRerouteManager.sLogger.e("reCalculateCurrentRoute: no end point");
                return;
            }
            final RouteOptions routeOptions = instance.getRouteOptions();
            routeOptions.setRouteType(RouteOptions.Type.FASTEST);
            final Maneuver nextManeuver = this.hereNavigationManager.getNavController().getNextManeuver();
            if (nextManeuver == null) {
                this.cleanupRouteIfStale();
                this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                NavdyTrafficRerouteManager.sLogger.i("reCalculateCurrentRoute: no current maneuver");
                return;
            }
            final int n4 = -1;
            final List<Maneuver> maneuvers = route.getManeuvers();
            int n5 = 0;
            int i = n4;
            if (maneuvers != null) {
                final int size = maneuvers.size();
                int n6 = 0;
                while (true) {
                    n5 = size;
                    i = n4;
                    if (n6 >= size) {
                        break;
                    }
                    if (HereMapUtil.areManeuverEqual(maneuvers.get(n6), nextManeuver)) {
                        i = n6;
                        n5 = size;
                        break;
                    }
                    ++n6;
                }
            }
            if (i == -1) {
                NavdyTrafficRerouteManager.sLogger.i("reCalculateCurrentRoute: cannot find current maneuver in route");
                this.cleanupRouteIfStale();
                this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                return;
            }
            final ArrayList<GeoCoordinate> list = new ArrayList<GeoCoordinate>();
            while (i < n5) {
                final GeoCoordinate coordinate = maneuvers.get(i).getCoordinate();
                if (coordinate != null) {
                    NavdyTrafficRerouteManager.sLogger.i("reCalculateCurrentRoute: added waypoint:" + coordinate);
                    list.add(coordinate);
                }
                ++i;
            }
            NavdyTrafficRerouteManager.sLogger.i("reCalculateCurrentRoute: waypoints:" + list.size());
            this.routeCalculator.cancel();
            this.handler.postDelayed(this.killRouteCalc, 45000L);
            this.routeCalculator.calculateRoute(null, lastGeoCoordinate, list, destination, true, (HereRouteCalculator.RouteCalculatorListener)new HereRouteCalculator.RouteCalculatorListener() {
                @Override
                public void error(final RoutingError routingError, final Throwable t) {
                    NavdyTrafficRerouteManager.this.handler.removeCallbacks(NavdyTrafficRerouteManager.this.killRouteCalc);
                    NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                    NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                    NavdyTrafficRerouteManager.sLogger.e("error occured:" + routingError, t);
                }
                
                @Override
                public void postSuccess(final ArrayList<NavigationRouteResult> list) {
                    TaskManager.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            while (true) {
                                Label_0163: {
                                    try {
                                        if (!NavdyTrafficRerouteManager.this.running) {
                                            NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: success,route manager stopped");
                                            NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                            NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                                        }
                                        else {
                                            if (list != null && list.size() != 0) {
                                                break Label_0163;
                                            }
                                            NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: success, but no result");
                                            NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                            NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                                        }
                                        return;
                                    }
                                    catch (Throwable t) {
                                        NavdyTrafficRerouteManager.sLogger.e(t);
                                        return;
                                    }
                                }
                                NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: success");
                                final NavigationRouteResult navigationRouteResult = list.get(0);
                                final HereRouteCache instance = HereRouteCache.getInstance();
                                final HereRouteCache.RouteInfo route = instance.getRoute(navigationRouteResult.routeId);
                                if (route == null) {
                                    NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: did not get routeinfo");
                                    NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                    NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                                    return;
                                }
                                instance.removeRoute(navigationRouteResult.routeId);
                                if (navigationRouteResult.duration_traffic == 0) {
                                    NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: did not get traffic duration");
                                    NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                    NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                                    return;
                                }
                                final RouteTta tta = route.route.getTta(Route.TrafficPenaltyMode.OPTIMAL, 268435455);
                                if (tta == null) {
                                    NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: could not get traffic route tta");
                                    NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                    NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                                    return;
                                }
                                final int duration = tta.getDuration();
                                final int duration2 = routeTta.getDuration();
                                NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: currentDuration=" + duration + " fasterDuration=" + duration2);
                                final long n = duration - duration2;
                                final int rerouteMinDuration = NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.getRerouteMinDuration();
                                if (n >= rerouteMinDuration) {
                                    NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: set faster route, duration:" + n + " >= threshold:" + rerouteMinDuration);
                                    final long currentTimeMillis = System.currentTimeMillis();
                                    final long n2 = currentTimeMillis + duration2 * 1000;
                                    NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: newEta:" + new Date(n2) + " now=" + new Date(currentTimeMillis) + " currentEta=" + new Date(n2));
                                    NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.setFasterRoute(route2, b, s, 0L, s2, 0L, n3, n2);
                                    return;
                                }
                                NavdyTrafficRerouteManager.sLogger.v("reCalculateCurrentRoute: duration:" + n + " < threshold:" + rerouteMinDuration);
                                NavdyTrafficRerouteManager.this.cleanupRouteIfStale();
                                NavdyTrafficRerouteManager.this.hereTrafficRerouteListener.fasterRouteNotFound(b, n, s, s3);
                            }
                        }
                    }, 2);
                }
                
                @Override
                public void preSuccess() {
                    NavdyTrafficRerouteManager.this.handler.removeCallbacks(NavdyTrafficRerouteManager.this.killRouteCalc);
                }
                
                @Override
                public void progress(final int n) {
                }
            }, 1, routeOptions, true, false, false);
        }
    }
    
    public void reset() {
        try {
            NavdyTrafficRerouteManager.sLogger.v("reset::");
            this.routeCalculator.cancel();
            this.hereTrafficRerouteListener.dismissReroute();
            this.handler.removeCallbacks(this.fasterRouteTrigger);
            if (this.running) {
                NavdyTrafficRerouteManager.sLogger.v("reset::timer reset");
                this.handler.postDelayed(this.fasterRouteTrigger, (long)HereTrafficRerouteListener.ROUTE_CHECK_INITIAL_INTERVAL);
            }
        }
        catch (Throwable t) {
            NavdyTrafficRerouteManager.sLogger.e(t);
        }
    }
    
    public void start() {
        if (!this.running) {
            this.running = true;
            this.handler.removeCallbacks(this.staleRoute);
            this.handler.removeCallbacks(this.fasterRouteTrigger);
            this.handler.postDelayed(this.fasterRouteTrigger, (long)HereTrafficRerouteListener.ROUTE_CHECK_INITIAL_INTERVAL);
            this.bus.register(this);
            NavdyTrafficRerouteManager.sLogger.v("running");
        }
    }
    
    public void stop() {
        if (this.running) {
            this.running = false;
            this.handler.removeCallbacks(this.fasterRouteTrigger);
            this.handler.postDelayed(this.staleRoute, 300000L);
            this.bus.unregister(this);
            NavdyTrafficRerouteManager.sLogger.v("stopped");
        }
    }
}
