package com.navdy.hud.app.maps.here;

import com.here.android.mpa.common.ViewRect;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.mapping.PositionIndicator;
import com.here.android.mpa.common.GeoCoordinate;
import java.util.Iterator;
import java.util.List;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.mapping.MapObject;
import android.os.HandlerThread;
import com.navdy.hud.app.util.GenericUtil;
import android.graphics.PointF;
import android.os.Handler;
import com.here.android.mpa.mapping.Map;
import com.navdy.service.library.log.Logger;
import java.util.concurrent.atomic.AtomicInteger;

public class HereMapController
{
    private static final AtomicInteger counter;
    private static final Logger sLogger;
    private final Map map;
    private final Handler mapBkHandler;
    private int mapRouteCount;
    private volatile State state;
    private PointF transformCenter;
    
    static {
        counter = new AtomicInteger(1);
        sLogger = new Logger(HereMapController.class);
    }
    
    public HereMapController(final Map map, final State state) {
        GenericUtil.checkNotOnMainThread();
        this.map = map;
        this.state = state;
        final HandlerThread handlerThread = new HandlerThread("HereMapController-" + HereMapController.counter.getAndIncrement());
        handlerThread.start();
        this.mapBkHandler = new Handler(handlerThread.getLooper());
    }
    
    private void addMapObjectInternal(final MapObject mapObject) {
        if (mapObject instanceof MapRoute) {
            final MapRoute mapRoute = (MapRoute)mapObject;
            final Route route = mapRoute.getRoute();
            ++this.mapRouteCount;
            HereMapController.sLogger.v("[map-route-added] maproute=" + System.identityHashCode(mapRoute) + " route=" + System.identityHashCode(route) + " count=" + this.mapRouteCount);
        }
        this.map.addMapObject(mapObject);
    }
    
    private void removeMapObjectInternal(final MapObject mapObject) {
        if (mapObject instanceof MapRoute) {
            --this.mapRouteCount;
            final MapRoute mapRoute = (MapRoute)mapObject;
            HereMapController.sLogger.v("[map-route-removed] maproute=" + System.identityHashCode(mapRoute) + " route=" + System.identityHashCode(mapRoute.getRoute()) + " count=" + this.mapRouteCount);
        }
        if (mapObject != null) {
            this.map.removeMapObject(mapObject);
        }
    }
    
    public void addMapObject(final MapObject mapObject) {
        this.mapBkHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                HereMapController.this.addMapObjectInternal(mapObject);
            }
        });
    }
    
    public void addMapObjects(final List<MapObject> list) {
        this.mapBkHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                final Iterator<MapObject> iterator = list.iterator();
                while (iterator.hasNext()) {
                    HereMapController.this.addMapObjectInternal(iterator.next());
                }
            }
        });
    }
    
    public void addTransformListener(final Map.OnTransformListener onTransformListener) {
        this.mapBkHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                HereMapController.this.map.addTransformListener(onTransformListener);
            }
        });
    }
    
    public void execute(final Runnable runnable) {
        this.mapBkHandler.post(runnable);
    }
    
    public void execute(final Runnable runnable, final Callback callback) {
        this.mapBkHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                runnable.run();
                if (callback != null) {
                    callback.finish();
                }
            }
        });
    }
    
    public GeoCoordinate getCenter() {
        GenericUtil.checkNotOnMainThread();
        return this.map.getCenter();
    }
    
    public Map getMap() {
        return this.map;
    }
    
    public String getMapScheme() {
        return this.map.getMapScheme();
    }
    
    public float getOrientation() {
        GenericUtil.checkNotOnMainThread();
        return this.map.getOrientation();
    }
    
    public PositionIndicator getPositionIndicator() {
        GenericUtil.checkNotOnMainThread();
        return this.map.getPositionIndicator();
    }
    
    public State getState() {
        return this.state;
    }
    
    public float getTilt() {
        GenericUtil.checkNotOnMainThread();
        return this.map.getTilt();
    }
    
    public double getZoomLevel() {
        GenericUtil.checkNotOnMainThread();
        return this.map.getZoomLevel();
    }
    
    public Map.PixelResult projectToPixel(final GeoCoordinate geoCoordinate) {
        GenericUtil.checkNotOnMainThread();
        return this.map.projectToPixel(geoCoordinate);
    }
    
    public void removeMapObject(final MapObject mapObject) {
        this.mapBkHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                HereMapController.this.removeMapObjectInternal(mapObject);
            }
        });
    }
    
    public void removeMapObjects(final List<MapObject> list) {
        this.mapBkHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                final Iterator<MapObject> iterator = list.iterator();
                while (iterator.hasNext()) {
                    HereMapController.this.removeMapObjectInternal(iterator.next());
                }
            }
        });
    }
    
    public void removeTransformListener(final Map.OnTransformListener onTransformListener) {
        this.mapBkHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                HereMapController.this.map.removeTransformListener(onTransformListener);
            }
        });
    }
    
    public void setCenter(final GeoCoordinate geoCoordinate, final Map.Animation animation, final double n, final float n2, final float n3) {
        if (this.state == State.AR_MODE) {
            this.mapBkHandler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    HereMapController.this.map.setCenter(geoCoordinate, animation, n, n2, n3);
                }
            });
        }
    }
    
    public void setCenterForState(final State state, final GeoCoordinate geoCoordinate, final Map.Animation animation, final double n, final float n2, final float n3) {
        if (this.state == state) {
            this.mapBkHandler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    HereMapController.this.map.setCenter(geoCoordinate, animation, n, n2, n3);
                }
            });
        }
    }
    
    public void setMapScheme(final String s) {
        this.mapBkHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                HereMapController.this.map.setMapScheme(s);
            }
        });
    }
    
    public void setState(final State state) {
        this.state = state;
        HereMapController.sLogger.v("state:" + state);
    }
    
    public void setTilt(final float n) {
        this.mapBkHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                HereMapController.this.map.setTilt(n);
            }
        });
    }
    
    public void setTrafficInfoVisible(final boolean b) {
        this.mapBkHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                HereMapController.this.map.setTrafficInfoVisible(b);
            }
        });
    }
    
    public void setTransformCenter(final PointF transformCenter) {
        this.transformCenter = transformCenter;
        this.mapBkHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                HereMapController.this.map.setTransformCenter(transformCenter);
            }
        });
    }
    
    public void setZoomLevel(final double n) {
        this.mapBkHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                try {
                    HereMapController.this.map.setZoomLevel(n);
                }
                catch (Throwable t) {
                    HereMapController.sLogger.e(t);
                }
            }
        });
    }
    
    public void zoomTo(final GeoBoundingBox geoBoundingBox, final ViewRect viewRect, final Map.Animation animation, final float n) {
        this.mapBkHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                try {
                    HereMapController.this.map.zoomTo(geoBoundingBox, viewRect, animation, n);
                }
                catch (Throwable t) {
                    HereMapController.sLogger.e(t);
                }
            }
        });
    }
    
    public interface Callback
    {
        void finish();
    }
    
    public enum State
    {
        AR_MODE, 
        NONE, 
        OVERVIEW, 
        ROUTE_PICKER, 
        TRANSITION;
    }
}
