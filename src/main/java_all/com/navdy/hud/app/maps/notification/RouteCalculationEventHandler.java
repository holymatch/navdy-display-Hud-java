package com.navdy.hud.app.maps.notification;

import com.here.android.mpa.routing.RouteOptions;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenView;
import com.navdy.hud.app.screen.BaseScreen;
import com.navdy.hud.app.maps.here.HereRouteManager;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.navigation.NavigationRouteResponse;
import com.navdy.service.library.events.navigation.NavigationRouteCancelRequest;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.hud.app.framework.destinations.Destination;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.ui.component.mainmenu.PlacesMenu;
import android.text.TextUtils;
import com.navdy.hud.app.framework.destinations.DestinationsManager;
import android.os.SystemClock;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.maps.NavigationMode;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.framework.notifications.INotification;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.service.library.events.navigation.NavigationSessionRequest;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import android.content.res.Resources;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.framework.notifications.NotificationType;
import android.os.Looper;
import java.util.concurrent.TimeUnit;
import android.os.Handler;
import com.navdy.hud.app.maps.MapEvents;
import com.squareup.otto.Bus;
import com.navdy.hud.app.ui.framework.INotificationAnimationListener;
import com.navdy.service.library.log.Logger;

public class RouteCalculationEventHandler
{
    private static final int MIN_ROUTE_CALC_DISPLAY_TIME;
    private static final int ROUTE_CALC_DELAY_TIME;
    private static final Logger logger;
    private INotificationAnimationListener animationListener;
    private Bus bus;
    private MapEvents.RouteCalculationEvent currentRouteCalcEvent;
    private Handler handler;
    private long routeCalcStartTime;
    private MapEvents.RouteCalculationEvent showRoutesCalcEvent;
    private Runnable startRouteRunnable;
    private final String startTrip;
    
    static {
        logger = new Logger(RouteCalculationEventHandler.class);
        MIN_ROUTE_CALC_DISPLAY_TIME = (int)TimeUnit.SECONDS.toMillis(2L);
        ROUTE_CALC_DELAY_TIME = (int)TimeUnit.SECONDS.toMillis(1L);
    }
    
    public RouteCalculationEventHandler(final Bus bus) {
        this.handler = new Handler(Looper.getMainLooper());
        this.animationListener = new INotificationAnimationListener() {
            @Override
            public void onStart(final String s, final NotificationType notificationType, final UIStateManager.Mode mode) {
                if (mode == UIStateManager.Mode.COLLAPSE && "navdy#route#calc#notif".equals(s)) {
                    final RouteCalculationNotification routeCalculationNotification = (RouteCalculationNotification)NotificationManager.getInstance().getNotification("navdy#route#calc#notif");
                    if (routeCalculationNotification != null) {
                        RouteCalculationEventHandler.logger.v("hideStartTrip");
                        routeCalculationNotification.hideStartTrip();
                    }
                }
            }
            
            @Override
            public void onStop(final String s, final NotificationType notificationType, final UIStateManager.Mode mode) {
                if (mode == UIStateManager.Mode.EXPAND && "navdy#route#calc#notif".equals(s)) {
                    RouteCalculationEventHandler.logger.v("showStartTrip: check");
                    final RouteCalculationNotification routeCalculationNotification = (RouteCalculationNotification)NotificationManager.getInstance().getNotification("navdy#route#calc#notif");
                    if (routeCalculationNotification != null && routeCalculationNotification.isStarting()) {
                        RouteCalculationEventHandler.logger.v("showStartTrip");
                        routeCalculationNotification.showStartTrip();
                    }
                }
            }
        };
        this.startRouteRunnable = new Runnable() {
            @Override
            public void run() {
                RouteCalculationEventHandler.this.startRoute();
            }
        };
        RouteCalculationEventHandler.logger.v("ctor");
        final Resources resources = HudApplication.getAppContext().getResources();
        this.bus = bus;
        this.startTrip = resources.getString(R.string.start_trip);
        RemoteDeviceManager.getInstance().getUiStateManager().addNotificationAnimationListener(this.animationListener);
        bus.register(this);
    }
    
    private void dismissNotification() {
        NotificationManager.getInstance().removeNotification("navdy#route#calc#notif");
    }
    
    private void navigateToRoute(final NavigationRouteResult navigationRouteResult) {
        final NavigationSessionRequest build = new NavigationSessionRequest.Builder().newState(NavigationSessionState.NAV_SESSION_STARTED).label(this.currentRouteCalcEvent.response.label).routeId(navigationRouteResult.routeId).simulationSpeed(0).originDisplay(true).build();
        this.bus.post(build);
        this.bus.post(new RemoteEvent(build));
    }
    
    private void showMoreRoutes() {
        RouteCalculationEventHandler.logger.v("showMoreRoutes");
        if (this.showRoutesCalcEvent != null) {
            final NotificationManager instance = NotificationManager.getInstance();
            RouteCalculationNotification routeCalculationNotification;
            if ((routeCalculationNotification = (RouteCalculationNotification)instance.getNotification("navdy#route#calc#notif")) == null) {
                routeCalculationNotification = new RouteCalculationNotification(this, this.bus);
            }
            routeCalculationNotification.showRoutes(this.showRoutesCalcEvent);
            this.showRoutesCalcEvent = null;
            RouteCalculationEventHandler.logger.v("showMoreRoutes: posted notif");
            instance.addNotification(routeCalculationNotification);
        }
    }
    
    private void startRoute() {
        if (this.currentRouteCalcEvent == null || this.currentRouteCalcEvent.request == null || this.currentRouteCalcEvent.response == null || this.currentRouteCalcEvent.response.results == null) {
            RouteCalculationEventHandler.logger.v("invalid state");
            this.dismissNotification();
        }
        else {
            RouteCalculationEventHandler.logger.v("put user on route");
            this.navigateToRoute(this.currentRouteCalcEvent.response.results.get(0));
            final int size = this.currentRouteCalcEvent.response.results.size();
            if (size > 1) {
                RouteCalculationEventHandler.logger.v("more route available:" + size);
                this.showRoutesCalcEvent = this.currentRouteCalcEvent;
                this.showMoreRoutes();
            }
            else {
                RouteCalculationEventHandler.logger.v("more route NOT available:" + size);
                this.currentRouteCalcEvent = null;
                this.dismissNotification();
            }
        }
    }
    
    public void clearCurrentRouteCalcEvent() {
        this.currentRouteCalcEvent = null;
        this.handler.removeCallbacks(this.startRouteRunnable);
    }
    
    public MapEvents.RouteCalculationEvent getCurrentRouteCalcEvent() {
        return this.currentRouteCalcEvent;
    }
    
    public boolean hasMoreRoutes() {
        return this.showRoutesCalcEvent != null;
    }
    
    @Subscribe
    public void onFirstManeuver(final MapEvents.ManeuverEvent maneuverEvent) {
        RouteCalculationEventHandler.logger.v("onFirstManeuver");
    }
    
    @Subscribe
    public void onNavigationModeChanged(final MapEvents.NavigationModeChange navigationModeChange) {
        final boolean switchingToNewRoute = HereNavigationManager.getInstance().isSwitchingToNewRoute();
        RouteCalculationEventHandler.logger.v("onNavigationModeChanged event[" + navigationModeChange.navigationMode + "] switching=" + switchingToNewRoute);
        if (navigationModeChange.navigationMode == NavigationMode.MAP && !switchingToNewRoute) {
            this.showRoutesCalcEvent = null;
            this.dismissNotification();
        }
    }
    
    @Subscribe
    public void onRouteCalculation(final MapEvents.RouteCalculationEvent currentRouteCalcEvent) {
        RouteCalculationEventHandler.logger.v("onRouteCalculation event[" + currentRouteCalcEvent.state + "]");
        final NotificationManager instance = NotificationManager.getInstance();
        switch (currentRouteCalcEvent.state) {
            case STARTED:
            case FINDING_NAV_COORDINATES: {
                final BaseScreen currentScreen = RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
                if (currentScreen != null && currentScreen.getScreen() == Screen.SCREEN_DESTINATION_PICKER) {
                    RouteCalculationEventHandler.logger.v("onRouteCalculation: destination picker on");
                    instance.enableNotifications(true);
                }
                this.handler.removeCallbacks(this.startRouteRunnable);
                this.showRoutesCalcEvent = null;
                this.currentRouteCalcEvent = currentRouteCalcEvent;
                this.routeCalcStartTime = SystemClock.elapsedRealtime();
                DestinationsManager.getInstance().clearSuggestedDestination();
                final HomeScreenView homescreenView = RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
                if (homescreenView != null) {
                    homescreenView.setShowCollapsedNotification(false);
                }
                RouteCalculationNotification routeCalculationNotification;
                if ((routeCalculationNotification = (RouteCalculationNotification)instance.getNotification("navdy#route#calc#notif")) == null) {
                    routeCalculationNotification = new RouteCalculationNotification(this, this.bus);
                }
                final String s = null;
                String s2 = null;
                String distanceStr = null;
                String s4;
                Destination destination;
                String s5;
                if (currentRouteCalcEvent.state == MapEvents.RouteCalculationState.STARTED) {
                    String s3;
                    if (!TextUtils.isEmpty((CharSequence)currentRouteCalcEvent.request.label)) {
                        s3 = (s4 = currentRouteCalcEvent.request.label);
                        s2 = currentRouteCalcEvent.request.streetAddress;
                    }
                    else if (!TextUtils.isEmpty((CharSequence)currentRouteCalcEvent.request.streetAddress)) {
                        s3 = (s4 = currentRouteCalcEvent.request.streetAddress);
                    }
                    else {
                        s3 = "";
                        s4 = "";
                    }
                    destination = DestinationsManager.getInstance().transformToInternalDestination(currentRouteCalcEvent.request.requestDestination);
                    s5 = s3;
                }
                else {
                    String s7;
                    String s6;
                    if (currentRouteCalcEvent.lookupDestination != null && !TextUtils.isEmpty((CharSequence)currentRouteCalcEvent.lookupDestination.destinationTitle)) {
                        s6 = (s7 = currentRouteCalcEvent.lookupDestination.destinationTitle);
                        s2 = currentRouteCalcEvent.lookupDestination.fullAddress;
                    }
                    else if (currentRouteCalcEvent.lookupDestination != null && !TextUtils.isEmpty((CharSequence)currentRouteCalcEvent.lookupDestination.fullAddress)) {
                        s6 = (s7 = currentRouteCalcEvent.lookupDestination.fullAddress);
                        s2 = s;
                    }
                    else {
                        s6 = "";
                        s7 = "";
                        s2 = s;
                    }
                    destination = currentRouteCalcEvent.lookupDestination;
                    s5 = s6;
                    s4 = s7;
                }
                String initials = null;
                final VerticalList.Model placeModel = PlacesMenu.getPlaceModel(destination, 0, null, null);
                if (placeModel.type == VerticalList.ModelType.TWO_ICONS) {
                    placeModel.iconSelectedColor = 0;
                }
                else if (destination == null) {
                    placeModel.iconSelectedColor = RouteCalculationNotification.notifBkColor;
                }
                else if (destination != null && destination.destinationType != null && destination.destinationType == Destination.DestinationType.DEFAULT && destination.favoriteDestinationType != null && destination.favoriteDestinationType == Destination.FavoriteDestinationType.FAVORITE_NONE) {
                    placeModel.icon = R.drawable.icon_mm_places_2;
                    placeModel.iconSelectedColor = RouteCalculationNotification.notifBkColor;
                }
                final int icon = placeModel.icon;
                final int iconSelectedColor = placeModel.iconSelectedColor;
                int destinationIcon = icon;
                int destinationIconBkColor = iconSelectedColor;
                if (destination != null) {
                    initials = DestinationsManager.getInitials(destination.destinationTitle, destination.favoriteDestinationType);
                    destinationIcon = icon;
                    destinationIconBkColor = iconSelectedColor;
                    if (destination.destinationIcon != 0) {
                        destinationIcon = icon;
                        destinationIconBkColor = iconSelectedColor;
                        if (destination.destinationIconBkColor != 0) {
                            destinationIcon = destination.destinationIcon;
                            destinationIconBkColor = destination.destinationIconBkColor;
                        }
                    }
                    distanceStr = destination.distanceStr;
                }
                final Resources resources = HudApplication.getAppContext().getResources();
                String s8 = null;
                switch (currentRouteCalcEvent.routeOptions.getRouteType()) {
                    case SHORTEST:
                        s8 = resources.getString(R.string.finding_route, new Object[] { RouteCalculationNotification.shortestRoute, s5 });
                        break;
                    case FASTEST:
                        s8 = resources.getString(R.string.finding_route, new Object[] { RouteCalculationNotification.fastestRoute, s5 });
                        break;
                }
                com.navdy.service.library.events.destination.Destination transformToProtoDestination = null;
                if (currentRouteCalcEvent.lookupDestination != null) {
                    transformToProtoDestination = DestinationsManager.getInstance().transformToProtoDestination(currentRouteCalcEvent.lookupDestination);
                }
                String requestId = null;
                if (currentRouteCalcEvent.request != null) {
                    requestId = currentRouteCalcEvent.request.requestId;
                }
                int iconFluctuatorColor;
                final int n = iconFluctuatorColor = 0;
                if (destinationIconBkColor == 0) {
                    iconFluctuatorColor = n;
                    if (placeModel.iconFluctuatorColor != -1) {
                        iconFluctuatorColor = placeModel.iconFluctuatorColor;
                    }
                }
                routeCalculationNotification.showRouteSearch(this.startTrip, s5, destinationIcon, destinationIconBkColor, iconFluctuatorColor, initials, s8, transformToProtoDestination, requestId, s4, s2, distanceStr);
                instance.addNotification(routeCalculationNotification);
                break;
            }
            case STOPPED:
                if (currentRouteCalcEvent.stopped || this.currentRouteCalcEvent == null || this.currentRouteCalcEvent.response == null) {
                    break;
                }
                currentRouteCalcEvent.stopped = true;
                if (this.currentRouteCalcEvent.response.status == RequestStatus.REQUEST_SUCCESS) {
                    final RouteCalculationNotification routeCalculationNotification2 = (RouteCalculationNotification)NotificationManager.getInstance().getNotification("navdy#route#calc#notif");
                    if (routeCalculationNotification2 != null) {
                        RouteCalculationEventHandler.logger.v("hideStartTrip");
                        routeCalculationNotification2.hideStartTrip();
                    }
                    final long n2 = SystemClock.elapsedRealtime() - this.routeCalcStartTime;
                    if (n2 < RouteCalculationEventHandler.MIN_ROUTE_CALC_DISPLAY_TIME) {
                        RouteCalculationEventHandler.logger.v("route calc too fast[" + n2 + "] wait");
                        this.handler.postDelayed(this.startRouteRunnable, (long)RouteCalculationEventHandler.ROUTE_CALC_DELAY_TIME);
                        break;
                    }
                    RouteCalculationEventHandler.logger.v("route calc took[" + n2 + "]");
                    this.startRoute();
                    break;
                }
                else {
                    RouteCalculationEventHandler.logger.e("routing calculation not successful:" + this.currentRouteCalcEvent.response.status);
                    if (this.currentRouteCalcEvent.response.status == RequestStatus.REQUEST_CANCELLED) {
                        RouteCalculationEventHandler.logger.v("request cancelled");
                        this.currentRouteCalcEvent = null;
                        this.dismissNotification();
                        break;
                    }
                    RouteCalculationEventHandler.logger.v("show error");
                    RouteCalculationNotification routeCalculationNotification3;
                    if ((routeCalculationNotification3 = (RouteCalculationNotification)instance.getNotification("navdy#route#calc#notif")) == null) {
                        routeCalculationNotification3 = new RouteCalculationNotification(this, this.bus);
                    }
                    routeCalculationNotification3.showError();
                    instance.addNotification(routeCalculationNotification3);
                    break;
                }
                break;
            case ABORT_NAVIGATION:
                RouteCalculationEventHandler.logger.v("abort-nav routeCalc event[ABORT_NAVIGATION] " + currentRouteCalcEvent.pendingNavigationRequestId);
                if (this.currentRouteCalcEvent == null || this.currentRouteCalcEvent.request == null) {
                    RouteCalculationEventHandler.logger.v("abort-nav:no current route calc event");
                    break;
                }
                if (!TextUtils.equals((CharSequence)this.currentRouteCalcEvent.request.requestId, (CharSequence)currentRouteCalcEvent.pendingNavigationRequestId)) {
                    RouteCalculationEventHandler.logger.v("abort-nav:does not match current request:" + this.currentRouteCalcEvent.request.requestId);
                    break;
                }
                RouteCalculationEventHandler.logger.v("abort-nav: found current route id, cancel");
                this.currentRouteCalcEvent = null;
                if (currentRouteCalcEvent.abortOriginDisplay) {
                    this.bus.post(new RemoteEvent(new NavigationRouteCancelRequest(currentRouteCalcEvent.pendingNavigationRequestId)));
                    this.dismissNotification();
                    break;
                }
                RouteCalculationEventHandler.logger.v("abort-nav: sent navrouteresponse cancel [" + currentRouteCalcEvent.pendingNavigationRequestId + "]");
                this.bus.post(new RemoteEvent(new NavigationRouteResponse.Builder().requestId(currentRouteCalcEvent.pendingNavigationRequestId).status(RequestStatus.REQUEST_CANCELLED).destination(new Coordinate.Builder().latitude(0.0).longitude(0.0).build()).build()));
                HereRouteManager.clearActiveRouteCalc(currentRouteCalcEvent.pendingNavigationRequestId);
                this.dismissNotification();
                break;
        }
    }
}
