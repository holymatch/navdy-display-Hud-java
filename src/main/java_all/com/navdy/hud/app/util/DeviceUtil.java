package com.navdy.hud.app.util;

import android.hardware.Camera;
import java.io.IOException;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import com.navdy.hud.app.storage.PathManager;
import android.os.Build;
import com.navdy.service.library.log.Logger;

public class DeviceUtil
{
    public static final String BUILD_TYPE_USER = "user";
    private static final HereSdkVersion CURRENT_HERE_SDK;
    public static final String TELEMETRY_FILE_NAME = "telemetry";
    private static Boolean hasCamera;
    private static boolean hudDevice;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(DeviceUtil.class);
        CURRENT_HERE_SDK = HereSdkVersion.SDK;
        if (Build.MODEL.equalsIgnoreCase("NAVDY_HUD-MX6DL") || Build.MODEL.equalsIgnoreCase("Display")) {
            DeviceUtil.hudDevice = true;
            DeviceUtil.sLogger.i("Running on Navdy Device:" + Build.MODEL);
        }
        else {
            DeviceUtil.sLogger.i("Not running on Navdy Device:" + Build.MODEL);
        }
    }
    
    public static void copyHEREMapsDataInfo(String string) {
        final File file = new File(PathManager.getInstance().getHereMapsDataDirectory());
        if (!file.exists()) {
            DeviceUtil.sLogger.d("Here maps data directory on the maps partition does not exists");
        }
        else {
            final File file2 = new File(file, "meta.json");
            if (!file2.exists()) {
                DeviceUtil.sLogger.d("Meta json file not found");
            }
            else {
                string = string + File.separator + "HERE_meta.json";
                try {
                    IOUtils.copyFile(file2.getAbsolutePath(), string);
                }
                catch (IOException ex) {
                    DeviceUtil.sLogger.e("Error copying the HERE maps data meta json file");
                }
            }
        }
    }
    
    public static String getCurrentHereSdkTimestamp() {
        return DeviceUtil.CURRENT_HERE_SDK.folderName;
    }
    
    public static String getCurrentHereSdkVersion() {
        return DeviceUtil.CURRENT_HERE_SDK.version;
    }
    
    public static String getHEREMapsDataInfo() {
        // 
        This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_0       
        //     2: new             Ljava/io/File;
        //     5: dup            
        //     6: invokestatic    com/navdy/hud/app/storage/PathManager.getInstance:()Lcom/navdy/hud/app/storage/PathManager;
        //     9: invokevirtual   com/navdy/hud/app/storage/PathManager.getHereMapsDataDirectory:()Ljava/lang/String;
        //    12: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    15: astore_1       
        //    16: aload_1        
        //    17: invokevirtual   java/io/File.exists:()Z
        //    20: ifne            35
        //    23: getstatic       com/navdy/hud/app/util/DeviceUtil.sLogger:Lcom/navdy/service/library/log/Logger;
        //    26: ldc             "Here maps data directory on the maps partition does not exists"
        //    28: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //    31: aload_0        
        //    32: astore_1       
        //    33: aload_1        
        //    34: areturn        
        //    35: new             Ljava/io/File;
        //    38: dup            
        //    39: aload_1        
        //    40: ldc             "meta.json"
        //    42: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //    45: astore_2       
        //    46: aload_2        
        //    47: invokevirtual   java/io/File.exists:()Z
        //    50: ifne            66
        //    53: getstatic       com/navdy/hud/app/util/DeviceUtil.sLogger:Lcom/navdy/service/library/log/Logger;
        //    56: ldc             "Meta json file not found"
        //    58: invokevirtual   com/navdy/service/library/log/Logger.d:(Ljava/lang/String;)V
        //    61: aload_0        
        //    62: astore_1       
        //    63: goto            33
        //    66: aconst_null    
        //    67: astore_3       
        //    68: aconst_null    
        //    69: astore          4
        //    71: aload_3        
        //    72: astore_1       
        //    73: new             Ljava/io/FileInputStream;
        //    76: astore          5
        //    78: aload_3        
        //    79: astore_1       
        //    80: aload           5
        //    82: aload_2        
        //    83: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //    86: aload           5
        //    88: ldc             "UTF-8"
        //    90: invokestatic    com/navdy/service/library/util/IOUtils.convertInputStreamToString:(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
        //    93: astore_3       
        //    94: aload_3        
        //    95: astore_1       
        //    96: aload           5
        //    98: ifnull          33
        //   101: aload           5
        //   103: invokevirtual   java/io/FileInputStream.close:()V
        //   106: aload_3        
        //   107: astore_1       
        //   108: goto            33
        //   111: astore_1       
        //   112: getstatic       com/navdy/hud/app/util/DeviceUtil.sLogger:Lcom/navdy/service/library/log/Logger;
        //   115: ldc             "Error closing the FileInputStream"
        //   117: aload_1        
        //   118: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   121: aload_3        
        //   122: astore_1       
        //   123: goto            33
        //   126: astore_3       
        //   127: aload           4
        //   129: astore          5
        //   131: aload           5
        //   133: astore_1       
        //   134: getstatic       com/navdy/hud/app/util/DeviceUtil.sLogger:Lcom/navdy/service/library/log/Logger;
        //   137: ldc             "Error reading the Meta data,"
        //   139: aload_3        
        //   140: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   143: aload_0        
        //   144: astore_1       
        //   145: aload           5
        //   147: ifnull          33
        //   150: aload           5
        //   152: invokevirtual   java/io/FileInputStream.close:()V
        //   155: aload_0        
        //   156: astore_1       
        //   157: goto            33
        //   160: astore_1       
        //   161: getstatic       com/navdy/hud/app/util/DeviceUtil.sLogger:Lcom/navdy/service/library/log/Logger;
        //   164: ldc             "Error closing the FileInputStream"
        //   166: aload_1        
        //   167: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   170: aload_0        
        //   171: astore_1       
        //   172: goto            33
        //   175: astore          5
        //   177: aload_1        
        //   178: astore_3       
        //   179: aload_3        
        //   180: ifnull          187
        //   183: aload_3        
        //   184: invokevirtual   java/io/FileInputStream.close:()V
        //   187: aload           5
        //   189: athrow         
        //   190: astore_1       
        //   191: getstatic       com/navdy/hud/app/util/DeviceUtil.sLogger:Lcom/navdy/service/library/log/Logger;
        //   194: ldc             "Error closing the FileInputStream"
        //   196: aload_1        
        //   197: invokevirtual   com/navdy/service/library/log/Logger.e:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   200: goto            187
        //   203: astore_1       
        //   204: aload           5
        //   206: astore_3       
        //   207: aload_1        
        //   208: astore          5
        //   210: goto            179
        //   213: astore_3       
        //   214: goto            131
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  73     78     126    131    Ljava/lang/Exception;
        //  73     78     175    179    Any
        //  80     86     126    131    Ljava/lang/Exception;
        //  80     86     175    179    Any
        //  86     94     213    217    Ljava/lang/Exception;
        //  86     94     203    213    Any
        //  101    106    111    126    Ljava/io/IOException;
        //  134    143    175    179    Any
        //  150    155    160    175    Ljava/io/IOException;
        //  183    187    190    203    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.util.ConcurrentModificationException
        //     at java.util.ArrayList$Itr.checkForComodification(Unknown Source)
        //     at java.util.ArrayList$Itr.next(Unknown Source)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:2863)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2445)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
        //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
        //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
        //     at java.lang.Thread.run(Unknown Source)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static boolean isNavdyDevice() {
        return DeviceUtil.hudDevice;
    }
    
    public static boolean isUserBuild() {
        return "user".equals(Build.TYPE);
    }
    
    public static boolean supportsCamera() {
        Label_0068: {
            if (DeviceUtil.hasCamera != null) {
                break Label_0068;
            }
            DeviceUtil.sLogger.v("Found " + Camera.getNumberOfCameras() + " cameras");
            Camera camera = null;
            Camera open = null;
            try {
                final Camera camera2 = camera = (open = Camera.open(0));
                DeviceUtil.hasCamera = true;
                if (camera2 != null) {
                    camera2.release();
                }
                return DeviceUtil.hasCamera;
            }
            catch (Exception ex) {
                camera = open;
                DeviceUtil.sLogger.e("Failed to open camera", ex);
                camera = open;
                DeviceUtil.hasCamera = false;
                if (open != null) {
                    open.release();
                    return DeviceUtil.hasCamera;
                }
                return DeviceUtil.hasCamera;
            }
            finally {
                if (camera != null) {
                    camera.release();
                }
            }
        }
    }
    
    public static void takeDeviceScreenShot(final String s) {
        try {
            Runtime.getRuntime().exec("screencap -p " + s).waitFor();
        }
        catch (IOException ex) {
            DeviceUtil.sLogger.e("Error while taking screen shot", ex);
        }
        catch (InterruptedException ex2) {
            DeviceUtil.sLogger.e("Error while taking screen shot", ex2);
        }
    }
    
    private enum HereSdkVersion
    {
        SDK("3.3.1", "00a584764c918d799f376cc41e46674ba558f8b4");
        
        private String folderName;
        private String version;
        
        private HereSdkVersion(final String version, final String folderName) {
            this.version = version;
            this.folderName = folderName;
        }
    }
}
