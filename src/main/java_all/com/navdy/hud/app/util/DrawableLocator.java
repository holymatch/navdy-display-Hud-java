package com.navdy.hud.app.util;

import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Bitmap;
import android.content.res.TypedArray;
import android.view.View;
import android.graphics.drawable.Drawable;

public class DrawableLocator
{
    private Drawable mDrawable;
    private View mParent;
    private int mResourceId;
    
    public DrawableLocator(final View mParent, final TypedArray typedArray, final int n, final int mResourceId) {
        this.mResourceId = 0;
        this.mParent = mParent;
        if (mParent.isInEditMode()) {
            this.mDrawable = typedArray.getDrawable(n);
        }
        else {
            this.mResourceId = typedArray.getResourceId(n, -1);
            if (this.mResourceId == -1) {
                this.mResourceId = mResourceId;
            }
        }
    }
    
    public static Bitmap drawableToBitmap(final Drawable drawable) {
        Bitmap bitmap;
        if (drawable instanceof BitmapDrawable) {
            bitmap = ((BitmapDrawable)drawable).getBitmap();
        }
        else {
            final Bitmap bitmap2 = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap$Config.ARGB_8888);
            final Canvas canvas = new Canvas(bitmap2);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            bitmap = bitmap2;
        }
        return bitmap;
    }
    
    public Bitmap getBitmap() {
        Bitmap bitmap;
        if (this.mDrawable != null) {
            bitmap = drawableToBitmap(this.mDrawable);
        }
        else if (this.mResourceId == 0) {
            bitmap = null;
        }
        else {
            bitmap = BitmapFactory.decodeResource(this.mParent.getResources(), this.mResourceId);
        }
        return bitmap;
    }
    
    public boolean isSet() {
        return this.mDrawable != null || this.mResourceId != 0;
    }
}
