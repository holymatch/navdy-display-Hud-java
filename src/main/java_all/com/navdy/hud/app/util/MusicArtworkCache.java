package com.navdy.hud.app.util;

import android.database.Cursor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.navdy.service.library.events.audio.MusicCollectionInfo;
import java.io.Closeable;
import com.navdy.service.library.util.IOUtils;
import android.text.TextUtils;
import java.util.ArrayList;
import com.navdy.hud.app.storage.cache.DiskLruCache;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.task.TaskManager;
import android.support.annotation.NonNull;
import android.database.sqlite.SQLiteDatabase;
import com.navdy.hud.app.storage.db.HudDatabase;
import android.content.ContentValues;
import java.util.HashMap;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.events.audio.MusicCollectionType;
import java.util.Map;

public class MusicArtworkCache
{
    private static final String SEPARATOR = "_";
    private static Map<MusicCollectionType, String> collectionTypeMap;
    private static final Logger sLogger;
    private final Object dbLock;
    
    static {
        sLogger = new Logger(MusicArtworkCache.class);
        (MusicArtworkCache.collectionTypeMap = new HashMap<MusicCollectionType, String>()).put(MusicCollectionType.COLLECTION_TYPE_PLAYLISTS, "playlist");
        MusicArtworkCache.collectionTypeMap.put(MusicCollectionType.COLLECTION_TYPE_ARTISTS, "music");
        MusicArtworkCache.collectionTypeMap.put(MusicCollectionType.COLLECTION_TYPE_ALBUMS, "music");
        MusicArtworkCache.collectionTypeMap.put(MusicCollectionType.COLLECTION_TYPE_PODCASTS, "podcast");
        MusicArtworkCache.collectionTypeMap.put(MusicCollectionType.COLLECTION_TYPE_AUDIOBOOKS, "audiobook");
    }
    
    public MusicArtworkCache() {
        this.dbLock = new Object();
    }
    
    private void createDbEntry(final CacheEntryHelper cacheEntryHelper) {
        MusicArtworkCache.sLogger.d("createDbEntry: " + cacheEntryHelper);
        if (cacheEntryHelper.author == null && cacheEntryHelper.album == null && cacheEntryHelper.name == null) {
            MusicArtworkCache.sLogger.w("Not cacheable");
        }
        else {
            final ContentValues contentValues = new ContentValues();
            contentValues.put("type", cacheEntryHelper.type);
            if (cacheEntryHelper.author != null) {
                contentValues.put("author", cacheEntryHelper.author);
            }
            if (cacheEntryHelper.album != null) {
                contentValues.put("album", cacheEntryHelper.album);
            }
            if (cacheEntryHelper.author != null) {
                contentValues.put("name", cacheEntryHelper.name);
            }
            contentValues.put("file_name", cacheEntryHelper.getFileName());
            final Object dbLock = this.dbLock;
            final SQLiteDatabase writableDatabase;
            synchronized (dbLock) {
                writableDatabase = HudDatabase.getInstance().getWritableDatabase();
                if (writableDatabase == null) {
                    MusicArtworkCache.sLogger.e("Couldn't get db");
                    return;
                }
            }
            final ContentValues contentValues2;
            final long insertWithOnConflict = writableDatabase.insertWithOnConflict("music_artwork_cache", (String)null, contentValues2, 4);
            if (insertWithOnConflict == -1L) {
                writableDatabase.update("music_artwork_cache", contentValues2, "rowid = ?", new String[] { String.valueOf(insertWithOnConflict) });
            }
        }
        // monitorexit(o)
    }
    
    private void getArtworkInternal(@NonNull final CacheEntryHelper cacheEntryHelper, @NonNull final Callback callback) {
        TaskManager.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final String access$400 = MusicArtworkCache.this.getFileNameFromDb(cacheEntryHelper);
                if (access$400 == null) {
                    MusicArtworkCache.sLogger.i("No entry for " + cacheEntryHelper);
                    callback.onMiss();
                }
                else {
                    final DiskLruCache diskLruCache = PicassoUtil.getDiskLruCache();
                    if (diskLruCache == null) {
                        MusicArtworkCache.sLogger.e("No disk cache");
                        callback.onMiss();
                    }
                    else {
                        final byte[] value = diskLruCache.get(access$400);
                        if (value != null) {
                            callback.onHit(value);
                        }
                        else {
                            callback.onMiss();
                        }
                    }
                }
            }
        }, 22);
    }
    
    private String getFileNameFromDb(@NonNull CacheEntryHelper name) {
        MusicArtworkCache.sLogger.d("getFileNameFromDb: " + name);
        String string = null;
        final CacheEntryHelper cacheEntryHelper = null;
        Object o = string;
        try {
            Object o2 = new(java.util.ArrayList.class);
            o = string;
            new ArrayList();
            o = string;
            o = string;
            final StringBuilder sb = new StringBuilder("type = ?");
            o = string;
            ((ArrayList<String>)o2).add(name.type);
            o = string;
            if (name.author != null) {
                o = string;
                sb.append(" AND author = ?");
                o = string;
                ((ArrayList<String>)o2).add(name.author);
            }
            o = string;
            if (name.album != null) {
                o = string;
                sb.append(" AND album = ?");
                o = string;
                ((ArrayList<String>)o2).add(name.album);
            }
            o = string;
            Label_0274: {
                Label_0218: {
                    if (TextUtils.equals((CharSequence)name.type, (CharSequence)"music")) {
                        o = string;
                        if (name.name == null) {
                            break Label_0218;
                        }
                        o = string;
                        if (name.album != null) {
                            break Label_0218;
                        }
                    }
                    o = string;
                    sb.append(" AND name = ?");
                    o = string;
                    if (name.name == null) {
                        break Label_0274;
                    }
                    o = string;
                    name = (CacheEntryHelper)name.name;
                    o = string;
                    ((ArrayList<String>)o2).add((String)name);
                }
                o = string;
                final Object dbLock = this.dbLock;
                o = string;
                name = cacheEntryHelper;
                try {
                    o = HudDatabase.getInstance().getWritableDatabase();
                    if (o == null) {
                        name = cacheEntryHelper;
                        MusicArtworkCache.sLogger.e("Couldn't get db");
                        name = cacheEntryHelper;
                        // monitorexit(o3)
                        IOUtils.closeObject(null);
                        name = null;
                    }
                    else {
                        name = cacheEntryHelper;
                        string = sb.toString();
                        name = cacheEntryHelper;
                        o2 = ((ArrayList<String>)o2).<String>toArray(new String[((ArrayList)o2).size()]);
                        name = cacheEntryHelper;
                        o = ((SQLiteDatabase)o).query("music_artwork_cache", new String[] { "file_name" }, string, (String[])o2, (String)null, (String)null, (String)null, "1");
                        if (o != null) {
                            name = (CacheEntryHelper)o;
                            if (((Cursor)o).moveToFirst()) {
                                name = (CacheEntryHelper)o;
                                final Object string2 = ((Cursor)o).getString(((Cursor)o).getColumnIndex("file_name"));
                                name = (CacheEntryHelper)o;
                                // monitorexit(o3)
                                IOUtils.closeObject((Closeable)o);
                                name = (CacheEntryHelper)string2;
                                return (String)name;
                            }
                        }
                        name = (CacheEntryHelper)o;
                        // monitorexit(o3)
                        IOUtils.closeObject((Closeable)o);
                        name = null;
                    }
                    return (String)name;
                    name = (CacheEntryHelper)"";
                }
                finally {
                    // monitorexit(o3)
                    o = name;
                }
            }
        }
        // monitorenter(o3 = dbLock)
        finally {
            IOUtils.closeObject((Closeable)o);
        }
    }
    
    private void putArtworkInternal(final CacheEntryHelper cacheEntryHelper, final byte[] array) {
        if (cacheEntryHelper.getFileName() == null) {
            MusicArtworkCache.sLogger.e("Couldn't generate a filename for: " + cacheEntryHelper);
        }
        else {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    final DiskLruCache diskLruCache = PicassoUtil.getDiskLruCache();
                    if (diskLruCache == null) {
                        MusicArtworkCache.sLogger.e("No disk cache");
                    }
                    else {
                        diskLruCache.put(cacheEntryHelper.getFileName(), array);
                        MusicArtworkCache.this.createDbEntry(cacheEntryHelper);
                    }
                }
            }, 22);
        }
    }
    
    public void getArtwork(@NonNull final MusicCollectionInfo musicCollectionInfo, @NonNull final Callback callback) {
        this.getArtworkInternal(new CacheEntryHelper(musicCollectionInfo), callback);
    }
    
    public void getArtwork(@NonNull final String s, final String s2, final String s3, final Callback callback) {
        this.getArtworkInternal(new CacheEntryHelper(s, s2, s3), callback);
    }
    
    public void putArtwork(@NonNull final MusicCollectionInfo musicCollectionInfo, final byte[] array) {
        this.putArtworkInternal(new CacheEntryHelper(musicCollectionInfo), array);
    }
    
    public void putArtwork(final String s, final String s2, final String s3, final byte[] array) {
        this.putArtworkInternal(new CacheEntryHelper(s, s2, s3), array);
    }
    
    private class CacheEntryHelper
    {
        String album;
        String author;
        String fileName;
        String name;
        @NonNull
        String type;
        
        CacheEntryHelper(final MusicCollectionInfo musicCollectionInfo) {
            this.type = MusicArtworkCache.collectionTypeMap.get(musicCollectionInfo.collectionType);
            switch (musicCollectionInfo.collectionType) {
                default:
                    this.name = musicCollectionInfo.name;
                case COLLECTION_TYPE_PODCASTS:
                    this.album = musicCollectionInfo.name;
                case COLLECTION_TYPE_ALBUMS:
                    this.album = musicCollectionInfo.name;
                    this.author = musicCollectionInfo.subtitle;
                case COLLECTION_TYPE_ARTISTS:
                    this.author = musicCollectionInfo.name;
                case COLLECTION_TYPE_PLAYLISTS:
                    this.name = musicCollectionInfo.name;
                case COLLECTION_TYPE_UNKNOWN:
                    MusicArtworkCache.sLogger.e("Unknown type");
                case COLLECTION_TYPE_AUDIOBOOKS:
            }
        }
        
        CacheEntryHelper(final String author, final String album, final String name) {
            this.type = "music";
            this.author = author;
            this.album = album;
            this.name = name;
        }
        
        private String formatIdentifier(final CharSequence charSequence) {
            final Matcher matcher = Pattern.compile("[^A-Za-z0-9_]").matcher(charSequence);
            String replaceAll;
            if (!TextUtils.isEmpty(charSequence)) {
                replaceAll = matcher.replaceAll("_");
            }
            else {
                replaceAll = null;
            }
            return replaceAll;
        }
        
        private String getFileName() {
            if (this.fileName == null) {
                final StringBuilder sb = new StringBuilder(this.type);
                if (!TextUtils.isEmpty((CharSequence)this.author)) {
                    sb.append("_");
                    sb.append(this.author);
                }
                if (!TextUtils.isEmpty((CharSequence)this.album)) {
                    sb.append("_");
                    sb.append(this.album);
                }
                if (!TextUtils.isEmpty((CharSequence)this.name)) {
                    sb.append("_");
                    sb.append(this.name);
                }
                this.fileName = this.formatIdentifier(sb);
            }
            return this.fileName;
        }
        
        @Override
        public String toString() {
            return this.type + ", " + this.author + " - " + this.album + " - " + this.name;
        }
    }
    
    public interface Callback
    {
        void onHit(@NonNull final byte[] p0);
        
        void onMiss();
    }
}
