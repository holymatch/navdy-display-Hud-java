package com.navdy.hud.app.util;

public class ConversionUtil
{
    public static final int MAX_KMPL = 45;
    public static final int MAX_MPG = 100;
    
    public static double convertLpHundredKmToKMPL(final double n) {
        double min = 0.0;
        if (n > 0.0) {
            min = Math.min(100.0 / n, 45.0);
        }
        return min;
    }
    
    public static double convertLpHundredKmToMPG(final double n) {
        double min = 0.0;
        if (n > 0.0) {
            min = Math.min(235.214 / n, 100.0);
        }
        return min;
    }
}
