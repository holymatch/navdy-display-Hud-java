package com.navdy.hud.app.framework.calendar;

import com.navdy.hud.app.event.DriverProfileChanged;
import com.squareup.otto.Subscribe;
import com.navdy.hud.app.profile.DriverProfile;
import java.util.Collection;
import com.navdy.hud.app.framework.DriverProfileHelper;
import java.util.ArrayList;
import com.navdy.service.library.events.calendars.CalendarEventUpdates;
import com.navdy.service.library.events.calendars.CalendarEvent;
import java.util.List;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;

public class CalendarManager
{
    private static final int CALENDAR_EVENTS_LIST_INITIAL_SIZE = 20;
    private static final Logger sLogger;
    public Bus mBus;
    private List<CalendarEvent> mCalendarEvents;
    private CalendarEventUpdates mLastCalendarEventUpdate;
    private String profileName;
    
    static {
        sLogger = new Logger(CalendarManager.class);
    }
    
    public CalendarManager(final Bus mBus) {
        (this.mBus = mBus).register(this);
        this.mCalendarEvents = new ArrayList<CalendarEvent>(20);
    }
    
    public List<CalendarEvent> getCalendarEvents() {
        return this.mCalendarEvents;
    }
    
    @Subscribe
    public void onCalendarEventsUpdate(final CalendarEventUpdates mLastCalendarEventUpdate) {
        CalendarManager.sLogger.d("Received Calendar updates :" + mLastCalendarEventUpdate);
        this.mLastCalendarEventUpdate = mLastCalendarEventUpdate;
        final DriverProfile currentProfile = DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        if (currentProfile != null) {
            this.profileName = currentProfile.getProfileName();
        }
        this.mCalendarEvents.clear();
        if (mLastCalendarEventUpdate != null && mLastCalendarEventUpdate.calendar_events != null) {
            CalendarManager.sLogger.d("Calendar events count :" + mLastCalendarEventUpdate.calendar_events.size());
            this.mCalendarEvents.addAll(mLastCalendarEventUpdate.calendar_events);
        }
        this.mBus.post(CalendarManagerEvent.UPDATED);
    }
    
    @Subscribe
    public void onDriverProfileChanged(final DriverProfileChanged driverProfileChanged) {
        if (!DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile().isDefaultProfile()) {
            this.mCalendarEvents.clear();
            this.mBus.post(CalendarManagerEvent.UPDATED);
        }
        else {
            CalendarManager.sLogger.d("Default profile loaded, not clearing the calendar events");
        }
    }
    
    public enum CalendarManagerEvent
    {
        UPDATED;
    }
}
