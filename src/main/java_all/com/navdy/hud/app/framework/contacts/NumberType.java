package com.navdy.hud.app.framework.contacts;

public enum NumberType
{
    HOME(1), 
    MOBILE(2), 
    OTHER(5), 
    WORK(3), 
    WORK_MOBILE(4);
    
    int value;
    
    private NumberType(final int value) {
        this.value = value;
    }
    
    public static NumberType buildFromValue(final int n) {
        NumberType numberType = null;
        switch (n) {
            default:
                numberType = NumberType.OTHER;
                break;
            case 1:
                numberType = NumberType.HOME;
                break;
            case 2:
                numberType = NumberType.MOBILE;
                break;
            case 3:
                numberType = NumberType.WORK;
                break;
            case 4:
                numberType = NumberType.WORK_MOBILE;
                break;
        }
        return numberType;
    }
    
    public int getValue() {
        return this.value;
    }
}
