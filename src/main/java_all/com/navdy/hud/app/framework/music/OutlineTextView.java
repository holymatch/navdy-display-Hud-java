package com.navdy.hud.app.framework.music;

import android.text.TextPaint;
import android.graphics.Paint;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.content.Context;
import java.lang.reflect.Field;
import com.navdy.service.library.log.Logger;
import android.widget.TextView;

public class OutlineTextView extends TextView
{
    private static final Logger sLogger;
    Field currentTextColorField;
    
    static {
        sLogger = new Logger(OutlineTextView.class);
    }
    
    public OutlineTextView(final Context context) {
        this(context, null);
    }
    
    public OutlineTextView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public OutlineTextView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        try {
            (this.currentTextColorField = TextView.class.getDeclaredField("mCurTextColor")).setAccessible(true);
        }
        catch (Throwable t) {
            OutlineTextView.sLogger.e(t);
        }
    }
    
    private void setCurrentTextColor(final Integer n) {
        try {
            if (this.currentTextColorField != null) {
                this.currentTextColorField.set(this, n);
            }
        }
        catch (Throwable t) {
            OutlineTextView.sLogger.e(t);
        }
    }
    
    public void onDraw(final Canvas canvas) {
        final int currentTextColor = this.getCurrentTextColor();
        final TextPaint paint = this.getPaint();
        paint.setStyle(Paint$Style.STROKE);
        paint.setStrokeJoin(Paint$Join.MITER);
        paint.setStrokeMiter(1.0f);
        this.setCurrentTextColor(-16777216);
        paint.setStrokeWidth(4.0f);
        super.onDraw(canvas);
        paint.setStyle(Paint$Style.FILL);
        this.setCurrentTextColor(currentTextColor);
        super.onDraw(canvas);
    }
}
