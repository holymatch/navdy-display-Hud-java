package com.navdy.hud.app.view;

import android.view.View;
import butterknife.ButterKnife;
import android.os.Bundle;
import com.squareup.otto.Subscribe;
import android.graphics.drawable.Drawable;
import android.content.Context;
import com.navdy.hud.app.manager.SpeedManager;
import android.widget.TextView;
import butterknife.InjectView;

public class SpeedLimitSignPresenter extends DashboardWidgetPresenter
{
    private int speedLimit;
    @InjectView(R.id.speed_limit_sign)
    protected SpeedLimitSignView speedLimitSignView;
    private String speedLimitSignWidgetName;
    @InjectView(R.id.txt_speed_limit_unavailable)
    protected TextView speedLimitUnavailableText;
    private SpeedManager speedManager;
    
    public SpeedLimitSignPresenter(final Context context) {
        this.speedManager = SpeedManager.getInstance();
        this.speedLimitSignWidgetName = context.getResources().getString(R.string.widget_speed_limit);
    }
    
    @Override
    public Drawable getDrawable() {
        return null;
    }
    
    @Override
    public String getWidgetIdentifier() {
        return "SPEED_LIMIT_SIGN_GAUGE_ID";
    }
    
    @Override
    public String getWidgetName() {
        return this.speedLimitSignWidgetName;
    }
    
    @Override
    protected boolean isRegisteringToBusRequired() {
        return true;
    }
    
    @Subscribe
    public void onSpeedUnitChanged(final SpeedManager.SpeedUnitChanged speedUnitChanged) {
        this.reDraw();
    }
    
    public void setSpeedLimit(final int speedLimit) {
        this.speedLimit = speedLimit;
        this.reDraw();
    }
    
    @Override
    public void setView(final DashboardWidgetView dashboardWidgetView, final Bundle bundle) {
        if (dashboardWidgetView != null) {
            dashboardWidgetView.setContentView(R.layout.speed_limit_sign_gauge);
            ButterKnife.inject(this, (View)dashboardWidgetView);
        }
        super.setView(dashboardWidgetView, bundle);
    }
    
    @Override
    protected void updateGauge() {
        if (this.speedLimit > 0) {
            this.speedLimitUnavailableText.setVisibility(GONE);
            this.speedLimitSignView.setVisibility(View.VISIBLE);
            this.speedLimitSignView.setSpeedLimitUnit(this.speedManager.getSpeedUnit());
            this.speedLimitSignView.setSpeedLimit(this.speedLimit);
        }
        else {
            this.speedLimitUnavailableText.setVisibility(View.VISIBLE);
            this.speedLimitSignView.setVisibility(GONE);
        }
    }
}
