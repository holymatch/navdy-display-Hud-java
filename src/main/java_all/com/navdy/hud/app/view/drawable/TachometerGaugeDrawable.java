package com.navdy.hud.app.view.drawable;

import android.graphics.Typeface;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Rect;
import android.graphics.Canvas;
import android.content.Context;
import android.graphics.drawable.Drawable;

public class TachometerGaugeDrawable extends GaugeDrawable
{
    private static final float GAUGE_SLICES_START_ANGLE = 146.8f;
    private static final float GAUGE_SLICE_ANGLE = 30.8f;
    private static final float GAUGE_START_ANGLE = 146.8f;
    private static final int GROOVES = 8;
    private static final float LAST_MAX_MARKER_SWEEP_ANGLE = 2.0f;
    private static final float TOTAL_SWEEP_ANGLE = 246.4f;
    private int fullModeGaugeBackgroundBottomOffset;
    private int fullModeGaugeInternalMarginBottom;
    private int fullModeGaugeInternalMarginLeft;
    private int fullModeGaugeInternalMarginRight;
    private int fullModeGaugeInternalMarginTop;
    private float gaugeBackgroundBorderWidth;
    private int gaugeBackgroundBottomOffset;
    private int gaugeInternalMarginBottom;
    private int gaugeInternalMarginLeft;
    private int gaugeInternalMarginRight;
    private int gaugeInternalMarginTop;
    private int grooveTextOffset;
    private float grooveTextSize;
    private int maxMarkerAlpha;
    private int maxMarkerColor;
    private int maxMarkerValue;
    private int regularColor;
    private int rpmWarningLevel;
    Drawable tachoMeterBackgroundDrawable;
    private int valueRingWidth;
    private int warningColor;
    
    public TachometerGaugeDrawable(final Context context, final int n) {
        super(context, n);
        this.maxMarkerAlpha = 0;
        this.fullModeGaugeInternalMarginTop = context.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_margin_top);
        this.fullModeGaugeInternalMarginLeft = context.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_margin_left);
        this.fullModeGaugeInternalMarginBottom = context.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_margin_bottom);
        this.fullModeGaugeInternalMarginRight = context.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_margin_right);
        this.fullModeGaugeBackgroundBottomOffset = context.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_background_bottom_offset);
        this.gaugeBackgroundBorderWidth = context.getResources().getDimension(R.dimen.tachometer_gauge_background_border_width);
        this.regularColor = context.getResources().getColor(R.color.tachometer_color);
        this.warningColor = context.getResources().getColor(R.color.tachometer_warning_color);
        this.maxMarkerColor = context.getResources().getColor(R.color.cyan);
        this.valueRingWidth = context.getResources().getDimensionPixelSize(R.dimen.tachometer_guage_inner_bar_width);
        this.tachoMeterBackgroundDrawable = context.getResources().getDrawable(R.drawable.asset_tachometer_background);
        this.grooveTextSize = context.getResources().getDimensionPixelSize(R.dimen.tachometer_gauge_groove_text_size);
        this.grooveTextOffset = context.getResources().getDimensionPixelSize(R.dimen.tachometer_gauge_groove_text_offset);
        this.gaugeInternalMarginTop = this.fullModeGaugeInternalMarginTop;
        this.gaugeInternalMarginLeft = this.fullModeGaugeInternalMarginLeft;
        this.gaugeInternalMarginBottom = this.fullModeGaugeInternalMarginBottom;
        this.gaugeInternalMarginRight = this.fullModeGaugeInternalMarginRight;
        this.gaugeBackgroundBottomOffset = this.fullModeGaugeBackgroundBottomOffset;
    }
    
    public TachometerGaugeDrawable(final Context context, final int n, final int n2) {
        super(context, n, n2);
        this.maxMarkerAlpha = 0;
    }
    
    @Override
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        final Rect bounds = this.getBounds();
        final Rect rect = new Rect(bounds.left + this.gaugeInternalMarginLeft, bounds.top + this.gaugeInternalMarginTop, bounds.right - this.gaugeInternalMarginRight, bounds.bottom - this.gaugeInternalMarginBottom);
        this.tachoMeterBackgroundDrawable.setBounds(new Rect(rect.left, rect.top, rect.right, rect.bottom - this.gaugeBackgroundBottomOffset));
        this.tachoMeterBackgroundDrawable.draw(canvas);
        final float n = this.valueRingWidth / 2 + this.gaugeBackgroundBorderWidth;
        final RectF rectF = new RectF(rect.left + n, rect.top + n, rect.right - n, rect.bottom - n);
        final float n2 = this.mValue / this.mMaxValue;
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStrokeWidth((float)this.valueRingWidth);
        this.mPaint.setStyle(Paint$Style.STROKE);
        int n3;
        if (this.mValue >= this.rpmWarningLevel) {
            n3 = this.warningColor;
        }
        else {
            n3 = this.regularColor;
        }
        this.mPaint.setColor(n3);
        canvas.drawArc(rectF, 146.8f, n2 * 246.4f, false, this.mPaint);
        final int n4 = rect.width() / 2 + this.grooveTextOffset;
        this.mPaint.setStyle(Paint$Style.FILL);
        this.mPaint.setColor(n3);
        this.mPaint.setTextSize(this.grooveTextSize);
        this.mPaint.setTypeface(Typeface.create(Typeface.DEFAULT, 1));
        int i = (int)Math.floor(this.mValue / (this.mMaxValue / 8.0f));
        int n5;
        if (i < 8) {
            n5 = i + 2;
        }
        else {
            n5 = i + 1;
        }
        while (i < n5) {
            final float n6 = 146.8f + i * 30.8f;
            final String string = Integer.toString(i);
            final Rect rect2 = new Rect();
            this.mPaint.getTextBounds(string, 0, string.length(), rect2);
            float n7 = 0.0f;
            float n8 = 0.0f;
            if (i == 0 || i == 8) {
                n7 = rect2.height();
                if (i == 0) {
                    n8 = rect2.width();
                }
                else {
                    n8 = -rect2.width();
                }
            }
            canvas.drawText(Integer.toString(i), (float)((int)(rect.left - this.grooveTextOffset + n4 + n4 * Math.cos(Math.toRadians(n6)) - n8) - rect2.width() / 2), (float)(rect2.height() / 2 + (int)(rect.top - this.grooveTextOffset + n4 + n4 * Math.sin(Math.toRadians(n6)) - n7)), this.mPaint);
            ++i;
        }
        if (this.maxMarkerValue > this.mValue) {
            final int alpha = this.mPaint.getAlpha();
            this.mPaint.setColor(this.maxMarkerColor);
            this.mPaint.setAlpha(this.maxMarkerAlpha);
            this.mPaint.setStrokeWidth((float)this.valueRingWidth);
            this.mPaint.setStyle(Paint$Style.STROKE);
            canvas.drawArc(rectF, 146.8f + this.maxMarkerValue / this.mMaxValue * 246.4f - 2.0f, 2.0f, false, this.mPaint);
            this.mPaint.setAlpha(alpha);
        }
    }
    
    public int getMaxMarkerAlpha() {
        return this.maxMarkerAlpha;
    }
    
    public int getMaxMarkerValue() {
        return this.maxMarkerValue;
    }
    
    public void setMaxMarkerAlpha(final int maxMarkerAlpha) {
        this.maxMarkerAlpha = maxMarkerAlpha;
    }
    
    public void setMaxMarkerValue(final int maxMarkerValue) {
        this.maxMarkerValue = maxMarkerValue;
    }
    
    public void setRPMWarningLevel(final int rpmWarningLevel) {
        this.rpmWarningLevel = rpmWarningLevel;
    }
}
