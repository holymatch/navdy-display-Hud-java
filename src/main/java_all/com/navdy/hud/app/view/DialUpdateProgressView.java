package com.navdy.hud.app.view;

import com.navdy.service.library.events.input.GestureEvent;
import android.view.View;
import butterknife.ButterKnife;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import com.navdy.hud.app.framework.toast.ToastManager;
import mortar.Mortar;
import com.navdy.hud.app.device.dial.DialFirmwareUpdater;
import android.util.AttributeSet;
import android.content.Context;
import butterknife.InjectView;
import android.widget.ProgressBar;
import javax.inject.Inject;
import com.navdy.hud.app.screen.DialUpdateProgressScreen;
import android.os.Handler;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import android.widget.RelativeLayout;

public class DialUpdateProgressView extends RelativeLayout implements IInputHandler
{
    private static final long UPDATE_TIMEOUT = 480000L;
    private static final Logger sLogger;
    private Handler handler;
    @Inject
    DialUpdateProgressScreen.Presenter mPresenter;
    @InjectView(R.id.progress)
    ProgressBar mProgress;
    private Runnable timeout;
    
    static {
        sLogger = new Logger(DialUpdateProgressView.class);
    }
    
    public DialUpdateProgressView(final Context context) {
        super(context, (AttributeSet)null);
        this.handler = new Handler();
        this.timeout = new Runnable() {
            @Override
            public void run() {
                DialUpdateProgressView.sLogger.v("timedout");
                DialUpdateProgressView.this.mPresenter.finish(DialFirmwareUpdater.Error.TIMEDOUT);
            }
        };
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public DialUpdateProgressView(final Context context, final AttributeSet set) {
        super(context, set);
        this.handler = new Handler();
        this.timeout = new Runnable() {
            @Override
            public void run() {
                DialUpdateProgressView.sLogger.v("timedout");
                DialUpdateProgressView.this.mPresenter.finish(DialFirmwareUpdater.Error.TIMEDOUT);
            }
        };
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ToastManager.getInstance().disableToasts(false);
        NotificationManager.getInstance().enableNotifications(true);
        if (this.mPresenter != null) {
            this.handler.removeCallbacks(this.timeout);
            this.mPresenter.dropView(this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        if (this.mPresenter != null) {
            this.mPresenter.takeView(this);
            NotificationManager.getInstance().enableNotifications(false);
            ToastManager.getInstance().disableToasts(true);
            this.mPresenter.startUpdate();
            this.handler.removeCallbacks(this.timeout);
            this.handler.postDelayed(this.timeout, 480000L);
        }
        this.setProgress(0);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        return true;
    }
    
    public void setProgress(final int progress) {
        this.mProgress.setProgress(progress);
    }
}
