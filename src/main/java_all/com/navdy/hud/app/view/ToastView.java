package com.navdy.hud.app.view;

import com.navdy.service.library.events.input.Gesture;
import java.util.List;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import com.navdy.service.library.events.input.GestureEvent;
import android.view.View;
import butterknife.ButterKnife;
import android.view.ViewPropertyAnimator;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen;
import com.navdy.hud.app.framework.voice.TTSUtils;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import android.util.AttributeSet;
import android.content.Context;
import butterknife.InjectView;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.activity.Main;
import com.squareup.otto.Bus;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import android.widget.FrameLayout;

public class ToastView extends FrameLayout implements IInputHandler
{
    private static final Logger sLogger;
    private Bus bus;
    public boolean gestureOn;
    InputManager inputManager;
    private Main mainScreen;
    @InjectView(R.id.mainView)
    ConfirmationLayout mainView;
    private volatile boolean sendShowEvent;
    
    static {
        sLogger = new Logger(ToastView.class);
    }
    
    public ToastView(final Context context) {
        this(context, null);
    }
    
    public ToastView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ToastView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
        this.inputManager = instance.getInputManager();
        this.bus = instance.getBus();
    }
    
    public void animateIn(final String s, final String s2, final String s3, final boolean b) {
        this.sendShowEvent = false;
        this.mainView.setTranslationY((float)ToastPresenter.ANIMATION_TRANSLATION);
        this.mainView.setAlpha(0.0f);
        final ViewPropertyAnimator translationY = this.mainView.animate().alpha(1.0f).translationY(0.0f);
        long duration;
        if (b) {
            duration = 0L;
        }
        else {
            duration = 100L;
        }
        translationY.setDuration(duration).setStartDelay(250L).withStartAction((Runnable)new Runnable() {
            @Override
            public void run() {
                ToastView.sLogger.v("toast:animationIn :" + s2);
                ToastView.sLogger.v("[focus] toastView");
                ToastView.this.inputManager.setFocus((InputManager.IInputHandler)ToastView.this);
                ToastView.this.setVisibility(View.VISIBLE);
                ToastManager.getInstance().setToastDisplayFlag(true);
            }
        }).withEndAction((Runnable)new Runnable() {
            @Override
            public void run() {
                if (s != null) {
                    TTSUtils.sendSpeechRequest(s, s3);
                }
                while (true) {
                    if (s2 == null) {
                        break Label_0094;
                    }
                    try {
                        ToastView.sLogger.v("in dismiss screen:" + s2);
                        ToastView.this.bus.post(new ShowScreen.Builder().screen(Screen.valueOf(s2)).build());
                        ToastPresenter.clearScreenName();
                        ToastView.this.bus.post(new ToastManager.ShowToast(ToastPresenter.getCurrentId()));
                        ToastView.this.sendShowEvent = true;
                    }
                    catch (Throwable t) {
                        ToastView.sLogger.e(t);
                        continue;
                    }
                    break;
                }
            }
        }).start();
    }
    
    public void animateOut(final boolean b) {
        int n;
        if (b) {
            n = 50;
        }
        else {
            n = 100;
        }
        if (!this.sendShowEvent) {
            this.bus.post(new ToastManager.ShowToast(ToastPresenter.getCurrentId()));
            this.sendShowEvent = true;
        }
        this.mainView.animate().alpha(0.0f).translationY((float)ToastPresenter.ANIMATION_TRANSLATION).setDuration((long)n).withStartAction((Runnable)new Runnable() {
            @Override
            public void run() {
                final String screenName = ToastPresenter.getScreenName();
                if (screenName == null) {
                    return;
                }
                try {
                    ToastView.sLogger.v("out dismiss screen:" + screenName);
                    ToastView.this.bus.post(new ShowScreen.Builder().screen(Screen.valueOf(screenName)).build());
                    ToastPresenter.clearScreenName();
                }
                catch (Throwable t) {
                    ToastView.sLogger.e(t);
                }
            }
        }).withEndAction((Runnable)new Runnable() {
            @Override
            public void run() {
                ToastView.sLogger.v("toast:animationOut");
                if (ToastView.this.mainScreen == null) {
                    ToastView.this.mainScreen = RemoteDeviceManager.getInstance().getUiStateManager().getRootScreen();
                }
                if (ToastView.this.mainScreen != null) {
                    ToastView.this.inputManager.setFocus(null);
                    ToastView.this.mainScreen.setInputFocus();
                }
                ToastView.this.setVisibility(GONE);
                ToastView.this.revertToOriginal();
                ToastPresenter.clear();
                ToastManager.getInstance().setToastDisplayFlag(false);
            }
        }).start();
    }
    
    public void dismissToast() {
        if (ToastManager.getInstance().isToastDisplayed()) {
            this.animateOut(false);
        }
    }
    
    public ConfirmationLayout getConfirmation() {
        return this.mainView;
    }
    
    public ConfirmationLayout getMainLayout() {
        return this.mainView;
    }
    
    public ConfirmationLayout getView() {
        return this.mainView;
    }
    
    public IInputHandler nextHandler() {
        return null;
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        if (!this.gestureOn || gestureEvent.gesture == null) {
            return false;
        }
        final IToastCallback currentCallback = ToastPresenter.getCurrentCallback();
        boolean b = false;
        if (currentCallback == null) {
            b = false;
        }
        else {
            final List<ChoiceLayout.Choice> choices = this.mainView.choiceLayout.getChoices();
            if (choices == null || choices.size() == 0) {
                b = false;
            }
            else {
                switch (gestureEvent.gesture) {
                    default:
                        return false;
                    case GESTURE_SWIPE_LEFT:
                        currentCallback.executeChoiceItem(0, choices.get(0).id);
                        b = true;
                        break;
                    case GESTURE_SWIPE_RIGHT:
                        if (choices.size() == 1) {
                            currentCallback.executeChoiceItem(0, choices.get(0).id);
                        }
                        else {
                            currentCallback.executeChoiceItem(1, choices.get(1).id);
                        }
                        b = true;
                        break;
                }
            }
        }
        return b;
        b = false;
        return b;
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b = true;
        final IToastCallback currentCallback = ToastPresenter.getCurrentCallback();
        if (currentCallback == null || !currentCallback.onKey(customKeyEvent)) {
            switch (customKeyEvent) {
                case LEFT:
                    this.mainView.choiceLayout.moveSelectionLeft();
                    break;
                case RIGHT:
                    this.mainView.choiceLayout.moveSelectionRight();
                    break;
                case SELECT:
                    if (ToastPresenter.hasTimeout() && this.mainView.choiceLayout.getVisibility() != 0) {
                        this.dismissToast();
                        break;
                    }
                    this.mainView.choiceLayout.executeSelectedItem(true);
                    break;
                case POWER_BUTTON_LONG_PRESS:
                case LONG_PRESS:
                    b = false;
                    break;
            }
        }
        return b;
    }
    
    public void revertToOriginal() {
        this.mainView.fluctuatorView.stop();
        this.gestureOn = false;
    }
}
