package com.navdy.hud.app.ui.component.homescreen;

import com.navdy.hud.app.util.ConversionUtil;
import com.navdy.hud.app.view.MainView;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import butterknife.ButterKnife;
import android.animation.ObjectAnimator;
import android.animation.Animator;
import android.view.View;
import android.animation.AnimatorSet;
import android.util.AttributeSet;
import android.content.Context;
import com.navdy.hud.app.ui.framework.UIStateManager;
import butterknife.InjectView;
import android.widget.TextView;
import com.navdy.service.library.log.Logger;
import android.widget.LinearLayout;

public class MpgView extends LinearLayout
{
    private Logger logger;
    @InjectView(R.id.txt_mpg_label)
    TextView mpgLabelTextView;
    @InjectView(R.id.txt_mpg)
    TextView mpgTextView;
    private UIStateManager uiStateManager;
    
    public MpgView(final Context context) {
        this(context, null);
    }
    
    public MpgView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public MpgView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public void getTopAnimator(final AnimatorSet.Builder animatorSet$Builder, final boolean b) {
        if (b) {
            animatorSet$Builder.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this, (int)this.getX() + HomeScreenResourceValues.topViewSpeedOut));
            animatorSet$Builder.with((Animator)ObjectAnimator.ofFloat(this, View.ALPHA, new float[] { 1.0f, 0.0f }));
        }
        else {
            animatorSet$Builder.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this, (int)this.getX() - HomeScreenResourceValues.topViewSpeedOut));
            animatorSet$Builder.with((Animator)ObjectAnimator.ofFloat(this, View.ALPHA, new float[] { 0.0f, 1.0f }));
        }
    }
    
    protected void onFinishInflate() {
        this.logger = HomeScreenView.sLogger;
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        this.uiStateManager = RemoteDeviceManager.getInstance().getUiStateManager();
    }
    
    public void resetTopViewsAnimator() {
        this.setAlpha(1.0f);
        this.setView(this.uiStateManager.getCustomAnimateMode());
    }
    
    public void setView(final MainView.CustomAnimationMode customAnimationMode) {
        switch (customAnimationMode) {
            case EXPAND:
                this.setX((float)HomeScreenResourceValues.speedX);
                break;
            case SHRINK_LEFT:
                this.setX((float)HomeScreenResourceValues.speedShrinkLeftX);
                break;
        }
    }
    
    void updateInstantaneousFuelConsumption(double convertLpHundredKmToMPG) {
        convertLpHundredKmToMPG = ConversionUtil.convertLpHundredKmToMPG(convertLpHundredKmToMPG);
        String format = "- -";
        if (convertLpHundredKmToMPG > 0.0) {
            format = String.format("%d", (int)convertLpHundredKmToMPG);
        }
        this.mpgTextView.setText((CharSequence)format);
    }
}
