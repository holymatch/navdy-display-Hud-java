package com.navdy.hud.app.ui.component.destination;

import com.navdy.service.library.events.input.GestureEvent;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import com.navdy.hud.app.HudApplication;
import mortar.Mortar;
import android.animation.Animator.AnimatorListener;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import javax.inject.Inject;
import butterknife.InjectView;
import com.navdy.hud.app.ui.component.ConfirmationLayout;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.service.library.log.Logger;
import com.navdy.hud.app.manager.InputManager;
import android.widget.RelativeLayout;
import com.navdy.hud.app.manager.InputManager.IInputHandler;
import com.navdy.hud.app.manager.InputManager.CustomKeyEvent;



public class DestinationPickerView extends RelativeLayout implements IInputHandler
{
    private static final Logger sLogger;
    private VerticalMenuComponent.Callback callback;
    @InjectView(R.id.confirmationLayout)
    ConfirmationLayout confirmationLayout;
    @Inject
    public DestinationPickerScreen.Presenter presenter;
    @InjectView(R.id.rightBackground)
    View rightBackground;
    VerticalMenuComponent vmenuComponent;
    
    static {
        sLogger = new Logger(DestinationPickerView.class);
    }
    
    public DestinationPickerView(final Context context) {
        this(context, null);
    }
    
    public DestinationPickerView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public DestinationPickerView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.callback = new VerticalMenuComponent.Callback() {
            @Override
            public void close() {
                AnalyticsSupport.recordRouteSelectionCancelled();
                DestinationPickerView.this.presenter.close();
            }
            
            @Override
            public boolean isClosed() {
                return DestinationPickerView.this.presenter.isClosed();
            }
            
            @Override
            public boolean isItemClickable(final VerticalList.ItemSelectionState itemSelectionState) {
                return true;
            }
            
            @Override
            public void onBindToView(final VerticalList.Model model, final View view, final int n, final VerticalList.ModelState modelState) {
            }
            
            @Override
            public void onFastScrollEnd() {
            }
            
            @Override
            public void onFastScrollStart() {
            }
            
            @Override
            public void onItemSelected(final VerticalList.ItemSelectionState itemSelectionState) {
                ++itemSelectionState.pos;
                DestinationPickerView.this.presenter.itemSelected(itemSelectionState);
            }
            
            @Override
            public void onLoad() {
                DestinationPickerView.sLogger.v("onLoad");
                if (DestinationPickerView.this.presenter != null && (DestinationPickerView.this.presenter.isShowingRouteMap() || DestinationPickerView.this.presenter.isShowDestinationMap())) {
                    DestinationPickerView.this.setBackgroundColor(0);
                    DestinationPickerView.this.getHandler().postDelayed((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            if (DestinationPickerView.this.presenter.isAlive()) {
                                if (DestinationPickerView.this.presenter.isShowingRouteMap()) {
                                    DestinationPickerView.this.presenter.startMapFluctuator();
                                }
                                else if (DestinationPickerView.this.presenter.isShowDestinationMap()) {
                                    DestinationPickerView.this.presenter.itemSelected(DestinationPickerView.this.presenter.initialSelection);
                                    DestinationPickerView.this.presenter.itemSelection = DestinationPickerView.this.presenter.initialSelection;
                                }
                            }
                        }
                    }, 1000L);
                }
                DestinationPickerView.this.vmenuComponent.animateIn(null);
            }
            
            @Override
            public void onScrollIdle() {
            }
            
            @Override
            public void select(final VerticalList.ItemSelectionState itemSelectionState) {
                ++itemSelectionState.pos;
                DestinationPickerView.this.presenter.itemClicked(itemSelectionState);
            }
            
            @Override
            public void showToolTip() {
            }
        };
        if (!this.isInEditMode()) {
            Mortar.inject(context, this);
        }
        else {
            HudApplication.setContext(context);
        }
    }
    
    public IInputHandler nextHandler() {
        return InputManager.nextContainingHandler((View)this);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.vmenuComponent.clear();
        if (this.presenter != null) {
            this.presenter.dropView(this);
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject((View)this);
        this.vmenuComponent = new VerticalMenuComponent((ViewGroup)this, this.callback, false);
        this.vmenuComponent.leftContainer.setAlpha(0.0f);
        this.vmenuComponent.rightContainer.setAlpha(0.0f);
    }
    
    public boolean onGesture(final GestureEvent gestureEvent) {
        return this.confirmationLayout.getVisibility() != 0 && this.vmenuComponent.handleGesture(gestureEvent);
    }
    
    public boolean onKey(final CustomKeyEvent customKeyEvent) {
        boolean b;
        if (this.confirmationLayout.getVisibility() == 0) {
            b = this.confirmationLayout.handleKey(customKeyEvent);
        }
        else {
            b = this.vmenuComponent.handleKey(customKeyEvent);
        }
        return b;
    }
}
