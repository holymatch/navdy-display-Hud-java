package com.navdy.hud.app.ui.component.image;

import android.graphics.Paint;
import android.graphics.Canvas;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.widget.ImageView;

public class InitialsImageView extends ImageView
{
    private static int greyColor;
    private static int largeStyleTextSize;
    private static int mediumStyleTextSize;
    private static int smallStyleTextSize;
    private static int tinyStyleTextSize;
    private static Typeface typefaceLarge;
    private static Typeface typefaceSmall;
    private static boolean valuesSet;
    private static int whiteColor;
    private int bkColor;
    private String initials;
    private Paint paint;
    private boolean scaled;
    private Style style;
    
    public InitialsImageView(final Context context) {
        this(context, null, 0);
    }
    
    public InitialsImageView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public InitialsImageView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.style = Style.DEFAULT;
        this.bkColor = InitialsImageView.greyColor;
        this.init();
    }
    
    private void init() {
        if (!InitialsImageView.valuesSet) {
            InitialsImageView.valuesSet = true;
            final Resources resources = this.getResources();
            InitialsImageView.greyColor = resources.getColor(R.color.grey_4a);
            InitialsImageView.whiteColor = resources.getColor(R.color.hud_white);
            InitialsImageView.tinyStyleTextSize = (int)resources.getDimension(R.dimen.contact_image_tiny_text);
            InitialsImageView.smallStyleTextSize = (int)resources.getDimension(R.dimen.contact_image_small_text);
            InitialsImageView.mediumStyleTextSize = (int)resources.getDimension(R.dimen.contact_image_medium_text);
            InitialsImageView.largeStyleTextSize = (int)resources.getDimension(R.dimen.contact_image_large_text);
            InitialsImageView.typefaceSmall = Typeface.create("sans-serif-medium", 0);
            InitialsImageView.typefaceLarge = Typeface.create("sans-serif", 1);
        }
        (this.paint = new Paint()).setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }
    
    public Style getStyle() {
        return this.style;
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        if (this.style != Style.DEFAULT) {
            final int width = this.getWidth();
            final int height = this.getHeight();
            switch (this.style) {
                case SMALL:
                case TINY:
                    canvas.drawColor(0);
                    if (this.bkColor != 0) {
                        this.paint.setColor(this.bkColor);
                        canvas.drawCircle((float)(width / 2), (float)(height / 2), (float)(width / 2), this.paint);
                        break;
                    }
                    break;
            }
            Style style = this.style;
            if (this.scaled) {
                style = Style.SMALL;
            }
            if (this.initials != null) {
                this.paint.setColor(InitialsImageView.whiteColor);
                switch (style) {
                    case SMALL:
                        this.paint.setTextSize((float)InitialsImageView.smallStyleTextSize);
                        this.paint.setTypeface(InitialsImageView.typefaceSmall);
                        break;
                    case MEDIUM:
                        this.paint.setTextSize((float)InitialsImageView.mediumStyleTextSize);
                        this.paint.setTypeface(InitialsImageView.typefaceLarge);
                        break;
                    case LARGE:
                        this.paint.setTextSize((float)InitialsImageView.largeStyleTextSize);
                        this.paint.setTypeface(InitialsImageView.typefaceLarge);
                        break;
                    case TINY:
                        this.paint.setTextSize((float)InitialsImageView.tinyStyleTextSize);
                        this.paint.setTypeface(InitialsImageView.typefaceSmall);
                        break;
                }
                this.paint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText(this.initials, (float)(width / 2), (float)(int)(height / 2 - (this.paint.descent() + this.paint.ascent()) / 2.0f), this.paint);
            }
        }
    }
    
    public void setBkColor(final int bkColor) {
        this.bkColor = bkColor;
    }
    
    public void setImage(final int imageResource, final String s, final Style style) {
        this.setImageResource(imageResource);
        this.setInitials(s, style);
    }
    
    public void setInitials(final String initials, final Style style) {
        this.initials = initials;
        this.style = style;
        this.invalidate();
    }
    
    public void setScaled(final boolean scaled) {
        this.scaled = scaled;
    }
    
    public enum Style
    {
        DEFAULT, 
        LARGE, 
        MEDIUM, 
        SMALL, 
        TINY;
    }
}
