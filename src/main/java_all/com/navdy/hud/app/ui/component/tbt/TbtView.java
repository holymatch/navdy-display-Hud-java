package com.navdy.hud.app.ui.component.tbt;

import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.text.SpannableStringBuilder;
import android.text.StaticLayout;
import android.text.Layout;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import android.widget.TextView;
import com.navdy.hud.app.maps.MapEvents;
import com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.view.View;
import android.view.ViewGroup;
import android.support.constraint.ConstraintSet;
import android.view.ViewGroup;
import kotlin.TypeCastException;
import com.navdy.hud.app.R;
import android.util.AttributeSet;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import kotlin.jvm.internal.Intrinsics;
import com.navdy.hud.app.HudApplication;
//import kotlin.jvm.internal.DefaultConstructorMarker;
import android.support.constraint.ConstraintLayout;
import com.navdy.hud.app.view.MainView;
import java.util.HashMap;
import android.content.res.Resources;
import com.navdy.service.library.log.Logger;
import kotlin.Metadata;
import android.widget.FrameLayout;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 %2\u00020\u0001:\u0001%B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0006\u0010\u0017\u001a\u00020\u0018J\u001a\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\f2\n\u0010\u001b\u001a\u00060\u001cR\u00020\u001dJ\b\u0010\u001e\u001a\u00020\u0018H\u0014J\u0012\u0010\u001f\u001a\u00020\u00182\b\u0010\u001a\u001a\u0004\u0018\u00010\fH\u0002J\u000e\u0010 \u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\fJ\u0010\u0010!\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\fH\u0002J\u000e\u0010\"\u001a\u00020\u00182\u0006\u0010#\u001a\u00020$R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0016X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006&" }, d2 = { "Lcom/navdy/hud/app/ui/component/tbt/TbtView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentMode", "Lcom/navdy/hud/app/view/MainView$CustomAnimationMode;", "directionView", "Lcom/navdy/hud/app/ui/component/tbt/TbtDirectionView;", "distanceView", "Lcom/navdy/hud/app/ui/component/tbt/TbtDistanceView;", "groupContainer", "Landroid/support/constraint/ConstraintLayout;", "tbtNextManeuverView", "Lcom/navdy/hud/app/ui/component/tbt/TbtNextManeuverView;", "textInstructionView", "Lcom/navdy/hud/app/ui/component/tbt/TbtTextInstructionView;", "clear", "", "getCustomAnimator", "mode", "mainBuilder", "Landroid/animation/AnimatorSet.Builder;", "Landroid/animation/AnimatorSet;", "onFinishInflate", "setConstraints", "setView", "setViewAttrs", "updateDisplay", "event", "Lcom/navdy/hud/app/maps/MapEvents$ManeuverDisplay;", "Companion", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class TbtView extends FrameLayout
{
    public static final Companion Companion;
    private static final Logger logger;
    private static final int paddingFull;
    private static final int paddingMedium;
    private static final Resources resources;
    private static final float shrinkLeftX;
    private HashMap _$_findViewCache;
    private MainView.CustomAnimationMode currentMode;
    private TbtDirectionView directionView;
    private TbtDistanceView distanceView;
    private ConstraintLayout groupContainer;
    private TbtNextManeuverView tbtNextManeuverView;
    private TbtTextInstructionView textInstructionView;
    
    static {
        Companion = new Companion();
        logger = new Logger("TbtView");
        final Resources resources2 = HudApplication.getAppContext().getResources();
        Intrinsics.checkExpressionValueIsNotNull(resources2, "HudApplication.getAppContext().resources");
        resources = resources2;
        paddingFull = TbtView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_padding_full);
        paddingMedium = TbtView.Companion.getResources().getDimensionPixelSize(R.dimen.tbt_padding_medium);
        shrinkLeftX = TbtView.Companion.getResources().getDimension(R.dimen.tbt_shrinkleft_x);
    }
    
    public TbtView(@NotNull final Context context) {
        super(context);
        Intrinsics.checkParameterIsNotNull(context, "context");
    }
    
    public TbtView(@NotNull final Context context, @NotNull final AttributeSet set) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(set, "attrs");
        super(context, set);
    }
    
    public TbtView(@NotNull final Context context, @NotNull final AttributeSet set, final int n) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        Intrinsics.checkParameterIsNotNull(set, "attrs");
        super(context, set, n);
    }
    
    @NotNull
    public static final /* synthetic */ Logger access$getLogger$cp() {
        return TbtView.logger;
    }
    
    public static final /* synthetic */ int access$getPaddingFull$cp() {
        return TbtView.paddingFull;
    }
    
    public static final /* synthetic */ int access$getPaddingMedium$cp() {
        return TbtView.paddingMedium;
    }
    
    @NotNull
    public static final /* synthetic */ Resources access$getResources$cp() {
        return TbtView.resources;
    }
    
    public static final /* synthetic */ float access$getShrinkLeftX$cp() {
        return TbtView.shrinkLeftX;
    }
    
    private final void setConstraints(final MainView.CustomAnimationMode customAnimationMode) {
        if (customAnimationMode != null) {
            final TbtNextManeuverView tbtNextManeuverView = this.tbtNextManeuverView;
            Integer value;
            if (tbtNextManeuverView != null) {
                value = tbtNextManeuverView.getVisibility();
            }
            else {
                value = null;
            }
            final boolean equal = Intrinsics.areEqual(value, 0);
            int n = 0;
            switch (TbtView$WhenMappings.$EnumSwitchMapping$2[customAnimationMode.ordinal()]) {
                default:
                    n = TbtView.Companion.getPaddingMedium();
                    break;
                case 1:
                    if (equal) {
                        n = TbtView.Companion.getPaddingMedium();
                        break;
                    }
                    n = TbtView.Companion.getPaddingFull();
                    break;
            }
            final ViewGroup.LayoutParams layoutParams = ((TbtDirectionView)((View)this).findViewById(R.id.tbtDirectionView)).getLayoutParams();
            if (layoutParams == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            ((ViewGroup.MarginLayoutParams)layoutParams).leftMargin = n;
            final ViewGroup.LayoutParams layoutParams2 = ((TbtTextInstructionView)((View)this).findViewById(R.id.tbtTextInstructionView)).getLayoutParams();
            if (layoutParams2 == null) {
                throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            ((ViewGroup.MarginLayoutParams)layoutParams2).leftMargin = n;
            if (equal) {
                final TbtNextManeuverView tbtNextManeuverView2 = this.tbtNextManeuverView;
                Object layoutParams3;
                if (tbtNextManeuverView2 != null) {
                    layoutParams3 = tbtNextManeuverView2.getLayoutParams();
                }
                else {
                    layoutParams3 = null;
                }
                if (layoutParams3 == null) {
                    throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                }
                ((ViewGroup.MarginLayoutParams)layoutParams3).leftMargin = TbtView.Companion.getPaddingMedium() * 2;
            }
            final ConstraintSet set = new ConstraintSet();
            set.clone(this.groupContainer);
            final TbtDirectionView directionView = this.directionView;
            int id;
            if (directionView != null) {
                id = directionView.getId();
            }
            else {
                id = 0;
            }
            final TbtDistanceView distanceView = this.distanceView;
            int id2;
            if (distanceView != null) {
                id2 = distanceView.getId();
            }
            else {
                id2 = 0;
            }
            set.connect(id, 1, id2, 2, 0);
            final TbtTextInstructionView textInstructionView = this.textInstructionView;
            int id3;
            if (textInstructionView != null) {
                id3 = textInstructionView.getId();
            }
            else {
                id3 = 0;
            }
            final TbtDirectionView directionView2 = this.directionView;
            int id4;
            if (directionView2 != null) {
                id4 = directionView2.getId();
            }
            else {
                id4 = 0;
            }
            set.connect(id3, 1, id4, 2, 0);
            if (equal) {
                final TbtNextManeuverView tbtNextManeuverView3 = this.tbtNextManeuverView;
                int id5;
                if (tbtNextManeuverView3 != null) {
                    id5 = tbtNextManeuverView3.getId();
                }
                else {
                    id5 = 0;
                }
                final TbtTextInstructionView textInstructionView2 = this.textInstructionView;
                int id6;
                if (textInstructionView2 != null) {
                    id6 = textInstructionView2.getId();
                }
                else {
                    id6 = 0;
                }
                set.connect(id5, 1, id6, 2, 0);
            }
            set.applyTo(this.groupContainer);
            this.invalidate();
            this.requestLayout();
        }
    }
    
    private final void setViewAttrs(final MainView.CustomAnimationMode customAnimationMode) {
        final TbtDistanceView distanceView = this.distanceView;
        if (distanceView != null) {
            distanceView.setMode(customAnimationMode);
        }
        final TbtDirectionView directionView = this.directionView;
        if (directionView != null) {
            directionView.setMode(customAnimationMode);
        }
        final TbtTextInstructionView textInstructionView = this.textInstructionView;
        if (textInstructionView != null) {
            textInstructionView.setMode(customAnimationMode);
        }
        final TbtNextManeuverView tbtNextManeuverView = this.tbtNextManeuverView;
        if (tbtNextManeuverView != null) {
            tbtNextManeuverView.setMode(customAnimationMode);
        }
        this.setConstraints(customAnimationMode);
        this.currentMode = customAnimationMode;
    }
    
    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }
    
    public View _$_findCachedViewById(final int n) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View viewById;
        if ((viewById = this._$_findViewCache.get(n)) == null) {
            viewById = this.findViewById(n);
            this._$_findViewCache.put(n, viewById);
        }
        return viewById;
    }
    
    public final void clear() {
        final TbtDistanceView distanceView = this.distanceView;
        if (distanceView != null) {
            distanceView.clear();
        }
        final TbtDirectionView directionView = this.directionView;
        if (directionView != null) {
            directionView.clear();
        }
        final TbtTextInstructionView textInstructionView = this.textInstructionView;
        if (textInstructionView != null) {
            textInstructionView.clear();
        }
        final TbtNextManeuverView tbtNextManeuverView = this.tbtNextManeuverView;
        if (tbtNextManeuverView != null) {
            tbtNextManeuverView.clear();
        }
    }
    
    public final void getCustomAnimator(@NotNull final MainView.CustomAnimationMode viewAttrs, @NotNull final AnimatorSet.Builder animatorSet$Builder) {
        Intrinsics.checkParameterIsNotNull(viewAttrs, "mode");
        Intrinsics.checkParameterIsNotNull(animatorSet$Builder, "mainBuilder");
        this.setViewAttrs(viewAttrs);
        switch (TbtView$WhenMappings.$EnumSwitchMapping$1[viewAttrs.ordinal()]) {
            case 1:
                animatorSet$Builder.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this, 0.0f));
                break;
            case 2:
                animatorSet$Builder.with((Animator)HomeScreenUtils.getXPositionAnimator((View)this, TbtView.Companion.getShrinkLeftX()));
                break;
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        final View viewById = this.findViewById(R.id.groupContainer);
        if (viewById == null) {
            throw new TypeCastException("null cannot be cast to non-null type android.support.constraint.ConstraintLayout");
        }
        this.groupContainer = (ConstraintLayout)viewById;
        final View viewById2 = this.findViewById(R.id.tbtDistanceView);
        if (viewById2 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtDistanceView");
        }
        this.distanceView = (TbtDistanceView)viewById2;
        final View viewById3 = this.findViewById(R.id.tbtDirectionView);
        if (viewById3 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtDirectionView");
        }
        this.directionView = (TbtDirectionView)viewById3;
        final View viewById4 = this.findViewById(R.id.tbtTextInstructionView);
        if (viewById4 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtTextInstructionView");
        }
        this.textInstructionView = (TbtTextInstructionView)viewById4;
        final View viewById5 = this.findViewById(R.id.tbtNextManeuverView);
        if (viewById5 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtNextManeuverView");
        }
        this.tbtNextManeuverView = (TbtNextManeuverView)viewById5;
    }
    
    public final void setView(@NotNull final MainView.CustomAnimationMode viewAttrs) {
        Intrinsics.checkParameterIsNotNull(viewAttrs, "mode");
        this.setViewAttrs(viewAttrs);
        switch (TbtView$WhenMappings.$EnumSwitchMapping$0[viewAttrs.ordinal()]) {
            case 1:
                this.setX(0.0f);
                break;
            case 2:
                this.setX(TbtView.Companion.getShrinkLeftX());
                break;
        }
    }
    
    public final void updateDisplay(@NotNull final MapEvents.ManeuverDisplay maneuverDisplay) {
        Intrinsics.checkParameterIsNotNull(maneuverDisplay, "event");
        final TbtDistanceView distanceView = this.distanceView;
        if (distanceView != null) {
            distanceView.updateDisplay(maneuverDisplay);
        }
        final TbtDirectionView directionView = this.directionView;
        if (directionView != null) {
            directionView.updateDisplay(maneuverDisplay);
        }
        final TbtNextManeuverView tbtNextManeuverView = this.tbtNextManeuverView;
        Integer value;
        if (tbtNextManeuverView != null) {
            value = tbtNextManeuverView.getVisibility();
        }
        else {
            value = null;
        }
        final boolean equal = Intrinsics.areEqual(value, 0);
        final TbtNextManeuverView tbtNextManeuverView2 = this.tbtNextManeuverView;
        if (tbtNextManeuverView2 != null) {
            tbtNextManeuverView2.updateDisplay(maneuverDisplay);
        }
        final TbtNextManeuverView tbtNextManeuverView3 = this.tbtNextManeuverView;
        Integer value2;
        if (tbtNextManeuverView3 != null) {
            value2 = tbtNextManeuverView3.getVisibility();
        }
        else {
            value2 = null;
        }
        final boolean equal2 = Intrinsics.areEqual(value2, 0);
        if (equal != equal2) {
            final TbtTextInstructionView textInstructionView = this.textInstructionView;
            if (textInstructionView != null) {
                textInstructionView.updateDisplay(maneuverDisplay, true, equal2);
            }
            this.setConstraints(this.currentMode);
        }
        else {
            final TbtTextInstructionView textInstructionView2 = this.textInstructionView;
            if (textInstructionView2 != null) {
                TbtTextInstructionView.updateDisplay$default(textInstructionView2, maneuverDisplay, false, false, 6, null);
            }
        }
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0014\u0010\r\u001a\u00020\u000eX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u0015" }, d2 = { "Lcom/navdy/hud/app/ui/component/tbt/TbtView$Companion;", "", "()V", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "paddingFull", "", "getPaddingFull", "()I", "paddingMedium", "getPaddingMedium", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "shrinkLeftX", "", "getShrinkLeftX", "()F", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public static final class Companion
    {
        private final Logger getLogger() {
            return TbtView.access$getLogger$cp();
        }
        
        private final Resources getResources() {
            return TbtView.access$getResources$cp();
        }
        
        public final int getPaddingFull() {
            return TbtView.access$getPaddingFull$cp();
        }
        
        public final int getPaddingMedium() {
            return TbtView.access$getPaddingMedium$cp();
        }
        
        public final float getShrinkLeftX() {
            return TbtView.access$getShrinkLeftX$cp();
        }
    }
}
