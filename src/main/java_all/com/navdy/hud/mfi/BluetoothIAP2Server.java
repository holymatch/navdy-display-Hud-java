package com.navdy.hud.mfi;

import java.util.Arrays;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.IOException;
import android.bluetooth.BluetoothServerSocket;
import android.content.Intent;
import android.app.Activity;
import android.bluetooth.BluetoothSocket;
import android.bluetooth.BluetoothDevice;
import android.util.Log;
import android.os.Message;
import android.os.Bundle;
import android.os.Handler;
import android.bluetooth.BluetoothAdapter;
import java.util.UUID;

public class BluetoothIAP2Server implements PhysicalLayer
{
    public static final UUID ACCESSORY_IAP2;
    public static final UUID DEVICE_IAP2;
    private static final String SDP_NAME = "Wireless iAP";
    public static final int STATE_CONNECTED = 3;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_LISTEN = 1;
    public static final int STATE_NONE = 0;
    private static final String TAG = "BluetoothIAP2Server";
    private LinkLayer linkLayer;
    private AcceptThread mAcceptThread;
    private final BluetoothAdapter mAdapter;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private final Handler mHandler;
    private int mState;
    
    static {
        DEVICE_IAP2 = UUID.fromString("00000000-deca-fade-deca-deafdecacafe");
        ACCESSORY_IAP2 = UUID.fromString("00000000-deca-fade-deca-deafdecacaff");
    }
    
    public BluetoothIAP2Server(final Handler mHandler) {
        this.mAdapter = BluetoothAdapter.getDefaultAdapter();
        this.mState = 0;
        this.mHandler = mHandler;
    }
    
    private void connectionFailed() {
        final Message obtainMessage = this.mHandler.obtainMessage(5);
        final Bundle data = new Bundle();
        data.putString("toast", "Unable to connect device");
        obtainMessage.setData(data);
        this.mHandler.sendMessage(obtainMessage);
        this.start();
    }
    
    private void connectionLost() {
        this.linkLayer.connectionEnded();
        final Message obtainMessage = this.mHandler.obtainMessage(5);
        final Bundle data = new Bundle();
        data.putString("toast", "Device connection was lost");
        obtainMessage.setData(data);
        this.mHandler.sendMessage(obtainMessage);
        this.mHandler.postDelayed((Runnable)new Runnable() {
            @Override
            public void run() {
                BluetoothIAP2Server.this.start();
            }
        }, 500L);
    }
    
    private void setState(final int mState) {
        synchronized (this) {
            Log.d("BluetoothIAP2Server", "setState() " + this.mState + " -> " + mState);
            this.mState = mState;
            this.mHandler.obtainMessage(1, mState, -1).sendToTarget();
        }
    }
    
    public void connect(final BluetoothDevice bluetoothDevice) {
        synchronized (this) {
            Log.d("BluetoothIAP2Server", "connect to: " + bluetoothDevice);
            if (this.mConnectThread != null) {
                this.mConnectThread.cancel();
                this.mConnectThread = null;
            }
            if (this.mConnectedThread != null) {
                this.mConnectedThread.cancel();
                this.mConnectedThread = null;
            }
            (this.mConnectThread = new ConnectThread(bluetoothDevice)).start();
            this.setState(2);
        }
    }
    
    public void connect(final LinkLayer linkLayer) {
        this.linkLayer = linkLayer;
    }
    
    public void connected(final BluetoothSocket bluetoothSocket, final BluetoothDevice bluetoothDevice) {
        synchronized (this) {
            Log.d("BluetoothIAP2Server", "connected");
            if (this.mConnectThread != null) {
                this.mConnectThread.cancel();
                this.mConnectThread = null;
            }
            if (this.mConnectedThread != null) {
                this.mConnectedThread.cancel();
                this.mConnectedThread = null;
            }
            if (this.mAcceptThread != null) {
                this.mAcceptThread.cancel();
                this.mAcceptThread = null;
            }
            (this.mConnectedThread = new ConnectedThread(bluetoothSocket)).start();
            final Message obtainMessage = this.mHandler.obtainMessage(4);
            final Bundle data = new Bundle();
            data.putString("device_name", bluetoothDevice.getName());
            obtainMessage.setData(data);
            this.mHandler.sendMessage(obtainMessage);
            this.setState(3);
        }
    }
    
    @Override
    public byte[] getLocalAddress() {
        return Utils.parseMACAddress(this.mAdapter.getAddress());
    }
    
    public String getName() {
        return this.mAdapter.getName();
    }
    
    @Override
    public void queue(final LinkPacket linkPacket) {
        this.write(linkPacket.data);
    }
    
    public void reconfirmDiscoverableState(final Activity activity) {
        // monitorenter(this)
        if (activity == null) {
            return;
        }
        try {
            final Intent intent = new Intent("android.bluetooth.adapter.action.REQUEST_DISCOVERABLE");
            intent.putExtra("android.bluetooth.adapter.extra.DISCOVERABLE_DURATION", 300);
            activity.startActivity(intent);
            Log.d("BluetoothIAP2Server", "Now Discoverable");
        }
        catch (Exception ex) {
            Log.e("BluetoothIAP2Server", "Failed to make discoverable", (Throwable)ex);
        }
        finally {
        }
        // monitorexit(this)
    }
    
    public void start() {
        synchronized (this) {
            Log.d("BluetoothIAP2Server", "start");
            this.setState(1);
            if (this.mAcceptThread == null) {
                (this.mAcceptThread = new AcceptThread()).start();
            }
        }
    }
    
    public void stop() {
        synchronized (this) {
            Log.d("BluetoothIAP2Server", "stop");
            if (this.mConnectThread != null) {
                this.mConnectThread.cancel();
                this.mConnectThread = null;
            }
            if (this.mConnectedThread != null) {
                this.mConnectedThread.cancel();
                this.mConnectedThread = null;
            }
            if (this.mAcceptThread != null) {
                this.mAcceptThread.cancel();
                this.mAcceptThread = null;
            }
            this.setState(0);
        }
    }
    
    public void write(final byte[] array) {
        synchronized (this) {
            if (this.mState == 3) {
                final ConnectedThread mConnectedThread = this.mConnectedThread;
                // monitorexit(this)
                mConnectedThread.write(array);
            }
        }
    }
    
    private class AcceptThread extends Thread
    {
        private final BluetoothServerSocket mmServerSocket;
        
        public AcceptThread() {
            final BluetoothServerSocket bluetoothServerSocket = null;
            while (true) {
                try {
                    final BluetoothServerSocket listenUsingRfcommWithServiceRecord = BluetoothIAP2Server.this.mAdapter.listenUsingRfcommWithServiceRecord("Wireless iAP", BluetoothIAP2Server.ACCESSORY_IAP2);
                    this.mmServerSocket = listenUsingRfcommWithServiceRecord;
                }
                catch (IOException ex) {
                    Log.e("BluetoothIAP2Server", "Socket listen() failed", (Throwable)ex);
                    final BluetoothServerSocket listenUsingRfcommWithServiceRecord = bluetoothServerSocket;
                    continue;
                }
                break;
            }
        }
        
        public void cancel() {
            Log.d("BluetoothIAP2Server", "Socket cancel " + this);
            try {
                if (this.mmServerSocket != null) {
                    this.mmServerSocket.close();
                }
            }
            catch (IOException ex) {
                Log.e("BluetoothIAP2Server", "Socket close() of bluetoothServer failed", (Throwable)ex);
            }
        }
        
        @Override
        public void run() {
            Log.d("BluetoothIAP2Server", "Socket BEGIN mAcceptThread" + this);
            this.setName("AcceptThread");
            if (this.mmServerSocket == null) {
                Log.e("BluetoothIAP2Server", "No socket (maybe BT not enabled?) - END mAcceptThread");
            }
            else {
            Label_0047:
                while (true) {
                    while (BluetoothIAP2Server.this.mState != 3) {
                        while (true) {
                            while (true) {
                                Label_0170: {
                                    BluetoothSocket accept = null;
                                    Label_0155: {
                                        try {
                                            accept = this.mmServerSocket.accept();
                                            Log.d("BluetoothIAP2Server", "Socket accepted");
                                            if (accept == null) {
                                                break;
                                            }
                                            final BluetoothIAP2Server this$0 = BluetoothIAP2Server.this;
                                            synchronized (this$0) {
                                                switch (BluetoothIAP2Server.this.mState) {
                                                    default:
                                                        continue Label_0047;
                                                    case 1:
                                                    case 2:
                                                        break Label_0155;
                                                    case 0:
                                                    case 3:
                                                        break Label_0170;
                                                }
                                            }
                                        }
                                        catch (IOException ex) {
                                            Log.e("BluetoothIAP2Server", "Socket accept() failed", (Throwable)ex);
                                        }
                                        break Label_0047;
                                    }
                                    BluetoothIAP2Server.this.connected(accept, accept.getRemoteDevice());
                                    continue;
                                    try {
                                        accept.close();
                                        continue;
                                    }
                                    catch (IOException ex2) {
                                        Log.e("BluetoothIAP2Server", "Could not close unwanted socket", (Throwable)ex2);
                                        continue;
                                    }
                                }
                                continue;
                            }
                        }
                    }
                    break;
                }
                Log.i("BluetoothIAP2Server", "END mAcceptThread");
            }
        }
    }
    
    private class ConnectThread extends Thread
    {
        private final BluetoothDevice mmDevice;
        private final BluetoothSocket mmSocket;
        
        public ConnectThread(final BluetoothDevice mmDevice) {
            this.mmDevice = mmDevice;
            BluetoothIAP2Server.this = null;
            while (true) {
                try {
                    BluetoothIAP2Server.this = (BluetoothIAP2Server)mmDevice.createRfcommSocketToServiceRecord(BluetoothIAP2Server.DEVICE_IAP2);
                    this.mmSocket = (BluetoothSocket)BluetoothIAP2Server.this;
                }
                catch (IOException ex) {
                    Log.e("BluetoothIAP2Server", "Socket create() failed", (Throwable)ex);
                    continue;
                }
                break;
            }
        }
        
        public void cancel() {
            try {
                this.mmSocket.close();
            }
            catch (IOException ex) {
                Log.e("BluetoothIAP2Server", "close() of connect socket failed", (Throwable)ex);
            }
        }
        
        @Override
        public void run() {
            // 
            This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     2: ldc             "BEGIN mConnectThread"
            //     4: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
            //     7: pop            
            //     8: aload_0        
            //     9: ldc             "ConnectThread"
            //    11: invokevirtual   com/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread.setName:(Ljava/lang/String;)V
            //    14: aload_0        
            //    15: getfield        com/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread.this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;
            //    18: invokestatic    com/navdy/hud/mfi/BluetoothIAP2Server.access$000:(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)Landroid/bluetooth/BluetoothAdapter;
            //    21: invokevirtual   android/bluetooth/BluetoothAdapter.cancelDiscovery:()Z
            //    24: pop            
            //    25: aload_0        
            //    26: getfield        com/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread.mmSocket:Landroid/bluetooth/BluetoothSocket;
            //    29: invokevirtual   android/bluetooth/BluetoothSocket.connect:()V
            //    32: aload_0        
            //    33: getfield        com/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread.this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;
            //    36: astore_1       
            //    37: aload_1        
            //    38: dup            
            //    39: astore_2       
            //    40: monitorenter   
            //    41: aload_0        
            //    42: getfield        com/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread.this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;
            //    45: aconst_null    
            //    46: invokestatic    com/navdy/hud/mfi/BluetoothIAP2Server.access$302:(Lcom/navdy/hud/mfi/BluetoothIAP2Server;Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;)Lcom/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread;
            //    49: pop            
            //    50: aload_2        
            //    51: monitorexit    
            //    52: aload_0        
            //    53: getfield        com/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread.this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;
            //    56: aload_0        
            //    57: getfield        com/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread.mmSocket:Landroid/bluetooth/BluetoothSocket;
            //    60: aload_0        
            //    61: getfield        com/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread.mmDevice:Landroid/bluetooth/BluetoothDevice;
            //    64: invokevirtual   com/navdy/hud/mfi/BluetoothIAP2Server.connected:(Landroid/bluetooth/BluetoothSocket;Landroid/bluetooth/BluetoothDevice;)V
            //    67: return         
            //    68: astore_1       
            //    69: aload_0        
            //    70: getfield        com/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread.mmSocket:Landroid/bluetooth/BluetoothSocket;
            //    73: invokevirtual   android/bluetooth/BluetoothSocket.close:()V
            //    76: aload_0        
            //    77: getfield        com/navdy/hud/mfi/BluetoothIAP2Server$ConnectThread.this$0:Lcom/navdy/hud/mfi/BluetoothIAP2Server;
            //    80: invokestatic    com/navdy/hud/mfi/BluetoothIAP2Server.access$200:(Lcom/navdy/hud/mfi/BluetoothIAP2Server;)V
            //    83: goto            67
            //    86: astore_1       
            //    87: ldc             "BluetoothIAP2Server"
            //    89: ldc             "unable to close() socket during connection failure"
            //    91: aload_1        
            //    92: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //    95: pop            
            //    96: goto            76
            //    99: astore_3       
            //   100: aload_2        
            //   101: monitorexit    
            //   102: aload_3        
            //   103: athrow         
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                 
            //  -----  -----  -----  -----  ---------------------
            //  25     32     68     99     Ljava/io/IOException;
            //  41     52     99     104    Any
            //  69     76     86     99     Ljava/io/IOException;
            //  100    102    99     104    Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0067:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
            //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
            //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
            //     at java.lang.Thread.run(Unknown Source)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
    }
    
    private class ConnectedThread extends Thread
    {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private final BluetoothSocket mmSocket;
        
        public ConnectedThread(final BluetoothSocket mmSocket) {
            Log.d("BluetoothIAP2Server", "create ConnectedThread");
            this.mmSocket = mmSocket;
            BluetoothIAP2Server.this = null;
            final BluetoothIAP2Server bluetoothIAP2Server2 = null;
            while (true) {
                try {
                    final Object mmInStream = BluetoothIAP2Server.this = (BluetoothIAP2Server)mmSocket.getInputStream();
                    BluetoothIAP2Server.this = (BluetoothIAP2Server)mmSocket.getOutputStream();
                    this.mmInStream = (InputStream)mmInStream;
                    this.mmOutStream = (OutputStream)BluetoothIAP2Server.this;
                }
                catch (IOException ex) {
                    Log.e("BluetoothIAP2Server", "temp sockets not created", (Throwable)ex);
                    final Object mmInStream = BluetoothIAP2Server.this;
                    BluetoothIAP2Server.this = bluetoothIAP2Server2;
                    continue;
                }
                break;
            }
        }
        
        public void cancel() {
            try {
                this.mmSocket.close();
            }
            catch (IOException ex) {
                Log.e("BluetoothIAP2Server", "close() of connect socket failed", (Throwable)ex);
            }
        }
        
        @Override
        public void run() {
            final BluetoothDevice remoteDevice = this.mmSocket.getRemoteDevice();
            Log.i("BluetoothIAP2Server", "BEGIN mConnectedThread - " + remoteDevice.getAddress() + " name:" + remoteDevice.getName());
            BluetoothIAP2Server.this.linkLayer.connectionStarted(remoteDevice.getAddress(), remoteDevice.getName());
            final byte[] array = new byte[1024];
            try {
                while (true) {
                    BluetoothIAP2Server.this.linkLayer.queue(new LinkPacket(Arrays.copyOf(array, this.mmInStream.read(array))));
                }
            }
            catch (IOException ex) {
                Log.e("BluetoothIAP2Server", "disconnected", (Throwable)ex);
                BluetoothIAP2Server.this.connectionLost();
            }
        }
        
        public void write(final byte[] array) {
            try {
                this.mmOutStream.write(array);
            }
            catch (IOException ex) {
                Log.e("BluetoothIAP2Server", "Exception during write", (Throwable)ex);
            }
        }
    }
}
