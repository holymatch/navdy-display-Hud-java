package com.navdy.service.library.util;

final public class RuntimeTypeAdapterFactory implements com.google.gson.TypeAdapterFactory {
    final private java.util.Map aliasToSubtype;
    final private Class baseType;
    final private java.util.Map labelToSubtype;
    final private java.util.Map subtypeToLabel;
    final private String typeFieldName;
    
    private RuntimeTypeAdapterFactory(Class a, String s) {
        this.labelToSubtype = (java.util.Map)new java.util.LinkedHashMap();
        this.aliasToSubtype = (java.util.Map)new java.util.LinkedHashMap();
        this.subtypeToLabel = (java.util.Map)new java.util.LinkedHashMap();
        if (s != null && a != null) {
            this.baseType = a;
            this.typeFieldName = s;
            return;
        }
        throw new NullPointerException();
    }
    
    static String access$000(com.navdy.service.library.util.RuntimeTypeAdapterFactory a) {
        return a.typeFieldName;
    }
    
    static Class access$100(com.navdy.service.library.util.RuntimeTypeAdapterFactory a) {
        return a.baseType;
    }
    
    static java.util.Map access$200(com.navdy.service.library.util.RuntimeTypeAdapterFactory a) {
        return a.subtypeToLabel;
    }
    
    public static com.navdy.service.library.util.RuntimeTypeAdapterFactory of(Class a) {
        return new com.navdy.service.library.util.RuntimeTypeAdapterFactory(a, "type");
    }
    
    public static com.navdy.service.library.util.RuntimeTypeAdapterFactory of(Class a, String s) {
        return new com.navdy.service.library.util.RuntimeTypeAdapterFactory(a, s);
    }
    
    public com.google.gson.TypeAdapter create(com.google.gson.Gson a, com.google.gson.reflect.TypeToken a0) {
        com.navdy.service.library.util.RuntimeTypeAdapterFactory$1 a1 = null;
        if (a0.getRawType() == this.baseType) {
            java.util.LinkedHashMap a2 = new java.util.LinkedHashMap();
            java.util.LinkedHashMap a3 = new java.util.LinkedHashMap();
            Object a4 = this.labelToSubtype.entrySet().iterator();
            while(((java.util.Iterator)a4).hasNext()) {
                Object a5 = ((java.util.Iterator)a4).next();
                com.google.gson.TypeAdapter a6 = a.getDelegateAdapter((com.google.gson.TypeAdapterFactory)this, com.google.gson.reflect.TypeToken.get((Class)((java.util.Map.Entry)a5).getValue()));
                ((java.util.Map)a2).put(((java.util.Map.Entry)a5).getKey(), a6);
                ((java.util.Map)a3).put(((java.util.Map.Entry)a5).getValue(), a6);
            }
            Object a7 = this.aliasToSubtype.entrySet().iterator();
            while(((java.util.Iterator)a7).hasNext()) {
                Object a8 = ((java.util.Iterator)a7).next();
                com.google.gson.TypeAdapter a9 = a.getDelegateAdapter((com.google.gson.TypeAdapterFactory)this, com.google.gson.reflect.TypeToken.get((Class)((java.util.Map.Entry)a8).getValue()));
                ((java.util.Map)a2).put(((java.util.Map.Entry)a8).getKey(), a9);
            }
            a1 = new com.navdy.service.library.util.RuntimeTypeAdapterFactory$1(this, (java.util.Map)a2, (java.util.Map)a3);
        } else {
            a1 = null;
        }
        return a1;
    }
    
    public com.navdy.service.library.util.RuntimeTypeAdapterFactory registerSubtype(Class a) {
        return this.registerSubtype(a, a.getSimpleName());
    }
    
    public com.navdy.service.library.util.RuntimeTypeAdapterFactory registerSubtype(Class a, String s) {
        if (a != null && s != null) {
            if (!this.subtypeToLabel.containsKey(a) && !this.labelToSubtype.containsKey(s)) {
                if (this.aliasToSubtype.containsKey(s)) {
                    throw new IllegalArgumentException("alias already exists for label");
                }
                this.labelToSubtype.put(s, a);
                this.subtypeToLabel.put(a, s);
                return this;
            }
            throw new IllegalArgumentException("types and labels must be unique");
        }
        throw new NullPointerException();
    }
    
    public com.navdy.service.library.util.RuntimeTypeAdapterFactory registerSubtypeAlias(Class a, String s) {
        if (a != null && s != null) {
            if (!this.labelToSubtype.containsKey(s) && !this.aliasToSubtype.containsKey(s)) {
                if (!this.subtypeToLabel.containsKey(a)) {
                    throw new IllegalArgumentException("subtype must already have a mapping to add an alias");
                }
                this.aliasToSubtype.put(s, a);
                return this;
            }
            throw new IllegalArgumentException("labels must be unique");
        }
        throw new NullPointerException();
    }
}
