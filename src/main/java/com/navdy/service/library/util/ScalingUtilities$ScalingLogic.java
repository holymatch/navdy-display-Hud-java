package com.navdy.service.library.util;


public enum ScalingUtilities$ScalingLogic {
    CROP(0),
    FIT(1);

    private int value;
    ScalingUtilities$ScalingLogic(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class ScalingUtilities$ScalingLogic extends Enum {
//    final private static com.navdy.service.library.util.ScalingUtilities$ScalingLogic[] $VALUES;
//    final public static com.navdy.service.library.util.ScalingUtilities$ScalingLogic CROP;
//    final public static com.navdy.service.library.util.ScalingUtilities$ScalingLogic FIT;
//    
//    static {
//        CROP = new com.navdy.service.library.util.ScalingUtilities$ScalingLogic("CROP", 0);
//        FIT = new com.navdy.service.library.util.ScalingUtilities$ScalingLogic("FIT", 1);
//        com.navdy.service.library.util.ScalingUtilities$ScalingLogic[] a = new com.navdy.service.library.util.ScalingUtilities$ScalingLogic[2];
//        a[0] = CROP;
//        a[1] = FIT;
//        $VALUES = a;
//    }
//    
//    private ScalingUtilities$ScalingLogic(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.service.library.util.ScalingUtilities$ScalingLogic valueOf(String s) {
//        return (com.navdy.service.library.util.ScalingUtilities$ScalingLogic)Enum.valueOf(com.navdy.service.library.util.ScalingUtilities$ScalingLogic.class, s);
//    }
//    
//    public static com.navdy.service.library.util.ScalingUtilities$ScalingLogic[] values() {
//        return $VALUES.clone();
//    }
//}
//