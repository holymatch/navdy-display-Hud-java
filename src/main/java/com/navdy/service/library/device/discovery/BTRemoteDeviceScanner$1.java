package com.navdy.service.library.device.discovery;

class BTRemoteDeviceScanner$1 implements Runnable {
    final com.navdy.service.library.device.discovery.BTRemoteDeviceScanner this$0;
    
    BTRemoteDeviceScanner$1(com.navdy.service.library.device.discovery.BTRemoteDeviceScanner a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (!com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.access$000(this.this$0)) {
            com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.access$002(this.this$0, true);
            android.content.IntentFilter a = new android.content.IntentFilter();
            a.addAction("android.bluetooth.device.action.FOUND");
            a.addAction("android.bluetooth.device.action.UUID");
            a.addAction("android.bluetooth.device.action.NAME_CHANGED");
            a.addAction("android.bluetooth.adapter.action.DISCOVERY_FINISHED");
            this.this$0.mContext.registerReceiver(com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.access$100(this.this$0), a);
            com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.access$202(this.this$0, (java.util.Set)new java.util.HashSet());
            if (com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.access$300(this.this$0) != null) {
                if (com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.access$300(this.this$0).isDiscovering()) {
                    com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.access$300(this.this$0).cancelDiscovery();
                }
                com.navdy.service.library.device.discovery.BTRemoteDeviceScanner.access$300(this.this$0).startDiscovery();
                this.this$0.dispatchOnScanStarted();
            }
        }
    }
}
