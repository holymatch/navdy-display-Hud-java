package com.navdy.service.library.device.connection.tunnel;

public class Utils {
    final static char[] hexArray;
    
    static {
        hexArray = "0123456789ABCDEF".toCharArray();
    }
    
    public Utils() {
    }
    
    public static String bytesToHex(byte[] a) {
        return com.navdy.service.library.device.connection.tunnel.Utils.bytesToHex(a, false);
    }
    
    public static String bytesToHex(byte[] a, boolean b) {
        int i = b ? 3 : 2;
        char[] a0 = new char[a.length * i];
        int i0 = 0;
        while(i0 < a.length) {
            int i1 = a[i0];
            int i2 = i1 & 255;
            int i3 = i0 * i;
            int i4 = (char)hexArray[i2 >>> 4];
            a0[i3] = (char)i4;
            int i5 = i0 * i + 1;
            int i6 = (char)hexArray[i2 & 15];
            a0[i5] = (char)i6;
            if (b) {
                a0[i0 * i + 2] = (char)32;
            }
            i0 = i0 + 1;
        }
        return new String(a0);
    }
    
    public static String toASCII(byte[] a) {
        StringBuilder a0 = new StringBuilder();
        int i = a.length;
        int i0 = 0;
        while(i0 < i) {
            int i1 = 0;
            int i2 = a[i0];
            int i3 = i2 & 255;
            label2: {
                label0: {
                    label1: {
                        if (i3 < 32) {
                            break label1;
                        }
                        if (i3 < 128) {
                            break label0;
                        }
                    }
                    i1 = 46;
                    break label2;
                }
                i1 = (char)i3;
            }
            a0.append((char)i1);
            i0 = i0 + 1;
        }
        return a0.toString();
    }
}
