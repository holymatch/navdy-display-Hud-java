package com.navdy.service.library.device.discovery;

public class BTDeviceBroadcaster implements com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster {
    final public static int DISCOVERY_TIMEOUT = 300;
    final public static String DISPLAY_NAME = " Navdy Display";
    final public static java.util.regex.Pattern DISPLAY_PATTERN;
    final private static com.navdy.service.library.log.Logger sLogger;
    private android.bluetooth.BluetoothAdapter adapter;
    private boolean started;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.service.library.device.discovery.BTDeviceBroadcaster.class);
        DISPLAY_PATTERN = java.util.regex.Pattern.compile("navdy|hud");
    }
    
    public BTDeviceBroadcaster() {
        this.adapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();
    }
    
    private void ensureDisplayLike() {
        String s = this.adapter.getName();
        if (!com.navdy.service.library.device.discovery.BTDeviceBroadcaster.isDisplay(s)) {
            this.adapter.setName(new StringBuilder().append(s).append(" Navdy Display").toString());
        }
    }
    
    public static boolean isDisplay(String s) {
        return DISPLAY_PATTERN.matcher((CharSequence)s.toLowerCase(java.util.Locale.US)).find();
    }
    
    private void setScanMode(int i, int i0) {
        try {
            Class a = Class.forName((this.adapter).getClass().getName());
            Class[] a0 = new Class[2];
            a0[0] = Integer.TYPE;
            a0[1] = Integer.TYPE;
            java.lang.reflect.Method a1 = a.getDeclaredMethod("setScanMode", a0);
            android.bluetooth.BluetoothAdapter a2 = this.adapter;
            Object[] a3 = new Object[2];
            a3[0] = Integer.valueOf(i);
            a3[1] = Integer.valueOf(i0);
            a1.invoke(a2, a3);
        } catch(Exception a4) {
            sLogger.d(new StringBuilder().append("enableDiscovery failed - ").append(a4.toString()).toString(), (Throwable)a4);
        }
    }
    
    public boolean start() {
        if (!this.started) {
            this.started = true;
            this.setScanMode(23, 300);
        }
        return this.started;
    }
    
    public boolean stop() {
        if (this.started) {
            this.started = false;
            this.setScanMode(20, 300);
        }
        return true;
    }
}
