package com.navdy.service.library.device.connection;

import android.content.Context;

import com.navdy.service.library.network.SocketAcceptor;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.util.IOUtils;

import java.io.IOException;

public class AcceptorListener extends ConnectionListener {
    /* access modifiers changed from: private */
    public SocketAcceptor acceptor;
    /* access modifiers changed from: private */
    public ConnectionType connectionType;

    private class AcceptThread extends ConnectionListener.AcceptThread {
        private volatile boolean closing;

        public AcceptThread() throws IOException {
            super();
            setName("AcceptThread-" + AcceptorListener.this.acceptor.getClass().getSimpleName());
        }

        public void cancel() {
            AcceptorListener.this.logger.d("Socket cancel " + this);
            this.closing = true;
            IOUtils.closeStream(AcceptorListener.this.acceptor);
        }

        public void run() {
            AcceptorListener.this.logger.d(getName() + " started, closing:" + this.closing);
            AcceptorListener.this.dispatchStarted();
            SocketAdapter socketAdapter = null;
            try {
                socketAdapter = AcceptorListener.this.acceptor.accept();
                AcceptorListener.this.dispatchConnected(new SocketConnection(AcceptorListener.this.acceptor.getRemoteConnectionInfo(socketAdapter, AcceptorListener.this.connectionType), socketAdapter));
            } catch (Throwable th) {
                if (!this.closing) {
                    AcceptorListener.this.logger.e("Socket accept() failed", th);
                }
                IOUtils.closeStream(socketAdapter);
            } finally {
                cancel();
            }
            AcceptorListener.this.dispatchStopped();
            AcceptorListener.this.logger.i("END " + getName());
        }
    }

    public AcceptorListener(Context context, SocketAcceptor socketAcceptor, ConnectionType connectionType2) {
        super(context, "Acceptor/" + socketAcceptor.getClass().getSimpleName());
        this.acceptor = socketAcceptor;
        this.connectionType = connectionType2;
    }

    /* access modifiers changed from: protected */
    public AcceptorListener.AcceptThread getNewAcceptThread() throws IOException {
        return new AcceptorListener.AcceptThread();
    }

    public ConnectionType getType() {
        return this.connectionType;
    }

    public String toString() {
        return getClass().getSimpleName() + (this.acceptor != null ? "/" + this.acceptor.getClass().getSimpleName() : "");
    }
}