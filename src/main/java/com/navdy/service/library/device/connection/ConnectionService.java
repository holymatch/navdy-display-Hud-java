package com.navdy.service.library.device.connection;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.TransactionTooLargeException;

import com.navdy.hud.app.IEventListener;
import com.navdy.hud.app.IEventSource;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.device.RemoteDevice.LinkStatus;
import com.navdy.service.library.device.RemoteDeviceRegistry;
import com.navdy.service.library.device.discovery.RemoteDeviceBroadcaster;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.Ext_NavdyEvent;
import com.navdy.service.library.events.NavdyEvent;
import com.navdy.service.library.events.NavdyEventUtil;
import com.navdy.service.library.events.Ping;
import com.navdy.service.library.events.WireUtil;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.connection.ConnectionStatus;
import com.navdy.service.library.events.connection.DisconnectRequest;
import com.navdy.service.library.events.connection.LinkPropertiesChanged;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.SystemUtils;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public abstract class ConnectionService extends Service implements ConnectionListener.Listener, RemoteDevice.Listener {
    public static final UUID ACCESSORY_IAP2 = UUID.fromString("00000000-deca-fade-deca-deafdecacaff");
    public static final String ACTION_LINK_BANDWIDTH_LEVEL_CHANGED = "LINK_BANDWIDTH_LEVEL_CHANGED";
    public static final String CATEGORY_NAVDY_LINK = "NAVDY_LINK";
    private static final int CONNECT_TIMEOUT = 10000;
    private static final int DEAD_CONNECTION_TIME = 60000;
    private static final ConnectionStatus DEVICES_CHANGED_EVENT = new ConnectionStatus(ConnectionStatus.Status.CONNECTION_PAIRED_DEVICES_CHANGED, null);
    public static final UUID DEVICE_IAP2 = UUID.fromString("00000000-deca-fade-deca-deafdecacafe");
    private static final int EVENT_DISCONNECT = 3;
    protected static final int EVENT_HEARTBEAT = 2;
    private static final int EVENT_RESTART_LISTENERS = 4;
    private static final int EVENT_STATE_CHANGE = 1;
    public static final String EXTRA_BANDWIDTH_LEVEL = "EXTRA_BANDWIDTH_MODE";
    private static final int HEARTBEAT_INTERVAL = 4000;
    private static final int IDLE_TIMEOUT = 1000;
    public static final String ILLEGAL_ARGUMENT = "ILLEGAL_ARGUMENT";
    public static final String NAVDY_IAP_NAME = "Navdy iAP";
    public static final String NAVDY_PROTO_SERVICE_NAME = "Navdy";
    public static final UUID NAVDY_PROTO_SERVICE_UUID = UUID.fromString("1992B7D7-C9B9-4F4F-AA7F-9FE33D95851E");
    public static final UUID NAVDY_PROXY_TUNNEL_UUID = UUID.fromString("D72BC85F-F015-4F24-A72F-35924E10888F");
    private static final int PAIRING_TIMEOUT = 30000;
    public static final String REASON_DEAD_CONNECTION = "DEAD_CONNECTION";
    private static final int RECONNECT_DELAY = 2000;
    private static final int RECONNECT_TIMEOUT = 10000;
    protected static final int RETRY_CONNECTING_AFTER_FAILURE_TIMEOUT = 30000;
    private static final int SLEEP_TIMEOUT = 60000;
    private BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (Objects.equals(intent.getAction(), "android.bluetooth.adapter.action.STATE_CHANGED") && intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE) == 12) {
                if (ConnectionService.this.state == ConnectionService.State.START) {
                    ConnectionService.this.logger.i("bluetooth turned on - exiting START state");
                    ConnectionService.this.setState(ConnectionService.State.IDLE);
                    return;
                }
                ConnectionService.this.logger.i("bluetooth turned on - restarting listeners");
                ConnectionService.this.serviceHandler.sendEmptyMessage(ConnectionService.EVENT_RESTART_LISTENERS);
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean broadcasting = false;
    protected final Object connectionLock = new Object();
    protected RemoteDeviceRegistry deviceRegistry;
    private boolean forceReconnect = false;
    protected boolean inProcess = true;
    private long lastMessageReceivedTime;
    protected final Object listenerLock = new Object();
    /* access modifiers changed from: private */
    public IEventListener[] listenersArray = new IEventListener[0];
    /* access modifiers changed from: private */
    public boolean listening = false;
    protected final Logger logger = new Logger(getClass());
    protected ConnectionListener[] mConnectionListeners;
    private IEventSource.Stub mEventSource = new IEventSource.Stub() {
        public void addEventListener(IEventListener iEventListener) throws RemoteException {
            synchronized (ConnectionService.this.listenerLock) {
                ConnectionService.this.mListeners.add(iEventListener);
                ConnectionService.this.createListenerList();
            }
            ConnectionService.this.logger.v("listener added: pid:" + SystemUtils.getProcessName(ConnectionService.this.getApplicationContext(), getCallingPid()));
            if (ConnectionService.this.mRemoteDevice == null || !ConnectionService.this.mRemoteDevice.isConnected()) {
                ConnectionService.this.forwardEventLocally(new ConnectionStateChange("", ConnectionStateChange.ConnectionState.CONNECTION_DISCONNECTED));
                ConnectionService.this.forwardEventLocally(new ConnectionStateChange("", ConnectionStateChange.ConnectionState.CONNECTION_LINK_LOST));
            } else {
                ConnectionService.this.forwardEventLocally(new ConnectionStateChange(ConnectionService.this.mRemoteDevice.getDeviceId().toString(), ConnectionStateChange.ConnectionState.CONNECTION_LINK_ESTABLISHED));
                ConnectionService.this.forwardEventLocally(new ConnectionStateChange(ConnectionService.this.mRemoteDevice.getDeviceId().toString(), ConnectionStateChange.ConnectionState.CONNECTION_CONNECTED));
                DeviceInfo deviceInfo = ConnectionService.this.mRemoteDevice.getDeviceInfo();
                if (deviceInfo != null) {
                    ConnectionService.this.logger.v("send device info");
                    ConnectionService.this.forwardEventLocally(deviceInfo);
                }
            }
            ConnectionService.this.sendEventsOnLocalConnect();
        }

        public void postEvent(byte[] bArr) throws RemoteException {
            synchronized (ConnectionService.this.listenerLock) {
                boolean z = false;
                boolean z2 = false;
                for (int i = 0; i < ConnectionService.this.listenersArray.length; i++) {
                    try {
                        ConnectionService.this.listenersArray[i].onEvent(bArr);
                        z2 = true;
                    } catch (DeadObjectException e) {
                        ConnectionService.this.logger.w("Reaping dead listener", e);
                        ConnectionService.this.mListeners.remove(ConnectionService.this.listenersArray[i]);
                        z = true;
                    } catch (TransactionTooLargeException e2) {
                        ConnectionService.this.logger.w("Communication Pipe is full:", e2);
                    } catch (Throwable th) {
                        ConnectionService.this.logger.w("Exception throws by remote:", th);
                    }
                }
                if (ConnectionService.this.logger.isLoggable(ConnectionService.EVENT_HEARTBEAT) && !z2) {
                    ConnectionService.this.logger.d("No one listening for event - byte length " + bArr.length);
                }
                if (z) {
                    ConnectionService.this.createListenerList();
                }
            }
        }

        public void postRemoteEvent(String str, byte[] bArr) throws RemoteException {
            if (str == null || bArr == null) {
                ConnectionService.this.logger.e("illegal argument");
                throw new RemoteException(ConnectionService.ILLEGAL_ARGUMENT);
            }
            NavdyDeviceId navdyDeviceId = new NavdyDeviceId(str);
            if (navdyDeviceId.equals(NavdyDeviceId.getThisDevice(ConnectionService.this))) {
                NavdyEvent.MessageType eventType = WireUtil.getEventType(bArr);
                if (eventType == null || !ConnectionService.this.processLocalEvent(bArr, eventType)) {
                    ConnectionService.this.logger.w("Connection service ignored message:" + eventType);
                }
            } else if (ConnectionService.this.mRemoteDevice == null || !ConnectionService.this.mRemoteDevice.isConnected()) {
                if (ConnectionService.this.logger.isLoggable(ConnectionService.EVENT_HEARTBEAT)) {
                    ConnectionService.this.logger.e("app not connected");
                }
            } else if (ConnectionService.this.mRemoteDevice.getDeviceId().equals(navdyDeviceId)) {
                ConnectionService.this.mRemoteDevice.postEvent(bArr);
            } else {
                ConnectionService.this.logger.i("Device id mismatch: deviceId=" + navdyDeviceId + " remote:" + ConnectionService.this.mRemoteDevice.getDeviceId());
            }
        }

        public void removeEventListener(IEventListener iEventListener) throws RemoteException {
            synchronized (ConnectionService.this.listenerLock) {
                ConnectionService.this.mListeners.remove(iEventListener);
                ConnectionService.this.createListenerList();
            }
            ConnectionService.this.logger.v("listener removed: pid:" + SystemUtils.getProcessName(ConnectionService.this.getApplicationContext(), getCallingPid()));
        }
    };
    /* access modifiers changed from: private */
    public final List<IEventListener> mListeners = new ArrayList<>();
    protected RemoteDevice mRemoteDevice;
    protected RemoteDeviceBroadcaster[] mRemoteDeviceBroadcasters;
    protected Wire mWire;
    /* access modifiers changed from: private */
    public ConnectionService.PendingConnectHandler pendingConnectHandler;
    private ProxyService proxyService;
    private volatile boolean quitting;
    protected Runnable reconnectRunnable = new Runnable() {
        public void run() {
            if (ConnectionService.this.state == ConnectionService.State.RECONNECTING && ConnectionService.this.mRemoteDevice != null && !ConnectionService.this.mRemoteDevice.isConnecting() && !ConnectionService.this.mRemoteDevice.isConnected()) {
                ConnectionService.this.logger.i("Retrying to connect to " + ConnectionService.this.mRemoteDevice.getDeviceId());
                ConnectionService.this.mRemoteDevice.connect();
            }
        }
    };
    protected boolean serverMode = false;
    protected volatile ConnectionService.ServiceHandler serviceHandler;
    private volatile Looper serviceLooper;
    protected ConnectionService.State state = ConnectionService.State.START;
    private long stateAge = 0;

    private class PendingConnectHandler implements Runnable {
        private Connection connection;
        private RemoteDevice device;

        public PendingConnectHandler(RemoteDevice remoteDevice, Connection connection2) {
            this.device = remoteDevice;
            this.connection = connection2;
        }

        public void run() {
            boolean z = ConnectionService.this.mRemoteDevice != this.device;
            ConnectionService.this.logger.d("Trying to " + (z ? "connect" : "reconnect") + " to " + this.device.getDeviceId().getDisplayName());
            ConnectionService.this.setRemoteDevice(this.device);
            if (this.connection != null) {
                ConnectionService.this.mRemoteDevice.setActiveConnection(this.connection);
            } else if (ConnectionService.this.mRemoteDevice.isConnected()) {
                ConnectionService.this.onDeviceConnected(ConnectionService.this.mRemoteDevice);
            } else {
                ConnectionService.this.setState(z ? ConnectionService.State.CONNECTING : ConnectionService.State.RECONNECTING);
                ConnectionService.this.mRemoteDevice.connect();
            }
            ConnectionService.this.pendingConnectHandler = null;
        }
    }

    protected final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(android.os.Message message) {
            switch (message.what) {
                case 1:
                    ConnectionService.State state = (ConnectionService.State) message.obj;
                    if (state != ConnectionService.this.state) {
                        ConnectionService.this.exitState(ConnectionService.this.state);
                        ConnectionService.this.state = state;
                        ConnectionService.this.enterState(ConnectionService.this.state);
                        return;
                    }
                    return;
                case ConnectionService.EVENT_HEARTBEAT /*2*/:
                    ConnectionService.this.heartBeat();
                    return;
                case ConnectionService.EVENT_DISCONNECT /*3*/:
                    RemoteDevice remoteDevice = (RemoteDevice) message.obj;
                    ConnectionService.this.handleDeviceDisconnect(remoteDevice, Connection.DisconnectCause.values()[message.arg1]);
                    return;
                case ConnectionService.EVENT_RESTART_LISTENERS /*4*/:
                    if (ConnectionService.this.listening) {
                        ConnectionService.this.logger.i("stopping/starting listeners");
                        ConnectionService.this.stopListeners();
                        ConnectionService.this.startListeners();
                    }
                    if (ConnectionService.this.broadcasting) {
                        ConnectionService.this.logger.i("restarting broadcasters");
                        ConnectionService.this.stopBroadcasters();
                        ConnectionService.this.startBroadcasters();
                    }
                    ConnectionService.this.stopProxyService();
                    ConnectionService.this.startProxyService();
                    return;
                default:
                    ConnectionService.this.logger.e("Unknown message: " + message);
            }
        }
    }

    public enum State {
        START,
        IDLE,
        SEARCHING,
        CONNECTING,
        RECONNECTING,
        CONNECTED,
        DISCONNECTING,
        DISCONNECTED,
        PAIRING,
        LISTENING,
        DESTROYED
    }

    private void checkConnection() {
        if (this.mRemoteDevice == null || !this.mRemoteDevice.isConnected()) {
            this.logger.v("dead connection remotedevice=" + (this.mRemoteDevice == null ? "null" : "not null") + " isConnected:" + (this.mRemoteDevice == null ? "false" : Boolean.valueOf(this.mRemoteDevice.isConnected())));
            setState(ConnectionService.State.DISCONNECTING);
        } else if (this.mRemoteDevice.getLinkStatus() == RemoteDevice.LinkStatus.CONNECTED) {
            long elapsedRealtime = SystemClock.elapsedRealtime() - this.lastMessageReceivedTime;
            if (elapsedRealtime > DEAD_CONNECTION_TIME) {
                this.logger.v("dead connection timed out:" + elapsedRealtime);
                if (reconnectAfterDeadConnection()) {
                    this.logger.d("Reconncting after dead connection timed out disconnect");
                    reconnect(REASON_DEAD_CONNECTION);
                    return;
                }
                setState(ConnectionService.State.DISCONNECTING);
            }
        }
    }

    /* access modifiers changed from: private */
    public void createListenerList() {
        this.listenersArray = new IEventListener[this.mListeners.size()];
        this.mListeners.toArray(this.listenersArray);
    }

    private void handleDeviceConnect(RemoteDevice remoteDevice) {
        this.logger.v("onDeviceConnected:remembering device");
        rememberPairedDevice(remoteDevice);
        this.logger.v("onDeviceConnected:heartBeat started");
        this.lastMessageReceivedTime = SystemClock.elapsedRealtime();
        this.logger.d("Connecting with app context: " + getApplicationContext());
        if (!this.mRemoteDevice.startLink()) {
            setState(ConnectionService.State.DISCONNECTING);
        }
    }

    private void handleRemoteDisconnect(DisconnectRequest disconnectRequest) {
        if (this.mRemoteDevice != null) {
            if (disconnectRequest.forget != null && disconnectRequest.forget) {
                forgetPairedDevice(this.mRemoteDevice);
            }
            setState(ConnectionService.State.DISCONNECTING);
        }
    }

    private boolean hasPaired() {
        return this.deviceRegistry.hasPaired();
    }

    private void sendPingIfNeeded() {
        try {
            if (this.mRemoteDevice != null && this.mRemoteDevice.isConnected() && SystemClock.elapsedRealtime() - this.mRemoteDevice.lastMessageSentTime > 4000) {
                this.mRemoteDevice.postEvent(new Ping());
                if (this.serverMode && this.logger.isLoggable(EVENT_HEARTBEAT)) {
                    this.logger.v("NAVDY-PACKET [H2P-Outgoing-Event] Ping");
                }
            }
        } catch (Throwable th) {
            this.logger.e(th);
        }
    }

    public void connect(ConnectionInfo connectionInfo) {
        setActiveDevice(new RemoteDevice(this, connectionInfo, this.inProcess));
    }

    public abstract ProxyService createProxyService() throws IOException;

    public abstract ProxyService createObdService() throws IOException;

    /* access modifiers changed from: protected */
    public void enterState(ConnectionService.State state2) {
        int i = IDLE_TIMEOUT;
        this.logger.d("Entering state:" + state2);
        if (state2 == ConnectionService.State.DESTROYED) {
            if (this.listening) {
                stopListeners();
            }
            if (this.broadcasting) {
                stopBroadcasters();
            }
            this.serviceHandler.removeCallbacksAndMessages(null);
            return;
        }
        this.stateAge = 0;
        switch (state2) {
            case PAIRING /*2*/:
                startBroadcasters();
                startListeners();
                forwardEventLocally(new ConnectionStatus(ConnectionStatus.Status.CONNECTION_PAIRING, ""));
                i = PAIRING_TIMEOUT;
                break;
            case LISTENING /*3*/:
                if (isPromiscuous()) {
                    startBroadcasters();
                }
                startListeners();
                if (this.serverMode) {
                    i = RETRY_CONNECTING_AFTER_FAILURE_TIMEOUT;
                    break;
                }
                break;
            case CONNECTING /*4*/:
                i = CONNECT_TIMEOUT;
                break;
            case RECONNECTING /*5*/:
                i = RECONNECT_TIMEOUT;
                break;
            case DISCONNECTING:
                if (this.mRemoteDevice != null) {
                    this.mRemoteDevice.disconnect();
                    break;
                }
                break;
            case CONNECTED:
                stopListeners();
                stopBroadcasters();
                handleDeviceConnect(this.mRemoteDevice);
                break;
        }
        this.serviceHandler.removeMessages(EVENT_HEARTBEAT);
        this.serviceHandler.sendEmptyMessageDelayed(EVENT_HEARTBEAT, (long) i);
    }

    /* access modifiers changed from: protected */
    public void exitState(ConnectionService.State state2) {
        this.logger.d("Exiting state:" + state2);
        switch (state2) {
            case PAIRING /*2*/:
                if (!isPromiscuous()) {
                    stopBroadcasters();
                    return;
                }
                return;
            case RECONNECTING /*5*/:
                this.serviceHandler.removeCallbacks(this.reconnectRunnable);
                return;
            case START:
                startProxyService();
                return;
            default:
        }
    }

    /* access modifiers changed from: protected */
    public void forgetPairedDevice(BluetoothDevice bluetoothDevice) {
        this.deviceRegistry.removePairedConnection(bluetoothDevice);
        forwardEventLocally(DEVICES_CHANGED_EVENT);
    }

    /* access modifiers changed from: protected */
    public void forgetPairedDevice(RemoteDevice remoteDevice) {
        this.deviceRegistry.removePairedConnection(remoteDevice.getActiveConnectionInfo());
        forwardEventLocally(DEVICES_CHANGED_EVENT);
    }

    /* access modifiers changed from: protected */
    public void forwardEventLocally(Message message) {
        forwardEventLocally(NavdyEventUtil.eventFromMessage(message).toByteArray());
    }

    /* access modifiers changed from: protected */
    public void forwardEventLocally(byte[] bArr) {
        try {
            this.mEventSource.postEvent(bArr);
        } catch (Throwable th) {
            this.logger.e(th);
        }
    }

    public abstract ConnectionListener[] getConnectionListeners(Context context);

    public abstract RemoteDeviceBroadcaster[] getRemoteDeviceBroadcasters();

    /* access modifiers changed from: protected */
    public void handleDeviceDisconnect(RemoteDevice remoteDevice, Connection.DisconnectCause disconnectCause) {
        this.logger.v("device disconnect:" + remoteDevice + " cause:" + disconnectCause);
        if (remoteDevice != null) {
            remoteDevice.stopLink();
        }
        if (this.mRemoteDevice != remoteDevice) {
            return;
        }
        if ((disconnectCause == Connection.DisconnectCause.ABORTED || this.forceReconnect) && this.serverMode) {
            this.forceReconnect = false;
            new ConnectionService.PendingConnectHandler(this.mRemoteDevice, null).run();
            return;
        }
        setRemoteDevice(null);
        setState(ConnectionService.State.IDLE);
    }

    /* access modifiers changed from: protected */
    public void heartBeat() {
        if (this.stateAge % 10 == 0) {
            this.logger.d("Heartbeat: in state:" + this.state);
        }
        this.stateAge++;
        int i = IDLE_TIMEOUT;
        switch (this.state) {
            case IDLE:
                if (this.pendingConnectHandler == null) {
                    if (hasPaired() || !this.serverMode) {
                        if (!hasPaired()) {
                            i = 60000;
                            break;
                        } else {
                            setState(ConnectionService.State.LISTENING);
                            break;
                        }
                    } else {
                        setState(ConnectionService.State.PAIRING);
                        break;
                    }
                } else {
                    this.pendingConnectHandler.run();
                    break;
                }
            case PAIRING /*2*/:
                i = PAIRING_TIMEOUT;
                break;
            case LISTENING /*3*/:
                if (this.serverMode) {
                    if (this.mRemoteDevice == null) {
                        if (!needAutoSearch()) {
                            i = 60000;
                            break;
                        } else {
                            setState(ConnectionService.State.SEARCHING);
                            break;
                        }
                    } else {
                        setState(ConnectionService.State.RECONNECTING);
                        this.serviceHandler.post(this.reconnectRunnable);
                        break;
                    }
                } else {
                    i = 60000;
                    break;
                }
            case CONNECTING /*4*/:
            case RECONNECTING /*5*/:
                setState(ConnectionService.State.IDLE);
                break;
            case CONNECTED:
                i = HEARTBEAT_INTERVAL;
                sendPingIfNeeded();
                checkConnection();
                break;
        }
        this.serviceHandler.removeMessages(EVENT_HEARTBEAT);
        this.serviceHandler.sendEmptyMessageDelayed(EVENT_HEARTBEAT, (long) i);
    }

    public boolean isConnected() {
        return this.mRemoteDevice != null && this.mRemoteDevice.isConnected();
    }

    public boolean isPromiscuous() {
        return false;
    }

    public boolean needAutoSearch() {
        return true;
    }

    public IBinder onBind(Intent intent) {
        if (IEventSource.class.getName().equals(intent.getAction())) {
            this.logger.d("returning IEventSource API endpoint");
            return this.mEventSource;
        }
        this.logger.w("invalid action:" + intent.getAction());
        return null;
    }

    public void onConnected(ConnectionListener connectionListener, Connection connection) {
        this.logger.v("listener connected");
        setActiveDevice(new RemoteDevice(getApplicationContext(), connection.getConnectionInfo(), this.inProcess), connection);
    }

    public void onConnectionFailed(ConnectionListener connectionListener) {
        this.logger.e("onConnectionFailed:restart listeners");
        setState(ConnectionService.State.IDLE);
    }

    public void onCreate() {
        this.logger.e("ConnectionService created: mainthread:" + Looper.getMainLooper().getThread().getId());
        this.mWire = new Wire(Ext_NavdyEvent.class);
        HandlerThread handlerThread = new HandlerThread("ConnectionService");
        handlerThread.start();
        this.serviceLooper = handlerThread.getLooper();
        this.serviceHandler = new ConnectionService.ServiceHandler(this.serviceLooper);
        registerReceiver(this.bluetoothReceiver, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        this.deviceRegistry = RemoteDeviceRegistry.getInstance(this);
    }

    public void onDestroy() {
        this.logger.v("ConnectionService destroyed.  mwhahahaha.");
        try {
            setState(ConnectionService.State.DESTROYED);
            unregisterReceiver(this.bluetoothReceiver);
            this.serviceLooper.quitSafely();
            stopProxyService();
        } catch (Throwable th) {
            this.logger.e(th);
        }
    }

    public void onDeviceConnectFailure(RemoteDevice remoteDevice, Connection.ConnectionFailureCause connectionFailureCause) {
        if (this.state == ConnectionService.State.RECONNECTING) {
            this.logger.i("onDeviceConnectFailure - retrying");
            this.serviceHandler.postDelayed(this.reconnectRunnable, 2000);
            return;
        }
        this.logger.i("onDeviceConnectFailure - switching to idle state");
        if (remoteDevice != this.mRemoteDevice || this.serverMode) {
            this.logger.d("Not clearing the remote device, to attempt reconnect");
        } else {
            setRemoteDevice(null);
        }
        setState(ConnectionService.State.IDLE);
    }

    public void onDeviceConnected(RemoteDevice remoteDevice) {
        setState(ConnectionService.State.CONNECTED);
    }

    public void onDeviceConnecting(RemoteDevice remoteDevice) {
    }

    public void onDeviceDisconnected(RemoteDevice remoteDevice, Connection.DisconnectCause disconnectCause) {
        this.serviceHandler.sendMessage(this.serviceHandler.obtainMessage(EVENT_DISCONNECT, disconnectCause.ordinal(), -1, remoteDevice));
    }

    public void onNavdyEventReceived(RemoteDevice remoteDevice, NavdyEvent navdyEvent) {
        this.lastMessageReceivedTime = SystemClock.elapsedRealtime();
        if (navdyEvent.type == NavdyEvent.MessageType.DisconnectRequest) {
            handleRemoteDisconnect(navdyEvent.getExtension(Ext_NavdyEvent.disconnectRequest));
        }
    }

    public void onNavdyEventReceived(RemoteDevice remoteDevice, byte[] bArr) {
        this.lastMessageReceivedTime = SystemClock.elapsedRealtime();
        NavdyEvent.MessageType eventType = WireUtil.getEventType(bArr);
        if (this.logger.isLoggable(EVENT_HEARTBEAT)) {
            this.logger.v((this.serverMode ? "NAVDY-PACKET [P2H" : "NAVDY-PACKET [H2P") + "-Event] " + (eventType != null ? eventType.name() : "UNKNOWN") + " size:" + bArr.length);
        }
        if (eventType != null && eventType != NavdyEvent.MessageType.Ping) {
            if (eventType == NavdyEvent.MessageType.DisconnectRequest) {
                try {
                    handleRemoteDisconnect(this.mWire.parseFrom(bArr, NavdyEvent.class).getExtension(Ext_NavdyEvent.disconnectRequest));
                } catch (Throwable th) {
                    this.logger.e("Failed to parse event", th);
                }
            } else if (!processEvent(bArr, eventType)) {
                if (eventType == NavdyEvent.MessageType.DeviceInfo) {
                    try {
                        if (this.mRemoteDevice != null && this.mRemoteDevice.isConnected()) {
                            DeviceInfo build = new DeviceInfo.Builder((DeviceInfo)NavdyEventUtil.messageFromEvent(this.mWire.parseFrom(bArr, NavdyEvent.class))).deviceId(this.mRemoteDevice.getDeviceId().toString()).build();
                            this.mRemoteDevice.setDeviceInfo(build);
                            this.logger.v("set remote device info");
                            forwardEventLocally(NavdyEventUtil.eventFromMessage(build).toByteArray());
                        }
                    } catch (Throwable th2) {
                        this.logger.e("Failed to parse deviceinfo", th2);
                    }
                } else {
                    forwardEventLocally(bArr);
                }
            }
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent != null && BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            if (this.state == ConnectionService.State.CONNECTED) {
                this.logger.i("BT enabled, disconnecting since app likely crashed");
                setState(ConnectionService.State.DISCONNECTING);
            } else {
                this.logger.i("BT enabled, switching to IDLE state");
                setState(ConnectionService.State.IDLE);
            }
        }
        return START_STICKY;
    }

    public void onStartFailure(ConnectionListener connectionListener) {
        this.logger.e("failed to start listening:" + connectionListener);
    }

    public void onStarted(ConnectionListener connectionListener) {
        this.logger.v("started listening");
    }

    public void onStopped(ConnectionListener connectionListener) {
        this.logger.v("stopped listening:" + connectionListener);
    }

    /* access modifiers changed from: protected */
    public boolean processEvent(byte[] bArr, NavdyEvent.MessageType messageType) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean processLocalEvent(byte[] bArr, NavdyEvent.MessageType messageType) {
        return false;
    }

    public void reconnect(String str) {
        synchronized (this) {
            if (this.mRemoteDevice != null) {
                this.forceReconnect = true;
                setState(ConnectionService.State.DISCONNECTING);
            }
        }
    }

    public boolean reconnectAfterDeadConnection() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void rememberPairedDevice(RemoteDevice remoteDevice) {
        this.deviceRegistry.addPairedConnection(remoteDevice.getActiveConnectionInfo());
        forwardEventLocally(DEVICES_CHANGED_EVENT);
    }

    /* access modifiers changed from: protected */
    public void sendEventsOnLocalConnect() {
    }

    public void sendMessage(Message message) {
        if (isConnected()) {
            this.mRemoteDevice.postEvent(message);
        }
    }

    public boolean setActiveDevice(RemoteDevice remoteDevice) {
        return setActiveDevice(remoteDevice, null);
    }

    public boolean setActiveDevice(RemoteDevice remoteDevice, Connection connection) {
        synchronized (this) {
            if (this.mRemoteDevice != null) {
                if (this.mRemoteDevice.isConnected() || this.mRemoteDevice.isConnecting()) {
                    setState(ConnectionService.State.DISCONNECTING);
                } else {
                    setRemoteDevice(null);
                }
            }
            if (remoteDevice != null) {
                ConnectionService.PendingConnectHandler pendingConnectHandler2 = new ConnectionService.PendingConnectHandler(remoteDevice, connection);
                if (this.mRemoteDevice == null) {
                    pendingConnectHandler2.run();
                } else {
                    this.pendingConnectHandler = pendingConnectHandler2;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void setBandwidthLevel(int i) {
        if (this.state == ConnectionService.State.CONNECTED && this.mRemoteDevice != null) {
            if (i == 0 || i == 1) {
                this.mRemoteDevice.setLinkBandwidthLevel(i);
                LinkPropertiesChanged.Builder builder = new LinkPropertiesChanged.Builder();
                builder.bandwidthLevel(i);
                forwardEventLocally(builder.build());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setRemoteDevice(RemoteDevice remoteDevice) {
        synchronized (this) {
            if (remoteDevice != this.mRemoteDevice) {
                if (this.mRemoteDevice != null) {
                    this.mRemoteDevice.removeListener(this);
                }
                this.mRemoteDevice = remoteDevice;
                if (this.mRemoteDevice != null) {
                    this.mRemoteDevice.addListener(this);
                }
            }
        }
    }

    public void setState(ConnectionService.State state2) {
        if (!this.quitting && this.state != state2) {
            if (state2 == ConnectionService.State.DESTROYED) {
                this.quitting = true;
            }
            this.serviceHandler.sendMessage(this.serviceHandler.obtainMessage(1, state2));
        }
    }

    /* access modifiers changed from: protected */
    public void startBroadcasters() {
        synchronized (this.connectionLock) {
            this.broadcasting = true;
            if (this.mRemoteDeviceBroadcasters == null) {
                this.mRemoteDeviceBroadcasters = getRemoteDeviceBroadcasters();
            }
            for (RemoteDeviceBroadcaster broadcaster: this.mRemoteDeviceBroadcasters) {
                this.logger.v("starting connection broadcaster:" + broadcaster.getClass().getName());
                try {
                    broadcaster.start();
                } catch (Throwable th) {
                    this.logger.e(th);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void startListeners() {
        int i = 0;
        synchronized (this.connectionLock) {
            this.listening = true;
            if (this.mConnectionListeners == null) {
                this.logger.v("initializing connector listener");
                this.mConnectionListeners = getConnectionListeners(getApplicationContext());
                for (ConnectionListener addListener : this.mConnectionListeners) {
                    addListener.addListener(this);
                }
            }
            for (ConnectionListener listener: this.mConnectionListeners) {
                this.logger.v("starting connection listener:" + listener);
                try {
                    listener.start();
                } catch (Throwable th) {
                    this.logger.e(th);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void startProxyService() {
        if (this.proxyService == null || !this.proxyService.isAlive()) {
            this.logger.v(SystemUtils.getProcessName(getApplicationContext(), android.os.Process.myPid()) + ": start service for proxy");
            try {
                this.proxyService = createProxyService();
                this.proxyService.start();
            } catch (Exception e) {
                this.logger.e("Failed to start proxy service", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void stopBroadcasters() {
        int i = 0;
        this.logger.v("stopping all broadcasters");
        synchronized (this.connectionLock) {
            this.broadcasting = false;
            if (this.mRemoteDeviceBroadcasters != null) {
                for (RemoteDeviceBroadcaster broadcaster: this.mRemoteDeviceBroadcasters) {
                    this.logger.v("stopping connection broadcaster:" + broadcaster.getClass().getName());
                    try {
                        broadcaster.stop();
                    } catch (Throwable th) {
                        this.logger.e(th);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void stopListeners() {
        int i = 0;
        synchronized (this.connectionLock) {
            this.listening = false;
            if (this.mConnectionListeners != null) {
                for (ConnectionListener listener: this.mConnectionListeners) {
                    this.logger.v("stopping:" + listener);
                    try {
                        listener.stop();
                    } catch (Throwable th) {
                        this.logger.e(th);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void stopProxyService() {
        if (this.proxyService != null) {
            this.proxyService.cancel();
            this.proxyService = null;
        }
    }
}