package com.navdy.service.library.network.http;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;

public interface IHttpManager {
    OkHttpClient getClient();

    Builder getClientCopy();
}
