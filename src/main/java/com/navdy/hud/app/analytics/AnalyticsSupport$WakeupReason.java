package com.navdy.hud.app.analytics;


public enum AnalyticsSupport$WakeupReason {
    POWERBUTTON(0, "Power_Button"),
    VOLTAGE_SPIKE(1, "Voltage_Spike"),
    DIAL(2, "Dial"),
    PHONE(3, "Phone");

    private int value;
    final public String attr;

    AnalyticsSupport$WakeupReason(int value) {
        this.value = value;
        this.attr = null;
    }
    public int getValue() {
        return value;
    }

    AnalyticsSupport$WakeupReason(int value, String s0) {
        this.value = value;
        this.attr = s0;
    }
}

//final public class AnalyticsSupport$WakeupReason extends Enum {
//    final private static com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason[] $VALUES;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason DIAL;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason PHONE;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason POWERBUTTON;
//    final public static com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason VOLTAGE_SPIKE;
//    final public String attr;
//    
//    static {
//        POWERBUTTON = new com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason("POWERBUTTON", 0, "Power_Button");
//        VOLTAGE_SPIKE = new com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason("VOLTAGE_SPIKE", 1, "Voltage_Spike");
//        DIAL = new com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason("DIAL", 2, "Dial");
//        PHONE = new com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason("PHONE", 3, "Phone");
//        com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason[] a = new com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason[4];
//        a[0] = POWERBUTTON;
//        a[1] = VOLTAGE_SPIKE;
//        a[2] = DIAL;
//        a[3] = PHONE;
//        $VALUES = a;
//    }
//    
//    private AnalyticsSupport$WakeupReason(String s, int i, String s0) {
//        super(s, i);
//        this.attr = s0;
//    }
//    
//    public static com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason valueOf(String s) {
//        return (com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason)Enum.valueOf(com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason.class, s);
//    }
//    
//    public static com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason[] values() {
//        return $VALUES.clone();
//    }
//}
//