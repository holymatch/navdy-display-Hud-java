package com.navdy.hud.app.device;

import com.navdy.hud.app.util.SerialNumber;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ProjectorBrightness {
    private static final int BRIGHTNESS_SCALE_VALUES_COUNT = 256;
    private static final String DATA_FILE = "/sys/dlpc/RGB_Brightness";
    private static final Logger sLogger = new Logger(ProjectorBrightness.class);
    private static final List<String> AUTO_BRIGHTNESS_MAPPING_FILES = new ArrayList<String>() {
        {
            add("/device/etc/calibration/dlpc_brightness");
            add("/system/etc/calibration/dlpc_brightness");
        }
    };
    private static final List<String> AUTO_BRIGHTNESS_MAPPING = readAutoBrightnessMapping();

    public static int getValue() throws IOException {
        try {
            return getValue(IOUtils.convertFileToString(DATA_FILE).trim());
        } catch (NumberFormatException e) {
            sLogger.e("Exception while reading auto-brightness value", e);
            throw new IOException("Corrupted data in file with auto-brightness value");
        }
    }

    protected static int getValue(String str) {
        int indexOf = AUTO_BRIGHTNESS_MAPPING.indexOf(str);
        if (indexOf >= 0) {
            return indexOf;
        }
        sLogger.e("Cannot get brightness value for auto-brightness string '" + str + "'");
        return 0;
    }

    public static void init() {
    }

    private static List<String> readAutoBrightnessMapping() {
        FileInputStream fileInputStream = null;
        BufferedReader bufferedReader = null;
        String str = SerialNumber.instance.revisionCode.equals("3") ? "_v3" : "_v4";
        Iterator it = AUTO_BRIGHTNESS_MAPPING_FILES.iterator();
        List<String> list = Collections.emptyList();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            String str2 = ((String) it.next()) + str;
            list = new ArrayList<>(BRIGHTNESS_SCALE_VALUES_COUNT);
            try {
                fileInputStream = new FileInputStream(str2);
                bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
                for (String readLine = bufferedReader.readLine(); readLine != null && list.size() < BRIGHTNESS_SCALE_VALUES_COUNT; readLine = bufferedReader.readLine()) {
                    list.add(readLine.trim());
                }
                if (list.size() >= BRIGHTNESS_SCALE_VALUES_COUNT) {
                    sLogger.i("Successfully read mapping from file " + str2);
                    IOUtils.closeStream(bufferedReader);
                    IOUtils.closeStream(fileInputStream);
                    break;
                }
                sLogger.w("File " + str2 + " not complete - skipping it");

            } catch (FileNotFoundException e5) {
                sLogger.w(str2 + " not found - checking next file");
            } catch (IOException e6) {
                sLogger.e("Cannot read auto-brightness mapping file " + str2 + " : " + e6.getMessage());

            } finally {
                IOUtils.closeStream(bufferedReader);
                IOUtils.closeStream(fileInputStream);
            }
        }
        if (list.size() >= BRIGHTNESS_SCALE_VALUES_COUNT) {
            return list;
        }
        sLogger.e("No auto-brightness mapping file found");
        return Collections.emptyList();
    }
}
