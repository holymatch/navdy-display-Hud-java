package com.navdy.hud.app.ui.component.mainmenu;

import android.graphics.Shader;

import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.hud.app.framework.glance.GlanceConstants;
import android.view.View;
import com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex;
import java.util.Iterator;
import java.util.ArrayList;
import kotlin.jvm.internal.TypeIntrinsics;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder;
import android.support.v4.content.ContextCompat;
import com.navdy.hud.app.HudApplication;
//import kotlin.jvm.internal.DefaultConstructorMarker;
import com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent;
import com.navdy.hud.app.framework.contacts.Contact;
import java.util.List;
import android.content.res.Resources;
import com.navdy.service.library.log.Logger;
import android.content.Context;
import com.navdy.hud.app.ui.component.vlist.VerticalList;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 B2\u00020\u0001:\u0003ABCBU\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0001\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n\u0012\b\u0010\f\u001a\u0004\u0018\u00010\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\u0006\u0010\u0011\u001a\u00020\u0012¢\u0006\u0002\u0010\u0013J(\u0010\u001b\u001a\u0004\u0018\u00010\u00012\b\u0010\u0006\u001a\u0004\u0018\u00010\u00012\b\u0010\u001c\u001a\u0004\u0018\u00010\r2\b\u0010\u001d\u001a\u0004\u0018\u00010\rH\u0016J\b\u0010\u001e\u001a\u00020\u000fH\u0016J\u0010\u0010\u001f\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0017H\u0016J\u0012\u0010 \u001a\u0004\u0018\u00010\u00182\u0006\u0010!\u001a\u00020\u000fH\u0016J\n\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\b\u0010$\u001a\u00020%H\u0016J\b\u0010&\u001a\u00020'H\u0016J\b\u0010(\u001a\u00020'H\u0016J\u0018\u0010)\u001a\u00020'2\u0006\u0010*\u001a\u00020\u000f2\u0006\u0010!\u001a\u00020\u000fH\u0016J.\u0010+\u001a\u00020,2\b\u0010-\u001a\u0004\u0018\u00010\u00182\b\u0010.\u001a\u0004\u0018\u00010/2\u0006\u0010!\u001a\u00020\u000f2\b\u00100\u001a\u0004\u0018\u000101H\u0016J\b\u00102\u001a\u00020,H\u0016J\b\u00103\u001a\u00020,H\u0016J\u0012\u00104\u001a\u00020,2\b\u00105\u001a\u0004\u0018\u000106H\u0016J\b\u00107\u001a\u00020,H\u0016J\u0012\u00108\u001a\u00020,2\b\u00109\u001a\u0004\u0018\u00010:H\u0016J\u0010\u0010;\u001a\u00020'2\u0006\u00105\u001a\u000206H\u0016J\u0010\u0010<\u001a\u00020,2\u0006\u0010*\u001a\u00020\u000fH\u0016J\u0010\u0010=\u001a\u00020,2\u0006\u0010>\u001a\u00020\u000fH\u0016J\b\u0010?\u001a\u00020,H\u0016J\b\u0010@\u001a\u00020,H\u0016R\u000e\u0010\u0014\u001a\u00020\u000fX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u000fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0017X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006D" }, d2 = { "Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu;", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;", "vscrollComponent", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;", "presenter", "Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;", "parent", "mode", "Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;", "contacts", "", "Lcom/navdy/hud/app/framework/contacts/Contact;", "notificationId", "", "iconResource", "", "iconColor", "callback", "Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;", "(Lcom/navdy/hud/app/ui/component/vmenu/VerticalMenuComponent;Lcom/navdy/hud/app/ui/component/mainmenu/MainMenuScreen2$Presenter;Lcom/navdy/hud/app/ui/component/mainmenu/IMenu;Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;Ljava/util/List;Ljava/lang/String;IILcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;)V", "backSelection", "backSelectionId", "cachedList", "", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "messagePickerMenu", "Lcom/navdy/hud/app/ui/component/mainmenu/MessagePickerMenu;", "getChildMenu", "args", "path", "getInitialSelection", "getItems", "getModelfromPos", "pos", "getScrollIndex", "Lcom/navdy/hud/app/ui/component/vmenu/VerticalFastScrollIndex;", "getType", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$Menu;", "isBindCallsEnabled", "", "isFirstItemEmpty", "isItemClickable", "id", "onBindToView", "", "model", "view", "Landroid/view/View;", "state", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ModelState;", "onFastScrollEnd", "onFastScrollStart", "onItemSelected", "selection", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$ItemSelectionState;", "onScrollIdle", "onUnload", "level", "Lcom/navdy/hud/app/ui/component/mainmenu/IMenu$MenuLevel;", "selectItem", "setBackSelectionId", "setBackSelectionPos", "n", "setSelectedIcon", "showToolTip", "Callback", "Companion", "Mode", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
public final class NumberPickerMenu implements IMenu
{
    public static final Companion Companion;
    private static final int SELECTION_ANIMATION_DELAY = 1000;
    private static final VerticalList.Model back;
    private static final int baseMessageId = 100;
    private static final Context context;
    private static final int fluctuatorColor;
    private static final Logger logger;
    private static final Resources resources;
    private int backSelection;
    private int backSelectionId;
    private List<VerticalList.Model> cachedList;
    protected final Callback callback;
    protected final List<Contact> contacts;
    private final int iconColor;
    private final int iconResource;
    private MessagePickerMenu messagePickerMenu;
    private final Mode mode;
    private final String notificationId;
    private final IMenu parent;
    private final MainMenuScreen2.Presenter presenter;
    private final VerticalMenuComponent vscrollComponent;
    
    static {
        Companion = new Companion();
        logger = new Logger(NumberPickerMenu.Companion.getClass().getSimpleName());
        context = HudApplication.getAppContext();
        resources = NumberPickerMenu.Companion.getContext().getResources();
        fluctuatorColor = ContextCompat.getColor(NumberPickerMenu.Companion.getContext(), R.color.mm_back);
        back = IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, NumberPickerMenu.Companion.getFluctuatorColor(), MainMenu.bkColorUnselected, NumberPickerMenu.Companion.getFluctuatorColor(), NumberPickerMenu.Companion.getResources().getString(R.string.back), null);
    }
    
    public NumberPickerMenu(@NotNull final VerticalMenuComponent vscrollComponent, @NotNull final MainMenuScreen2.Presenter presenter, @NotNull final IMenu parent, @NotNull final Mode mode, @NotNull final List<? extends Contact> contacts, @Nullable final String notificationId, final int iconResource, final int iconColor, @NotNull final Callback callback) {
        Intrinsics.checkParameterIsNotNull(vscrollComponent, "vscrollComponent");
        Intrinsics.checkParameterIsNotNull(presenter, "presenter");
        Intrinsics.checkParameterIsNotNull(parent, "parent");
        Intrinsics.checkParameterIsNotNull(mode, "mode");
        Intrinsics.checkParameterIsNotNull(contacts, "contacts");
        Intrinsics.checkParameterIsNotNull(callback, "callback");
        this.vscrollComponent = vscrollComponent;
        this.presenter = presenter;
        this.parent = parent;
        this.mode = mode;
        this.contacts = (List<Contact>)contacts;
        this.notificationId = notificationId;
        this.iconResource = iconResource;
        this.iconColor = iconColor;
        this.callback = callback;
    }
    
    public static final /* synthetic */ VerticalList.Model access$getBack$cp() {
        return NumberPickerMenu.back;
    }
    
    public static final /* synthetic */ int access$getBaseMessageId$cp() {
        return NumberPickerMenu.baseMessageId;
    }
    
    public static final /* synthetic */ Context access$getContext$cp() {
        return NumberPickerMenu.context;
    }
    
    public static final /* synthetic */ int access$getFluctuatorColor$cp() {
        return NumberPickerMenu.fluctuatorColor;
    }
    
    @NotNull
    public static final /* synthetic */ Logger access$getLogger$cp() {
        return NumberPickerMenu.logger;
    }
    
    public static final /* synthetic */ Resources access$getResources$cp() {
        return NumberPickerMenu.resources;
    }
    
    public static final /* synthetic */ int access$getSELECTION_ANIMATION_DELAY$cp() {
        return NumberPickerMenu.SELECTION_ANIMATION_DELAY;
    }
    
    @Nullable
    @Override
    public IMenu getChildMenu(@Nullable final IMenu menu, @Nullable final String s, @Nullable final String s2) {
        return null;
    }
    
    @Override
    public int getInitialSelection() {
        return 1;
    }
    
    @Nullable
    @Override
    public List<VerticalList.Model> getItems() {
        List<VerticalList.Model> mutableList;
        if (this.cachedList != null) {
            final List<VerticalList.Model> cachedList = this.cachedList;
            if (cachedList == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableList<com.navdy.hud.app.ui.component.vlist.VerticalList.Model>");
            }
            mutableList = (List<VerticalList.Model>)TypeIntrinsics.asMutableList(cachedList);
        }
        else {
            final ArrayList<VerticalList.Model> list = new ArrayList<VerticalList.Model>();
            list.add(NumberPickerMenu.Companion.getBack());
            int n = 0;
            for (final Contact contact : this.contacts) {
                String s;
                if (contact.numberTypeStr != null) {
                    s = contact.numberTypeStr;
                }
                else {
                    s = contact.formattedNumber;
                }
                String formattedNumber;
                if (contact.numberTypeStr != null) {
                    formattedNumber = contact.formattedNumber;
                }
                else {
                    formattedNumber = null;
                }
                list.add(IconBkColorViewHolder.buildModel(NumberPickerMenu.Companion.getBaseMessageId() + n, this.iconResource, this.iconColor, MainMenu.bkColorUnselected, this.iconColor, s, formattedNumber));
                ++n;
            }
            this.cachedList = list;
            mutableList = list;
        }
        return mutableList;
    }
    
    @Nullable
    @Override
    public VerticalList.Model getModelfromPos(final int n) {
        final List<VerticalList.Model> cachedList = this.cachedList;
        VerticalList.Model model;
        if (cachedList != null && cachedList.size() > n) {
            model = cachedList.get(n);
        }
        else {
            model = null;
        }
        return model;
    }
    
    @Nullable
    @Override
    public VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    @NotNull
    @Override
    public Menu getType() {
        return Menu.NUMBER_PICKER;
    }
    
    @Override
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    @Override
    public boolean isFirstItemEmpty() {
        return false;
    }
    
    @Override
    public boolean isItemClickable(final int n, final int n2) {
        return true;
    }
    
    @Override
    public void onBindToView(@Nullable final VerticalList.Model model, @Nullable final View view, final int n, @Nullable final VerticalList.ModelState modelState) {
    }
    
    @Override
    public void onFastScrollEnd() {
    }
    
    @Override
    public void onFastScrollStart() {
    }
    
    @Override
    public void onItemSelected(@Nullable final VerticalList.ItemSelectionState itemSelectionState) {
    }
    
    @Override
    public void onScrollIdle() {
    }
    
    @Override
    public void onUnload(@Nullable final MenuLevel menuLevel) {
    }
    
    @Override
    public boolean selectItem(@NotNull VerticalList.ItemSelectionState selection) {
        Intrinsics.checkParameterIsNotNull(selection, "selection");
        Companion.getLogger().v("select id:" + selection.id + " pos:" + selection.pos);
        switch (selection.id) {
            case R.id.menu_back:
                AnalyticsSupport.recordMenuSelection("back");
                this.presenter.loadMenu(this.parent, MenuLevel.BACK_TO_PARENT, this.backSelection, this.backSelectionId);
                this.backSelectionId = 0;
                break;
            default:
                int contactIndex = selection.id - Companion.getBaseMessageId();
                switch (this.mode) {
                    case MESSAGE:
                        if (this.messagePickerMenu == null) {
                            VerticalMenuComponent verticalMenuComponent = this.vscrollComponent;
                            MainMenuScreen2.Presenter presenter = this.presenter;
                            IMenu iMenu = this;
                            List cannedMessages = GlanceConstants.getCannedMessages();
                            Intrinsics.checkExpressionValueIsNotNull(cannedMessages, "GlanceConstants.getCannedMessages()");
                            this.messagePickerMenu = new MessagePickerMenu(verticalMenuComponent, presenter, iMenu, cannedMessages, (Contact) this.contacts.get(contactIndex), this.notificationId);
                        }
                        this.presenter.loadMenu(this.messagePickerMenu, MenuLevel.SUB_LEVEL, selection.pos, 0);
                        break;
                    default:
                        this.presenter.performSelectionAnimation(new NumberPickerMenu$selectItem$1(this, contactIndex), Companion.getSELECTION_ANIMATION_DELAY());
                        break;
                }
        }
        return true;
    }
    
    @Override
    public void setBackSelectionId(final int backSelectionId) {
        this.backSelectionId = backSelectionId;
    }
    
    @Override
    public void setBackSelectionPos(final int backSelection) {
        this.backSelection = backSelection;
    }
    
    @Override
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(this.iconResource, this.iconColor, null, 1.0f);
        this.vscrollComponent.selectedText.setText((CharSequence)NumberPickerMenu.Companion.getResources().getString(R.string.which_number));
    }
    
    @Override
    public void showToolTip() {
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006" }, d2 = { "Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Callback;", "", "selected", "", "contact", "Lcom/navdy/hud/app/framework/contacts/Contact;", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public interface Callback
    {
        void selected(@NotNull final Contact p0);
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001c\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\b0\bX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\u00020\u0004X\u0082D¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u0006R\u001c\u0010\u000e\u001a\n \t*\u0004\u0018\u00010\u000f0\u000fX\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u00020\u0004X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0006R\u0014\u0010\u0014\u001a\u00020\u0015X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u001c\u0010\u0018\u001a\n \t*\u0004\u0018\u00010\u00190\u0019X\u0082\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001b¨\u0006\u001c" }, d2 = { "Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Companion;", "", "()V", "SELECTION_ANIMATION_DELAY", "", "getSELECTION_ANIMATION_DELAY", "()I", "back", "Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "kotlin.jvm.PlatformType", "getBack", "()Lcom/navdy/hud/app/ui/component/vlist/VerticalList$Model;", "baseMessageId", "getBaseMessageId", "context", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", "fluctuatorColor", "getFluctuatorColor", "logger", "Lcom/navdy/service/library/log/Logger;", "getLogger", "()Lcom/navdy/service/library/log/Logger;", "resources", "Landroid/content/res/Resources;", "getResources", "()Landroid/content/res/Resources;", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public static final class Companion
    {
        private final VerticalList.Model getBack() {
            return NumberPickerMenu.access$getBack$cp();
        }
        
        private final int getBaseMessageId() {
            return NumberPickerMenu.access$getBaseMessageId$cp();
        }
        
        private final Context getContext() {
            return NumberPickerMenu.access$getContext$cp();
        }
        
        private final int getFluctuatorColor() {
            return NumberPickerMenu.access$getFluctuatorColor$cp();
        }
        
        private final Logger getLogger() {
            return NumberPickerMenu.access$getLogger$cp();
        }
        
        private final Resources getResources() {
            return NumberPickerMenu.access$getResources$cp();
        }
        
        private final int getSELECTION_ANIMATION_DELAY() {
            return NumberPickerMenu.access$getSELECTION_ANIMATION_DELAY$cp();
        }
    }
    
    @Metadata(bv = { 1, 0, 1 }, d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006" }, d2 = { "Lcom/navdy/hud/app/ui/component/mainmenu/NumberPickerMenu$Mode;", "", "(Ljava/lang/String;I)V", "MESSAGE", "CALL", "SHARE_TRIP", "app_hudRelease" }, k = 1, mv = { 1, 1, 6 })
    public enum Mode {
        MESSAGE(0),
        CALL(1),
        SHARE_TRIP(2);

        private int value;
        Mode(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
