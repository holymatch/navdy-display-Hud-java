package com.navdy.hud.app.ui.component.image;

import android.graphics.Canvas;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Paint;
import android.widget.ImageView;

public class ColorImageView extends ImageView
{
    private int color;
    private Paint paint;
    
    public ColorImageView(final Context context) {
        this(context, null, 0);
    }
    
    public ColorImageView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public ColorImageView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.init();
    }
    
    private void init() {
        (this.paint = new Paint()).setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        final int width = this.getWidth();
        final int height = this.getHeight();
        canvas.drawColor(0);
        this.paint.setColor(this.color);
        canvas.drawCircle((float)(width / 2), (float)(height / 2), (float)(width / 2), this.paint);
    }
    
    public void setColor(final int color) {
        this.color = color;
        this.invalidate();
    }
}
