package com.navdy.hud.app.debug;

import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import com.navdy.service.library.log.Logger;

public class BusLeakDetector
{
    private static final String EMPTY = "";
    private static final Logger sLogger;
    private static final BusLeakDetector singleton;
    private HashMap<Class, Integer> registrationMap;
    
    static {
        sLogger = new Logger(BusLeakDetector.class);
        singleton = new BusLeakDetector();
    }
    
    public BusLeakDetector() {
        this.registrationMap = new HashMap<Class, Integer>(100);
        BusLeakDetector.sLogger.v("::ctor::");
    }
    
    public static BusLeakDetector getInstance() {
        return BusLeakDetector.singleton;
    }
    
    public void addReference(final Class clazz) {
        synchronized (this) {
            final Integer n = this.registrationMap.get(clazz);
            if (n == null) {
                this.registrationMap.put(clazz, 1);
            }
            else {
                this.registrationMap.put(clazz, n + 1);
            }
        }
    }

    public synchronized void printReferences() {
        sLogger.v("::printReferences: [" + this.registrationMap.size() + "]");
        for (Map.Entry<Class, Integer> entry : this.registrationMap.entrySet()) {
            String suffix;
            int count = ((Integer) entry.getValue()).intValue();
            switch (count) {
                case 1:
                    suffix = "";
                    break;
                case 2:
                    suffix = " **";
                    break;
                case 3:
                    suffix = " ****";
                    break;
                case 4:
                    suffix = " ********";
                    break;
                default:
                    suffix = " ****************";
                    break;
            }
            sLogger.v(((Class) entry.getKey()).getName() + " = " + count + suffix);
        }
    }
    // monitorexit(this)
    
    public void removeReference(final Class clazz) {
        synchronized (this) {
            final Integer n = this.registrationMap.get(clazz);
            if (n != null) {
                final Integer value = n - 1;
                if (value == 0) {
                    this.registrationMap.remove(clazz);
                }
                else {
                    this.registrationMap.put(clazz, value);
                }
            }
        }
    }
}
