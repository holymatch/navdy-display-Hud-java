package com.navdy.hud.app.maps.here;

import static com.navdy.hud.app.maps.here.HereMapAnimator.*;


class HereMapAnimator$3 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapAnimator this$0;
    private final double val$zoom;
    
    HereMapAnimator$3(com.navdy.hud.app.maps.here.HereMapAnimator a, double d) {
        super();
        this.this$0 = a;
        this.val$zoom = d;
    }
    
    public void run() {

        long j = (access$1000(this.this$0) <= 0L) ? 0L : android.os.SystemClock.elapsedRealtime() - access$1000(this.this$0);
        boolean b = access$1100(this.this$0, this.val$zoom);
        {
            if (b) {
                access$1002(this.this$0, android.os.SystemClock.elapsedRealtime());
                synchronized (access$1200()) {
                    access$1302(this.this$0, j);
                    access$1402(this.this$0, this.val$zoom);
                    /*monexit(a)*/
                    ;
                }
            } else {
                access$400().v("filtering out spurious zoom: " + this.val$zoom);
            }
        }

    }
}
