package com.navdy.hud.app.maps.util;
import com.navdy.hud.app.R;

public class DestinationUtil {
    public DestinationUtil() {
    }
    
    public static java.util.List convert(android.content.Context a, java.util.List a0, int i, int i0, boolean b) {
        com.navdy.hud.app.maps.here.HereLocationFixManager a1 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getLocationFixManager();
        com.here.android.mpa.common.GeoCoordinate a2 = null;
        if (a1 != null) {
            a2 = a1.getLastGeoCoordinate();
        }
        com.navdy.service.library.events.location.LatLong a3 = null;
        if (a2 != null) {
            a3 = new com.navdy.service.library.events.location.LatLong(Double.valueOf(a2.getLatitude()), Double.valueOf(a2.getLongitude()));
        }
        java.util.ArrayList a4 = new java.util.ArrayList(a0.size());
        Object a5 = a0;
        int i1 = 0;
        while(i1 < ((java.util.List)a5).size()) {
            double d = 0.0;
            double d0 = 0.0;
            float f = 0.0f;
            double d1 = 0.0;
            double d2 = 0.0;
            int i2 = 0;
            int i3 = 0;
            com.navdy.service.library.events.destination.Destination a6 = (com.navdy.service.library.events.destination.Destination)((java.util.List)a5).get(i1);
            label1: if (a6.navigation_position == null) {
                d = 0.0;
                d0 = 0.0;
                f = -1f;
            } else {
                d = a6.navigation_position.latitude.doubleValue();
                d0 = a6.navigation_position.longitude.doubleValue();
                double d3 = a6.navigation_position.latitude.doubleValue();
                int i4 = (d3 > 0.0) ? 1 : (d3 == 0.0) ? 0 : -1;
                label0: {
                    if (i4 == 0) {
                        break label0;
                    }
                    if (a6.navigation_position.longitude.doubleValue() == 0.0) {
                        break label0;
                    }
                    f = com.navdy.hud.app.maps.util.MapUtils.distanceBetween(a6.navigation_position, a3);
                    break label1;
                }
                if (a6.display_position == null) {
                    f = -1f;
                } else if (a6.display_position.latitude.doubleValue() == 0.0) {
                    f = -1f;
                } else {
                    f = (a6.display_position.longitude.doubleValue() == 0.0) ? -1f : com.navdy.hud.app.maps.util.MapUtils.distanceBetween(a6.display_position, a3);
                }
            }
            int i5 = (f > -1f) ? 1 : (f == -1f) ? 0 : -1;
            String s = null;
            String s0 = null;
            if (i5 != 0) {
                com.navdy.hud.app.maps.util.DistanceConverter$Distance a7 = new com.navdy.hud.app.maps.util.DistanceConverter$Distance();
                com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit(), f, a7);
                s0 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getFormattedDistance(a7.value, a7.unit, b);
                int i6 = s0.indexOf(" ");
                s = null;
                if (i6 != -1) {
                    s = new StringBuilder().append("<b>").append(s0.substring(0, i6)).append("</b>").append(s0.substring(i6)).toString();
                }
            }
            if (a6.display_position == null) {
                d1 = 0.0;
                d2 = 0.0;
            } else {
                d1 = a6.display_position.latitude.doubleValue();
                d2 = a6.display_position.longitude.doubleValue();
            }
            com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.PlaceTypeResourceHolder a8 = com.navdy.hud.app.ui.component.destination.DestinationPickerScreen.getPlaceTypeHolder(a6.place_type);
            if (a8 == null) {
                i2 = i;
                i3 = R.drawable.icon_mm_places_2;
            } else {
                i3 = a8.iconRes;
                i2 = android.support.v4.content.ContextCompat.getColor(a, a8.colorRes);
            }
            com.navdy.hud.app.ui.component.destination.DestinationParcelable a9 = new com.navdy.hud.app.ui.component.destination.DestinationParcelable(0, a6.destination_title, a6.destination_subtitle, false, s, true, a6.full_address, d, d0, d1, d2, i3, 0, i2, i0, com.navdy.hud.app.ui.component.destination.DestinationParcelable.DestinationType.DESTINATION, a6.place_type);
            a9.setIdentifier(a6.identifier);
            a9.setPlaceId(a6.place_id);
            a9.distanceStr = s0;
            a9.setContacts(com.navdy.hud.app.framework.contacts.ContactUtil.fromContacts(a6.contacts));
            a9.setPhoneNumbers(com.navdy.hud.app.framework.contacts.ContactUtil.fromPhoneNumbers(a6.phoneNumbers));
            ((java.util.List)a4).add(a9);
            i1 = i1 + 1;
        }
        return (java.util.List)a4;
    }
}
