package com.navdy.hud.app.maps.here;
import android.util.Log;

import static com.navdy.hud.app.maps.here.HereMapsManager.CUSTOM_TRACKING_MAP_SCHEME;
import static com.navdy.hud.app.maps.here.HereMapsManager.sLogger;


class HereMapsManager$4 implements android.content.SharedPreferences.OnSharedPreferenceChangeListener {
    final com.navdy.hud.app.maps.here.HereMapsManager this$0;

    HereMapsManager$4(com.navdy.hud.app.maps.here.HereMapsManager a) {
        super();
        this.this$0 = a;
    }

    public void onSharedPreferenceChanged(android.content.SharedPreferences a, String s) {
        sLogger.d("onSharedPreferenceChanged: " + s);
        boolean b = this.this$0.isInitialized();
        label0: {
            Throwable a0;
            label1: {
                Throwable a1;
                if (b) {
                    int i;
                    switch(s.hashCode()) {
                        case 134000933: {
                            i = (s.equals("map.zoom")) ? 1 : -1;
                            break;
                        }
                        case 133816335: {
                            i = (s.equals("map.tilt")) ? 0 : -1;
                            break;
                        }
                        case -285821321: {
                            i = (s.equals("map.scheme")) ? 2 : -1;
                            break;
                        }
                        case -1094616929: {
                            i = (s.equals("map.animation.mode")) ? 3 : -1;
                            break;
                        }
                        default: {
                            i = -1;
                        }
                    }
                    switch(i) {
                        case 2: {
                            final HereMapsManager$4 a2 = this;
                            com.navdy.service.library.task.TaskManager.getInstance().execute(new Runnable() {
                                final HereMapsManager$4 this$1 = a2;

                                public void run() {
                                    String currentScheme = this.this$1.this$0.mapController.getMapScheme();
                                    String trackingScheme = this.this$1.this$0.getTrackingMapScheme();
                                    sLogger.v("Scheme: " + currentScheme + " -> " + trackingScheme);
                                    if (!currentScheme.equals(trackingScheme)) {
                                        this.this$1.this$0.mapController.setMapScheme(trackingScheme);
                                    }
                                }
                            }, 2);
                            break label0;
                        }
                        case 1: {
                            String s0 = a.getString(s, null);
                            if (s0 != null) {
                                float f;
                                try {
                                    f = Float.parseFloat(s0);
                                } catch(Throwable a2) {
                                    a1 = a2;
                                    break;
                                }
                                sLogger.w("onSharedPreferenceChanged:zoom:" + f);
                                this.this$0.mapController.setZoomLevel((double)f);
                                break label0;
                            } else {
                                sLogger.v("onSharedPreferenceChanged:no zoom");
                                break label0;
                            }
                        }
                        case 0: {
                            String s1 = a.getString(s, null);
                            if (s1 != null) {
                                float f0;
                                try {
                                    f0 = Float.parseFloat(s1);
                                } catch(Throwable a3) {
                                    a0 = a3;
                                    break label1;
                                }
                                sLogger.w("onSharedPreferenceChanged:tilt:" + f0);
                                this.this$0.mapController.setTilt(f0);
                                break label0;
                            } else {
                                sLogger.v("onSharedPreferenceChanged:no tilt");
                                break label0;
                            }
                        }
                        default: {
                            break label0;
                        }
                    }
                } else {
                    sLogger.w("onSharedPreferenceChanged: map engine not intialized:" + s);
                    break label0;
                }
                sLogger.e("onSharedPreferenceChanged:zoom", a1);
                break label0;
            }
            sLogger.e("onSharedPreferenceChanged:tilt", a0);
        }
    }
}
