package com.navdy.hud.app.maps.here;

class HereRouteViaGenerator$DistanceContainer implements Comparable {
    int index;
    long order;
    double val;
    String via;
    
    HereRouteViaGenerator$DistanceContainer(double d, int i, long j, String s) {
        this.val = d;
        this.index = i;
        this.order = j;
        this.via = s;
    }
    
    public int compareTo(com.navdy.hud.app.maps.here.HereRouteViaGenerator$DistanceContainer a) {
        int i = this.index - a.index;
        if (i == 0) {
            i = (int)a.val - (int)this.val;
            if (i == 0) {
                i = (int)(this.order - a.order);
            }
        }
        return i;
    }
    
    public int compareTo(Object a) {
        return this.compareTo((com.navdy.hud.app.maps.here.HereRouteViaGenerator$DistanceContainer)a);
    }
}
