package com.navdy.hud.app.maps.here;

public class HereMapController {
    final private static java.util.concurrent.atomic.AtomicInteger counter;
    final private static com.navdy.service.library.log.Logger sLogger;
    final protected com.here.android.mpa.mapping.Map map;
    final private android.os.Handler mapBkHandler;
    private int mapRouteCount;
    private volatile com.navdy.hud.app.maps.here.HereMapController$State state;

    static {
        counter = new java.util.concurrent.atomic.AtomicInteger(1);
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereMapController.class);
    }
    
    public HereMapController(com.here.android.mpa.mapping.Map a, com.navdy.hud.app.maps.here.HereMapController$State a0) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        this.map = a;
        this.state = a0;
        android.os.HandlerThread a1 = new android.os.HandlerThread("HereMapController-" + counter.getAndIncrement());
        a1.start();
        this.mapBkHandler = new android.os.Handler(a1.getLooper());
    }

    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static void access$200(com.navdy.hud.app.maps.here.HereMapController a, com.here.android.mpa.mapping.MapObject a0) {
        a.addMapObjectInternal(a0);
    }
    
    static void access$300(com.navdy.hud.app.maps.here.HereMapController a, com.here.android.mpa.mapping.MapObject a0) {
        a.removeMapObjectInternal(a0);
    }
    
    private void addMapObjectInternal(com.here.android.mpa.mapping.MapObject a) {
        if (a instanceof com.here.android.mpa.mapping.MapRoute) {
            com.here.android.mpa.mapping.MapRoute a0 = (com.here.android.mpa.mapping.MapRoute)a;
            com.here.android.mpa.routing.Route a2 = a0.getRoute();
            this.mapRouteCount = this.mapRouteCount + 1;
            sLogger.v("[map-route-added] maproute=" + System.identityHashCode(a0) + " route=" + System.identityHashCode(a2) + " count=" + this.mapRouteCount);
            a = a0;
        }
        this.map.addMapObject(a);
    }
    
    private void removeMapObjectInternal(com.here.android.mpa.mapping.MapObject a) {
        if (a instanceof com.here.android.mpa.mapping.MapRoute) {
            this.mapRouteCount = this.mapRouteCount - 1;
            com.here.android.mpa.mapping.MapRoute a0 = (com.here.android.mpa.mapping.MapRoute)a;
            com.here.android.mpa.routing.Route a2 = a0.getRoute();
            sLogger.v("[map-route-removed] maproute=" + System.identityHashCode(a0) + " route=" + System.identityHashCode(a2) + " count=" + this.mapRouteCount);
            a = a0;
        }
        if (a != null) {
            this.map.removeMapObject(a);
        }
    }
    
    public void addMapObject(com.here.android.mpa.mapping.MapObject a) {
        this.mapBkHandler.post(new HereMapController$5(this, a));
    }
    
    public void addMapObjects(java.util.List a) {
        this.mapBkHandler.post(new HereMapController$6(this, a));
    }
    
    public void addTransformListener(com.here.android.mpa.mapping.Map.OnTransformListener a) {
        this.mapBkHandler.post(new HereMapController$10(this, a));
    }
    
    public void execute(Runnable a) {
        this.mapBkHandler.post(a);
    }
    
    public void execute(Runnable a, com.navdy.hud.app.maps.here.HereMapController$Callback a0) {
        this.mapBkHandler.post(new HereMapController$14(this, a, a0));
    }
    
    public com.here.android.mpa.common.GeoCoordinate getCenter() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.map.getCenter();
    }
    
    public com.here.android.mpa.mapping.Map getMap() {
        return this.map;
    }
    
    public String getMapScheme() {
        return this.map.getMapScheme();
    }
    
    public float getOrientation() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.map.getOrientation();
    }
    
    public com.here.android.mpa.mapping.PositionIndicator getPositionIndicator() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.map.getPositionIndicator();
    }
    
    public com.navdy.hud.app.maps.here.HereMapController$State getState() {
        return this.state;
    }
    
    public float getTilt() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.map.getTilt();
    }
    
    public double getZoomLevel() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.map.getZoomLevel();
    }
    
    public com.here.android.mpa.mapping.Map.PixelResult projectToPixel(com.here.android.mpa.common.GeoCoordinate a) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        return this.map.projectToPixel(a);
    }
    
    public void removeMapObject(com.here.android.mpa.mapping.MapObject a) {
        this.mapBkHandler.post(new HereMapController$7(this, a));
    }
    
    public void removeMapObjects(java.util.List a) {
        this.mapBkHandler.post(new HereMapController$8(this, a));
    }
    
    public void removeTransformListener(com.here.android.mpa.mapping.Map.OnTransformListener a) {
        this.mapBkHandler.post(new HereMapController$11(this, a));
    }
    
    public void setCenter(com.here.android.mpa.common.GeoCoordinate a, com.here.android.mpa.mapping.Map.Animation a0, double d, float f, float f0) {
        if (this.state == com.navdy.hud.app.maps.here.HereMapController$State.AR_MODE) {
            this.mapBkHandler.post(new HereMapController$3(this, a, a0, d, f, f0));
        }
    }
    
    public void setCenterForState(com.navdy.hud.app.maps.here.HereMapController$State a, com.here.android.mpa.common.GeoCoordinate a0, com.here.android.mpa.mapping.Map.Animation a1, double d, float f, float f0) {
        if (this.state == a) {
            this.mapBkHandler.post(new HereMapController$4(this, a0, a1, d, f, f0));
        }
    }
    
    public void setMapScheme(final String s) {
        final HereMapController a = this;
        this.mapBkHandler.post(new Runnable() {
            final HereMapController this$0 = a;

            public void run() {
                this.this$0.map.setMapScheme(s);
            }
        });
    }
    
    public void setState(com.navdy.hud.app.maps.here.HereMapController$State a) {
        this.state = a;
        sLogger.v("state:" + a);
    }
    
    public void setTilt(float f) {
        this.mapBkHandler.post(new HereMapController$1(this, f));
    }
    
    public void setTrafficInfoVisible(boolean b) {
        this.mapBkHandler.post(new HereMapController$15(this, b));
    }
    
    public void setTransformCenter(android.graphics.PointF a) {
        this.mapBkHandler.post(new HereMapController$9(this, a));
    }
    
    public void setZoomLevel(double d) {
        this.mapBkHandler.post(new HereMapController$2(this, d));
    }
    
    public void zoomTo(com.here.android.mpa.common.GeoBoundingBox a, com.here.android.mpa.common.ViewRect a0, com.here.android.mpa.mapping.Map.Animation a1, float f) {
        this.mapBkHandler.post(new HereMapController$12(this, a, a0, a1, f));
    }
}
