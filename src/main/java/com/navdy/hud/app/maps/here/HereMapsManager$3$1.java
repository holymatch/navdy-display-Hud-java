package com.navdy.hud.app.maps.here;
import android.os.SystemClock;

import static com.navdy.hud.app.maps.here.HereMapsManager.sLogger;


class HereMapsManager$3$1 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager$3 this$1;
    private final com.here.android.mpa.common.GeoPosition val$geoPosition;
    private final boolean val$isMapMatched;
    private final com.here.android.mpa.common.PositioningManager.LocationMethod val$locationMethod;
    
    HereMapsManager$3$1(com.navdy.hud.app.maps.here.HereMapsManager$3 a, com.here.android.mpa.common.GeoPosition a0, com.here.android.mpa.common.PositioningManager.LocationMethod a1, boolean b) {
        super();
        this.this$1 = a;
        this.val$geoPosition = a0;
        this.val$locationMethod = a1;
        this.val$isMapMatched = b;
    }
    
    public void run() {
        label0: {
            try {
                boolean b = this.val$geoPosition instanceof com.here.android.mpa.common.MatchedGeoPosition;
                label2: {
                    if (b) {
                        break label2;
                    }
                    com.here.android.mpa.common.GeoCoordinate a = this.val$geoPosition.getCoordinate();
                    sLogger.v("position not map-matched lat:" + a.getLatitude() + " lng:" + a.getLongitude() + " speed:" + this.val$geoPosition.getSpeed());
                    break label0;
                }
                if (this.this$1.this$0.extrapolationOn) {
                    if (com.navdy.hud.app.maps.here.HereMapUtil.isInTunnel(this.this$1.this$0.positioningManager.getRoadElement())) {
                        this.this$1.this$0.sendExtrapolationEvent();
                    } else {
                        sLogger.i("TUNNEL extrapolation off");
                        this.this$1.this$0.extrapolationOn = false;
                        this.this$1.this$0.sendExtrapolationEvent();
                    }
                }
            } catch(Throwable a0) {
                HereMapsManager.sLogger.e(a0);
            }
            com.here.android.mpa.common.GeoPosition a1 = this.this$1.this$0.lastGeoPosition;
            label1: {
                if (a1 == null) {
                    break label1;
                }
                long j = android.os.SystemClock.elapsedRealtime() - this.this$1.this$0.lastGeoPositionTime;
                if (j >= 500L) {
                    break label1;
                }
                if (!com.navdy.hud.app.maps.here.HereMapUtil.isCoordinateEqual(this.this$1.this$0.lastGeoPosition.getCoordinate(), this.val$geoPosition.getCoordinate())) {
                    break label1;
                }
                if (!HereMapsManager.sLogger.isLoggable(2)) {
                    break label0;
                }
                HereMapsManager.sLogger.v("GEO-Here same pos as last:" + j);
                break label0;
            }
            this.this$1.this$0.lastGeoPosition = this.val$geoPosition;
            long j = SystemClock.elapsedRealtime();
            this.this$1.this$0.lastGeoPositionTime = j;
            this.this$1.this$0.hereMapAnimator.setGeoPosition(this.val$geoPosition);
            this.this$1.this$0.hereLocationFixManager.onHerePositionUpdated(this.val$locationMethod, this.val$geoPosition, this.val$isMapMatched);
            com.navdy.hud.app.maps.here.HereMapCameraManager.getInstance().onGeoPositionChange(this.val$geoPosition);
            if (this.this$1.this$0.speedManager.getObdSpeed() == -1 && android.os.SystemClock.elapsedRealtime() - this.this$1.this$0.hereLocationFixManager.getLastLocationTime() >= 2000L && this.val$geoPosition instanceof com.here.android.mpa.common.MatchedGeoPosition && ((com.here.android.mpa.common.MatchedGeoPosition)this.val$geoPosition).isExtrapolated() && this.this$1.this$0.speedManager.setGpsSpeed((float)this.val$geoPosition.getSpeed(), android.os.SystemClock.elapsedRealtimeNanos() / 1000000L)) {
                this.this$1.this$0.bus.post(new com.navdy.hud.app.maps.MapEvents$GPSSpeedEvent());
            }
            this.this$1.this$0.bus.post(this.val$geoPosition);
            if (HereMapsManager.sLogger.isLoggable(2)) {
                com.here.android.mpa.common.GeoCoordinate a2 = this.val$geoPosition.getCoordinate();
                HereMapsManager.sLogger.v("GEO-Here speed-mps[" + this.val$geoPosition.getSpeed() + "] " + "] lat=[" + a2.getLatitude() + "] lon=[" + a2.getLongitude() + "] provider=[" + this.val$locationMethod.name() + "] timestamp:[" + this.val$geoPosition.getTimestamp().getTime() + "]");
            }
        }
    }
}
