package com.navdy.hud.app.maps;

public class NavSessionPreferences {
    public boolean spokenTurnByTurn;
    
    public NavSessionPreferences(com.navdy.service.library.events.preferences.NavigationPreferences a) {
        this.setDefault(a);
    }
    
    public void setDefault(com.navdy.service.library.events.preferences.NavigationPreferences a) {
        this.spokenTurnByTurn = Boolean.TRUE.equals(a.spokenTurnByTurn);
    }
}
