package com.navdy.hud.app.maps.here;

public class HereTrafficETATracker extends com.here.android.mpa.guidance.NavigationManager.NavigationManagerEventListener {
    final private static long ETA_THRESHOLD;
    final private static long INVALID = -1L;
    final private static long REFRESH_ETA_INTERVAL_MILLIS;
    private long baseEta;
    final private com.squareup.otto.Bus bus;
    private Runnable dismissEtaRunnable;
    final private android.os.Handler handler;
    final private com.navdy.hud.app.maps.here.HereNavController navController;
    private Runnable navigationModeChangeBkRunnable;
    private Runnable refreshEtaBkRunnable;
    private Runnable refreshEtaRunnable;
    final private com.navdy.service.library.log.Logger sLogger;
    
    static {
        REFRESH_ETA_INTERVAL_MILLIS = java.util.concurrent.TimeUnit.SECONDS.toMillis(60L);
        ETA_THRESHOLD = java.util.concurrent.TimeUnit.MINUTES.toMillis(10L);
    }
    
    public HereTrafficETATracker(com.navdy.hud.app.maps.here.HereNavController a, com.squareup.otto.Bus a0) {
        this.sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereTrafficETATracker.class);
        this.refreshEtaRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereTrafficETATracker$1(this);
        this.refreshEtaBkRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereTrafficETATracker$2(this);
        this.dismissEtaRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereTrafficETATracker$3(this);
        this.navigationModeChangeBkRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereTrafficETATracker$4(this);
        this.navController = a;
        this.bus = a0;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.sLogger.v("ctor");
    }
    
    static Runnable access$000(com.navdy.hud.app.maps.here.HereTrafficETATracker a) {
        return a.refreshEtaBkRunnable;
    }
    
    static void access$100(com.navdy.hud.app.maps.here.HereTrafficETATracker a) {
        a.etaUpdate();
    }
    
    static void access$200(com.navdy.hud.app.maps.here.HereTrafficETATracker a) {
        a.sendTrafficDelayDismissEvent();
    }
    
    static com.navdy.hud.app.maps.here.HereNavController access$300(com.navdy.hud.app.maps.here.HereTrafficETATracker a) {
        return a.navController;
    }
    
    static long access$400(com.navdy.hud.app.maps.here.HereTrafficETATracker a) {
        return a.baseEta;
    }
    
    static long access$402(com.navdy.hud.app.maps.here.HereTrafficETATracker a, long j) {
        a.baseEta = j;
        return j;
    }
    
    static com.navdy.service.library.log.Logger access$500(com.navdy.hud.app.maps.here.HereTrafficETATracker a) {
        return a.sLogger;
    }
    
    static void access$600(com.navdy.hud.app.maps.here.HereTrafficETATracker a) {
        a.refreshEta();
    }
    
    static Runnable access$700(com.navdy.hud.app.maps.here.HereTrafficETATracker a) {
        return a.refreshEtaRunnable;
    }
    
    static android.os.Handler access$800(com.navdy.hud.app.maps.here.HereTrafficETATracker a) {
        return a.handler;
    }
    
    static Runnable access$900(com.navdy.hud.app.maps.here.HereTrafficETATracker a) {
        return a.dismissEtaRunnable;
    }
    
    private void etaUpdate() {
        this.sLogger.v("etaUpdate");
        if (this.baseEta != -1L) {
            java.util.Date a = this.navController.getEta(true, com.here.android.mpa.routing.Route.TrafficPenaltyMode.OPTIMAL);
            if (com.navdy.hud.app.maps.here.HereMapUtil.isValidEtaDate(a)) {
                long j = a.getTime();
                long j0 = j - this.baseEta;
                if (j0 <= ETA_THRESHOLD) {
                    this.sLogger.v(new StringBuilder().append("ETA threshold ok:").append(j0).toString());
                } else {
                    this.sLogger.v(new StringBuilder().append("ETA threshold delay, triggering delay:").append(j0).toString());
                    this.baseEta = j;
                    this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficDelayEvent(j0 / 1000L));
                    this.handler.removeCallbacks(this.dismissEtaRunnable);
                    this.handler.postDelayed(this.dismissEtaRunnable, ETA_THRESHOLD);
                }
            }
            this.refreshEta();
        } else {
            this.sLogger.v("base ETA is invalid");
        }
    }
    
    private void refreshEta() {
        this.sLogger.v("posting refresh ETA in 60 sec...");
        this.handler.removeCallbacks(this.refreshEtaRunnable);
        this.handler.postDelayed(this.refreshEtaRunnable, REFRESH_ETA_INTERVAL_MILLIS);
    }
    
    private void sendTrafficDelayDismissEvent() {
        this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficDelayDismissEvent());
    }
    
    public void onNavigationModeChanged() {
        com.navdy.service.library.task.TaskManager.getInstance().execute(this.navigationModeChangeBkRunnable, 3);
    }
    
    public void onRouteUpdated(com.here.android.mpa.routing.Route a) {
        this.sendTrafficDelayDismissEvent();
    }
}
