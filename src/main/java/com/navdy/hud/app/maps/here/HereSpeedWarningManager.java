package com.navdy.hud.app.maps.here;
import com.navdy.hud.app.R;
import com.squareup.otto.Subscribe;

public class HereSpeedWarningManager {
    final private static int BUFFER_SPEED = 7;
    final private static long DISCARD_INTERVAL;
    final private static int PERIODIC_CHECK_INTERVAL = 5000;
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private Runnable checkSpeedRunnable;
    private android.content.Context context;
    private android.os.Handler handler;
    private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    private long lastSpeedWarningTime;
    private long lastUpdate;
    private com.navdy.hud.app.maps.MapsEventHandler mapsEventHandler;
    private Runnable periodicCheckRunnable;
    private Runnable periodicRunnable;
    private boolean registered;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    private volatile boolean speedWarningOn;
    private String tag;
    private int warningSpeed;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereSpeedWarningManager.class);
        DISCARD_INTERVAL = java.util.concurrent.TimeUnit.SECONDS.toMillis(10L);
    }
    
    HereSpeedWarningManager(String s, com.squareup.otto.Bus a, com.navdy.hud.app.maps.MapsEventHandler a0, com.navdy.hud.app.maps.here.HereNavigationManager a1) {
        this.context = com.navdy.hud.app.HudApplication.getAppContext();
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        this.periodicRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereSpeedWarningManager$1(this);
        this.periodicCheckRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereSpeedWarningManager$2(this);
        this.checkSpeedRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereSpeedWarningManager$3(this);
        this.tag = s;
        this.bus = a;
        this.mapsEventHandler = a0;
        this.hereNavigationManager = a1;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
    }
    
    static Runnable access$000(com.navdy.hud.app.maps.here.HereSpeedWarningManager a) {
        return a.periodicCheckRunnable;
    }
    
    static boolean access$100(com.navdy.hud.app.maps.here.HereSpeedWarningManager a) {
        return a.registered;
    }
    
    static long access$200(com.navdy.hud.app.maps.here.HereSpeedWarningManager a) {
        return a.lastUpdate;
    }
    
    static void access$300(com.navdy.hud.app.maps.here.HereSpeedWarningManager a) {
        a.checkSpeed();
    }
    
    static com.navdy.service.library.log.Logger access$400() {
        return sLogger;
    }
    
    static Runnable access$500(com.navdy.hud.app.maps.here.HereSpeedWarningManager a) {
        return a.periodicRunnable;
    }
    
    static android.os.Handler access$600(com.navdy.hud.app.maps.here.HereSpeedWarningManager a) {
        return a.handler;
    }
    
    private void checkSpeed() {
        float f = (float)this.speedManager.getCurrentSpeed();
        label0: if (f != -1f) {
            com.here.android.mpa.common.RoadElement a = com.navdy.hud.app.maps.here.HereMapUtil.getCurrentRoadElement();
            if (a != null) {
                this.lastUpdate = android.os.SystemClock.elapsedRealtime();
                int i = com.navdy.hud.app.manager.SpeedManager.convert((double)a.getSpeedLimit(), com.navdy.hud.app.manager.SpeedManager.SpeedUnit.METERS_PER_SECOND, this.speedManager.getSpeedUnit());
                if (i > 0) {
                    if (f > (float)i) {
                        boolean b = this.speedWarningOn;
                        label1: {
                            if (!b) {
                                break label1;
                            }
                            if (this.warningSpeed == -1) {
                                break label1;
                            }
                            if (this.warningSpeed == -1) {
                                break label0;
                            }
                            if (!(f > (float)this.warningSpeed)) {
                                break label0;
                            }
                        }
                        this.onSpeedExceeded(a.getRoadName(), i, (int)f);
                    } else if (this.speedWarningOn) {
                        this.onSpeedExceededEnd(a.getRoadName(), i, (int)f);
                    }
                } else if (this.speedWarningOn) {
                    sLogger.v("clear speed warning, no speedlimit info avail");
                    this.onSpeedExceededEnd(a.getRoadName(), i, (int)f);
                }
            }
        } else if (this.speedWarningOn) {
            sLogger.i("lost speed info while warning on");
            this.onSpeedExceededEnd((String)null, 0, 0);
        }
    }
    
    private void onSpeedExceeded(String s, int i, int i0) {
        this.bus.post(com.navdy.hud.app.maps.here.HereNavigationManager.SPEED_EXCEEDED);
        if (!this.speedWarningOn) {
            this.warningSpeed = -1;
        }
        boolean b = this.speedWarningOn;
        this.speedWarningOn = true;
        com.navdy.service.library.events.preferences.NavigationPreferences a = this.mapsEventHandler.getNavigationPreferences();
        boolean b0 = Boolean.TRUE.equals(a.spokenSpeedLimitWarnings);
        label0: {
            label1: {
                label2: {
                    if (!b0) {
                        break label2;
                    }
                    if (!com.navdy.hud.app.framework.phonecall.CallUtils.isPhoneCallInProgress()) {
                        break label1;
                    }
                }
                if (b) {
                    break label0;
                }
                sLogger.w(new StringBuilder().append(this.tag).append(" speed exceeded:").append(s).append(" limit=").append(i).append(" current=").append(i0).toString());
                break label0;
            }
            com.navdy.hud.app.manager.SpeedManager.SpeedUnit a0 = this.speedManager.getSpeedUnit();
            int i1 = (this.warningSpeed != -1) ? this.warningSpeed + this.warningSpeed / 10 : i + 7;
            if (i0 < i1) {
                if (!b) {
                    sLogger.w(new StringBuilder().append(this.tag).append(" speed exceeded current[").append(i0).append("] threshold[").append(i1).append("] allowed[").append(i).append("] ").append(a0.name()).toString());
                }
            } else {
                sLogger.w(new StringBuilder().append(this.tag).append(" speed exceeded-notify current[").append(i0).append("] threshold[").append(i1).append("] allowed[").append(i).append("] ").append(a0.name()).toString());
                this.warningSpeed = i0;
                switch(a0) {
                    case METERS_PER_SECOND: {
                        String dummy = this.hereNavigationManager.TTS_METERS;
                        break;
                    }
                    case KILOMETERS_PER_HOUR: {
                        String dummy0 = this.hereNavigationManager.TTS_KMS;
                        break;
                    }
                    case MILES_PER_HOUR: {
                        String dummy1 = this.hereNavigationManager.TTS_MILES;
                        break;
                    }
                }
                long j = android.os.SystemClock.elapsedRealtime();
                long j0 = j - this.lastSpeedWarningTime;
                if (j0 > DISCARD_INTERVAL) {
                    android.content.Context a1 = this.context;
                    Object[] a2 = new Object[1];
                    a2[0] = Integer.valueOf(i);
                    String s0 = a1.getString(R.string.tts_speed_warning, a2);
                    this.lastSpeedWarningTime = j;
                    com.navdy.hud.app.framework.voice.TTSUtils.sendSpeechRequest(s0, com.navdy.service.library.events.audio.SpeechRequest.Category.SPEECH_SPEED_WARNING, (String)null);
                } else {
                    sLogger.v(new StringBuilder().append("don't post speed tts:").append(j0).toString());
                }
            }
        }
    }
    
    private void onSpeedExceededEnd(String s, int i, int i0) {
        this.speedWarningOn = false;
        this.warningSpeed = -1;
        this.bus.post(com.navdy.hud.app.maps.here.HereNavigationManager.SPEED_NORMAL);
        sLogger.i(new StringBuilder().append(this.tag).append(" speed normal:").append(s).append(" limit=").append(i).append(" current=").append(i0).toString());
    }
    
    @Subscribe
    public void onPositionUpdated(com.here.android.mpa.common.GeoPosition a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute(this.checkSpeedRunnable, 12);
    }
    
    public void start() {
        if (!this.registered) {
            this.registered = true;
            this.bus.register(this);
            this.handler.removeCallbacks(this.periodicRunnable);
            this.handler.postDelayed(this.periodicRunnable, 5000L);
            sLogger.v("started");
        }
    }
    
    public void stop() {
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
            this.handler.removeCallbacks(this.periodicRunnable);
            sLogger.v("stopped");
        }
    }
}
