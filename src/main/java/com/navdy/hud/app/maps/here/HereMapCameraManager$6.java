package com.navdy.hud.app.maps.here;

class HereMapCameraManager$6 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapCameraManager this$0;
    final Runnable val$endAction;
    
    HereMapCameraManager$6(com.navdy.hud.app.maps.here.HereMapCameraManager a, Runnable a0) {
        super();
        this.this$0 = a;
        this.val$endAction = a0;
    }
    
    public void run() {
        if (this.val$endAction != null) {
            com.navdy.hud.app.maps.here.HereMapCameraManager.access$100(this.this$0).post(this.val$endAction);
        }
    }
}
