package com.navdy.hud.app.maps.here;

import com.navdy.service.library.events.navigation.NavigationRouteResult;

abstract public interface HereRouteCalculator$RouteCalculatorListener {
    abstract public void error(com.here.android.mpa.routing.RoutingError arg, Throwable arg0);
    
    
    abstract public void postSuccess(java.util.ArrayList<NavigationRouteResult> arg);
    
    
    abstract public void preSuccess();
    
    
    abstract public void progress(int arg);
}
