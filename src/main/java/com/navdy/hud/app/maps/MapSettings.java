package com.navdy.hud.app.maps;

public class MapSettings {
    final private static String CUSTOM_ANIMATION = "persist.sys.custom_animation";
    final private static String DEBUG_HERE_LOC = "persist.sys.dbg_here_loc";
    final private static int DEFAULT_MAP_FPS = 10;
    final private static String FULL_CUSTOM_ANIMATION = "persist.sys.full_animation";
    final private static String GENERATE_ROUTE_ICONS = "persist.sys.route_icons";
    final private static String GLYMPSE_DURATION = "persist.sys.glympse_dur";
    final private static String LANE_GUIDANCE_ENABLED = "persist.sys.lane_info_enabled";
    final private static String MAP_FPS = "persist.sys.map_fps";
    final private static String NO_TURN_TEXT_IN_TBT = "persist.sys.no_turn_text";
    final private static String TBT_ONTO_DISABLED = "persist.sys.map.tbt.disableonto";
    final private static String TRAFFIC_WIDGETS_ENABLED = "persist.sys.map.traffic.widgets";
    final private static String TTS_RECALCULATION_DISABLED = "persist.sys.map.tts.norecalc";
    final private static boolean customAnimationEnabled;
    final private static boolean debugHereLocation;
    final private static boolean dontShowTurnText;
    final private static int fps;
    final private static boolean fullCustomAnimatonEnabled;
    final private static boolean generateRouteIcons;
    final private static int glympseDuration;
    final private static boolean laneGuidanceEnabled;
    private static int simulationSpeed;
    final private static boolean tbtOntoDisabled;
    final private static boolean trafficDashWidgetsEnabled;
    final private static boolean ttsDisableRecalculating;
    
    static {
        trafficDashWidgetsEnabled = com.navdy.hud.app.util.os.SystemProperties.getBoolean(TRAFFIC_WIDGETS_ENABLED, true);
        tbtOntoDisabled = com.navdy.hud.app.util.os.SystemProperties.getBoolean(TBT_ONTO_DISABLED, false);
        ttsDisableRecalculating = com.navdy.hud.app.util.os.SystemProperties.getBoolean(TTS_RECALCULATION_DISABLED, false);
        customAnimationEnabled = com.navdy.hud.app.util.os.SystemProperties.getBoolean(CUSTOM_ANIMATION, false);
        fullCustomAnimatonEnabled = com.navdy.hud.app.util.os.SystemProperties.getBoolean( FULL_CUSTOM_ANIMATION, false);
        fps = com.navdy.hud.app.util.os.SystemProperties.getInt(MAP_FPS, DEFAULT_MAP_FPS);
        laneGuidanceEnabled = com.navdy.hud.app.util.os.SystemProperties.getBoolean(LANE_GUIDANCE_ENABLED, true);
        generateRouteIcons = com.navdy.hud.app.util.os.SystemProperties.getBoolean(GENERATE_ROUTE_ICONS, false);
        debugHereLocation = com.navdy.hud.app.util.os.SystemProperties.getBoolean(DEBUG_HERE_LOC, false);
        dontShowTurnText = com.navdy.hud.app.util.os.SystemProperties.getBoolean(NO_TURN_TEXT_IN_TBT, false);

        int i = com.navdy.hud.app.util.os.SystemProperties.getInt(GLYMPSE_DURATION, 0);

        label2: {
            label0: {
                label1: {
                    if (i <= 0) break label1;
                    if (i <= 1440) break label0;
                }
                glympseDuration = 0;
                break label2;
            }
            glympseDuration = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis((long)i);
        }
    }
    
    public MapSettings() {
    }
    
    public static boolean doNotShowTurnTextInTBT() {
        return true;
    }
    
    public static int getGlympseDuration() {
        return (glympseDuration <= 0) ? com.navdy.hud.app.framework.glympse.GlympseManager.GLYMPSE_DURATION : glympseDuration;
    }
    
    public static int getMapFps() {
        return fps;
    }
    
    public static int getSimulationSpeed() {
        return simulationSpeed;
    }
    
    public static boolean isCustomAnimationEnabled() {
        return customAnimationEnabled;
    }
    
    public static boolean isDebugHereLocation() {
        return debugHereLocation;
    }
    
    public static boolean isFullCustomAnimatonEnabled() {
        return fullCustomAnimatonEnabled;
    }
    
    public static boolean isGenerateRouteIcons() {
        return generateRouteIcons;
    }
    
    public static boolean isLaneGuidanceEnabled() {
        return laneGuidanceEnabled;
    }
    
    public static boolean isTbtOntoDisabled() {
        boolean b;

        label2: {
            label0: {
                label1: {
                    if (tbtOntoDisabled) break label1;
                    if (com.navdy.hud.app.util.DeviceUtil.isUserBuild()) break label0;
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public static boolean isTrafficDashWidgetsEnabled() {
        return trafficDashWidgetsEnabled;
    }
    
    public static boolean isTtsRecalculationDisabled() {
        return ttsDisableRecalculating;
    }
    
    public static void setSimulationSpeed(int i) {
        simulationSpeed = i;
    }
}
