package com.navdy.hud.app.config;

import android.text.TextUtils;
import com.navdy.hud.app.framework.toast.ToastPresenter;
import com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder;
import com.navdy.hud.app.util.DeviceUtil;
import com.navdy.hud.app.util.os.SystemProperties;

public class StringSetting extends Setting {
    private boolean changed;
    private String defaultValue;
    public String value;

    public enum Scope {
        NEVER,
        ENG,
        BETA,
        ALWAYS,
        CUSTOM
    }

//    public Setting(String var1, String var2, String var3, String var4) {
//        this.name = var1;
//        this.path = var2;
//        this.description = var3;
//        if (var4 != null && var4.length() > MAX_PROP_LEN) {
//            throw new IllegalArgumentException("property name (" + var4 + ") too long");
//        } else {
//            this.property = var4;
//        }
//    }

    public StringSetting(String name, String path, String description, String def) {
        super(name, path, description, def);
        defaultValue = def;
        load();
    }

    public void save() {
        if (this.changed) {
            this.changed = false;
            String propName = getProperty();
            if (propName != null) {
                SystemProperties.set(propName, this.value);
            }
        }
    }

    public void load() {
        String propName = getProperty();
        if (propName != null) {
            String setting = SystemProperties.get(propName);
            if (TextUtils.isEmpty(setting)) {
                this.value = this.defaultValue;
                return;
            }
            this.value = setting;
        }
    }
}
