package com.navdy.hud.app.bluetooth.vcard;

import android.util.Log;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class VCardConfig
{
    public static final String DEFAULT_EXPORT_CHARSET = "UTF-8";
    public static final String DEFAULT_IMPORT_CHARSET = "UTF-8";
    public static final String DEFAULT_INTERMEDIATE_CHARSET = "ISO-8859-1";
    public static final int FLAG_APPEND_TYPE_PARAM = 67108864;
    public static final int FLAG_CONVERT_PHONETIC_NAME_STRINGS = 134217728;
    private static final int FLAG_DOCOMO = 536870912;
    public static final int FLAG_REFRAIN_IMAGE_EXPORT = 8388608;
    public static final int FLAG_REFRAIN_PHONE_NUMBER_FORMATTING = 33554432;
    public static final int FLAG_REFRAIN_QP_TO_NAME_PROPERTIES = 268435456;
    private static final int FLAG_USE_ANDROID_PROPERTY = Integer.MIN_VALUE;
    private static final int FLAG_USE_DEFACT_PROPERTY = 1073741824;
    static final int LOG_LEVEL = 0;
    static final int LOG_LEVEL_NONE = 0;
    static final int LOG_LEVEL_PERFORMANCE_MEASUREMENT = 1;
    static final int LOG_LEVEL_SHOW_WARNING = 2;
    static final int LOG_LEVEL_VERBOSE = 3;
    private static final String LOG_TAG = "vCard";
    public static final int NAME_ORDER_DEFAULT = 0;
    public static final int NAME_ORDER_EUROPE = 4;
    public static final int NAME_ORDER_JAPANESE = 8;
    private static final int NAME_ORDER_MASK = 12;
    public static int VCARD_TYPE_DEFAULT = 0;
    public static final int VCARD_TYPE_DOCOMO = 939524104;
    static final String VCARD_TYPE_DOCOMO_STR = "docomo";
    public static final int VCARD_TYPE_UNKNOWN = 0;
    public static final int VCARD_TYPE_V21_EUROPE = -1073741820;
    static final String VCARD_TYPE_V21_EUROPE_STR = "v21_europe";
    public static final int VCARD_TYPE_V21_GENERIC = -1073741824;
    static String VCARD_TYPE_V21_GENERIC_STR;
    public static final int VCARD_TYPE_V21_JAPANESE = -1073741816;
    public static final int VCARD_TYPE_V21_JAPANESE_MOBILE = 402653192;
    static final String VCARD_TYPE_V21_JAPANESE_MOBILE_STR = "v21_japanese_mobile";
    static final String VCARD_TYPE_V21_JAPANESE_STR = "v21_japanese_utf8";
    public static final int VCARD_TYPE_V30_EUROPE = -1073741819;
    static final String VCARD_TYPE_V30_EUROPE_STR = "v30_europe";
    public static final int VCARD_TYPE_V30_GENERIC = -1073741823;
    static final String VCARD_TYPE_V30_GENERIC_STR = "v30_generic";
    public static final int VCARD_TYPE_V30_JAPANESE = -1073741815;
    static final String VCARD_TYPE_V30_JAPANESE_STR = "v30_japanese_utf8";
    public static final int VCARD_TYPE_V40_GENERIC = -1073741822;
    static final String VCARD_TYPE_V40_GENERIC_STR = "v40_generic";
    public static final int VERSION_21 = 0;
    public static final int VERSION_30 = 1;
    public static final int VERSION_40 = 2;
    public static final int VERSION_MASK = 3;
    private static final Set<Integer> sJapaneseMobileTypeSet;
    private static final Map<String, Integer> sVCardTypeMap;
    
    static {
        VCardConfig.VCARD_TYPE_V21_GENERIC_STR = "v21_generic";
        VCardConfig.VCARD_TYPE_DEFAULT = -1073741824;
        (sVCardTypeMap = new HashMap<String, Integer>()).put(VCardConfig.VCARD_TYPE_V21_GENERIC_STR, -1073741824);
        VCardConfig.sVCardTypeMap.put("v30_generic", -1073741823);
        VCardConfig.sVCardTypeMap.put("v21_europe", -1073741820);
        VCardConfig.sVCardTypeMap.put("v30_europe", -1073741819);
        VCardConfig.sVCardTypeMap.put("v21_japanese_utf8", -1073741816);
        VCardConfig.sVCardTypeMap.put("v30_japanese_utf8", -1073741815);
        VCardConfig.sVCardTypeMap.put("v21_japanese_mobile", 402653192);
        VCardConfig.sVCardTypeMap.put("docomo", 939524104);
        (sJapaneseMobileTypeSet = new HashSet<Integer>()).add(-1073741816);
        VCardConfig.sJapaneseMobileTypeSet.add(-1073741815);
        VCardConfig.sJapaneseMobileTypeSet.add(402653192);
        VCardConfig.sJapaneseMobileTypeSet.add(939524104);
    }
    
    public static boolean appendTypeParamName(final int n) {
        return isVersion30(n) || (0x4000000 & n) != 0x0;
    }
    
    public static int getNameOrderType(final int n) {
        return n & 0xC;
    }
    
    public static int getVCardTypeFromString(final String s) {
        final String lowerCase = s.toLowerCase();
        int n;
        if (VCardConfig.sVCardTypeMap.containsKey(lowerCase)) {
            n = VCardConfig.sVCardTypeMap.get(lowerCase);
        }
        else if ("default".equalsIgnoreCase(s)) {
            n = VCardConfig.VCARD_TYPE_DEFAULT;
        }
        else {
            Log.e("vCard", "Unknown vCard type String: \"" + s + "\"");
            n = VCardConfig.VCARD_TYPE_DEFAULT;
        }
        return n;
    }
    
    public static boolean isDoCoMo(final int n) {
        return (0x20000000 & n) != 0x0;
    }
    
    public static boolean isJapaneseDevice(final int n) {
        return VCardConfig.sJapaneseMobileTypeSet.contains(n);
    }
    
    public static boolean isVersion21(final int n) {
        return (n & 0x3) == 0x0;
    }
    
    public static boolean isVersion30(final int n) {
        boolean b = true;
        if ((n & 0x3) != 0x1) {
            b = false;
        }
        return b;
    }
    
    public static boolean isVersion40(final int n) {
        return (n & 0x3) == 0x2;
    }
    
    public static boolean needsToConvertPhoneticString(final int n) {
        return (0x8000000 & n) != 0x0;
    }
    
    public static boolean onlyOneNoteFieldIsAvailable(final int n) {
        return n == 939524104;
    }
    
    static boolean refrainPhoneNumberFormatting(final int n) {
        return (0x2000000 & n) != 0x0;
    }
    
    public static boolean shouldRefrainQPToNameProperties(final int n) {
        return !shouldUseQuotedPrintable(n) || (0x10000000 & n) != 0x0;
    }
    
    public static boolean shouldUseQuotedPrintable(final int n) {
        return !isVersion30(n);
    }
    
    public static boolean showPerformanceLog() {
        return false;
    }
    
    public static boolean usesAndroidSpecificProperty(final int n) {
        return (Integer.MIN_VALUE & n) != 0x0;
    }
    
    public static boolean usesDefactProperty(final int n) {
        return (0x40000000 & n) != 0x0;
    }
}
