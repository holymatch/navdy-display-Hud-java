package com.navdy.hud.app.view.drawable;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import com.navdy.hud.app.R;

public class TachometerGaugeDrawable extends GaugeDrawable {
    private static final float GAUGE_SLICES_START_ANGLE = 146.8f;
    private static final float GAUGE_SLICE_ANGLE = 30.8f;
    private static final float GAUGE_START_ANGLE = 146.8f;
    private static final int GROOVES = 8;
    private static final float LAST_MAX_MARKER_SWEEP_ANGLE = 2.0f;
    private static final float TOTAL_SWEEP_ANGLE = 246.4f;
    private int fullModeGaugeBackgroundBottomOffset;
    private int fullModeGaugeInternalMarginBottom;
    private int fullModeGaugeInternalMarginLeft;
    private int fullModeGaugeInternalMarginRight;
    private int fullModeGaugeInternalMarginTop;
    private float gaugeBackgroundBorderWidth;
    private int gaugeBackgroundBottomOffset;
    private int gaugeInternalMarginBottom;
    private int gaugeInternalMarginLeft;
    private int gaugeInternalMarginRight;
    private int gaugeInternalMarginTop;
    private int grooveTextOffset;
    private float grooveTextSize;
    private int maxMarkerAlpha = 0;
    private int maxMarkerColor;
    private int maxMarkerValue;
    private int regularColor;
    private int rpmWarningLevel;
    Drawable tachoMeterBackgroundDrawable;
    private int valueRingWidth;
    private int warningColor;

    public TachometerGaugeDrawable(Context context, int backgroundResource) {
        super(context, backgroundResource);
        this.fullModeGaugeInternalMarginTop = context.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_margin_top);
        this.fullModeGaugeInternalMarginLeft = context.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_margin_left);
        this.fullModeGaugeInternalMarginBottom = context.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_margin_bottom);
        this.fullModeGaugeInternalMarginRight = context.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_margin_right);
        this.fullModeGaugeBackgroundBottomOffset = context.getResources().getDimensionPixelSize(R.dimen.tachometer_full_mode_gauge_background_bottom_offset);
        this.gaugeBackgroundBorderWidth = context.getResources().getDimension(R.dimen.tachometer_gauge_background_border_width);
        this.regularColor = context.getResources().getColor(R.color.tachometer_color);
        this.warningColor = context.getResources().getColor(R.color.tachometer_warning_color);
        this.maxMarkerColor = context.getResources().getColor(R.color.cyan);
        this.valueRingWidth = context.getResources().getDimensionPixelSize(R.dimen.tachometer_guage_inner_bar_width);
        this.tachoMeterBackgroundDrawable = context.getResources().getDrawable(R.drawable.asset_tachometer_background);
        this.grooveTextSize = (float) context.getResources().getDimensionPixelSize(R.dimen.tachometer_gauge_groove_text_size);
        this.grooveTextOffset = context.getResources().getDimensionPixelSize(R.dimen.tachometer_gauge_groove_text_offset);
        this.gaugeInternalMarginTop = this.fullModeGaugeInternalMarginTop;
        this.gaugeInternalMarginLeft = this.fullModeGaugeInternalMarginLeft;
        this.gaugeInternalMarginBottom = this.fullModeGaugeInternalMarginBottom;
        this.gaugeInternalMarginRight = this.fullModeGaugeInternalMarginRight;
        this.gaugeBackgroundBottomOffset = this.fullModeGaugeBackgroundBottomOffset;
    }

    public TachometerGaugeDrawable(Context context, int backgroundResource, int stateColorsResId) {
        super(context, backgroundResource, stateColorsResId);
    }

    public void draw(Canvas canvas) {
        int color;
        super.draw(canvas);
        Rect bounds = getBounds();
        Rect rect = new Rect(bounds.left + this.gaugeInternalMarginLeft, bounds.top + this.gaugeInternalMarginTop, bounds.right - this.gaugeInternalMarginRight, bounds.bottom - this.gaugeInternalMarginBottom);
        this.tachoMeterBackgroundDrawable.setBounds(new Rect(rect.left, rect.top, rect.right, rect.bottom - this.gaugeBackgroundBottomOffset));
        this.tachoMeterBackgroundDrawable.draw(canvas);
        float innerRingInset = ((float) (this.valueRingWidth / 2)) + this.gaugeBackgroundBorderWidth;
        RectF valueRingBounds = new RectF(((float) rect.left) + innerRingInset, ((float) rect.top) + innerRingInset, ((float) rect.right) - innerRingInset, ((float) rect.bottom) - innerRingInset);
        float meterReadingAngle = (this.mValue / this.mMaxValue) * TOTAL_SWEEP_ANGLE;
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStrokeWidth((float) this.valueRingWidth);
        this.mPaint.setStyle(Style.STROKE);
        if (this.mValue >= ((float) this.rpmWarningLevel)) {
            color = this.warningColor;
        } else {
            color = this.regularColor;
        }
        this.mPaint.setColor(color);
        canvas.drawArc(valueRingBounds, 146.8f, meterReadingAngle, false, this.mPaint);
        int radius = (rect.width() / 2) + this.grooveTextOffset;
        this.mPaint.setStyle(Style.FILL);
        this.mPaint.setColor(color);
        this.mPaint.setTextSize(this.grooveTextSize);
        this.mPaint.setTypeface(Typeface.create(Typeface.DEFAULT, 1));
        int index = (int) Math.floor((double) (this.mValue / (this.mMaxValue / 8.0f)));
        int upperBound = index < 8 ? index + 2 : index + 1;
        int i = index;
        while (i < upperBound) {
            float angle = 146.8f + (((float) i) * GAUGE_SLICE_ANGLE);
            String text = Integer.toString(i);
            Rect textBounds = new Rect();
            this.mPaint.getTextBounds(text, 0, text.length(), textBounds);
            float bottomOffset = 0.0f;
            float leftOffset = 0.0f;
            if (i == 0 || i == 8) {
                bottomOffset = (float) textBounds.height();
                leftOffset = i == 0 ? (float) textBounds.width() : (float) (-textBounds.width());
            }
            canvas.drawText(Integer.toString(i), (float) (((int) ((((double) ((rect.left - this.grooveTextOffset) + radius)) + (((double) radius) * Math.cos(Math.toRadians((double) angle)))) - ((double) leftOffset))) - (textBounds.width() / 2)), (float) ((textBounds.height() / 2) + ((int) ((((double) ((rect.top - this.grooveTextOffset) + radius)) + (((double) radius) * Math.sin(Math.toRadians((double) angle)))) - ((double) bottomOffset)))), this.mPaint);
            i++;
        }
        if (((float) this.maxMarkerValue) > this.mValue) {
            int alpha = this.mPaint.getAlpha();
            this.mPaint.setColor(this.maxMarkerColor);
            this.mPaint.setAlpha(this.maxMarkerAlpha);
            this.mPaint.setStrokeWidth((float) this.valueRingWidth);
            this.mPaint.setStyle(Style.STROKE);
            canvas.drawArc(valueRingBounds, (146.8f + ((((float) this.maxMarkerValue) / this.mMaxValue) * TOTAL_SWEEP_ANGLE)) - 2.0f, 2.0f, false, this.mPaint);
            this.mPaint.setAlpha(alpha);
        }
    }

    public void setRPMWarningLevel(int mRPMWarningLevel) {
        this.rpmWarningLevel = mRPMWarningLevel;
    }

    public int getMaxMarkerValue() {
        return this.maxMarkerValue;
    }

    public void setMaxMarkerValue(int maxMarkerValue) {
        this.maxMarkerValue = maxMarkerValue;
    }

    public int getMaxMarkerAlpha() {
        return this.maxMarkerAlpha;
    }

    public void setMaxMarkerAlpha(int maxMarkerAlpha) {
        this.maxMarkerAlpha = maxMarkerAlpha;
    }
}
