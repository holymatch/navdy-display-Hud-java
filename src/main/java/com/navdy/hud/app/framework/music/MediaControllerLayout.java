package com.navdy.hud.app.framework.music;

import android.view.View;
import android.widget.ImageView;
import java.util.Iterator;

import com.navdy.hud.app.R;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.ui.component.UISettings;
import java.util.ArrayList;
import java.util.List;
import android.util.AttributeSet;
import android.content.Context;
import java.util.Set;
import com.navdy.hud.app.manager.MusicManager;
import java.util.HashMap;
import com.navdy.hud.app.ui.component.ChoiceLayout;

public class MediaControllerLayout extends ChoiceLayout
{
    public static final HashMap<MusicManager.MediaControl, Integer> MEDIA_CONTROL_ICON_MAPPING;
    private Set<MusicManager.MediaControl> availableMediaControls;

    static {
        (MEDIA_CONTROL_ICON_MAPPING = new HashMap<>()).put(MusicManager.MediaControl.PLAY, R.drawable.icon_play_sm_2);
        MediaControllerLayout.MEDIA_CONTROL_ICON_MAPPING.put(MusicManager.MediaControl.PAUSE, R.drawable.icon_pause_sm_2);
        MediaControllerLayout.MEDIA_CONTROL_ICON_MAPPING.put(MusicManager.MediaControl.PREVIOUS, R.drawable.icon_prev_sm_2);
        MediaControllerLayout.MEDIA_CONTROL_ICON_MAPPING.put(MusicManager.MediaControl.NEXT, R.drawable.icon_next_sm_2);
        MediaControllerLayout.MEDIA_CONTROL_ICON_MAPPING.put(MusicManager.MediaControl.MUSIC_MENU, R.drawable.icon_music_sm_2);
        MediaControllerLayout.MEDIA_CONTROL_ICON_MAPPING.put(MusicManager.MediaControl.MUSIC_MENU_DEEP, R.drawable.icon_music_queue_sm);
    }

    public MediaControllerLayout(final Context context) {
        super(context);
    }

    public MediaControllerLayout(final Context context, final AttributeSet set) {
        super(context, set);
    }

    public MediaControllerLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }

    private List<Choice> getChoicesForControls() {
        final ArrayList<Choice> list = new ArrayList<>();
        if (UISettings.isMusicBrowsingEnabled()) {
            int n;
            if (RemoteDeviceManager.getInstance().getMusicManager().getMusicMenuPath() != null) {
                n = MediaControllerLayout.MEDIA_CONTROL_ICON_MAPPING.get(MusicManager.MediaControl.MUSIC_MENU_DEEP);
            }
            else {
                n = MediaControllerLayout.MEDIA_CONTROL_ICON_MAPPING.get(MusicManager.MediaControl.MUSIC_MENU);
            }
            list.add(new Choice(new Icon(n, n), MusicManager.MediaControl.MUSIC_MENU.ordinal()));
        }
        if (this.availableMediaControls.contains(MusicManager.MediaControl.PREVIOUS)) {
            final int intValue = MediaControllerLayout.MEDIA_CONTROL_ICON_MAPPING.get(MusicManager.MediaControl.PREVIOUS);
            list.add(new Choice(new Icon(intValue, intValue), MusicManager.MediaControl.PREVIOUS.ordinal()));
        }
        MusicManager.MediaControl mediaControl;
        if (this.availableMediaControls.contains(MusicManager.MediaControl.PLAY)) {
            mediaControl = MusicManager.MediaControl.PLAY;
        }
        else {
            mediaControl = MusicManager.MediaControl.PAUSE;
        }
        final int intValue2 = MediaControllerLayout.MEDIA_CONTROL_ICON_MAPPING.get(mediaControl);
        list.add(new Choice(new Icon(intValue2, intValue2), mediaControl.ordinal()));
        if (this.availableMediaControls.contains(MusicManager.MediaControl.NEXT)) {
            final int intValue3 = MediaControllerLayout.MEDIA_CONTROL_ICON_MAPPING.get(MusicManager.MediaControl.NEXT);
            list.add(new Choice(new Icon(intValue3, intValue3), MusicManager.MediaControl.NEXT.ordinal()));
        }
        return list;
    }

    private int getDefaultSelection(final List<Choice> list) {
        int n = 0;
        for (final Choice choice : list) {
            if (choice.id == MusicManager.MediaControl.MUSIC_MENU.ordinal() || choice.id == MusicManager.MediaControl.PREVIOUS.ordinal()) {
                ++n;
            }
        }
        return n;
    }

    private void updateChoices(final List<Choice> choices) {
        this.choices = choices;
        for (int i = 0; i < this.choices.size(); ++i) {
            ((ImageView)this.choiceContainer.getChildAt(i)).setImageResource(this.choices.get(i).icon.resIdSelected);
        }
    }

    @Override
    protected boolean isHighlightPersistent() {
        return true;
    }

    public void updateControls(Set<MusicManager.MediaControl> controls) {
        int i = 0;
        boolean changed = false;
        List<Choice> choices;
        if (controls == null || controls.size() == 0) {
            this.availableMediaControls = controls;
            setVisibility(INVISIBLE);
        } else if (this.availableMediaControls == null || this.availableMediaControls.size() != controls.size()) {
            this.availableMediaControls = controls;
            choices = getChoicesForControls();
            setChoices(Mode.ICON, choices, getDefaultSelection(choices), null);
            setHighlightVisibility(VISIBLE);
            setVisibility(VISIBLE);
        } else {
            MusicManager.MediaControl[] mediaControlArr = MusicManager.CONTROLS;
            int length = mediaControlArr.length;
            while (i < length) {
                MusicManager.MediaControl control = mediaControlArr[i];
                if (controls.contains(control) != this.availableMediaControls.contains(control)) {
                    changed = true;
                    this.availableMediaControls = controls;
                    break;
                }
                i++;
            }
            choices = getChoicesForControls();
            if (changed) {
                updateChoices(choices);
                return;
            }
            int selected = getSelectedItemIndex();
            if (UISettings.isMusicBrowsingEnabled() && selected == 0) {
                setSelectedItem(getDefaultSelection(choices));
            }
        }
    }
}
