package com.navdy.hud.app.framework.contacts;

import android.content.res.Resources;
import com.navdy.hud.app.HudApplication;
import com.navdy.hud.app.R;
import com.navdy.hud.app.util.GenericUtil;
import com.navdy.hud.app.util.PhoneUtil;
import java.util.HashMap;
import java.util.Map;

public class ContactImageHelper {
    public static final int DEFAULT_IMAGE_INDEX = -1;
    public static int NO_CONTACT_COLOR = 0;
    public static final int NO_CONTACT_IMAGE = R.drawable.icon_user_numberonly;
    private static final Map<Integer, Integer> sContactColorMap = new HashMap<>();
    private static final Map<Integer, Integer> sContactImageMap = new HashMap<>();
    private static final ContactImageHelper sInstance = new ContactImageHelper();

    static {
        Resources resources = HudApplication.getAppContext().getResources();
        NO_CONTACT_COLOR = resources.getColor(R.color.grey_4a);
        int counter = 0;
        sContactColorMap.put(counter, resources.getColor(R.color.icon_user_bg_0));
        sContactImageMap.put(counter, R.drawable.icon_user_bg_0);
        counter += 1;
        sContactColorMap.put(counter, resources.getColor(R.color.icon_user_bg_1));
        sContactImageMap.put(counter, R.drawable.icon_user_bg_1);
        
        sContactColorMap.put(counter, resources.getColor(R.color.icon_user_bg_2));
        sContactImageMap.put(counter, R.drawable.icon_user_bg_2);
        counter += 1;
        sContactColorMap.put(counter, resources.getColor(R.color.icon_user_bg_3));
        sContactImageMap.put(counter, R.drawable.icon_user_bg_3);
        counter += 1;
        sContactColorMap.put(counter, resources.getColor(R.color.icon_user_bg_4));
        sContactImageMap.put(counter, R.drawable.icon_user_bg_4);
        counter += 1;
        sContactColorMap.put(counter, resources.getColor(R.color.icon_user_bg_5));
        sContactImageMap.put(counter, R.drawable.icon_user_bg_5);
        counter += 1;
        sContactColorMap.put(counter, resources.getColor(R.color.icon_user_bg_6));
        sContactImageMap.put(counter, R.drawable.icon_user_bg_6);
        counter += 1;
        sContactColorMap.put(counter, resources.getColor(R.color.icon_user_bg_7));
        sContactImageMap.put(counter, R.drawable.icon_user_bg_7);
        counter += 1;
        sContactColorMap.put(counter, resources.getColor(R.color.icon_user_bg_8));
        sContactImageMap.put(counter, R.drawable.icon_user_bg_8);
    }

    public static ContactImageHelper getInstance() {
        return sInstance;
    }

    public int getContactImageIndex(String contactId) {
        if (contactId == null) {
            return DEFAULT_IMAGE_INDEX;
        }
        contactId = GenericUtil.normalizeToFilename(contactId);
        if (ContactUtil.isValidNumber(contactId)) {
            contactId = PhoneUtil.convertToE164Format(contactId);
        }
        return Math.abs(contactId.hashCode() % sContactImageMap.size());
    }

    public int getResourceId(int index) {
        if (index < 0 || index >= sContactImageMap.size()) {
            return NO_CONTACT_IMAGE;
        }
        return sContactImageMap.get(index);
    }

    public int getResourceColor(int index) {
        if (index < 0 || index >= sContactColorMap.size()) {
            return NO_CONTACT_COLOR;
        }
        return sContactColorMap.get(index);
    }

    public int getDriverImageResId(String deviceName) {
        return getResourceId(Math.abs(GenericUtil.normalizeToFilename(deviceName).hashCode() % sContactImageMap.size()));
    }
}