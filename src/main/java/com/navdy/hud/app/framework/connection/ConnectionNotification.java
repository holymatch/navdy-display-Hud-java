package com.navdy.hud.app.framework.connection;

import com.navdy.hud.app.R;
import com.navdy.hud.app.framework.toast.ToastManager$ToastParams;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.events.DeviceInfo;
import com.squareup.wire.Message;
import com.navdy.hud.app.event.RemoteEvent;
import com.navdy.service.library.events.input.LaunchAppEvent;
import com.navdy.hud.app.service.ConnectionHandler;
import com.navdy.hud.app.event.Disconnect;
import com.navdy.hud.app.event.ShowScreenWithArgs;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.hud.app.screen.WelcomeScreen;
import com.navdy.service.library.device.RemoteDeviceRegistry;
import com.navdy.hud.app.framework.toast.ToastManager;
import com.navdy.hud.app.view.ToastView;
import com.navdy.hud.app.manager.InputManager;
import com.navdy.hud.app.framework.toast.IToastCallback;
import com.navdy.hud.app.framework.contacts.ContactUtil;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import android.os.Bundle;
import com.navdy.hud.app.framework.DriverProfileHelper;
import android.graphics.Bitmap;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import android.widget.ImageView;
import com.navdy.service.library.task.TaskManager;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import android.text.TextUtils;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.hud.app.profile.DriverProfile;
import java.io.File;
import com.navdy.hud.app.ui.component.image.InitialsImageView;
import android.content.res.Resources;
import com.navdy.hud.app.manager.RemoteDeviceManager;
import com.navdy.hud.app.HudApplication;
import com.makeramen.RoundedTransformationBuilder;
import android.os.Looper;
import com.navdy.service.library.log.Logger;
import com.squareup.picasso.Transformation;
import android.os.Handler;
import com.squareup.otto.Bus;
import com.navdy.hud.app.ui.component.ChoiceLayout;
import java.util.ArrayList;
import com.navdy.hud.app.service.ConnectionHandler.ApplicationLaunchAttempted;

public class ConnectionNotification
{
    private static final int CONNECTED_TIMEOUT = 2000;
    public static final String CONNECT_ID = "connection#toast";
    public static final String DISCONNECT_ID = "disconnection#toast";
    private static final String EMPTY = "";
    private static final int NOT_CONNECTED_TIMEOUT = 30000;
    private static final int TAG_CONNECT = 0;
    private static final int TAG_DISMISS = 1;
    private static final int TAG_DONT_LAUNCH = 3;
    private static final int TAG_LAUNCH_APP = 2;
    private static final ArrayList<ChoiceLayout.Choice> appClosedChoices;
    private static final ArrayList<ChoiceLayout.Choice> appClosedLaunchFailedChoices;
    private static final int appNotConnectedWidth;
    private static final Bus bus;
    private static final String connect;
    private static final String connectedTitle;
    private static final int contentWidth;
    private static final ArrayList<ChoiceLayout.Choice> disconnectedChoices;
    private static final String disconnectedTitle;
    private static final String dismiss;
    private static Handler handler;
    private static final String iphoneAppClosedLaunchFailedMessage;
    private static final String iphoneAppClosedMessage1;
    private static final String iphoneAppClosedMessage2;
    private static final String iphoneAppClosedTitle;
    private static final String launch;
    private static Transformation roundTransformation;
    private static final Logger sLogger;
    
    static {
        sLogger = new Logger(ConnectionNotification.class);
        ConnectionNotification.handler = new Handler(Looper.getMainLooper());
        appClosedChoices = new ArrayList<ChoiceLayout.Choice>(2);
        appClosedLaunchFailedChoices = new ArrayList<ChoiceLayout.Choice>(1);
        disconnectedChoices = new ArrayList<ChoiceLayout.Choice>(2);
        ConnectionNotification.roundTransformation = new RoundedTransformationBuilder().oval(true).build();
        final Resources resources = HudApplication.getAppContext().getResources();
        connectedTitle = resources.getString(R.string.phone_connected);
        disconnectedTitle = resources.getString(R.string.phone_disconnected);
        iphoneAppClosedTitle = resources.getString(R.string.app_disconnected);
        iphoneAppClosedMessage1 = resources.getString(R.string.app_disconnected_title);
        iphoneAppClosedMessage2 = resources.getString(R.string.app_disconnected_msg);
        iphoneAppClosedLaunchFailedMessage = resources.getString(R.string.app_disconnected_msg_launch_failed);
        connect = resources.getString(R.string.connect);
        dismiss = resources.getString(R.string.dismiss);
        launch = resources.getString(R.string.launch);
        ConnectionNotification.disconnectedChoices.add(new ChoiceLayout.Choice(ConnectionNotification.connect, 0));
        ConnectionNotification.disconnectedChoices.add(new ChoiceLayout.Choice(ConnectionNotification.dismiss, 1));
        ConnectionNotification.appClosedChoices.add(new ChoiceLayout.Choice(ConnectionNotification.launch, 2));
        ConnectionNotification.appClosedChoices.add(new ChoiceLayout.Choice(ConnectionNotification.dismiss, 3));
        ConnectionNotification.appClosedLaunchFailedChoices.add(new ChoiceLayout.Choice(ConnectionNotification.dismiss, 3));
        contentWidth = (int)resources.getDimension(R.dimen.toast_phone_disconnected_width);
        appNotConnectedWidth = (int)resources.getDimension(R.dimen.toast_app_disconnected_width);
        bus = RemoteDeviceManager.getInstance().getBus();
    }
    
    private static String getName(final DriverProfile driverProfile, final NavdyDeviceId navdyDeviceId) {
        String s;
        if (TextUtils.isEmpty(s = driverProfile.getDriverName())) {
            s = navdyDeviceId.getDeviceName();
        }
        String s2;
        if ((s2 = s) == null) {
            s2 = "";
        }
        return s2;
    }
    
    private static void loadDriveImagefromProfile(final InitialsImageView initialsImageView, final String s, final File file, final DriverProfile driverProfile) {
        final Bitmap bitmapfromCache = PicassoUtil.getBitmapfromCache(file);
        if (bitmapfromCache != null) {
            initialsImageView.setInitials(null, InitialsImageView.Style.DEFAULT);
            initialsImageView.setImageBitmap(bitmapfromCache);
        }
        else {
            TaskManager.getInstance().execute(new Runnable() {
                @Override
                public void run() {
                    if (file.exists()) {
                        ConnectionNotification.handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (initialsImageView.getTag() == s) {
                                    initialsImageView.setInitials(null, InitialsImageView.Style.DEFAULT);
                                    PicassoUtil.getInstance().load(file).fit().transform(ConnectionNotification.roundTransformation).into(initialsImageView);
                                }
                            }
                        });
                    }
                    else if (driverProfile != null) {
                        final File driverImageFile = driverProfile.getDriverImageFile();
                        if (driverImageFile != null && !driverImageFile.exists()) {
                            ConnectionNotification.sLogger.d("photo not available locally");
                            PhoneImageDownloader.getInstance().submitDownload("DriverImage", PhoneImageDownloader.Priority.NORMAL, PhotoType.PHOTO_DRIVER_PROFILE, null);
                        }
                    }
                }
            }, 1);
        }
    }
    
    public static void showConnectedToast() {
        final DriverProfile currentProfile = DriverProfileHelper.getInstance().getCurrentProfile();
        final NavdyDeviceId deviceId = RemoteDeviceManager.getInstance().getDeviceId();
        if (deviceId != null) {
            final File driverImageFile = currentProfile.getDriverImageFile();
            final boolean imageAvailableInCache = PicassoUtil.isImageAvailableInCache(driverImageFile);
            final String name = getName(currentProfile, deviceId);
            final String deviceName = deviceId.getDeviceName();
            final Bundle bundle = new Bundle();
            bundle.putInt("13", 2000);
            if (imageAvailableInCache) {
                bundle.putString("9", driverImageFile.getAbsolutePath());
            }
            else {
                final int driverImageResId = ContactImageHelper.getInstance().getDriverImageResId(deviceId.getDeviceName());
                final String initials = ContactUtil.getInitials(name);
                bundle.putInt("8", driverImageResId);
                bundle.putString("10", initials);
            }
            bundle.putInt("11", R.drawable.icon_alert_phone_connected);
            bundle.putString("1", ConnectionNotification.connectedTitle);
            bundle.putInt("1_1", R.style.ToastMainTitle);
            final String driverName = currentProfile.getDriverName();
            if (!TextUtils.isEmpty(driverName)) {
                bundle.putString("4", driverName);
                bundle.putInt("5", R.style.Glances_1);
                bundle.putString("6", deviceName);
                bundle.putInt("7", R.style.Toast_1);
            }
            else {
                bundle.putString("4", deviceName);
                bundle.putInt("5", R.style.Glances_1);
            }
            bundle.putInt("16", ConnectionNotification.contentWidth);
            IToastCallback toastCallback = null;
            if (!imageAvailableInCache) {
                toastCallback = new IToastCallback() {
                    InitialsImageView driverImage;
                    
                    @Override
                    public void executeChoiceItem(final int n, final int n2) {
                    }
                    
                    @Override
                    public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
                        return false;
                    }
                    
                    @Override
                    public void onStart(final ToastView toastView) {
                        (this.driverImage = toastView.getMainLayout().screenImage).setTag("connection#toast");
                        loadDriveImagefromProfile(this.driverImage, "connection#toast", driverImageFile, currentProfile);
                    }
                    
                    @Override
                    public void onStop() {
                        this.driverImage.setTag(null);
                        this.driverImage = null;
                    }
                };
            }
            final ToastManager instance = ToastManager.getInstance();
            instance.dismissCurrentToast("disconnection#toast");
            instance.clearPendingToast("disconnection#toast");
            instance.addToast(new ToastManager$ToastParams("connection#toast", bundle, toastCallback, true, true));
        }
    }
    
    public static void showDisconnectedToast(final boolean b) {
        final RemoteDeviceManager instance = RemoteDeviceManager.getInstance();
        final ConnectionHandler connectionHandler = instance.getConnectionHandler();
        final DeviceInfo remoteDeviceInfo = instance.getRemoteDeviceInfo();
        NavdyDeviceId deviceId = null;
        final NavdyDeviceId navdyDeviceId = null;
        final Bundle bundle = new Bundle();
        boolean b2 = false;
        final boolean b3 = false;
        File file = null;
        final File file2 = null;
        bundle.putInt("13", 30000);
        bundle.putInt("11", R.drawable.icon_type_phone_disconnected);
        bundle.putInt("1_1", R.style.ToastMainTitle);
        bundle.putBoolean("19", true);
        if (b || (remoteDeviceInfo != null && connectionHandler.isAppClosed())) {
            bundle.putString("1", ConnectionNotification.iphoneAppClosedTitle);
            bundle.putInt("8", R.drawable.icon_navdyapp);
            bundle.putInt("16", ConnectionNotification.appNotConnectedWidth);
            if (!connectionHandler.isAppLaunchAttempted()) {
                ConnectionNotification.sLogger.d("App launch is not attempted yet so showing the app launch prompt");
                bundle.putParcelableArrayList("20", ConnectionNotification.appClosedChoices);
                bundle.putString("6", ConnectionNotification.iphoneAppClosedMessage1);
                bundle.putInt("7", R.style.Toast_3);
                deviceId = navdyDeviceId;
            }
            else {
                ConnectionNotification.sLogger.d("App launch has already been attempted but app is not launched yet");
                bundle.putParcelableArrayList("20", ConnectionNotification.appClosedLaunchFailedChoices);
                bundle.putString("6", ConnectionNotification.iphoneAppClosedLaunchFailedMessage);
                bundle.putInt("7", R.style.Toast_3);
                deviceId = navdyDeviceId;
            }
        }
        else {
            final DeviceInfo lastConnectedDeviceInfo = connectionHandler.getLastConnectedDeviceInfo();
            if (lastConnectedDeviceInfo == null) {
                ConnectionNotification.sLogger.v("last connected device not found");
                final ConnectionInfo lastPairedDevice = RemoteDeviceRegistry.getInstance(HudApplication.getAppContext()).getLastPairedDevice();
                if (lastPairedDevice == null) {
                    ConnectionNotification.sLogger.v("no last paired device found");
                    bundle.putInt("8", R.drawable.icon_user_numberonly);
                }
                else {
                    deviceId = lastPairedDevice.getDeviceId();
                    ConnectionNotification.sLogger.v("last paired device found:" + deviceId);
                }
            }
            else {
                ConnectionNotification.sLogger.v("last connected device found");
                deviceId = new NavdyDeviceId(lastConnectedDeviceInfo.deviceId);
            }
            bundle.putString("1", ConnectionNotification.disconnectedTitle);
            bundle.putString("4", ConnectionNotification.disconnectedTitle);
            bundle.putParcelableArrayList("20", ConnectionNotification.disconnectedChoices);
            bundle.putInt("16", ConnectionNotification.contentWidth);
        }
        DriverProfile driverProfile = null;
        if (deviceId != null) {
            final DriverProfile profileForId = DriverProfileHelper.getInstance().getDriverProfileManager().getProfileForId(deviceId);
            final String deviceName = deviceId.getDeviceName();
            String driverName = null;
            File file3;
            boolean b4;
            if (profileForId == null) {
                ConnectionNotification.sLogger.v("driver profile not found:" + deviceId);
                bundle.putInt("8", R.drawable.icon_user_numberonly);
                file3 = file2;
                b4 = b3;
            }
            else {
                driverName = profileForId.getDriverName();
                ConnectionNotification.sLogger.v("driver profile found:" + deviceId);
                final File driverImageFile = profileForId.getDriverImageFile();
                final boolean imageAvailableInCache = PicassoUtil.isImageAvailableInCache(driverImageFile);
                final String name = getName(profileForId, deviceId);
                if (imageAvailableInCache) {
                    bundle.putString("9", driverImageFile.getAbsolutePath());
                    b4 = b3;
                    file3 = file2;
                }
                else {
                    final int driverImageResId = ContactImageHelper.getInstance().getDriverImageResId(deviceName);
                    final String initials = ContactUtil.getInitials(name);
                    bundle.putInt("8", driverImageResId);
                    bundle.putString("10", initials);
                    b4 = true;
                    file3 = driverImageFile;
                }
            }
            if (!TextUtils.isEmpty(driverName)) {
                bundle.putString("4", driverName);
                bundle.putInt("5", R.style.Glances_1);
                bundle.putString("6", deviceName);
                bundle.putInt("7", R.style.Toast_1);
                driverProfile = profileForId;
                file = file3;
                b2 = b4;
            }
            else {
                bundle.putString("4", deviceName);
                bundle.putInt("5", R.style.Glances_1);
                b2 = b4;
                file = file3;
                driverProfile = profileForId;
            }
        }
        final boolean finalB = b2;
        final File finalFile = file;
        final DriverProfile finalDriverProfile = driverProfile;
        final IToastCallback toastCallback = new IToastCallback() {
            InitialsImageView driverImage;
            ToastView toastView;
            
            @Override
            public void executeChoiceItem(final int n, final int n2) {
                if (this.toastView != null) {
                    switch (n2) {
                        case 0: {
                            this.toastView.dismissToast();
                            final Bundle bundle = new Bundle();
                            bundle.putString(WelcomeScreen.ARG_ACTION, WelcomeScreen.ACTION_RECONNECT);
                            ConnectionNotification.bus.post(new ShowScreenWithArgs(Screen.SCREEN_WELCOME, bundle, false));
                            break;
                        }
                        case 3:
                            ConnectionNotification.bus.post(new Disconnect());
                            this.toastView.dismissToast();
                            break;
                        case 1:
                            this.toastView.dismissToast();
                            break;
                        case 2:
                            this.toastView.dismissToast();
                            ConnectionNotification.bus.post(new ApplicationLaunchAttempted());

                            ConnectionNotification.bus.post(new RemoteEvent(new LaunchAppEvent(null)));
                            break;
                    }
                }
            }
            
            @Override
            public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
                return false;
            }
            
            @Override
            public void onStart(final ToastView toastView) {
                this.toastView = toastView;
                (this.driverImage = toastView.getMainLayout().screenImage).setTag("disconnection#toast");
                if (finalB) {
                    loadDriveImagefromProfile(this.driverImage, "disconnection#toast", finalFile, finalDriverProfile);
                }
            }
            
            @Override
            public void onStop() {
                this.toastView = null;
                this.driverImage.setTag(null);
                this.driverImage = null;
            }
        };
        final ToastManager instance2 = ToastManager.getInstance();
        instance2.dismissCurrentToast(instance2.getCurrentToastId());
        instance2.clearAllPendingToast();
        instance2.addToast(new ToastManager$ToastParams("disconnection#toast", bundle, toastCallback, true, true));
    }
}
