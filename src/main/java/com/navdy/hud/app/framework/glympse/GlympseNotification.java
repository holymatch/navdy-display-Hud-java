package com.navdy.hud.app.framework.glympse;

import com.navdy.hud.app.R;
import com.navdy.hud.app.analytics.AnalyticsSupport;
import com.navdy.service.library.events.input.GestureEvent;
import com.navdy.hud.app.ui.framework.UIStateManager;
import com.navdy.hud.app.manager.InputManager;
import android.animation.AnimatorSet;
import butterknife.ButterKnife;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.navdy.hud.app.framework.notifications.NotificationType;
import android.view.View;
import android.content.Context;
import android.graphics.Bitmap;
import com.navdy.hud.app.framework.contacts.ContactImageHelper;
import com.navdy.hud.app.util.picasso.PicassoUtil;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.hud.app.framework.contacts.PhoneImageDownloader;
import com.navdy.hud.app.maps.here.HereNavigationManager;
import com.navdy.hud.app.maps.here.HereMapsManager;
import android.graphics.Shader;
import android.text.TextUtils;
import com.navdy.hud.app.framework.notifications.NotificationManager;
import java.util.UUID;
import java.util.ArrayList;
import android.os.Looper;
import com.navdy.hud.app.HudApplication;
import android.widget.TextView;
import com.navdy.hud.app.framework.notifications.INotificationController;
import com.navdy.hud.app.framework.contacts.Contact;
import com.navdy.hud.app.ui.component.image.IconColorImageView;
import butterknife.InjectView;
import android.widget.ImageView;
import android.content.res.Resources;
import com.navdy.service.library.log.Logger;
import android.os.Handler;
import java.util.List;
import com.navdy.hud.app.ui.component.ChoiceLayout2;
import com.navdy.hud.app.framework.notifications.INotification;

public class GlympseNotification implements INotification, ChoiceLayout2.IListener
{
    private static final float NOTIFICATION_ALERT_BADGE_SCALE = 0.95f;
    private static final float NOTIFICATION_BADGE_SCALE = 0.5f;
    private static final float NOTIFICATION_DEFAULT_ICON_SCALE = 1.0f;
    private static final float NOTIFICATION_ICON_SCALE = 1.38f;
    private static final int NOTIFICATION_TIMEOUT = 5000;
    private static final int alertColor;
    private static final List<ChoiceLayout2.Choice> choicesOnlyDismiss;
    private static final List<ChoiceLayout2.Choice> choicesRetryAndDismiss;
    private static final Handler handler;
    private static final Logger logger;
    private static final Resources resources;
    private static final int shareLocationTripColor;
    @InjectView(R.id.badge)
    ImageView badge;
    @InjectView(R.id.badge_icon)
    IconColorImageView badgeIcon;
    @InjectView(R.id.choice_layout)
    ChoiceLayout2 choiceLayout;
    private final Contact contact;
    private INotificationController controller;
    private final String destinationLabel;
    private final double latitude;
    private final double longitude;
    private final String message;
    private final String notifId;
    @InjectView(R.id.notification_icon)
    IconColorImageView notificationIcon;
    @InjectView(R.id.notification_user_image)
    ImageView notificationUserImage;
    private boolean retrySendingMessage;
    @InjectView(R.id.subtitle)
    TextView subtitle;
    @InjectView(R.id.title)
    TextView title;
    private final Type type;
    private final String uuid;
    
    static {
        logger = new Logger(GlympseNotification.class);
        resources = HudApplication.getAppContext().getResources();
        handler = new Handler(Looper.getMainLooper());
        final int color = GlympseNotification.resources.getColor(R.color.glance_dismiss);
        final ChoiceLayout2.Choice choice = new ChoiceLayout2.Choice(R.id.dismiss, R.drawable.icon_glances_dismiss, color, R.drawable.icon_glances_dismiss, -16777216, GlympseNotification.resources.getString(R.string.dismiss), color);
        (choicesOnlyDismiss = new ArrayList<ChoiceLayout2.Choice>()).add(choice);
        final int color2 = GlympseNotification.resources.getColor(R.color.glance_ok_blue);
        (choicesRetryAndDismiss = new ArrayList<ChoiceLayout2.Choice>()).add(new ChoiceLayout2.Choice(R.id.retry, R.drawable.icon_glances_retry, color2, R.drawable.icon_glances_retry, -16777216, GlympseNotification.resources.getString(R.string.retry), color2));
        GlympseNotification.choicesRetryAndDismiss.add(choice);
        shareLocationTripColor = GlympseNotification.resources.getColor(R.color.share_location_trip_color);
        alertColor = GlympseNotification.resources.getColor(R.color.share_location_trip_alert_color);
    }
    
    GlympseNotification(final Contact contact, final Type type) {
        this(contact, type, null, null, 0.0, 0.0);
    }
    
    public GlympseNotification(final Contact contact, final Type type, final String message, final String destinationLabel, final double latitude, final double longitude) {
        this.contact = contact;
        this.type = type;
        this.message = message;
        this.destinationLabel = destinationLabel;
        this.latitude = latitude;
        this.longitude = longitude;
        this.uuid = UUID.randomUUID().toString();
        this.notifId = "navdy#glympse#notif#" + type.name() + "#" + this.uuid;
    }
    
    private static void addGlympseOfflineGlance(final String s, final Contact contact, final String s2, final double n, final double n2) {
        NotificationManager.getInstance().addNotification(new GlympseNotification(contact, Type.OFFLINE, s, s2, n, n2));
    }
    
    private String getContactName() {
        String s;
        if (!TextUtils.isEmpty((CharSequence)this.contact.name)) {
            s = this.contact.name;
        }
        else if (!TextUtils.isEmpty((CharSequence)this.contact.formattedNumber)) {
            s = this.contact.formattedNumber;
        }
        else {
            s = "";
        }
        return s;
    }
    
    private void setOfflineUI() {
        this.title.setText((CharSequence)GlympseNotification.resources.getString(R.string.sending_failed));
        this.subtitle.setText((CharSequence)GlympseNotification.resources.getString(R.string.offline));
        this.notificationIcon.setIcon(R.drawable.icon_message, GlympseNotification.shareLocationTripColor, null, 1.38f);
        this.notificationIcon.setVisibility(View.VISIBLE);
        this.badgeIcon.setIcon(R.drawable.icon_msg_alert, GlympseNotification.alertColor, null, 0.95f);
        this.badgeIcon.setVisibility(View.VISIBLE);
        this.choiceLayout.setChoices(GlympseNotification.choicesRetryAndDismiss, 0, (ChoiceLayout2.IListener)this);
    }
    
    private void setReadUI() {
        this.title.setText((CharSequence)this.getContactName());
        if (HereMapsManager.getInstance().isInitialized() && HereNavigationManager.getInstance().isNavigationModeOn()) {
            this.subtitle.setText((CharSequence)GlympseNotification.resources.getString(R.string.viewed_your_trip));
        }
        else {
            this.subtitle.setText((CharSequence)GlympseNotification.resources.getString(R.string.viewed_your_location));
        }
        final Bitmap bitmapfromCache = PicassoUtil.getBitmapfromCache(PhoneImageDownloader.getInstance().getImagePath(this.contact.number, PhotoType.PHOTO_CONTACT));
        if (bitmapfromCache != null) {
            this.notificationUserImage.setImageBitmap(bitmapfromCache);
            this.notificationUserImage.setVisibility(View.VISIBLE);
        }
        else {
            final ContactImageHelper instance = ContactImageHelper.getInstance();
            this.notificationIcon.setIcon(instance.getResourceId(this.contact.defaultImageIndex), instance.getResourceColor(this.contact.defaultImageIndex), null, 1.0f);
            this.notificationIcon.setVisibility(View.VISIBLE);
        }
        this.badgeIcon.setIcon(R.drawable.icon_message, GlympseNotification.shareLocationTripColor, null, 0.5f);
        this.badgeIcon.setVisibility(View.VISIBLE);
        this.choiceLayout.setChoices(GlympseNotification.choicesOnlyDismiss, 0, (ChoiceLayout2.IListener)this);
    }
    
    private void setSentUI() {
        if (HereMapsManager.getInstance().isInitialized() && HereNavigationManager.getInstance().isNavigationModeOn()) {
            this.title.setText((CharSequence)GlympseNotification.resources.getString(R.string.trip_sent));
        }
        else {
            this.title.setText((CharSequence)GlympseNotification.resources.getString(R.string.location_sent));
        }
        this.subtitle.setText((CharSequence)GlympseNotification.resources.getString(R.string.to_contact_name, new Object[] { this.getContactName() }));
        this.notificationIcon.setIcon(R.drawable.icon_message, GlympseNotification.shareLocationTripColor, null, 1.38f);
        this.notificationIcon.setVisibility(View.VISIBLE);
        this.badge.setImageResource(R.drawable.icon_msg_success);
        this.badge.setVisibility(View.VISIBLE);
        this.choiceLayout.setChoices(GlympseNotification.choicesOnlyDismiss, 0, (ChoiceLayout2.IListener)this);
    }
    
    @Override
    public boolean canAddToStackIfCurrentExists() {
        return true;
    }
    
    @Override
    public void executeItem(final ChoiceLayout2.Selection selection) {
        if (selection.id == R.id.retry) {
            this.retrySendingMessage = true;
            NotificationManager.getInstance().removeNotification(this.notifId);
        }
        else if (selection.id == R.id.dismiss) {
            NotificationManager.getInstance().removeNotification(this.notifId);
        }
    }
    
    @Override
    public boolean expandNotification() {
        return false;
    }
    
    @Override
    public int getColor() {
        return 0;
    }
    
    @Override
    public View getExpandedView(final Context context, final Object o) {
        return null;
    }
    
    @Override
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    @Override
    public String getId() {
        return this.notifId;
    }
    
    @Override
    public int getTimeout() {
        int n;
        if (this.type == Type.OFFLINE) {
            n = 0;
        }
        else {
            n = 5000;
        }
        return n;
    }
    
    @Override
    public NotificationType getType() {
        return NotificationType.GLYMPSE;
    }
    
    @Override
    public View getView(final Context context) {
        final View inflate = LayoutInflater.from(context).inflate(R.layout.notification_glympse, (ViewGroup)null);
        ButterKnife.inject(this, inflate);
        switch (this.type) {
            case SENT:
                this.setSentUI();
                break;
            case READ:
                this.setReadUI();
                break;
            case OFFLINE:
                this.setOfflineUI();
                break;
        }
        return inflate;
    }
    
    @Override
    public AnimatorSet getViewSwitchAnimation(final boolean b) {
        return null;
    }
    
    @Override
    public boolean isAlive() {
        return false;
    }
    
    @Override
    public boolean isPurgeable() {
        return false;
    }
    
    @Override
    public void itemSelected(final ChoiceLayout2.Selection selection) {
    }
    
    @Override
    public InputManager.IInputHandler nextHandler() {
        return null;
    }
    
    @Override
    public void onClick() {
    }
    
    @Override
    public void onExpandedNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onExpandedNotificationSwitched() {
    }
    
    @Override
    public boolean onGesture(final GestureEvent gestureEvent) {
        return false;
    }
    
    @Override
    public boolean onKey(final InputManager.CustomKeyEvent customKeyEvent) {
        boolean b = false;
        if (this.controller != null) {
            switch (customKeyEvent) {
                case LEFT:
                    this.choiceLayout.moveSelectionLeft();
                    if (this.type != Type.OFFLINE) {
                        this.controller.resetTimeout();
                    }
                    b = true;
                    break;
                case RIGHT:
                    this.choiceLayout.moveSelectionRight();
                    if (this.type != Type.OFFLINE) {
                        this.controller.resetTimeout();
                    }
                    b = true;
                    break;
                case SELECT:
                    this.choiceLayout.executeSelectedItem();
                    b = true;
                    break;
            }
        }
        return b;
    }
    
    @Override
    public void onNotificationEvent(final UIStateManager.Mode mode) {
    }
    
    @Override
    public void onStart(final INotificationController controller) {
        this.controller = controller;
    }
    
    @Override
    public void onStop() {
        this.controller = null;
        if (this.retrySendingMessage) {
            GlympseNotification.handler.post((Runnable)new Runnable() {
                @Override
                public void run() {
                    final boolean b = true;
                    GlympseNotification.logger.v("Glympse:retry sending message");
                    final StringBuilder sb = new StringBuilder();
                    boolean b2;
                    if (GlympseManager.getInstance().addMessage(GlympseNotification.this.contact, GlympseNotification.this.message, GlympseNotification.this.destinationLabel, GlympseNotification.this.latitude, GlympseNotification.this.longitude, sb) != GlympseManager.Error.NONE) {
                        b2 = true;
                    }
                    else {
                        b2 = false;
                    }
                    String s;
                    if (GlympseNotification.this.destinationLabel == null) {
                        s = "Location";
                    }
                    else {
                        s = "Trip";
                    }
                    if (b2) {
                        addGlympseOfflineGlance(GlympseNotification.this.message, GlympseNotification.this.contact, GlympseNotification.this.destinationLabel, GlympseNotification.this.latitude, GlympseNotification.this.longitude);
                    }
                    GlympseNotification.logger.v("Glympse:retry sucess:" + !b2);
                    AnalyticsSupport.recordGlympseSent(!b2 && b, s, GlympseNotification.this.message, sb.toString());
                }
            });
        }
    }
    
    @Override
    public void onTrackHand(final float n) {
    }
    
    @Override
    public void onUpdate() {
    }
    
    @Override
    public boolean supportScroll() {
        return false;
    }

    public enum Type {
        SENT(0),
        READ(1),
        OFFLINE(2);

        private int value;
        Type(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
