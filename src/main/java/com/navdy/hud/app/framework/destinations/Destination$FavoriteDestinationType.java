package com.navdy.hud.app.framework.destinations;


public enum Destination$FavoriteDestinationType {
    FAVORITE_NONE(0),
    FAVORITE_HOME(1),
    FAVORITE_WORK(2),
    FAVORITE_CONTACT(3),
    FAVORITE_CALENDAR(4),
    FAVORITE_CUSTOM(5);

    private int value;
    Destination$FavoriteDestinationType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }

        public static com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType buildFromValue(int i) {
        com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType[] a = com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.values();
        int i0 = a.length;
        int i1 = 0;
        while(true) {
            com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType a0 = null;
            if (i1 >= i0) {
                a0 = FAVORITE_NONE;
            } else {
                a0 = a[i1];
                if (i != a0.value) {
                    i1 = i1 + 1;
                    continue;
                }
            }
            return a0;
        }
    }

}
//
//final public class Destination$FavoriteDestinationType extends Enum {
//    final private static com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType[] $VALUES;
//    final public static com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType FAVORITE_CALENDAR;
//    final public static com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType FAVORITE_CONTACT;
//    final public static com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType FAVORITE_CUSTOM;
//    final public static com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType FAVORITE_HOME;
//    final public static com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType FAVORITE_NONE;
//    final public static com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType FAVORITE_WORK;
//    final public int value;
//
//    static {
//        FAVORITE_NONE = new com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType("FAVORITE_NONE", 0, 0);
//        FAVORITE_HOME = new com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType("FAVORITE_HOME", 1, 1);
//        FAVORITE_WORK = new com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType("FAVORITE_WORK", 2, 2);
//        FAVORITE_CONTACT = new com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType("FAVORITE_CONTACT", 3, 3);
//        FAVORITE_CALENDAR = new com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType("FAVORITE_CALENDAR", 4, 4);
//        FAVORITE_CUSTOM = new com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType("FAVORITE_CUSTOM", 5, 10);
//        com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType[] a = new com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType[6];
//        a[0] = FAVORITE_NONE;
//        a[1] = FAVORITE_HOME;
//        a[2] = FAVORITE_WORK;
//        a[3] = FAVORITE_CONTACT;
//        a[4] = FAVORITE_CALENDAR;
//        a[5] = FAVORITE_CUSTOM;
//        $VALUES = a;
//    }
//
//    private Destination$FavoriteDestinationType(String s, int i, int i0) {
//        super(s, i);
//        this.value = i0;
//    }
//
//    public static com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType buildFromValue(int i) {
//        com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType[] a = com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.values();
//        int i0 = a.length;
//        int i1 = 0;
//        while(true) {
//            com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType a0 = null;
//            if (i1 >= i0) {
//                a0 = FAVORITE_NONE;
//            } else {
//                a0 = a[i1];
//                if (i != a0.value) {
//                    i1 = i1 + 1;
//                    continue;
//                }
//            }
//            return a0;
//        }
//    }
//
//    public static com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType valueOf(String s) {
//        return (com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType)Enum.valueOf(com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType.class, s);
//    }
//
//    public static com.navdy.hud.app.framework.destinations.Destination$FavoriteDestinationType[] values() {
//        return $VALUES.clone();
//    }
//}
