package com.navdy.hud.mfi;

abstract public interface Constants {
    final public static String DEVICE_NAME = "device_name";
    final public static int MESSAGE_DEVICE_NAME = 4;
    final public static int MESSAGE_EA_READ = 2;
    final public static int MESSAGE_EA_WRITE = 3;
    final public static int MESSAGE_STATE_CHANGE = 1;
    final public static int MESSAGE_TOAST = 5;
    final public static String TOAST = "toast";
}
