package com.navdy.hud.mfi;

class FileTransferSession$1 {
    final static int[] $SwitchMap$com$navdy$hud$mfi$FileTransferSession$FileTransferCommand;
    
    static {
        $SwitchMap$com$navdy$hud$mfi$FileTransferSession$FileTransferCommand = new int[com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.values().length];
        int[] a = $SwitchMap$com$navdy$hud$mfi$FileTransferSession$FileTransferCommand;
        com.navdy.hud.mfi.FileTransferSession$FileTransferCommand a0 = com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_SETUP;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$FileTransferSession$FileTransferCommand[com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_FIRST_DATA.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$FileTransferSession$FileTransferCommand[com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_FIRST_AND_ONLY_DATA.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$FileTransferSession$FileTransferCommand[com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_DATA.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$FileTransferSession$FileTransferCommand[com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_LAST_DATA.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$FileTransferSession$FileTransferCommand[com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_CANCEL.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$mfi$FileTransferSession$FileTransferCommand[com.navdy.hud.mfi.FileTransferSession$FileTransferCommand.COMMAND_PAUSE.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException5) {
        }
    }
}
