package com.navdy.hud.mfi;

abstract public class IAPControlMessageProcessor {
    protected java.util.concurrent.ExecutorService mSerialExecutor;
    protected com.navdy.hud.mfi.iAPProcessor miAPProcessor;
    
    public IAPControlMessageProcessor(com.navdy.hud.mfi.iAPProcessor$iAPMessage[] a, com.navdy.hud.mfi.iAPProcessor a0) {
        this.mSerialExecutor = java.util.concurrent.Executors.newSingleThreadExecutor();
        this.miAPProcessor = a0;
        if (a != null) {
            int i = a.length;
            int i0 = 0;
            while(i0 < i) {
                com.navdy.hud.mfi.iAPProcessor$iAPMessage a1 = a[i0];
                this.miAPProcessor.registerControlMessageProcessor(a1, this);
                i0 = i0 + 1;
            }
        }
    }
    
    abstract public void bProcessControlMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage arg, int arg0, byte[] arg1);
    
    
    public void processControlMessage(com.navdy.hud.mfi.iAPProcessor$iAPMessage a, int i, byte[] a0) {
        this.mSerialExecutor.submit((Runnable)new com.navdy.hud.mfi.IAPControlMessageProcessor$1(this, a, i, a0));
    }
}
