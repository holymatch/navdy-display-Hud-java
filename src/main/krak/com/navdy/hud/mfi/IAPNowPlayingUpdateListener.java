package com.navdy.hud.mfi;

abstract public interface IAPNowPlayingUpdateListener {
    abstract public void onNowPlayingUpdate(com.navdy.hud.mfi.NowPlayingUpdate arg);
}
