package com.navdy.hud.mfi;

public class ByteArrayBuilder {
    java.io.ByteArrayOutputStream bos;
    java.io.DataOutputStream dos;
    
    public ByteArrayBuilder() {
        this.bos = new java.io.ByteArrayOutputStream();
        this.dos = new java.io.DataOutputStream((java.io.OutputStream)this.bos);
    }
    
    public ByteArrayBuilder(int i) {
        this.bos = new java.io.ByteArrayOutputStream(i);
        this.dos = new java.io.DataOutputStream((java.io.OutputStream)this.bos);
    }
    
    public com.navdy.hud.mfi.ByteArrayBuilder addBlob(byte[] a) {
        this.dos.write(a);
        return this;
    }
    
    public com.navdy.hud.mfi.ByteArrayBuilder addBlob(byte[] a, int i, int i0) {
        this.dos.write(a, i, i0);
        return this;
    }
    
    public com.navdy.hud.mfi.ByteArrayBuilder addInt16(int i) {
        this.dos.writeShort(i);
        return this;
    }
    
    public com.navdy.hud.mfi.ByteArrayBuilder addInt32(int i) {
        this.dos.writeInt(i);
        return this;
    }
    
    public com.navdy.hud.mfi.ByteArrayBuilder addInt64(long j) {
        this.dos.writeLong(j);
        return this;
    }
    
    public com.navdy.hud.mfi.ByteArrayBuilder addInt8(int i) {
        this.dos.writeByte(i);
        return this;
    }
    
    public com.navdy.hud.mfi.ByteArrayBuilder addUTF8(String s) {
        this.dos.write(s.getBytes("UTF-8"));
        return this;
    }
    
    public byte[] build() {
        return this.bos.toByteArray();
    }
    
    public int size() {
        return this.bos.size();
    }
}
