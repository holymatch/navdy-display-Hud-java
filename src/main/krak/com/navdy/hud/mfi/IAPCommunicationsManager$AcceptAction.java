package com.navdy.hud.mfi;


    public enum IAPCommunicationsManager$AcceptAction {
        HoldAndAccept(0),
    EndAndAccept(1);

        private int value;
        IAPCommunicationsManager$AcceptAction(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class IAPCommunicationsManager$AcceptAction extends Enum {
//    final private static com.navdy.hud.mfi.IAPCommunicationsManager$AcceptAction[] $VALUES;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$AcceptAction EndAndAccept;
//    final public static com.navdy.hud.mfi.IAPCommunicationsManager$AcceptAction HoldAndAccept;
//    
//    static {
//        HoldAndAccept = new com.navdy.hud.mfi.IAPCommunicationsManager$AcceptAction("HoldAndAccept", 0);
//        EndAndAccept = new com.navdy.hud.mfi.IAPCommunicationsManager$AcceptAction("EndAndAccept", 1);
//        com.navdy.hud.mfi.IAPCommunicationsManager$AcceptAction[] a = new com.navdy.hud.mfi.IAPCommunicationsManager$AcceptAction[2];
//        a[0] = HoldAndAccept;
//        a[1] = EndAndAccept;
//        $VALUES = a;
//    }
//    
//    private IAPCommunicationsManager$AcceptAction(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$AcceptAction valueOf(String s) {
//        return (com.navdy.hud.mfi.IAPCommunicationsManager$AcceptAction)Enum.valueOf(com.navdy.hud.mfi.IAPCommunicationsManager$AcceptAction.class, s);
//    }
//    
//    public static com.navdy.hud.mfi.IAPCommunicationsManager$AcceptAction[] values() {
//        return $VALUES.clone();
//    }
//}
//