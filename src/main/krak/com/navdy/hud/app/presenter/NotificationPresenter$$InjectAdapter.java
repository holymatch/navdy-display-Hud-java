package com.navdy.hud.app.presenter;

final public class NotificationPresenter$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding supertype;
    
    public NotificationPresenter$$InjectAdapter() {
        super("com.navdy.hud.app.presenter.NotificationPresenter", "members/com.navdy.hud.app.presenter.NotificationPresenter", true, com.navdy.hud.app.presenter.NotificationPresenter.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.presenter.NotificationPresenter.class, (this).getClass().getClassLoader());
        this.supertype = a.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.presenter.NotificationPresenter.class, (this).getClass().getClassLoader(), false, true);
    }
    
    public com.navdy.hud.app.presenter.NotificationPresenter get() {
        com.navdy.hud.app.presenter.NotificationPresenter a = new com.navdy.hud.app.presenter.NotificationPresenter();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.bus);
        a0.add(this.supertype);
    }
    
    public void injectMembers(com.navdy.hud.app.presenter.NotificationPresenter a) {
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        this.supertype.injectMembers(a);
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.presenter.NotificationPresenter)a);
    }
}
