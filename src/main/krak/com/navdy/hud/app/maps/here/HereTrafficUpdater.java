package com.navdy.hud.app.maps.here;

public class HereTrafficUpdater {
    final private static int MAX_DISTANCE_NOTIFICATION_METERS = 5000;
    final private static int MIN_DISTANCE_NOTIFICATION_METERS = 200;
    final private static int MIN_JAM_DURATION_SECS = 180;
    final private static int ONE_MINUTE_SECS = 60;
    final private static long REFRESH_INTERVAL_MILLIS = 30000L;
    final public static boolean TRAFFIC_DEBUG_ENABLED = false;
    final private com.squareup.otto.Bus bus;
    private com.here.android.mpa.mapping.TrafficEvent currentEvent;
    private int currentRemainingTimeInJam;
    private com.here.android.mpa.guidance.TrafficUpdater$RequestInfo currentRequestInfo;
    private com.here.android.mpa.routing.Route currentRoute;
    private com.navdy.hud.app.maps.here.HereTrafficUpdater$State currentState;
    private Runnable dismissEtaUpdate;
    final private android.os.Handler handler;
    final private com.navdy.service.library.log.Logger logger;
    final private com.here.android.mpa.guidance.NavigationManager navigationManager;
    private com.here.android.mpa.guidance.TrafficUpdater$GetEventsListener onGetEventsListener;
    private com.here.android.mpa.guidance.TrafficUpdater$Listener onRequestListener;
    private Runnable refreshRunnable;
    final private String tag;
    final private com.here.android.mpa.guidance.TrafficUpdater trafficUpdater;
    final private boolean verbose;
    
    public HereTrafficUpdater(com.navdy.service.library.log.Logger a, String s, boolean b, com.here.android.mpa.guidance.NavigationManager a0, com.squareup.otto.Bus a1) {
        this.refreshRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereTrafficUpdater$1(this);
        this.dismissEtaUpdate = (Runnable)new com.navdy.hud.app.maps.here.HereTrafficUpdater$2(this);
        this.onRequestListener = (com.here.android.mpa.guidance.TrafficUpdater$Listener)new com.navdy.hud.app.maps.here.HereTrafficUpdater$3(this);
        this.onGetEventsListener = (com.here.android.mpa.guidance.TrafficUpdater$GetEventsListener)new com.navdy.hud.app.maps.here.HereTrafficUpdater$4(this);
        this.logger = a;
        this.tag = s;
        this.verbose = b;
        this.navigationManager = a0;
        this.bus = a1;
        this.trafficUpdater = com.here.android.mpa.guidance.TrafficUpdater.getInstance();
        this.trafficUpdater.enableUpdate(true);
        this.handler = new android.os.Handler();
        this.currentState = com.navdy.hud.app.maps.here.HereTrafficUpdater$State.STOPPED;
        this.bus.register(this);
    }
    
    static void access$000(com.navdy.hud.app.maps.here.HereTrafficUpdater a) {
        a.startRequest();
    }
    
    static com.squareup.otto.Bus access$100(com.navdy.hud.app.maps.here.HereTrafficUpdater a) {
        return a.bus;
    }
    
    static com.here.android.mpa.routing.Route access$200(com.navdy.hud.app.maps.here.HereTrafficUpdater a) {
        return a.currentRoute;
    }
    
    static String access$300(com.navdy.hud.app.maps.here.HereTrafficUpdater a) {
        return a.tag;
    }
    
    static com.navdy.service.library.log.Logger access$400(com.navdy.hud.app.maps.here.HereTrafficUpdater a) {
        return a.logger;
    }
    
    static com.here.android.mpa.guidance.TrafficUpdater$GetEventsListener access$500(com.navdy.hud.app.maps.here.HereTrafficUpdater a) {
        return a.onGetEventsListener;
    }
    
    static com.here.android.mpa.guidance.TrafficUpdater access$600(com.navdy.hud.app.maps.here.HereTrafficUpdater a) {
        return a.trafficUpdater;
    }
    
    static void access$700(com.navdy.hud.app.maps.here.HereTrafficUpdater a) {
        a.refresh();
    }
    
    static com.navdy.hud.app.maps.here.HereTrafficUpdater$State access$800(com.navdy.hud.app.maps.here.HereTrafficUpdater a) {
        return a.currentState;
    }
    
    static void access$900(com.navdy.hud.app.maps.here.HereTrafficUpdater a, com.here.android.mpa.routing.Route a0, java.util.List a1) {
        a.processEvents(a0, a1);
    }
    
    private void cancelRequest() {
        if (this.currentRequestInfo != null) {
            if (this.currentRequestInfo.getError() == com.here.android.mpa.guidance.TrafficUpdater$Error.NONE) {
                this.trafficUpdater.cancelRequest(this.currentRequestInfo.getRequestId());
            }
            this.currentRequestInfo = null;
        } else {
            this.logger.w(new StringBuilder().append(this.tag).append("cancelRequest: currentRequestInfo must not be null.").toString());
        }
    }
    
    private void changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater$State a) {
        label7: if (a != this.currentState) {
            com.navdy.hud.app.maps.here.HereTrafficUpdater$State a0 = com.navdy.hud.app.maps.here.HereTrafficUpdater$State.STOPPED;
            label3: {
                label0: {
                    label2: {
                        label6: {
                            if (a != a0) {
                                break label6;
                            }
                            if (this.currentState != com.navdy.hud.app.maps.here.HereTrafficUpdater$State.TRACK_DISTANCE) {
                                if (this.currentState == com.navdy.hud.app.maps.here.HereTrafficUpdater$State.TRACK_JAM) {
                                    this.stopTrackingJamTime();
                                }
                            } else {
                                this.stopTrackingDistance();
                            }
                            this.stopUpdates();
                            break label2;
                        }
                        com.navdy.hud.app.maps.here.HereTrafficUpdater$State a1 = com.navdy.hud.app.maps.here.HereTrafficUpdater$State.ACTIVE;
                        label5: {
                            if (a != a1) {
                                break label5;
                            }
                            if (this.currentState != com.navdy.hud.app.maps.here.HereTrafficUpdater$State.STOPPED) {
                                if (this.currentState != com.navdy.hud.app.maps.here.HereTrafficUpdater$State.TRACK_DISTANCE) {
                                    if (this.currentState != com.navdy.hud.app.maps.here.HereTrafficUpdater$State.TRACK_JAM) {
                                        break label2;
                                    }
                                    this.stopTrackingJamTime();
                                    break label2;
                                } else {
                                    this.stopTrackingDistance();
                                    break label2;
                                }
                            } else {
                                this.startUpdates();
                                break label2;
                            }
                        }
                        com.navdy.hud.app.maps.here.HereTrafficUpdater$State a2 = com.navdy.hud.app.maps.here.HereTrafficUpdater$State.TRACK_DISTANCE;
                        label4: {
                            if (a != a2) {
                                break label4;
                            }
                            if (this.currentState != com.navdy.hud.app.maps.here.HereTrafficUpdater$State.ACTIVE) {
                                break label3;
                            }
                            this.startTrackingDistance();
                            break label2;
                        }
                        if (a != com.navdy.hud.app.maps.here.HereTrafficUpdater$State.TRACK_JAM) {
                            break label2;
                        }
                        com.navdy.hud.app.maps.here.HereTrafficUpdater$State a3 = this.currentState;
                        com.navdy.hud.app.maps.here.HereTrafficUpdater$State a4 = com.navdy.hud.app.maps.here.HereTrafficUpdater$State.ACTIVE;
                        label1: {
                            if (a3 != a4) {
                                break label1;
                            }
                            this.startTrackingJamTime();
                            break label2;
                        }
                        if (this.currentState != com.navdy.hud.app.maps.here.HereTrafficUpdater$State.TRACK_DISTANCE) {
                            break label0;
                        }
                        this.stopTrackingDistance();
                        this.startTrackingJamTime();
                    }
                    this.currentState = a;
                    break label7;
                }
                this.logger.w(new StringBuilder().append(this.tag).append(" invalid state change from ").append(this.currentState.name()).append(" to ").append(a.name()).toString());
                break label7;
            }
            this.logger.w(new StringBuilder().append(this.tag).append(" invalid state change from ").append(this.currentState.name()).append(" to ").append(a.name()).toString());
        }
    }
    
    private int getRemainingTimeInJam(com.here.android.mpa.routing.Route a, com.here.android.mpa.mapping.TrafficEvent a0) {
        java.util.List a1 = a0.getAffectedRoadElements();
        com.here.android.mpa.common.RoadElement a2 = (com.here.android.mpa.common.RoadElement)a1.get(a1.size() - 1);
        com.here.android.mpa.routing.RouteElements a3 = a.getRouteElementsFromLength((int)this.navigationManager.getElapsedDistance(), a.getLength());
        String s = a2.getIdentifier().toString();
        java.util.Iterator a4 = a3.getElements().iterator();
        double d = 0.0;
        Object a5 = a4;
        while(true) {
            boolean b = false;
            if (((java.util.Iterator)a5).hasNext()) {
                com.here.android.mpa.common.RoadElement a6 = ((com.here.android.mpa.routing.RouteElement)((java.util.Iterator)a5).next()).getRoadElement();
                if (!a6.getIdentifier().toString().equals(s)) {
                    d = d + a6.getGeometryLength() / (double)a6.getDefaultSpeed();
                    continue;
                }
                b = true;
            } else {
                b = false;
            }
            return b ? (int)d : 0;
        }
    }
    
    private boolean isCurrentRoute(com.here.android.mpa.routing.Route a) {
        return a.equals(this.currentRoute);
    }
    
    private void onDistanceTrackingUpdate(com.navdy.hud.app.maps.here.HerePositionUpdateListener$MapMatchEvent a) {
        label2: if (this.currentEvent != null) {
            boolean b = this.currentEvent.getSeverity().getValue() >= com.here.android.mpa.mapping.TrafficEvent$Severity.VERY_HIGH.getValue();
            com.here.android.mpa.common.RoadElement a0 = a.mapMatch.getRoadElement();
            String s = null;
            if (a0 != null) {
                s = a0.getIdentifier().toString();
            }
            String s0 = ((com.here.android.mpa.common.RoadElement)this.currentEvent.getAffectedRoadElements().get(0)).getIdentifier().toString();
            if (this.verbose) {
                this.logger.v(new StringBuilder().append(this.tag).append("Position update: currentRoadId=").append(s).append("; eventFirstRoadId=").append(s0).toString());
            }
            if (android.text.TextUtils.equals((CharSequence)s0, (CharSequence)s)) {
                int i = 0;
                label0: {
                    label1: {
                        if (!b) {
                            break label1;
                        }
                        i = this.getRemainingTimeInJam(this.currentRoute, this.currentEvent);
                        this.logger.v(new StringBuilder().append(this.tag).append("triggering jam, remaining time=").append(i).append(" sec").toString());
                        if (i >= 180) {
                            break label0;
                        }
                    }
                    this.currentEvent = null;
                    this.changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater$State.ACTIVE);
                    break label2;
                }
                this.bus.post(new com.navdy.hud.app.maps.here.HereTrafficUpdater$JamTrackEvent(this, this.currentRoute, this.currentEvent, i));
            }
        } else {
            this.logger.w(new StringBuilder().append(this.tag).append("onDistanceTrackingUpdate triggered while currentEvent is null.").toString());
        }
    }
    
    private void onJamTrackingUpdate() {
        if (this.currentRoute != null) {
            if (this.currentEvent != null) {
                if (this.currentRemainingTimeInJam != 0) {
                    int i = this.getRemainingTimeInJam(this.currentRoute, this.currentEvent);
                    if (this.verbose) {
                        this.logger.v(new StringBuilder().append(this.tag).append("Jam time update: remainingTimeInJam=").append(i).toString());
                    }
                    if (i <= 0) {
                        this.currentRemainingTimeInJam = 0;
                        this.changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater$State.ACTIVE);
                    } else {
                        int i0 = com.navdy.hud.app.maps.util.MapUtils.getMinuteCeiling(this.currentRemainingTimeInJam);
                        int i1 = com.navdy.hud.app.maps.util.MapUtils.getMinuteCeiling(i);
                        if (Math.abs(i0 - i1) >= 60) {
                            this.currentRemainingTimeInJam = i;
                            this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficJamProgressEvent(i1));
                        }
                    }
                } else {
                    this.logger.w(new StringBuilder().append(this.tag).append("onJamTrackingUpdate triggered while currentRemainingTimeInJam is zero.").toString());
                }
            } else {
                this.logger.w(new StringBuilder().append(this.tag).append("onJamTrackingUpdate triggered while currentEvent is null.").toString());
            }
        } else {
            this.logger.w(new StringBuilder().append(this.tag).append("onJamTrackingUpdate triggered while currentRoute is null.").toString());
        }
    }
    
    private void processEvents(com.here.android.mpa.routing.Route a, java.util.List a0) {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        java.util.HashMap a1 = new java.util.HashMap();
        Object a2 = a0.iterator();
        while(((java.util.Iterator)a2).hasNext()) {
            com.here.android.mpa.mapping.TrafficEvent a3 = (com.here.android.mpa.mapping.TrafficEvent)((java.util.Iterator)a2).next();
            java.util.List a4 = a3.getAffectedRoadElements();
            if (a4.size() > 0) {
                ((java.util.Map)a1).put(((com.here.android.mpa.common.RoadElement)a4.get(0)).getIdentifier().toString(), a3);
            }
        }
        java.util.Iterator a5 = a.getRouteElementsFromLength((int)this.navigationManager.getElapsedDistance(), a.getLength()).getElements().iterator();
        com.here.android.mpa.mapping.TrafficEvent a6 = null;
        Object a7 = a5;
        int i = 0;
        while(true) {
            boolean b = ((java.util.Iterator)a7).hasNext();
            com.here.android.mpa.mapping.TrafficEvent a8 = null;
            label5: {
                label2: {
                    if (!b) {
                        break label2;
                    }
                    com.here.android.mpa.common.RoadElement a9 = ((com.here.android.mpa.routing.RouteElement)((java.util.Iterator)a7).next()).getRoadElement();
                    String s = a9.getIdentifier().toString();
                    this.logger.v(new StringBuilder().append(this.tag).append("route element id=").append(s).toString());
                    boolean b0 = ((java.util.Map)a1).containsKey(s);
                    label0: {
                        if (!b0) {
                            break label0;
                        }
                        a8 = (com.here.android.mpa.mapping.TrafficEvent)((java.util.Map)a1).get(s);
                        boolean b1 = a8.getSeverity().getValue() > com.here.android.mpa.mapping.TrafficEvent$Severity.HIGH.getValue();
                        boolean b2 = a8.getSeverity().getValue() > com.here.android.mpa.mapping.TrafficEvent$Severity.NORMAL.getValue();
                        this.logger.v(new StringBuilder().append(this.tag).append("event inside route: isVerySevereEvent=").append(b1).append("; isSevereEvent=").append(b2).append("; distanceToLocation=").append(i).toString());
                        label3: {
                            label4: {
                                if (!b1) {
                                    break label4;
                                }
                                if (i < 200) {
                                    break label3;
                                }
                            }
                            label1: {
                                if (!b1) {
                                    break label1;
                                }
                                if (i <= 200) {
                                    break label1;
                                }
                                break label2;
                            }
                            if (!b2) {
                                break label0;
                            }
                            if (a6 != null) {
                                break label0;
                            }
                            if (i <= 200) {
                                break label0;
                            }
                            a6 = a8;
                            break label0;
                        }
                        int i0 = this.getRemainingTimeInJam(a, a8);
                        this.logger.v(new StringBuilder().append(this.tag).append("triggering jam, remaining time=").append(i0).append(" sec").toString());
                        if (i0 < 180) {
                            break label0;
                        }
                        this.bus.post(new com.navdy.hud.app.maps.here.HereTrafficUpdater$JamTrackEvent(this, a, a8, i0));
                        break label5;
                    }
                    i = (int)((double)i + a9.getGeometryLength());
                    if (i <= 5000) {
                        continue;
                    }
                    a8 = null;
                }
                if (a8 == null) {
                    if (a6 == null) {
                        this.bus.post(new com.navdy.hud.app.maps.here.HereTrafficUpdater$TrackDismissEvent(this, a));
                    } else {
                        this.bus.post(new com.navdy.hud.app.maps.here.HereTrafficUpdater$DistanceTrackEvent(this, a, a6));
                    }
                } else {
                    this.bus.post(new com.navdy.hud.app.maps.here.HereTrafficUpdater$DistanceTrackEvent(this, a, a8));
                }
            }
            return;
        }
    }
    
    private void refresh() {
        this.logger.v(new StringBuilder().append(this.tag).append("posting refresh in 30 sec...").toString());
        this.cancelRequest();
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.postDelayed(this.refreshRunnable, 30000L);
    }
    
    private void startRequest() {
        if (this.currentRoute != null) {
            this.currentRequestInfo = this.trafficUpdater.request(this.currentRoute, this.onRequestListener);
            com.here.android.mpa.guidance.TrafficUpdater$Error a = this.currentRequestInfo.getError();
            switch(com.navdy.hud.app.maps.here.HereTrafficUpdater$5.$SwitchMap$com$here$android$mpa$guidance$TrafficUpdater$Error[a.ordinal()]) {
                case 2: case 3: case 4: {
                    this.logger.i(new StringBuilder().append(this.tag).append(" recovering from error before TrafficUpdater.request").toString());
                    this.refresh();
                    break;
                }
                default: {
                    this.logger.i(new StringBuilder().append(this.tag).append(" got error before TrafficUpdater.request. Invalid/malformed request.").toString());
                    this.changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater$State.STOPPED);
                    break;
                }
                case 1: {
                }
            }
        } else {
            this.logger.w(new StringBuilder().append(this.tag).append("startRequest: currentRoute must not be null.").toString());
        }
    }
    
    private void startTrackingDistance() {
        this.bus.post(this.transformToLiveTrafficEvent(this.currentEvent));
    }
    
    private void startTrackingJamTime() {
        this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficJamProgressEvent(com.navdy.hud.app.maps.util.MapUtils.getMinuteCeiling(this.currentRemainingTimeInJam)));
    }
    
    private void startUpdates() {
        if (this.currentRoute == null) {
            this.logger.e(new StringBuilder().append(this.tag).append(" route must not be null.").toString());
        }
    }
    
    private void stopTrackingDistance() {
        this.bus.post(new com.navdy.hud.app.maps.MapEvents$LiveTrafficDismissEvent());
    }
    
    private void stopTrackingJamTime() {
        this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficJamDismissEvent());
    }
    
    private void stopUpdates() {
        this.cancelRequest();
        this.currentEvent = null;
        this.handler.removeCallbacks(this.refreshRunnable);
        this.handler.removeCallbacks(this.dismissEtaUpdate);
        this.currentRoute = null;
        this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficJamDismissEvent());
        this.bus.post(new com.navdy.hud.app.maps.MapEvents$TrafficDelayDismissEvent());
    }
    
    private void trackEvent(com.here.android.mpa.mapping.TrafficEvent a) {
        this.currentEvent = a;
        this.changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater$State.TRACK_DISTANCE);
    }
    
    private void trackJam(int i) {
        this.currentRemainingTimeInJam = i;
        this.changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater$State.TRACK_JAM);
    }
    
    private com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent transformToLiveTrafficEvent(com.here.android.mpa.mapping.TrafficEvent a) {
        return new com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent((a.isFlow()) ? com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type.CONGESTION : com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Type.INCIDENT, (a.getSeverity().getValue() <= com.here.android.mpa.mapping.TrafficEvent$Severity.HIGH.getValue()) ? com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity.HIGH : com.navdy.hud.app.maps.MapEvents$LiveTrafficEvent$Severity.VERY_HIGH);
    }
    
    public void activate(com.here.android.mpa.routing.Route a) {
        this.currentRoute = a;
        this.changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater$State.ACTIVE);
    }
    
    public void onDistanceTrackEvent(com.navdy.hud.app.maps.here.HereTrafficUpdater$DistanceTrackEvent a) {
        if (this.isCurrentRoute(a.route)) {
            this.trackEvent(a.trafficEvent);
        }
    }
    
    public void onJamTrackEvent(com.navdy.hud.app.maps.here.HereTrafficUpdater$JamTrackEvent a) {
        if (this.isCurrentRoute(a.route)) {
            this.currentEvent = a.trafficEvent;
            this.trackJam(a.remainingTimeInJam);
        }
    }
    
    public void onTrackDismissEvent(com.navdy.hud.app.maps.here.HereTrafficUpdater$TrackDismissEvent a) {
        if (this.isCurrentRoute(a.route)) {
            this.changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater$State.ACTIVE);
        }
    }
    
    public void onTrackingUpdate(com.navdy.hud.app.maps.here.HerePositionUpdateListener$MapMatchEvent a) {
        try {
            if (this.currentState != com.navdy.hud.app.maps.here.HereTrafficUpdater$State.TRACK_DISTANCE) {
                if (this.currentState == com.navdy.hud.app.maps.here.HereTrafficUpdater$State.TRACK_JAM) {
                    this.onJamTrackingUpdate();
                }
            } else {
                this.onDistanceTrackingUpdate(a);
            }
        } catch(Throwable a0) {
            this.logger.e(a0);
            com.navdy.hud.app.util.CrashReporter.getInstance().reportNonFatalException(a0);
        }
    }
    
    public void stop() {
        this.changeState(com.navdy.hud.app.maps.here.HereTrafficUpdater$State.STOPPED);
    }
}
