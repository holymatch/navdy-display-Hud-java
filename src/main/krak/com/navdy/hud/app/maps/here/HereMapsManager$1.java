package com.navdy.hud.app.maps.here;

class HereMapsManager$1 implements com.here.android.mpa.common.OnEngineInitListener {
    final com.navdy.hud.app.maps.here.HereMapsManager this$0;
    
    HereMapsManager$1(com.navdy.hud.app.maps.here.HereMapsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener$Error a) {
        Object a0 = null;
        Throwable a1 = null;
        long j = android.os.SystemClock.elapsedRealtime();
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("MAP-ENGINE-INIT took [").append(j - com.navdy.hud.app.maps.here.HereMapsManager.access$000(this.this$0)).append("]").toString());
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v(new StringBuilder().append("MAP-ENGINE-INIT HERE-SDK version:").append(com.here.android.mpa.common.Version.getSdkVersion()).toString());
        com.here.android.mpa.common.OnEngineInitListener$Error a2 = com.here.android.mpa.common.OnEngineInitListener$Error.NONE;
        label2: {
            label0: if (a != a2) {
                com.here.android.mpa.common.OnEngineInitListener$Error a3 = null;
                String s = a.toString();
                com.navdy.hud.app.analytics.AnalyticsSupport.recordKeyFailure("HereMaps", s);
                com.navdy.hud.app.maps.here.HereMapsManager.access$100().e(new StringBuilder().append("MAP-ENGINE-INIT engine NOT initialized:").append(a).toString());
                com.navdy.hud.app.maps.here.HereMapsManager.access$2202(this.this$0, a);
                synchronized(com.navdy.hud.app.maps.here.HereMapsManager.access$1700(this.this$0)) {
                    com.navdy.hud.app.maps.here.HereMapsManager.access$1802(this.this$0, false);
                    this.this$0.bus.post(new com.navdy.hud.app.maps.MapEvents$MapEngineInitialize(com.navdy.hud.app.maps.here.HereMapsManager.access$1900(this.this$0)));
                    /*monexit(a0)*/;
                    a3 = com.here.android.mpa.common.OnEngineInitListener$Error.USAGE_EXPIRED;
                }
                label1: {
                    if (a == a3) {
                        break label1;
                    }
                    if (a != com.here.android.mpa.common.OnEngineInitListener$Error.MISSING_APP_CREDENTIAL) {
                        break label0;
                    }
                }
                com.navdy.hud.app.ui.activity.Main.handleLibraryInitializationError(new StringBuilder().append("Here Maps initialization failure: ").append(s).toString());
            } else {
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereMapsManager$1$1(this, j), 3);
            }
            return;
        }

    }
}
