package com.navdy.hud.app.maps.here;

class HereMapImageGenerator$2 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapImageGenerator this$0;
    final android.graphics.Bitmap val$bitmap;
    final com.navdy.hud.app.maps.here.HereMapImageGenerator$MapGeneratorParams val$mapGeneratorParams;
    
    HereMapImageGenerator$2(com.navdy.hud.app.maps.here.HereMapImageGenerator a, android.graphics.Bitmap a0, com.navdy.hud.app.maps.here.HereMapImageGenerator$MapGeneratorParams a1) {
        super();
        this.this$0 = a;
        this.val$bitmap = a0;
        this.val$mapGeneratorParams = a1;
    }
    
    public void run() {
        label2: {
            java.io.FileOutputStream a = null;
            Throwable a0 = null;
            label0: {
                try {
                    android.graphics.Bitmap a1 = this.val$bitmap;
                    label1: {
                        if (a1 == null) {
                            com.navdy.hud.app.maps.here.HereMapImageGenerator.access$100().e("bitmap not saved:");
                            break label1;
                        } else {
                            String s = this.this$0.getMapImageFile(this.val$mapGeneratorParams.id);
                            a = new java.io.FileOutputStream(s);
                            try {
                                this.val$bitmap.compress(android.graphics.Bitmap.CompressFormat.JPEG, 100, (java.io.OutputStream)a);
                                com.navdy.service.library.util.IOUtils.fileSync(a);
                                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                            } catch(Throwable a2) {
                                a0 = a2;
                                break label0;
                            }
                            com.navdy.hud.app.maps.here.HereMapImageGenerator.access$100().v(new StringBuilder().append("bitmap saved:").append(s).toString());
                            break label1;
                        }
                    }
                    if (this.val$mapGeneratorParams.callback != null) {
                        com.navdy.hud.app.maps.here.HereMapImageGenerator.access$300(this.this$0).post((Runnable)new com.navdy.hud.app.maps.here.HereMapImageGenerator$2$1(this));
                    }
                } catch(Throwable a3) {
                    a0 = a3;
                    a = null;
                    break label0;
                }
                com.navdy.service.library.util.IOUtils.fileSync((java.io.FileOutputStream)null);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
                break label2;
            }
            try {
                com.navdy.hud.app.maps.here.HereMapImageGenerator.access$100().e(a0);
            } catch(Throwable a4) {
                com.navdy.service.library.util.IOUtils.fileSync(a);
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                throw a4;
            }
            com.navdy.service.library.util.IOUtils.fileSync(a);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
        }
    }
}
