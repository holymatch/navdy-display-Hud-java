package com.navdy.hud.app.maps.here;

public class HereRerouteListener extends com.here.android.mpa.guidance.NavigationManager$RerouteListener {
    final public static com.navdy.hud.app.maps.MapEvents$RerouteEvent REROUTE_FAILED;
    final public static com.navdy.hud.app.maps.MapEvents$RerouteEvent REROUTE_FINISHED;
    final public static com.navdy.hud.app.maps.MapEvents$RerouteEvent REROUTE_STARTED;
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    private com.navdy.service.library.log.Logger logger;
    final private com.navdy.hud.app.analytics.NavigationQualityTracker navigationQualityTracker;
    private String routeCalcId;
    private volatile boolean routeCalculationOn;
    private String tag;
    
    static {
        REROUTE_STARTED = new com.navdy.hud.app.maps.MapEvents$RerouteEvent(com.navdy.hud.app.maps.MapEvents$RouteEventType.STARTED);
        REROUTE_FINISHED = new com.navdy.hud.app.maps.MapEvents$RerouteEvent(com.navdy.hud.app.maps.MapEvents$RouteEventType.FINISHED);
        REROUTE_FAILED = new com.navdy.hud.app.maps.MapEvents$RerouteEvent(com.navdy.hud.app.maps.MapEvents$RouteEventType.FAILED);
    }
    
    HereRerouteListener(com.navdy.service.library.log.Logger a, String s, com.navdy.hud.app.maps.here.HereNavigationManager a0, com.squareup.otto.Bus a1) {
        this.logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereRerouteListener.class);
        this.tag = s;
        this.hereNavigationManager = a0;
        this.bus = a1;
        this.navigationQualityTracker = com.navdy.hud.app.analytics.NavigationQualityTracker.getInstance();
    }
    
    static String access$000(com.navdy.hud.app.maps.here.HereRerouteListener a) {
        return a.routeCalcId;
    }
    
    static String access$002(com.navdy.hud.app.maps.here.HereRerouteListener a, String s) {
        a.routeCalcId = s;
        return s;
    }
    
    static com.navdy.hud.app.maps.here.HereNavigationManager access$100(com.navdy.hud.app.maps.here.HereRerouteListener a) {
        return a.hereNavigationManager;
    }
    
    static String access$200(com.navdy.hud.app.maps.here.HereRerouteListener a) {
        return a.tag;
    }
    
    static com.navdy.service.library.log.Logger access$300(com.navdy.hud.app.maps.here.HereRerouteListener a) {
        return a.logger;
    }
    
    static boolean access$402(com.navdy.hud.app.maps.here.HereRerouteListener a, boolean b) {
        a.routeCalculationOn = b;
        return b;
    }
    
    static com.squareup.otto.Bus access$500(com.navdy.hud.app.maps.here.HereRerouteListener a) {
        return a.bus;
    }
    
    static com.navdy.hud.app.analytics.NavigationQualityTracker access$600(com.navdy.hud.app.maps.here.HereRerouteListener a) {
        return a.navigationQualityTracker;
    }
    
    public boolean isRecalculating() {
        return this.routeCalculationOn;
    }
    
    public void onRerouteBegin() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereRerouteListener$1(this), 20);
    }
    
    public void onRerouteEnd(com.here.android.mpa.routing.Route a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereRerouteListener$2(this, a), 20);
    }
    
    public void onRerouteFailed() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.maps.here.HereRerouteListener$3(this), 20);
    }
}
