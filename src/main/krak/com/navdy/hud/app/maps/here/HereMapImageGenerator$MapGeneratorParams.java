package com.navdy.hud.app.maps.here;

public class HereMapImageGenerator$MapGeneratorParams {
    public com.navdy.hud.app.maps.here.HereMapImageGenerator$ICallback callback;
    public int height;
    public String id;
    public double latitude;
    public double longitude;
    public int width;
    public double zoomLevel;
    
    public HereMapImageGenerator$MapGeneratorParams() {
    }
}
