package com.navdy.hud.app.maps.here;

class HereNavigationManager$9 implements Runnable {
    final com.navdy.hud.app.maps.here.HereNavigationManager this$0;
    
    HereNavigationManager$9(com.navdy.hud.app.maps.here.HereNavigationManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        synchronized(this) {
            if (this.this$0.isNavigationModeOn()) {
                com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("arrived = true");
                com.navdy.hud.app.maps.here.HereNavigationManager.access$700(this.this$0).arrivedAtDestination();
                com.navdy.hud.app.maps.here.HereMapUtil.removeRouteInfo(com.navdy.hud.app.maps.here.HereNavigationManager.sLogger, false);
                if (com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).navigationRouteRequest.routeAttributes.contains(com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute.ROUTE_ATTRIBUTE_GAS)) {
                    com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("arrived : gas");
                    com.navdy.service.library.events.navigation.NavigationRouteRequest a = com.navdy.hud.app.maps.here.HereMapUtil.getSavedRouteData(com.navdy.hud.app.maps.here.HereNavigationManager.sLogger).navigationRouteRequest;
                    this.this$0.setNavigationMode(com.navdy.hud.app.maps.NavigationMode.MAP, (com.navdy.hud.app.maps.here.HereNavigationInfo)null, (StringBuilder)null);
                    if (a == null) {
                        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("arrived : gas, no next route");
                    } else {
                        com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("arrived : gas, has next route");
                        com.navdy.hud.app.maps.here.HereNavigationManager.access$200(this.this$0).post((Runnable)new com.navdy.hud.app.maps.here.HereNavigationManager$9$1(this, a));
                    }
                } else {
                    com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("arrived : regular");
                    if (!com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).hasArrived) {
                        com.navdy.hud.app.maps.here.HereNavigationManager.access$500(this.this$0).post(new com.navdy.hud.app.maps.MapEvents$ManeuverEvent(com.navdy.hud.app.maps.MapEvents$ManeuverEvent$Type.LAST, (com.here.android.mpa.routing.Maneuver)null));
                    }
                    com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).hasArrived = true;
                    com.navdy.hud.app.maps.here.HereNavigationManager.access$100(this.this$0).ignoreArrived = false;
                    com.navdy.hud.app.maps.here.HereNavigationManager.access$500(this.this$0).post(com.navdy.hud.app.maps.here.HereNavigationManager.access$1200());
                    com.navdy.hud.app.maps.here.HereNavigationManager.access$1300(this.this$0);
                    this.this$0.removeCurrentRoute();
                    com.navdy.hud.app.maps.here.HereNavigationManager.access$1402(this.this$0, com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_STOPPED);
                    com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("arrived: session stopped");
                    this.this$0.postNavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ARRIVED, false);
                    this.this$0.postNavigationSessionStatusEvent(false);
                    com.navdy.hud.app.maps.here.HereNavigationManager.sLogger.v("arrived: posted arrival maneuver");
                }
            }
            /*monexit(this)*/;
        }
    }
}
