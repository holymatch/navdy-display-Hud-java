package com.navdy.hud.app.maps.here;

class HereLocationFixManager$3 implements android.location.LocationListener {
    final com.navdy.hud.app.maps.here.HereLocationFixManager this$0;
    boolean toastSent;
    final android.location.LocationManager val$locationManager;
    final long val$t1;
    
    HereLocationFixManager$3(com.navdy.hud.app.maps.here.HereLocationFixManager a, android.location.LocationManager a0, long j) {
        super();
        this.this$0 = a;
        this.val$locationManager = a0;
        this.val$t1 = j;
        this.toastSent = false;
    }
    
    public void onLocationChanged(android.location.Location a) {
        if (this.toastSent) {
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().v("ublox first fix toast already posted");
        } else {
            this.val$locationManager.removeUpdates((android.location.LocationListener)this);
            this.toastSent = true;
            long j = android.os.SystemClock.elapsedRealtime() - this.val$t1;
            com.navdy.hud.app.maps.here.HereLocationFixManager.access$200().v(new StringBuilder().append("got first u-blox fix, unregister (").append(j).append(")").toString());
            if (j >= 10000L) {
                com.navdy.hud.app.framework.voice.TTSUtils.debugShowGotUbloxFix();
            } else {
                com.navdy.hud.app.maps.here.HereLocationFixManager.access$700(this.this$0).postDelayed((Runnable)new com.navdy.hud.app.maps.here.HereLocationFixManager$3$1(this), 10000L - j);
            }
        }
    }
    
    public void onProviderDisabled(String s) {
    }
    
    public void onProviderEnabled(String s) {
    }
    
    public void onStatusChanged(String s, int i, android.os.Bundle a) {
    }
}
