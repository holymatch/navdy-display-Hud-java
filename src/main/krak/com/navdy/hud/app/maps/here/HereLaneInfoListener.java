package com.navdy.hud.app.maps.here;

public class HereLaneInfoListener extends com.here.android.mpa.guidance.NavigationManager$LaneInformationListener {
    final private static com.navdy.hud.app.maps.MapEvents$LaneData EMPTY_LANE_DATA;
    final private static com.navdy.hud.app.maps.MapEvents$HideTrafficLaneInfo HIDE_LANE_INFO;
    private static com.navdy.service.library.log.Logger sLogger;
    private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.maps.MapEvents$DisplayTrafficLaneInfo lastLanesData;
    private com.navdy.hud.app.maps.here.HereNavController navController;
    private volatile boolean running;
    private Runnable startRunnable;
    private Runnable stopRunnable;
    private com.navdy.service.library.task.TaskManager taskManager;
    private java.util.ArrayList tempDirections;
    private java.util.ArrayList tempDirections2;
    private java.util.ArrayList tempLanes;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.maps.here.HereLaneInfoListener.class);
        HIDE_LANE_INFO = new com.navdy.hud.app.maps.MapEvents$HideTrafficLaneInfo();
        EMPTY_LANE_DATA = new com.navdy.hud.app.maps.MapEvents$LaneData(com.navdy.hud.app.maps.MapEvents$LaneData$Position.OFF_ROUTE, (android.graphics.drawable.Drawable[])null);
    }
    
    HereLaneInfoListener(com.squareup.otto.Bus a, com.navdy.hud.app.maps.here.HereNavController a0) {
        this.taskManager = com.navdy.service.library.task.TaskManager.getInstance();
        this.tempDirections = new java.util.ArrayList();
        this.tempDirections2 = new java.util.ArrayList();
        this.tempLanes = new java.util.ArrayList();
        this.startRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereLaneInfoListener$1(this);
        this.stopRunnable = (Runnable)new com.navdy.hud.app.maps.here.HereLaneInfoListener$2(this);
        sLogger.v("ctor");
        this.bus = a;
        this.navController = a0;
    }
    
    static com.navdy.hud.app.maps.here.HereNavController access$000(com.navdy.hud.app.maps.here.HereLaneInfoListener a) {
        return a.navController;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static boolean access$1000(com.navdy.hud.app.maps.here.HereLaneInfoListener a) {
        return a.running;
    }
    
    static com.navdy.hud.app.maps.MapEvents$DisplayTrafficLaneInfo access$200(com.navdy.hud.app.maps.here.HereLaneInfoListener a) {
        return a.lastLanesData;
    }
    
    static com.navdy.hud.app.maps.MapEvents$DisplayTrafficLaneInfo access$202(com.navdy.hud.app.maps.here.HereLaneInfoListener a, com.navdy.hud.app.maps.MapEvents$DisplayTrafficLaneInfo a0) {
        a.lastLanesData = a0;
        return a0;
    }
    
    static com.navdy.hud.app.maps.MapEvents$HideTrafficLaneInfo access$300() {
        return HIDE_LANE_INFO;
    }
    
    static com.squareup.otto.Bus access$400(com.navdy.hud.app.maps.here.HereLaneInfoListener a) {
        return a.bus;
    }
    
    static com.here.android.mpa.guidance.LaneInformation$Direction access$500(com.navdy.hud.app.maps.here.HereLaneInfoListener a, java.util.EnumSet a0) {
        return a.getOnRouteDirectionFromManeuver(a0);
    }
    
    static java.util.ArrayList access$600(com.navdy.hud.app.maps.here.HereLaneInfoListener a) {
        return a.tempLanes;
    }
    
    static com.navdy.hud.app.maps.MapEvents$LaneData access$700() {
        return EMPTY_LANE_DATA;
    }
    
    static java.util.ArrayList access$800(com.navdy.hud.app.maps.here.HereLaneInfoListener a) {
        return a.tempDirections;
    }
    
    static java.util.ArrayList access$900(com.navdy.hud.app.maps.here.HereLaneInfoListener a) {
        return a.tempDirections2;
    }
    
    private com.here.android.mpa.guidance.LaneInformation$Direction getOnRouteDirectionFromManeuver(java.util.EnumSet a) {
        com.here.android.mpa.guidance.LaneInformation$Direction a0 = null;
        try {
            com.here.android.mpa.routing.Maneuver a1 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNextManeuver();
            if (a1 != null) {
                com.here.android.mpa.routing.Maneuver$Turn a2 = a1.getTurn();
                a0 = null;
                if (a2 != null) {
                    switch(com.navdy.hud.app.maps.here.HereLaneInfoListener$4.$SwitchMap$com$here$android$mpa$routing$Maneuver$Turn[a2.ordinal()]) {
                        case 7: case 8: {
                            if (a.contains(com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_LEFT)) {
                                a0 = com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_LEFT;
                                break;
                            } else {
                                int i = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getNumDirections(a, com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.LEFT);
                                a0 = null;
                                if (i != 1) {
                                    break;
                                }
                                a0 = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDirection(a, com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.LEFT);
                                break;
                            }
                        }
                        case 6: {
                            if (a.contains(com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_LEFT)) {
                                a0 = com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_LEFT;
                                break;
                            } else {
                                int i0 = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getNumDirections(a, com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.LEFT);
                                a0 = null;
                                if (i0 != 1) {
                                    break;
                                }
                                a0 = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDirection(a, com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.LEFT);
                                break;
                            }
                        }
                        case 5: {
                            if (a.contains(com.here.android.mpa.guidance.LaneInformation$Direction.LEFT)) {
                                a0 = com.here.android.mpa.guidance.LaneInformation$Direction.LEFT;
                                break;
                            } else {
                                int i1 = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getNumDirections(a, com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.LEFT);
                                a0 = null;
                                if (i1 != 1) {
                                    break;
                                }
                                a0 = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDirection(a, com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.LEFT);
                                break;
                            }
                        }
                        case 3: case 4: {
                            if (a.contains(com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_RIGHT)) {
                                a0 = com.here.android.mpa.guidance.LaneInformation$Direction.SHARP_RIGHT;
                                break;
                            } else {
                                int i2 = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getNumDirections(a, com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.RIGHT);
                                a0 = null;
                                if (i2 != 1) {
                                    break;
                                }
                                a0 = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDirection(a, com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.RIGHT);
                                break;
                            }
                        }
                        case 2: {
                            if (a.contains(com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_RIGHT)) {
                                a0 = com.here.android.mpa.guidance.LaneInformation$Direction.SLIGHTLY_RIGHT;
                                break;
                            } else {
                                int i3 = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getNumDirections(a, com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.RIGHT);
                                a0 = null;
                                if (i3 != 1) {
                                    break;
                                }
                                a0 = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDirection(a, com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.RIGHT);
                                break;
                            }
                        }
                        case 1: {
                            if (a.contains(com.here.android.mpa.guidance.LaneInformation$Direction.RIGHT)) {
                                a0 = com.here.android.mpa.guidance.LaneInformation$Direction.RIGHT;
                                break;
                            } else {
                                int i4 = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getNumDirections(a, com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.RIGHT);
                                a0 = null;
                                if (i4 != 1) {
                                    break;
                                }
                                a0 = com.navdy.hud.app.maps.here.HereLaneInfoBuilder.getDirection(a, com.navdy.hud.app.maps.here.HereLaneInfoBuilder$DirectionType.RIGHT);
                                break;
                            }
                        }
                        default: {
                            a0 = null;
                        }
                    }
                }
            } else {
                a0 = null;
            }
        } catch(Throwable a3) {
            sLogger.e(a3);
            a0 = null;
        }
        return a0;
    }
    
    public void hideLaneInfo() {
        sLogger.v("hideLaneInfo::");
        this.bus.post(HIDE_LANE_INFO);
    }
    
    public void onLaneInformation(java.util.List a, com.here.android.mpa.common.RoadElement a0) {
        if (this.running) {
            this.taskManager.execute((Runnable)new com.navdy.hud.app.maps.here.HereLaneInfoListener$3(this, a), 15);
        }
    }
    
    void start() {
        synchronized(this) {
            if (this.running) {
                sLogger.v("already running");
            } else {
                sLogger.v("start");
                this.taskManager.execute(this.startRunnable, 15);
                this.running = true;
            }
        }
        /*monexit(this)*/;
    }
    
    void stop() {
        synchronized(this) {
            if (this.running) {
                sLogger.v("stop");
                this.taskManager.execute(this.stopRunnable, 15);
                this.running = false;
                this.bus.post(HIDE_LANE_INFO);
                this.lastLanesData = null;
            } else {
                sLogger.v("not running");
            }
        }
        /*monexit(this)*/;
    }
}
