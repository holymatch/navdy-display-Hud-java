package com.navdy.hud.app.maps.here;

class HereTrafficUpdater2$HereTrafficEvent {
    public long distance;
    public com.here.android.mpa.mapping.TrafficEvent trafficEvent;
    
    public HereTrafficUpdater2$HereTrafficEvent(com.here.android.mpa.mapping.TrafficEvent a, long j) {
        this.trafficEvent = a;
        this.distance = j;
    }
}
