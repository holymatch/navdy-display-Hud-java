package com.navdy.hud.app.maps.here;

class HereMapsManager$12 implements Runnable {
    final com.navdy.hud.app.maps.here.HereMapsManager this$0;
    
    HereMapsManager$12(com.navdy.hud.app.maps.here.HereMapsManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.maps.here.HereMapsManager.access$500(this.this$0).setTrafficInfoVisible(false);
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("hidetraffic: map off");
        com.here.android.mpa.mapping.MapRoute a = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentMapRoute();
        if (a != null) {
            a.setTrafficEnabled(false);
            com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("hidetraffic: route off");
        }
        com.navdy.hud.app.maps.here.HereMapsManager.access$100().v("hidetraffic: traffic is disabled");
    }
}
