package com.navdy.hud.app.maps.util;

class DistanceConverter$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit;
    final static int[] $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit;
    
    static {
        $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit = new int[com.navdy.hud.app.manager.SpeedManager$SpeedUnit.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit;
        com.navdy.hud.app.manager.SpeedManager$SpeedUnit a0 = com.navdy.hud.app.manager.SpeedManager$SpeedUnit.KILOMETERS_PER_HOUR;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[com.navdy.hud.app.manager.SpeedManager$SpeedUnit.MILES_PER_HOUR.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit = new int[com.navdy.service.library.events.navigation.DistanceUnit.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit;
        com.navdy.service.library.events.navigation.DistanceUnit a2 = com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_METERS;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_FEET.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_KMS.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[com.navdy.service.library.events.navigation.DistanceUnit.DISTANCE_MILES.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException4) {
        }
    }
}
