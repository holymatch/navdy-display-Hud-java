package com.navdy.hud.app.screen;

@Layout(R.layout.screen_first_launch)
public class FirstLaunchScreen extends com.navdy.hud.app.screen.BaseScreen {
    final private static com.navdy.service.library.log.Logger sLogger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.screen.FirstLaunchScreen.class);
    }
    
    public FirstLaunchScreen() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    public int getAnimationIn(flow.Flow$Direction a) {
        return -1;
    }
    
    public int getAnimationOut(flow.Flow$Direction a) {
        return -1;
    }
    
    public Object getDaggerModule() {
        return new com.navdy.hud.app.screen.FirstLaunchScreen$Module(this);
    }
    
    public String getMortarScopeName() {
        return (this).getClass().getName();
    }
    
    public com.navdy.service.library.events.ui.Screen getScreen() {
        return com.navdy.service.library.events.ui.Screen.SCREEN_FIRST_LAUNCH;
    }
}
