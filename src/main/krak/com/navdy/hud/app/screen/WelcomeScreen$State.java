package com.navdy.hud.app.screen;


    public enum WelcomeScreen$State {
        UNKNOWN(0),
        DOWNLOAD_APP(1),
        SEARCHING(2),
        PICKING(3),
        CONNECTING(4),
        LAUNCHING_APP(5),
        CONNECTION_FAILED(6),
        WELCOME(7);

        private int value;
        WelcomeScreen$State(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class WelcomeScreen$State extends Enum {
//    final private static com.navdy.hud.app.screen.WelcomeScreen$State[] $VALUES;
//    final public static com.navdy.hud.app.screen.WelcomeScreen$State CONNECTING;
//    final public static com.navdy.hud.app.screen.WelcomeScreen$State CONNECTION_FAILED;
//    final public static com.navdy.hud.app.screen.WelcomeScreen$State DOWNLOAD_APP;
//    final public static com.navdy.hud.app.screen.WelcomeScreen$State LAUNCHING_APP;
//    final public static com.navdy.hud.app.screen.WelcomeScreen$State PICKING;
//    final public static com.navdy.hud.app.screen.WelcomeScreen$State SEARCHING;
//    final public static com.navdy.hud.app.screen.WelcomeScreen$State UNKNOWN;
//    final public static com.navdy.hud.app.screen.WelcomeScreen$State WELCOME;
//    
//    static {
//        UNKNOWN = new com.navdy.hud.app.screen.WelcomeScreen$State("UNKNOWN", 0);
//        DOWNLOAD_APP = new com.navdy.hud.app.screen.WelcomeScreen$State("DOWNLOAD_APP", 1);
//        SEARCHING = new com.navdy.hud.app.screen.WelcomeScreen$State("SEARCHING", 2);
//        PICKING = new com.navdy.hud.app.screen.WelcomeScreen$State("PICKING", 3);
//        CONNECTING = new com.navdy.hud.app.screen.WelcomeScreen$State("CONNECTING", 4);
//        LAUNCHING_APP = new com.navdy.hud.app.screen.WelcomeScreen$State("LAUNCHING_APP", 5);
//        CONNECTION_FAILED = new com.navdy.hud.app.screen.WelcomeScreen$State("CONNECTION_FAILED", 6);
//        WELCOME = new com.navdy.hud.app.screen.WelcomeScreen$State("WELCOME", 7);
//        com.navdy.hud.app.screen.WelcomeScreen$State[] a = new com.navdy.hud.app.screen.WelcomeScreen$State[8];
//        a[0] = UNKNOWN;
//        a[1] = DOWNLOAD_APP;
//        a[2] = SEARCHING;
//        a[3] = PICKING;
//        a[4] = CONNECTING;
//        a[5] = LAUNCHING_APP;
//        a[6] = CONNECTION_FAILED;
//        a[7] = WELCOME;
//        $VALUES = a;
//    }
//    
//    private WelcomeScreen$State(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.screen.WelcomeScreen$State valueOf(String s) {
//        return (com.navdy.hud.app.screen.WelcomeScreen$State)Enum.valueOf(com.navdy.hud.app.screen.WelcomeScreen$State.class, s);
//    }
//    
//    public static com.navdy.hud.app.screen.WelcomeScreen$State[] values() {
//        return $VALUES.clone();
//    }
//}
//