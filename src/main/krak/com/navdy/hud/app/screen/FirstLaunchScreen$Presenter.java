package com.navdy.hud.app.screen;
import javax.inject.Inject;

public class FirstLaunchScreen$Presenter extends com.navdy.hud.app.ui.framework.BasePresenter {
    @Inject
    com.squareup.otto.Bus bus;
    private com.navdy.hud.app.device.dial.DialManager dialManager;
    @Inject
    com.navdy.hud.app.device.PowerManager powerManager;
    private boolean registered;
    private boolean stateChecked;
    @Inject
    com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    public FirstLaunchScreen$Presenter() {
    }
    
    static void access$100(com.navdy.hud.app.screen.FirstLaunchScreen$Presenter a) {
        a.checkState();
    }
    
    private void checkForMediaService() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.screen.FirstLaunchScreen$Presenter$2(this), 1);
    }
    
    private void checkState() {
        if (this.stateChecked) {
            com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v("state already checked");
        } else {
            this.stateChecked = true;
            if (this.powerManager.inQuietMode()) {
                com.navdy.hud.app.util.os.SystemProperties.set("service.bootanim.exit", "1");
            } else {
                int i = this.dialManager.getBondedDialCount();
                if (i <= 0) {
                    com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v("checkForMediaService");
                    this.checkForMediaService();
                } else {
                    com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v(new StringBuilder().append("bonded dial count=").append(i).append(" , go to next screen, stop boot animation").toString());
                    com.navdy.hud.app.util.os.SystemProperties.set("service.bootanim.exit", "1");
                    this.exitScreen();
                }
            }
        }
    }
    
    private void exitScreen() {
        if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().isRemoteDeviceConnected()) {
            com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v("go to default screen");
            com.navdy.hud.app.ui.framework.UIStateManager a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
            this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(a.getDefaultMainActiveScreen()).build());
        } else {
            com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v("go to welcome screen");
            android.os.Bundle a0 = new android.os.Bundle();
            a0.putString(com.navdy.hud.app.screen.WelcomeScreen.ARG_ACTION, com.navdy.hud.app.screen.WelcomeScreen.ACTION_RECONNECT);
            this.bus.post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME, a0, false));
        }
    }
    
    private void launchDialPairing() {
        com.navdy.hud.app.util.os.SystemProperties.set("service.bootanim.exit", "1");
        int i = this.dialManager.getBondedDialCount();
        com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v(new StringBuilder().append("bonded dial count=").append(i).append(" , go to dial pairing screen").toString());
        this.bus.post(new com.navdy.service.library.events.ui.ShowScreen$Builder().screen(com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING).build());
    }
    
    @Subscribe
    public void onDialManagerInitEvent(com.navdy.hud.app.device.dial.DialConstants$DialManagerInitEvent a) {
        com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v("got dialmanager init event");
        if (this.registered) {
            com.navdy.hud.app.view.FirstLaunchView a0 = (com.navdy.hud.app.view.FirstLaunchView)this.getView();
            if (a0 == null) {
                this.checkState();
            } else {
                a0.firstLaunchLogo.animate().translationY((float)com.navdy.hud.app.framework.toast.ToastPresenter.ANIMATION_TRANSLATION).alpha(0.0f).withEndAction((Runnable)new com.navdy.hud.app.screen.FirstLaunchScreen$Presenter$1(this)).start();
            }
        } else {
            this.checkState();
        }
    }
    
    public void onLoad(android.os.Bundle a) {
        com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v("onLoad");
        this.bus.register(this);
        this.registered = true;
        this.dialManager = com.navdy.hud.app.device.dial.DialManager.getInstance();
        if (this.dialManager.isInitialized()) {
            com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v("already inititalized");
            this.checkState();
        } else {
            com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v("show view");
        }
        this.uiStateManager.enableSystemTray(false);
        this.uiStateManager.enableNotificationColor(false);
        com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v("systemtray:invisible");
    }
    
    @Subscribe
    public void onMediaServerUp(com.navdy.hud.app.screen.FirstLaunchScreen$MediaServerUp a) {
        com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v("media service is up, stop boot-animation");
        this.launchDialPairing();
    }
    
    protected void onUnload() {
        com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v("onUnload");
        if (this.registered) {
            this.bus.unregister(this);
        }
        super.onUnload();
        this.uiStateManager.enableSystemTray(true);
        this.uiStateManager.enableNotificationColor(true);
        com.navdy.hud.app.screen.FirstLaunchScreen.access$000().v("systemtray:visible");
    }
}
