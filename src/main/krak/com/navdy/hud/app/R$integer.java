package com.navdy.hud.app;
import com.navdy.hud.app.R;

final public class R$integer {
    final public static int audio_feedback_frame_anim = R.integer.audio_feedback_frame_anim;
    final public static int choices2_lyt_halo_duration = R.integer.choices2_lyt_halo_duration;
    final public static int choices2_lyt_halo_start_delay = R.integer.choices2_lyt_halo_start_delay;
    final public static int content_loading_anim = R.integer.content_loading_anim;
    final public static int obd_data_size_required_in_KB = R.integer.obd_data_size_required_in_KB;
    final public static int obd_data_version = R.integer.obd_data_version;
    final public static int recalc_anim_delay = R.integer.recalc_anim_delay;
    final public static int recalc_anim_dur = R.integer.recalc_anim_dur;
    final public static int start_dest_anim_delay = R.integer.start_dest_anim_delay;
    final public static int start_dest_anim_dur = R.integer.start_dest_anim_dur;
    final public static int vlist_halo_anim_delay = R.integer.vlist_halo_anim_delay;
    final public static int vlist_halo_anim_dur = R.integer.vlist_halo_anim_dur;
    final public static int welcome_screen_fluctuate_anim_delay = R.integer.welcome_screen_fluctuate_anim_delay;
    final public static int welcome_screen_fluctuate_anim_dur = R.integer.welcome_screen_fluctuate_anim_dur;
    
    public R$integer() {
    }
}
