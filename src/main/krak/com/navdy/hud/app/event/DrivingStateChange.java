package com.navdy.hud.app.event;

public class DrivingStateChange {
    final public boolean driving;
    
    public DrivingStateChange(boolean b) {
        this.driving = b;
    }
}
