package com.navdy.hud.app.framework.voice;
import com.navdy.hud.app.R;

public class VoiceAssistNotification implements com.navdy.hud.app.framework.notifications.INotification {
    final private static int DEFAULT_VOICE_ASSIST_TIMEOUT = 4000;
    final private static String EMPTY = "";
    final private static int SIRI_STARTING_TIMEOUT = 5000;
    final private static int SIRI_TIMEOUT = 200000;
    final private static com.squareup.otto.Bus bus;
    private static int gnow_color;
    private static String google_now;
    final private static com.navdy.service.library.log.Logger sLogger;
    private static String siri;
    private static String siriIsActive;
    private static String siriIsStarting;
    private static int siri_color;
    private static String voice_assist_disabled;
    private static String voice_assist_na;
    private com.navdy.hud.app.framework.notifications.INotificationController controller;
    private com.navdy.hud.app.ui.component.FluctuatorAnimatorView fluctuator;
    private android.widget.ImageView image;
    private android.view.ViewGroup layout;
    private android.os.Handler mSiriTimeoutHandler;
    private Runnable mSiriTimeoutRunnable;
    private android.widget.TextView status;
    private android.widget.TextView subStatus;
    private boolean waitingAfterTrigger;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.voice.VoiceAssistNotification.class);
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        google_now = a.getString(R.string.google_now);
        siri = a.getString(R.string.siri);
        voice_assist_na = a.getString(R.string.voice_assist_not_available);
        voice_assist_disabled = a.getString(R.string.voice_assist_disabled);
        siriIsActive = a.getString(R.string.siri_active);
        siriIsStarting = a.getString(R.string.siri_starting);
        gnow_color = a.getColor(R.color.grey_fluctuator);
        siri_color = a.getColor(17170443);
        bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
    }
    
    public VoiceAssistNotification() {
        this.waitingAfterTrigger = false;
    }
    
    static android.view.ViewGroup access$000(com.navdy.hud.app.framework.voice.VoiceAssistNotification a) {
        return a.layout;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static com.navdy.hud.app.ui.component.FluctuatorAnimatorView access$200(com.navdy.hud.app.framework.voice.VoiceAssistNotification a) {
        return a.fluctuator;
    }
    
    static String access$300() {
        return voice_assist_disabled;
    }
    
    static android.widget.TextView access$400(com.navdy.hud.app.framework.voice.VoiceAssistNotification a) {
        return a.subStatus;
    }
    
    static android.widget.ImageView access$500(com.navdy.hud.app.framework.voice.VoiceAssistNotification a) {
        return a.image;
    }
    
    private void dismissNotification() {
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification("navdy#voiceassist#notif");
    }
    
    private void sendSiriKeyEvents() {
        sLogger.i("sending siri key events");
        bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.input.MediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_SIRI, com.navdy.service.library.events.input.KeyEvent.KEY_DOWN)));
        bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.input.MediaRemoteKeyEvent(com.navdy.service.library.events.input.MediaRemoteKey.MEDIA_REMOTE_KEY_SIRI, com.navdy.service.library.events.input.KeyEvent.KEY_UP)));
    }
    
    public boolean canAddToStackIfCurrentExists() {
        return false;
    }
    
    public boolean expandNotification() {
        return false;
    }
    
    public int getColor() {
        return 0;
    }
    
    public android.view.View getExpandedView(android.content.Context a, Object a0) {
        return null;
    }
    
    public int getExpandedViewIndicatorColor() {
        return 0;
    }
    
    public String getId() {
        return "navdy#voiceassist#notif";
    }
    
    public int getTimeout() {
        com.navdy.hud.app.manager.RemoteDeviceManager a = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        boolean b = a.isRemoteDeviceConnected();
        com.navdy.service.library.events.DeviceInfo$Platform a0 = b ? a.getRemoteDevicePlatform() : null;
        return b ? (a0 == null) ? 4000 : (com.navdy.hud.app.framework.voice.VoiceAssistNotification$2.$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform[a0.ordinal()] == 2) ? 200000 : 4000 : 4000;
    }
    
    public com.navdy.hud.app.framework.notifications.NotificationType getType() {
        return com.navdy.hud.app.framework.notifications.NotificationType.VOICE_ASSIST;
    }
    
    public android.view.View getView(android.content.Context a) {
        if (this.layout == null) {
            this.layout = (android.view.ViewGroup)android.view.LayoutInflater.from(a).inflate(R.layout.notification_voice_assist, (android.view.ViewGroup)null);
            this.status = (android.widget.TextView)this.layout.findViewById(R.id.status);
            this.subStatus = (android.widget.TextView)this.layout.findViewById(R.id.subStatus);
            this.fluctuator = (com.navdy.hud.app.ui.component.FluctuatorAnimatorView)this.layout.findViewById(R.id.fluctuator);
            this.image = (android.widget.ImageView)this.layout.findViewById(R.id.image);
        }
        return this.layout;
    }
    
    public android.animation.AnimatorSet getViewSwitchAnimation(boolean b) {
        return null;
    }
    
    public boolean isAlive() {
        return false;
    }
    
    public boolean isPurgeable() {
        return false;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    public void onClick() {
    }
    
    public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onExpandedNotificationSwitched() {
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        if (com.navdy.hud.app.framework.voice.VoiceAssistNotification$2.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()] != 0) {
            if (!this.waitingAfterTrigger) {
                this.sendSiriKeyEvents();
            }
            b = true;
        } else {
            b = false;
        }
        return b;
    }
    
    public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode a) {
    }
    
    public void onStart(com.navdy.hud.app.framework.notifications.INotificationController a) {
        if (this.layout != null) {
            int i = 0;
            this.controller = a;
            bus.register(this);
            com.navdy.hud.app.manager.RemoteDeviceManager a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
            boolean b = a0.isRemoteDeviceConnected();
            com.navdy.service.library.events.DeviceInfo$Platform a1 = b ? a0.getRemoteDevicePlatform() : null;
            label2: {
                label0: {
                    label1: {
                        if (!b) {
                            break label1;
                        }
                        if (a1 != null) {
                            break label0;
                        }
                    }
                    this.status.setText((CharSequence)voice_assist_na);
                    this.subStatus.setText((CharSequence)"");
                    this.image.setImageResource(R.drawable.icon_voice);
                    i = 0;
                    break label2;
                }
                switch(com.navdy.hud.app.framework.voice.VoiceAssistNotification$2.$SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform[a1.ordinal()]) {
                    case 2: {
                        i = siri_color;
                        this.status.setText((CharSequence)siri);
                        this.subStatus.setText((CharSequence)siriIsStarting);
                        this.image.setImageResource(R.drawable.icon_siri);
                        break;
                    }
                    case 1: {
                        this.status.setText((CharSequence)google_now);
                        this.subStatus.setText((CharSequence)"");
                        this.image.setImageResource(R.drawable.icon_googlenow_off);
                        i = gnow_color;
                        break;
                    }
                    default: {
                        i = 0;
                    }
                }
                sLogger.i("sending VoiceAssistRequest");
                bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.audio.VoiceAssistRequest(Boolean.valueOf(false))));
            }
            this.fluctuator.setFillColor(i);
            this.fluctuator.setStrokeColor(i);
            this.fluctuator.setVisibility(4);
            this.fluctuator.stop();
        }
    }
    
    public void onStop() {
        this.controller = null;
        bus.unregister(this);
        if (this.layout != null) {
            this.fluctuator.stop();
        }
        if (this.mSiriTimeoutHandler != null) {
            this.mSiriTimeoutHandler.removeCallbacks(this.mSiriTimeoutRunnable);
        }
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        sLogger.v("startVoiceAssistance: enabled toast");
    }
    
    public void onTrackHand(float f) {
    }
    
    public void onUpdate() {
    }
    
    public void onVoiceAssistResponse(com.navdy.service.library.events.audio.VoiceAssistResponse a) {
        label3: if (this.controller != null && this.layout != null) {
            String s = null;
            String s0 = null;
            int i = 0;
            boolean b = false;
            if (this.mSiriTimeoutHandler != null) {
                this.mSiriTimeoutHandler.removeCallbacks(this.mSiriTimeoutRunnable);
            }
            com.navdy.hud.app.manager.RemoteDeviceManager a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
            boolean b0 = a0.isRemoteDeviceConnected();
            com.navdy.service.library.events.DeviceInfo$Platform a1 = b0 ? a0.getRemoteDevicePlatform() : null;
            label0: {
                label1: {
                    label2: {
                        if (!b0) {
                            break label2;
                        }
                        if (a1 != null) {
                            break label1;
                        }
                    }
                    s = voice_assist_na;
                    this.fluctuator.stop();
                    this.fluctuator.setVisibility(4);
                    s0 = "";
                    i = R.drawable.icon_voice;
                    b = false;
                    break label0;
                }
                sLogger.i(new StringBuilder().append("received response ").append(a.state).toString());
                switch(com.navdy.hud.app.framework.voice.VoiceAssistNotification$2.$SwitchMap$com$navdy$service$library$events$audio$VoiceAssistResponse$VoiceAssistState[a.state.ordinal()]) {
                    case 4: {
                        if (a1 != com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_Android) {
                            s = siri;
                            s0 = siriIsActive;
                            if (this.waitingAfterTrigger) {
                                this.waitingAfterTrigger = false;
                                i = R.drawable.icon_siri;
                                b = true;
                                break label0;
                            } else {
                                this.sendSiriKeyEvents();
                                i = R.drawable.icon_siri;
                                b = true;
                                break label0;
                            }
                        } else {
                            s = google_now;
                            s0 = "";
                            i = R.drawable.icon_googlenow_on;
                            b = true;
                            break label0;
                        }
                    }
                    case 3: {
                        this.fluctuator.setVisibility(4);
                        this.fluctuator.stop();
                        this.dismissNotification();
                        this.waitingAfterTrigger = false;
                        break;
                    }
                    case 2: {
                        if (a1 != com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_Android) {
                            s = siri;
                            s0 = voice_assist_disabled;
                            i = R.drawable.icon_siri;
                        } else {
                            s = google_now;
                            s0 = voice_assist_disabled;
                            i = R.drawable.icon_googlenow_off;
                        }
                        this.fluctuator.setVisibility(4);
                        this.fluctuator.stop();
                        this.waitingAfterTrigger = false;
                        b = false;
                        break label0;
                    }
                    case 1: {
                        if (a1 != com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS) {
                            s = "";
                            s0 = "";
                            i = 0;
                            b = false;
                            break label0;
                        } else {
                            s = siri;
                            s0 = siriIsStarting;
                            this.waitingAfterTrigger = true;
                            this.sendSiriKeyEvents();
                            if (this.mSiriTimeoutHandler == null) {
                                this.mSiriTimeoutHandler = new android.os.Handler();
                            }
                            this.mSiriTimeoutRunnable = (Runnable)new com.navdy.hud.app.framework.voice.VoiceAssistNotification$1(this);
                            this.mSiriTimeoutHandler.postDelayed(this.mSiriTimeoutRunnable, 5000L);
                            i = R.drawable.icon_siri;
                            b = false;
                            break label0;
                        }
                    }
                    default: {
                        s = "";
                        s0 = "";
                        i = 0;
                        b = false;
                        break label0;
                    }
                }
                break label3;
            }
            this.status.setText((CharSequence)s);
            this.subStatus.setText((CharSequence)s0);
            this.image.setImageResource(i);
            if (b) {
                this.fluctuator.setVisibility(0);
                this.fluctuator.start();
            } else {
                this.fluctuator.setVisibility(4);
                this.fluctuator.stop();
            }
        }
    }
    
    public boolean supportScroll() {
        return false;
    }
}
