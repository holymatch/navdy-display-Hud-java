package com.navdy.hud.app.framework.voice;
import com.navdy.hud.app.R;

public class VoiceSearchNotification$$ViewInjector {
    public VoiceSearchNotification$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.framework.voice.VoiceSearchNotification a0, Object a1) {
        a0.subStatus = (android.widget.TextView)a.findRequiredView(a1, R.id.subStatus, "field 'subStatus'");
        a0.imageInactive = (android.widget.ImageView)a.findRequiredView(a1, R.id.image_voice_search_inactive, "field 'imageInactive'");
        a0.imageActive = (android.widget.ImageView)a.findRequiredView(a1, R.id.image_voice_search_active, "field 'imageActive'");
        a0.imageStatus = (android.widget.ImageView)a.findRequiredView(a1, R.id.img_status_badge, "field 'imageStatus'");
        a0.imageProcessing = (android.widget.ImageView)a.findRequiredView(a1, R.id.img_processing_badge, "field 'imageProcessing'");
        a0.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout2)a.findRequiredView(a1, R.id.choiceLayout, "field 'choiceLayout'");
        a0.resultsCountContainer = (android.view.ViewGroup)a.findRequiredView(a1, R.id.results_count_container, "field 'resultsCountContainer'");
        a0.resultsCount = (android.widget.TextView)a.findRequiredView(a1, R.id.results_count, "field 'resultsCount'");
        a0.listeningFeedbackView = a.findRequiredView(a1, R.id.voice_search_listening_feedback, "field 'listeningFeedbackView'");
    }
    
    public static void reset(com.navdy.hud.app.framework.voice.VoiceSearchNotification a) {
        a.subStatus = null;
        a.imageInactive = null;
        a.imageActive = null;
        a.imageStatus = null;
        a.imageProcessing = null;
        a.choiceLayout = null;
        a.resultsCountContainer = null;
        a.resultsCount = null;
        a.listeningFeedbackView = null;
    }
}
