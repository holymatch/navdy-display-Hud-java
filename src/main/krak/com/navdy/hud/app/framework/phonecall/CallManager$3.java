package com.navdy.hud.app.framework.phonecall;

class CallManager$3 {
    final static int[] $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction;
    final static int[] $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneStatus;
    
    static {
        $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneStatus = new int[com.navdy.service.library.events.callcontrol.PhoneStatus.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneStatus;
        com.navdy.service.library.events.callcontrol.PhoneStatus a0 = com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_IDLE;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneStatus[com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_RINGING.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneStatus[com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DIALING.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneStatus[com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_OFFHOOK.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$PhoneStatus[com.navdy.service.library.events.callcontrol.PhoneStatus.PHONE_DISCONNECTING.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction = new int[com.navdy.service.library.events.callcontrol.CallAction.values().length];
        int[] a1 = $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction;
        com.navdy.service.library.events.callcontrol.CallAction a2 = com.navdy.service.library.events.callcontrol.CallAction.CALL_ACCEPT;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction[com.navdy.service.library.events.callcontrol.CallAction.CALL_END.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction[com.navdy.service.library.events.callcontrol.CallAction.CALL_DIAL.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction[com.navdy.service.library.events.callcontrol.CallAction.CALL_MUTE.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$callcontrol$CallAction[com.navdy.service.library.events.callcontrol.CallAction.CALL_UNMUTE.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException8) {
        }
    }
}
