package com.navdy.hud.app.framework.notifications;

abstract public interface INotification extends com.navdy.hud.app.manager.InputManager$IInputHandler, com.navdy.hud.app.gesture.GestureDetector$GestureListener {
    abstract public boolean canAddToStackIfCurrentExists();
    
    
    abstract public boolean expandNotification();
    
    
    abstract public int getColor();
    
    
    abstract public android.view.View getExpandedView(android.content.Context arg, Object arg0);
    
    
    abstract public int getExpandedViewIndicatorColor();
    
    
    abstract public String getId();
    
    
    abstract public int getTimeout();
    
    
    abstract public com.navdy.hud.app.framework.notifications.NotificationType getType();
    
    
    abstract public android.view.View getView(android.content.Context arg);
    
    
    abstract public android.animation.AnimatorSet getViewSwitchAnimation(boolean arg);
    
    
    abstract public boolean isAlive();
    
    
    abstract public boolean isPurgeable();
    
    
    abstract public void onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode arg);
    
    
    abstract public void onExpandedNotificationSwitched();
    
    
    abstract public void onNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode arg);
    
    
    abstract public void onStart(com.navdy.hud.app.framework.notifications.INotificationController arg);
    
    
    abstract public void onStop();
    
    
    abstract public void onUpdate();
    
    
    abstract public boolean supportScroll();
}
