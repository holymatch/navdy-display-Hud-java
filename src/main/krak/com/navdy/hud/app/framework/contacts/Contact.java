package com.navdy.hud.app.framework.contacts;

public class Contact implements android.os.Parcelable {
    final public static android.os.Parcelable$Creator CREATOR;
    final private static com.navdy.service.library.log.Logger sLogger;
    public int defaultImageIndex;
    public String firstName;
    public String formattedNumber;
    public String initials;
    public String name;
    public String number;
    public com.navdy.hud.app.framework.contacts.NumberType numberType;
    public String numberTypeStr;
    public long numericNumber;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.contacts.Contact.class);
        CREATOR = (android.os.Parcelable$Creator)new com.navdy.hud.app.framework.contacts.Contact$1();
    }
    
    public Contact(android.os.Parcel a) {
        this.name = a.readString();
        this.number = a.readString();
        int i = a.readInt();
        if (i != -1) {
            this.numberType = com.navdy.hud.app.framework.contacts.NumberType.values()[i];
        }
        this.defaultImageIndex = a.readInt();
        this.numericNumber = a.readLong();
        this.firstName = a.readString();
        this.initials = a.readString();
        this.numberTypeStr = a.readString();
        this.formattedNumber = a.readString();
    }
    
    public Contact(com.navdy.service.library.events.contacts.Contact a) {
        this.name = a.name;
        this.number = a.number;
        this.numberType = com.navdy.hud.app.framework.contacts.ContactUtil.getNumberType(a.numberType);
        this.defaultImageIndex = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getContactImageIndex(this.number);
        this.numberTypeStr = a.label;
        this.init(0L);
    }
    
    public Contact(com.navdy.service.library.events.contacts.PhoneNumber a) {
        this.number = a.number;
        this.numberType = com.navdy.hud.app.framework.contacts.ContactUtil.getNumberType(a.numberType);
        this.defaultImageIndex = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance().getContactImageIndex(this.number);
        this.numberTypeStr = a.customType;
        this.init(0L);
    }
    
    public Contact(String s, String s0, com.navdy.hud.app.framework.contacts.NumberType a, int i, long j) {
        this.name = s;
        this.number = s0;
        this.numberType = a;
        this.defaultImageIndex = i;
        this.numericNumber = j;
        this.init(j);
    }
    
    private void init(long j) {
        if (!android.text.TextUtils.isEmpty((CharSequence)this.name)) {
            this.firstName = com.navdy.hud.app.framework.contacts.ContactUtil.getFirstName(this.name);
            this.initials = com.navdy.hud.app.framework.contacts.ContactUtil.getInitials(this.name);
        }
        if (this.numberTypeStr == null) {
            this.numberTypeStr = com.navdy.hud.app.framework.contacts.ContactUtil.getPhoneType(this.numberType);
        }
        this.formattedNumber = com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(this.number);
        java.util.Locale a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentLocale();
        boolean b = android.text.TextUtils.isEmpty((CharSequence)this.number);
        label0: {
            Throwable a0 = null;
            if (b) {
                break label0;
            }
            label1: if (j <= 0L) {
                try {
                    this.numericNumber = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance().parse(this.number, a.getCountry()).getNationalNumber();
                } catch(Throwable a1) {
                    a0 = a1;
                    break label1;
                }
                break label0;
            } else {
                this.numericNumber = j;
                break label0;
            }
            sLogger.e(a0);
        }
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void setName(String s) {
        this.name = s;
    }
    
    public void writeToParcel(android.os.Parcel a, int i) {
        a.writeString(this.name);
        a.writeString(this.number);
        a.writeInt((this.numberType == null) ? -1 : this.numberType.ordinal());
        a.writeInt(this.defaultImageIndex);
        a.writeLong(this.numericNumber);
        a.writeString(this.firstName);
        a.writeString(this.initials);
        a.writeString(this.numberTypeStr);
        a.writeString(this.formattedNumber);
    }
}
