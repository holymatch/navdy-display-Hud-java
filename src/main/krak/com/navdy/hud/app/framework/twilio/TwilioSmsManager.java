package com.navdy.hud.app.framework.twilio;

public class TwilioSmsManager {
    final private static String ACCOUNT_SID;
    final private static String AUTH_TOKEN;
    final private static int CONNECTION_TIMEOUT;
    final private static String CREDENTIALS;
    final private static String MESSAGE_SERVICE_ID;
    final private static int READ_TIMEOUT;
    final private static String SMS_ENDPOINT;
    final private static int WRITE_TIMEOUT;
    private static com.navdy.hud.app.framework.twilio.TwilioSmsManager instance;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    final private okhttp3.OkHttpClient okHttpClient;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.twilio.TwilioSmsManager.class);
        CONNECTION_TIMEOUT = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(15L);
        READ_TIMEOUT = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(30L);
        WRITE_TIMEOUT = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(30L);
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        ACCOUNT_SID = com.navdy.service.library.util.CredentialUtil.getCredentials(a, "TWILIO_ACCOUNT_SID");
        AUTH_TOKEN = com.navdy.service.library.util.CredentialUtil.getCredentials(a, "TWILIO_AUTH_TOKEN");
        MESSAGE_SERVICE_ID = com.navdy.service.library.util.CredentialUtil.getCredentials(a, "TWILIO_MSG_SERVICE_ID");
        SMS_ENDPOINT = new StringBuilder().append("https://api.twilio.com/2010-04-01/Accounts/").append(ACCOUNT_SID).append("/Messages.json").toString();
        CREDENTIALS = okhttp3.Credentials.basic(ACCOUNT_SID, AUTH_TOKEN);
        instance = new com.navdy.hud.app.framework.twilio.TwilioSmsManager();
    }
    
    private TwilioSmsManager() {
        this.okHttpClient = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getHttpManager().getClient().newBuilder().readTimeout((long)READ_TIMEOUT, java.util.concurrent.TimeUnit.MILLISECONDS).writeTimeout((long)WRITE_TIMEOUT, java.util.concurrent.TimeUnit.MILLISECONDS).connectTimeout((long)CONNECTION_TIMEOUT, java.util.concurrent.TimeUnit.MILLISECONDS).build();
        this.driverProfileManager = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager();
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static String access$100() {
        return MESSAGE_SERVICE_ID;
    }
    
    static String access$200() {
        return CREDENTIALS;
    }
    
    static String access$300() {
        return SMS_ENDPOINT;
    }
    
    static okhttp3.OkHttpClient access$400(com.navdy.hud.app.framework.twilio.TwilioSmsManager a) {
        return a.okHttpClient;
    }
    
    static void access$500(com.navdy.hud.app.framework.twilio.TwilioSmsManager a, com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode a0, String s, com.navdy.hud.app.framework.twilio.TwilioSmsManager$Callback a1) {
        a.sendResult(a0, s, a1);
    }
    
    public static com.navdy.hud.app.framework.twilio.TwilioSmsManager getInstance() {
        return instance;
    }
    
    private void sendResult(com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode a, String s, com.navdy.hud.app.framework.twilio.TwilioSmsManager$Callback a0) {
        label1: if (a0 != null) {
            com.navdy.hud.app.profile.DriverProfile a1 = this.driverProfileManager.getCurrentProfile();
            String s0 = a1.getProfileName();
            boolean b = a1.isDefaultProfile();
            label0: {
                if (b) {
                    break label0;
                }
                if (android.text.TextUtils.equals((CharSequence)s, (CharSequence)s0)) {
                    break label0;
                }
                sLogger.v(new StringBuilder().append("profile changed:").append(s).append(",").append(s0).toString());
                break label1;
            }
            if (com.navdy.hud.app.framework.twilio.TwilioSmsManager$2.$SwitchMap$com$navdy$hud$app$framework$twilio$TwilioSmsManager$ErrorCode[a.ordinal()] != 0) {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordSmsSent(true, "iOS", com.navdy.hud.app.framework.glance.GlanceConstants.areMessageCanned());
            } else {
                com.navdy.hud.app.analytics.AnalyticsSupport.recordSmsSent(false, "iOS", com.navdy.hud.app.framework.glance.GlanceConstants.areMessageCanned());
            }
            a0.result(a);
        }
    }
    
    public com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode sendSms(String s, String s0, com.navdy.hud.app.framework.twilio.TwilioSmsManager$Callback a) {
        com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode a0 = null;
        label2: {
            label0: {
                label1: {
                    if (s == null) {
                        break label1;
                    }
                    if (s0 != null) {
                        break label0;
                    }
                }
                a0 = com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode.INVALID_PARAMETER;
                break label2;
            }
            try {
                if (com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(com.navdy.hud.app.HudApplication.getAppContext())) {
                    String s1 = this.driverProfileManager.getCurrentProfile().getProfileName();
                    com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.twilio.TwilioSmsManager$1(this, s0, s, s1, a), 23);
                    a0 = com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode.SUCCESS;
                } else {
                    a0 = com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode.NETWORK_ERROR;
                }
            } catch(Throwable a1) {
                sLogger.e("sendSms", a1);
                a0 = com.navdy.hud.app.framework.twilio.TwilioSmsManager$ErrorCode.INTERNAL_ERROR;
            }
        }
        return a0;
    }
}
