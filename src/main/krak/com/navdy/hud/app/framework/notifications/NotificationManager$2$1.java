package com.navdy.hud.app.framework.notifications;

class NotificationManager$2$1 implements Runnable {
    final com.navdy.hud.app.framework.notifications.NotificationManager$2 this$1;
    final com.navdy.hud.app.ui.component.carousel.CarouselIndicator val$indicator;
    final com.navdy.hud.app.framework.notifications.INotification val$notificationObj;
    
    NotificationManager$2$1(com.navdy.hud.app.framework.notifications.NotificationManager$2 a, com.navdy.hud.app.framework.notifications.INotification a0, com.navdy.hud.app.ui.component.carousel.CarouselIndicator a1) {
        super();
        this.this$1 = a;
        this.val$notificationObj = a0;
        this.val$indicator = a1;
    }
    
    public void run() {
        com.navdy.hud.app.framework.notifications.NotificationManager.sLogger.v("calling onExpandedNotificationEvent-expand");
        this.val$notificationObj.onExpandedNotificationEvent(com.navdy.hud.app.ui.framework.UIStateManager$Mode.EXPAND);
        this.val$indicator.setVisibility(0);
        com.navdy.hud.app.framework.notifications.NotificationManager.access$1600(this.this$1.this$0, this.val$indicator.getCurrentItem());
    }
}
