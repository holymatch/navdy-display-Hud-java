package com.navdy.hud.app.framework.notifications;

class NotificationManager$14 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.framework.notifications.NotificationManager this$0;
    final int val$color;
    final android.view.View val$large;
    final com.navdy.hud.app.framework.notifications.NotificationManager$Info val$notifInfo;
    final boolean val$previous;
    final android.view.View val$small;
    
    NotificationManager$14(com.navdy.hud.app.framework.notifications.NotificationManager a, android.view.View a0, android.view.View a1, com.navdy.hud.app.framework.notifications.NotificationManager$Info a2, boolean b, int i) {
        super();
        this.this$0 = a;
        this.val$small = a0;
        this.val$large = a1;
        this.val$notifInfo = a2;
        this.val$previous = b;
        this.val$color = i;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        this.this$0.viewSwitchAnimation(this.val$small, this.val$large, this.val$notifInfo, this.val$previous, this.val$color);
    }
}
