package com.navdy.hud.app.framework.voice;
import com.navdy.hud.app.R;

public class VoiceSearchTipsView$$ViewInjector {
    public VoiceSearchTipsView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.framework.voice.VoiceSearchTipsView a0, Object a1) {
        a0.tipsText1 = (android.widget.TextView)a.findRequiredView(a1, R.id.tip_text_1, "field 'tipsText1'");
        a0.tipsText2 = (android.widget.TextView)a.findRequiredView(a1, R.id.tip_text_2, "field 'tipsText2'");
        a0.tipsIcon1 = (android.widget.ImageView)a.findRequiredView(a1, R.id.tip_icon_1, "field 'tipsIcon1'");
        a0.tipsIcon2 = (android.widget.ImageView)a.findRequiredView(a1, R.id.tip_icon_2, "field 'tipsIcon2'");
        a0.holder1 = (android.view.ViewGroup)a.findRequiredView(a1, R.id.tip_holder_1, "field 'holder1'");
        a0.holder2 = (android.view.ViewGroup)a.findRequiredView(a1, R.id.tip_holder_2, "field 'holder2'");
        a0.voiceSearchInformationView = a.findRequiredView(a1, R.id.voice_search_information, "field 'voiceSearchInformationView'");
        a0.audioSourceText = (android.widget.TextView)a.findRequiredView(a1, R.id.audio_source_text, "field 'audioSourceText'");
        a0.audioSourceIcon = (android.widget.ImageView)a.findRequiredView(a1, R.id.audio_source_icon, "field 'audioSourceIcon'");
        a0.localeTagText = (android.widget.TextView)a.findRequiredView(a1, R.id.locale_tag_text, "field 'localeTagText'");
        a0.localeFullNameText = (android.widget.TextView)a.findRequiredView(a1, R.id.locale_full_text, "field 'localeFullNameText'");
    }
    
    public static void reset(com.navdy.hud.app.framework.voice.VoiceSearchTipsView a) {
        a.tipsText1 = null;
        a.tipsText2 = null;
        a.tipsIcon1 = null;
        a.tipsIcon2 = null;
        a.holder1 = null;
        a.holder2 = null;
        a.voiceSearchInformationView = null;
        a.audioSourceText = null;
        a.audioSourceIcon = null;
        a.localeTagText = null;
        a.localeFullNameText = null;
    }
}
