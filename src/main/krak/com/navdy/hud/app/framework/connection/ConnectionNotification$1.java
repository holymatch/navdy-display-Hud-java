package com.navdy.hud.app.framework.connection;

final class ConnectionNotification$1 implements com.navdy.hud.app.framework.toast.IToastCallback {
    com.navdy.hud.app.ui.component.image.InitialsImageView driverImage;
    com.navdy.hud.app.view.ToastView toastView;
    final java.io.File val$loadImagePath;
    final boolean val$loadImageRequired;
    final com.navdy.hud.app.profile.DriverProfile val$prevProfile;
    
    ConnectionNotification$1(boolean b, java.io.File a, com.navdy.hud.app.profile.DriverProfile a0) {
        super();
        this.val$loadImageRequired = b;
        this.val$loadImagePath = a;
        this.val$prevProfile = a0;
    }
    
    public void executeChoiceItem(int i, int i0) {
        if (this.toastView != null) {
            switch(i0) {
                case 3: {
                    com.navdy.hud.app.framework.connection.ConnectionNotification.access$100().post(new com.navdy.hud.app.event.Disconnect());
                    this.toastView.dismissToast();
                    break;
                }
                case 2: {
                    this.toastView.dismissToast();
                    com.navdy.hud.app.framework.connection.ConnectionNotification.access$100().post(new com.navdy.hud.app.service.ConnectionHandler$ApplicationLaunchAttempted());
                    com.navdy.hud.app.framework.connection.ConnectionNotification.access$100().post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)new com.navdy.service.library.events.input.LaunchAppEvent((String)null)));
                    break;
                }
                case 1: {
                    this.toastView.dismissToast();
                    break;
                }
                case 0: {
                    this.toastView.dismissToast();
                    android.os.Bundle a = new android.os.Bundle();
                    a.putString(com.navdy.hud.app.screen.WelcomeScreen.ARG_ACTION, com.navdy.hud.app.screen.WelcomeScreen.ACTION_RECONNECT);
                    com.navdy.hud.app.framework.connection.ConnectionNotification.access$100().post(new com.navdy.hud.app.event.ShowScreenWithArgs(com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME, a, false));
                    break;
                }
            }
        }
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        return false;
    }
    
    public void onStart(com.navdy.hud.app.view.ToastView a) {
        this.toastView = a;
        this.driverImage = a.getMainLayout().screenImage;
        this.driverImage.setTag("disconnection#toast");
        if (this.val$loadImageRequired) {
            com.navdy.hud.app.framework.connection.ConnectionNotification.access$000(this.driverImage, "disconnection#toast", this.val$loadImagePath, this.val$prevProfile);
        }
    }
    
    public void onStop() {
        this.toastView = null;
        this.driverImage.setTag(null);
        this.driverImage = null;
    }
}
