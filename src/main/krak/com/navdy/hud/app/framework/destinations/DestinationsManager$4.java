package com.navdy.hud.app.framework.destinations;

class DestinationsManager$4 implements Runnable {
    final com.navdy.hud.app.framework.destinations.DestinationsManager this$0;
    final com.navdy.service.library.events.places.SuggestedDestination val$suggestedDestination;
    
    DestinationsManager$4(com.navdy.hud.app.framework.destinations.DestinationsManager a, com.navdy.service.library.events.places.SuggestedDestination a0) {
        super();
        this.this$0 = a;
        this.val$suggestedDestination = a0;
    }
    
    public void run() {
        com.navdy.hud.app.framework.destinations.DestinationsManager.access$200().v(new StringBuilder().append("onDestinationSuggestion:").append(this.val$suggestedDestination).toString());
        com.navdy.hud.app.maps.here.HereMapsManager a = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        label1: if (a.isInitialized()) {
            if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) {
                com.navdy.hud.app.framework.destinations.DestinationsManager.access$200().e("onDestinationSuggestion: nav mode is on");
            } else {
                com.navdy.hud.app.ui.component.homescreen.HomeScreenView a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView();
                label0: {
                    if (a0 == null) {
                        break label0;
                    }
                    if (!com.navdy.hud.app.maps.here.HereRouteManager.isUIShowingRouteCalculation()) {
                        break label0;
                    }
                    com.navdy.hud.app.framework.destinations.DestinationsManager.access$200().e("onDestinationSuggestion: route calc is on");
                    break label1;
                }
                com.navdy.hud.app.framework.destinations.DestinationsManager.access$402(this.this$0, this.val$suggestedDestination);
                if (com.navdy.hud.app.maps.here.HereMapUtil.getSavedRouteData(com.navdy.hud.app.framework.destinations.DestinationsManager.access$200()).navigationRouteRequest == null) {
                    if (a.getLocationFixManager().getLastGeoCoordinate() != null) {
                        this.this$0.launchSuggestedDestination();
                    } else {
                        com.navdy.hud.app.framework.destinations.DestinationsManager.access$200().e("onDestinationSuggestion: no current position");
                    }
                } else {
                    com.navdy.hud.app.framework.destinations.DestinationsManager.access$200().e("onDestinationSuggestion: has saved route data");
                }
            }
        } else {
            com.navdy.hud.app.framework.destinations.DestinationsManager.access$200().e("onDestinationSuggestion: Map engine not initialized, cannot process suggestion, at this time");
            com.navdy.hud.app.framework.destinations.DestinationsManager.access$402(this.this$0, this.val$suggestedDestination);
        }
    }
}
