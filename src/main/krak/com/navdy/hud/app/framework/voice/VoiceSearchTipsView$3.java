package com.navdy.hud.app.framework.voice;

class VoiceSearchTipsView$3 implements Runnable {
    final com.navdy.hud.app.framework.voice.VoiceSearchTipsView this$0;
    
    VoiceSearchTipsView$3(com.navdy.hud.app.framework.voice.VoiceSearchTipsView a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$600(this.this$0)) {
            com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$202(this.this$0, 0);
            String s = com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$400()[com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$200(this.this$0)];
            int i = com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$500(this.this$0, com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$200(this.this$0));
            this.this$0.tipsText1.setText((CharSequence)s);
            this.this$0.tipsIcon1.setImageResource(i);
            this.this$0.holder1.setAlpha(1f);
            this.this$0.holder2.setAlpha(0.0f);
            com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$602(this.this$0, false);
        } else {
            int i0 = (com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$200(this.this$0) + 1) % com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$300();
            String s0 = com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$400()[i0];
            int i1 = com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$500(this.this$0, i0);
            this.this$0.tipsText2.setText((CharSequence)s0);
            this.this$0.tipsIcon2.setImageResource(i1);
            com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$700(this.this$0).start();
        }
        com.navdy.hud.app.framework.voice.VoiceSearchTipsView.access$100(this.this$0).postDelayed((Runnable)this, 3000L);
    }
}
