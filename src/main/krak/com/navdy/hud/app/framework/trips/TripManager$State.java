package com.navdy.hud.app.framework.trips;


public enum TripManager$State {
    STARTED(0),
    STOPPED(1);

    private int value;
    TripManager$State(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class TripManager$State extends Enum {
//    final private static com.navdy.hud.app.framework.trips.TripManager$State[] $VALUES;
//    final public static com.navdy.hud.app.framework.trips.TripManager$State STARTED;
//    final public static com.navdy.hud.app.framework.trips.TripManager$State STOPPED;
//    
//    static {
//        STARTED = new com.navdy.hud.app.framework.trips.TripManager$State("STARTED", 0);
//        STOPPED = new com.navdy.hud.app.framework.trips.TripManager$State("STOPPED", 1);
//        com.navdy.hud.app.framework.trips.TripManager$State[] a = new com.navdy.hud.app.framework.trips.TripManager$State[2];
//        a[0] = STARTED;
//        a[1] = STOPPED;
//        $VALUES = a;
//    }
//    
//    private TripManager$State(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.framework.trips.TripManager$State valueOf(String s) {
//        return (com.navdy.hud.app.framework.trips.TripManager$State)Enum.valueOf(com.navdy.hud.app.framework.trips.TripManager$State.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.trips.TripManager$State[] values() {
//        return $VALUES.clone();
//    }
//}
//