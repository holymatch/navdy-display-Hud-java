package com.navdy.hud.app.framework.contacts;
import com.navdy.hud.app.R;

public class ContactImageHelper {
    final public static int DEFAULT_IMAGE_INDEX = -1;
    public static int NO_CONTACT_COLOR = 0;
    final public static int NO_CONTACT_IMAGE = R.drawable.icon_user_numberonly;
    final private static java.util.Map sContactColorMap;
    final private static java.util.Map sContactImageMap;
    final private static com.navdy.hud.app.framework.contacts.ContactImageHelper sInstance;
    
    static {
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        sContactImageMap = (java.util.Map)new java.util.HashMap();
        sContactColorMap = (java.util.Map)new java.util.HashMap();
        NO_CONTACT_COLOR = a.getColor(R.color.grey_4a);
        sContactColorMap.put(Integer.valueOf(0), Integer.valueOf(a.getColor(R.color.icon_user_bg_0)));
        sContactImageMap.put(Integer.valueOf(0), Integer.valueOf(R.drawable.icon_user_bg_0));
        sContactColorMap.put(Integer.valueOf(1), Integer.valueOf(a.getColor(R.color.icon_user_bg_1)));
        sContactImageMap.put(Integer.valueOf(1), Integer.valueOf(R.drawable.icon_user_bg_1));
        sContactColorMap.put(Integer.valueOf(2), Integer.valueOf(a.getColor(R.color.icon_user_bg_2)));
        sContactImageMap.put(Integer.valueOf(2), Integer.valueOf(R.drawable.icon_user_bg_2));
        sContactColorMap.put(Integer.valueOf(3), Integer.valueOf(a.getColor(R.color.icon_user_bg_3)));
        sContactImageMap.put(Integer.valueOf(3), Integer.valueOf(R.drawable.icon_user_bg_3));
        sContactColorMap.put(Integer.valueOf(4), Integer.valueOf(a.getColor(R.color.icon_user_bg_4)));
        sContactImageMap.put(Integer.valueOf(4), Integer.valueOf(R.drawable.icon_user_bg_4));
        sContactColorMap.put(Integer.valueOf(5), Integer.valueOf(a.getColor(R.color.icon_user_bg_5)));
        sContactImageMap.put(Integer.valueOf(5), Integer.valueOf(R.drawable.icon_user_bg_5));
        sContactColorMap.put(Integer.valueOf(6), Integer.valueOf(a.getColor(R.color.icon_user_bg_6)));
        sContactImageMap.put(Integer.valueOf(6), Integer.valueOf(R.drawable.icon_user_bg_6));
        sContactColorMap.put(Integer.valueOf(7), Integer.valueOf(a.getColor(R.color.icon_user_bg_7)));
        sContactImageMap.put(Integer.valueOf(7), Integer.valueOf(R.drawable.icon_user_bg_7));
        sContactColorMap.put(Integer.valueOf(8), Integer.valueOf(a.getColor(R.color.icon_user_bg_8)));
        sContactImageMap.put(Integer.valueOf(8), Integer.valueOf(R.drawable.icon_user_bg_8));
        sInstance = new com.navdy.hud.app.framework.contacts.ContactImageHelper();
    }
    
    public ContactImageHelper() {
    }
    
    public static com.navdy.hud.app.framework.contacts.ContactImageHelper getInstance() {
        return sInstance;
    }
    
    public int getContactImageIndex(String s) {
        int i = 0;
        if (s != null) {
            String s0 = com.navdy.hud.app.util.GenericUtil.normalizeToFilename(s);
            if (com.navdy.hud.app.framework.contacts.ContactUtil.isValidNumber(s0)) {
                s0 = com.navdy.hud.app.util.PhoneUtil.convertToE164Format(s0);
            }
            i = Math.abs(s0.hashCode() % sContactImageMap.size());
        } else {
            i = -1;
        }
        return i;
    }
    
    public int getDriverImageResId(String s) {
        return this.getResourceId(Math.abs(com.navdy.hud.app.util.GenericUtil.normalizeToFilename(s).hashCode() % sContactImageMap.size()));
    }
    
    public int getResourceColor(int i) {
        int i0 = 0;
        label2: {
            label0: {
                label1: {
                    if (i < 0) {
                        break label1;
                    }
                    if (i < sContactColorMap.size()) {
                        break label0;
                    }
                }
                i0 = NO_CONTACT_COLOR;
                break label2;
            }
            i0 = ((Integer)sContactColorMap.get(Integer.valueOf(i))).intValue();
        }
        return i0;
    }
    
    public int getResourceId(int i) {
        int i0 = 0;
        label2: {
            label0: {
                label1: {
                    if (i < 0) {
                        break label1;
                    }
                    if (i < sContactImageMap.size()) {
                        break label0;
                    }
                }
                i0 = R.drawable.icon_user_numberonly;
                break label2;
            }
            i0 = ((Integer)sContactImageMap.get(Integer.valueOf(i))).intValue();
        }
        return i0;
    }
}
