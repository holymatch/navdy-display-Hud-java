package com.navdy.hud.app.framework.contacts;


public enum NumberType {
    HOME(0),
    MOBILE(1),
    WORK(2),
    WORK_MOBILE(3),
    OTHER(4);

    private int value;
    NumberType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class NumberType extends Enum {
//    final private static com.navdy.hud.app.framework.contacts.NumberType[] $VALUES;
//    final public static com.navdy.hud.app.framework.contacts.NumberType HOME;
//    final public static com.navdy.hud.app.framework.contacts.NumberType MOBILE;
//    final public static com.navdy.hud.app.framework.contacts.NumberType OTHER;
//    final public static com.navdy.hud.app.framework.contacts.NumberType WORK;
//    final public static com.navdy.hud.app.framework.contacts.NumberType WORK_MOBILE;
//    int value;
//    
//    static {
//        HOME = new com.navdy.hud.app.framework.contacts.NumberType("HOME", 0, 1);
//        MOBILE = new com.navdy.hud.app.framework.contacts.NumberType("MOBILE", 1, 2);
//        WORK = new com.navdy.hud.app.framework.contacts.NumberType("WORK", 2, 3);
//        WORK_MOBILE = new com.navdy.hud.app.framework.contacts.NumberType("WORK_MOBILE", 3, 4);
//        OTHER = new com.navdy.hud.app.framework.contacts.NumberType("OTHER", 4, 5);
//        com.navdy.hud.app.framework.contacts.NumberType[] a = new com.navdy.hud.app.framework.contacts.NumberType[5];
//        a[0] = HOME;
//        a[1] = MOBILE;
//        a[2] = WORK;
//        a[3] = WORK_MOBILE;
//        a[4] = OTHER;
//        $VALUES = a;
//    }
//    
//    private NumberType(String s, int i, int i0) {
//        super(s, i);
//        this.value = i0;
//    }
//    
//    public static com.navdy.hud.app.framework.contacts.NumberType buildFromValue(int i) {
//        com.navdy.hud.app.framework.contacts.NumberType a = null;
//        switch(i) {
//            case 4: {
//                a = WORK_MOBILE;
//                break;
//            }
//            case 3: {
//                a = WORK;
//                break;
//            }
//            case 2: {
//                a = MOBILE;
//                break;
//            }
//            case 1: {
//                a = HOME;
//                break;
//            }
//            default: {
//                a = OTHER;
//            }
//        }
//        return a;
//    }
//    
//    public static com.navdy.hud.app.framework.contacts.NumberType valueOf(String s) {
//        return (com.navdy.hud.app.framework.contacts.NumberType)Enum.valueOf(com.navdy.hud.app.framework.contacts.NumberType.class, s);
//    }
//    
//    public static com.navdy.hud.app.framework.contacts.NumberType[] values() {
//        return $VALUES.clone();
//    }
//    
//    public int getValue() {
//        return this.value;
//    }
//}
//