package com.navdy.hud.app.framework.fuel;

class FuelRoutingManager$FuelRoutingAction implements Runnable {
    private int currentRouteCacheSize;
    final private com.here.android.mpa.common.GeoCoordinate endPoint;
    final private com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback onRouteToGasStationCallback;
    final private com.here.android.mpa.search.Place place;
    final private java.util.List routeCache;
    final private java.util.Queue routingQueue;
    final private com.here.android.mpa.common.GeoCoordinate routingStart;
    final com.navdy.hud.app.framework.fuel.FuelRoutingManager this$0;
    final private java.util.List waypoints;
    
    public FuelRoutingManager$FuelRoutingAction(com.navdy.hud.app.framework.fuel.FuelRoutingManager a, int i, com.here.android.mpa.search.Place a0, java.util.Queue a1, java.util.List a2, com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback a3, com.here.android.mpa.common.GeoCoordinate a4, java.util.List a5, com.here.android.mpa.common.GeoCoordinate a6) {
        super();
        this.this$0 = a;
        this.place = a0;
        this.routingQueue = a1;
        this.routeCache = a2;
        this.onRouteToGasStationCallback = a3;
        this.routingStart = a4;
        this.waypoints = a5;
        this.endPoint = a6;
        this.currentRouteCacheSize = i;
    }
    
    static com.here.android.mpa.search.Place access$1400(com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction a) {
        return a.place;
    }
    
    static java.util.List access$1500(com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction a) {
        return a.routeCache;
    }
    
    static int access$1600(com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction a) {
        return a.currentRouteCacheSize;
    }
    
    static int access$1602(com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction a, int i) {
        a.currentRouteCacheSize = i;
        return i;
    }
    
    static com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback access$1700(com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction a) {
        return a.onRouteToGasStationCallback;
    }
    
    static java.util.Queue access$1900(com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction a) {
        return a.routingQueue;
    }
    
    public void run() {
        if (com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$600(this.this$0)) {
            com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$2100(this.this$0).calculateRoute((com.navdy.service.library.events.navigation.NavigationRouteRequest)null, this.routingStart, this.waypoints, this.endPoint, false, (com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener)new com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelRoutingAction$1(this), 1, com.navdy.hud.app.framework.fuel.FuelRoutingManager.access$2000(this.this$0), false, false, false);
        }
    }
}
