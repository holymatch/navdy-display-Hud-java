package com.navdy.hud.app.framework.message;

class SmsNotification$1 implements Runnable {
    final com.navdy.hud.app.framework.message.SmsNotification this$0;
    
    SmsNotification$1(com.navdy.hud.app.framework.message.SmsNotification a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.framework.glance.GlanceHandler a = com.navdy.hud.app.framework.glance.GlanceHandler.getInstance();
        if (a.sendMessage(com.navdy.hud.app.framework.message.SmsNotification.access$000(this.this$0), com.navdy.hud.app.framework.message.SmsNotification.access$100(this.this$0), com.navdy.hud.app.framework.message.SmsNotification.access$200(this.this$0))) {
            com.navdy.hud.app.framework.message.SmsNotification.access$300().v("retry message sent");
            a.sendSmsSuccessNotification(com.navdy.hud.app.framework.message.SmsNotification.access$500(this.this$0), com.navdy.hud.app.framework.message.SmsNotification.access$400(this.this$0), com.navdy.hud.app.framework.message.SmsNotification.access$100(this.this$0), com.navdy.hud.app.framework.message.SmsNotification.access$200(this.this$0));
        } else {
            com.navdy.hud.app.framework.message.SmsNotification.access$300().v("retry message not sent");
            a.sendSmsFailedNotification(com.navdy.hud.app.framework.message.SmsNotification.access$400(this.this$0), com.navdy.hud.app.framework.message.SmsNotification.access$100(this.this$0), com.navdy.hud.app.framework.message.SmsNotification.access$200(this.this$0));
        }
    }
}
