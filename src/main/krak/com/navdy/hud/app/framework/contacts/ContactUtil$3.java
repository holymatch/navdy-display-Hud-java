package com.navdy.hud.app.framework.contacts;

class ContactUtil$3 {
    final static int[] $SwitchMap$com$navdy$hud$app$framework$contacts$NumberType;
    final static int[] $SwitchMap$com$navdy$service$library$events$contacts$PhoneNumberType;
    
    static {
        $SwitchMap$com$navdy$service$library$events$contacts$PhoneNumberType = new int[com.navdy.service.library.events.contacts.PhoneNumberType.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$contacts$PhoneNumberType;
        com.navdy.service.library.events.contacts.PhoneNumberType a0 = com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_HOME;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$contacts$PhoneNumberType[com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_MOBILE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$contacts$PhoneNumberType[com.navdy.service.library.events.contacts.PhoneNumberType.PHONE_NUMBER_WORK.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        $SwitchMap$com$navdy$hud$app$framework$contacts$NumberType = new int[com.navdy.hud.app.framework.contacts.NumberType.values().length];
        int[] a1 = $SwitchMap$com$navdy$hud$app$framework$contacts$NumberType;
        com.navdy.hud.app.framework.contacts.NumberType a2 = com.navdy.hud.app.framework.contacts.NumberType.WORK_MOBILE;
        try {
            a1[a2.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$contacts$NumberType[com.navdy.hud.app.framework.contacts.NumberType.WORK.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$contacts$NumberType[com.navdy.hud.app.framework.contacts.NumberType.MOBILE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$framework$contacts$NumberType[com.navdy.hud.app.framework.contacts.NumberType.HOME.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException5) {
        }
    }
}
