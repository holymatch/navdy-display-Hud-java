package com.navdy.hud.app.framework.voice;

final class VoiceSearchNotification$6 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    private boolean canceled;
    final android.animation.AnimatorSet val$animationSet;
    
    VoiceSearchNotification$6(android.animation.AnimatorSet a) {
        super();
        this.val$animationSet = a;
        this.canceled = false;
    }
    
    public void onAnimationCancel(android.animation.Animator a) {
        this.canceled = true;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        if (!this.canceled) {
            this.val$animationSet.start();
        }
    }
    
    public void onAnimationStart(android.animation.Animator a) {
        this.canceled = false;
    }
}
