package com.navdy.hud.app.framework.fuel;
import com.navdy.hud.app.R;

final public class FuelRoutingManager {
    final private static int CANCEL_NOTIFICATION_THRESHOLD = 20;
    final private static int DEFAULT_LOW_FUEL_THRESHOLD = 15;
    final public static String FINDING_FAST_STATION_TOAST_ID = "toast#find#gas";
    final private static long FUEL_GLANCE_REDISPLAY_THRESHOLD;
    final public static com.here.android.mpa.search.CategoryFilter GAS_CATEGORY;
    final private static String GAS_STATION_CATEGORY = "petrol-station";
    final public static String LOW_FUEL_ID;
    final private static String LOW_FUEL_ID_STR = "low#fuel#level";
    final private static int N_GAS_STATIONS_REQUESTED = 3;
    final private static int ONE_MINUTE_SECONDS = 60;
    final private static long TEST_SIMULATION_SPEED = 20L;
    private static com.navdy.hud.app.framework.fuel.FuelRoutingManager sInstance;
    final private static com.navdy.service.library.log.Logger sLogger;
    private boolean available;
    final private com.squareup.otto.Bus bus;
    private com.here.android.mpa.search.Place currentGasStation;
    private int currentLowFuelThreshold;
    private com.navdy.service.library.events.navigation.NavigationRouteRequest currentNavigationRouteRequest;
    private java.util.ArrayList currentOutgoingResults;
    private long fuelGlanceDismissTime;
    private com.here.android.mpa.routing.RouteOptions gasRouteOptions;
    final private android.os.Handler handler;
    final private com.navdy.hud.app.maps.here.HereMapsManager hereMapsManager;
    private com.navdy.hud.app.maps.here.HereNavigationManager hereNavigationManager;
    private com.navdy.hud.app.maps.here.HereRouteCalculator hereRouteCalculator;
    private boolean isCalculatingFuelRoute;
    private boolean isInTestMode;
    final private com.navdy.hud.app.obd.ObdManager obdManager;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.framework.fuel.FuelRoutingManager.class);
        FUEL_GLANCE_REDISPLAY_THRESHOLD = java.util.concurrent.TimeUnit.MINUTES.toMillis(20L);
        LOW_FUEL_ID = com.navdy.hud.app.framework.glance.GlanceHelper.getNotificationId(com.navdy.service.library.events.glances.GlanceEvent$GlanceType.GLANCE_TYPE_FUEL, "low#fuel#level");
        GAS_CATEGORY = new com.here.android.mpa.search.CategoryFilter();
        sInstance = new com.navdy.hud.app.framework.fuel.FuelRoutingManager();
        GAS_CATEGORY.add("petrol-station");
    }
    
    private FuelRoutingManager() {
        this.obdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
        this.hereMapsManager = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.isInTestMode = false;
        this.currentLowFuelThreshold = 15;
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static boolean access$100(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        return a.available;
    }
    
    static long access$1000() {
        return FUEL_GLANCE_REDISPLAY_THRESHOLD;
    }
    
    static int access$1100(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        return a.currentLowFuelThreshold;
    }
    
    static void access$1200(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        a.setFuelLevelBackToNormal();
    }
    
    static android.os.Handler access$1300(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        return a.handler;
    }
    
    static void access$1800(com.navdy.hud.app.framework.fuel.FuelRoutingManager a, java.util.List a0, com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback a1) {
        a.calculateOptimalGasStation(a0, a1);
    }
    
    static java.util.ArrayList access$200(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        return a.currentOutgoingResults;
    }
    
    static com.here.android.mpa.routing.RouteOptions access$2000(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        return a.gasRouteOptions;
    }
    
    static java.util.ArrayList access$202(com.navdy.hud.app.framework.fuel.FuelRoutingManager a, java.util.ArrayList a0) {
        a.currentOutgoingResults = a0;
        return a0;
    }
    
    static com.navdy.hud.app.maps.here.HereRouteCalculator access$2100(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        return a.hereRouteCalculator;
    }
    
    static com.here.android.mpa.search.Place access$2202(com.navdy.hud.app.framework.fuel.FuelRoutingManager a, com.here.android.mpa.search.Place a0) {
        a.currentGasStation = a0;
        return a0;
    }
    
    static com.navdy.hud.app.maps.here.HereMapsManager access$2300(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        return a.hereMapsManager;
    }
    
    static com.squareup.otto.Bus access$2400(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        return a.bus;
    }
    
    static void access$2500(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        a.checkFuelLevel();
    }
    
    static com.navdy.hud.app.maps.here.HereNavigationManager access$300(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        return a.hereNavigationManager;
    }
    
    static boolean access$400(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        return a.isInTestMode;
    }
    
    static com.navdy.service.library.events.navigation.NavigationRouteRequest access$500(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        return a.currentNavigationRouteRequest;
    }
    
    static com.navdy.service.library.events.navigation.NavigationRouteRequest access$502(com.navdy.hud.app.framework.fuel.FuelRoutingManager a, com.navdy.service.library.events.navigation.NavigationRouteRequest a0) {
        a.currentNavigationRouteRequest = a0;
        return a0;
    }
    
    static boolean access$600(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        return a.isCalculatingFuelRoute;
    }
    
    static boolean access$602(com.navdy.hud.app.framework.fuel.FuelRoutingManager a, boolean b) {
        a.isCalculatingFuelRoute = b;
        return b;
    }
    
    static void access$700(com.navdy.hud.app.framework.fuel.FuelRoutingManager a, com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback a0) {
        a.calculateGasStationRoutes(a0);
    }
    
    static com.navdy.hud.app.obd.ObdManager access$800(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        return a.obdManager;
    }
    
    static long access$900(com.navdy.hud.app.framework.fuel.FuelRoutingManager a) {
        return a.fuelGlanceDismissTime;
    }
    
    static long access$902(com.navdy.hud.app.framework.fuel.FuelRoutingManager a, long j) {
        a.fuelGlanceDismissTime = j;
        return j;
    }
    
    private void calculateGasStationRoutes(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback a) {
        if (a == null) {
            throw new IllegalArgumentException();
        }
        com.here.android.mpa.common.GeoCoordinate a0 = this.getBestInitialGeo();
        if (a0 != null) {
            com.navdy.hud.app.maps.here.HerePlacesManager.handleCategoriesRequest(GAS_CATEGORY, 3, (com.navdy.hud.app.maps.here.HerePlacesManager$OnCategoriesSearchListener)new com.navdy.hud.app.framework.fuel.FuelRoutingManager$4(this, a, a0));
        } else {
            sLogger.w("No user location while calculating routes to gas station");
            a.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error.NO_USER_LOCATION);
        }
    }
    
    private void calculateOptimalGasStation(java.util.List a, com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback a0) {
        java.util.Iterator a1 = a.iterator();
        com.navdy.hud.app.framework.fuel.FuelRoutingManager$RouteCacheItem a2 = null;
        Object a3 = a0;
        int i = -1;
        Object a4 = a1;
        while(true) {
            boolean b = ((java.util.Iterator)a4).hasNext();
            com.navdy.hud.app.framework.fuel.FuelRoutingManager$RouteCacheItem a5 = a2;
            int i0 = i;
            if (b) {
                a2 = (com.navdy.hud.app.framework.fuel.FuelRoutingManager$RouteCacheItem)((java.util.Iterator)a4).next();
                i = a2.route.duration.intValue();
                String s = a2.gasStation.getName();
                int i1 = (int)a2.gasStation.getLocation().getCoordinate().distanceTo(this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate());
                sLogger.v(new StringBuilder().append("evaluating [").append(s).append("] distance [").append(i1).append("] tta [").append(i).append("]").toString());
                if (i0 >= 0 && i >= i0) {
                    a2 = a5;
                    i = i0;
                }
            } else {
                if (a2 == null) {
                    sLogger.w("No routes to any gas stations");
                    ((com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback)a3).onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error.NO_ROUTES);
                } else {
                    sLogger.v(new StringBuilder().append("Optimal route: \n\tName:").append(a2.gasStation.getName()).append("\n\tAddress: ").append(a2.gasStation.getLocation().getAddress()).append("\n\tTTA: ").append(i).append(" s\n\tGas station distance: ").append(a2.gasStation.getLocation().getCoordinate().distanceTo(this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate())).append(" m").toString());
                    this.requestOptimalRoute(a2, (com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback)a3);
                }
                return;
            }
        }
    }
    
    private void checkFuelLevel() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.fuel.FuelRoutingManager$3(this), 21);
    }
    
    private com.here.android.mpa.common.GeoCoordinate getBestInitialGeo() {
        com.here.android.mpa.common.GeoCoordinate a = null;
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.here.android.mpa.common.GeoCoordinate a0 = this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate();
        com.here.android.mpa.routing.Route a1 = this.hereNavigationManager.getCurrentRoute();
        if (a1 == null) {
            a = a0;
        } else {
            com.here.android.mpa.routing.RouteElements a2 = a1.getRouteElementsFromLength((int)this.hereNavigationManager.getNavController().getElapsedDistance());
            if (a2 == null) {
                a = a0;
            } else {
                java.util.List a3 = a2.getElements();
                if (a3 == null) {
                    a = a0;
                } else {
                    java.util.Iterator a4 = a3.iterator();
                    int i = 0;
                    Object a5 = a4;
                    while(true) {
                        boolean b = ((java.util.Iterator)a5).hasNext();
                        a = null;
                        if (!b) {
                            break;
                        }
                        com.here.android.mpa.routing.RouteElement a6 = (com.here.android.mpa.routing.RouteElement)((java.util.Iterator)a5).next();
                        com.here.android.mpa.common.RoadElement a7 = a6.getRoadElement();
                        if (a7 != null && a7.getDefaultSpeed() > 0.0f) {
                            i = (int)((long)i + Math.round(a7.getGeometryLength() / (double)a7.getDefaultSpeed()));
                        }
                        {
                            if (i <= 60) {
                                continue;
                            }
                            java.util.List a8 = a6.getGeometry();
                            if (a8 == null) {
                                continue;
                            }
                            if (a8.size() <= 0) {
                                continue;
                            }
                            a = (com.here.android.mpa.common.GeoCoordinate)a8.get(a8.size() - 1);
                            break;
                        }
                    }
                    if (a == null) {
                        a = a0;
                    }
                }
            }
        }
        return a;
    }
    
    public static com.navdy.hud.app.framework.fuel.FuelRoutingManager getInstance() {
        return sInstance;
    }
    
    private void requestOptimalRoute(com.navdy.hud.app.framework.fuel.FuelRoutingManager$RouteCacheItem a, com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback a0) {
        try {
            com.here.android.mpa.common.GeoCoordinate a1 = com.navdy.hud.app.maps.here.HerePlacesManager.getPlaceEntry(a.gasStation);
            com.here.android.mpa.common.GeoCoordinate a2 = a.gasStation.getLocation().getCoordinate();
            com.navdy.service.library.events.location.Coordinate a3 = new com.navdy.service.library.events.location.Coordinate$Builder().latitude(Double.valueOf(a1.getLatitude())).longitude(Double.valueOf(a1.getLongitude())).build();
            com.navdy.service.library.events.location.Coordinate a4 = new com.navdy.service.library.events.location.Coordinate$Builder().latitude(Double.valueOf(a2.getLatitude())).longitude(Double.valueOf(a2.getLongitude())).build();
            java.util.ArrayList a5 = new java.util.ArrayList(1);
            ((java.util.List)a5).add(com.navdy.service.library.events.navigation.NavigationRouteRequest$RouteAttribute.ROUTE_ATTRIBUTE_GAS);
            com.navdy.service.library.events.navigation.NavigationRouteRequest a6 = new com.navdy.service.library.events.navigation.NavigationRouteRequest$Builder().destination(a3).label(a.gasStation.getName()).streetAddress(a.gasStation.getLocation().getAddress().toString()).destination_identifier(a.gasStation.getId()).originDisplay(Boolean.valueOf(true)).geoCodeStreetAddress(Boolean.valueOf(false)).destinationType(com.navdy.service.library.events.destination.Destination$FavoriteType.FAVORITE_NONE).requestId(java.util.UUID.randomUUID().toString()).destinationDisplay(a4).routeAttributes((java.util.List)a5).build();
            this.hereRouteCalculator.calculateRoute(a6, this.hereMapsManager.getLocationFixManager().getLastGeoCoordinate(), (java.util.List)null, a1, true, (com.navdy.hud.app.maps.here.HereRouteCalculator$RouteCalculatorListener)new com.navdy.hud.app.framework.fuel.FuelRoutingManager$5(this, a0, a, a6), 1, this.gasRouteOptions, true, true, false);
        } catch(Throwable a7) {
            sLogger.e("requestOptimalRoute", a7);
            a0.onError(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnRouteToGasStationCallback$Error.UNKNOWN_ERROR);
        }
    }
    
    private void reset(int i) {
        this.hereRouteCalculator.cancel();
        if (this.hereNavigationManager.isOnGasRoute()) {
            this.hereNavigationManager.arrived();
        }
        this.currentLowFuelThreshold = i;
        this.isInTestMode = false;
        this.currentOutgoingResults = null;
        this.currentNavigationRouteRequest = null;
        this.currentGasStation = null;
        if (sLogger.isLoggable(2)) {
            sLogger.v("reset, state is now TRACKING");
        }
    }
    
    private void setFuelLevelBackToNormal() {
        sLogger.i("checkFuelLevel:Fuel level normal");
        this.reset();
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().removeNotification(LOW_FUEL_ID);
        this.fuelGlanceDismissTime = 0L;
    }
    
    public void dismissGasRoute() {
        sLogger.v("dismissGasRoute");
        int i = this.currentLowFuelThreshold / 2;
        this.currentLowFuelThreshold = i;
        this.reset(i);
        this.fuelGlanceDismissTime = android.os.SystemClock.elapsedRealtime();
    }
    
    public void findGasStations(int i, boolean b) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.fuel.FuelRoutingManager$6(this, i, b), 21);
    }
    
    public void findNearestGasStation(com.navdy.hud.app.framework.fuel.FuelRoutingManager$OnNearestGasStationCallback a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.fuel.FuelRoutingManager$2(this, a), 21);
    }
    
    public com.here.android.mpa.search.Place getCurrentGasStation() {
        return this.currentGasStation;
    }
    
    public long getFuelGlanceDismissTime() {
        return this.fuelGlanceDismissTime;
    }
    
    public boolean isAvailable() {
        return this.available;
    }
    
    public boolean isBusy() {
        boolean b = false;
        boolean b0 = this.isCalculatingFuelRoute;
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (this.hereNavigationManager.isOnGasRoute()) {
                        break label1;
                    }
                    if (!com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().isNotificationPresent(LOW_FUEL_ID)) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public void markAvailable() {
        if (!this.available) {
            this.available = true;
            this.hereNavigationManager = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            this.hereRouteCalculator = new com.navdy.hud.app.maps.here.HereRouteCalculator(sLogger, false);
            this.gasRouteOptions = new com.here.android.mpa.routing.RouteOptions();
            this.gasRouteOptions.setRouteCount(1);
            this.gasRouteOptions.setTransportMode(com.here.android.mpa.routing.RouteOptions$TransportMode.CAR);
            this.gasRouteOptions.setRouteType(com.here.android.mpa.routing.RouteOptions$Type.FASTEST);
            this.bus.register(this);
            this.handler.postDelayed((Runnable)new com.navdy.hud.app.framework.fuel.FuelRoutingManager$7(this), 20000L);
            sLogger.v("mark available");
        }
    }
    
    public void onClearTestObdLowFuelLevel(com.navdy.hud.app.framework.fuel.FuelRoutingManager$ClearTestObdLowFuelLevel a) {
        this.reset();
        this.fuelGlanceDismissTime = 0L;
    }
    
    public void onFuelAddedTestEvent(com.navdy.hud.app.framework.fuel.FuelRoutingManager$FuelAddedTestEvent a) {
        this.setFuelLevelBackToNormal();
    }
    
    public void onNewRouteAdded(com.navdy.hud.app.maps.MapEvents$NewRouteAdded a) {
        if (sLogger.isLoggable(2)) {
            sLogger.v("new route added, resetting gas routes");
        }
        com.navdy.hud.app.framework.notifications.NotificationManager a0 = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        if (a.rerouteReason == null && a0.isNotificationPresent(LOW_FUEL_ID)) {
            a0.removeNotification(LOW_FUEL_ID);
        }
    }
    
    public void onObdPidChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdPidChangeEvent a) {
        if (a.pid.getId() == 47) {
            this.checkFuelLevel();
        }
    }
    
    public void onTestObdLowFuelLevel(com.navdy.hud.app.framework.fuel.FuelRoutingManager$TestObdLowFuelLevel a) {
        if (!this.isInTestMode) {
            this.isInTestMode = true;
            this.checkFuelLevel();
        }
    }
    
    public void reset() {
        sLogger.v("reset");
        this.reset(15);
    }
    
    public void routeToGasStation() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.framework.fuel.FuelRoutingManager$1(this), 21);
    }
    
    public void showFindingGasStationToast() {
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        com.navdy.hud.app.framework.toast.ToastManager a0 = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        a0.dismissCurrentToast("toast#find#gas");
        a0.clearPendingToast("toast#find#gas");
        android.os.Bundle a1 = new android.os.Bundle();
        a1.putInt("13", 1500);
        a1.putInt("8", R.drawable.icon_glance_fuel_low);
        a1.putString("4", a.getString(R.string.search_gas_station));
        a1.putInt("5", R.style.Glances_1);
        a0.addToast(new com.navdy.hud.app.framework.toast.ToastManager$ToastParams("toast#find#gas", a1, (com.navdy.hud.app.framework.toast.IToastCallback)null, true, false, false));
    }
}
