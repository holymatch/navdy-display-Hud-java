package com.navdy.hud.app;
import com.navdy.hud.app.R;

final public class R$anim {
    final public static int fade_in_with_bounce = R.anim.fade_in_with_bounce;
    final public static int press_button = R.anim.press_button;
    final public static int slide_in_down = R.anim.slide_in_down;
    final public static int slide_in_left = R.anim.slide_in_left;
    final public static int slide_in_right = R.anim.slide_in_right;
    final public static int slide_in_up = R.anim.slide_in_up;
    final public static int slide_out_down = R.anim.slide_out_down;
    final public static int slide_out_left = R.anim.slide_out_left;
    final public static int slide_out_right = R.anim.slide_out_right;
    final public static int slide_out_up = R.anim.slide_out_up;
    
    public R$anim() {
    }
}
