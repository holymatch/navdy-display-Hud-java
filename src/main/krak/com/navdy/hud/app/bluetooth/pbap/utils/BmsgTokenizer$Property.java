package com.navdy.hud.app.bluetooth.pbap.utils;

public class BmsgTokenizer$Property {
    final public String name;
    final public String value;
    
    public BmsgTokenizer$Property(String s, String s0) {
        if (s != null && s0 != null) {
            this.name = s;
            this.value = s0;
            android.util.Log.v("BMSG >> ", this.toString());
            return;
        }
        throw new IllegalArgumentException();
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        boolean b0 = a instanceof com.navdy.hud.app.bluetooth.pbap.utils.BmsgTokenizer$Property;
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    com.navdy.hud.app.bluetooth.pbap.utils.BmsgTokenizer$Property a0 = (com.navdy.hud.app.bluetooth.pbap.utils.BmsgTokenizer$Property)a;
                    if (!a0.name.equals(this.name)) {
                        break label1;
                    }
                    if (((com.navdy.hud.app.bluetooth.pbap.utils.BmsgTokenizer$Property)a0).value.equals(this.value)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public String toString() {
        return new StringBuilder().append(this.name).append(":").append(this.value).toString();
    }
}
