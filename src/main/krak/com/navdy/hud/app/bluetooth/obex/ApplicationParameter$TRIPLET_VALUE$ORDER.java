package com.navdy.hud.app.bluetooth.obex;

public class ApplicationParameter$TRIPLET_VALUE$ORDER {
    final public static byte ORDER_BY_ALPHANUMERIC = (byte)1;
    final public static byte ORDER_BY_INDEX = (byte)0;
    final public static byte ORDER_BY_PHONETIC = (byte)2;
    
    public ApplicationParameter$TRIPLET_VALUE$ORDER() {
    }
}
