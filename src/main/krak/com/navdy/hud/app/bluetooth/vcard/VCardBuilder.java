package com.navdy.hud.app.bluetooth.vcard;

public class VCardBuilder {
    final public static int DEFAULT_EMAIL_TYPE = 3;
    final public static int DEFAULT_PHONE_TYPE = 1;
    final public static int DEFAULT_POSTAL_TYPE = 1;
    final private static String LOG_TAG = "vCard";
    final private static String SHIFT_JIS = "SHIFT_JIS";
    final private static String VCARD_DATA_PUBLIC = "PUBLIC";
    final private static String VCARD_DATA_SEPARATOR = ":";
    final private static String VCARD_DATA_VCARD = "VCARD";
    final public static String VCARD_END_OF_LINE = "\r\n";
    final private static String VCARD_ITEM_SEPARATOR = ";";
    final private static String VCARD_PARAM_ENCODING_BASE64_AS_B = "ENCODING=B";
    final private static String VCARD_PARAM_ENCODING_BASE64_V21 = "ENCODING=BASE64";
    final private static String VCARD_PARAM_ENCODING_QP = "ENCODING=QUOTED-PRINTABLE";
    final private static String VCARD_PARAM_EQUAL = "=";
    final private static String VCARD_PARAM_SEPARATOR = ";";
    final private static String VCARD_WS = " ";
    final private static java.util.Set sAllowedAndroidPropertySet;
    final private static java.util.Map sPostalTypePriorityMap;
    final private boolean mAppendTypeParamName;
    private StringBuilder mBuilder;
    final private String mCharset;
    private boolean mEndAppended;
    final private boolean mIsDoCoMo;
    final private boolean mIsJapaneseMobilePhone;
    final private boolean mIsV30OrV40;
    final private boolean mNeedsToConvertPhoneticString;
    final private boolean mOnlyOneNoteFieldIsAvailable;
    final private boolean mRefrainsQPToNameProperties;
    final private boolean mShouldAppendCharsetParam;
    final private boolean mShouldUseQuotedPrintable;
    final private boolean mUsesAndroidProperty;
    final private boolean mUsesDefactProperty;
    final private String mVCardCharsetParameter;
    final private int mVCardType;
    
    static {
        String[] a = new String[3];
        a[0] = "vnd.android.cursor.item/nickname";
        a[1] = "vnd.android.cursor.item/contact_event";
        a[2] = "vnd.android.cursor.item/relation";
        sAllowedAndroidPropertySet = java.util.Collections.unmodifiableSet((java.util.Set)new java.util.HashSet((java.util.Collection)java.util.Arrays.asList((Object[])a)));
        sPostalTypePriorityMap = (java.util.Map)new java.util.HashMap();
        sPostalTypePriorityMap.put(Integer.valueOf(1), Integer.valueOf(0));
        sPostalTypePriorityMap.put(Integer.valueOf(2), Integer.valueOf(1));
        sPostalTypePriorityMap.put(Integer.valueOf(3), Integer.valueOf(2));
        sPostalTypePriorityMap.put(Integer.valueOf(0), Integer.valueOf(3));
    }
    
    public VCardBuilder(int i) {
        this(i, (String)null);
    }
    
    public VCardBuilder(int i, String s) {
        super();
        boolean b = false;
        boolean b0 = false;
        this.mVCardType = i;
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(i)) {
            android.util.Log.w("vCard", "Should not use vCard 4.0 when building vCard. It is not officially published yet.");
        }
        boolean b1 = com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(i);
        label4: {
            label2: {
                label3: {
                    if (b1) {
                        break label3;
                    }
                    if (!com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(i)) {
                        break label2;
                    }
                }
                b = true;
                break label4;
            }
            b = false;
        }
        this.mIsV30OrV40 = b;
        this.mShouldUseQuotedPrintable = com.navdy.hud.app.bluetooth.vcard.VCardConfig.shouldUseQuotedPrintable(i);
        this.mIsDoCoMo = com.navdy.hud.app.bluetooth.vcard.VCardConfig.isDoCoMo(i);
        this.mIsJapaneseMobilePhone = com.navdy.hud.app.bluetooth.vcard.VCardConfig.needsToConvertPhoneticString(i);
        this.mOnlyOneNoteFieldIsAvailable = com.navdy.hud.app.bluetooth.vcard.VCardConfig.onlyOneNoteFieldIsAvailable(i);
        this.mUsesAndroidProperty = com.navdy.hud.app.bluetooth.vcard.VCardConfig.usesAndroidSpecificProperty(i);
        this.mUsesDefactProperty = com.navdy.hud.app.bluetooth.vcard.VCardConfig.usesDefactProperty(i);
        this.mRefrainsQPToNameProperties = com.navdy.hud.app.bluetooth.vcard.VCardConfig.shouldRefrainQPToNameProperties(i);
        this.mAppendTypeParamName = com.navdy.hud.app.bluetooth.vcard.VCardConfig.appendTypeParamName(i);
        this.mNeedsToConvertPhoneticString = com.navdy.hud.app.bluetooth.vcard.VCardConfig.needsToConvertPhoneticString(i);
        boolean b2 = com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(i);
        label0: {
            label1: {
                if (!b2) {
                    break label1;
                }
                if ("UTF-8".equalsIgnoreCase(s)) {
                    b0 = false;
                    break label0;
                }
            }
            b0 = true;
        }
        this.mShouldAppendCharsetParam = b0;
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isDoCoMo(i)) {
            if ("SHIFT_JIS".equalsIgnoreCase(s)) {
                this.mCharset = s;
            } else if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                this.mCharset = "SHIFT_JIS";
            } else {
                this.mCharset = s;
            }
            this.mVCardCharsetParameter = "CHARSET=SHIFT_JIS";
        } else if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            android.util.Log.i("vCard", "Use the charset \"UTF-8\" for export.");
            this.mCharset = "UTF-8";
            this.mVCardCharsetParameter = "CHARSET=UTF-8";
        } else {
            this.mCharset = s;
            this.mVCardCharsetParameter = new StringBuilder().append("CHARSET=").append(s).toString();
        }
        this.clear();
    }
    
    private com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendNamePropertiesV40(java.util.List a) {
        boolean b = this.mIsDoCoMo;
        label7: {
            label8: {
                if (b) {
                    break label8;
                }
                if (!this.mNeedsToConvertPhoneticString) {
                    break label7;
                }
            }
            android.util.Log.w("vCard", "Invalid flag is used in vCard 4.0 construction. Ignored.");
        }
        label4: {
            label5: {
                label6: {
                    if (a == null) {
                        break label6;
                    }
                    if (!a.isEmpty()) {
                        break label5;
                    }
                }
                this.appendLine("FN", "");
                break label4;
            }
            android.content.ContentValues a0 = this.getPrimaryContentValueWithStructuredName(a);
            String s = a0.getAsString("data3");
            String s0 = a0.getAsString("data5");
            String s1 = a0.getAsString("data2");
            String s2 = a0.getAsString("data4");
            String s3 = a0.getAsString("data6");
            String s4 = a0.getAsString("data1");
            boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s);
            label3: {
                if (!b0) {
                    break label3;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                    break label3;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    break label3;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s2)) {
                    break label3;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s3)) {
                    break label3;
                }
                boolean b1 = android.text.TextUtils.isEmpty((CharSequence)s4);
                label2: {
                    if (b1) {
                        break label2;
                    }
                    s = s4;
                    break label3;
                }
                this.appendLine("FN", "");
                break label4;
            }
            String s5 = a0.getAsString("data9");
            String s6 = a0.getAsString("data8");
            String s7 = a0.getAsString("data7");
            String s8 = this.escapeCharacters(s);
            String s9 = this.escapeCharacters(s1);
            String s10 = this.escapeCharacters(s0);
            String s11 = this.escapeCharacters(s2);
            String s12 = this.escapeCharacters(s3);
            this.mBuilder.append("N");
            boolean b2 = android.text.TextUtils.isEmpty((CharSequence)s5);
            label0: {
                label1: {
                    if (!b2) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s6)) {
                        break label1;
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)s7)) {
                        break label0;
                    }
                }
                this.mBuilder.append(";");
                String s13 = new StringBuilder().append(this.escapeCharacters(s5)).append((char)59).append(this.escapeCharacters(s7)).append((char)59).append(this.escapeCharacters(s6)).toString();
                this.mBuilder.append("SORT-AS=").append(com.navdy.hud.app.bluetooth.vcard.VCardUtils.toStringAsV40ParamValue(s13));
            }
            this.mBuilder.append(":");
            this.mBuilder.append(s8);
            this.mBuilder.append(";");
            this.mBuilder.append(s9);
            this.mBuilder.append(";");
            this.mBuilder.append(s10);
            this.mBuilder.append(";");
            this.mBuilder.append(s11);
            this.mBuilder.append(";");
            this.mBuilder.append(s12);
            this.mBuilder.append("\r\n");
            if (android.text.TextUtils.isEmpty((CharSequence)s4)) {
                android.util.Log.w("vCard", "DISPLAY_NAME is empty.");
                this.appendLine("FN", this.escapeCharacters(com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructNameFromElements(com.navdy.hud.app.bluetooth.vcard.VCardConfig.getNameOrderType(this.mVCardType), s, s0, s1, s2, s3)));
            } else {
                String s14 = this.escapeCharacters(s4);
                this.mBuilder.append("FN");
                this.mBuilder.append(":");
                this.mBuilder.append(s14);
                this.mBuilder.append("\r\n");
            }
            this.appendPhoneticNameFields(a0);
        }
        return this;
    }
    
    private void appendPhoneticNameFields(android.content.ContentValues a) {
        String s = a.getAsString("data9");
        String s0 = a.getAsString("data8");
        String s1 = a.getAsString("data7");
        if (this.mNeedsToConvertPhoneticString) {
            s = com.navdy.hud.app.bluetooth.vcard.VCardUtils.toHalfWidthString(s);
            s0 = com.navdy.hud.app.bluetooth.vcard.VCardUtils.toHalfWidthString(s0);
            s1 = com.navdy.hud.app.bluetooth.vcard.VCardUtils.toHalfWidthString(s1);
        }
        boolean b = android.text.TextUtils.isEmpty((CharSequence)s);
        label12: {
            label13: {
                if (!b) {
                    break label13;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    break label13;
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                    break label13;
                }
                if (!this.mIsDoCoMo) {
                    break label12;
                }
                this.mBuilder.append("SOUND");
                this.mBuilder.append(";");
                this.mBuilder.append("X-IRMC-N");
                this.mBuilder.append(":");
                this.mBuilder.append(";");
                this.mBuilder.append(";");
                this.mBuilder.append(";");
                this.mBuilder.append(";");
                this.mBuilder.append("\r\n");
                break label12;
            }
            if (!com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType)) {
                if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType)) {
                    String s2 = com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructNameFromElements(this.mVCardType, s, s0, s1);
                    this.mBuilder.append("SORT-STRING");
                    if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType)) {
                        String[] a0 = new String[1];
                        a0[0] = s2;
                        if (this.shouldAppendCharsetParam(a0)) {
                            this.mBuilder.append(";");
                            this.mBuilder.append(this.mVCardCharsetParameter);
                        }
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(this.escapeCharacters(s2));
                    this.mBuilder.append("\r\n");
                } else if (this.mIsJapaneseMobilePhone) {
                    boolean b0 = false;
                    String s3 = null;
                    String s4 = null;
                    String s5 = null;
                    boolean b1 = false;
                    this.mBuilder.append("SOUND");
                    this.mBuilder.append(";");
                    this.mBuilder.append("X-IRMC-N");
                    boolean b2 = this.mRefrainsQPToNameProperties;
                    label11: {
                        label9: {
                            label10: {
                                if (b2) {
                                    break label10;
                                }
                                String[] a1 = new String[1];
                                a1[0] = s;
                                if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a1)) {
                                    break label9;
                                }
                                String[] a2 = new String[1];
                                a2[0] = s0;
                                if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a2)) {
                                    break label9;
                                }
                                String[] a3 = new String[1];
                                a3[0] = s1;
                                if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a3)) {
                                    break label9;
                                }
                            }
                            b0 = false;
                            break label11;
                        }
                        b0 = true;
                    }
                    if (b0) {
                        s3 = this.encodeQuotedPrintable(s);
                        s4 = this.encodeQuotedPrintable(s0);
                        s5 = this.encodeQuotedPrintable(s1);
                    } else {
                        s3 = this.escapeCharacters(s);
                        s4 = this.escapeCharacters(s0);
                        s5 = this.escapeCharacters(s1);
                    }
                    String[] a4 = new String[3];
                    a4[0] = s3;
                    a4[1] = s4;
                    a4[2] = s5;
                    if (this.shouldAppendCharsetParam(a4)) {
                        this.mBuilder.append(";");
                        this.mBuilder.append(this.mVCardCharsetParameter);
                    }
                    this.mBuilder.append(":");
                    if (android.text.TextUtils.isEmpty((CharSequence)s3)) {
                        b1 = true;
                    } else {
                        this.mBuilder.append(s3);
                        b1 = false;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s4)) {
                        if (b1) {
                            b1 = false;
                        } else {
                            this.mBuilder.append((char)32);
                        }
                        this.mBuilder.append(s4);
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s5)) {
                        if (!b1) {
                            this.mBuilder.append((char)32);
                        }
                        this.mBuilder.append(s5);
                    }
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                    this.mBuilder.append("\r\n");
                }
            }
            if (this.mUsesDefactProperty) {
                if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                    boolean b3 = false;
                    boolean b4 = this.mShouldUseQuotedPrintable;
                    label8: {
                        label6: {
                            label7: {
                                if (!b4) {
                                    break label7;
                                }
                                String[] a5 = new String[1];
                                a5[0] = s1;
                                if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a5)) {
                                    break label6;
                                }
                            }
                            b3 = false;
                            break label8;
                        }
                        b3 = true;
                    }
                    String s6 = b3 ? this.encodeQuotedPrintable(s1) : this.escapeCharacters(s1);
                    this.mBuilder.append("X-PHONETIC-FIRST-NAME");
                    String[] a6 = new String[1];
                    a6[0] = s1;
                    if (this.shouldAppendCharsetParam(a6)) {
                        this.mBuilder.append(";");
                        this.mBuilder.append(this.mVCardCharsetParameter);
                    }
                    if (b3) {
                        this.mBuilder.append(";");
                        this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(s6);
                    this.mBuilder.append("\r\n");
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    boolean b5 = false;
                    boolean b6 = this.mShouldUseQuotedPrintable;
                    label5: {
                        label3: {
                            label4: {
                                if (!b6) {
                                    break label4;
                                }
                                String[] a7 = new String[1];
                                a7[0] = s0;
                                if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a7)) {
                                    break label3;
                                }
                            }
                            b5 = false;
                            break label5;
                        }
                        b5 = true;
                    }
                    String s7 = b5 ? this.encodeQuotedPrintable(s0) : this.escapeCharacters(s0);
                    this.mBuilder.append("X-PHONETIC-MIDDLE-NAME");
                    String[] a8 = new String[1];
                    a8[0] = s0;
                    if (this.shouldAppendCharsetParam(a8)) {
                        this.mBuilder.append(";");
                        this.mBuilder.append(this.mVCardCharsetParameter);
                    }
                    if (b5) {
                        this.mBuilder.append(";");
                        this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(s7);
                    this.mBuilder.append("\r\n");
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                    boolean b7 = false;
                    boolean b8 = this.mShouldUseQuotedPrintable;
                    label2: {
                        label0: {
                            label1: {
                                if (!b8) {
                                    break label1;
                                }
                                String[] a9 = new String[1];
                                a9[0] = s;
                                if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a9)) {
                                    break label0;
                                }
                            }
                            b7 = false;
                            break label2;
                        }
                        b7 = true;
                    }
                    String s8 = b7 ? this.encodeQuotedPrintable(s) : this.escapeCharacters(s);
                    this.mBuilder.append("X-PHONETIC-LAST-NAME");
                    String[] a10 = new String[1];
                    a10[0] = s;
                    if (this.shouldAppendCharsetParam(a10)) {
                        this.mBuilder.append(";");
                        this.mBuilder.append(this.mVCardCharsetParameter);
                    }
                    if (b7) {
                        this.mBuilder.append(";");
                        this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(s8);
                    this.mBuilder.append("\r\n");
                }
            }
        }
    }
    
    private void appendPostalsForDoCoMo(java.util.List a) {
        java.util.Iterator a0 = a.iterator();
        android.content.ContentValues a1 = null;
        int i = 2147483647;
        int i0 = 2147483647;
        Object a2 = a0;
        while(true) {
            boolean b = ((java.util.Iterator)a2).hasNext();
            android.content.ContentValues a3 = a1;
            if (b) {
                a1 = (android.content.ContentValues)((java.util.Iterator)a2).next();
                if (a1 == null) {
                    a1 = a3;
                    continue;
                }
                Integer a4 = a1.getAsInteger("data2");
                Integer a5 = (Integer)sPostalTypePriorityMap.get(a4);
                int i1 = (a5 == null) ? 2147483647 : a5.intValue();
                if (i1 >= i) {
                    a1 = a3;
                    continue;
                }
                i0 = a4.intValue();
                if (i1 != 0) {
                    i = i1;
                    continue;
                }
            }
            if (a1 != null) {
                this.appendPostalLine(i0, a1.getAsString("data3"), a1, false, true);
            } else {
                android.util.Log.w("vCard", "Should not come here. Must have at least one postal data.");
            }
            return;
        }
    }
    
    private void appendPostalsForGeneric(java.util.List a) {
        Object a0 = a.iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            android.content.ContentValues a1 = (android.content.ContentValues)((java.util.Iterator)a0).next();
            if (a1 != null) {
                Integer a2 = a1.getAsInteger("data2");
                int i = (a2 == null) ? 1 : a2.intValue();
                String s = a1.getAsString("data3");
                Integer a3 = a1.getAsInteger("is_primary");
                this.appendPostalLine(i, s, a1, a3 != null && a3.intValue() > 0, false);
            }
        }
    }
    
    private void appendTypeParameter(String s) {
        this.appendTypeParameter(this.mBuilder, s);
    }
    
    private void appendTypeParameter(StringBuilder a, String s) {
        boolean b = com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType);
        label0: {
            label2: {
                if (b) {
                    break label2;
                }
                boolean b0 = com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType);
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (!this.mAppendTypeParamName) {
                        break label0;
                    }
                }
                if (this.mIsDoCoMo) {
                    break label0;
                }
            }
            a.append("TYPE").append("=");
        }
        a.append(s);
    }
    
    private void appendTypeParameters(java.util.List a) {
        java.util.Iterator a0 = a.iterator();
        boolean b = true;
        Object a1 = a0;
        while(((java.util.Iterator)a1).hasNext()) {
            String s = (String)((java.util.Iterator)a1).next();
            boolean b0 = com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType);
            {
                if (!b0 && !com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType)) {
                    if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.isV21Word(s)) {
                        continue;
                    }
                    if (b) {
                        b = false;
                    } else {
                        this.mBuilder.append(";");
                    }
                    this.appendTypeParameter(s);
                    continue;
                }
                String s0 = (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType)) ? com.navdy.hud.app.bluetooth.vcard.VCardUtils.toStringAsV40ParamValue(s) : com.navdy.hud.app.bluetooth.vcard.VCardUtils.toStringAsV30ParamValue(s);
                if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    if (b) {
                        b = false;
                    } else {
                        this.mBuilder.append(";");
                    }
                    this.appendTypeParameter(s0);
                }
            }
        }
    }
    
    private void appendUncommonPhoneType(StringBuilder a, Integer a0) {
        if (this.mIsDoCoMo) {
            a.append("VOICE");
        } else {
            String s = com.navdy.hud.app.bluetooth.vcard.VCardUtils.getPhoneTypeString(a0);
            if (s == null) {
                android.util.Log.e("vCard", new StringBuilder().append("Unknown or unsupported (by vCard) Phone type: ").append(a0).toString());
            } else {
                this.appendTypeParameter(s);
            }
        }
    }
    
    private void buildSinglePartNameField(String s, String s0) {
        boolean b = false;
        boolean b0 = this.mRefrainsQPToNameProperties;
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    String[] a = new String[1];
                    a[0] = s0;
                    if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        String s1 = b ? this.encodeQuotedPrintable(s0) : this.escapeCharacters(s0);
        this.mBuilder.append(s);
        String[] a0 = new String[1];
        a0[0] = s0;
        if (this.shouldAppendCharsetParam(a0)) {
            this.mBuilder.append(";");
            this.mBuilder.append(this.mVCardCharsetParameter);
        }
        if (b) {
            this.mBuilder.append(";");
            this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
        }
        this.mBuilder.append(":");
        this.mBuilder.append(s1);
    }
    
    private boolean containsNonEmptyName(android.content.ContentValues a) {
        boolean b = false;
        String s = a.getAsString("data3");
        String s0 = a.getAsString("data5");
        String s1 = a.getAsString("data2");
        String s2 = a.getAsString("data4");
        String s3 = a.getAsString("data6");
        String s4 = a.getAsString("data9");
        String s5 = a.getAsString("data8");
        String s6 = a.getAsString("data7");
        String s7 = a.getAsString("data1");
        boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s);
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s2)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s3)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s4)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s5)) {
                        break label1;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s6)) {
                        break label1;
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)s7)) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    private String encodeQuotedPrintable(String s) {
        String s0 = null;
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            s0 = "";
        } else {
            byte[] a = null;
            int i = 0;
            int i0 = 0;
            StringBuilder a0 = new StringBuilder();
            try {
                a = s.getBytes(this.mCharset);
                i = 0;
                i0 = 0;
            } catch(java.io.UnsupportedEncodingException ignoredException) {
                android.util.Log.e("vCard", new StringBuilder().append("Charset ").append(this.mCharset).append(" cannot be used. ").append("Try default charset").toString());
                a = s.getBytes();
                i = 0;
                i0 = 0;
            }
            while(i < a.length) {
                Object[] a1 = new Object[1];
                int i1 = a[i];
                a1[0] = Byte.valueOf((byte)i1);
                a0.append(String.format("=%02X", a1));
                i = i + 1;
                i0 = i0 + 3;
                if (i0 >= 67) {
                    a0.append("=\r\n");
                    i0 = 0;
                }
            }
            s0 = a0.toString();
        }
        return s0;
    }
    
    private String escapeCharacters(String s) {
        String s0 = null;
        if (android.text.TextUtils.isEmpty((CharSequence)s)) {
            s0 = "";
        } else {
            StringBuilder a = new StringBuilder();
            int i = s.length();
            int i0 = 0;
            while(i0 < i) {
                int i1 = s.charAt(i0);
                switch(i1) {
                    case 92: {
                        boolean b = this.mIsV30OrV40;
                        label1: {
                            if (!b) {
                                break label1;
                            }
                            a.append("\\\\");
                            break;
                        }
                    }
                    case 60: case 62: {
                        if (this.mIsDoCoMo) {
                            a.append((char)92);
                            a.append((char)i1);
                            break;
                        } else {
                            a.append((char)i1);
                            break;
                        }
                    }
                    case 59: {
                        a.append((char)92);
                        a.append((char)59);
                        break;
                    }
                    case 44: {
                        if (this.mIsV30OrV40) {
                            a.append("\\,");
                            break;
                        } else {
                            a.append((char)i1);
                            break;
                        }
                    }
                    case 13: {
                        int i2 = i0 + 1;
                        label0: {
                            if (i2 >= i) {
                                break label0;
                            }
                            int i3 = s.charAt(i0);
                            if (i3 == 10) {
                                break;
                            }
                        }
                    }
                    case 10: {
                        a.append("\\n");
                        break;
                    }
                    default: {
                        a.append((char)i1);
                    }
                }
                i0 = i0 + 1;
            }
            s0 = a.toString();
        }
        return s0;
    }
    
    private android.content.ContentValues getPrimaryContentValueWithStructuredName(java.util.List a) {
        java.util.Iterator a0 = a.iterator();
        android.content.ContentValues a1 = null;
        android.content.ContentValues a2 = null;
        Object a3 = a0;
        while(true) {
            boolean b = ((java.util.Iterator)a3).hasNext();
            android.content.ContentValues a4 = a2;
            label0: if (b) {
                a2 = (android.content.ContentValues)((java.util.Iterator)a3).next();
                if (a2 == null) {
                    a2 = a4;
                    continue;
                }
                Integer a5 = a2.getAsInteger("is_super_primary");
                label1: {
                    if (a5 == null) {
                        break label1;
                    }
                    if (a5.intValue() > 0) {
                        break label0;
                    }
                }
                if (a4 != null) {
                    a2 = a4;
                    continue;
                } else {
                    Integer a6 = a2.getAsInteger("is_primary");
                    if (a6 != null && a6.intValue() > 0 && this.containsNonEmptyName(a2)) {
                        continue;
                    }
                    if (a1 != null) {
                        a2 = a4;
                        continue;
                    } else if (this.containsNonEmptyName(a2)) {
                        a1 = a2;
                        a2 = a4;
                        continue;
                    } else {
                        a2 = a4;
                        continue;
                    }
                }
            }
            if (a2 != null) {
                a1 = a2;
            } else if (a1 == null) {
                a1 = new android.content.ContentValues();
            }
            return a1;
        }
    }
    
    private boolean shouldAppendCharsetParam(String[] a) {
        boolean b = false;
        if (this.mShouldAppendCharsetParam) {
            int i = a.length;
            int i0 = 0;
            while(true) {
                if (i0 >= i) {
                    b = false;
                    break;
                } else {
                    String[] a0 = new String[1];
                    a0[0] = a[i0];
                    if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(a0)) {
                        i0 = i0 + 1;
                        continue;
                    }
                    b = true;
                    break;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    private java.util.List splitPhoneNumbers(String s) {
        java.util.ArrayList a = new java.util.ArrayList();
        StringBuilder a0 = new StringBuilder();
        int i = s.length();
        int i0 = 0;
        while(i0 < i) {
            int i1 = s.charAt(i0);
            label1: {
                label0: {
                    if (i1 != 10) {
                        break label0;
                    }
                    if (a0.length() <= 0) {
                        break label0;
                    }
                    ((java.util.List)a).add(a0.toString());
                    a0 = new StringBuilder();
                    break label1;
                }
                a0.append((char)i1);
            }
            i0 = i0 + 1;
        }
        if (a0.length() > 0) {
            ((java.util.List)a).add(a0.toString());
        }
        return (java.util.List)a;
    }
    
    private com.navdy.hud.app.bluetooth.vcard.VCardBuilder$PostalStruct tryConstructPostalStruct(android.content.ContentValues a) {
        com.navdy.hud.app.bluetooth.vcard.VCardBuilder$PostalStruct a0 = null;
        String s = a.getAsString("data5");
        String s0 = a.getAsString("data6");
        String s1 = a.getAsString("data4");
        String s2 = a.getAsString("data7");
        String s3 = a.getAsString("data8");
        String s4 = a.getAsString("data9");
        String s5 = a.getAsString("data10");
        String[] a1 = new String[7];
        a1[0] = s;
        a1[1] = s0;
        a1[2] = s1;
        a1[3] = s2;
        a1[4] = s3;
        a1[5] = s4;
        a1[6] = s5;
        if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.areAllEmpty(a1)) {
            String s6 = a.getAsString("data1");
            if (android.text.TextUtils.isEmpty((CharSequence)s6)) {
                a0 = null;
            } else {
                boolean b = false;
                boolean b0 = this.mShouldUseQuotedPrintable;
                label5: {
                    label3: {
                        label4: {
                            if (!b0) {
                                break label4;
                            }
                            String[] a2 = new String[1];
                            a2[0] = s6;
                            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a2)) {
                                break label3;
                            }
                        }
                        b = false;
                        break label5;
                    }
                    b = true;
                }
                String[] a3 = new String[1];
                a3[0] = s6;
                boolean b1 = !com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(a3);
                String s7 = b ? this.encodeQuotedPrintable(s6) : this.escapeCharacters(s6);
                StringBuilder a4 = new StringBuilder();
                a4.append(";");
                a4.append(s7);
                a4.append(";");
                a4.append(";");
                a4.append(";");
                a4.append(";");
                a4.append(";");
                a0 = new com.navdy.hud.app.bluetooth.vcard.VCardBuilder$PostalStruct(b, b1, a4.toString());
            }
        } else {
            boolean b2 = false;
            String s8 = null;
            String s9 = null;
            String s10 = null;
            String s11 = null;
            String s12 = null;
            String s13 = null;
            boolean b3 = this.mShouldUseQuotedPrintable;
            label2: {
                label0: {
                    label1: {
                        if (!b3) {
                            break label1;
                        }
                        if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a1)) {
                            break label0;
                        }
                    }
                    b2 = false;
                    break label2;
                }
                b2 = true;
            }
            boolean b4 = !com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(a1);
            if (android.text.TextUtils.isEmpty((CharSequence)s2)) {
                s2 = (android.text.TextUtils.isEmpty((CharSequence)s0)) ? "" : s0;
            } else if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                s2 = new StringBuilder().append(s2).append(" ").append(s0).toString();
            }
            if (b2) {
                s8 = this.encodeQuotedPrintable(s);
                s9 = this.encodeQuotedPrintable(s1);
                s10 = this.encodeQuotedPrintable(s2);
                s11 = this.encodeQuotedPrintable(s3);
                s12 = this.encodeQuotedPrintable(s4);
                s13 = this.encodeQuotedPrintable(s5);
            } else {
                s8 = this.escapeCharacters(s);
                s9 = this.escapeCharacters(s1);
                s10 = this.escapeCharacters(s2);
                s11 = this.escapeCharacters(s3);
                s12 = this.escapeCharacters(s4);
                s13 = this.escapeCharacters(s5);
                this.escapeCharacters(s0);
            }
            StringBuilder a5 = new StringBuilder();
            a5.append(s8);
            a5.append(";");
            a5.append(";");
            a5.append(s9);
            a5.append(";");
            a5.append(s10);
            a5.append(";");
            a5.append(s11);
            a5.append(";");
            a5.append(s12);
            a5.append(";");
            a5.append(s13);
            a0 = new com.navdy.hud.app.bluetooth.vcard.VCardBuilder$PostalStruct(b2, b4, a5.toString());
        }
        return a0;
    }
    
    public void appendAndroidSpecificProperty(String s, android.content.ContentValues a) {
        if (sAllowedAndroidPropertySet.contains(s)) {
            boolean b = false;
            boolean b0 = false;
            java.util.ArrayList a0 = new java.util.ArrayList();
            int i = 1;
            while(i <= 15) {
                String s0 = a.getAsString(new StringBuilder().append("data").append(i).toString());
                if (s0 == null) {
                    s0 = "";
                }
                ((java.util.List)a0).add(s0);
                i = i + 1;
            }
            boolean b1 = this.mShouldAppendCharsetParam;
            label5: {
                label3: {
                    label4: {
                        if (!b1) {
                            break label4;
                        }
                        if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii((java.util.Collection)a0)) {
                            break label3;
                        }
                    }
                    b = false;
                    break label5;
                }
                b = true;
            }
            boolean b2 = this.mShouldUseQuotedPrintable;
            label2: {
                label0: {
                    label1: {
                        if (!b2) {
                            break label1;
                        }
                        if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii((java.util.Collection)a0)) {
                            break label0;
                        }
                    }
                    b0 = false;
                    break label2;
                }
                b0 = true;
            }
            this.mBuilder.append("X-ANDROID-CUSTOM");
            if (b) {
                this.mBuilder.append(";");
                this.mBuilder.append(this.mVCardCharsetParameter);
            }
            if (b0) {
                this.mBuilder.append(";");
                this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
            }
            this.mBuilder.append(":");
            this.mBuilder.append(s);
            Object a1 = ((java.util.List)a0).iterator();
            while(((java.util.Iterator)a1).hasNext()) {
                String s1 = (String)((java.util.Iterator)a1).next();
                String s2 = b0 ? this.encodeQuotedPrintable(s1) : this.escapeCharacters(s1);
                this.mBuilder.append(";");
                this.mBuilder.append(s2);
            }
            this.mBuilder.append("\r\n");
        }
    }
    
    public void appendEmailLine(int i, String s, String s0, boolean b) {
        String s1 = null;
        switch(i) {
            case 4: {
                s1 = "CELL";
                break;
            }
            case 3: {
                s1 = null;
                break;
            }
            case 2: {
                s1 = "WORK";
                break;
            }
            case 1: {
                s1 = "HOME";
                break;
            }
            case 0: {
                if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.isMobilePhoneLabel(s)) {
                    s1 = "CELL";
                    break;
                } else {
                    boolean b0 = android.text.TextUtils.isEmpty((CharSequence)s);
                    label0: {
                        label1: {
                            if (b0) {
                                break label1;
                            }
                            String[] a = new String[1];
                            a[0] = s;
                            if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyAlphaDigitHyphen(a)) {
                                break label0;
                            }
                        }
                        s1 = null;
                        break;
                    }
                    s1 = new StringBuilder().append("X-").append(s).toString();
                    break;
                }
            }
            default: {
                android.util.Log.e("vCard", new StringBuilder().append("Unknown Email type: ").append(i).toString());
                s1 = null;
            }
        }
        java.util.ArrayList a0 = new java.util.ArrayList();
        if (b) {
            ((java.util.List)a0).add("PREF");
        }
        if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
            ((java.util.List)a0).add(s1);
        }
        this.appendLineWithCharsetAndQPDetection("EMAIL", (java.util.List)a0, s0);
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendEmails(java.util.List a) {
        boolean b = false;
        if (a == null) {
            b = false;
        } else {
            java.util.HashSet a0 = new java.util.HashSet();
            java.util.Iterator a1 = a.iterator();
            b = false;
            Object a2 = a1;
            while(((java.util.Iterator)a2).hasNext()) {
                android.content.ContentValues a3 = (android.content.ContentValues)((java.util.Iterator)a2).next();
                String s = a3.getAsString("data1");
                if (s != null) {
                    s = s.trim();
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                    Integer a4 = a3.getAsInteger("data2");
                    int i = (a4 == null) ? 3 : a4.intValue();
                    String s0 = a3.getAsString("data3");
                    Integer a5 = a3.getAsInteger("is_primary");
                    boolean b0 = a5 != null && a5.intValue() > 0;
                    if (((java.util.Set)a0).contains(s)) {
                        b = true;
                    } else {
                        ((java.util.Set)a0).add(s);
                        this.appendEmailLine(i, s0, s, b0);
                        b = true;
                    }
                }
            }
        }
        if (!b && this.mIsDoCoMo) {
            this.appendEmailLine(1, "", "", false);
        }
        return this;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendEvents(java.util.List a) {
        if (a != null) {
            String s = null;
            java.util.Iterator a0 = a.iterator();
            String s0 = null;
            String s1 = null;
            Object a1 = a0;
            while(true) {
                if (((java.util.Iterator)a1).hasNext()) {
                    android.content.ContentValues a2 = (android.content.ContentValues)((java.util.Iterator)a1).next();
                    if (a2 == null) {
                        continue;
                    }
                    Integer a3 = a2.getAsInteger("data2");
                    if (a3 == null || a3.intValue() != 3) {
                        if (!this.mUsesAndroidProperty) {
                            continue;
                        }
                        this.appendAndroidSpecificProperty("vnd.android.cursor.item/contact_event", a2);
                        continue;
                    }
                    s = a2.getAsString("data1");
                    if (s == null) {
                        continue;
                    }
                    Integer a4 = a2.getAsInteger("is_super_primary");
                    if (a4 != null && a4.intValue() > 0) {
                        break;
                    }
                    Integer a5 = a2.getAsInteger("is_primary");
                    if (a5 != null && a5.intValue() > 0) {
                        s0 = s;
                    } else if (s1 == null) {
                        s1 = s;
                    }
                } else {
                    s = s0;
                    break;
                }
            }
            if (s == null) {
                if (s1 != null) {
                    this.appendLineWithCharsetAndQPDetection("BDAY", s1.trim());
                }
            } else {
                this.appendLineWithCharsetAndQPDetection("BDAY", s.trim());
            }
        }
        return this;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendIms(java.util.List a) {
        if (a != null) {
            Object a0 = a.iterator();
            while(((java.util.Iterator)a0).hasNext()) {
                android.content.ContentValues a1 = (android.content.ContentValues)((java.util.Iterator)a0).next();
                Integer a2 = a1.getAsInteger("data5");
                if (a2 != null) {
                    String s = com.navdy.hud.app.bluetooth.vcard.VCardUtils.getPropertyNameForIm(a2.intValue());
                    if (s != null) {
                        String s0 = a1.getAsString("data1");
                        if (s0 != null) {
                            s0 = s0.trim();
                        }
                        if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                            String s1 = null;
                            Integer a3 = a1.getAsInteger("data2");
                            switch((a3 == null) ? 3 : a3.intValue()) {
                                case 2: {
                                    s1 = "WORK";
                                    break;
                                }
                                case 1: {
                                    s1 = "HOME";
                                    break;
                                }
                                case 0: {
                                    String s2 = a1.getAsString("data3");
                                    s1 = (s2 == null) ? null : new StringBuilder().append("X-").append(s2).toString();
                                    break;
                                }
                                default: {
                                    s1 = null;
                                }
                            }
                            java.util.ArrayList a4 = new java.util.ArrayList();
                            if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                                ((java.util.List)a4).add(s1);
                            }
                            Integer a5 = a1.getAsInteger("is_primary");
                            if (a5 != null && a5.intValue() > 0) {
                                ((java.util.List)a4).add("PREF");
                            }
                            this.appendLineWithCharsetAndQPDetection(s, (java.util.List)a4, s0);
                        }
                    }
                }
            }
        }
        return this;
    }
    
    public void appendLine(String s, String s0) {
        this.appendLine(s, s0, false, false);
    }
    
    public void appendLine(String s, String s0, boolean b, boolean b0) {
        this.appendLine(s, (java.util.List)null, s0, b, b0);
    }
    
    public void appendLine(String s, java.util.List a) {
        this.appendLine(s, a, false, false);
    }
    
    public void appendLine(String s, java.util.List a, String s0) {
        this.appendLine(s, a, s0, false, false);
    }
    
    public void appendLine(String s, java.util.List a, String s0, boolean b, boolean b0) {
        String s1 = null;
        this.mBuilder.append(s);
        if (a != null && a.size() > 0) {
            this.mBuilder.append(";");
            this.appendTypeParameters(a);
        }
        if (b) {
            this.mBuilder.append(";");
            this.mBuilder.append(this.mVCardCharsetParameter);
        }
        if (b0) {
            this.mBuilder.append(";");
            this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
            s1 = this.encodeQuotedPrintable(s0);
        } else {
            s1 = this.escapeCharacters(s0);
        }
        this.mBuilder.append(":");
        this.mBuilder.append(s1);
        this.mBuilder.append("\r\n");
    }
    
    public void appendLine(String s, java.util.List a, java.util.List a0, boolean b, boolean b0) {
        this.mBuilder.append(s);
        if (a != null && a.size() > 0) {
            this.mBuilder.append(";");
            this.appendTypeParameters(a);
        }
        if (b) {
            this.mBuilder.append(";");
            this.mBuilder.append(this.mVCardCharsetParameter);
        }
        if (b0) {
            this.mBuilder.append(";");
            this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
        }
        this.mBuilder.append(":");
        Object a1 = a0.iterator();
        boolean b1 = true;
        while(((java.util.Iterator)a1).hasNext()) {
            String s0 = (String)((java.util.Iterator)a1).next();
            String s1 = b0 ? this.encodeQuotedPrintable(s0) : this.escapeCharacters(s0);
            if (b1) {
                b1 = false;
            } else {
                this.mBuilder.append(";");
            }
            this.mBuilder.append(s1);
        }
        this.mBuilder.append("\r\n");
    }
    
    public void appendLine(String s, java.util.List a, boolean b, boolean b0) {
        this.appendLine(s, (java.util.List)null, a, b, b0);
    }
    
    public void appendLineWithCharsetAndQPDetection(String s, String s0) {
        this.appendLineWithCharsetAndQPDetection(s, (java.util.List)null, s0);
    }
    
    public void appendLineWithCharsetAndQPDetection(String s, java.util.List a) {
        this.appendLineWithCharsetAndQPDetection(s, (java.util.List)null, a);
    }
    
    public void appendLineWithCharsetAndQPDetection(String s, java.util.List a, String s0) {
        boolean b = false;
        String[] a0 = new String[1];
        a0[0] = s0;
        boolean b0 = !com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(a0);
        boolean b1 = this.mShouldUseQuotedPrintable;
        label2: {
            label0: {
                label1: {
                    if (!b1) {
                        break label1;
                    }
                    String[] a1 = new String[1];
                    a1[0] = s0;
                    if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a1)) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        this.appendLine(s, a, s0, b0, b);
    }
    
    public void appendLineWithCharsetAndQPDetection(String s, java.util.List a, java.util.List a0) {
        boolean b = false;
        boolean b0 = false;
        boolean b1 = this.mShouldAppendCharsetParam;
        label5: {
            label3: {
                label4: {
                    if (!b1) {
                        break label4;
                    }
                    if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii((java.util.Collection)a0)) {
                        break label3;
                    }
                }
                b = false;
                break label5;
            }
            b = true;
        }
        boolean b2 = this.mShouldUseQuotedPrintable;
        label2: {
            label0: {
                label1: {
                    if (!b2) {
                        break label1;
                    }
                    if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii((java.util.Collection)a0)) {
                        break label0;
                    }
                }
                b0 = false;
                break label2;
            }
            b0 = true;
        }
        this.appendLine(s, a, a0, b, b0);
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendNameProperties(java.util.List a) {
        com.navdy.hud.app.bluetooth.vcard.VCardBuilder a0 = null;
        label8: if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType)) {
            a0 = this.appendNamePropertiesV40(a);
        } else {
            label9: {
                label10: {
                    if (a == null) {
                        break label10;
                    }
                    if (!a.isEmpty()) {
                        break label9;
                    }
                }
                if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType)) {
                    this.appendLine("N", "");
                    this.appendLine("FN", "");
                    a0 = this;
                    break label8;
                } else if (this.mIsDoCoMo) {
                    this.appendLine("N", "");
                    a0 = this;
                    break label8;
                } else {
                    a0 = this;
                    break label8;
                }
            }
            android.content.ContentValues a1 = this.getPrimaryContentValueWithStructuredName(a);
            String s = a1.getAsString("data3");
            String s0 = a1.getAsString("data5");
            String s1 = a1.getAsString("data2");
            String s2 = a1.getAsString("data4");
            String s3 = a1.getAsString("data6");
            String s4 = a1.getAsString("data1");
            boolean b = android.text.TextUtils.isEmpty((CharSequence)s);
            label6: {
                boolean b0 = false;
                boolean b1 = false;
                String s5 = null;
                String s6 = null;
                String s7 = null;
                String s8 = null;
                String s9 = null;
                label7: {
                    if (!b) {
                        break label7;
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                        break label7;
                    }
                    if (android.text.TextUtils.isEmpty((CharSequence)s4)) {
                        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType)) {
                            this.appendLine("N", "");
                            this.appendLine("FN", "");
                            break label6;
                        } else {
                            if (!this.mIsDoCoMo) {
                                break label6;
                            }
                            this.appendLine("N", "");
                            break label6;
                        }
                    } else {
                        this.buildSinglePartNameField("N", s4);
                        this.mBuilder.append(";");
                        this.mBuilder.append(";");
                        this.mBuilder.append(";");
                        this.mBuilder.append(";");
                        this.mBuilder.append("\r\n");
                        this.buildSinglePartNameField("FN", s4);
                        this.mBuilder.append("\r\n");
                        break label6;
                    }
                }
                String[] a2 = new String[5];
                a2[0] = s;
                a2[1] = s1;
                a2[2] = s0;
                a2[3] = s2;
                a2[4] = s3;
                boolean b2 = this.shouldAppendCharsetParam(a2);
                boolean b3 = this.mRefrainsQPToNameProperties;
                label5: {
                    label3: {
                        label4: {
                            if (b3) {
                                break label4;
                            }
                            String[] a3 = new String[1];
                            a3[0] = s;
                            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a3)) {
                                break label3;
                            }
                            String[] a4 = new String[1];
                            a4[0] = s1;
                            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a4)) {
                                break label3;
                            }
                            String[] a5 = new String[1];
                            a5[0] = s0;
                            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a5)) {
                                break label3;
                            }
                            String[] a6 = new String[1];
                            a6[0] = s2;
                            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a6)) {
                                break label3;
                            }
                            String[] a7 = new String[1];
                            a7[0] = s3;
                            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a7)) {
                                break label3;
                            }
                        }
                        b0 = false;
                        break label5;
                    }
                    b0 = true;
                }
                if (android.text.TextUtils.isEmpty((CharSequence)s4)) {
                    s4 = com.navdy.hud.app.bluetooth.vcard.VCardUtils.constructNameFromElements(com.navdy.hud.app.bluetooth.vcard.VCardConfig.getNameOrderType(this.mVCardType), s, s0, s1, s2, s3);
                }
                String[] a8 = new String[1];
                a8[0] = s4;
                boolean b4 = this.shouldAppendCharsetParam(a8);
                boolean b5 = this.mRefrainsQPToNameProperties;
                label2: {
                    label0: {
                        label1: {
                            if (b5) {
                                break label1;
                            }
                            String[] a9 = new String[1];
                            a9[0] = s4;
                            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a9)) {
                                break label0;
                            }
                        }
                        b1 = false;
                        break label2;
                    }
                    b1 = true;
                }
                if (b0) {
                    s5 = this.encodeQuotedPrintable(s);
                    s6 = this.encodeQuotedPrintable(s1);
                    s7 = this.encodeQuotedPrintable(s0);
                    s8 = this.encodeQuotedPrintable(s2);
                    s9 = this.encodeQuotedPrintable(s3);
                } else {
                    s5 = this.escapeCharacters(s);
                    s6 = this.escapeCharacters(s1);
                    s7 = this.escapeCharacters(s0);
                    s8 = this.escapeCharacters(s2);
                    s9 = this.escapeCharacters(s3);
                }
                String s10 = b1 ? this.encodeQuotedPrintable(s4) : this.escapeCharacters(s4);
                this.mBuilder.append("N");
                if (this.mIsDoCoMo) {
                    if (b2) {
                        this.mBuilder.append(";");
                        this.mBuilder.append(this.mVCardCharsetParameter);
                    }
                    if (b0) {
                        this.mBuilder.append(";");
                        this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(s4);
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                    this.mBuilder.append(";");
                } else {
                    if (b2) {
                        this.mBuilder.append(";");
                        this.mBuilder.append(this.mVCardCharsetParameter);
                    }
                    if (b0) {
                        this.mBuilder.append(";");
                        this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
                    }
                    this.mBuilder.append(":");
                    this.mBuilder.append(s5);
                    this.mBuilder.append(";");
                    this.mBuilder.append(s6);
                    this.mBuilder.append(";");
                    this.mBuilder.append(s7);
                    this.mBuilder.append(";");
                    this.mBuilder.append(s8);
                    this.mBuilder.append(";");
                    this.mBuilder.append(s9);
                }
                this.mBuilder.append("\r\n");
                this.mBuilder.append("FN");
                if (b4) {
                    this.mBuilder.append(";");
                    this.mBuilder.append(this.mVCardCharsetParameter);
                }
                if (b1) {
                    this.mBuilder.append(";");
                    this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
                }
                this.mBuilder.append(":");
                this.mBuilder.append(s10);
                this.mBuilder.append("\r\n");
            }
            this.appendPhoneticNameFields(a1);
            a0 = this;
        }
        return a0;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendNickNames(java.util.List a) {
        boolean b = this.mIsV30OrV40;
        label0: {
            boolean b0 = false;
            label2: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    b0 = false;
                    break label2;
                }
                if (!this.mUsesAndroidProperty) {
                    break label0;
                }
                b0 = true;
            }
            if (a != null) {
                Object a0 = a.iterator();
                while(((java.util.Iterator)a0).hasNext()) {
                    android.content.ContentValues a1 = (android.content.ContentValues)((java.util.Iterator)a0).next();
                    String s = a1.getAsString("data1");
                    if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                        if (b0) {
                            this.appendAndroidSpecificProperty("vnd.android.cursor.item/nickname", a1);
                        } else {
                            this.appendLineWithCharsetAndQPDetection("NICKNAME", s);
                        }
                    }
                }
            }
        }
        return this;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendNotes(java.util.List a) {
        if (a != null) {
            if (this.mOnlyOneNoteFieldIsAvailable) {
                boolean b = false;
                StringBuilder a0 = new StringBuilder();
                java.util.Iterator a1 = a.iterator();
                boolean b0 = true;
                Object a2 = a1;
                while(((java.util.Iterator)a2).hasNext()) {
                    String s = ((android.content.ContentValues)((java.util.Iterator)a2).next()).getAsString("data1");
                    if (s == null) {
                        s = "";
                    }
                    if (s.length() > 0) {
                        if (b0) {
                            b0 = false;
                        } else {
                            a0.append((char)10);
                        }
                        a0.append(s);
                    }
                }
                String s0 = a0.toString();
                String[] a3 = new String[1];
                a3[0] = s0;
                boolean b1 = !com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(a3);
                boolean b2 = this.mShouldUseQuotedPrintable;
                label5: {
                    label3: {
                        label4: {
                            if (!b2) {
                                break label4;
                            }
                            String[] a4 = new String[1];
                            a4[0] = s0;
                            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a4)) {
                                break label3;
                            }
                        }
                        b = false;
                        break label5;
                    }
                    b = true;
                }
                this.appendLine("NOTE", s0, b1, b);
            } else {
                Object a5 = a.iterator();
                while(((java.util.Iterator)a5).hasNext()) {
                    String s1 = ((android.content.ContentValues)((java.util.Iterator)a5).next()).getAsString("data1");
                    if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                        boolean b3 = false;
                        String[] a6 = new String[1];
                        a6[0] = s1;
                        boolean b4 = !com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(a6);
                        boolean b5 = this.mShouldUseQuotedPrintable;
                        label2: {
                            label0: {
                                label1: {
                                    if (!b5) {
                                        break label1;
                                    }
                                    String[] a7 = new String[1];
                                    a7[0] = s1;
                                    if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a7)) {
                                        break label0;
                                    }
                                }
                                b3 = false;
                                break label2;
                            }
                            b3 = true;
                        }
                        this.appendLine("NOTE", s1, b4, b3);
                    }
                }
            }
        }
        return this;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendOrganizations(java.util.List a) {
        if (a != null) {
            Object a0 = a.iterator();
            while(((java.util.Iterator)a0).hasNext()) {
                boolean b = false;
                android.content.ContentValues a1 = (android.content.ContentValues)((java.util.Iterator)a0).next();
                String s = a1.getAsString("data1");
                if (s != null) {
                    s = s.trim();
                }
                String s0 = a1.getAsString("data5");
                if (s0 != null) {
                    s0 = s0.trim();
                }
                String s1 = a1.getAsString("data4");
                if (s1 != null) {
                    s1 = s1.trim();
                }
                StringBuilder a2 = new StringBuilder();
                if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                    a2.append(s);
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    if (a2.length() > 0) {
                        a2.append((char)59);
                    }
                    a2.append(s0);
                }
                String s2 = a2.toString();
                String[] a3 = new String[1];
                a3[0] = s2;
                boolean b0 = !com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(a3);
                boolean b1 = this.mShouldUseQuotedPrintable;
                label5: {
                    label3: {
                        label4: {
                            if (!b1) {
                                break label4;
                            }
                            String[] a4 = new String[1];
                            a4[0] = s2;
                            if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a4)) {
                                break label3;
                            }
                        }
                        b = false;
                        break label5;
                    }
                    b = true;
                }
                this.appendLine("ORG", s2, b0, b);
                if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                    boolean b2 = false;
                    String[] a5 = new String[1];
                    a5[0] = s1;
                    boolean b3 = !com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyPrintableAscii(a5);
                    boolean b4 = this.mShouldUseQuotedPrintable;
                    label2: {
                        label0: {
                            label1: {
                                if (!b4) {
                                    break label1;
                                }
                                String[] a6 = new String[1];
                                a6[0] = s1;
                                if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyNonCrLfPrintableAscii(a6)) {
                                    break label0;
                                }
                            }
                            b2 = false;
                            break label2;
                        }
                        b2 = true;
                    }
                    this.appendLine("TITLE", s1, b3, b2);
                }
            }
        }
        return this;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendPhones(java.util.List a, com.navdy.hud.app.bluetooth.vcard.VCardPhoneNumberTranslationCallback a0) {
        boolean b = false;
        if (a == null) {
            b = false;
        } else {
            java.util.HashSet a1 = new java.util.HashSet();
            java.util.Iterator a2 = a.iterator();
            Object a3 = a0;
            b = false;
            Object a4 = a2;
            label2: while(((java.util.Iterator)a4).hasNext()) {
                android.content.ContentValues a5 = (android.content.ContentValues)((java.util.Iterator)a4).next();
                Integer a6 = a5.getAsInteger("data2");
                String s = a5.getAsString("data3");
                Integer a7 = a5.getAsInteger("is_primary");
                boolean b0 = a7 != null && a7.intValue() > 0;
                String s0 = a5.getAsString("data1");
                if (s0 != null) {
                    s0 = s0.trim();
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                    int i = (a6 == null) ? 1 : a6.intValue();
                    if (a3 == null) {
                        if (i != 6 && !com.navdy.hud.app.bluetooth.vcard.VCardConfig.refrainPhoneNumberFormatting(this.mVCardType)) {
                            java.util.List a8 = this.splitPhoneNumbers(s0);
                            if (a8.isEmpty()) {
                                continue label2;
                            }
                            Object a9 = a8.iterator();
                            while(((java.util.Iterator)a9).hasNext()) {
                                String s1 = (String)((java.util.Iterator)a9).next();
                                if (!((java.util.Set)a1).contains(s1)) {
                                    String s2 = s1.replace((char)44, (char)112).replace((char)59, (char)119);
                                    if (android.text.TextUtils.equals((CharSequence)s2, (CharSequence)s1)) {
                                        StringBuilder a10 = new StringBuilder();
                                        int i0 = s1.length();
                                        int i1 = 0;
                                        while(i1 < i0) {
                                            int i2 = s1.charAt(i1);
                                            boolean b1 = Character.isDigit((char)i2);
                                            label0: {
                                                label1: {
                                                    if (b1) {
                                                        break label1;
                                                    }
                                                    if (i2 != 43) {
                                                        break label0;
                                                    }
                                                }
                                                a10.append((char)i2);
                                            }
                                            i1 = i1 + 1;
                                        }
                                        int i3 = com.navdy.hud.app.bluetooth.vcard.VCardUtils.getPhoneNumberFormat(this.mVCardType);
                                        s2 = com.navdy.hud.app.bluetooth.vcard.VCardUtils$PhoneNumberUtilsPort.formatNumber(a10.toString(), i3);
                                    }
                                    if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType) && !android.text.TextUtils.isEmpty((CharSequence)s2) && !s2.startsWith("tel:")) {
                                        s2 = new StringBuilder().append("tel:").append(s2).toString();
                                    }
                                    ((java.util.Set)a1).add(s1);
                                    this.appendTelLine(Integer.valueOf(i), s, s2, b0);
                                }
                            }
                            b = true;
                            continue;
                        }
                        if (((java.util.Set)a1).contains(s0)) {
                            b = true;
                        } else {
                            ((java.util.Set)a1).add(s0);
                            this.appendTelLine(Integer.valueOf(i), s, s0, b0);
                            b = true;
                        }
                    } else {
                        String s3 = ((com.navdy.hud.app.bluetooth.vcard.VCardPhoneNumberTranslationCallback)a3).onValueReceived(s0, i, s, b0);
                        if (!((java.util.Set)a1).contains(s3)) {
                            ((java.util.Set)a1).add(s3);
                            this.appendTelLine(Integer.valueOf(i), s, s3, b0);
                        }
                    }
                }
            }
        }
        if (!b && this.mIsDoCoMo) {
            this.appendTelLine(Integer.valueOf(1), "", "", false);
        }
        return this;
    }
    
    public void appendPhotoLine(String s, String s0) {
        StringBuilder a = new StringBuilder();
        a.append("PHOTO");
        a.append(";");
        if (this.mIsV30OrV40) {
            a.append("ENCODING=B");
        } else {
            a.append("ENCODING=BASE64");
        }
        a.append(";");
        this.appendTypeParameter(a, s0);
        a.append(":");
        a.append(s);
        String s1 = a.toString();
        StringBuilder a0 = new StringBuilder();
        int i = s1.length();
        int i0 = 75 - "\r\n".length();
        int i1 = " ".length();
        int i2 = 0;
        int i3 = i0;
        int i4 = 0;
        while(i4 < i) {
            int i5 = s1.charAt(i4);
            a0.append((char)i5);
            i2 = i2 + 1;
            if (i2 > i3) {
                a0.append("\r\n");
                a0.append(" ");
                i3 = i0 - i1;
                i2 = 0;
            }
            i4 = i4 + 1;
        }
        this.mBuilder.append(a0.toString());
        this.mBuilder.append("\r\n");
        this.mBuilder.append("\r\n");
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendPhotos(java.util.List a) {
        if (a != null) {
            Object a0 = a.iterator();
            while(((java.util.Iterator)a0).hasNext()) {
                android.content.ContentValues a1 = (android.content.ContentValues)((java.util.Iterator)a0).next();
                if (a1 != null) {
                    byte[] a2 = a1.getAsByteArray("data15");
                    if (a2 != null) {
                        String s = com.navdy.hud.app.bluetooth.vcard.VCardUtils.guessImageType(a2);
                        if (s != null) {
                            String s0 = new String(android.util.Base64.encode(a2, 2));
                            if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                                this.appendPhotoLine(s0, s);
                            }
                        } else {
                            android.util.Log.d("vCard", "Unknown photo type. Ignored.");
                        }
                    }
                }
            }
        }
        return this;
    }
    
    public void appendPostalLine(int i, String s, android.content.ContentValues a, boolean b, boolean b0) {
        com.navdy.hud.app.bluetooth.vcard.VCardBuilder$PostalStruct a0 = this.tryConstructPostalStruct(a);
        label0: {
            boolean b1 = false;
            boolean b2 = false;
            String s0 = null;
            label2: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    b1 = a0.reallyUseQuotedPrintable;
                    b2 = a0.appendCharset;
                    s0 = a0.addressData;
                    break label2;
                }
                if (!b0) {
                    break label0;
                }
                s0 = "";
                b2 = false;
                b1 = false;
            }
            java.util.ArrayList a1 = new java.util.ArrayList();
            if (b) {
                ((java.util.List)a1).add("PREF");
            }
            switch(i) {
                case 2: {
                    ((java.util.List)a1).add("WORK");
                    break;
                }
                case 1: {
                    ((java.util.List)a1).add("HOME");
                    break;
                }
                case 0: {
                    if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                        break;
                    }
                    String[] a2 = new String[1];
                    a2[0] = s;
                    if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyAlphaDigitHyphen(a2)) {
                        break;
                    }
                    ((java.util.List)a1).add(new StringBuilder().append("X-").append(s).toString());
                    break;
                }
                default: {
                    android.util.Log.e("vCard", new StringBuilder().append("Unknown StructuredPostal type: ").append(i).toString());
                    break;
                }
                case 3: {
                }
            }
            this.mBuilder.append("ADR");
            if (!((java.util.List)a1).isEmpty()) {
                this.mBuilder.append(";");
                this.appendTypeParameters((java.util.List)a1);
            }
            if (b2) {
                this.mBuilder.append(";");
                this.mBuilder.append(this.mVCardCharsetParameter);
            }
            if (b1) {
                this.mBuilder.append(";");
                this.mBuilder.append("ENCODING=QUOTED-PRINTABLE");
            }
            this.mBuilder.append(":");
            this.mBuilder.append(s0);
            this.mBuilder.append("\r\n");
        }
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendPostals(java.util.List a) {
        label0: {
            label1: {
                label2: {
                    if (a == null) {
                        break label2;
                    }
                    if (!a.isEmpty()) {
                        break label1;
                    }
                }
                if (!this.mIsDoCoMo) {
                    break label0;
                }
                this.mBuilder.append("ADR");
                this.mBuilder.append(";");
                this.mBuilder.append("HOME");
                this.mBuilder.append(":");
                this.mBuilder.append("\r\n");
                break label0;
            }
            if (this.mIsDoCoMo) {
                this.appendPostalsForDoCoMo(a);
            } else {
                this.appendPostalsForGeneric(a);
            }
        }
        return this;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendRelation(java.util.List a) {
        if (this.mUsesAndroidProperty && a != null) {
            Object a0 = a.iterator();
            while(((java.util.Iterator)a0).hasNext()) {
                android.content.ContentValues a1 = (android.content.ContentValues)((java.util.Iterator)a0).next();
                if (a1 != null) {
                    this.appendAndroidSpecificProperty("vnd.android.cursor.item/relation", a1);
                }
            }
        }
        return this;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendSipAddresses(java.util.List a) {
        boolean b = this.mIsV30OrV40;
        label0: {
            boolean b0 = false;
            label2: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    b0 = false;
                    break label2;
                }
                if (!this.mUsesDefactProperty) {
                    break label0;
                }
                b0 = true;
            }
            if (a != null) {
                Object a0 = a.iterator();
                while(((java.util.Iterator)a0).hasNext()) {
                    String s = ((android.content.ContentValues)((java.util.Iterator)a0).next()).getAsString("data1");
                    if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                        if (b0) {
                            if (s.startsWith("sip:")) {
                                if (s.length() == 4) {
                                    continue;
                                }
                                s = s.substring(4);
                            }
                            this.appendLineWithCharsetAndQPDetection("X-SIP", s);
                        } else {
                            if (!s.startsWith("sip:")) {
                                s = new StringBuilder().append("sip:").append(s).toString();
                            }
                            this.appendLineWithCharsetAndQPDetection((com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType)) ? "TEL" : "IMPP", s);
                        }
                    }
                }
            }
        }
        return this;
    }
    
    public void appendTelLine(Integer a, String s, String s0, boolean b) {
        this.mBuilder.append("TEL");
        this.mBuilder.append(";");
        int i = (a != null) ? a.intValue() : 7;
        java.util.ArrayList a0 = new java.util.ArrayList();
        switch(i) {
            case 20: {
                a0.add("MSG");
                break;
            }
            case 18: {
                a0.add("WORK");
                if (this.mIsDoCoMo) {
                    a0.add("VOICE");
                    break;
                } else {
                    a0.add("PAGER");
                    break;
                }
            }
            case 17: {
                String[] a1 = new String[2];
                a1[0] = "WORK";
                a1[1] = "CELL";
                a0.addAll((java.util.Collection)java.util.Arrays.asList((Object[])a1));
                break;
            }
            case 15: {
                a0.add("TLX");
                break;
            }
            case 13: {
                a0.add("FAX");
                break;
            }
            case 12: {
                b = true;
                break;
            }
            case 11: {
                a0.add("ISDN");
                break;
            }
            case 10: {
                a0.add("WORK");
                b = true;
                break;
            }
            case 9: {
                a0.add("CAR");
                break;
            }
            case 7: {
                a0.add("VOICE");
                break;
            }
            case 6: {
                if (this.mIsDoCoMo) {
                    a0.add("VOICE");
                    break;
                } else {
                    a0.add("PAGER");
                    break;
                }
            }
            case 5: {
                String[] a2 = new String[2];
                a2[0] = "HOME";
                a2[1] = "FAX";
                a0.addAll((java.util.Collection)java.util.Arrays.asList((Object[])a2));
                break;
            }
            case 4: {
                String[] a3 = new String[2];
                a3[0] = "WORK";
                a3[1] = "FAX";
                a0.addAll((java.util.Collection)java.util.Arrays.asList((Object[])a3));
                break;
            }
            case 3: {
                String[] a4 = new String[1];
                a4[0] = "WORK";
                a0.addAll((java.util.Collection)java.util.Arrays.asList((Object[])a4));
                break;
            }
            case 2: {
                a0.add("CELL");
                break;
            }
            case 1: {
                String[] a5 = new String[1];
                a5[0] = "HOME";
                a0.addAll((java.util.Collection)java.util.Arrays.asList((Object[])a5));
                break;
            }
            case 0: {
                if (android.text.TextUtils.isEmpty((CharSequence)s)) {
                    a0.add("VOICE");
                    break;
                } else if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.isMobilePhoneLabel(s)) {
                    a0.add("CELL");
                    break;
                } else if (this.mIsV30OrV40) {
                    a0.add(s);
                    break;
                } else {
                    String s1 = s.toUpperCase();
                    if (com.navdy.hud.app.bluetooth.vcard.VCardUtils.isValidInV21ButUnknownToContactsPhoteType(s1)) {
                        a0.add(s1);
                        break;
                    } else {
                        String[] a6 = new String[1];
                        a6[0] = s;
                        if (!com.navdy.hud.app.bluetooth.vcard.VCardUtils.containsOnlyAlphaDigitHyphen(a6)) {
                            break;
                        }
                        a0.add(new StringBuilder().append("X-").append(s).toString());
                        break;
                    }
                }
            }
            default: {
                break;
            }
            case 8: case 14: case 16: case 19: {
            }
        }
        if (b) {
            a0.add("PREF");
        }
        if (a0.isEmpty()) {
            this.appendUncommonPhoneType(this.mBuilder, Integer.valueOf(i));
        } else {
            this.appendTypeParameters((java.util.List)a0);
        }
        this.mBuilder.append(":");
        this.mBuilder.append(s0);
        this.mBuilder.append("\r\n");
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardBuilder appendWebsites(java.util.List a) {
        if (a != null) {
            Object a0 = a.iterator();
            while(((java.util.Iterator)a0).hasNext()) {
                String s = ((android.content.ContentValues)((java.util.Iterator)a0).next()).getAsString("data1");
                if (s != null) {
                    s = s.trim();
                }
                if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                    this.appendLineWithCharsetAndQPDetection("URL", s);
                }
            }
        }
        return this;
    }
    
    public void clear() {
        this.mBuilder = new StringBuilder();
        this.mEndAppended = false;
        this.appendLine("BEGIN", "VCARD");
        if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion40(this.mVCardType)) {
            this.appendLine("VERSION", "4.0");
        } else if (com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion30(this.mVCardType)) {
            this.appendLine("VERSION", "3.0");
        } else {
            if (!com.navdy.hud.app.bluetooth.vcard.VCardConfig.isVersion21(this.mVCardType)) {
                android.util.Log.w("vCard", "Unknown vCard version detected.");
            }
            this.appendLine("VERSION", "2.1");
        }
    }
    
    public String toString() {
        if (!this.mEndAppended) {
            if (this.mIsDoCoMo) {
                this.appendLine("X-CLASS", "PUBLIC");
                this.appendLine("X-REDUCTION", "");
                this.appendLine("X-NO", "");
                this.appendLine("X-DCM-HMN-MODE", "");
            }
            this.appendLine("END", "VCARD");
            this.mEndAppended = true;
        }
        return this.mBuilder.toString();
    }
}
