package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry$NoteData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement {
    final public String mNote;
    
    public VCardEntry$NoteData(String s) {
        this.mNote = s;
    }
    
    public void constructInsertOperation(java.util.List a, int i) {
        android.content.ContentProviderOperation$Builder a0 = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract$Data.CONTENT_URI);
        a0.withValueBackReference("raw_contact_id", i);
        a0.withValue("mimetype", "vnd.android.cursor.item/note");
        a0.withValue("data1", this.mNote);
        a.add(a0.build());
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        if (this != a) {
            if (a instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry$NoteData) {
                com.navdy.hud.app.bluetooth.vcard.VCardEntry$NoteData a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$NoteData)a;
                b = android.text.TextUtils.equals((CharSequence)this.mNote, (CharSequence)a0.mNote);
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel getEntryLabel() {
        return com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel.NOTE;
    }
    
    public String getNote() {
        return this.mNote;
    }
    
    public int hashCode() {
        return (this.mNote == null) ? 0 : this.mNote.hashCode();
    }
    
    public boolean isEmpty() {
        return android.text.TextUtils.isEmpty((CharSequence)this.mNote);
    }
    
    public String toString() {
        return new StringBuilder().append("note: ").append(this.mNote).toString();
    }
}
