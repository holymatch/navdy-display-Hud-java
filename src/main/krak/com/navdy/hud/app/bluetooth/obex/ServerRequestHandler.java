package com.navdy.hud.app.bluetooth.obex;

public class ServerRequestHandler {
    private long mConnectionId;
    
    protected ServerRequestHandler() {
        this.mConnectionId = -1L;
    }
    
    public long getConnectionId() {
        return this.mConnectionId;
    }
    
    public boolean isSrmSupported() {
        return false;
    }
    
    public int onAbort(com.navdy.hud.app.bluetooth.obex.HeaderSet a, com.navdy.hud.app.bluetooth.obex.HeaderSet a0) {
        return 209;
    }
    
    public void onAuthenticationFailure(byte[] a) {
    }
    
    public void onClose() {
    }
    
    public int onConnect(com.navdy.hud.app.bluetooth.obex.HeaderSet a, com.navdy.hud.app.bluetooth.obex.HeaderSet a0) {
        return 160;
    }
    
    public int onDelete(com.navdy.hud.app.bluetooth.obex.HeaderSet a, com.navdy.hud.app.bluetooth.obex.HeaderSet a0) {
        return 209;
    }
    
    public void onDisconnect(com.navdy.hud.app.bluetooth.obex.HeaderSet a, com.navdy.hud.app.bluetooth.obex.HeaderSet a0) {
    }
    
    public int onGet(com.navdy.hud.app.bluetooth.obex.Operation a) {
        return 209;
    }
    
    public int onPut(com.navdy.hud.app.bluetooth.obex.Operation a) {
        return 209;
    }
    
    public int onSetPath(com.navdy.hud.app.bluetooth.obex.HeaderSet a, com.navdy.hud.app.bluetooth.obex.HeaderSet a0, boolean b, boolean b0) {
        return 209;
    }
    
    public void setConnectionId(long j) {
        if (j >= -1L && j <= 4294967295L) {
            this.mConnectionId = j;
            return;
        }
        throw new IllegalArgumentException("Illegal Connection ID");
    }
    
    public void updateStatus(String s) {
    }
}
