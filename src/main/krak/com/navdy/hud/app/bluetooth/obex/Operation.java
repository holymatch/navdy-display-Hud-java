package com.navdy.hud.app.bluetooth.obex;

abstract public interface Operation {
    abstract public void abort();
    
    
    abstract public void close();
    
    
    abstract public String getEncoding();
    
    
    abstract public int getHeaderLength();
    
    
    abstract public long getLength();
    
    
    abstract public int getMaxPacketSize();
    
    
    abstract public com.navdy.hud.app.bluetooth.obex.HeaderSet getReceivedHeader();
    
    
    abstract public int getResponseCode();
    
    
    abstract public String getType();
    
    
    abstract public void noBodyHeader();
    
    
    abstract public java.io.DataInputStream openDataInputStream();
    
    
    abstract public java.io.DataOutputStream openDataOutputStream();
    
    
    abstract public java.io.InputStream openInputStream();
    
    
    abstract public java.io.OutputStream openOutputStream();
    
    
    abstract public void sendHeaders(com.navdy.hud.app.bluetooth.obex.HeaderSet arg);
}
