package com.navdy.hud.app.bluetooth.vcard;

public class VCardEntry$NicknameData implements com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryElement {
    final private String mNickname;
    
    public VCardEntry$NicknameData(String s) {
        this.mNickname = s;
    }
    
    public void constructInsertOperation(java.util.List a, int i) {
        android.content.ContentProviderOperation$Builder a0 = android.content.ContentProviderOperation.newInsert(android.provider.ContactsContract$Data.CONTENT_URI);
        a0.withValueBackReference("raw_contact_id", i);
        a0.withValue("mimetype", "vnd.android.cursor.item/nickname");
        a0.withValue("data2", Integer.valueOf(1));
        a0.withValue("data1", this.mNickname);
        a.add(a0.build());
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        if (a instanceof com.navdy.hud.app.bluetooth.vcard.VCardEntry$NicknameData) {
            com.navdy.hud.app.bluetooth.vcard.VCardEntry$NicknameData a0 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$NicknameData)a;
            b = android.text.TextUtils.equals((CharSequence)this.mNickname, (CharSequence)a0.mNickname);
        } else {
            b = false;
        }
        return b;
    }
    
    public com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel getEntryLabel() {
        return com.navdy.hud.app.bluetooth.vcard.VCardEntry$EntryLabel.NICKNAME;
    }
    
    public String getNickname() {
        return this.mNickname;
    }
    
    public int hashCode() {
        return (this.mNickname == null) ? 0 : this.mNickname.hashCode();
    }
    
    public boolean isEmpty() {
        return android.text.TextUtils.isEmpty((CharSequence)this.mNickname);
    }
    
    public String toString() {
        return new StringBuilder().append("nickname: ").append(this.mNickname).toString();
    }
}
