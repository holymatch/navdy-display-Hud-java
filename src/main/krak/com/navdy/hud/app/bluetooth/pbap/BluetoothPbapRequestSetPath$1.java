package com.navdy.hud.app.bluetooth.pbap;

class BluetoothPbapRequestSetPath$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$bluetooth$pbap$BluetoothPbapRequestSetPath$SetPathDir;
    
    static {
        $SwitchMap$com$navdy$hud$app$bluetooth$pbap$BluetoothPbapRequestSetPath$SetPathDir = new int[com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$bluetooth$pbap$BluetoothPbapRequestSetPath$SetPathDir;
        com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir a0 = com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir.ROOT;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$bluetooth$pbap$BluetoothPbapRequestSetPath$SetPathDir[com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir.DOWN.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$bluetooth$pbap$BluetoothPbapRequestSetPath$SetPathDir[com.navdy.hud.app.bluetooth.pbap.BluetoothPbapRequestSetPath$SetPathDir.UP.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
    }
}
