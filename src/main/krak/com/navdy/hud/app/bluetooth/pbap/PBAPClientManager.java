package com.navdy.hud.app.bluetooth.pbap;

public class PBAPClientManager {
    final private static int CHECK_PBAP_CONNECTION_HANG_INTERVAL = 60000;
    final private static int CHECK_PBAP_PULL_PHONEBOOK_HANG_INTERVAL = 45000;
    final private static int INITIAL_CONNECTION_DELAY = 30000;
    final public static int MAX_RECENT_CALLS = 30;
    final private static int TYPE_HOME = 1;
    final private static int TYPE_MOBILE = 2;
    final private static int TYPE_WORK = 3;
    final private static int TYPE_WORK_MOBILE = 17;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.bluetooth.pbap.PBAPClientManager singleton;
    private android.bluetooth.BluetoothAdapter bluetoothAdapter;
    private Runnable checkPbapConnectionHang;
    private android.os.Handler handler;
    private android.os.Handler$Callback handlerCallback;
    private android.os.HandlerThread handlerThread;
    private long lastSyncAttemptTime;
    private com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient pbapClient;
    private com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager;
    private Runnable stopPbapClientRunnable;
    private Runnable syncRunnable;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager.class);
        singleton = new com.navdy.hud.app.bluetooth.pbap.PBAPClientManager();
    }
    
    public PBAPClientManager() {
        this.handlerCallback = (android.os.Handler$Callback)new com.navdy.hud.app.bluetooth.pbap.PBAPClientManager$1(this);
        this.checkPbapConnectionHang = (Runnable)new com.navdy.hud.app.bluetooth.pbap.PBAPClientManager$2(this);
        this.syncRunnable = (Runnable)new com.navdy.hud.app.bluetooth.pbap.PBAPClientManager$3(this);
        this.stopPbapClientRunnable = (Runnable)new com.navdy.hud.app.bluetooth.pbap.PBAPClientManager$4(this);
        this.remoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        this.remoteDeviceManager.getBus().register(this);
        this.handlerThread = new android.os.HandlerThread("HudPBAPClientManager");
        this.handlerThread.start();
        this.handler = new android.os.Handler(this.handlerThread.getLooper(), this.handlerCallback);
        android.bluetooth.BluetoothManager a = (android.bluetooth.BluetoothManager)com.navdy.hud.app.HudApplication.getAppContext().getSystemService("bluetooth");
        if (a != null) {
            this.bluetoothAdapter = a.getAdapter();
        }
        this.handler.removeCallbacks(this.syncRunnable);
        this.handler.postDelayed(this.syncRunnable, 30000L);
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static Runnable access$100(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager a) {
        return a.checkPbapConnectionHang;
    }
    
    static android.os.Handler access$200(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager a) {
        return a.handler;
    }
    
    static com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient access$300(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager a) {
        return a.pbapClient;
    }
    
    static void access$400(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager a, java.util.ArrayList a0) {
        a.buildRecentCallList(a0);
    }
    
    static void access$500(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager a) {
        a.stopPbapClient();
    }
    
    static Runnable access$600(com.navdy.hud.app.bluetooth.pbap.PBAPClientManager a) {
        return a.syncRunnable;
    }
    
    private void buildRecentCallList(java.util.ArrayList a) {
        label2: {
            Throwable a0 = null;
            com.navdy.service.library.log.Logger a1 = null;
            label0: {
                com.navdy.service.library.log.Logger a2 = null;
                try {
                    sLogger.v(new StringBuilder().append("recent calls vcard-list size[").append(a.size()).append("]").toString());
                    boolean b = !com.navdy.hud.app.util.DeviceUtil.isUserBuild();
                    java.util.LinkedHashMap a3 = new java.util.LinkedHashMap();
                    com.google.i18n.phonenumbers.PhoneNumberUtil a4 = com.google.i18n.phonenumbers.PhoneNumberUtil.getInstance();
                    String s = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentLocale().getCountry();
                    Object a5 = a.iterator();
                    while(((java.util.Iterator)a5).hasNext()) {
                        int i = 0;
                        Throwable a6 = null;
                        com.navdy.hud.app.bluetooth.vcard.VCardEntry a7 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry)((java.util.Iterator)a5).next();
                        if (b) {
                            sLogger.v(new StringBuilder().append("vcard entry:").append(a7).toString());
                        }
                        java.util.List a8 = a7.getPhoneList();
                        String s0 = null;
                        if (a8 == null) {
                            i = 0;
                        } else {
                            int i0 = a8.size();
                            s0 = null;
                            if (i0 <= 0) {
                                i = 0;
                            } else {
                                Object a9 = a8.iterator();
                                while(true) {
                                    boolean b0 = ((java.util.Iterator)a9).hasNext();
                                    s0 = null;
                                    if (b0) {
                                        com.navdy.hud.app.bluetooth.vcard.VCardEntry$PhoneData a10 = (com.navdy.hud.app.bluetooth.vcard.VCardEntry$PhoneData)((java.util.Iterator)a9).next();
                                        s0 = a10.getNumber();
                                        if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                                            continue;
                                        }
                                        i = a10.getType();
                                        break;
                                    } else {
                                        i = 0;
                                        break;
                                    }
                                }
                            }
                        }
                        if (s0 == null) {
                            sLogger.v("no phone number in vcard");
                            continue;
                        }
                        label1: {
                            com.google.i18n.phonenumbers.Phonenumber$PhoneNumber a11 = null;
                            try {
                                a11 = a4.parse(s0, s);
                            } catch(Throwable a12) {
                                a6 = a12;
                                break label1;
                            }
                            long j = a11.getNationalNumber();
                            com.navdy.hud.app.framework.recentcall.RecentCall a13 = (com.navdy.hud.app.framework.recentcall.RecentCall)((java.util.HashMap)a3).get(Long.valueOf(j));
                            if (a13 != null && !android.text.TextUtils.isEmpty((CharSequence)a13.name)) {
                                if (!b) {
                                    continue;
                                }
                                sLogger.v(new StringBuilder().append("Ignoring entry with duplicate phone number: ").append(j).toString());
                                continue;
                            }
                            String s1 = a7.getDisplayName();
                            if (!com.navdy.hud.app.framework.contacts.ContactUtil.isDisplayNameValid(s1, s0, j)) {
                                s1 = null;
                            }
                            int i1 = a7.getCallType();
                            java.util.Date a14 = a7.getCallTime();
                            if (a14 == null) {
                                sLogger.v(new StringBuilder().append("ignoring spam number:").append(s0).toString());
                                continue;
                            }
                            com.navdy.hud.app.framework.recentcall.RecentCall a15 = new com.navdy.hud.app.framework.recentcall.RecentCall(s1, com.navdy.hud.app.framework.recentcall.RecentCall$Category.PHONE_CALL, s0, this.getNumberType(i), a14, this.getCallType(i1), -1, j);
                            if (b) {
                                sLogger.v(new StringBuilder().append("adding recent call:").append(a15).toString());
                            }
                            ((java.util.HashMap)a3).put(Long.valueOf(j), a15);
                            if (((java.util.HashMap)a3).size() != 30) {
                                continue;
                            }
                            sLogger.v("exceeded max size");
                            break;
                        }
                        sLogger.e(a6);
                    }
                    sLogger.v(new StringBuilder().append("recent calls map size[").append(((java.util.HashMap)a3).size()).append("]").toString());
                    com.navdy.hud.app.profile.DriverProfile a16 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
                    java.util.ArrayList a17 = new java.util.ArrayList();
                    Object a18 = ((java.util.HashMap)a3).values().iterator();
                    while(((java.util.Iterator)a18).hasNext()) {
                        ((java.util.List)a17).add((com.navdy.hud.app.framework.recentcall.RecentCall)((java.util.Iterator)a18).next());
                    }
                    com.navdy.hud.app.storage.db.helper.RecentCallPersistenceHelper.storeRecentCalls(a16.getProfileName(), (java.util.List)a17, true);
                    a2 = sLogger;
                } catch(Throwable a19) {
                    a0 = a19;
                    break label0;
                }
                a2.v("buildRecentCallList: stopping client");
                this.stopPbapClient();
                break label2;
            }
            try {
                sLogger.e(a0);
                a1 = sLogger;
            } catch(Throwable a20) {
                sLogger.v("buildRecentCallList: stopping client");
                this.stopPbapClient();
                throw a20;
            }
            a1.v("buildRecentCallList: stopping client");
            this.stopPbapClient();
        }
    }
    
    private com.navdy.hud.app.framework.recentcall.RecentCall$CallType getCallType(int i) {
        com.navdy.hud.app.framework.recentcall.RecentCall$CallType a = null;
        switch(i) {
            case 12: {
                a = com.navdy.hud.app.framework.recentcall.RecentCall$CallType.MISSED;
                break;
            }
            case 11: {
                a = com.navdy.hud.app.framework.recentcall.RecentCall$CallType.OUTGOING;
                break;
            }
            case 10: {
                a = com.navdy.hud.app.framework.recentcall.RecentCall$CallType.INCOMING;
                break;
            }
            default: {
                a = com.navdy.hud.app.framework.recentcall.RecentCall$CallType.UNNKNOWN;
            }
        }
        return a;
    }
    
    public static com.navdy.hud.app.bluetooth.pbap.PBAPClientManager getInstance() {
        return singleton;
    }
    
    private com.navdy.hud.app.framework.contacts.NumberType getNumberType(int i) {
        com.navdy.hud.app.framework.contacts.NumberType a = null;
        switch(i) {
            case 17: {
                a = com.navdy.hud.app.framework.contacts.NumberType.WORK_MOBILE;
                break;
            }
            case 3: {
                a = com.navdy.hud.app.framework.contacts.NumberType.WORK;
                break;
            }
            case 2: {
                a = com.navdy.hud.app.framework.contacts.NumberType.MOBILE;
                break;
            }
            case 1: {
                a = com.navdy.hud.app.framework.contacts.NumberType.HOME;
                break;
            }
            default: {
                a = com.navdy.hud.app.framework.contacts.NumberType.OTHER;
            }
        }
        return a;
    }
    
    private void printCallMap(java.util.Collection a) {
        Object a0 = a.iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            com.navdy.hud.app.framework.recentcall.RecentCall a1 = (com.navdy.hud.app.framework.recentcall.RecentCall)((java.util.Iterator)a0).next();
            sLogger.v(new StringBuilder().append("[").append(a1.category).append("] ").append("[").append(a1.number).append("]").append("[").append(a1.numberType).append("]").append("[").append(a1.name).append("]").append("[").append(a1.callTime).append("]").append("[").append(a1.callType).append("]").toString());
        }
    }
    
    private void startPbapClient(String s) {
        try {
            if (this.pbapClient == null) {
                sLogger.v("startPbapClient");
                this.pbapClient = new com.navdy.hud.app.bluetooth.pbap.BluetoothPbapClient(this.bluetoothAdapter.getRemoteDevice(s), this.handler);
                this.pbapClient.connect();
                sLogger.v("started pbap client");
            }
        } catch(Throwable a) {
            sLogger.e("could not start pbap client", a);
        }
    }
    
    private void stopPbapClient() {
        label3: {
            Throwable a = null;
            label0: {
                label2: {
                    label1: try {
                        if (this.pbapClient == null) {
                            break label1;
                        }
                        sLogger.v("stopPbapClient");
                        this.pbapClient.disconnect();
                        sLogger.v("stopped pbap client");
                        break label2;
                    } catch(Throwable a0) {
                        a = a0;
                        break label0;
                    }
                    this.pbapClient = null;
                    this.lastSyncAttemptTime = 0L;
                    this.handler.removeCallbacks(this.checkPbapConnectionHang);
                    break label3;
                }
                this.pbapClient = null;
                this.lastSyncAttemptTime = 0L;
                this.handler.removeCallbacks(this.checkPbapConnectionHang);
                break label3;
            }
            try {
                sLogger.e("error stopping pbap client", a);
            } catch(Throwable a1) {
                this.pbapClient = null;
                this.lastSyncAttemptTime = 0L;
                this.handler.removeCallbacks(this.checkPbapConnectionHang);
                throw a1;
            }
            this.pbapClient = null;
            this.lastSyncAttemptTime = 0L;
            this.handler.removeCallbacks(this.checkPbapConnectionHang);
        }
    }
    
    public void onConnectionStatusChange(com.navdy.service.library.events.connection.ConnectionStateChange a) {
        if (com.navdy.hud.app.bluetooth.pbap.PBAPClientManager$5.$SwitchMap$com$navdy$service$library$events$connection$ConnectionStateChange$ConnectionState[a.state.ordinal()] != 0) {
            this.handler.post(this.stopPbapClientRunnable);
            com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance().clearRecentCalls();
            com.navdy.hud.app.framework.contacts.FavoriteContactsManager.getInstance().clearFavoriteContacts();
        }
    }
    
    public void onDeviceSyncRequired(com.navdy.hud.app.service.ConnectionHandler$DeviceSyncEvent a) {
        boolean b = a.amount == 1;
        if (b) {
            this.handler.removeCallbacks(this.syncRunnable);
            sLogger.v("trigger PBAP download");
            this.handler.post(this.syncRunnable);
        }
        com.navdy.hud.app.framework.contacts.FavoriteContactsManager.getInstance().syncFavoriteContacts(b);
    }
    
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        com.navdy.hud.app.framework.recentcall.RecentCallManager a0 = com.navdy.hud.app.framework.recentcall.RecentCallManager.getInstance();
        a0.clearContactLookupMap();
        a0.buildRecentCalls();
        com.navdy.hud.app.framework.contacts.FavoriteContactsManager.getInstance().buildFavoriteContacts();
    }
    
    public void syncRecentCalls() {
        try {
            sLogger.v("syncRecentCalls");
            if (this.lastSyncAttemptTime <= 0L) {
                this.handler.removeCallbacks(this.checkPbapConnectionHang);
                com.navdy.service.library.events.DeviceInfo a = this.remoteDeviceManager.getRemoteDeviceInfo();
                if (a != null) {
                    String s = new com.navdy.service.library.device.NavdyDeviceId(a.deviceId).getBluetoothAddress();
                    if (s != null) {
                        this.stopPbapClient();
                        this.lastSyncAttemptTime = android.os.SystemClock.elapsedRealtime();
                        sLogger.v("syncRecentCalls-attempting to connect");
                        this.startPbapClient(s);
                        this.handler.postDelayed(this.checkPbapConnectionHang, 60000L);
                    } else {
                        sLogger.i(new StringBuilder().append("BT Address n/a deviceId:").append(a.deviceId).toString());
                    }
                } else {
                    sLogger.v("no remote device, bailing out");
                }
            } else {
                sLogger.v("syncRecentCalls in progress");
            }
        } catch(Throwable a0) {
            sLogger.e("syncRecentCalls", a0);
            this.stopPbapClient();
        }
    }
}
