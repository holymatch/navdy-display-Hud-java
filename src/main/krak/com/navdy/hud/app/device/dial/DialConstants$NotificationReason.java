package com.navdy.hud.app.device.dial;


public enum DialConstants$NotificationReason {
    DISCONNECTED(0),
    CONNECTED(1),
    LOW_BATTERY(2),
    VERY_LOW_BATTERY(3),
    EXTREMELY_LOW_BATTERY(4),
    OK_BATTERY(5);

    private int value;
    DialConstants$NotificationReason(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class DialConstants$NotificationReason extends Enum {
//    final private static com.navdy.hud.app.device.dial.DialConstants$NotificationReason[] $VALUES;
//    final public static com.navdy.hud.app.device.dial.DialConstants$NotificationReason CONNECTED;
//    final public static com.navdy.hud.app.device.dial.DialConstants$NotificationReason DISCONNECTED;
//    final public static com.navdy.hud.app.device.dial.DialConstants$NotificationReason EXTREMELY_LOW_BATTERY;
//    final public static com.navdy.hud.app.device.dial.DialConstants$NotificationReason LOW_BATTERY;
//    final public static com.navdy.hud.app.device.dial.DialConstants$NotificationReason OK_BATTERY;
//    final public static com.navdy.hud.app.device.dial.DialConstants$NotificationReason VERY_LOW_BATTERY;
//    
//    static {
//        DISCONNECTED = new com.navdy.hud.app.device.dial.DialConstants$NotificationReason("DISCONNECTED", 0);
//        CONNECTED = new com.navdy.hud.app.device.dial.DialConstants$NotificationReason("CONNECTED", 1);
//        LOW_BATTERY = new com.navdy.hud.app.device.dial.DialConstants$NotificationReason("LOW_BATTERY", 2);
//        VERY_LOW_BATTERY = new com.navdy.hud.app.device.dial.DialConstants$NotificationReason("VERY_LOW_BATTERY", 3);
//        EXTREMELY_LOW_BATTERY = new com.navdy.hud.app.device.dial.DialConstants$NotificationReason("EXTREMELY_LOW_BATTERY", 4);
//        OK_BATTERY = new com.navdy.hud.app.device.dial.DialConstants$NotificationReason("OK_BATTERY", 5);
//        com.navdy.hud.app.device.dial.DialConstants$NotificationReason[] a = new com.navdy.hud.app.device.dial.DialConstants$NotificationReason[6];
//        a[0] = DISCONNECTED;
//        a[1] = CONNECTED;
//        a[2] = LOW_BATTERY;
//        a[3] = VERY_LOW_BATTERY;
//        a[4] = EXTREMELY_LOW_BATTERY;
//        a[5] = OK_BATTERY;
//        $VALUES = a;
//    }
//    
//    private DialConstants$NotificationReason(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.device.dial.DialConstants$NotificationReason valueOf(String s) {
//        return (com.navdy.hud.app.device.dial.DialConstants$NotificationReason)Enum.valueOf(com.navdy.hud.app.device.dial.DialConstants$NotificationReason.class, s);
//    }
//    
//    public static com.navdy.hud.app.device.dial.DialConstants$NotificationReason[] values() {
//        return $VALUES.clone();
//    }
//}
//