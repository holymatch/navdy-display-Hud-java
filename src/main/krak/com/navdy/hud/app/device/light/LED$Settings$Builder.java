package com.navdy.hud.app.device.light;

public class LED$Settings$Builder {
    private boolean activityBlink;
    private com.navdy.hud.app.device.light.LED$BlinkFrequency blinkFrequency;
    private boolean blinkInfinite;
    private int blinkPulseCount;
    private int color;
    private boolean isBlinking;
    private boolean isTurnedOn;
    private String name;
    
    public LED$Settings$Builder() {
        this.color = com.navdy.hud.app.device.light.LED.DEFAULT_COLOR;
        this.isTurnedOn = true;
        this.isBlinking = true;
        this.blinkFrequency = com.navdy.hud.app.device.light.LED$BlinkFrequency.LOW;
        this.blinkInfinite = true;
        this.blinkPulseCount = 0;
        this.activityBlink = false;
        this.name = "UNKNOWN";
    }
    
    public com.navdy.hud.app.device.light.LED$Settings build() {
        return new com.navdy.hud.app.device.light.LED$Settings(this.color, this.isTurnedOn, this.isBlinking, this.blinkFrequency, this.blinkInfinite, this.blinkPulseCount, this.activityBlink, this.name);
    }
    
    public com.navdy.hud.app.device.light.LED$Settings$Builder setActivityBlink(boolean b) {
        this.activityBlink = b;
        return this;
    }
    
    public com.navdy.hud.app.device.light.LED$Settings$Builder setBlinkFrequency(com.navdy.hud.app.device.light.LED$BlinkFrequency a) {
        this.blinkFrequency = a;
        return this;
    }
    
    public com.navdy.hud.app.device.light.LED$Settings$Builder setBlinkInfinite(boolean b) {
        this.blinkInfinite = b;
        return this;
    }
    
    public com.navdy.hud.app.device.light.LED$Settings$Builder setBlinkPulseCount(int i) {
        this.blinkPulseCount = i;
        return this;
    }
    
    public com.navdy.hud.app.device.light.LED$Settings$Builder setColor(int i) {
        this.color = i;
        return this;
    }
    
    public com.navdy.hud.app.device.light.LED$Settings$Builder setIsBlinking(boolean b) {
        this.isBlinking = b;
        return this;
    }
    
    public com.navdy.hud.app.device.light.LED$Settings$Builder setIsTurnedOn(boolean b) {
        this.isTurnedOn = b;
        return this;
    }
    
    public com.navdy.hud.app.device.light.LED$Settings$Builder setName(String s) {
        this.name = s;
        return this;
    }
}
