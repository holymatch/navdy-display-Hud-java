package com.navdy.hud.app.device.dial;

public class DialFirmwareUpdater {
    final private static int FIRMWARE_CHUNK_SIZE = 16;
    final public static String FIRMWARE_INCREMENTAL_VERSION_PREFIX = "Firmware incremental version: ";
    final public static String FIRMWARE_LOCAL_FILE = "/system/etc/firmware/dial.ota";
    final public static String FIRMWARE_VERSION_PREFIX = "Firmware version: ";
    final public static int LOG_PROGRESS_PERIOD = 30;
    final private static int OTA_COMMAND_BOOT_DFU = 3;
    final private static int OTA_COMMAND_ERASE_BLOCK_0 = 0;
    final private static int OTA_COMMAND_ERASE_BLOCK_1 = 1;
    final private static int OTA_COMMAND_PREPARE = 4;
    final private static int OTA_COMMAND_RESET_DFU_PTR = 2;
    final private static int OTA_STATE_CANCELLED = 1;
    final private static int OTA_STATE_NONE = 0;
    final private static int OTA_STATE_READY_TO_REBOOT = 32;
    final private static int OTA_STATE_STARTED = 16;
    final private static int RESP_ERROR_OTA_ALREADY_STARTED = 131;
    final private static int RESP_ERROR_OTA_INVALID_COMMAND = 128;
    final private static int RESP_ERROR_OTA_TOO_LARGE = 129;
    final private static int RESP_ERROR_OTA_UNKNOWN_STATE = 130;
    final private static int RESP_OK = 0;
    final private static int UNDETERMINED_FIRMWARE_VERSION = -1;
    final private static long UPDATE_FINISH_DELAY = 5000L;
    final private static long UPDATE_INTERPACKET_DELAY = 50L;
    final private static com.navdy.service.library.log.Logger sLogger;
    private int commandSent;
    private android.bluetooth.BluetoothGattCharacteristic deviceInfoFirmwareRevisionCharacteristic;
    final private com.navdy.hud.app.device.dial.DialManager dialManager;
    private String dialName;
    private byte[] firmwareData;
    private int firmwareOffset;
    private android.os.Handler handler;
    private android.bluetooth.BluetoothGattCharacteristic otaControlCharacteristic;
    private android.bluetooth.BluetoothGattCharacteristic otaDataCharacteristic;
    private android.bluetooth.BluetoothGattCharacteristic otaIncrementalVersionCharacteristic;
    private Runnable sendFirmwareChunkRunnable;
    private com.navdy.hud.app.device.dial.DialFirmwareUpdater$UpdateListener updateListener;
    private com.navdy.hud.app.device.dial.DialFirmwareUpdater$UpdateProgressListener updateProgressListener;
    private java.util.concurrent.atomic.AtomicBoolean updateRunning;
    private com.navdy.hud.app.device.dial.DialFirmwareUpdater$Versions versions;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.dial.DialFirmwareUpdater.class);
    }
    
    public DialFirmwareUpdater(com.navdy.hud.app.device.dial.DialManager a, android.os.Handler a0) {
        this.versions = new com.navdy.hud.app.device.dial.DialFirmwareUpdater$Versions(this);
        this.updateRunning = new java.util.concurrent.atomic.AtomicBoolean(false);
        this.dialName = null;
        this.sendFirmwareChunkRunnable = (Runnable)new com.navdy.hud.app.device.dial.DialFirmwareUpdater$1(this);
        this.dialManager = a;
        this.handler = a0;
        label0: {
            try {
                try {
                    this.firmwareData = com.navdy.service.library.util.IOUtils.readBinaryFile("/system/etc/firmware/dial.ota");
                    this.versions.local.reset();
                    String s = com.navdy.hud.app.device.dial.DialFirmwareUpdater.extractVersion(this.firmwareData, "Firmware incremental version: ");
                    if (s != null) {
                        this.versions.local.incrementalVersion = Integer.parseInt(s);
                    }
                    this.versions.local.revisionString = com.navdy.hud.app.device.dial.DialFirmwareUpdater.extractVersion(this.firmwareData, "Firmware version: ");
                    break label0;
                } catch(java.io.FileNotFoundException ignoredException) {
                }
            } catch(Throwable a1) {
                sLogger.e("can't read dial firmware OTA file", a1);
                break label0;
            }
            sLogger.w("no dial firmware OTA file found");
        }
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static void access$100(com.navdy.hud.app.device.dial.DialFirmwareUpdater a) {
        a.sendFirmwareChunk();
    }
    
    static void access$200(com.navdy.hud.app.device.dial.DialFirmwareUpdater a, com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error a0, String s) {
        a.finishUpdate(a0, s);
    }
    
    private static String extractVersion(byte[] a, String s) {
        String s0 = null;
        int i = com.navdy.hud.app.device.dial.DialFirmwareUpdater.indexOf(a, s.getBytes());
        if (i >= 0) {
            int i0 = i;
            while(true) {
                int i1 = a.length;
                s0 = null;
                if (i0 >= i1) {
                    break;
                }
                int i2 = a[i0];
                {
                    if (i2 != 0) {
                        int i3 = a[i0];
                        if (i3 != 13) {
                            i0 = i0 + 1;
                            continue;
                        }
                    }
                    s0 = new String(java.util.Arrays.copyOfRange(a, s.length() + i, i0));
                    break;
                }
            }
        } else {
            s0 = null;
        }
        return s0;
    }
    
    private void finishUpdate(com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error a, String s) {
        sLogger.i(new StringBuilder().append("finished dial update error: ").append(a).append(" , ").append(s).toString());
        if (this.updateProgressListener != null) {
            this.updateProgressListener.onFinished(a, s);
        }
    }
    
    public static int indexOf(byte[] a, byte[] a0) {
        int i = 0;
        while(true) {
            if (i >= a.length - a0.length + 1) {
                i = -1;
            } else {
                boolean b = false;
                int i0 = 0;
                while(true) {
                    if (i0 >= a0.length) {
                        b = true;
                        break;
                    } else {
                        int i1 = a[i + i0];
                        int i2 = a0[i0];
                        if (i1 == i2) {
                            i0 = i0 + 1;
                            continue;
                        }
                        b = false;
                        break;
                    }
                }
                if (!b) {
                    i = i + 1;
                    continue;
                }
            }
            return i;
        }
    }
    
    private void sendCommand(int i) {
        sLogger.d(new StringBuilder().append("sendCommand: ").append(i).toString());
        this.commandSent = i;
        com.navdy.hud.app.device.dial.DialManager a = this.dialManager;
        android.bluetooth.BluetoothGattCharacteristic a0 = this.otaControlCharacteristic;
        byte[] a1 = new byte[1];
        int i0 = (byte)i;
        a1[0] = (byte)i0;
        a.queueWrite(a0, a1);
    }
    
    private void sendFirmwareChunk() {
        if (this.firmwareOffset / 16 % 30 == 0) {
            sLogger.d(new StringBuilder().append("firmwareOffset: ").append(this.firmwareOffset).toString());
            if (this.updateProgressListener != null) {
                this.updateProgressListener.onProgress(this.firmwareOffset * 100 / this.firmwareData.length);
            }
        }
        byte[] a = java.util.Arrays.copyOfRange(this.firmwareData, this.firmwareOffset, this.firmwareOffset + 16);
        this.dialManager.queueWrite(this.otaDataCharacteristic, a);
        this.firmwareOffset = this.firmwareOffset + 16;
    }
    
    private void startFirmwareTransfer() {
        this.firmwareOffset = 0;
        this.sendFirmwareChunk();
    }
    
    public void cancelUpdate() {
        if (this.updateRunning.compareAndSet(true, false)) {
            this.finishUpdate(com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error.CANCELLED, (String)null);
        }
    }
    
    public boolean characteristicsAvailable() {
        boolean b = false;
        android.bluetooth.BluetoothGattCharacteristic a = this.otaControlCharacteristic;
        label2: {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (this.otaDataCharacteristic == null) {
                        break label1;
                    }
                    if (this.otaIncrementalVersionCharacteristic == null) {
                        break label1;
                    }
                    if (this.deviceInfoFirmwareRevisionCharacteristic != null) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    public String getDialName() {
        return this.dialName;
    }
    
    public com.navdy.hud.app.device.dial.DialFirmwareUpdater$Versions getVersions() {
        return this.versions;
    }
    
    public void onCharacteristicRead(android.bluetooth.BluetoothGattCharacteristic a, int i) {
        if (this.characteristicsAvailable()) {
            if (a == this.otaIncrementalVersionCharacteristic) {
                if (i != 0) {
                    sLogger.e("can't read dial firmware incremental version");
                } else {
                    String s = new String(a.getValue());
                    this.versions.dial.incrementalVersion = Integer.parseInt(s);
                    com.navdy.service.library.log.Logger a0 = sLogger;
                    Object[] a1 = new Object[3];
                    a1[0] = Boolean.valueOf(this.versions.isUpdateAvailable());
                    a1[1] = Integer.valueOf(this.versions.dial.incrementalVersion);
                    a1[2] = Integer.valueOf(this.versions.local.incrementalVersion);
                    a0.i(String.format("updateAvailable: %s (%d -> %d)", a1));
                    if (this.updateListener != null) {
                        this.updateListener.onUpdateState(this.versions.isUpdateAvailable());
                    }
                }
            }
            if (a == this.deviceInfoFirmwareRevisionCharacteristic) {
                if (i != 0) {
                    sLogger.e("can't read dial firmware revisionString string");
                } else {
                    this.versions.dial.revisionString = new String(a.getValue());
                    sLogger.i(new StringBuilder().append("dial.revisionString: ").append(this.versions.dial.revisionString).toString());
                    this.dialManager.queueRead(this.otaIncrementalVersionCharacteristic);
                }
            }
        }
    }
    
    public void onCharacteristicWrite(android.bluetooth.BluetoothGatt a, android.bluetooth.BluetoothGattCharacteristic a0, int i) {
        if (this.characteristicsAvailable()) {
            if (a0 != this.otaControlCharacteristic) {
                if (a0 == this.otaDataCharacteristic) {
                    if (i != 0) {
                        sLogger.d(new StringBuilder().append("dial OTA error response for data chunk, status: ").append(i).toString());
                        this.finishUpdate(com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error.INVALID_RESPONSE, String.valueOf(i));
                    } else if (this.updateRunning.get()) {
                        if (this.firmwareOffset >= this.firmwareData.length) {
                            sLogger.v("f/w upgrade complete, send OTA_COMMAND_BOOT_DFU, mark updating false");
                            this.updateRunning.set(false);
                            this.sendCommand(3);
                            android.bluetooth.BluetoothDevice a1 = a.getDevice();
                            this.handler.postDelayed((Runnable)new com.navdy.hud.app.device.dial.DialFirmwareUpdater$2(this, a1), 5000L);
                        } else {
                            this.handler.postDelayed(this.sendFirmwareChunkRunnable, 50L);
                        }
                    }
                }
            } else {
                sLogger.d(new StringBuilder().append("dial OTA response for command ").append(this.commandSent).append(", status: ").append(i).toString());
                if (i != 0) {
                    if (this.commandSent != 3) {
                        this.finishUpdate(com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error.INVALID_COMMAND, String.valueOf(this.commandSent));
                    }
                } else {
                    switch(this.commandSent) {
                        case 4: {
                            this.sendCommand(0);
                            break;
                        }
                        case 2: {
                            this.startFirmwareTransfer();
                            break;
                        }
                        case 1: {
                            this.sendCommand(2);
                            break;
                        }
                        case 0: {
                            this.sendCommand(1);
                            break;
                        }
                    }
                }
            }
        }
    }
    
    public void onServicesDiscovered(android.bluetooth.BluetoothGatt a) {
        this.otaControlCharacteristic = null;
        this.otaDataCharacteristic = null;
        this.otaIncrementalVersionCharacteristic = null;
        this.deviceInfoFirmwareRevisionCharacteristic = null;
        this.dialName = this.dialManager.getDialName();
        Object a0 = a.getServices().iterator();
        while(((java.util.Iterator)a0).hasNext()) {
            android.bluetooth.BluetoothGattService a1 = (android.bluetooth.BluetoothGattService)((java.util.Iterator)a0).next();
            java.util.UUID a2 = a1.getUuid();
            sLogger.d(new StringBuilder().append("service: ").append(a2.toString()).toString());
            if (com.navdy.hud.app.device.dial.DialConstants.OTA_SERVICE_UUID.equals(a2)) {
                Object a3 = a1.getCharacteristics().iterator();
                while(((java.util.Iterator)a3).hasNext()) {
                    android.bluetooth.BluetoothGattCharacteristic a4 = (android.bluetooth.BluetoothGattCharacteristic)((java.util.Iterator)a3).next();
                    if (com.navdy.hud.app.device.dial.DialConstants.OTA_CONTROL_CHARACTERISTIC_UUID.equals(a4.getUuid())) {
                        this.otaControlCharacteristic = a4;
                        this.otaControlCharacteristic.setWriteType(2);
                    } else if (com.navdy.hud.app.device.dial.DialConstants.OTA_DATA_CHARACTERISTIC_UUID.equals(a4.getUuid())) {
                        this.otaDataCharacteristic = a4;
                        this.otaDataCharacteristic.setWriteType(1);
                    } else if (com.navdy.hud.app.device.dial.DialConstants.OTA_INCREMENTAL_VERSION_CHARACTERISTIC_UUID.equals(a4.getUuid())) {
                        this.otaIncrementalVersionCharacteristic = a4;
                    }
                }
            } else if (com.navdy.hud.app.device.dial.DialConstants.DEVICE_INFO_SERVICE_UUID.equals(a2)) {
                Object a5 = a1.getCharacteristics().iterator();
                while(((java.util.Iterator)a5).hasNext()) {
                    android.bluetooth.BluetoothGattCharacteristic a6 = (android.bluetooth.BluetoothGattCharacteristic)((java.util.Iterator)a5).next();
                    if (com.navdy.hud.app.device.dial.DialConstants.FIRMWARE_REVISION_CHARACTERISTIC_UUID.equals(a6.getUuid())) {
                        this.deviceInfoFirmwareRevisionCharacteristic = a6;
                    }
                }
            }
        }
        if (this.characteristicsAvailable()) {
            sLogger.d("required OTA characteristics found");
            this.startVersionDetection();
        } else {
            sLogger.e("required OTA characteristics not found");
        }
    }
    
    public void setUpdateListener(com.navdy.hud.app.device.dial.DialFirmwareUpdater$UpdateListener a) {
        this.updateListener = a;
    }
    
    public boolean startUpdate(com.navdy.hud.app.device.dial.DialFirmwareUpdater$UpdateProgressListener a) {
        boolean b = false;
        sLogger.i("starting dial firmware update");
        if (this.firmwareData.length % 16 == 0) {
            if (this.updateRunning.compareAndSet(false, true)) {
                this.updateProgressListener = a;
                this.sendCommand(4);
                b = true;
            } else {
                b = false;
            }
        } else {
            sLogger.e("dial firmware OTA file size is not divisible by 16, aborting update");
            a.onFinished(com.navdy.hud.app.device.dial.DialFirmwareUpdater$Error.INVALID_IMAGE_SIZE, "dial firmware OTA file size is not divisible by 16, aborting update");
            b = false;
        }
        return b;
    }
    
    public boolean startVersionDetection() {
        boolean b = false;
        this.versions.dial.reset();
        if (this.characteristicsAvailable()) {
            this.dialManager.queueRead(this.deviceInfoFirmwareRevisionCharacteristic);
            b = true;
        } else {
            b = false;
        }
        return b;
    }
}
