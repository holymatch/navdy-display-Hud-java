package com.navdy.hud.app.device.dial;


public enum DialConstants$DialConnectionStatus$Status {
    CONNECTED(0),
    DISCONNECTED(1),
    CONNECTING(2),
    CONNECTION_FAILED(3),
    NO_DIAL_FOUND(4);

    private int value;
    DialConstants$DialConnectionStatus$Status(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class DialConstants$DialConnectionStatus$Status extends Enum {
//    final private static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status[] $VALUES;
//    final public static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status CONNECTED;
//    final public static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status CONNECTING;
//    final public static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status CONNECTION_FAILED;
//    final public static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status DISCONNECTED;
//    final public static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status NO_DIAL_FOUND;
//    
//    static {
//        CONNECTED = new com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status("CONNECTED", 0);
//        DISCONNECTED = new com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status("DISCONNECTED", 1);
//        CONNECTING = new com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status("CONNECTING", 2);
//        CONNECTION_FAILED = new com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status("CONNECTION_FAILED", 3);
//        NO_DIAL_FOUND = new com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status("NO_DIAL_FOUND", 4);
//        com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status[] a = new com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status[5];
//        a[0] = CONNECTED;
//        a[1] = DISCONNECTED;
//        a[2] = CONNECTING;
//        a[3] = CONNECTION_FAILED;
//        a[4] = NO_DIAL_FOUND;
//        $VALUES = a;
//    }
//    
//    private DialConstants$DialConnectionStatus$Status(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status valueOf(String s) {
//        return (com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status)Enum.valueOf(com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status.class, s);
//    }
//    
//    public static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status[] values() {
//        return $VALUES.clone();
//    }
//}
//