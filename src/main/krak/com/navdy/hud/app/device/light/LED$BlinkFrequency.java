package com.navdy.hud.app.device.light;


    public enum LED$BlinkFrequency {
        LOW(0),
    HIGH(1);

        private int value;
        LED$BlinkFrequency(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class LED$BlinkFrequency extends Enum {
//    final private static com.navdy.hud.app.device.light.LED$BlinkFrequency[] $VALUES;
//    final public static com.navdy.hud.app.device.light.LED$BlinkFrequency HIGH;
//    final public static com.navdy.hud.app.device.light.LED$BlinkFrequency LOW;
//    private int blinkDelay;
//    
//    static {
//        LOW = new com.navdy.hud.app.device.light.LED$BlinkFrequency("LOW", 0, 1000);
//        HIGH = new com.navdy.hud.app.device.light.LED$BlinkFrequency("HIGH", 1, 100);
//        com.navdy.hud.app.device.light.LED$BlinkFrequency[] a = new com.navdy.hud.app.device.light.LED$BlinkFrequency[2];
//        a[0] = LOW;
//        a[1] = HIGH;
//        $VALUES = a;
//    }
//    
//    private LED$BlinkFrequency(String s, int i, int i0) {
//        super(s, i);
//        this.blinkDelay = i0;
//    }
//    
//    static int access$000(com.navdy.hud.app.device.light.LED$BlinkFrequency a) {
//        return a.blinkDelay;
//    }
//    
//    public static com.navdy.hud.app.device.light.LED$BlinkFrequency valueOf(String s) {
//        return (com.navdy.hud.app.device.light.LED$BlinkFrequency)Enum.valueOf(com.navdy.hud.app.device.light.LED$BlinkFrequency.class, s);
//    }
//    
//    public static com.navdy.hud.app.device.light.LED$BlinkFrequency[] values() {
//        return $VALUES.clone();
//    }
//    
//    public int getBlinkDelay() {
//        return this.blinkDelay;
//    }
//}
//