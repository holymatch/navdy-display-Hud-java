package com.navdy.hud.app.device.dial;

class DialManager$9$1 implements com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection {
    final com.navdy.hud.app.device.dial.DialManager$9 this$1;
    final android.bluetooth.BluetoothDevice val$device;
    
    DialManager$9$1(com.navdy.hud.app.device.dial.DialManager$9 a, android.bluetooth.BluetoothDevice a0) {
        super();
        this.this$1 = a;
        this.val$device = a0;
    }
    
    public void onAttemptConnection(boolean b) {
        com.navdy.hud.app.device.dial.DialManager.sLogger.v(new StringBuilder().append("attempt connection [").append(this.val$device.getName()).append(" ] success[").append(b).append("]").toString());
        if (b) {
            com.navdy.hud.app.device.dial.DialManager.access$1500(this.this$1.this$0);
            com.navdy.hud.app.device.dial.DialManager.sLogger.i(new StringBuilder().append("[Dial]btConnRecvr: dial connected [").append(this.val$device.getName()).append("] addr:").append(this.val$device.getAddress()).toString());
        }
    }
    
    public void onAttemptDisconnection(boolean b) {
    }
}
