package com.navdy.hud.app.device.dial;

final class DialManagerHelper$6 implements Runnable {
    final com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection val$callBack;
    
    DialManagerHelper$6(com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection a) {
        super();
        this.val$callBack = a;
    }
    
    public void run() {
        this.val$callBack.onAttemptConnection(false);
    }
}
