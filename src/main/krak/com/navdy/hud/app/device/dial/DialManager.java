package com.navdy.hud.app.device.dial;

public class DialManager {
    final private static String BALANCED = "balanced";
    final private static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus DIAL_CONNECTED;
    final private static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus DIAL_CONNECTING;
    final private static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus DIAL_CONNECTION_FAILED;
    final private static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus DIAL_NOT_CONNECTED;
    final private static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus DIAL_NOT_FOUND;
    final private static String DIAL_POWER_SETTING = "persist.sys.bt.dial.pwr";
    final private static String HIGH_POWER = "high";
    final private static String LOW_POWER = "low";
    final private static java.util.Map PowerMap;
    final static String TAG_DIAL = "[Dial]";
    final static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.device.dial.DialManager singleton;
    private android.bluetooth.BluetoothGattCharacteristic batteryCharateristic;
    private Runnable batteryReadRunnable;
    private android.bluetooth.BluetoothAdapter bluetoothAdapter;
    private android.content.BroadcastReceiver bluetoothBondingReceiver;
    private android.content.BroadcastReceiver bluetoothConnectivityReceiver;
    private android.bluetooth.BluetoothGatt bluetoothGatt;
    private android.bluetooth.le.BluetoothLeScanner bluetoothLEScanner;
    private android.bluetooth.BluetoothManager bluetoothManager;
    private android.content.BroadcastReceiver bluetoothOnOffReceiver;
    private Runnable bondHangRunnable;
    private int bondTries;
    private java.util.ArrayList bondedDials;
    private android.bluetooth.BluetoothDevice bondingDial;
    private com.squareup.otto.Bus bus;
    private volatile android.bluetooth.BluetoothDevice connectedDial;
    private Runnable connectionErrorRunnable;
    private Runnable deviceInfoGattReadRunnable;
    private Thread dialEventsListenerThread;
    private com.navdy.hud.app.device.dial.DialFirmwareUpdater dialFirmwareUpdater;
    private android.bluetooth.BluetoothGattCharacteristic dialForgetKeysCharacteristic;
    private volatile boolean dialManagerInitialized;
    private android.bluetooth.BluetoothGattCharacteristic dialRebootCharacteristic;
    private String disconnectedDial;
    private int encryptionRetryCount;
    private String firmWareVersion;
    private android.bluetooth.BluetoothGattCharacteristic firmwareVersionCharacteristic;
    private android.bluetooth.BluetoothGattCallback gattCallback;
    private com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor gattCharacteristicProcessor;
    private android.os.Handler handler;
    private android.os.HandlerThread handlerThread;
    private String hardwareVersion;
    private android.bluetooth.BluetoothGattCharacteristic hardwareVersionCharacteristic;
    private android.bluetooth.BluetoothGattCharacteristic hidHostReadyCharacteristic;
    private Runnable hidHostReadyReadRunnable;
    private java.util.Set ignoredNonNavdyDials;
    private volatile boolean isGattOn;
    private volatile boolean isScanning;
    private int lastKnownBatteryLevel;
    private Integer lastKnownRawBatteryLevel;
    private Integer lastKnownSystemTemperature;
    private android.bluetooth.le.ScanCallback leScanCallback;
    private com.navdy.hud.app.device.dial.DialConstants$NotificationReason notificationReasonBattery;
    private android.bluetooth.BluetoothGattCharacteristic rawBatteryCharateristic;
    private Runnable scanHangRunnable;
    private java.util.Map scannedNavdyDials;
    private Runnable sendLocalyticsSuccessRunnable;
    private android.content.SharedPreferences sharedPreferences;
    private Runnable stopScanRunnable;
    private volatile android.bluetooth.BluetoothDevice tempConnectedDial;
    private android.bluetooth.BluetoothGattCharacteristic temperatureCharateristic;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.device.dial.DialManager.class);
        singleton = new com.navdy.hud.app.device.dial.DialManager();
        DIAL_CONNECTED = new com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus(com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status.CONNECTED);
        DIAL_NOT_CONNECTED = new com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus(com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status.DISCONNECTED);
        DIAL_CONNECTING = new com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus(com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status.CONNECTING);
        DIAL_CONNECTION_FAILED = new com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus(com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status.CONNECTION_FAILED);
        DIAL_NOT_FOUND = new com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus(com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus$Status.NO_DIAL_FOUND);
        PowerMap = (java.util.Map)new java.util.HashMap();
        PowerMap.put("low", Integer.valueOf(2));
        PowerMap.put("balanced", Integer.valueOf(0));
        PowerMap.put("high", Integer.valueOf(1));
    }
    
    public DialManager() {
        this.lastKnownBatteryLevel = -1;
        this.bondedDials = new java.util.ArrayList();
        this.scannedNavdyDials = (java.util.Map)new java.util.LinkedHashMap();
        this.ignoredNonNavdyDials = (java.util.Set)new java.util.HashSet();
        this.notificationReasonBattery = com.navdy.hud.app.device.dial.DialConstants$NotificationReason.OK_BATTERY;
        this.scanHangRunnable = (Runnable)new com.navdy.hud.app.device.dial.DialManager$1(this);
        this.bondHangRunnable = (Runnable)new com.navdy.hud.app.device.dial.DialManager$2(this);
        this.sendLocalyticsSuccessRunnable = (Runnable)new com.navdy.hud.app.device.dial.DialManager$3(this);
        this.leScanCallback = new com.navdy.hud.app.device.dial.DialManager$4(this);
        this.stopScanRunnable = (Runnable)new com.navdy.hud.app.device.dial.DialManager$5(this);
        this.bluetoothBondingReceiver = new com.navdy.hud.app.device.dial.DialManager$6(this);
        this.bluetoothOnOffReceiver = new com.navdy.hud.app.device.dial.DialManager$7(this);
        this.connectionErrorRunnable = (Runnable)new com.navdy.hud.app.device.dial.DialManager$8(this);
        this.bluetoothConnectivityReceiver = new com.navdy.hud.app.device.dial.DialManager$9(this);
        this.batteryReadRunnable = (Runnable)new com.navdy.hud.app.device.dial.DialManager$10(this);
        this.deviceInfoGattReadRunnable = (Runnable)new com.navdy.hud.app.device.dial.DialManager$11(this);
        this.hidHostReadyReadRunnable = (Runnable)new com.navdy.hud.app.device.dial.DialManager$12(this);
        this.gattCallback = new com.navdy.hud.app.device.dial.DialManager$13(this);
        sLogger.v("[Dial]initializing...");
        if (com.navdy.hud.app.HudApplication.getApplication() != null) {
            this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
            this.sharedPreferences = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getSharedPreferences();
            this.bluetoothManager = (android.bluetooth.BluetoothManager)com.navdy.hud.app.HudApplication.getAppContext().getSystemService("bluetooth");
            if (this.bluetoothManager != null) {
                this.bluetoothAdapter = this.bluetoothManager.getAdapter();
                if (this.bluetoothAdapter != null) {
                    this.handlerThread = new android.os.HandlerThread("DialHandlerThread");
                    this.handlerThread.start();
                    this.handler = new android.os.Handler(this.handlerThread.getLooper());
                    if (this.bluetoothAdapter.isEnabled()) {
                        sLogger.i("[Dial]Bluetooth is enabled");
                        this.init();
                    } else {
                        sLogger.i("[Dial]Bluetooth is not enabled, should be booting up, wait for the event");
                    }
                    this.startDialEventListener();
                    this.registerReceivers();
                } else {
                    sLogger.e("[Dial]Bluetooth is not available");
                }
            } else {
                sLogger.e("[Dial]Bluetooth manager not available");
            }
        }
    }
    
    static android.bluetooth.BluetoothDevice access$000(com.navdy.hud.app.device.dial.DialManager a) {
        return a.bondingDial;
    }
    
    static android.bluetooth.BluetoothDevice access$002(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0) {
        a.bondingDial = a0;
        return a0;
    }
    
    static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus access$100() {
        return DIAL_NOT_CONNECTED;
    }
    
    static boolean access$1000(com.navdy.hud.app.device.dial.DialManager a) {
        return a.dialManagerInitialized;
    }
    
    static boolean access$1002(com.navdy.hud.app.device.dial.DialManager a, boolean b) {
        a.dialManagerInitialized = b;
        return b;
    }
    
    static void access$1100(com.navdy.hud.app.device.dial.DialManager a) {
        a.init();
    }
    
    static android.bluetooth.BluetoothDevice access$1200(com.navdy.hud.app.device.dial.DialManager a) {
        return a.tempConnectedDial;
    }
    
    static com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus access$1300() {
        return DIAL_CONNECTION_FAILED;
    }
    
    static void access$1400(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0, com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten a1, int i) {
        a.disconectAndRemoveBond(a0, a1, i);
    }
    
    static void access$1500(com.navdy.hud.app.device.dial.DialManager a) {
        a.handleDialConnection();
    }
    
    static String access$1600(com.navdy.hud.app.device.dial.DialManager a) {
        return a.disconnectedDial;
    }
    
    static String access$1602(com.navdy.hud.app.device.dial.DialManager a, String s) {
        a.disconnectedDial = s;
        return s;
    }
    
    static void access$1700(com.navdy.hud.app.device.dial.DialManager a) {
        a.clearConnectedDial();
    }
    
    static void access$1800(com.navdy.hud.app.device.dial.DialManager a) {
        a.stopGATTClient();
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$1900(com.navdy.hud.app.device.dial.DialManager a) {
        return a.batteryCharateristic;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$1902(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothGattCharacteristic a0) {
        a.batteryCharateristic = a0;
        return a0;
    }
    
    static com.squareup.otto.Bus access$200(com.navdy.hud.app.device.dial.DialManager a) {
        return a.bus;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$2000(com.navdy.hud.app.device.dial.DialManager a) {
        return a.rawBatteryCharateristic;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$2002(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothGattCharacteristic a0) {
        a.rawBatteryCharateristic = a0;
        return a0;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$2100(com.navdy.hud.app.device.dial.DialManager a) {
        return a.temperatureCharateristic;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$2102(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothGattCharacteristic a0) {
        a.temperatureCharateristic = a0;
        return a0;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$2200(com.navdy.hud.app.device.dial.DialManager a) {
        return a.firmwareVersionCharacteristic;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$2202(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothGattCharacteristic a0) {
        a.firmwareVersionCharacteristic = a0;
        return a0;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$2300(com.navdy.hud.app.device.dial.DialManager a) {
        return a.hardwareVersionCharacteristic;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$2302(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothGattCharacteristic a0) {
        a.hardwareVersionCharacteristic = a0;
        return a0;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$2400(com.navdy.hud.app.device.dial.DialManager a) {
        return a.hidHostReadyCharacteristic;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$2402(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothGattCharacteristic a0) {
        a.hidHostReadyCharacteristic = a0;
        return a0;
    }
    
    static java.util.Map access$2600() {
        return PowerMap;
    }
    
    static com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor access$2700(com.navdy.hud.app.device.dial.DialManager a) {
        return a.gattCharacteristicProcessor;
    }
    
    static com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor access$2702(com.navdy.hud.app.device.dial.DialManager a, com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor a0) {
        a.gattCharacteristicProcessor = a0;
        return a0;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$2800(com.navdy.hud.app.device.dial.DialManager a) {
        return a.dialForgetKeysCharacteristic;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$2802(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothGattCharacteristic a0) {
        a.dialForgetKeysCharacteristic = a0;
        return a0;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$2900(com.navdy.hud.app.device.dial.DialManager a) {
        return a.dialRebootCharacteristic;
    }
    
    static android.bluetooth.BluetoothGattCharacteristic access$2902(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothGattCharacteristic a0) {
        a.dialRebootCharacteristic = a0;
        return a0;
    }
    
    static android.bluetooth.BluetoothDevice access$300(com.navdy.hud.app.device.dial.DialManager a) {
        return a.connectedDial;
    }
    
    static boolean access$3000(com.navdy.hud.app.device.dial.DialManager a) {
        return a.isGattOn;
    }
    
    static android.bluetooth.BluetoothDevice access$302(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0) {
        a.connectedDial = a0;
        return a0;
    }
    
    static com.navdy.hud.app.device.dial.DialFirmwareUpdater access$3100(com.navdy.hud.app.device.dial.DialManager a) {
        return a.dialFirmwareUpdater;
    }
    
    static com.navdy.hud.app.device.dial.DialFirmwareUpdater access$3102(com.navdy.hud.app.device.dial.DialManager a, com.navdy.hud.app.device.dial.DialFirmwareUpdater a0) {
        a.dialFirmwareUpdater = a0;
        return a0;
    }
    
    static android.content.SharedPreferences access$3200(com.navdy.hud.app.device.dial.DialManager a) {
        return a.sharedPreferences;
    }
    
    static Runnable access$3300(com.navdy.hud.app.device.dial.DialManager a) {
        return a.hidHostReadyReadRunnable;
    }
    
    static Runnable access$3400(com.navdy.hud.app.device.dial.DialManager a) {
        return a.batteryReadRunnable;
    }
    
    static Runnable access$3500(com.navdy.hud.app.device.dial.DialManager a) {
        return a.sendLocalyticsSuccessRunnable;
    }
    
    static Runnable access$3600(com.navdy.hud.app.device.dial.DialManager a) {
        return a.deviceInfoGattReadRunnable;
    }
    
    static void access$3700(com.navdy.hud.app.device.dial.DialManager a, int i) {
        a.processBatteryLevel(i);
    }
    
    static void access$3800(com.navdy.hud.app.device.dial.DialManager a, Integer a0) {
        a.processRawBatteryLevel(a0);
    }
    
    static void access$3900(com.navdy.hud.app.device.dial.DialManager a, Integer a0) {
        a.processSystemTemperature(a0);
    }
    
    static android.os.Handler access$400(com.navdy.hud.app.device.dial.DialManager a) {
        return a.handler;
    }
    
    static String access$4000(com.navdy.hud.app.device.dial.DialManager a) {
        return a.hardwareVersion;
    }
    
    static String access$4002(com.navdy.hud.app.device.dial.DialManager a, String s) {
        a.hardwareVersion = s;
        return s;
    }
    
    static String access$4100(com.navdy.hud.app.device.dial.DialManager a) {
        return a.firmWareVersion;
    }
    
    static String access$4102(com.navdy.hud.app.device.dial.DialManager a, String s) {
        a.firmWareVersion = s;
        return s;
    }
    
    static void access$4200(com.navdy.hud.app.device.dial.DialManager a) {
        a.buildDialList();
    }
    
    static void access$4300(com.navdy.hud.app.device.dial.DialManager a) {
        a.checkDialConnections();
    }
    
    static java.util.ArrayList access$4400(com.navdy.hud.app.device.dial.DialManager a) {
        return a.bondedDials;
    }
    
    static void access$4500(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0) {
        a.removeBond(a0);
    }
    
    static boolean access$4600(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0) {
        return a.isPresentInBondList(a0);
    }
    
    static void access$4700(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0) {
        a.connectEvent(a0);
    }
    
    static void access$4800(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0) {
        a.disconnectEvent(a0);
    }
    
    static void access$4900(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0) {
        a.encryptionFailedEvent(a0);
    }
    
    static void access$500(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0, int i, android.bluetooth.le.ScanRecord a1) {
        a.handleScannedDevice(a0, i, a1);
    }
    
    static void access$5000(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0) {
        a.handleInputReportDescriptor(a0);
    }
    
    static Runnable access$600(com.navdy.hud.app.device.dial.DialManager a) {
        return a.bondHangRunnable;
    }
    
    static boolean access$700(com.navdy.hud.app.device.dial.DialManager a, android.bluetooth.BluetoothDevice a0) {
        return a.addtoBondList(a0);
    }
    
    static android.bluetooth.BluetoothAdapter access$800(com.navdy.hud.app.device.dial.DialManager a) {
        return a.bluetoothAdapter;
    }
    
    static int access$900(com.navdy.hud.app.device.dial.DialManager a) {
        return a.bondTries;
    }
    
    static int access$902(com.navdy.hud.app.device.dial.DialManager a, int i) {
        a.bondTries = i;
        return i;
    }
    
    static int access$908(com.navdy.hud.app.device.dial.DialManager a) {
        int i = a.bondTries;
        a.bondTries = i + 1;
        return i;
    }
    
    private boolean addtoBondList(android.bluetooth.BluetoothDevice a) {
        java.util.ArrayList a0 = null;
        Throwable a1 = null;
        label0: {
            boolean b = false;
            if (a != null) {
                synchronized(this.bondedDials) {
                    java.util.Iterator a2 = this.bondedDials.iterator();
                    boolean b0 = false;
                    Object a3 = a2;
                    while(((java.util.Iterator)a3).hasNext()) {
                        if (((android.bluetooth.BluetoothDevice)((java.util.Iterator)a3).next()).equals(a)) {
                            b0 = true;
                        }
                    }
                    if (b0) {
                        /*monexit(a0)*/;
                        b = false;
                    } else {
                        this.bondedDials.add(a);
                        sLogger.v(new StringBuilder().append("bonded device added to list [").append(a.getName()).append("]").toString());
                        /*monexit(a0)*/;
                        b = true;
                    }
                }
            } else {
                b = false;
            }
            return b;
        }
        while(true) {
            try {
                /*monexit(a0)*/;
            } catch(IllegalMonitorStateException | NullPointerException a5) {
                Throwable a6 = a5;
                a1 = a6;
                continue;
            }
            throw a1;
        }
    }
    
    private void bondWithDial(android.bluetooth.BluetoothDevice a, android.bluetooth.le.ScanRecord a0) {
        this.bondTries = 0;
        this.bondingDial = a;
        sLogger.v(new StringBuilder().append("[Dial]calling createBond [").append(a.getName()).append("] addr[").append(a.getAddress()).append("]").append(" rssi[").append(a0.getTxPowerLevel()).append("] state[").append(com.navdy.hud.app.bluetooth.utils.BluetoothUtils.getBondState(a.getBondState())).append("]").toString());
        boolean b = this.removeFromBondList(a);
        label1: {
            label0: {
                if (b) {
                    break label0;
                }
                if (a.getBondState() == 12) {
                    break label0;
                }
                this.bondTries = this.bondTries + 1;
                this.handler.removeCallbacks(this.bondHangRunnable);
                DIAL_CONNECTING.dialName = a.getName();
                this.bus.post(DIAL_CONNECTING);
                a.createBond();
                this.handler.postDelayed(this.bondHangRunnable, 30000L);
                break label1;
            }
            if (b) {
                sLogger.v(new StringBuilder().append("[Dial]present in bond list:").append(a.getName()).append(" invalid state").toString());
            }
            if (a.getBondState() == 12) {
                sLogger.v(new StringBuilder().append("[Dial]already bonded:").append(a.getName()).append(" invalid state").toString());
            }
            this.bondingDial = null;
            this.disconectAndRemoveBond(a, (com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten)new com.navdy.hud.app.device.dial.DialManager$17(this, a), 2000);
        }
    }
    
    private void buildDialList() {
        label0: try {
            java.util.ArrayList a = null;
            Throwable a0 = null;
            sLogger.v("[Dial]trying to find paired dials");
            java.util.Set a1 = this.bluetoothAdapter.getBondedDevices();
            sLogger.v(new StringBuilder().append("[Dial]Bonded devices:").append((a1 != null) ? a1.size() : 0).toString());
            java.util.ArrayList a2 = new java.util.ArrayList(8);
            Object a3 = a1.iterator();
            while(((java.util.Iterator)a3).hasNext()) {
                android.bluetooth.BluetoothDevice a4 = (android.bluetooth.BluetoothDevice)((java.util.Iterator)a3).next();
                sLogger.v(new StringBuilder().append("[Dial]Bonded Device Address[").append(a4.getAddress()).append("] name[").append(a4.getName()).append(" type[").append(com.navdy.hud.app.bluetooth.utils.BluetoothUtils.getDeviceType(a4.getType())).append("]").toString());
                if (com.navdy.hud.app.device.dial.DialManager.isDialDeviceName(a4.getName())) {
                    sLogger.v(new StringBuilder().append("[Dial]found paired dial Address[").append(a4).append("] name[").append(a4.getName()).append("]").toString());
                    a2.add(a4);
                }
            }
            label1: if (a2.size() != 0) {
                sLogger.v(new StringBuilder().append("[Dial]found paired dials:").append(a2.size()).toString());
                synchronized(this.bondedDials) {
                    this.bondedDials.clear();
                    this.bondedDials.addAll((java.util.Collection)a2);
                    /*monexit(a)*/;
                }
                break label0;
            } else {
                sLogger.v("[Dial]no dial device found");
                break label0;
            }
            while(true) {
                try {
                    /*monexit(a)*/;
                } catch(IllegalMonitorStateException | NullPointerException a6) {
                    Throwable a7 = a6;
                    a0 = a7;
                    continue;
                }
                throw a0;
            }
        } catch(Throwable a8) {
            sLogger.e("[Dial]", a8);
        }
    }
    
    private void checkDialConnections() {
        try {
            synchronized(this.bondedDials) {
                Object a0 = this.bondedDials.iterator();
                while(((java.util.Iterator)a0).hasNext()) {
                    android.bluetooth.BluetoothDevice a1 = (android.bluetooth.BluetoothDevice)((java.util.Iterator)a0).next();
                    com.navdy.hud.app.device.dial.DialManagerHelper.isDialConnected(this.bluetoothAdapter, a1, (com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnectionStatus)new com.navdy.hud.app.device.dial.DialManager$16(this, a1));
                }
                /*monexit(a)*/;
            }
        } catch(Throwable a6) {
            sLogger.e("[Dial]", a6);
        }
    }
    
    private void clearConnectedDial() {
        this.firmWareVersion = null;
        this.hardwareVersion = null;
        this.lastKnownBatteryLevel = -1;
        this.lastKnownRawBatteryLevel = null;
        this.lastKnownSystemTemperature = null;
        this.connectedDial = null;
    }
    
    private void connectEvent(android.bluetooth.BluetoothDevice a) {
        sLogger.v(new StringBuilder().append("[DialEvent-connect] ").append(a).toString());
        this.handler.removeCallbacks(this.bondHangRunnable);
        this.handler.removeCallbacks(this.connectionErrorRunnable);
        if (this.connectedDial == null) {
            if (this.isPresentInBondList(a)) {
                sLogger.v("[DialEvent-connect] not on dial pairing screen, mark as connected");
                this.connectedDial = a;
                this.handleDialConnection();
            } else {
                sLogger.v("launch connectionErrorRunnable");
                this.tempConnectedDial = a;
                this.handler.postDelayed(this.connectionErrorRunnable, 5000L);
            }
        } else {
            sLogger.v(new StringBuilder().append("[DialEvent-connect] ignore already connected dial:").append(this.connectedDial).toString());
        }
    }
    
    private void disconectAndRemoveBond(android.bluetooth.BluetoothDevice a, com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten a0, int i) {
        com.navdy.hud.app.device.dial.DialManagerHelper.disconnectFromDial(this.bluetoothAdapter, a, (com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection)new com.navdy.hud.app.device.dial.DialManager$18(this, a, i, a0));
    }
    
    private void disconnectEvent(android.bluetooth.BluetoothDevice a) {
        this.handler.removeCallbacks(this.bondHangRunnable);
        this.handler.removeCallbacks(this.connectionErrorRunnable);
        this.tempConnectedDial = null;
        if (this.connectedDial != null) {
            if (this.connectedDial.equals(a)) {
                this.disconnectedDial = this.connectedDial.getName();
                this.clearConnectedDial();
                this.stopGATTClient();
                DIAL_NOT_CONNECTED.dialName = this.disconnectedDial;
                this.bus.post(DIAL_NOT_CONNECTED);
                sLogger.v("[DialEvent-disconnect] dial marked not connected");
            } else {
                sLogger.v(new StringBuilder().append("[DialEvent-disconnect] notif not for connected dial connected[").append(this.connectedDial).append("]").toString());
            }
        } else {
            sLogger.v("[DialEvent-disconnect] no connected dial");
        }
    }
    
    private void encryptionFailedEvent(android.bluetooth.BluetoothDevice a) {
        if (this.isPresentInBondList(a)) {
            if (this.encryptionRetryCount >= 2) {
                this.removeFromBondList(a);
                sLogger.e("[DialEvent-encryptfail] device found in bonded dial list, stop gatt");
                this.stopGATTClient();
                sLogger.e("[DialEvent-encryptfail] sleeping");
                com.navdy.hud.app.util.GenericUtil.sleep(2000);
                sLogger.e("[DialEvent-encryptfail] sleep");
                com.navdy.hud.app.device.dial.DialManagerHelper.disconnectFromDial(this.bluetoothAdapter, a, (com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection)new com.navdy.hud.app.device.dial.DialManager$23(this, a));
            } else {
                this.encryptionRetryCount = this.encryptionRetryCount + 1;
                sLogger.e(new StringBuilder().append("[DialEvent-encryptfail] Attempt ").append(this.encryptionRetryCount).append(" will retry.").toString());
                this.stopGATTClient();
                com.navdy.hud.app.util.GenericUtil.sleep(2000);
                com.navdy.hud.app.device.dial.DialManagerHelper.disconnectFromDial(this.bluetoothAdapter, a, (com.navdy.hud.app.device.dial.DialManagerHelper$IDialConnection)new com.navdy.hud.app.device.dial.DialManager$22(this));
            }
        } else {
            this.encryptionRetryCount = 0;
            sLogger.e("[DialEventsListener] device not in bonded dial list");
        }
    }
    
    public static com.navdy.hud.app.device.dial.DialConstants$NotificationReason getBatteryLevelCategory(int i) {
        com.navdy.hud.app.device.dial.DialConstants$NotificationReason a = null;
        label1: {
            label2: {
                if (i <= 50) {
                    break label2;
                }
                if (i > 80) {
                    break label2;
                }
                a = com.navdy.hud.app.device.dial.DialConstants$NotificationReason.LOW_BATTERY;
                break label1;
            }
            label0: {
                if (i <= 10) {
                    break label0;
                }
                if (i > 50) {
                    break label0;
                }
                a = com.navdy.hud.app.device.dial.DialConstants$NotificationReason.VERY_LOW_BATTERY;
                break label1;
            }
            a = (i > 10) ? com.navdy.hud.app.device.dial.DialConstants$NotificationReason.OK_BATTERY : com.navdy.hud.app.device.dial.DialConstants$NotificationReason.EXTREMELY_LOW_BATTERY;
        }
        return a;
    }
    
    private android.bluetooth.BluetoothDevice getBluetoothDevice(String s) {
        android.bluetooth.BluetoothDevice a = null;
        synchronized(this.bondedDials) {
            Object a1 = this.bondedDials.iterator();
            while(true) {
                if (((java.util.Iterator)a1).hasNext()) {
                    a = (android.bluetooth.BluetoothDevice)((java.util.Iterator)a1).next();
                    if (!android.text.TextUtils.equals((CharSequence)a.getName(), (CharSequence)s)) {
                        continue;
                    }
                    /*monexit(a0)*/;
                    break;
                } else {
                    /*monexit(a0)*/;
                    a = null;
                    break;
                }
            }
        }
        return a;
    }
    
    public static int getDisplayBatteryLevel(int i) {
        return (int)((float)i * 0.1f);
    }
    
    public static com.navdy.hud.app.device.dial.DialManager getInstance() {
        return singleton;
    }
    
    private void handleDialConnection() {
        this.encryptionRetryCount = 0;
        com.navdy.hud.app.framework.toast.ToastManager a = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
        com.navdy.hud.app.device.dial.DialNotification.dismissAllBatteryToasts();
        a.dismissCurrentToast("dial-disconnect");
        a.clearPendingToast("dial-disconnect");
        DIAL_CONNECTED.dialName = this.getDialName();
        this.bus.post(DIAL_CONNECTED);
        this.startGATTClient();
    }
    
    private void handleInputReportDescriptor(android.bluetooth.BluetoothDevice a) {
        this.handler.removeCallbacks(this.connectionErrorRunnable);
        sLogger.v(new StringBuilder().append("[DialEvent-handleInputReportDescriptor] ").append(a).toString());
        if (this.tempConnectedDial != null) {
            if (this.connectedDial == null) {
                sLogger.v("[DialEvent-handleInputReportDescriptor] done");
                this.tempConnectedDial = null;
                this.connectedDial = a;
                this.handleDialConnection();
            } else {
                sLogger.v(new StringBuilder().append("[DialEvent-handleInputReportDescriptor] dial already connected[").append(this.connectedDial).append("]").toString());
                if (this.connectedDial.equals(a)) {
                    DIAL_CONNECTED.dialName = this.getDialName();
                    this.bus.post(DIAL_CONNECTED);
                }
            }
        } else {
            sLogger.v("[DialEvent-handleInputReportDescriptor] no temp connected dial");
        }
    }
    
    private void handleScannedDevice(android.bluetooth.BluetoothDevice a, int i, android.bluetooth.le.ScanRecord a0) {
        Throwable a1 = null;
        Object a2 = null;
        int i0 = a.getType();
        label0: {
            Throwable a3 = null;
            Object a4 = null;
            label1: {
                label4: if (i0 == 2) {
                    if (this.isScanning) {
                    label3: synchronized(this.scannedNavdyDials) {
                            try {
                                boolean b = this.scannedNavdyDials.containsKey(a);
                                label2: {
                                    if (b) {
                                        break label2;
                                    }
                                    /*monexit(a5)*/;
                                    break label3;
                                }
                                /*monexit(a5)*/;
                            } catch(Throwable a6) {
                                a3 = a6;
                                a4 = a5;
                                break label1;
                            }
                            break label4;
                        }
                        String s = a.getName();
                        if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                            if (this.isDialDevice(a)) {
                                java.util.List a7 = a0.getServiceUuids();
                                if (a7 != null) {
                                    boolean b0 = false;
                                    Object a8 = a7.iterator();
                                    while(true) {
                                        if (((java.util.Iterator)a8).hasNext()) {
                                            if (!((android.os.ParcelUuid)((java.util.Iterator)a8).next()).getUuid().equals(com.navdy.hud.app.device.dial.DialConstants.HID_SERVICE_UUID)) {
                                                continue;
                                            }
                                            b0 = true;
                                            break;
                                        } else {
                                            b0 = false;
                                            break;
                                        }
                                    }
                                    if (b0) {
                                        com.navdy.hud.app.device.dial.DialConstants$PairingRule a9 = null;
                                        com.navdy.hud.app.device.dial.DialConstants$PairingRule a10 = null;
                                        sLogger.v(new StringBuilder().append("[Dial]Scanned name[").append(a.getName()).append("] mac[").append(a.getAddress()).append("] bonded[").append(com.navdy.hud.app.bluetooth.utils.BluetoothUtils.getBondState(a.getBondState())).append("] rssi[").append(i).append("]").append(" type[").append(com.navdy.hud.app.bluetooth.utils.BluetoothUtils.getDeviceType(a.getType())).append("]").toString());
                                        synchronized(this.scannedNavdyDials) {
                                            this.scannedNavdyDials.put(a, a0);
                                            /*monexit(a11)*/;
                                            a9 = com.navdy.hud.app.device.dial.DialConstants.PAIRING_RULE;
                                            a10 = com.navdy.hud.app.device.dial.DialConstants$PairingRule.FIRST;
                                        }
                                        if (a9 == a10) {
                                            sLogger.v("[Dial]pairing rule is first, stopping scan...");
                                            this.handler.removeCallbacks(this.stopScanRunnable);
                                            this.handler.post(this.stopScanRunnable);
                                        }
                                    } else if (!this.ignoredNonNavdyDials.contains(a)) {
                                        sLogger.v(new StringBuilder().append("[Dial]HID service not found ignoring [").append(s).append("]").toString());
                                        this.ignoredNonNavdyDials.add(a);
                                    }
                                } else {
                                    sLogger.v(new StringBuilder().append("[Dial]no service uuid found [").append(s).append("]").toString());
                                }
                            } else if (!this.ignoredNonNavdyDials.contains(a)) {
                                sLogger.v(new StringBuilder().append("[Dial]ignoring [").append(s).append("]").toString());
                                this.ignoredNonNavdyDials.add(a);
                            }
                        }
                    } else {
                        sLogger.i(new StringBuilder().append("[Dial]not scanning anymore [").append(a.getName()).append("]").toString());
                    }
                }
                return;
            }
            while(true) {
                try {
                    /*monexit(a4)*/;
                } catch(IllegalMonitorStateException | NullPointerException a13) {
                    Throwable a14 = a13;
                    Object a15 = a4;
                    a3 = a14;
                    a4 = a15;
                    continue;
                }
                throw a3;
            }
        }
        while(true) {
            try {
                /*monexit(a2)*/;
            } catch(IllegalMonitorStateException | NullPointerException a16) {
                Throwable a17 = a16;
                Object a18 = a2;
                a1 = a17;
                a2 = a18;
                continue;
            }
            throw a1;
        }
    }
    
    private void init() {
        synchronized(this) {
            String s = this.bluetoothAdapter.getAddress();
            sLogger.v(new StringBuilder().append("[Dial]Client Address:").append(s).append(" name:").append(this.bluetoothAdapter.getName()).toString());
            this.bluetoothLEScanner = this.bluetoothAdapter.getBluetoothLeScanner();
            sLogger.v(new StringBuilder().append("[Dial]btle scanner:").append(this.bluetoothLEScanner).toString());
            this.handler.post((Runnable)new com.navdy.hud.app.device.dial.DialManager$14(this));
            this.handler.post((Runnable)new com.navdy.hud.app.device.dial.DialManager$15(this));
        }
        /*monexit(this)*/;
    }
    
    public static boolean isDialDeviceName(String s) {
        boolean b = false;
        if (s != null) {
            int i = 0;
            while(true) {
                if (i >= com.navdy.hud.app.device.dial.DialConstants.NAVDY_DIAL_FILTER.length) {
                    b = false;
                    break;
                } else {
                    if (!s.contains((CharSequence)com.navdy.hud.app.device.dial.DialConstants.NAVDY_DIAL_FILTER[i])) {
                        i = i + 1;
                        continue;
                    }
                    b = true;
                    break;
                }
            }
        } else {
            b = false;
        }
        return b;
    }
    
    private boolean isPresentInBondList(android.bluetooth.BluetoothDevice a) {
        java.util.ArrayList a0 = null;
        Throwable a1 = null;
        label0: {
            boolean b = false;
            if (a != null) {
                synchronized(this.bondedDials) {
                    Object a2 = this.bondedDials.iterator();
                    while(true) {
                        if (((java.util.Iterator)a2).hasNext()) {
                            if (!((android.bluetooth.BluetoothDevice)((java.util.Iterator)a2).next()).equals(a)) {
                                continue;
                            }
                            /*monexit(a0)*/;
                            b = true;
                            break;
                        } else {
                            /*monexit(a0)*/;
                            b = false;
                            break;
                        }
                    }
                }
            } else {
                b = false;
            }
            return b;
        }
        while(true) {
            try {
                /*monexit(a0)*/;
            } catch(IllegalMonitorStateException | NullPointerException a4) {
                Throwable a5 = a4;
                a1 = a5;
                continue;
            }
            throw a1;
        }
    }
    
    private void processBatteryLevel(int i) {
        com.navdy.hud.app.device.dial.DialConstants$NotificationReason a = com.navdy.hud.app.device.dial.DialManager.getBatteryLevelCategory(i);
        this.lastKnownBatteryLevel = i;
        sLogger.v(new StringBuilder().append("battery status current[").append(a).append("] previous[").append(this.notificationReasonBattery).append("]").toString());
        if (a != com.navdy.hud.app.device.dial.DialConstants$NotificationReason.OK_BATTERY) {
            if (this.notificationReasonBattery != a) {
                switch(com.navdy.hud.app.device.dial.DialManager$24.$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason[a.ordinal()]) {
                    case 3: {
                        if (this.notificationReasonBattery != com.navdy.hud.app.device.dial.DialConstants$NotificationReason.EXTREMELY_LOW_BATTERY) {
                            sLogger.v("battery notification change");
                            this.notificationReasonBattery = a;
                            break;
                        } else {
                            sLogger.v("battery threshold change-ignore");
                            break;
                        }
                    }
                    case 2: {
                        com.navdy.hud.app.device.dial.DialConstants$NotificationReason a0 = this.notificationReasonBattery;
                        com.navdy.hud.app.device.dial.DialConstants$NotificationReason a1 = com.navdy.hud.app.device.dial.DialConstants$NotificationReason.EXTREMELY_LOW_BATTERY;
                        label0: {
                            label1: {
                                if (a0 == a1) {
                                    break label1;
                                }
                                if (this.notificationReasonBattery != com.navdy.hud.app.device.dial.DialConstants$NotificationReason.VERY_LOW_BATTERY) {
                                    break label0;
                                }
                            }
                            sLogger.v("battery threshold change-ignore");
                            break;
                        }
                        sLogger.v("battery notification change");
                        this.notificationReasonBattery = a;
                        break;
                    }
                    case 1: {
                        sLogger.v("battery notification change");
                        this.notificationReasonBattery = a;
                        break;
                    }
                }
            } else {
                sLogger.v("battery no change");
            }
        } else {
            if (this.notificationReasonBattery != null && this.notificationReasonBattery != com.navdy.hud.app.device.dial.DialConstants$NotificationReason.OK_BATTERY) {
                this.notificationReasonBattery = null;
            }
            com.navdy.hud.app.device.dial.DialNotification.dismissAllBatteryToasts();
        }
    }
    
    private void processRawBatteryLevel(Integer a) {
        this.lastKnownRawBatteryLevel = a;
    }
    
    private void processSystemTemperature(Integer a) {
        this.lastKnownSystemTemperature = a;
    }
    
    private void registerReceivers() {
        android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
        android.content.IntentFilter a0 = new android.content.IntentFilter("android.bluetooth.device.action.BOND_STATE_CHANGED");
        a.registerReceiver(this.bluetoothBondingReceiver, a0);
        android.content.IntentFilter a1 = new android.content.IntentFilter();
        a1.addAction("android.bluetooth.device.action.ACL_CONNECTED");
        a1.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
        a1.addAction("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED");
        a.registerReceiver(this.bluetoothConnectivityReceiver, a1);
        android.content.IntentFilter a2 = new android.content.IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED");
        a.registerReceiver(this.bluetoothOnOffReceiver, a2);
    }
    
    private void removeBond(android.bluetooth.BluetoothDevice a) {
        try {
            java.lang.reflect.Method a0 = (a).getClass().getMethod("removeBond", (Class[])null);
            if (a0 == null) {
                sLogger.e("[Dial]cannot get to removeBond api");
            } else {
                sLogger.e(new StringBuilder().append("[Dial]removingBond [").append(a.getName()).append("]").toString());
                a0.invoke(a, (Object[])null);
                sLogger.e("[Dial]removedBond");
            }
        } catch(Throwable a1) {
            sLogger.e("[Dial]", a1);
        }
    }
    
    private boolean removeFromBondList(android.bluetooth.BluetoothDevice a) {
        java.util.ArrayList a0 = null;
        Throwable a1 = null;
        label0: {
            boolean b = false;
            if (a != null) {
                synchronized(this.bondedDials) {
                    java.util.Iterator a2 = this.bondedDials.iterator();
                    boolean b0 = false;
                    Object a3 = a2;
                    while(((java.util.Iterator)a3).hasNext()) {
                        if (((android.bluetooth.BluetoothDevice)((java.util.Iterator)a3).next()).equals(a)) {
                            b0 = true;
                        }
                    }
                    if (b0) {
                        this.bondedDials.remove(a);
                        /*monexit(a0)*/;
                        b = true;
                    } else {
                        /*monexit(a0)*/;
                        b = false;
                    }
                }
            } else {
                b = false;
            }
            return b;
        }
        while(true) {
            try {
                /*monexit(a0)*/;
            } catch(IllegalMonitorStateException | NullPointerException a5) {
                Throwable a6 = a5;
                a1 = a6;
                continue;
            }
            throw a1;
        }
    }
    
    private void startDialEventListener() {
        synchronized(this) {
            if (this.dialEventsListenerThread == null) {
                this.dialEventsListenerThread = new Thread((Runnable)new com.navdy.hud.app.device.dial.DialManager$21(this));
                this.dialEventsListenerThread.setName("dialEventListener");
                this.dialEventsListenerThread.start();
                sLogger.v("dialEventListener thread started");
            }
        }
        /*monexit(this)*/;
    }
    
    private void startGATTClient() {
        synchronized(this) {
            label0: try {
                if (this.bluetoothGatt != null) {
                    break label1;
                }
                sLogger.v("[Dial]startGATTClient, launched gatt");
                this.isGattOn = true;
                this.bluetoothGatt = this.connectedDial.connectGatt(com.navdy.hud.app.HudApplication.getAppContext(), true, this.gattCallback);
                break label1;
            } catch(Throwable a0) {
                try {
                    sLogger.e("[Dial]", a0);
                    this.isGattOn = false;
                } catch(Throwable a1) {
                    a = a1;
                    break label0;
                }
                break label1;
            }
            /*monexit(this)*/;
            throw a;
        }
        /*monexit(this)*/;
    }
    
    private void stopGATTClient() {
        label3: synchronized(this) {
            Throwable a = null;
            label0: {
                label2: {
                    label1: try {
                        if (this.bluetoothGatt == null) {
                            break label1;
                        }
                        this.handler.removeCallbacks(this.batteryReadRunnable);
                        this.bluetoothGatt.close();
                        sLogger.v("[Dial]stopGATTClient, gatt closed");
                        this.dialFirmwareUpdater.cancelUpdate();
                        break label2;
                    } catch(Throwable a0) {
                        a = a0;
                        break label0;
                    }
                    this.bluetoothGatt = null;
                    this.isGattOn = false;
                    break label3;
                }
                this.bluetoothGatt = null;
                this.isGattOn = false;
                break label3;
            }
            try {
                sLogger.e("[Dial]gatt closed", a);
            } catch(Throwable a1) {
                this.bluetoothGatt = null;
                this.isGattOn = false;
                throw a1;
            }
            this.bluetoothGatt = null;
            this.isGattOn = false;
        }
        /*monexit(this)*/;
    }
    
    public void forgetAllDials(com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten a) {
        this.requestDialForgetKeys();
        this.handler.removeCallbacks(this.bondHangRunnable);
        synchronized(this.bondedDials) {
            com.navdy.service.library.log.Logger a1 = sLogger;
            a1.v(new StringBuilder().append("forget all dials:").append(this.bondedDials.size()).toString());
            java.util.ArrayList a2 = new java.util.ArrayList((java.util.Collection)this.bondedDials);
            this.bondedDials.clear();
            this.clearConnectedDial();
            java.util.concurrent.atomic.AtomicInteger a3 = new java.util.concurrent.atomic.AtomicInteger(a2.size());
            java.util.Iterator a4 = a2.iterator();
            Object a5 = a;
            Object a6 = a4;
            while(((java.util.Iterator)a6).hasNext()) {
                this.disconectAndRemoveBond((android.bluetooth.BluetoothDevice)((java.util.Iterator)a6).next(), (com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten)new com.navdy.hud.app.device.dial.DialManager$20(this, a3, (com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten)a5), 2000);
            }
            /*monexit(a0)*/;
        }
    }
    
    public void forgetAllDials(boolean b, com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten a) {
        int i = this.getBondedDialCount();
        String s = this.getBondedDialList();
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.device.dial.DialManager$19(this, a), 1);
        if (b) {
            com.navdy.hud.app.framework.toast.ToastManager a0 = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
            a0.dismissCurrentToast();
            a0.clearAllPendingToast();
            a0.disableToasts(false);
            com.navdy.hud.app.device.dial.DialNotification.showForgottenToast(i > 1, s);
        }
    }
    
    public void forgetDial(android.bluetooth.BluetoothDevice a) {
        this.removeBond(a);
        this.removeFromBondList(a);
    }
    
    public com.navdy.hud.app.device.dial.DialConstants$NotificationReason getBatteryNotificationReason() {
        return this.notificationReasonBattery;
    }
    
    public int getBondedDialCount() {
        int i = 0;
        synchronized(this.bondedDials) {
            i = this.bondedDials.size();
            /*monexit(a)*/;
        }
        return i;
    }
    
    public String getBondedDialList() {
        StringBuilder a = new StringBuilder();
        synchronized(this.bondedDials) {
            int i = this.bondedDials.size();
            int i0 = 0;
            while(i0 < i) {
                a.append(((android.bluetooth.BluetoothDevice)this.bondedDials.get(i0)).getName());
                if (i0 + 1 < i) {
                    a.append(", ");
                }
                i0 = i0 + 1;
            }
            /*monexit(a0)*/;
        }
        return a.toString();
    }
    
    android.bluetooth.BluetoothDevice getDialDevice() {
        return this.connectedDial;
    }
    
    public com.navdy.hud.app.device.dial.DialFirmwareUpdater getDialFirmwareUpdater() {
        return this.dialFirmwareUpdater;
    }
    
    public String getDialName() {
        String s = (this.connectedDial == null) ? null : this.connectedDial.getName();
        return s;
    }
    
    public String getFirmWareVersion() {
        return this.firmWareVersion;
    }
    
    public String getHardwareVersion() {
        return this.hardwareVersion;
    }
    
    public int getLastKnownBatteryLevel() {
        return this.lastKnownBatteryLevel;
    }
    
    public Integer getLastKnownRawBatteryLevel() {
        return this.lastKnownRawBatteryLevel;
    }
    
    public Integer getLastKnownSystemTemperature() {
        return this.lastKnownSystemTemperature;
    }
    
    public boolean isDialConnected() {
        return this.connectedDial != null;
    }
    
    public boolean isDialDevice(android.bluetooth.BluetoothDevice a) {
        boolean b = false;
        if (a != null) {
            String s = a.getName();
            b = s != null && com.navdy.hud.app.device.dial.DialManager.isDialDeviceName(s);
        } else {
            b = false;
        }
        return b;
    }
    
    public boolean isDialPaired() {
        return this.getBondedDialCount() > 0;
    }
    
    public boolean isInitialized() {
        return this.dialManagerInitialized;
    }
    
    public boolean isScanning() {
        return this.isScanning;
    }
    
    public void queueRead(android.bluetooth.BluetoothGattCharacteristic a) {
        if (this.gattCharacteristicProcessor != null) {
            this.gattCharacteristicProcessor.process((com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor$Command)new com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor$ReadCommand(a));
        } else {
            sLogger.w("gattCharacteristicProcessor not initialized");
        }
    }
    
    public void queueWrite(android.bluetooth.BluetoothGattCharacteristic a, byte[] a0) {
        if (this.gattCharacteristicProcessor != null) {
            this.gattCharacteristicProcessor.process((com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor$Command)new com.navdy.hud.app.device.dial.DialManager$CharacteristicCommandProcessor$WriteCommand(a, a0));
        } else {
            sLogger.w("gattCharacteristicProcessor not initialized");
        }
    }
    
    public boolean reportInputEvent(android.view.KeyEvent a) {
        boolean b = false;
        android.view.InputDevice a0 = a.getDevice();
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (a0.isVirtual()) {
                        break label1;
                    }
                    if (com.navdy.hud.app.device.dial.DialManager.isDialDeviceName(a0.getName())) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            if (this.isDialConnected()) {
                com.navdy.hud.app.screen.BaseScreen a1 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getCurrentScreen();
                if (a1 != null && a1.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_DIAL_PAIRING) {
                    sLogger.v("reportInputEvent: sent connected event");
                    DIAL_CONNECTED.dialName = this.getDialName();
                    this.bus.post(DIAL_CONNECTED);
                }
            } else {
                sLogger.v(new StringBuilder().append("reportInputEvent:got dial connection via input:").append(a0.getName()).toString());
                this.connectedDial = this.getBluetoothDevice(a0.getName());
                this.handler.removeCallbacks(this.connectionErrorRunnable);
                this.handler.removeCallbacks(this.bondHangRunnable);
                sLogger.v("reportInputEvent remove connectionErrorRunnable");
                if (this.connectedDial == null) {
                    sLogger.i("reportInputEvent: did not get device from bonded list");
                    this.buildDialList();
                    this.connectedDial = this.getBluetoothDevice(a0.getName());
                    if (this.connectedDial == null) {
                        sLogger.v("reportInputEvent: did not get device from new bonded list");
                    } else {
                        sLogger.v("reportInputEvent: got device from new bonded list");
                        this.handleDialConnection();
                    }
                } else {
                    sLogger.v("reportInputEvent: got device from bonded list");
                    this.handleDialConnection();
                }
            }
            b = true;
        }
        return b;
    }
    
    public void requestDialForgetKeys() {
        try {
            if (this.dialForgetKeysCharacteristic == null) {
                sLogger.v("Not queuing dial forget keys request: null");
            } else {
                sLogger.v("Queueing dial forget keys request");
                this.queueWrite(this.dialForgetKeysCharacteristic, com.navdy.hud.app.device.dial.DialConstants.DIAL_FORGET_KEYS_MAGIC);
            }
        } catch(Throwable a) {
            sLogger.e("[Dial]", a);
        }
    }
    
    public void requestDialReboot(boolean b) {
        label1: {
            label0: {
                if (b) {
                    break label0;
                }
                if (System.currentTimeMillis() - this.sharedPreferences.getLong("last_dial_reboot", 0L) >= com.navdy.hud.app.device.dial.DialConstants.MIN_TIME_BETWEEN_REBOOTS) {
                    break label0;
                }
                sLogger.v("Not rebooting dial, last reboot too recent.");
                break label1;
            }
            try {
                if (this.dialRebootCharacteristic == null) {
                    sLogger.v("Not queuing dial reboot request: null");
                } else {
                    sLogger.v("Queueing dial reboot request");
                    this.queueWrite(this.dialRebootCharacteristic, com.navdy.hud.app.device.dial.DialConstants.DIAL_REBOOT_MAGIC);
                }
            } catch(Throwable a) {
                sLogger.e("[Dial]", a);
            }
        }
    }
    
    public void startScan() {
        label1: try {
            Throwable a = null;
            Object a0 = null;
            label0: if (this.isScanning) {
                sLogger.v("scan already running");
                break label1;
            } else {
                this.handler.removeCallbacks(this.scanHangRunnable);
                this.bondingDial = null;
                synchronized(this.scannedNavdyDials) {
                    this.scannedNavdyDials.clear();
                    this.ignoredNonNavdyDials.clear();
                    /*monexit(a1)*/;
                }
                this.bluetoothLEScanner.startScan(this.leScanCallback);
                this.handler.postDelayed(this.scanHangRunnable, 30000L);
                this.isScanning = true;
                break label1;
            }
            while(true) {
                try {
                    /*monexit(a0)*/;
                } catch(IllegalMonitorStateException | NullPointerException a3) {
                    Throwable a4 = a3;
                    Object a5 = a0;
                    a = a4;
                    a0 = a5;
                    continue;
                }
                throw a;
            }
        } catch(Throwable a6) {
            sLogger.e(a6);
        }
    }
    
    public void stopScan(boolean b) {
        Throwable a = null;
        Object a0 = null;
        boolean b0 = this.isScanning;
        label0: {
            label3: if (b0) {
                android.bluetooth.BluetoothDevice a1 = null;
                android.bluetooth.le.ScanRecord a2 = null;
                sLogger.v("[Dial]stopping btle scan...");
                this.handler.removeCallbacks(this.scanHangRunnable);
                this.isScanning = false;
                try {
                    this.bluetoothLEScanner.stopScan(this.leScanCallback);
                } catch(Throwable a3) {
                    sLogger.e(a3);
                }
                label2: synchronized(this.scannedNavdyDials) {
                    this.ignoredNonNavdyDials.clear();
                    sLogger.v(new StringBuilder().append("[Dial]stopscan dials found:").append(this.scannedNavdyDials.size()).toString());
                    int i = this.scannedNavdyDials.size();
                    label1: {
                        if (i == 0) {
                            break label1;
                        }
                        Object a5 = this.scannedNavdyDials.entrySet().iterator().next();
                        a1 = (android.bluetooth.BluetoothDevice)((java.util.Map.Entry)a5).getKey();
                        a2 = (android.bluetooth.le.ScanRecord)((java.util.Map.Entry)a5).getValue();
                        this.scannedNavdyDials.clear();
                        /*monexit(a4)*/;
                        break label2;
                    }
                    sLogger.i("[Dial]No dials found");
                    if (b) {
                        this.bus.post(DIAL_NOT_FOUND);
                    }
                    /*monexit(a4)*/;
                    break label3;
                }
                this.bondWithDial(a1, a2);
            } else {
                sLogger.w("[Dial]not currently scanning");
            }
            return;
        }
        while(true) {
            try {
                /*monexit(a0)*/;
            } catch(IllegalMonitorStateException | NullPointerException a7) {
                Throwable a8 = a7;
                Object a9 = a0;
                a = a8;
                a0 = a9;
                continue;
            }
            throw a;
        }
    }
}
