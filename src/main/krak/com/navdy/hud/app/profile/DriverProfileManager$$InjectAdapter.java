package com.navdy.hud.app.profile;

final public class DriverProfileManager$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding pathManager;
    private dagger.internal.Binding timeHelper;
    
    public DriverProfileManager$$InjectAdapter() {
        super("com.navdy.hud.app.profile.DriverProfileManager", "members/com.navdy.hud.app.profile.DriverProfileManager", false, com.navdy.hud.app.profile.DriverProfileManager.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.profile.DriverProfileManager.class, (this).getClass().getClassLoader());
        this.pathManager = a.requestBinding("com.navdy.hud.app.storage.PathManager", com.navdy.hud.app.profile.DriverProfileManager.class, (this).getClass().getClassLoader());
        this.timeHelper = a.requestBinding("com.navdy.hud.app.common.TimeHelper", com.navdy.hud.app.profile.DriverProfileManager.class, (this).getClass().getClassLoader());
    }
    
    public com.navdy.hud.app.profile.DriverProfileManager get() {
        return new com.navdy.hud.app.profile.DriverProfileManager((com.squareup.otto.Bus)this.bus.get(), (com.navdy.hud.app.storage.PathManager)this.pathManager.get(), (com.navdy.hud.app.common.TimeHelper)this.timeHelper.get());
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a.add(this.bus);
        a.add(this.pathManager);
        a.add(this.timeHelper);
    }
}
