package com.navdy.hud.app.gesture;

abstract public interface MultipleClickGestureDetector$IMultipleClickKeyGesture extends com.navdy.hud.app.manager.InputManager$IInputHandler {
    abstract public void onMultipleClick(int arg);
}
