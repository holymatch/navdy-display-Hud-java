package com.navdy.hud.app.gesture;

abstract public interface GestureDetector$GestureListener {
    abstract public void onClick();
    
    
    abstract public void onTrackHand(float arg);
}
