package com.navdy.hud.app.gesture;


    public enum GestureServiceConnector$Error {
        CANNOT_CONNECT(0),
    COMMUNICATION_LOST(1),
    SWIPED_RESTARTED(2);

        private int value;
        GestureServiceConnector$Error(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class GestureServiceConnector$Error extends Enum {
//    final private static com.navdy.hud.app.gesture.GestureServiceConnector$Error[] $VALUES;
//    final public static com.navdy.hud.app.gesture.GestureServiceConnector$Error CANNOT_CONNECT;
//    final public static com.navdy.hud.app.gesture.GestureServiceConnector$Error COMMUNICATION_LOST;
//    final public static com.navdy.hud.app.gesture.GestureServiceConnector$Error SWIPED_RESTARTED;
//    
//    static {
//        CANNOT_CONNECT = new com.navdy.hud.app.gesture.GestureServiceConnector$Error("CANNOT_CONNECT", 0);
//        COMMUNICATION_LOST = new com.navdy.hud.app.gesture.GestureServiceConnector$Error("COMMUNICATION_LOST", 1);
//        SWIPED_RESTARTED = new com.navdy.hud.app.gesture.GestureServiceConnector$Error("SWIPED_RESTARTED", 2);
//        com.navdy.hud.app.gesture.GestureServiceConnector$Error[] a = new com.navdy.hud.app.gesture.GestureServiceConnector$Error[3];
//        a[0] = CANNOT_CONNECT;
//        a[1] = COMMUNICATION_LOST;
//        a[2] = SWIPED_RESTARTED;
//        $VALUES = a;
//    }
//    
//    private GestureServiceConnector$Error(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.gesture.GestureServiceConnector$Error valueOf(String s) {
//        return (com.navdy.hud.app.gesture.GestureServiceConnector$Error)Enum.valueOf(com.navdy.hud.app.gesture.GestureServiceConnector$Error.class, s);
//    }
//    
//    public static com.navdy.hud.app.gesture.GestureServiceConnector$Error[] values() {
//        return $VALUES.clone();
//    }
//}
//