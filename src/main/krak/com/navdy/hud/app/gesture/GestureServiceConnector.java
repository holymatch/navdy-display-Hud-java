package com.navdy.hud.app.gesture;

public class GestureServiceConnector {
    final private static String AUTO_EXPOSURE_OFF = "autoexpo off";
    final private static String AUTO_EXPOSURE_ON = "autoexpo on";
    final private static String CALIBRATE_MASK = "calibrate-mask";
    final private static String CALIBRATE_MASK_FORCE = "calibrate-mask-force";
    final private static String CALIBRATE_SAVE_SNAPSHOT_ON = "calibrate-save-snapshot on";
    final private static String DEV_SOCKET_SWIPED = "/dev/socket/swiped";
    final private static String EXPOSURE = "exposure";
    final private static String FORMAT = "format";
    final private static String GAIN = "gain";
    final public static String GESTURE_ENABLED = "gesture.enabled";
    final public static String GESTURE_LEFT = "gesture left";
    final public static String GESTURE_RIGHT = "gesture right";
    final public static String GESTURE_VERSION = "persist.sys.gesture.version";
    final public static String LEFT = "left";
    final public static char LEFT_FIRST_CHAR = (char)108;
    final public static int MAX_RECORD_TIME = 6000;
    final private static int MINIMUM_INTERVAL_BETWEEN_SNAPSHOTS = 5000;
    final private static String NOTIFY = "notify";
    final private static int PID_CHECK_INTERVAL = 10000;
    final public static String RECORDING_SAVED = "recording-saved:";
    final public static String RECORD_SAVE = "record save";
    final public static String RIGHT = "right";
    final public static char RIGHT_FIRST_CHAR = (char)114;
    final public static String SNAPSHOT = "snapshot ";
    final public static String SNAPSHOT_PATH = "/data/misc/swiped/camera.png";
    final private static String SOCKET_NAME = "swiped";
    final private static int SO_TIMEOUT = 2000;
    final public static String SWIPED_DISABLE_CALIBRATION = "calibration pause";
    final public static String SWIPED_ENABLE_CALIBRATION = "calibration resume";
    final private static String SWIPED_PROCESS_NAME = "/system/xbin/swiped";
    final public static String SWIPED_RECORD_MODE_ONE_SHOT = "record-mode oneshot";
    final public static String SWIPED_RECORD_MODE_ROLLING = "record-mode rolling";
    final public static String SWIPED_START_RECORDING = "record on";
    final public static String SWIPED_STOP_RECORDING = "record off";
    final public static String SWIPE_PROGRESS = "swipe-progress:";
    final public static int SWIPE_PROGRESS_COMMAND_DATA_OFFSET;
    final public static String SWIPE_PROGRESS_LEFT = "swipe-progress: left,";
    final public static String SWIPE_PROGRESS_RIGHT = "swipe-progress: right,";
    final public static String SWIPE_PROGRESS_UNKNOWN = "swipe-progress: unknown,";
    final public static String UNKNOWN = "unknown";
    final private static java.nio.charset.CharsetEncoder encoder;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static byte[] temp;
    private com.squareup.otto.Bus bus;
    private long lastSnapshotTime;
    private com.navdy.hud.app.device.PowerManager powerManager;
    private String recordingPath;
    private volatile boolean running;
    private boolean shuttingDown;
    private volatile Thread swipedReader;
    private volatile android.net.LocalSocket swipedSocket;
    private String vin;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.gesture.GestureServiceConnector.class);
        SWIPE_PROGRESS_COMMAND_DATA_OFFSET = "swipe-progress:".length();
        encoder = java.nio.charset.Charset.defaultCharset().newEncoder();
        temp = new byte[1024];
    }
    
    public GestureServiceConnector(com.squareup.otto.Bus a, com.navdy.hud.app.device.PowerManager a0) {
        this.lastSnapshotTime = 0L;
        this.vin = null;
        this.bus = a;
        this.powerManager = a0;
        a.register(this);
    }
    
    static boolean access$000(com.navdy.hud.app.gesture.GestureServiceConnector a) {
        return a.running;
    }
    
    static com.navdy.service.library.log.Logger access$100() {
        return sLogger;
    }
    
    static android.net.LocalSocket access$200(com.navdy.hud.app.gesture.GestureServiceConnector a) {
        return a.swipedSocket;
    }
    
    static android.net.LocalSocket access$202(com.navdy.hud.app.gesture.GestureServiceConnector a, android.net.LocalSocket a0) {
        a.swipedSocket = a0;
        return a0;
    }
    
    static com.navdy.hud.app.gesture.GestureServiceConnector$Error access$300(com.navdy.hud.app.gesture.GestureServiceConnector a, android.net.LocalSocket a0) {
        return a.communicateWithSwipeDaemon(a0);
    }
    
    static void access$400(com.navdy.hud.app.gesture.GestureServiceConnector a) {
        a.closeSocket();
    }
    
    static void access$500(com.navdy.hud.app.gesture.GestureServiceConnector a) {
        a.reStart();
    }
    
    private void closeSocket() {
        if (this.swipedSocket != null) {
            java.io.InputStream a = null;
            java.io.OutputStream a0 = null;
            try {
                a = this.swipedSocket.getInputStream();
            } catch(Throwable ignoredException) {
                a = null;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
            try {
                a0 = this.swipedSocket.getOutputStream();
            } catch(Throwable ignoredException0) {
                a0 = null;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.swipedSocket);
            this.swipedSocket = null;
            sLogger.v("swipedconnector socket closed");
        }
    }
    
    private void closeThread() {
        if (this.swipedReader != null) {
            if (this.swipedReader.isAlive()) {
                sLogger.v("swipedconnector thread alive");
                this.swipedReader.interrupt();
                sLogger.v("swipedconnector thread waiting");
                try {
                    this.swipedReader.join();
                } catch(Throwable ignoredException) {
                }
                sLogger.v("swipedconnector thread waited");
            }
            this.swipedReader = null;
        }
    }
    
    private com.navdy.hud.app.gesture.GestureServiceConnector$Error communicateWithSwipeDaemon(android.net.LocalSocket a) {
        boolean b = false;
        com.navdy.hud.app.gesture.GestureServiceConnector$Error a0 = null;
        label12: {
            Throwable a1 = null;
            label11: try {
                b = false;
                a.connect(new android.net.LocalSocketAddress("swiped", android.net.LocalSocketAddress$Namespace.RESERVED));
                this.sendCommand("notify");
                b = true;
                this.sendCommand("calibration resume");
                b = true;
                this.sendCommand("record off");
                b = true;
                this.sendCommand("record-mode rolling");
                b = true;
                break label12;
            } catch(Throwable a2) {
                sLogger.d("socket connect to reserved namespace failed", a2);
                try {
                    a.connect(new android.net.LocalSocketAddress("/dev/socket/swiped", android.net.LocalSocketAddress$Namespace.FILESYSTEM));
                    this.sendCommand("notify");
                } catch(Throwable ignoredException) {
                    a1 = a2;
                    break label11;
                }
                b = true;
                break label12;
            }
            sLogger.d("socket connect to filesystem namespace failed", a1);
        }
        label0: if (this.running) {
            if (b) {
                {
                    label7: {
                        Throwable a3 = null;
                        label1: {
                            int i = 0;
                            java.io.InputStream a4 = null;
                            byte[] a5 = null;
                            long j = 0L;
                            try {
                                i = com.navdy.service.library.util.SystemUtils.getNativeProcessId("/system/xbin/swiped");
                                sLogger.v("swipedconnector connected");
                                this.bus.post(new com.navdy.hud.app.gesture.GestureServiceConnector$ConnectedEvent());
                                a4 = a.getInputStream();
                                sLogger.d("read starting");
                                a5 = new byte[1000];
                                sLogger.v("setting timeout");
                                this.swipedSocket.setSoTimeout(2000);
                                sLogger.v("set timeout");
                                j = android.os.SystemClock.elapsedRealtime();
                                sLogger.v(new StringBuilder().append("swipedconnector pid  [").append(i).append("]").toString());
                            } catch(Throwable a6) {
                                a3 = a6;
                                break label1;
                            }
                            label6: {
                                int i0 = 0;
                                label2: while(true) {
                                    Throwable a7 = null;
                                    label3: {
                                        String[] a8 = null;
                                        int i1 = 0;
                                        java.util.ArrayList a9 = null;
                                        int i2 = 0;
                                        label10: {
                                            label9: try {
                                                int i3 = 0;
                                                int i4 = 0;
                                                boolean b0 = false;
                                                if (!this.running) {
                                                    break label7;
                                                }
                                                label8: {
                                                    long j0 = 0L;
                                                    java.io.IOException a10 = null;
                                                    try {
                                                        j0 = j;
                                                        i3 = a4.read(a5);
                                                        break label8;
                                                    } catch(java.io.IOException a11) {
                                                        a10 = a11;
                                                    }
                                                    if (!this.running) {
                                                        break label7;
                                                    }
                                                    if (!"Try again".equals(a10.getMessage())) {
                                                        break label7;
                                                    }
                                                    j = android.os.SystemClock.elapsedRealtime();
                                                    if (j - j0 <= 10000L) {
                                                        j = j0;
                                                        continue label2;
                                                    }
                                                    i0 = com.navdy.service.library.util.SystemUtils.getNativeProcessId("/system/xbin/swiped");
                                                    break label9;
                                                }
                                                if (i3 < 0) {
                                                    break label6;
                                                }
                                                if (i3 <= 1) {
                                                    continue label2;
                                                }
                                                String s = new String(a5, 0, i3 - 1);
                                                switch(s.hashCode()) {
                                                    case 2037586046: {
                                                        i4 = (s.equals("gesture left")) ? 0 : -1;
                                                        break;
                                                    }
                                                    case -1253681019: {
                                                        i4 = (s.equals("gesture right")) ? 1 : -1;
                                                        break;
                                                    }
                                                    default: {
                                                        i4 = -1;
                                                    }
                                                }
                                                switch(i4) {
                                                    case 1: {
                                                        this.bus.post(new com.navdy.service.library.events.input.GestureEvent(com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_RIGHT, Integer.valueOf(0), Integer.valueOf(0)));
                                                        continue label2;
                                                    }
                                                    case 0: {
                                                        this.bus.post(new com.navdy.service.library.events.input.GestureEvent(com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_LEFT, Integer.valueOf(0), Integer.valueOf(0)));
                                                        continue label2;
                                                    }
                                                    default: {
                                                        b0 = s.startsWith("swipe-progress:");
                                                    }
                                                }
                                                label5: {
                                                    Throwable a12 = null;
                                                    if (!b0) {
                                                        break label5;
                                                    }
                                                    if (s.length() <= SWIPE_PROGRESS_COMMAND_DATA_OFFSET + 1) {
                                                        break label5;
                                                    }
                                                    int i5 = s.charAt(SWIPE_PROGRESS_COMMAND_DATA_OFFSET + 1);
                                                    label4: {
                                                        Throwable a13 = null;
                                                        switch(i5) {
                                                            case 114: {
                                                                try {
                                                                    float f = Float.valueOf(s.substring("swipe-progress: right,".length())).floatValue();
                                                                    this.bus.post(new com.navdy.hud.app.gesture.GestureServiceConnector$GestureProgress(com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection.RIGHT, f));
                                                                    continue label2;
                                                                } catch(Throwable a14) {
                                                                    a13 = a14;
                                                                    break;
                                                                }
                                                            }
                                                            case 108: {
                                                                try {
                                                                    float f0 = Float.valueOf(s.substring("swipe-progress: left,".length())).floatValue();
                                                                    this.bus.post(new com.navdy.hud.app.gesture.GestureServiceConnector$GestureProgress(com.navdy.hud.app.gesture.GestureServiceConnector$GestureDirection.LEFT, f0));
                                                                    continue label2;
                                                                } catch(Throwable a15) {
                                                                    a12 = a15;
                                                                    break label4;
                                                                }
                                                            }
                                                            default: {
                                                                continue label2;
                                                            }
                                                        }
                                                        sLogger.e(new StringBuilder().append("Error parsing the progress ").append(s).toString(), a13);
                                                        continue label2;
                                                    }
                                                    sLogger.e(new StringBuilder().append("Error parsing the progress ").append(s).toString(), a12);
                                                    continue label2;
                                                }
                                                if (s.startsWith("recording-saved:")) {
                                                    this.recordingPath = s.substring("recording-saved:".length() + 1);
                                                    sLogger.i(new StringBuilder().append("recording saved at ").append(this.recordingPath).toString());
                                                    synchronized(this) {
                                                        (this).notify();
                                                        /*monexit(this)*/;
                                                    }
                                                    this.bus.post(new com.navdy.hud.app.gesture.GestureServiceConnector$RecordingSaved(this.recordingPath));
                                                    continue label2;
                                                }
                                                if (!s.startsWith("swiped event: ")) {
                                                    continue label2;
                                                }
                                                a8 = s.split(" ");
                                                i1 = a8.length;
                                                break label10;
                                            } catch(Throwable a17) {
                                                a3 = a17;
                                                break label1;
                                            }
                                            if (i0 == i) {
                                                continue label2;
                                            }
                                            break label2;
                                        }
                                        if (i1 < 3) {
                                            continue label2;
                                        }
                                        String s0 = a8[2];
                                        try {
                                            a9 = new java.util.ArrayList();
                                            i2 = 3;
                                        } catch(Throwable a18) {
                                            a3 = a18;
                                            break label1;
                                        }
                                        while(true) {
                                            String[] a19 = null;
                                            try {
                                                if (i2 >= a8.length) {
                                                    try {
                                                        com.navdy.hud.app.analytics.AnalyticsSupport.recordSwipedCalibration(s0, (String[])((java.util.List)a9).toArray((Object[])new String[((java.util.List)a9).size()]));
                                                        continue label2;
                                                    } catch(Throwable a20) {
                                                        a3 = a20;
                                                        break label1;
                                                    }
                                                }
                                                a19 = a8[i2].split("=");
                                            } catch(Throwable a21) {
                                                a3 = a21;
                                                break label1;
                                            }
                                            String s1 = a19[0];
                                            String s2 = a19[1];
                                            try {
                                                ((java.util.List)a9).add(s1);
                                                ((java.util.List)a9).add(s2);
                                                i2 = i2 + 1;
                                            } catch(Throwable a22) {
                                                a3 = a22;
                                                break label1;
                                            }
                                        }
                                    }
                                    while(true) {
                                        try {
                                            /*monexit(this)*/;
                                        } catch(IllegalMonitorStateException | NullPointerException a23) {
                                            Throwable a24 = a23;
                                            a7 = a24;
                                            continue;
                                        }
                                        try {
                                            throw a7;
                                        } catch(Throwable a25) {
                                            a3 = a25;
                                            break label1;
                                        }
                                    }
                                }
                                try {
                                    sLogger.e(new StringBuilder().append("swipedconnector pid has changed from [").append(i).append("] to [").append(i0).append("]").toString());
                                    a0 = com.navdy.hud.app.gesture.GestureServiceConnector$Error.SWIPED_RESTARTED;
                                    break label0;
                                } catch(Throwable a26) {
                                    a3 = a26;
                                    break label1;
                                }
                            }
                            try {
                                sLogger.v("swipedconnector closed the socket");
                                a0 = com.navdy.hud.app.gesture.GestureServiceConnector$Error.COMMUNICATION_LOST;
                                break label0;
                            } catch(Throwable a27) {
                                a3 = a27;
                            }
                        }
                        sLogger.e("swipedconnector", a3);
                    }
                    a0 = com.navdy.hud.app.gesture.GestureServiceConnector$Error.COMMUNICATION_LOST;
                }
            } else {
                sLogger.v("swipedconnector could not connect");
                a0 = com.navdy.hud.app.gesture.GestureServiceConnector$Error.CANNOT_CONNECT;
            }
        } else {
            sLogger.v("swipedconnector is not running");
            a0 = com.navdy.hud.app.gesture.GestureServiceConnector$Error.CANNOT_CONNECT;
        }
        return a0;
    }
    
    private void reStart() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.gesture.GestureServiceConnector$2(this), 1);
    }
    
    public void ObdStateChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdConnectionStatusEvent a) {
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice() && a.connected) {
            String s = com.navdy.hud.app.obd.ObdManager.getInstance().getVin();
            if (s != null && !s.equals(this.vin)) {
                this.vin = s;
                this.sendCommandAsync(new StringBuilder().append("set-id ").append(this.vin).toString());
            }
        }
    }
    
    public String dumpRecording() {
        String s = null;
        label0: synchronized(this) {
            try {
                try {
                    this.sendCommand("record save");
                    (this).wait(6000L);
                    s = this.recordingPath;
                    break label0;
                } catch(java.io.IOException ignoredException) {
                }
            } catch(InterruptedException ignoredException0) {
            }
            s = null;
        }
        /*monexit(this)*/;
        return s;
    }
    
    public void enablePreview(boolean b) {
    }
    
    public boolean isRunning() {
        return this.swipedSocket != null;
    }
    
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            com.navdy.service.library.events.preferences.InputPreferences a0 = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getInputPreferences();
            if (a0 != null) {
                this.onInputPreferenceUpdate(a0);
            }
        }
    }
    
    public void onInputPreferenceUpdate(com.navdy.service.library.events.preferences.InputPreferences a) {
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice() && !this.shuttingDown && !this.powerManager.inQuietMode()) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.gesture.GestureServiceConnector$5(this, a), 1);
        }
    }
    
    public void onShutdown(com.navdy.hud.app.event.Shutdown a) {
        if (a.state == com.navdy.hud.app.event.Shutdown$State.CONFIRMED) {
            this.shuttingDown = true;
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.gesture.GestureServiceConnector$6(this), 1);
        }
    }
    
    public void onTakeSnapshot(com.navdy.hud.app.gesture.GestureServiceConnector$TakeSnapshot a) {
        boolean b = com.navdy.hud.app.util.DeviceUtil.isNavdyDevice();
        label1: {
            java.io.IOException a0 = null;
            if (!b) {
                break label1;
            }
            sLogger.d("Request to take snap shot");
            label0: {
                try {
                    this.takeSnapShot("/data/misc/swiped/camera.png");
                } catch(java.io.IOException a1) {
                    a0 = a1;
                    break label0;
                }
                break label1;
            }
            sLogger.e("Exception while taking the snapshot ", (Throwable)a0);
        }
    }
    
    public void onWakeup(com.navdy.hud.app.event.Wakeup a) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.gesture.GestureServiceConnector$4(this), 1);
    }
    
    public void sendCommand(String s) {
        synchronized(this) {
            if (this.swipedSocket != null) {
                java.nio.ByteBuffer a = java.nio.ByteBuffer.wrap(temp);
                encoder.encode(java.nio.CharBuffer.wrap((CharSequence)s), a, true);
                a.put((byte)0);
                this.swipedSocket.getOutputStream().write(temp, 0, a.position());
            }
        }
        /*monexit(this)*/;
    }
    
    public void sendCommand(Object[] a) {
        if (a != null && a.length > 0) {
            StringBuilder a0 = new StringBuilder();
            int i = a.length;
            int i0 = 0;
            while(i0 < i) {
                Object a1 = a[i0];
                if (a1 != null) {
                    if (a1 != a[a.length - 1]) {
                        a0.append(new StringBuilder().append(a1.toString()).append(" ").toString());
                    } else {
                        a0.append(a1.toString());
                    }
                }
                i0 = i0 + 1;
            }
            this.sendCommand(a0.toString());
        }
    }
    
    public void sendCommandAsync(String s) {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.gesture.GestureServiceConnector$3(this, s), 10);
    }
    
    public void setCalibrationEnabled(boolean b) {
        String s = b ? "calibration resume" : "calibration pause";
        try {
            this.sendCommand(s);
        } catch(java.io.IOException a) {
            sLogger.e("Exception while setting calibration state ", (Throwable)a);
        }
    }
    
    public void setDiscreteMode(boolean b) {
    }
    
    public void setRecordMode(boolean b) {
        sLogger.d(new StringBuilder().append("setRecordMode ").append(b).toString());
        String s = b ? "record-mode oneshot" : "record-mode rolling";
        try {
            this.sendCommand(s);
        } catch(java.io.IOException a) {
            sLogger.e("Exception while setting the record mode ", (Throwable)a);
        }
    }
    
    public void start() {
        synchronized(this) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
                if (this.running) {
                    sLogger.v("swipedconnector already running");
                } else {
                    this.closeSocket();
                    this.closeThread();
                    if (this.powerManager.inQuietMode()) {
                        sLogger.v("Not starting gesture engine due to quiet mode");
                    } else {
                        this.running = true;
                        com.navdy.hud.app.util.os.SystemProperties.set("gesture.enabled", com.navdy.hud.app.util.os.SystemProperties.get("persist.sys.gesture.version", com.navdy.hud.app.util.DeviceUtil.isUserBuild() ? "1" : "beta"));
                        this.swipedReader = new Thread((Runnable)new com.navdy.hud.app.gesture.GestureServiceConnector$1(this));
                        this.swipedReader.setName("SwipedReaderThread");
                        this.swipedReader.start();
                        sLogger.v("swipedconnector connect thread started");
                    }
                }
            }
        }
        /*monexit(this)*/;
    }
    
    public void startRecordingVideo() {
        try {
            sLogger.d("startRecordingVideo");
            this.sendCommand("record on");
        } catch(java.io.IOException ignoredException) {
        }
    }
    
    public void stop() {
        synchronized(this) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice() && this.running) {
                this.running = false;
                com.navdy.hud.app.util.os.SystemProperties.set("gesture.enabled", "0");
                this.closeSocket();
                this.closeThread();
            }
        }
        /*monexit(this)*/;
    }
    
    public void stopRecordingVideo() {
        sLogger.d("stopRecordingVideo");
        try {
            this.sendCommand("record off");
        } catch(java.io.IOException ignoredException) {
        }
    }
    
    public void takeSnapShot(String s) {
        long j = android.os.SystemClock.elapsedRealtime();
        if (j - this.lastSnapshotTime > 5000L) {
            this.lastSnapshotTime = j;
            this.sendCommand(new StringBuilder().append("snapshot ").append(s).toString());
        }
    }
}
