package com.navdy.hud.app.gesture;

class GestureServiceConnector$1 implements Runnable {
    final com.navdy.hud.app.gesture.GestureServiceConnector this$0;
    
    GestureServiceConnector$1(com.navdy.hud.app.gesture.GestureServiceConnector a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        while(true) {
            Throwable a = null;
            boolean b = com.navdy.hud.app.gesture.GestureServiceConnector.access$000(this.this$0);
            label0: {
                label3: if (b) {
                    label2: {
                        label1: {
                            try {
                                com.navdy.hud.app.gesture.GestureServiceConnector.access$100().v("creating socket");
                                com.navdy.hud.app.gesture.GestureServiceConnector.access$202(this.this$0, new android.net.LocalSocket(1));
                                com.navdy.hud.app.gesture.GestureServiceConnector.access$100().v("bind socket");
                                com.navdy.hud.app.gesture.GestureServiceConnector.access$200(this.this$0).bind(new android.net.LocalSocketAddress(""));
                                com.navdy.hud.app.gesture.GestureServiceConnector.access$100().v("calling communicateWithSwipeDaemon");
                                com.navdy.hud.app.gesture.GestureServiceConnector$Error a0 = com.navdy.hud.app.gesture.GestureServiceConnector.access$300(this.this$0, com.navdy.hud.app.gesture.GestureServiceConnector.access$200(this.this$0));
                                com.navdy.hud.app.gesture.GestureServiceConnector.access$100().v("called communicateWithSwipeDaemon");
                                if (!com.navdy.hud.app.gesture.GestureServiceConnector.access$000(this.this$0)) {
                                    break label1;
                                }
                                com.navdy.hud.app.gesture.GestureServiceConnector.access$400(this.this$0);
                                switch(com.navdy.hud.app.gesture.GestureServiceConnector$7.$SwitchMap$com$navdy$hud$app$gesture$GestureServiceConnector$Error[a0.ordinal()]) {
                                    case 3: {
                                        com.navdy.hud.app.gesture.GestureServiceConnector.access$100().v("communicateWithSwipeDaemon  swiped restarted, restart");
                                        com.navdy.hud.app.gesture.GestureServiceConnector.access$500(this.this$0);
                                        break label2;
                                    }
                                    case 2: {
                                        com.navdy.hud.app.gesture.GestureServiceConnector.access$100().v("communicateWithSwipeDaemon  communication lost, restart");
                                        com.navdy.hud.app.gesture.GestureServiceConnector.access$500(this.this$0);
                                        break;
                                    }
                                    case 1: {
                                        com.navdy.hud.app.gesture.GestureServiceConnector.access$100().v("communicateWithSwipeDaemon  cannot connect");
                                        Thread.sleep(5000L);
                                        break label1;
                                    }
                                    default: {
                                        break label1;
                                    }
                                }
                            } catch(Throwable a1) {
                                a = a1;
                                break label0;
                            }
                            com.navdy.hud.app.gesture.GestureServiceConnector.access$100().v("swipedconnector connect thread exit");
                            break label3;
                        }
                        com.navdy.hud.app.gesture.GestureServiceConnector.access$100().v("swipedconnector connect thread exit");
                        continue;
                    }
                    com.navdy.hud.app.gesture.GestureServiceConnector.access$100().v("swipedconnector connect thread exit");
                }
                return;
            }
            try {
                com.navdy.hud.app.gesture.GestureServiceConnector.access$100().e(a);
                if (com.navdy.hud.app.gesture.GestureServiceConnector.access$000(this.this$0)) {
                    com.navdy.hud.app.util.GenericUtil.sleep(5000);
                }
            } catch(Throwable a2) {
                com.navdy.hud.app.gesture.GestureServiceConnector.access$100().v("swipedconnector connect thread exit");
                throw a2;
            }
            com.navdy.hud.app.gesture.GestureServiceConnector.access$100().v("swipedconnector connect thread exit");
        }
    }
}
