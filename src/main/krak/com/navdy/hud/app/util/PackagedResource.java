package com.navdy.hud.app.util;

public class PackagedResource {
    final private static int BUFFER_SIZE = 16384;
    final private static String MD5_SUFFIX = ".md5";
    final private static Object lockObject;
    final private static com.navdy.service.library.log.Logger sLogger;
    private String basePath;
    private String destinationFilename;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.PackagedResource.class);
        lockObject = new Object();
    }
    
    public PackagedResource(String s, String s0) {
        this.destinationFilename = s;
        this.basePath = s0;
    }
    
    private static void extractFile(java.util.zip.ZipInputStream a, String s, byte[] a0) {
        label3: {
            java.io.FileOutputStream a1 = null;
            Throwable a2 = null;
            label1: {
                label2: {
                    try {
                        a1 = new java.io.FileOutputStream(s);
                        break label2;
                    } catch(Throwable a3) {
                        a2 = a3;
                    }
                    a1 = null;
                    break label1;
                }
                label0: {
                    try {
                        while(true) {
                            int i = a.read(a0);
                            if (i == -1) {
                                break;
                            }
                            a1.write(a0, 0, i);
                        }
                        com.navdy.service.library.util.IOUtils.fileSync(a1);
                        break label0;
                    } catch(Throwable a4) {
                        a2 = a4;
                    }
                    break label1;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                break label3;
            }
            try {
                sLogger.e("error extracting file", a2);
            } catch(Throwable a5) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                throw a5;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
        }
    }
    
    private static boolean unpack(String s, java.io.InputStream a) {
        boolean b = false;
        java.io.File a0 = new java.io.File(s);
        byte[] a1 = new byte[16384];
        if (!a0.exists()) {
            a0.mkdirs();
        }
        java.util.zip.ZipInputStream a2 = new java.util.zip.ZipInputStream((java.io.InputStream)new java.io.BufferedInputStream(a));
        label1: {
            Throwable a3 = null;
            label0: {
                try {
                    while(true) {
                        java.util.zip.ZipEntry a4 = a2.getNextEntry();
                        if (a4 == null) {
                            break;
                        }
                        String s0 = new StringBuilder().append(s).append(java.io.File.separator).append(a4.getName()).toString();
                        if (a4.isDirectory()) {
                            new java.io.File(s0).mkdirs();
                        } else {
                            com.navdy.hud.app.util.PackagedResource.extractFile(a2, s0, a1);
                        }
                        a2.closeEntry();
                    }
                } catch(Throwable a5) {
                    a3 = a5;
                    break label0;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                b = true;
                break label1;
            }
            try {
                sLogger.e("Error expanding zipEntry", a3);
            } catch(Throwable a6) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
                throw a6;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a2);
            b = false;
        }
        return b;
    }
    
    public static boolean unpackZip(String s, String s0) {
        boolean b = false;
        label3: {
            java.io.FileInputStream a = null;
            Throwable a0 = null;
            label1: {
                label2: {
                    try {
                        a = new java.io.FileInputStream(new StringBuilder().append(s).append(java.io.File.separator).append(s0).toString());
                        break label2;
                    } catch(Throwable a1) {
                        a0 = a1;
                    }
                    a = null;
                    break label1;
                }
                label0: {
                    try {
                        b = com.navdy.hud.app.util.PackagedResource.unpack(s, (java.io.InputStream)a);
                        break label0;
                    } catch(Throwable a2) {
                        a0 = a2;
                    }
                    break label1;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                break label3;
            }
            try {
                sLogger.e("Error reading resource", a0);
            } catch(Throwable a3) {
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                throw a3;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
            b = false;
        }
        return b;
    }
    
    public void updateFromResources(android.content.Context a, int i) {
        this.updateFromResources(a, i, -1);
    }
    
    public void updateFromResources(android.content.Context a, int i, int i0) {
        this.updateFromResources(a, i, i0, false);
    }
    
    public void updateFromResources(android.content.Context a, int i, int i0, boolean b) {        label5: synchronized(lockObject) {
            android.content.res.Resources a1 = a.getResources();
            java.io.File a2 = new java.io.File(this.basePath, new StringBuilder().append(this.destinationFilename).append(".md5").toString());
            java.io.File a3 = new java.io.File(this.basePath, this.destinationFilename);
            sLogger.v(new StringBuilder().append("updateFromResources [").append(this.destinationFilename).append("]").toString());
            label0: {
                java.io.InputStream a4 = null;
                label1: try {
                    boolean b0 = false;
                    String s = null;
                    if (i0 > 0) {
                        a4 = null;
                        a4 = a1.openRawResource(i0);
                        s = com.navdy.service.library.util.IOUtils.convertInputStreamToString(a4, "UTF-8");
                        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
                    }
                    a4 = null;
                    boolean b1 = a2.exists();
                    label4: {
                        label2: {
                            label3: {
                                if (!b1) {
                                    break label3;
                                }
                                if (b) {
                                    break label2;
                                }
                                a4 = null;
                                if (a3.exists()) {
                                    break label2;
                                }
                            }
                            com.navdy.service.library.log.Logger a5 = sLogger;
                            a4 = null;
                            a5.d("update required, no file");
                            b0 = true;
                            break label4;
                        }
                        try {
                            b0 = true;
                            String s0 = com.navdy.service.library.util.IOUtils.convertFileToString(a2.getAbsolutePath());
                            if (android.text.TextUtils.equals((CharSequence)s0, (CharSequence)s)) {
                                com.navdy.service.library.log.Logger a6 = sLogger;
                                b0 = false;
                                a6.d(new StringBuilder().append("no update required signature matches:").append(s0).toString());
                                b0 = false;
                            } else {
                                com.navdy.service.library.log.Logger a7 = sLogger;
                                b0 = true;
                                a7.d(new StringBuilder().append("update required, device:").append(s0).append(" apk:").append(s).toString());
                                b0 = true;
                            }
                        } catch(Throwable a8) {
                            com.navdy.service.library.log.Logger a9 = sLogger;
                            a4 = null;
                            a9.e(a8);
                        }
                    }
                    if (!b0) {
                        break label1;
                    }
                    a4 = null;
                    com.navdy.service.library.util.IOUtils.copyFile(a3.getAbsolutePath(), a1.openRawResource(i));
                    if (b) {
                        a4 = null;
                        com.navdy.hud.app.util.PackagedResource.unpackZip(this.basePath, a3.getName());
                        com.navdy.service.library.util.IOUtils.deleteFile(a, a3.getAbsolutePath());
                    }
                    if (s == null) {
                        break label0;
                    }
                    a4 = null;
                    com.navdy.service.library.util.IOUtils.copyFile(a2.getAbsolutePath(), s.getBytes());
                    break label0;
                } catch(Throwable a10) {
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a4);
                    throw a10;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
                /*monexit(a0)*/;
                break label5;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)null);
            /*monexit(a0)*/;
        }
    }
}
