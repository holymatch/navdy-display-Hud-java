package com.navdy.hud.app.util;

class ReportIssueService$2 implements com.navdy.service.library.network.http.services.JiraClient$ResultCallback {
    final com.navdy.hud.app.util.ReportIssueService this$0;
    final String val$assigneeEmail;
    final String val$assigneeName;
    final java.io.File val$attachmentFile;
    final java.io.File val$fileToSync;
    final org.json.JSONObject val$jsonObject;
    
    ReportIssueService$2(com.navdy.hud.app.util.ReportIssueService a, org.json.JSONObject a0, java.io.File a1, java.io.File a2, String s, String s0) {
        super();
        this.this$0 = a;
        this.val$jsonObject = a0;
        this.val$fileToSync = a1;
        this.val$attachmentFile = a2;
        this.val$assigneeName = s;
        this.val$assigneeEmail = s0;
    }
    
    public void onError(Throwable a) {
        com.navdy.hud.app.util.ReportIssueService.access$000().e(new StringBuilder().append("Error during sync ").append(a).toString());
        com.navdy.hud.app.util.ReportIssueService.access$200(this.this$0);
    }
    
    public void onSuccess(Object a) {
        boolean b = a instanceof String;
        String s = null;
        if (b) {
            s = (String)a;
            com.navdy.hud.app.util.ReportIssueService.access$000().d(new StringBuilder().append("Issue reported ").append(s).toString());
        }
        label5: try {
            java.io.FileWriter a0 = null;
            this.val$jsonObject.put("ticketId", s);
            String s0 = this.val$jsonObject.toString();
            label4: {
                label1: {
                    java.io.FileWriter a1 = null;
                    Throwable a2 = null;
                    try {
                        a1 = null;
                        label0: {
                            java.io.IOException a3 = null;
                            label3: {
                                label2: {
                                    try {
                                        a1 = null;
                                        a0 = new java.io.FileWriter(this.val$fileToSync);
                                        break label2;
                                    } catch(java.io.IOException a4) {
                                        a3 = a4;
                                    }
                                    a0 = null;
                                    break label3;
                                }
                                try {
                                    try {
                                        a0.write(s0);
                                        a0.close();
                                        break label1;
                                    } catch(java.io.IOException a5) {
                                        a3 = a5;
                                    }
                                } catch(Throwable a6) {
                                    a2 = a6;
                                    break label0;
                                }
                            }
                            a1 = a0;
                            com.navdy.hud.app.util.ReportIssueService.access$000().e(new StringBuilder().append("Error while dumping the issue ").append(a3.getMessage()).toString(), (Throwable)a3);
                            break label4;
                        }
                        a1 = a0;
                    } catch(Throwable a7) {
                        a2 = a7;
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                    throw a2;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
                break label5;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a0);
        } catch(org.json.JSONException ignoredException) {
            com.navdy.hud.app.util.ReportIssueService.access$000().e("JSONException while writing the meta data to the file");
        }
        com.navdy.hud.app.util.ReportIssueService.access$000().d("Attaching files to the submitted ticket");
        com.navdy.hud.app.util.ReportIssueService.access$300(this.this$0, s, this.val$attachmentFile, (com.navdy.service.library.network.http.services.JiraClient$ResultCallback)new com.navdy.hud.app.util.ReportIssueService$2$1(this));
    }
}
