package com.navdy.hud.app.util;

public class SerialNumber {
    public static com.navdy.hud.app.util.SerialNumber instance;
    final public String configurationCode;
    final public int dayOfWeek;
    final public String factory;
    final public String revisionCode;
    final public String serialNo;
    final public int week;
    final public int year;
    
    static {
        instance = com.navdy.hud.app.util.SerialNumber.parse(android.os.Build.SERIAL);
    }
    
    public SerialNumber(String s, int i, int i0, int i1, String s0, String s1, String s2) {
        this.factory = s;
        this.year = i;
        this.week = i0;
        this.dayOfWeek = i1;
        this.serialNo = s0;
        this.configurationCode = s1;
        this.revisionCode = s2;
    }
    
    public static com.navdy.hud.app.util.SerialNumber parse(String s) {
        com.navdy.hud.app.util.SerialNumber a = null;
        label6: {
            label4: {
                label5: {
                    if (s == null) {
                        break label5;
                    }
                    if (s.length() == 16) {
                        break label4;
                    }
                }
                a = new com.navdy.hud.app.util.SerialNumber("NDY", 0, 0, 0, "0", "0", "0");
                break label6;
            }
            String s0 = s.substring(0, 3);
            int i = com.navdy.hud.app.util.SerialNumber.parseNumber(s.substring(3, 4), 0, "year");
            int i0 = com.navdy.hud.app.util.SerialNumber.parseNumber(s.substring(4, 6), 1, "week");
            label2: {
                label3: {
                    if (i0 < 1) {
                        break label3;
                    }
                    if (i0 <= 52) {
                        break label2;
                    }
                }
                i0 = 1;
            }
            int i1 = com.navdy.hud.app.util.SerialNumber.parseNumber(s.substring(6, 7), 1, "dayOfWeek");
            label0: {
                label1: {
                    if (i1 < 1) {
                        break label1;
                    }
                    if (i1 <= 7) {
                        break label0;
                    }
                }
                i1 = 1;
            }
            a = new com.navdy.hud.app.util.SerialNumber(s0, i, i0, i1, s.substring(7, 10), s.substring(10, 14), s.substring(14, 15));
        }
        return a;
    }
    
    private static int parseNumber(String s, int i, String s0) {
        try {
            i = Integer.parseInt(s);
        } catch(NumberFormatException ignoredException) {
            android.util.Log.e("SERIAL", new StringBuilder().append("Invalid serial number field ").append(s0).append(" value:").append(s).toString());
        }
        return i;
    }
}
