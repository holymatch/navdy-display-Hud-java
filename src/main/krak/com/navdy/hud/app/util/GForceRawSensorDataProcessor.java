package com.navdy.hud.app.util;

public class GForceRawSensorDataProcessor {
    private static com.navdy.service.library.log.Logger logger;
    private boolean calibrated;
    private android.renderscript.Matrix3f rotationMatrix;
    private float[] v;
    public float xAccel;
    public float yAccel;
    public float zAccel;
    
    static {
        logger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.GForceRawSensorDataProcessor.class);
    }
    
    public GForceRawSensorDataProcessor() {
        this.v = new float[3];
    }
    
    private android.renderscript.Matrix3f calculateRotationMatrix(float[] a) {
        android.renderscript.Matrix3f a0 = new android.renderscript.Matrix3f();
        float f = a[0];
        float f0 = a[1];
        float f1 = a[2];
        double d = Math.atan2((double)f0, (double)f1);
        double d0 = Math.atan2((double)(-f), Math.sqrt((double)(f0 * f0 + f1 * f1)));
        double d1 = -d;
        double d2 = -d0;
        float f2 = (float)Math.cos(d1);
        float f3 = (float)Math.sin(d1);
        float f4 = -f3;
        float f5 = (float)Math.cos(d2);
        float f6 = (float)Math.sin(d2);
        float f7 = -f6;
        float[] a1 = new float[9];
        a1[0] = 1f;
        a1[1] = 0.0f;
        a1[2] = 0.0f;
        a1[3] = 0.0f;
        a1[4] = f2;
        a1[5] = f3;
        a1[6] = 0.0f;
        a1[7] = f4;
        a1[8] = f2;
        android.renderscript.Matrix3f a2 = new android.renderscript.Matrix3f(a1);
        float[] a3 = new float[9];
        a3[0] = f5;
        a3[1] = 0.0f;
        a3[2] = f7;
        a3[3] = 0.0f;
        a3[4] = 1f;
        a3[5] = 0.0f;
        a3[6] = f6;
        a3[7] = 0.0f;
        a3[8] = f5;
        a0.loadMultiply(a2, new android.renderscript.Matrix3f(a3));
        logger.d(new StringBuilder().append("acceleration vector: ").append(f).append(",").append(f0).append(",").append(f1).append(" derived rotation matrix: ").append(java.util.Arrays.toString(a0.getArray())).toString());
        return a0;
    }
    
    private float magnitude(float[] a) {
        return (float)Math.sqrt((double)(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]));
    }
    
    private void multiply(android.renderscript.Matrix3f a, float[] a0) {
        float[] a1 = a.getArray();
        float f = a0[0];
        float f0 = a0[1];
        float f1 = a0[2];
        float f2 = a1[0];
        float f3 = a1[1];
        float f4 = a1[2];
        float f5 = a1[3];
        float f6 = a1[4];
        float f7 = a1[5];
        float f8 = a1[6];
        float f9 = a1[7];
        float f10 = a1[8];
        a0[0] = f2 * f + f3 * f0 + f4 * f1;
        a0[1] = f5 * f + f6 * f0 + f7 * f1;
        a0[2] = f8 * f + f9 * f0 + f10 * f1;
    }
    
    public boolean isCalibrated() {
        return this.calibrated;
    }
    
    public void onRawData(com.navdy.hud.app.device.gps.RawSensorData a) {
        this.v[0] = a.x;
        this.v[1] = a.y;
        this.v[2] = a.z;
        if (!this.calibrated) {
            float f = this.magnitude(this.v);
            if ((double)f > 0.9 && (double)f < 1.1) {
                this.rotationMatrix = this.calculateRotationMatrix(this.v);
                this.calibrated = true;
            }
        }
        if (this.calibrated) {
            this.multiply(this.rotationMatrix, this.v);
        }
        this.xAccel = this.v[0];
        this.yAccel = this.v[1];
        this.zAccel = this.v[2];
    }
    
    public void setCalibrated(boolean b) {
        this.calibrated = b;
    }
}
