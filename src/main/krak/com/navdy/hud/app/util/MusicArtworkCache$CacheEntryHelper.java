package com.navdy.hud.app.util;

class MusicArtworkCache$CacheEntryHelper {
    String album;
    String author;
    String fileName;
    String name;
    final com.navdy.hud.app.util.MusicArtworkCache this$0;
    String type;
    
    MusicArtworkCache$CacheEntryHelper(com.navdy.hud.app.util.MusicArtworkCache a, com.navdy.service.library.events.audio.MusicCollectionInfo a0) {
        super();
        this.this$0 = a;
        this.type = (String)com.navdy.hud.app.util.MusicArtworkCache.access$000().get(a0.collectionType);
        switch(com.navdy.hud.app.util.MusicArtworkCache$3.$SwitchMap$com$navdy$service$library$events$audio$MusicCollectionType[a0.collectionType.ordinal()]) {
            case 5: {
                this.album = a0.name;
                break;
            }
            case 4: {
                this.album = a0.name;
                this.author = a0.subtitle;
                break;
            }
            case 3: {
                this.author = a0.name;
                break;
            }
            case 2: {
                this.name = a0.name;
                break;
            }
            case 1: {
                com.navdy.hud.app.util.MusicArtworkCache.access$100().e("Unknown type");
                break;
            }
            default: {
                this.name = a0.name;
                break;
            }
            case 6: {
            }
        }
    }
    
    MusicArtworkCache$CacheEntryHelper(com.navdy.hud.app.util.MusicArtworkCache a, String s, String s0, String s1) {
        super();
        this.this$0 = a;
        this.type = "music";
        this.author = s;
        this.album = s0;
        this.name = s1;
    }
    
    static String access$200(com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper a) {
        return a.getFileName();
    }
    
    private String formatIdentifier(CharSequence a) {
        java.util.regex.Matcher a0 = java.util.regex.Pattern.compile("[^A-Za-z0-9_]").matcher(a);
        String s = (android.text.TextUtils.isEmpty(a)) ? null : a0.replaceAll("_");
        return s;
    }
    
    private String getFileName() {
        if (this.fileName == null) {
            StringBuilder a = new StringBuilder(this.type);
            if (!android.text.TextUtils.isEmpty((CharSequence)this.author)) {
                a.append("_");
                a.append(this.author);
            }
            if (!android.text.TextUtils.isEmpty((CharSequence)this.album)) {
                a.append("_");
                a.append(this.album);
            }
            if (!android.text.TextUtils.isEmpty((CharSequence)this.name)) {
                a.append("_");
                a.append(this.name);
            }
            this.fileName = this.formatIdentifier((CharSequence)a);
        }
        return this.fileName;
    }
    
    public String toString() {
        return new StringBuilder().append(this.type).append(", ").append(this.author).append(" - ").append(this.album).append(" - ").append(this.name).toString();
    }
}
