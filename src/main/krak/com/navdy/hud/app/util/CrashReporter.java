package com.navdy.hud.app.util;

final public class CrashReporter {
    final private static int ANR_LOG_LIMIT = 32768;
    final private static String ANR_PATH = "/data/anr/traces.txt";
    final public static String HOCKEY_APP_ID = "20625a4f769df641ecf46825a71c1911";
    final public static String HOCKEY_APP_STABLE_ID = "4dd8d996ee304ef2994965aa7f6d4f15";
    final public static String HOCKEY_APP_UNSTABLE_ID = "ad4b9dbcbb3b4083a49af48878026e5a";
    final private static int KERNEL_CRASH_INFO_LIMIT = 32768;
    final private static String LOG_ANR_MARKER = "Wrote stack traces to '/data/anr/traces.txt'";
    final private static String LOG_BEGIN_MARKER = "--------- beginning of main";
    final private static int PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL;
    final private static int PERIOD_DELIVERY_CHECK_INTERVAL;
    final private static java.util.regex.Pattern START_OF_OOPS;
    final private static int SYSTEM_LOG_LIMIT = 32768;
    final private static String TOMBSTONE_FILE_PATTERN = "tombstone_light_";
    final private static String TOMBSTONE_PATH = "/data/tombstones";
    final public static java.util.regex.Pattern USER_REBOOT;
    private static com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    final private static com.navdy.hud.app.util.CrashReporter sInstance;
    final private static com.navdy.service.library.log.Logger sLogger;
    private static volatile boolean stopCrashReporting;
    private static String userId;
    private com.navdy.hud.app.util.CrashReporter$NavdyCrashListener crashManagerListener;
    private Runnable deliveryRunnable;
    private android.os.Handler handler;
    private boolean installed;
    private com.navdy.hud.app.util.OTAUpdateService$OTAFailedException otaFailure;
    private Runnable periodicRunnable;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.util.CrashReporter.class);
        START_OF_OOPS = java.util.regex.Pattern.compile("(Unable to handle)|(Internal error:)");
        USER_REBOOT = java.util.regex.Pattern.compile("(reboot: Restarting system with command)|(do_powerctl:)");
        PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis(2L);
        PERIOD_DELIVERY_CHECK_INTERVAL = (int)java.util.concurrent.TimeUnit.MINUTES.toMillis(5L);
        userId = android.os.Build.SERIAL;
        sInstance = new com.navdy.hud.app.util.CrashReporter();
    }
    
    private CrashReporter() {
        this.crashManagerListener = new com.navdy.hud.app.util.CrashReporter$NavdyCrashListener();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.periodicRunnable = (Runnable)new com.navdy.hud.app.util.CrashReporter$1(this);
        this.deliveryRunnable = (Runnable)new com.navdy.hud.app.util.CrashReporter$2(this);
        this.otaFailure = null;
        this.handler.postDelayed(this.periodicRunnable, (long)PERIOD_DELIVERY_CHECK_INITIAL_INTERVAL);
        driverProfileManager = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager();
    }
    
    static String access$000() {
        return userId;
    }
    
    static com.navdy.hud.app.profile.DriverProfileManager access$100() {
        return driverProfileManager;
    }
    
    static void access$1000(com.navdy.hud.app.util.CrashReporter a, String[] a0) {
        a.cleanupKernelCrashes(a0);
    }
    
    static com.navdy.hud.app.util.CrashReporter$NavdyCrashListener access$1100(com.navdy.hud.app.util.CrashReporter a) {
        return a.crashManagerListener;
    }
    
    static Runnable access$200(com.navdy.hud.app.util.CrashReporter a) {
        return a.deliveryRunnable;
    }
    
    static boolean access$300(com.navdy.hud.app.util.CrashReporter a) {
        return a.checkForCrashes();
    }
    
    static com.navdy.service.library.log.Logger access$400() {
        return sLogger;
    }
    
    static Runnable access$500(com.navdy.hud.app.util.CrashReporter a) {
        return a.periodicRunnable;
    }
    
    static int access$600() {
        return PERIOD_DELIVERY_CHECK_INTERVAL;
    }
    
    static android.os.Handler access$700(com.navdy.hud.app.util.CrashReporter a) {
        return a.handler;
    }
    
    static void access$800(com.navdy.hud.app.util.CrashReporter a, Thread a0, Throwable a1, Thread$UncaughtExceptionHandler a2) {
        a.handleUncaughtException(a0, a1, a2);
    }
    
    static String access$900(com.navdy.hud.app.util.CrashReporter a, java.io.File a0) {
        return a.filterKernelCrash(a0);
    }
    
    private boolean checkForCrashes() {
        boolean b = com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isNetworkAccessAllowed(com.navdy.hud.app.framework.network.NetworkBandwidthController$Component.HOCKEY);
        label1: {
            Throwable a = null;
            if (!b) {
                break label1;
            }
            label0: {
                try {
                    net.hockeyapp.android.CrashManager.submitStackTraces(new java.lang.ref.WeakReference(com.navdy.hud.app.HudApplication.getAppContext()), (net.hockeyapp.android.CrashManagerListener)this.crashManagerListener);
                } catch(Throwable a0) {
                    a = a0;
                    break label0;
                }
                break label1;
            }
            sLogger.e(a);
        }
        return true;
    }
    
    private void cleanupAnrFiles() {
        java.io.File a = new java.io.File("/data/anr/traces.txt");
        if (a.exists()) {
            boolean b = a.delete();
            sLogger.v(new StringBuilder().append("delete /data/anr/traces.txt :").append(b).toString());
        }
    }
    
    private void cleanupKernelCrashes(String[] a) {
        int i = a.length;
        int i0 = 0;
        while(i0 < i) {
            String s = a[i0];
            java.io.File a0 = new java.io.File(s);
            if (a0.exists()) {
                sLogger.v(new StringBuilder().append("Trying to delete ").append(s).toString());
                if (!a0.delete()) {
                    sLogger.e(new StringBuilder().append("Unable to delete kernel crash file: ").append(s).toString());
                }
            }
            i0 = i0 + 1;
        }
    }
    
    private String filterKernelCrash(java.io.File a) {
        String s = null;
        try {
            s = null;
            String s0 = com.navdy.service.library.util.IOUtils.convertFileToString(a.getAbsolutePath());
            if (s0 == null) {
                s = s0;
            } else {
                java.util.regex.Pattern a0 = USER_REBOOT;
                s = s0;
                if (a0.matcher((CharSequence)s0).find()) {
                    com.navdy.service.library.log.Logger a1 = sLogger;
                    s = s0;
                    a1.v("Ignoring user reboot log");
                    s = null;
                } else {
                    java.util.regex.Pattern a2 = START_OF_OOPS;
                    s = s0;
                    java.util.regex.Matcher a3 = a2.matcher((CharSequence)s0);
                    int i = s0.length() - 32768;
                    if (a3.find()) {
                        s = s0;
                        if (a3.start() < i) {
                            s = s0;
                            i = a3.start();
                        }
                    }
                    s = s0;
                    int i0 = Math.max(0, i);
                    s = s0.substring(i0, Math.min(s0.length() - i0, 32768));
                }
            }
        } catch(Throwable a4) {
            sLogger.e(a4);
        }
        return s;
    }
    
    public static com.navdy.hud.app.util.CrashReporter getInstance() {
        return sInstance;
    }
    
    private void handleUncaughtException(Thread a, Throwable a0, Thread$UncaughtExceptionHandler a1) {
        if (stopCrashReporting) {
            sLogger.i("FATAL-reporting-turned-off", a0);
        } else {
            stopCrashReporting = true;
            android.util.Log.e("FATAL-CRASH", new StringBuilder().append("FATAL-CRASH").append(" Uncaught exception - ").append(a.getName()).append(":").append(a.getId()).append(" ui thread id:").append(android.os.Looper.getMainLooper().getThread().getId()).toString(), a0);
            sLogger.i("closing logger");
            com.navdy.service.library.log.Logger.close();
            if (a1 != null) {
                a1.uncaughtException(a, a0);
            }
        }
    }
    
    public static boolean isEnabled() {
        boolean b = false;
        boolean b0 = com.navdy.hud.app.util.DeviceUtil.isNavdyDevice();
        label2: {
            label0: {
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!com.navdy.hud.app.HudApplication.isDeveloperBuild()) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void saveHockeyAppException(Throwable a) {
        try {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.util.CrashReporter$6(this, a), 1);
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }
    
    private static void setCrashReporting(boolean b) {
        stopCrashReporting = b;
    }
    
    private void setHockeyAppCrashUploading(boolean b) {
        try {
            sLogger.v(new StringBuilder().append("setHockeyAppCrashUploading: try to enabled:").append(b).toString());
            java.lang.reflect.Field a = net.hockeyapp.android.CrashManager.class.getDeclaredField("submitting");
            a.setAccessible(true);
            a.set(null, Boolean.valueOf(!b));
            sLogger.v(new StringBuilder().append("setHockeyAppCrashUploading: enabled=").append(b).toString());
        } catch(Throwable a0) {
            sLogger.e("setHockeyAppCrashUploading", a0);
        }
    }
    
    public void checkForAnr() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.navdy.service.library.log.Logger a = sLogger;
        try {
            a.v("checking for anr's");
            if (new java.io.File("/data/anr/traces.txt").exists()) {
                String s = com.navdy.service.library.util.IOUtils.convertFileToString("/data/anr/traces.txt");
                if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                    this.reportNonFatalException((Throwable)new com.navdy.hud.app.util.CrashReporter$AnrException(s));
                    sLogger.v(new StringBuilder().append("anr crash created size[").append(s.length()).append("]").toString());
                }
                this.cleanupAnrFiles();
            } else {
                sLogger.v("no anr files");
            }
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
    }
    
    public void checkForKernelCrashes(String[] a) {
        if (a != null && a.length != 0) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.util.CrashReporter$4(this, a), 1);
        }
    }
    
    public void checkForOTAFailure() {
        synchronized(this) {
            if (this.otaFailure != null) {
                this.reportNonFatalException((Throwable)this.otaFailure);
                this.otaFailure = null;
            }
        }
        /*monexit(this)*/;
    }
    
    public void checkForTombstones() {
        com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
        com.navdy.service.library.log.Logger a = sLogger;
        label2: try {
            a.v("checking for tombstones");
            java.io.File a0 = new java.io.File("/data/tombstones");
            boolean b = a0.exists();
            label3: {
                label4: {
                    if (!b) {
                        break label4;
                    }
                    if (a0.isDirectory()) {
                        break label3;
                    }
                }
                sLogger.v("no tombstones");
                break label2;
            }
            String[] a1 = a0.list((java.io.FilenameFilter)new com.navdy.hud.app.util.CrashReporter$5(this));
            label0: {
                label1: {
                    if (a1 == null) {
                        break label1;
                    }
                    if (a1.length != 0) {
                        break label0;
                    }
                }
                sLogger.v("no tombstones");
                break label2;
            }
            int i = 0;
            while(i < a1.length) {
                java.io.File a2 = new java.io.File(new StringBuilder().append("/data/tombstones").append(java.io.File.separator).append(a1[i]).toString());
                if (a2.exists()) {
                    String s = a2.getAbsolutePath();
                    String s0 = com.navdy.service.library.util.IOUtils.convertFileToString(s);
                    if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                        this.reportNonFatalException((Throwable)new com.navdy.hud.app.util.CrashReporter$TombstoneException(s0));
                        sLogger.v(new StringBuilder().append("tombstone exception logged file[").append(s).append("] size[").append(s0.length()).append("]").toString());
                    }
                    boolean b0 = a2.delete();
                    sLogger.v(new StringBuilder().append("tombstone deleted: ").append(b0).toString());
                }
                i = i + 1;
            }
        } catch(Throwable a3) {
            sLogger.e(a3);
        }
    }
    
    public void installCrashHandler(android.content.Context a) {
        synchronized(this) {
            if (!this.installed) {
                if (!com.navdy.hud.app.util.DeviceUtil.isUserBuild()) {
                    userId = new StringBuilder().append(android.os.Build.SERIAL).append("-eng").toString();
                }
                Thread$UncaughtExceptionHandler a0 = Thread.getDefaultUncaughtExceptionHandler();
                if (a0 == null) {
                    sLogger.v("default uncaught handler is null");
                } else {
                    sLogger.v(new StringBuilder().append("default uncaught handler:").append((a0).getClass().getName()).toString());
                }
                this.setHockeyAppCrashUploading(false);
                String s = com.navdy.hud.app.util.DeviceUtil.isUserBuild() ? "20625a4f769df641ecf46825a71c1911" : "4dd8d996ee304ef2994965aa7f6d4f15";
                net.hockeyapp.android.CrashManager.register(a, s, (net.hockeyapp.android.CrashManagerListener)this.crashManagerListener);
                net.hockeyapp.android.metrics.MetricsManager.register(a, (android.app.Application)com.navdy.hud.app.HudApplication.getApplication(), s);
                sLogger.v("enabled metrics manager");
                this.setHockeyAppCrashUploading(true);
                Thread$UncaughtExceptionHandler a1 = Thread.getDefaultUncaughtExceptionHandler();
                if (a1 == null) {
                    sLogger.v("default hockey handler is null");
                } else {
                    sLogger.v(new StringBuilder().append("hockey uncaught handler:").append((a1).getClass().getName()).toString());
                }
                Thread.setDefaultUncaughtExceptionHandler((Thread$UncaughtExceptionHandler)new com.navdy.hud.app.util.CrashReporter$3(this, a1));
                this.installed = true;
            }
        }
        /*monexit(this)*/;
    }
    
    public boolean isInstalled() {
        return this.installed;
    }
    
    public void log(String s) {
    }
    
    public void reportNonFatalException(Throwable a) {
        boolean b = this.installed;
        label0: {
            Throwable a0 = null;
            if (!b) {
                break label0;
            }
            try {
                this.saveHockeyAppException(a);
                break label0;
            } catch(Throwable a1) {
                a0 = a1;
            }
            sLogger.e(a0);
        }
    }
    
    public void reportOTAFailure(com.navdy.hud.app.util.OTAUpdateService$OTAFailedException a) {
        synchronized(this) {
            if (this.installed) {
                this.reportNonFatalException((Throwable)a);
            } else {
                if (this.otaFailure != null) {
                    sLogger.e("multiple OTAFailedException reports");
                }
                this.otaFailure = a;
            }
        }
        /*monexit(this)*/;
    }
    
    public void stopCrashReporting(boolean b) {
        com.navdy.hud.app.util.CrashReporter.setCrashReporting(b);
    }
}
