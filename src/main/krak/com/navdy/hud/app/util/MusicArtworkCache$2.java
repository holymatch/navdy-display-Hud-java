package com.navdy.hud.app.util;

class MusicArtworkCache$2 implements Runnable {
    final com.navdy.hud.app.util.MusicArtworkCache this$0;
    final com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper val$cacheEntryHelper;
    final com.navdy.hud.app.util.MusicArtworkCache$Callback val$callback;
    
    MusicArtworkCache$2(com.navdy.hud.app.util.MusicArtworkCache a, com.navdy.hud.app.util.MusicArtworkCache$CacheEntryHelper a0, com.navdy.hud.app.util.MusicArtworkCache$Callback a1) {
        super();
        this.this$0 = a;
        this.val$cacheEntryHelper = a0;
        this.val$callback = a1;
    }
    
    public void run() {
        String s = com.navdy.hud.app.util.MusicArtworkCache.access$400(this.this$0, this.val$cacheEntryHelper);
        if (s != null) {
            com.navdy.hud.app.storage.cache.DiskLruCache a = com.navdy.hud.app.util.picasso.PicassoUtil.getDiskLruCache();
            if (a != null) {
                byte[] a0 = a.get(s);
                if (a0 == null) {
                    this.val$callback.onMiss();
                } else {
                    this.val$callback.onHit(a0);
                }
            } else {
                com.navdy.hud.app.util.MusicArtworkCache.access$100().e("No disk cache");
                this.val$callback.onMiss();
            }
        } else {
            com.navdy.hud.app.util.MusicArtworkCache.access$100().i(new StringBuilder().append("No entry for ").append(this.val$cacheEntryHelper).toString());
            this.val$callback.onMiss();
        }
    }
}
