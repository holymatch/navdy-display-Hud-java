package com.navdy.hud.app.util;

abstract public interface MusicArtworkCache$Callback {
    abstract public void onHit(byte[] arg);
    
    
    abstract public void onMiss();
}
