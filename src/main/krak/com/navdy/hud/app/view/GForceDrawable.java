package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class GForceDrawable extends com.navdy.hud.app.view.drawable.CustomDrawable {
    final private static int BACKGROUND = 0;
    final private static int DANGER = 4;
    final private static int EXTREME = 5;
    final private static int HIGHLIGHT = 2;
    final public static float HIGH_G = 1.05f;
    final private static int LOW = 1;
    final public static float LOW_G = 0.45f;
    final public static float MAX_ACCEL = 2f;
    final public static float MEDIUM_G = 0.75f;
    final public static float NO_G = 0.15f;
    final private static int WARNING = 3;
    private static android.graphics.Typeface typeface;
    private int[] colors;
    protected com.navdy.service.library.log.Logger logger;
    private android.graphics.Paint mTextPaint;
    private int textColor;
    private float xAccel;
    private float yAccel;
    private float zAccel;
    
    public GForceDrawable(android.content.Context a) {
        this.logger = new com.navdy.service.library.log.Logger((this).getClass());
        this.mPaint.setColor(-1);
        this.mPaint.setStrokeWidth(1f);
        if (typeface == null) {
            typeface = android.graphics.Typeface.create("sans-serif-medium", 0);
        }
        this.mTextPaint = new android.graphics.Paint();
        this.mTextPaint.setTextAlign(android.graphics.Paint$Align.CENTER);
        android.content.res.Resources a0 = a.getResources();
        this.colors = a0.getIntArray(R.array.gforce_colors);
        this.textColor = a0.getColor(R.color.gforce_text);
    }
    
    private float clamp(float f, float f0, float f1) {
        if (f < f0) {
            f = f0;
        } else if (f > f1) {
            f = f1;
        }
        return f;
    }
    
    public void draw(android.graphics.Canvas a) {
        boolean b = false;
        String s = null;
        super.draw(a);
        android.graphics.Rect a0 = this.getBounds();
        int i = a0.width() - 10;
        float f = (float)((a0.height() - 10) / 2);
        float f0 = (float)(i / 2);
        float f1 = this.clamp(this.xAccel, -2f, 2f);
        float f2 = this.clamp(this.yAccel, -2f, 2f);
        this.clamp(this.zAccel, -2f, 2f);
        float f3 = Math.max(Math.abs(f1), Math.abs(f2));
        this.mPaint.setColor((this.colors[0] != 0) ? 1 : 0);
        a.drawCircle((float)5 + f0, (float)5 + f, 50f, this.mPaint);
        float f4 = (float)(Math.atan((double)(-f2 / f1)) * 180.0 / 3.141592653589793);
        if (f1 < 0.0f) {
            f4 = f4 + 180f;
        }
        boolean b0 = this.colors[1] != 0;
        if (f3 > 0.15f) {
            float f5 = 20f + 70f * (this.clamp(f3, 0.15f, 0.75f) - 0.15f) / 0.6f;
            float f6 = f5 / 2f;
            b0 = (f3 < 0.45f) ? this.colors[2] != 0 : (f3 < 0.75f) ? this.colors[3] != 0 : (f3 < 1.05f) ? this.colors[4] != 0 : this.colors[5] != 0;
            this.mPaint.setColor(b0 ? 1 : 0);
            a.drawArc(new android.graphics.RectF(50f - 50f, 50f - 50f, 50f + 50f, 50f + 50f), f4 - f6, f5, true, this.mPaint);
        }
        this.mPaint.setColor(-16777216);
        a.drawCircle((float)5 + f0, (float)5 + f, 36f, this.mPaint);
        this.mTextPaint.setColor(this.textColor);
        this.mTextPaint.setTextSize(16f);
        this.mTextPaint.setTypeface(android.graphics.Typeface.SANS_SERIF);
        a.drawText("G", (float)5 + f0, (float)5 + f - 18f, this.mTextPaint);
        a.drawText("-", (float)5 + f0, (float)5 + f + 28f, this.mTextPaint);
        this.mTextPaint.setColor(b0 ? 1 : 0);
        this.mTextPaint.setTextSize(28f);
        this.mTextPaint.setTypeface(typeface);
        float f7 = this.xAccel;
        int i0 = (f7 > 0.0f) ? 1 : (f7 == 0.0f) ? 0 : -1;
        label2: {
            label0: {
                label1: {
                    if (i0 != 0) {
                        break label1;
                    }
                    if (this.yAccel != 0.0f) {
                        break label1;
                    }
                    if (this.zAccel == 0.0f) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        if (b) {
            s = "-";
        } else {
            java.util.Locale a1 = java.util.Locale.US;
            Object[] a2 = new Object[1];
            a2[0] = Float.valueOf(f3);
            s = String.format(a1, "%01.2f", a2);
        }
        a.drawText(s, (float)5 + f0, (float)5 + f + 10f, this.mTextPaint);
    }
    
    protected void onBoundsChange(android.graphics.Rect a) {
        super.onBoundsChange(a);
    }
    
    public void setAcceleration(float f, float f0, float f1) {
        this.xAccel = f;
        this.yAccel = f0;
        this.zAccel = f1;
    }
}
