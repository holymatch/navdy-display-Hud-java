package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

class ShutDownConfirmationView$6 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.view.ShutDownConfirmationView this$0;
    
    ShutDownConfirmationView$6(com.navdy.hud.app.view.ShutDownConfirmationView a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        int i = ((Integer)a.getAnimatedValue()).intValue();
        android.content.res.Resources a0 = this.this$0.getResources();
        Object[] a1 = new Object[1];
        a1[0] = Integer.valueOf(i);
        String s = a0.getString(R.string.powering_off, a1);
        this.this$0.mSummaryText.setText((CharSequence)s);
    }
}
