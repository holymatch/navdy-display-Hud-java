package com.navdy.hud.app.view.drawable;
import com.navdy.hud.app.R;

public class SmallAnalogClockDrawable extends com.navdy.hud.app.view.drawable.CustomDrawable {
    private int frameColor;
    private int frameStrokeWidth;
    private int hour;
    private int hourHandColor;
    private int hourHandStrokeWidth;
    private int minute;
    private int minuteHandColor;
    private int minuteHandStrokeWidth;
    private int seconds;
    
    public SmallAnalogClockDrawable(android.content.Context a) {
        this.frameColor = a.getResources().getColor(R.color.small_analog_clock_frame_color);
        this.frameStrokeWidth = a.getResources().getDimensionPixelSize(R.dimen.small_analog_clock_frame_stroke_width);
        this.hourHandColor = a.getResources().getColor(R.color.cyan);
        this.hourHandStrokeWidth = a.getResources().getDimensionPixelSize(R.dimen.small_analog_clock_hour_hand_width);
        this.minuteHandStrokeWidth = a.getResources().getDimensionPixelSize(R.dimen.small_analog_clock_minute_hand_width);
        this.minuteHandColor = -1;
    }
    
    public void draw(android.graphics.Canvas a) {
        super.draw(a);
        android.graphics.Rect a0 = this.getBounds();
        android.graphics.RectF a1 = new android.graphics.RectF(a0);
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(android.graphics.Paint$Style.STROKE);
        this.mPaint.setStrokeWidth((float)this.frameStrokeWidth);
        this.mPaint.setColor(this.frameColor);
        android.graphics.RectF a2 = new android.graphics.RectF(a0);
        a2.inset((float)(this.frameStrokeWidth / 2) + 0.5f, (float)(this.frameStrokeWidth / 2) + 0.5f);
        a.drawArc(a2, 0.0f, 360f, true, this.mPaint);
        this.mPaint.setStyle(android.graphics.Paint$Style.STROKE);
        this.mPaint.setColor(this.hourHandColor);
        this.mPaint.setStrokeCap(android.graphics.Paint$Cap.ROUND);
        this.mPaint.setStrokeWidth((float)this.hourHandStrokeWidth);
        float f = com.navdy.hud.app.util.DateUtil.getClockAngleForHour(this.hour, this.minute);
        float f0 = (float)(a0.width() / 2 - 7);
        int i = (int)((double)a0.centerX() + (double)f0 * Math.cos(Math.toRadians((double)f)));
        int i0 = (int)((double)a0.centerY() + (double)f0 * Math.sin(Math.toRadians((double)f)));
        a.drawLine((float)a0.centerX(), (float)a0.centerY(), (float)i, (float)i0, this.mPaint);
        this.mPaint.setStyle(android.graphics.Paint$Style.STROKE);
        this.mPaint.setColor(this.minuteHandColor);
        this.mPaint.setStrokeCap(android.graphics.Paint$Cap.ROUND);
        this.mPaint.setStrokeWidth((float)this.minuteHandStrokeWidth);
        float f1 = com.navdy.hud.app.util.DateUtil.getClockAngleForMinutes(this.minute);
        float f2 = (float)(a0.width() / 2 - 6);
        int i1 = (int)((double)a0.centerX() + (double)f2 * Math.cos(Math.toRadians((double)f1)));
        int i2 = (int)((double)a0.centerY() + (double)f2 * Math.sin(Math.toRadians((double)f1)));
        a.drawLine((float)a0.centerX(), (float)a0.centerY(), (float)i1, (float)i2, this.mPaint);
    }
    
    public void setTime(int i, int i0, int i1) {
        this.hour = i;
        this.minute = i0;
        this.seconds = i1;
    }
}
