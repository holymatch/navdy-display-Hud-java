package com.navdy.hud.app.view.drawable;
import com.navdy.hud.app.R;

public class FuelGaugeDrawable2 extends com.navdy.hud.app.view.drawable.GaugeDrawable {
    final private static int TICKS_COUNT = 4;
    final private static int TICK_ANGLE_WIDTH = 2;
    private int mBackgroundColor;
    private int mFuelGaugeWidth;
    private boolean mLeftOriented;
    
    public FuelGaugeDrawable2(android.content.Context a, int i) {
        super(a, 0, i);
        this.mLeftOriented = true;
        this.mFuelGaugeWidth = a.getResources().getDimensionPixelSize(R.dimen.fuel_gauge_width);
        this.mBackgroundColor = this.mColorTable[3];
    }
    
    public void draw(android.graphics.Canvas a) {
        super.draw(a);
        android.graphics.Rect a0 = this.getBounds();
        android.graphics.RectF a1 = (this.mLeftOriented) ? new android.graphics.RectF((float)a0.left, (float)a0.top, (float)(a0.right + a0.width()), (float)a0.bottom) : new android.graphics.RectF((float)(a0.left - a0.width()), (float)a0.top, (float)a0.right, (float)a0.bottom);
        a1.inset((float)(this.mFuelGaugeWidth / 2 + 1), (float)(this.mFuelGaugeWidth / 2 + 1));
        this.mPaint.setStyle(android.graphics.Paint$Style.STROKE);
        this.mPaint.setStrokeWidth((float)this.mFuelGaugeWidth);
        this.mPaint.setColor(this.mBackgroundColor);
        int i = (this.mLeftOriented) ? 90 : 270;
        a.drawArc(a1, (float)i, (float)180, false, this.mPaint);
        this.mPaint.setColor(this.mDefaultColor);
        int i0 = (int)(this.mValue / this.mMaxValue * (float)180);
        a.drawArc(a1, (float)((this.mLeftOriented) ? 90 : 450 - i0), (float)i0, false, this.mPaint);
        this.mPaint.setColor(-16777216);
        int i1 = 1;
        while(i1 < 4) {
            a.drawArc(a1, (float)((i1 * 45 + i) % 360), 2f, false, this.mPaint);
            i1 = i1 + 1;
        }
    }
    
    public void setLeftOriented(boolean b) {
        this.mLeftOriented = b;
    }
}
