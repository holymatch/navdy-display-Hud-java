package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class DialManagerView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler {
    final private static int CONNECTED_TIMEOUT = 30000;
    final public static int DIAL_CONNECTED = 5;
    final public static int DIAL_CONNECTING = 4;
    final public static int DIAL_CONNECTION_FAILED = 6;
    final public static int DIAL_PAIRED = 3;
    final public static int DIAL_PAIRING = 1;
    final public static int DIAL_SEARCHING = 2;
    final private static String EMPTY = "";
    final private static long POST_OTA_TIMEOUT;
    final private static int POS_DISMISS = 0;
    final private static int RESTART_INTERVAL = 2000;
    private static android.net.Uri firstLaunchVideoUri;
    private static android.net.Uri repairVideoUri;
    final private static com.navdy.service.library.log.Logger sLogger;
    private int batteryLevel;
    private int completeCount;
    private com.navdy.hud.app.ui.component.ChoiceLayout$IListener connectedChoiceListener;
    private Runnable connectedTimeoutRunnable;
    @InjectView(R.id.connectedView)
    public com.navdy.hud.app.ui.component.ConfirmationLayout connectedView;
    private String dialName;
    private boolean firstTime;
    private boolean foundDial;
    private android.os.Handler handler;
    private int incrementalVersion;
    private int initialState;
    @Inject
    com.navdy.hud.app.screen.DialManagerScreen$Presenter presenter;
    @InjectView(R.id.repair_text)
    public android.widget.TextView rePairTextView;
    private boolean registered;
    private Runnable restartScanRunnable;
    @Inject
    android.content.SharedPreferences sharedPreferences;
    private android.animation.ObjectAnimator spinner;
    private int state;
    @InjectView(R.id.videoContainer)
    public android.view.ViewGroup videoContainer;
    private android.net.Uri videoUri;
    @InjectView(R.id.videoView)
    public android.widget.VideoView videoView;
    private boolean videoViewInitialized;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.DialManagerView.class);
        POST_OTA_TIMEOUT = java.util.concurrent.TimeUnit.SECONDS.toMillis(15L);
    }
    
    public DialManagerView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public DialManagerView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public DialManagerView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.initialState = 0;
        this.state = 0;
        this.dialName = "Navdy Dial";
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.connectedTimeoutRunnable = (Runnable)new com.navdy.hud.app.view.DialManagerView$1(this);
        this.connectedChoiceListener = (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)new com.navdy.hud.app.view.DialManagerView$2(this);
        this.restartScanRunnable = (Runnable)new com.navdy.hud.app.view.DialManagerView$3(this);
        if (this.isInEditMode()) {
            com.navdy.hud.app.HudApplication.setContext(a);
        } else {
            mortar.Mortar.inject(a, this);
            if (firstLaunchVideoUri == null) {
                String s = com.navdy.hud.app.HudApplication.getAppContext().getPackageName();
                firstLaunchVideoUri = android.net.Uri.parse(new StringBuilder().append("android.resource://").append(s).append("/raw/dial_pairing_flow").toString());
                repairVideoUri = android.net.Uri.parse(new StringBuilder().append("android.resource://").append(s).append("/raw/dial_repairing_flow").toString());
            }
        }
        this.initFromAttributes(a, a0);
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static android.os.Handler access$100(com.navdy.hud.app.view.DialManagerView a) {
        return a.handler;
    }
    
    static int access$200(com.navdy.hud.app.view.DialManagerView a) {
        return a.completeCount;
    }
    
    static int access$204(com.navdy.hud.app.view.DialManagerView a) {
        int i = a.completeCount + 1;
        a.completeCount = i;
        return i;
    }
    
    static android.animation.ObjectAnimator access$300(com.navdy.hud.app.view.DialManagerView a) {
        return a.spinner;
    }
    
    private void clearConnectedTimeout() {
        this.presenter.getHandler().removeCallbacks(this.connectedTimeoutRunnable);
    }
    
    private void initFromAttributes(android.content.Context a, android.util.AttributeSet a0) {
        android.content.res.TypedArray a1 = a.getTheme().obtainStyledAttributes(a0, com.navdy.hud.app.R$styleable.DialManagerScreen, 0, 0);
        try {
            this.initialState = a1.getInt(0, 1);
            this.batteryLevel = a1.getInt(1, -1);
        } catch(Throwable a2) {
            a1.recycle();
            throw a2;
        }
        a1.recycle();
    }
    
    private boolean isInfoViewVisible() {
        boolean b = false;
        int i = this.connectedView.getVisibility();
        label2: {
            label0: {
                label1: {
                    if (i != 0) {
                        break label1;
                    }
                    if (this.connectedView.title4.getVisibility() == 0) {
                        break label0;
                    }
                }
                b = false;
                break label2;
            }
            b = true;
        }
        return b;
    }
    
    private void resetConnectedTimeout() {
        android.os.Handler a = this.presenter.getHandler();
        a.removeCallbacks(this.connectedTimeoutRunnable);
        a.postDelayed(this.connectedTimeoutRunnable, 30000L);
    }
    
    private void resetConnectedView() {
        this.connectedView.title1.setVisibility(8);
        this.connectedView.title1.setTextAppearance(this.getContext(), R.style.title1);
        this.connectedView.title2.setTextAppearance(this.getContext(), R.style.title2);
        this.connectedView.title3.setVisibility(8);
        this.connectedView.title4.setVisibility(8);
        this.connectedView.sideImage.setVisibility(8);
        ((android.view.ViewGroup$MarginLayoutParams)this.connectedView.choiceLayout.getLayoutParams()).bottomMargin = (int)this.getResources().getDimension(R.dimen.dial_pairing_status_margin_bottom);
        this.connectedView.choiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, java.util.Collections.EMPTY_LIST, 0, this.connectedChoiceListener);
    }
    
    private void setConnectingUI() {
        this.stopVideo();
        this.connectedView.screenImage.setImage(R.drawable.icon_dial_2, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
        this.connectedView.screenImage.setVisibility(0);
        this.connectedView.mainSection.setLayoutTransition(new android.animation.LayoutTransition());
        this.connectedView.mainSection.removeView((android.view.View)this.connectedView.titleContainer);
        this.connectedView.sideImage.setImageResource(R.drawable.icon_sm_spinner);
        this.connectedView.sideImage.setVisibility(0);
        this.startSpinner();
        this.connectedView.setVisibility(0);
    }
    
    private void setState(int i) {
        if (this.state != i) {
            sLogger.i(new StringBuilder().append("Switching to state:").append(i).toString());
        }
        this.state = i;
        switch(this.state) {
            case 6: {
                this.showConnectionFailedState();
                break;
            }
            case 5: {
                this.showDialConnectedState();
                break;
            }
            case 4: {
                this.setConnectingUI();
                break;
            }
            case 3: {
                this.showPairedState();
                break;
            }
            case 2: {
                this.showDialSearchingState();
                break;
            }
            case 1: {
                this.showDialPairingState();
                break;
            }
        }
    }
    
    private void setVideoURI(android.net.Uri a) {
        label1: try {
            label0: {
                if (a == null) {
                    break label0;
                }
                if (a.equals(this.videoUri)) {
                    break label0;
                }
                this.videoView.setVideoURI(a);
                break label1;
            }
            if (a == null) {
                this.videoView.stopPlayback();
            }
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
        this.videoUri = a;
    }
    
    private void showConnectionFailedState() {
        this.stopVideo();
        this.connectedView.screenImage.setImage(R.drawable.icon_dial_2, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
        this.connectedView.screenImage.setVisibility(0);
        this.connectedView.sideImage.setImageResource(R.drawable.icon_connection_failed);
        this.connectedView.sideImage.setVisibility(0);
        this.connectedView.setVisibility(0);
    }
    
    private void showDialConnectedState() {
        this.stopVideo();
        android.content.Context a = this.getContext();
        android.content.res.Resources a0 = a.getResources();
        this.connectedView.screenImage.setImage(R.drawable.icon_dial_2, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
        this.connectedView.sideImage.setImageResource(R.drawable.icon_sm_success);
        this.connectedView.sideImage.setVisibility(0);
        this.connectedView.titleContainer.setVisibility(0);
        this.connectedView.title1.setTextAppearance(a, R.style.title2);
        this.connectedView.title1.setText((CharSequence)a0.getString(R.string.dial_paired_text));
        this.connectedView.title2.setTextAppearance(a, R.style.title1);
        if (this.dialName == null) {
            this.connectedView.title2.setVisibility(8);
        } else {
            this.connectedView.title2.setText((CharSequence)this.dialName);
        }
        com.navdy.hud.app.device.dial.DialConstants$NotificationReason a1 = com.navdy.hud.app.device.dial.DialManager.getBatteryLevelCategory(this.batteryLevel);
        switch(com.navdy.hud.app.view.DialManagerView$9.$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$NotificationReason[a1.ordinal()]) {
            case 4: {
                StringBuilder a2 = new StringBuilder().append("  ");
                Object[] a3 = new Object[1];
                a3[0] = com.navdy.hud.app.device.dial.DialNotification.getDialBatteryLevel();
                android.text.SpannableStringBuilder a4 = new android.text.SpannableStringBuilder((CharSequence)a2.append(a0.getString(R.string.dial_battery_level_extremely_low, a3)).toString());
                a4.setSpan(new android.text.style.ImageSpan(a, R.drawable.icon_dial_batterylow_copy, 1), 0, 1, 33);
                this.connectedView.title3.setText((CharSequence)a4);
                break;
            }
            case 3: {
                StringBuilder a5 = new StringBuilder().append("  ");
                Object[] a6 = new Object[1];
                a6[0] = com.navdy.hud.app.device.dial.DialNotification.getDialBatteryLevel();
                android.text.SpannableStringBuilder a7 = new android.text.SpannableStringBuilder((CharSequence)a5.append(a0.getString(R.string.dial_battery_level_very_low, a6)).toString());
                a7.setSpan(new android.text.style.ImageSpan(a, R.drawable.icon_dial_batterylow_copy, 1), 0, 1, 33);
                this.connectedView.title3.setText((CharSequence)a7);
                break;
            }
            case 2: {
                StringBuilder a8 = new StringBuilder().append("  ");
                Object[] a9 = new Object[1];
                a9[0] = com.navdy.hud.app.device.dial.DialNotification.getDialBatteryLevel();
                android.text.SpannableStringBuilder a10 = new android.text.SpannableStringBuilder((CharSequence)a8.append(a0.getString(R.string.dial_battery_level_low, a9)).toString());
                a10.setSpan(new android.text.style.ImageSpan(a, R.drawable.icon_dial_batterylow_2_copy, 1), 0, 1, 33);
                this.connectedView.title3.setText((CharSequence)a10);
                break;
            }
            default: {
                this.connectedView.title3.setText((CharSequence)a0.getString(R.string.dial_battery_level_ok));
            }
        }
        int i = this.incrementalVersion;
        String s = (i != -1) ? new StringBuilder().append("1.0.").append(i).toString() : "";
        android.widget.TextView a11 = this.connectedView.title4;
        Object[] a12 = new Object[1];
        a12[0] = s;
        a11.setText((CharSequence)a0.getString(R.string.dial_firmware_rev, a12));
        this.connectedView.title4.setVisibility(0);
        ((android.view.ViewGroup$MarginLayoutParams)this.connectedView.choiceLayout.getLayoutParams()).bottomMargin = (int)a0.getDimension(R.dimen.dial_connected_status_margin_bottom);
        java.util.ArrayList a13 = new java.util.ArrayList(2);
        ((java.util.List)a13).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice(a0.getString(R.string.dial_paired_choice_dismiss), 0));
        this.connectedView.choiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a13, 0, this.connectedChoiceListener);
        this.connectedView.setVisibility(0);
        if (this.presenter != null) {
            android.os.Handler a14 = this.presenter.getHandler();
            a14.removeCallbacks(this.connectedTimeoutRunnable);
            a14.postDelayed(this.connectedTimeoutRunnable, 30000L);
        }
    }
    
    private void showDialPairingState() {
        this.firstTime = com.navdy.hud.app.device.dial.DialManager.getInstance().getBondedDialCount() == 0;
        this.connectedView.setVisibility(8);
        this.resetConnectedView();
        if (!this.videoViewInitialized) {
            this.videoView.setOnPreparedListener((android.media.MediaPlayer$OnPreparedListener)new com.navdy.hud.app.view.DialManagerView$5(this));
            this.videoView.setOnCompletionListener((android.media.MediaPlayer$OnCompletionListener)new com.navdy.hud.app.view.DialManagerView$6(this));
            this.videoViewInitialized = true;
        }
        this.videoContainer.setVisibility(0);
        android.content.SharedPreferences a = this.sharedPreferences;
        label1: {
            label0: {
                if (a == null) {
                    break label0;
                }
                if (!this.sharedPreferences.getBoolean("first_run_dial_video_shown", false)) {
                    break label0;
                }
                this.setVideoURI(repairVideoUri);
                this.rePairTextView.setText((CharSequence)this.getContext().getString(R.string.navdy_dial_repair_text));
                this.rePairTextView.setVisibility(0);
                break label1;
            }
            this.setVideoURI(firstLaunchVideoUri);
            this.rePairTextView.setVisibility(8);
        }
        sLogger.v("starting scanning");
        com.navdy.hud.app.device.dial.DialManager.getInstance().startScan();
    }
    
    private void showDialSearchingState() {
        this.firstTime = false;
        this.resetConnectedView();
        this.setConnectingUI();
        sLogger.v("starting scanning");
        com.navdy.hud.app.device.dial.DialManager.getInstance().startScan();
        this.handler.postDelayed(this.restartScanRunnable, POST_OTA_TIMEOUT);
    }
    
    private void showPairedState() {
        sLogger.v(new StringBuilder().append("showPairingSuccessUI=").append(this.dialName).toString());
        android.content.Context a = this.getContext();
        this.stopVideo();
        android.content.res.Resources a0 = a.getResources();
        this.connectedView.mainSection.removeView((android.view.View)this.connectedView.titleContainer);
        android.animation.LayoutTransition a1 = new android.animation.LayoutTransition();
        this.connectedView.mainSection.setLayoutTransition(a1);
        this.connectedView.screenImage.setImage(R.drawable.icon_dial_2, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
        this.connectedView.screenImage.setVisibility(0);
        this.connectedView.sideImage.setImageResource(R.drawable.icon_sm_success);
        this.connectedView.sideImage.setVisibility(0);
        this.connectedView.title1.setVisibility(8);
        this.connectedView.title2.setTextAppearance(a, R.style.Glances_1_bold);
        this.connectedView.title2.setText((CharSequence)a0.getString(R.string.navdy_dial));
        this.dialName = com.navdy.hud.app.device.dial.DialNotification.getDialAddressPart(this.dialName);
        if (android.text.TextUtils.isEmpty((CharSequence)this.dialName)) {
            this.connectedView.title3.setVisibility(8);
        } else {
            android.widget.TextView a2 = this.connectedView.title3;
            Object[] a3 = new Object[1];
            a3[0] = this.dialName;
            a2.setText((CharSequence)a0.getString(R.string.navdy_dial_name, a3));
            this.connectedView.title3.setVisibility(0);
        }
        this.connectedView.titleContainer.setVisibility(0);
        this.connectedView.mainSection.addView((android.view.View)this.connectedView.titleContainer);
        this.connectedView.setVisibility(0);
    }
    
    private void showPairingSuccessUI(String s) {
        this.dialName = s;
        this.setState(3);
        com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus().post(new com.navdy.hud.app.event.LocalSpeechRequest(new com.navdy.service.library.events.audio.SpeechRequest$Builder().words(com.navdy.hud.app.framework.voice.TTSUtils.TTS_DIAL_CONNECTED).language(java.util.Locale.getDefault().toLanguageTag()).category(com.navdy.service.library.events.audio.SpeechRequest$Category.SPEECH_NOTIFICATION).build()));
        this.handler.postDelayed((Runnable)new com.navdy.hud.app.view.DialManagerView$8(this), 5000L);
    }
    
    private void startSpinner() {
        if (this.spinner == null) {
            android.widget.ImageView a = this.connectedView.sideImage;
            android.util.Property a0 = android.view.View.ROTATION;
            float[] a1 = new float[1];
            a1[0] = 360f;
            this.spinner = android.animation.ObjectAnimator.ofFloat(a, a0, a1);
            this.spinner.setDuration(500L);
            this.spinner.setInterpolator((android.animation.TimeInterpolator)new android.view.animation.AccelerateDecelerateInterpolator());
            this.spinner.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.view.DialManagerView$7(this));
        }
        if (!this.spinner.isRunning()) {
            this.spinner.start();
        }
    }
    
    private void stopSpinner() {
        if (this.spinner != null) {
            this.spinner.removeAllListeners();
            this.spinner.cancel();
            this.connectedView.sideImage.setRotation(0.0f);
            this.spinner = null;
        }
    }
    
    private void stopVideo() {
        if (this.videoView.isPlaying()) {
            this.setVideoURI((android.net.Uri)null);
            sLogger.v("stopped video");
        } else {
            sLogger.v("video not playing");
        }
        this.videoContainer.setVisibility(8);
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return null;
    }
    
    protected void onAttachedToWindow() {
        sLogger.v("onAttachToWindow");
        super.onAttachedToWindow();
        if (this.presenter == null) {
            this.setState(this.initialState);
        } else {
            com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(true);
            com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(false);
            this.presenter.takeView(this);
            this.presenter.getBus().register(this);
            this.registered = true;
        }
    }
    
    protected void onDetachedFromWindow() {
        sLogger.v("onDetachToWindow");
        this.clearConnectedTimeout();
        if (this.videoContainer.getVisibility() == 0) {
            sLogger.v("stop dial scan");
            com.navdy.hud.app.device.dial.DialManager.getInstance().stopScan(false);
            this.stopVideo();
        }
        com.navdy.hud.app.framework.toast.ToastManager.getInstance().disableToasts(false);
        com.navdy.hud.app.framework.notifications.NotificationManager.getInstance().enableNotifications(true);
        super.onDetachedFromWindow();
        if (this.presenter != null) {
            this.presenter.dropView((android.view.View)this);
            if (this.registered) {
                this.presenter.getBus().unregister(this);
            }
        }
        this.stopSpinner();
        if (!this.isInfoViewVisible()) {
            com.navdy.hud.app.device.dial.DialManagerHelper.sendLocalyticsEvent(this.presenter.getHandler(), this.firstTime, this.foundDial, 10000, true);
        }
    }
    
    public void onDialConnectionStatus(com.navdy.hud.app.device.dial.DialConstants$DialConnectionStatus a) {
        if (!this.isInfoViewVisible()) {
            sLogger.v(new StringBuilder().append("onDialConnectionStatus:").append(a.status).append(" name:").append(a.dialName).toString());
            if (this.foundDial) {
                sLogger.v("already found dial");
            } else {
                com.navdy.hud.app.device.dial.DialManager a0 = com.navdy.hud.app.device.dial.DialManager.getInstance();
                this.handler.removeCallbacks(this.restartScanRunnable);
                this.stopSpinner();
                switch(com.navdy.hud.app.view.DialManagerView$9.$SwitchMap$com$navdy$hud$app$device$dial$DialConstants$DialConnectionStatus$Status[a.status.ordinal()]) {
                    case 5: {
                        sLogger.v("no dials found");
                        this.showDialPairing();
                        break;
                    }
                    case 4: {
                        sLogger.v(new StringBuilder().append("dial connection failed:").append(a.dialName).toString());
                        this.setState(6);
                        this.handler.postDelayed(this.restartScanRunnable, 2000L);
                        break;
                    }
                    case 3: {
                        sLogger.v(new StringBuilder().append("dial connection disconnected:").append(a.dialName).toString());
                        this.handler.post(this.restartScanRunnable);
                        break;
                    }
                    case 2: {
                        sLogger.v(new StringBuilder().append("dial connnecting:").append(a.dialName).toString());
                        this.stopVideo();
                        this.setConnectingUI();
                        break;
                    }
                    case 1: {
                        sLogger.v(new StringBuilder().append("dial connected:").append(a.dialName).toString());
                        this.foundDial = true;
                        a0.stopScan(false);
                        com.navdy.hud.app.framework.toast.ToastManager a1 = com.navdy.hud.app.framework.toast.ToastManager.getInstance();
                        a1.dismissCurrentToast();
                        a1.clearAllPendingToast();
                        a1.disableToasts(false);
                        sLogger.v("show connected state");
                        this.showPairingSuccessUI(a.dialName);
                        break;
                    }
                }
            }
        }
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        ((com.navdy.hud.app.view.MaxWidthLinearLayout)this.findViewById(R.id.infoContainer)).setMaxWidth(this.getContext().getResources().getDimensionPixelOffset(R.dimen.dial_message_max_width));
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        return false;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        switch(com.navdy.hud.app.view.DialManagerView$9.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
            case 6: {
                this.clearConnectedTimeout();
                sLogger.v("pwrbtn long press in connected mode");
                com.navdy.hud.app.device.dial.DialManager.getInstance().forgetAllDials(true, (com.navdy.hud.app.device.dial.DialManagerHelper$IDialForgotten)new com.navdy.hud.app.view.DialManagerView$4(this));
                break;
            }
            case 5: {
                if (!this.isInfoViewVisible()) {
                    break;
                }
                this.clearConnectedTimeout();
                this.connectedView.choiceLayout.executeSelectedItem(true);
                break;
            }
            case 4: {
                if (!this.isInfoViewVisible()) {
                    break;
                }
                this.resetConnectedTimeout();
                this.connectedView.choiceLayout.moveSelectionRight();
                break;
            }
            case 3: {
                if (!this.isInfoViewVisible()) {
                    break;
                }
                this.resetConnectedTimeout();
                this.connectedView.choiceLayout.moveSelectionLeft();
                break;
            }
            case 1: case 2: {
                this.clearConnectedTimeout();
                this.presenter.dismiss();
                break;
            }
        }
        return true;
    }
    
    public void onShowToast(com.navdy.hud.app.framework.toast.ToastManager$ShowToast a) {
        sLogger.v(new StringBuilder().append("got toast:").append(a.name).toString());
        if (android.text.TextUtils.equals((CharSequence)a.name, (CharSequence)"dial-connect") && this.presenter != null) {
            this.presenter.dismiss();
        }
    }
    
    public void showDialConnected() {
        this.dialName = com.navdy.hud.app.device.dial.DialNotification.getDialName();
        this.batteryLevel = com.navdy.hud.app.device.dial.DialManager.getInstance().getLastKnownBatteryLevel();
        this.incrementalVersion = com.navdy.hud.app.device.dial.DialManager.getInstance().getDialFirmwareUpdater().getVersions().dial.incrementalVersion;
        this.setState(5);
    }
    
    public void showDialPairing() {
        this.setState(1);
    }
    
    public void showDialSearching(String s) {
        this.dialName = s;
        this.setState(2);
    }
}
