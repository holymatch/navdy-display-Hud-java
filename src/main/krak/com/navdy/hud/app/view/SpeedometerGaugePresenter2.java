package com.navdy.hud.app.view;

public class SpeedometerGaugePresenter2 extends com.navdy.hud.app.view.GaugeViewPresenter implements com.navdy.hud.app.view.SerialValueAnimator$SerialValueAnimatorAdapter {
    final private static boolean ALWAYS_SHOW_SPEED_LIMIT = true;
    final public static String ID = "SPEEDO_METER_WIDGET_2";
    final private static int MAX_ANIMATION_DURATION = 100;
    final public static int MAX_SPEED = 158;
    final private static int MIN_ANIMATION_DURATION = 20;
    final public static int RESAMPLE_INTERVAL = 1000;
    final public static int SAFE = 0;
    final private static int SPEED_LIMIT_ANIMATION_DELAY = 500;
    final private static int SPEED_LIMIT_ANIMATION_DURATION = 1000;
    final public static int SPEED_LIMIT_EXCEEDED = 1;
    final public static int SPEED_LIMIT_TRESHOLD_EXCEEDED = 2;
    private int animationDuration;
    private android.os.Handler handler;
    final private String kmhString;
    private int lastSpeed;
    private long lastSpeedSampleArrivalTime;
    private com.navdy.hud.app.view.SerialValueAnimator mSerialValueAnimator;
    private int mSpeed;
    private int mSpeedLimit;
    private com.navdy.hud.app.view.drawable.SpeedoMeterGaugeDrawable2 mSpeedoMeterGaugeDrawable;
    final private String mphString;
    private boolean showingSpeedLimitAnimation;
    private int speedLimitAlpha;
    private Runnable speedLimitFadeOutAnimationRunnable;
    private android.animation.ValueAnimator speedLimitFadeOutAnimator;
    private boolean speedLimitMarkerNeedsTobeRemoved;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    final private String speedoMeterWidgetName;
    private int textSize2Chars;
    private int textSize3Chars;
    private boolean updateText;
    
    public SpeedometerGaugePresenter2(android.content.Context a) {
        this.mSpeedLimit = 0;
        this.speedLimitMarkerNeedsTobeRemoved = false;
        this.updateText = true;
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        this.lastSpeed = -2147483648;
        this.lastSpeedSampleArrivalTime = 0L;
        this.animationDuration = 100;
        this.handler = new android.os.Handler();
        this.mSpeedoMeterGaugeDrawable = new com.navdy.hud.app.view.drawable.SpeedoMeterGaugeDrawable2(a, 0, R.array.smart_dash_speedo_meter_color_table, R.dimen.speedo_meter_guage_inner_bar_width);
        this.mSpeedoMeterGaugeDrawable.setMaxGaugeValue(158f);
        this.mSerialValueAnimator = new com.navdy.hud.app.view.SerialValueAnimator((com.navdy.hud.app.view.SerialValueAnimator$SerialValueAnimatorAdapter)this, 100);
        this.speedLimitFadeOutAnimator = new android.animation.ValueAnimator();
        this.speedLimitFadeOutAnimationRunnable = (Runnable)new com.navdy.hud.app.view.SpeedometerGaugePresenter2$1(this);
        android.content.res.Resources a0 = a.getResources();
        this.mphString = a0.getString(R.string.mph);
        this.kmhString = a0.getString(R.string.kilometers_per_hour);
        this.textSize2Chars = a0.getDimensionPixelSize(R.dimen.tachometer_guage_speed_text_size);
        this.textSize3Chars = a0.getDimensionPixelSize(R.dimen.tachometer_guage_speed_text_size_2);
        this.speedoMeterWidgetName = a0.getString(R.string.gauge_speedo_meter);
    }
    
    static android.animation.ValueAnimator access$000(com.navdy.hud.app.view.SpeedometerGaugePresenter2 a) {
        return a.speedLimitFadeOutAnimator;
    }
    
    static int access$102(com.navdy.hud.app.view.SpeedometerGaugePresenter2 a, int i) {
        a.speedLimitAlpha = i;
        return i;
    }
    
    static boolean access$202(com.navdy.hud.app.view.SpeedometerGaugePresenter2 a, boolean b) {
        a.showingSpeedLimitAnimation = b;
        return b;
    }
    
    private void setSpeedValueInternal(float f) {
        if (this.mSpeed < this.mSpeedLimit) {
            if (!this.showingSpeedLimitAnimation && this.speedLimitMarkerNeedsTobeRemoved) {
                this.showSpeedLimitTextAndFadeOut();
            }
        } else {
            this.showSpeedLimitText();
        }
        this.reDraw();
    }
    
    private void showSpeedLimitText() {
        this.handler.removeCallbacks(this.speedLimitFadeOutAnimationRunnable);
        this.speedLimitAlpha = 255;
    }
    
    private void showSpeedLimitTextAndFadeOut() {
        this.speedLimitAlpha = 255;
    }
    
    public void animationComplete(float f) {
        this.updateText = true;
        this.setSpeedValueInternal(f);
    }
    
    public android.graphics.drawable.Drawable getDrawable() {
        return this.mSpeedoMeterGaugeDrawable;
    }
    
    public float getValue() {
        return (float)this.mSpeed;
    }
    
    public String getWidgetIdentifier() {
        return "SPEEDO_METER_WIDGET_2";
    }
    
    public String getWidgetName() {
        return this.speedoMeterWidgetName;
    }
    
    public void setSpeed(int i) {
        if (i != -1) {
            long j = android.os.SystemClock.elapsedRealtime();
            long j0 = j - this.lastSpeedSampleArrivalTime;
            this.lastSpeedSampleArrivalTime = j;
            if (j0 <= 1000L) {
                this.animationDuration = (int)Math.max(20L, Math.min((long)this.animationDuration, j0));
            } else {
                this.animationDuration = 100;
            }
            if (this.lastSpeed != i) {
                this.lastSpeed = i;
                if (i > 158) {
                    i = 158;
                }
                this.mSerialValueAnimator.setDuration(this.animationDuration);
                this.mSerialValueAnimator.setValue((float)i);
            }
        }
    }
    
    public void setSpeedLimit(int i) {
        if (i != this.mSpeedLimit) {
            this.mSpeedLimit = i;
            this.showSpeedLimitText();
            if (this.mSpeed < this.mSpeedLimit) {
                this.showSpeedLimitTextAndFadeOut();
            }
            this.reDraw();
        }
    }
    
    public void setValue(float f) {
        int i = (int)f;
        if (i != this.mSpeed) {
            this.mSpeed = i;
            this.updateText = false;
            this.setSpeedValueInternal((float)this.mSpeed);
        }
    }
    
    public void setView(com.navdy.hud.app.view.DashboardWidgetView a) {
        if (a != null) {
            a.setContentView(R.layout.speedo_meter_gauge_2);
        }
        super.setView(a);
        if (this.mGaugeView != null) {
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                android.util.TypedValue a0 = new android.util.TypedValue();
                this.mGaugeView.getResources().getValue(R.dimen.speedometer_text_letter_spacing, a0, true);
                float f = a0.getFloat();
                this.mGaugeView.getValueTextView().setLetterSpacing(f);
            }
            this.reDraw();
        }
    }
    
    public void updateGauge() {
        if (this.mGaugeView != null) {
            String s = null;
            int i = 0;
            int i0 = 0;
            int i1 = 0;
            boolean b = false;
            android.content.res.Resources a = this.mGaugeView.getResources();
            this.mSpeedoMeterGaugeDrawable.setGaugeValue((float)this.mSpeed);
            this.mSpeedoMeterGaugeDrawable.setSpeedLimit(this.mSpeedLimit);
            com.navdy.hud.app.manager.SpeedManager$SpeedUnit a0 = this.speedManager.getSpeedUnit();
            String dummy = this.mphString;
            if (com.navdy.hud.app.view.SpeedometerGaugePresenter2$2.$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[a0.ordinal()] == 2) {
                s = this.kmhString;
                i = 13;
            } else {
                s = this.mphString;
                i = 8;
            }
            if (this.updateText) {
                this.mGaugeView.getValueTextView().setTextSize(2, (this.mSpeed < 100) ? (float)this.textSize2Chars : (float)this.textSize3Chars);
                this.mGaugeView.setValueText((CharSequence)Integer.toString(this.mSpeed));
            }
            if (this.mSpeedLimit != 0) {
                i0 = (this.mSpeed <= this.mSpeedLimit) ? 0 : 1;
                if (this.mSpeed > this.mSpeedLimit + i) {
                    i0 = 2;
                }
            } else {
                i0 = 0;
            }
            switch(i0) {
                case 2: {
                    i1 = R.color.speed_very_fast_warning_color;
                    b = true;
                    break;
                }
                case 1: {
                    i1 = R.color.cyan;
                    b = true;
                    break;
                }
                case 0: {
                    i1 = 17170443;
                    b = false;
                    break;
                }
                default: {
                    i1 = 17170443;
                    b = false;
                }
            }
            this.mGaugeView.getUnitTextView().setTextColor(a.getColor(i1));
            this.mSpeedoMeterGaugeDrawable.setState(i0);
            this.mSpeedoMeterGaugeDrawable.speedLimitTextAlpha = this.speedLimitAlpha;
            if (b) {
                com.navdy.hud.app.view.GaugeView a1 = this.mGaugeView;
                Object[] a2 = new Object[2];
                a2[0] = Integer.valueOf(this.mSpeedLimit);
                a2[1] = s;
                a1.setUnitText((CharSequence)a.getString(R.string.speed_limit_with_unit, a2));
            } else {
                this.mGaugeView.setUnitText((CharSequence)s);
            }
        }
    }
}
