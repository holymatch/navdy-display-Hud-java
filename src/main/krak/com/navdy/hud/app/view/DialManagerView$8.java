package com.navdy.hud.app.view;

class DialManagerView$8 implements Runnable {
    final com.navdy.hud.app.view.DialManagerView this$0;
    
    DialManagerView$8(com.navdy.hud.app.view.DialManagerView a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.screen.DialManagerScreen$Presenter a = this.this$0.presenter;
        label1: {
            label0: {
                if (a == null) {
                    break label0;
                }
                if (!this.this$0.presenter.isActive()) {
                    break label0;
                }
                this.this$0.presenter.dismiss();
                break label1;
            }
            com.navdy.hud.app.view.DialManagerView.access$000().v("showPairingSuccessUI:hide not active");
        }
    }
}
