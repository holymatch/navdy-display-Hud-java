package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class GestureVideoCaptureView$$ViewInjector {
    public GestureVideoCaptureView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.GestureVideoCaptureView a0, Object a1) {
        a0.sideImageView = (android.widget.ImageView)a.findRequiredView(a1, R.id.sideImage, "field 'sideImageView'");
        a0.mainImageView = (android.widget.ImageView)a.findRequiredView(a1, R.id.image, "field 'mainImageView'");
        a0.mainText = (android.widget.TextView)a.findRequiredView(a1, R.id.title1, "field 'mainText'");
        a0.secondaryText = (android.widget.TextView)a.findRequiredView(a1, R.id.title3, "field 'secondaryText'");
        a0.mChoiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout)a.findRequiredView(a1, R.id.choiceLayout, "field 'mChoiceLayout'");
    }
    
    public static void reset(com.navdy.hud.app.view.GestureVideoCaptureView a) {
        a.sideImageView = null;
        a.mainImageView = null;
        a.mainText = null;
        a.secondaryText = null;
        a.mChoiceLayout = null;
    }
}
