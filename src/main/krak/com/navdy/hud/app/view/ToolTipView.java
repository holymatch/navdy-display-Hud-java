package com.navdy.hud.app.view;

public class ToolTipView extends android.widget.LinearLayout {
    final private static com.navdy.service.library.log.Logger sLogger;
    private int iconWidth;
    private int margin;
    private int maxIcons;
    private int maxTextWidth;
    private int minTextWidth;
    private int padding;
    private int sideMargin;
    @InjectView(R.id.tooltip_container)
    android.view.ViewGroup toolTipContainer;
    @InjectView(R.id.tooltip_scrim)
    android.view.View toolTipScrim;
    @InjectView(R.id.tooltip_text)
    android.widget.TextView toolTipTextView;
    @InjectView(R.id.tooltip_triangle)
    android.view.View toolTipTriangle;
    private int totalWidth;
    private int triangleWidth;
    private android.widget.TextView widthCalcTextView;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.view.ToolTipView.class);
    }
    
    public ToolTipView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public ToolTipView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public ToolTipView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.init();
    }
    
    private void init() {
        android.content.Context a = this.getContext();
        this.widthCalcTextView = new android.widget.TextView(a);
        this.widthCalcTextView.setTextAppearance(a, R.style.tool_tip);
        android.content.res.Resources a0 = a.getResources();
        this.maxTextWidth = a0.getDimensionPixelSize(R.dimen.tool_tip_max_text_width);
        this.minTextWidth = a0.getDimensionPixelSize(R.dimen.tool_tip_min_text_width);
        this.padding = a0.getDimensionPixelSize(R.dimen.tool_tip_padding);
        this.sideMargin = a0.getDimensionPixelSize(R.dimen.tool_tip_margin);
        this.triangleWidth = a0.getDimensionPixelSize(R.dimen.tool_tip_triangle_width);
        this.setOrientation(1);
        android.view.LayoutInflater.from(a).inflate(R.layout.tooltip_lyt, (android.view.ViewGroup)this, true);
        butterknife.ButterKnife.inject((android.view.View)this);
    }
    
    public void hide() {
        this.setVisibility(8);
    }
    
    public void setParams(int i, int i0, int i1) {
        this.iconWidth = i;
        this.margin = i0;
        this.maxIcons = i1;
        this.totalWidth = i * i1 + (i1 - 1) * i0;
    }
    
    public void show(int i, String s) {
        int i0 = 0;
        boolean b = false;
        int i1 = 0;
        android.text.TextPaint a = this.widthCalcTextView.getPaint();
        if (new android.text.StaticLayout((CharSequence)s, a, this.maxTextWidth, android.text.Layout$Alignment.ALIGN_NORMAL, 1f, 0.0f, false).getLineCount() != 1) {
            i0 = this.maxTextWidth;
            b = true;
        } else {
            i0 = (int)a.measureText(s);
            if (i0 >= this.minTextWidth) {
                b = false;
            } else {
                i0 = this.minTextWidth;
                b = false;
            }
        }
        android.view.ViewGroup$MarginLayoutParams a0 = (android.view.ViewGroup$MarginLayoutParams)this.getLayoutParams();
        int i2 = i0 + this.padding * 2;
        a0.width = i2;
        int i3 = this.iconWidth * i + this.margin * i + this.iconWidth / 2;
        int i4 = this.triangleWidth / 2;
        switch(i) {
            case 2: case 3: {
                int i5 = this.totalWidth - i3 - this.sideMargin;
                i1 = (i2 / 2 <= i5) ? i3 - i2 / 2 : i3 - (i2 - i5);
                break;
            }
            case 0: case 1: {
                i1 = (i2 / 2 <= i3 - this.sideMargin) ? i3 - i2 / 2 : this.sideMargin;
                break;
            }
            default: {
                i1 = 0;
            }
        }
        ((android.view.ViewGroup$MarginLayoutParams)this.toolTipTriangle.getLayoutParams()).leftMargin = i3 - i4 - i1;
        this.toolTipTextView.setText((CharSequence)s);
        this.toolTipTextView.requestLayout();
        if (b) {
            this.toolTipScrim.setVisibility(0);
        } else {
            this.toolTipScrim.setVisibility(8);
        }
        this.setX((float)i1);
        this.requestLayout();
        this.setVisibility(0);
    }
}
