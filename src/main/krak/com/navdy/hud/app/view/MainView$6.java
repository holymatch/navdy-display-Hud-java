package com.navdy.hud.app.view;

class MainView$6 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.view.MainView this$0;
    final android.view.ViewGroup val$notificationExtensionView;
    final android.view.View val$view;
    
    MainView$6(com.navdy.hud.app.view.MainView a, android.view.ViewGroup a0, android.view.View a1) {
        super();
        this.this$0 = a;
        this.val$notificationExtensionView = a0;
        this.val$view = a1;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        super.onAnimationEnd(a);
        if (this.val$view instanceof com.navdy.hud.app.ui.activity.Main$INotificationExtensionView) {
            ((com.navdy.hud.app.ui.activity.Main$INotificationExtensionView)this.val$view).onStart();
        }
    }
    
    public void onAnimationStart(android.animation.Animator a) {
        super.onAnimationStart(a);
        this.val$notificationExtensionView.setVisibility(0);
        com.navdy.hud.app.view.MainView.access$1102(this.this$0, true);
    }
}
