package com.navdy.hud.app.view;

public class SpeedLimitSignView extends android.widget.FrameLayout {
    @InjectView(R.id.speed_limit_sign_eu)
    protected android.view.ViewGroup euSpeedLimitSignView;
    private int speedLimit;
    private android.widget.TextView speedLimitTextView;
    private com.navdy.hud.app.manager.SpeedManager$SpeedUnit speedLimitUnit;
    @InjectView(R.id.speed_limit_sign_us)
    protected android.view.ViewGroup usSpeedLimitSignView;
    
    public SpeedLimitSignView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public SpeedLimitSignView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, -1);
    }
    
    public SpeedLimitSignView(android.content.Context a, android.util.AttributeSet a0, int i) {
        this(a, a0, i, -1);
    }
    
    public SpeedLimitSignView(android.content.Context a, android.util.AttributeSet a0, int i, int i0) {
        super(a, a0, i, i0);
        this.speedLimitUnit = null;
        com.navdy.hud.app.view.SpeedLimitSignView.inflate(this.getContext(), R.layout.speed_sign_eu, (android.view.ViewGroup)this);
        com.navdy.hud.app.view.SpeedLimitSignView.inflate(this.getContext(), R.layout.speed_limit_sign_us, (android.view.ViewGroup)this);
        butterknife.ButterKnife.inject(this, (android.view.View)this);
    }
    
    public com.navdy.hud.app.manager.SpeedManager$SpeedUnit getSpeedLimitUnit() {
        return this.speedLimitUnit;
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.setSpeedLimitUnit(this.speedLimitUnit);
    }
    
    public void setSpeedLimit(int i) {
        if (this.speedLimit != i) {
            this.speedLimit = i;
            if (this.speedLimitTextView != null) {
                this.speedLimitTextView.setText((CharSequence)Integer.toString(i));
            }
        }
    }
    
    public void setSpeedLimitUnit(com.navdy.hud.app.manager.SpeedManager$SpeedUnit a) {
        if (this.speedLimitUnit != a) {
            this.speedLimitUnit = a;
            int i = com.navdy.hud.app.view.SpeedLimitSignView$1.$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[this.speedLimitUnit.ordinal()];
            android.view.ViewGroup a0 = null;
            switch(i) {
                case 2: {
                    a0 = this.euSpeedLimitSignView;
                    this.usSpeedLimitSignView.setVisibility(8);
                    break;
                }
                case 1: {
                    a0 = this.usSpeedLimitSignView;
                    this.euSpeedLimitSignView.setVisibility(8);
                    break;
                }
            }
            a0.setVisibility(0);
            this.speedLimitTextView = (android.widget.TextView)a0.findViewById(R.id.txt_speed_limit);
            this.setSpeedLimit(this.speedLimit);
        }
    }
}
