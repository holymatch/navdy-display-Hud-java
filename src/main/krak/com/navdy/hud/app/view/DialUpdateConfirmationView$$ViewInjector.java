package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

public class DialUpdateConfirmationView$$ViewInjector {
    public DialUpdateConfirmationView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.view.DialUpdateConfirmationView a0, Object a1) {
        a0.mScreenTitleText = (android.widget.TextView)a.findRequiredView(a1, R.id.mainTitle, "field 'mScreenTitleText'");
        a0.mMainTitleText = (android.widget.TextView)a.findRequiredView(a1, R.id.title2, "field 'mMainTitleText'");
        a0.mInfoText = (android.widget.TextView)a.findRequiredView(a1, R.id.title3, "field 'mInfoText'");
        a0.mSummaryText = (android.widget.TextView)a.findRequiredView(a1, R.id.title4, "field 'mSummaryText'");
        a0.mChoiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout)a.findRequiredView(a1, R.id.choiceLayout, "field 'mChoiceLayout'");
        a0.mIcon = (android.widget.ImageView)a.findRequiredView(a1, R.id.image, "field 'mIcon'");
        a0.mRightSwipe = (android.widget.ImageView)a.findRequiredView(a1, R.id.rightSwipe, "field 'mRightSwipe'");
        a0.mLefttSwipe = (android.widget.ImageView)a.findRequiredView(a1, R.id.leftSwipe, "field 'mLefttSwipe'");
        a0.mainSection = (android.widget.RelativeLayout)a.findRequiredView(a1, R.id.mainSection, "field 'mainSection'");
    }
    
    public static void reset(com.navdy.hud.app.view.DialUpdateConfirmationView a) {
        a.mScreenTitleText = null;
        a.mMainTitleText = null;
        a.mInfoText = null;
        a.mSummaryText = null;
        a.mChoiceLayout = null;
        a.mIcon = null;
        a.mRightSwipe = null;
        a.mLefttSwipe = null;
        a.mainSection = null;
    }
}
