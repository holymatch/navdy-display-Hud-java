package com.navdy.hud.app.view;
import com.navdy.hud.app.R;

abstract public class ChoicesView extends android.widget.RelativeLayout implements com.navdy.hud.app.manager.InputManager$IInputHandler, com.navdy.hud.app.gesture.GestureDetector$GestureListener {
    protected boolean animateItemOnExecution;
    protected boolean animateItemOnSelection;
    protected com.navdy.hud.app.gesture.GestureDetector detector;
    protected android.view.animation.Animation executeAnimation;
    protected int lastTouchX;
    protected int lastTouchY;
    private android.view.animation.Animation$AnimationListener listener;
    protected com.navdy.service.library.log.Logger logger;
    protected int selectedItem;
    protected boolean wrapAround;
    
    public ChoicesView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public ChoicesView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public ChoicesView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.logger = new com.navdy.service.library.log.Logger((this).getClass());
        this.selectedItem = -1;
        this.wrapAround = false;
        this.animateItemOnExecution = true;
        this.animateItemOnSelection = true;
        this.listener = (android.view.animation.Animation$AnimationListener)new com.navdy.hud.app.view.ChoicesView$3(this);
        this.detector = new com.navdy.hud.app.gesture.GestureDetector((com.navdy.hud.app.gesture.GestureDetector$GestureListener)this);
    }
    
    private android.animation.Animator buildBounceAnim(float f, float f0) {
        android.animation.AnimatorSet a = new android.animation.AnimatorSet();
        float[] a0 = new float[1];
        a0[0] = f;
        android.animation.AnimatorSet$Builder a1 = a.play((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(this, "value", a0));
        float[] a2 = new float[1];
        a2[0] = f0;
        a1.before((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(this, "value", a2));
        return a;
    }
    
    private void moveSelectionLeft() {
        int i = this.getItemCount();
        if (this.selectedItem > 0 && i > 0) {
            this.setSelectedItem((this.selectedItem - 1 + i) % i);
        }
    }
    
    private void moveSelectionRight() {
        if (this.selectedItem != -1) {
            int i = this.getItemCount();
            if (this.selectedItem + 1 < i) {
                this.setSelectedItem((this.selectedItem + 1) % i);
            }
        }
    }
    
    abstract protected void animateDown();
    
    
    abstract protected void animateUp();
    
    
    public void executeSelectedItem(boolean b) {
        if (this.selectedItem != -1 && this.getItemCount() > 0) {
            this.logger.d(new StringBuilder().append("USER_STUDY executing option ").append(this.selectedItem + 1).toString());
            if (b) {
                if (this.executeAnimation == null) {
                    this.executeAnimation = android.view.animation.AnimationUtils.loadAnimation(this.getContext(), R.anim.press_button);
                    this.executeAnimation.setAnimationListener(this.listener);
                }
                this.getItemAt(this.selectedItem).startAnimation(this.executeAnimation);
            } else {
                this.onExecuteItem(this.selectedItem);
            }
        }
    }
    
    abstract public android.view.View getItemAt(int arg);
    
    
    abstract public int getItemCount();
    
    
    public int getSelectedItem() {
        return this.selectedItem;
    }
    
    abstract public float getValue();
    
    
    public int indexForValue(float f) {
        int i = this.getItemCount();
        return (i > 1) ? (int)((float)(i - 1) * f + 0.5f) : 0;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler((android.view.View)this);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.setOnTouchListener((android.view.View$OnTouchListener)new com.navdy.hud.app.view.ChoicesView$1(this));
        this.setOnClickListener((android.view.View$OnClickListener)new com.navdy.hud.app.view.ChoicesView$2(this));
    }
    
    public void onClick() {
        this.executeSelectedItem(this.animateItemOnExecution);
    }
    
    protected void onDeselectItem(android.view.View a, int i) {
    }
    
    abstract public void onExecuteItem(int arg);
    
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        this.detector.onGesture(a);
        com.navdy.service.library.events.input.Gesture a0 = a.gesture;
        int i = com.navdy.hud.app.view.ChoicesView$4.$SwitchMap$com$navdy$service$library$events$input$Gesture[a0.ordinal()];
        label2: {
            switch(i) {
                case 6: {
                    boolean b0 = this.wrapAround;
                    label3: {
                        label4: {
                            if (b0) {
                                break label4;
                            }
                            if (this.selectedItem + 1 == this.getItemCount()) {
                                break label3;
                            }
                        }
                        this.moveSelectionRight();
                        b = true;
                        break label2;
                    }
                    this.buildBounceAnim(1f, this.valueForIndex(this.selectedItem)).start();
                    b = true;
                    break label2;
                }
                case 5: {
                    boolean b1 = this.wrapAround;
                    label0: {
                        label1: {
                            if (b1) {
                                break label1;
                            }
                            if (this.selectedItem == 0) {
                                break label0;
                            }
                        }
                        this.moveSelectionLeft();
                        b = true;
                        break label2;
                    }
                    this.buildBounceAnim(0.0f, this.valueForIndex(0)).start();
                    b = true;
                    break label2;
                }
                case 3: case 4: {
                    this.animateDown();
                    this.setSelectedItem(-1);
                    break;
                }
                case 1: case 2: {
                    this.animateUp();
                    break;
                }
            }
            b = false;
        }
        return b;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        if (this.selectedItem != -1) {
            switch(com.navdy.hud.app.view.ChoicesView$4.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 3: {
                    this.executeSelectedItem(true);
                    b = true;
                    break;
                }
                case 2: {
                    this.moveSelectionRight();
                    b = true;
                    break;
                }
                case 1: {
                    this.moveSelectionLeft();
                    b = true;
                    break;
                }
                default: {
                    b = false;
                }
            }
        } else {
            this.animateUp();
            b = true;
        }
        return b;
    }
    
    protected void onSelectItem(android.view.View a, int i) {
    }
    
    public void onTrackHand(float f) {
        this.setValue(f);
    }
    
    public void setSelectedItem(int i) {
        if (i != this.selectedItem) {
            String s = (i >= 0) ? String.valueOf(i + 1) : "none";
            this.logger.d(new StringBuilder().append("selecting option - ").append(s).toString());
            int i0 = this.selectedItem;
            android.view.View a = null;
            if (i0 != -1) {
                a = this.getItemAt(this.selectedItem);
            }
            android.view.View a0 = null;
            if (i != -1) {
                a0 = this.getItemAt(i);
            }
            if (a != null) {
                this.onDeselectItem(a, i);
            }
            int i1 = this.selectedItem;
            this.selectedItem = i;
            if (a0 != null) {
                this.onSelectItem(a0, i1);
            }
            if (i != -1) {
                this.setValue(this.valueForIndex(i));
            }
        }
    }
    
    abstract public void setValue(float arg);
    
    
    public float valueForIndex(int i) {
        int i0 = this.getItemCount();
        return (i0 > 1) ? (float)i / (float)(i0 - 1) : 0.0f;
    }
}
