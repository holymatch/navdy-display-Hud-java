package com.navdy.hud.app.view;

class GestureLearningView$10 implements android.animation.Animator$AnimatorListener {
    final com.navdy.hud.app.view.GestureLearningView this$0;
    final android.widget.TextView val$directionText;
    final android.widget.ImageView val$overLay;
    
    GestureLearningView$10(com.navdy.hud.app.view.GestureLearningView a, android.widget.ImageView a0, android.widget.TextView a1) {
        super();
        this.this$0 = a;
        this.val$overLay = a0;
        this.val$directionText = a1;
    }
    
    public void onAnimationCancel(android.animation.Animator a) {
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        this.val$overLay.setAlpha(0.0f);
        this.val$directionText.setAlpha(0.0f);
    }
    
    public void onAnimationRepeat(android.animation.Animator a) {
    }
    
    public void onAnimationStart(android.animation.Animator a) {
        this.val$overLay.setAlpha(1f);
        this.val$directionText.setAlpha(1f);
    }
}
