package com.navdy.hud.app.view;

final public class ForceUpdateView$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding mPresenter;
    
    public ForceUpdateView$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.view.ForceUpdateView", false, com.navdy.hud.app.view.ForceUpdateView.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.mPresenter = a.requestBinding("com.navdy.hud.app.screen.ForceUpdateScreen$Presenter", com.navdy.hud.app.view.ForceUpdateView.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.mPresenter);
    }
    
    public void injectMembers(com.navdy.hud.app.view.ForceUpdateView a) {
        a.mPresenter = (com.navdy.hud.app.screen.ForceUpdateScreen$Presenter)this.mPresenter.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.view.ForceUpdateView)a);
    }
}
