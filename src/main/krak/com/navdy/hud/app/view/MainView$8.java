package com.navdy.hud.app.view;

class MainView$8 {
    final static int[] $SwitchMap$com$navdy$hud$app$view$MainView$AnimationMode;
    
    static {
        $SwitchMap$com$navdy$hud$app$view$MainView$AnimationMode = new int[com.navdy.hud.app.view.MainView$AnimationMode.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$view$MainView$AnimationMode;
        com.navdy.hud.app.view.MainView$AnimationMode a0 = com.navdy.hud.app.view.MainView$AnimationMode.MOVE_LEFT_SHRINK;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$view$MainView$AnimationMode[com.navdy.hud.app.view.MainView$AnimationMode.RIGHT_EXPAND.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
    }
}
