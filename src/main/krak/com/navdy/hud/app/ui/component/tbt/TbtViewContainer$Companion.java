package com.navdy.hud.app.ui.component.tbt;

final public class TbtViewContainer$Companion {
    private TbtViewContainer$Companion() {
    }
    
    public TbtViewContainer$Companion(kotlin.jvm.internal.DefaultConstructorMarker a) {
        this();
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$p(com.navdy.hud.app.ui.component.tbt.TbtViewContainer$Companion a) {
        return a.getLogger();
    }
    
    final private com.navdy.service.library.log.Logger getLogger() {
        return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.access$getLogger$cp();
    }
    
    final private android.view.animation.AccelerateDecelerateInterpolator getManeuverSwapInterpolator() {
        return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.access$getManeuverSwapInterpolator$cp();
    }
    
    final public com.squareup.otto.Bus getBus() {
        return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.access$getBus$cp();
    }
    
    final public float getItemHeight() {
        return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.access$getItemHeight$cp();
    }
    
    final public long getMANEUVER_PROGRESS_ANIMATION_DURATION() {
        return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.access$getMANEUVER_PROGRESS_ANIMATION_DURATION$cp();
    }
    
    final public long getMANEUVER_SWAP_DELAY() {
        return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.access$getMANEUVER_SWAP_DELAY$cp();
    }
    
    final public long getMANEUVER_SWAP_DURATION() {
        return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.access$getMANEUVER_SWAP_DURATION$cp();
    }
    
    final public com.navdy.hud.app.manager.RemoteDeviceManager getRemoteDeviceManager() {
        return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.access$getRemoteDeviceManager$cp();
    }
    
    final public android.content.res.Resources getResources() {
        return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.access$getResources$cp();
    }
    
    final public com.navdy.hud.app.ui.framework.UIStateManager getUiStateManager() {
        return com.navdy.hud.app.ui.component.tbt.TbtViewContainer.access$getUiStateManager$cp();
    }
}
