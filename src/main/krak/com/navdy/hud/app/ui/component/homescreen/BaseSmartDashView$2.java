package com.navdy.hud.app.ui.component.homescreen;

class BaseSmartDashView$2 {
    final com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView this$0;
    
    BaseSmartDashView$2(com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView a) {
        super();
        this.this$0 = a;
    }
    
    public void GPSSpeedChangeEvent(com.navdy.hud.app.maps.MapEvents$GPSSpeedEvent a) {
        this.this$0.updateSpeed(false);
    }
    
    public void ObdPidChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdPidChangeEvent a) {
        this.this$0.ObdPidChangeEvent(a);
    }
    
    public void onMapEvent(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView.access$000(this.this$0, a.currentSpeedLimit);
    }
    
    public void onSpeedDataExpired(com.navdy.hud.app.manager.SpeedManager$SpeedDataExpired a) {
        this.this$0.updateSpeed(true);
    }
    
    public void onSpeedUnitChanged(com.navdy.hud.app.manager.SpeedManager$SpeedUnitChanged a) {
        this.this$0.updateSpeed(true);
    }
}
