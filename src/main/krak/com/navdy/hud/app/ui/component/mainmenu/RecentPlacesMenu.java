package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

public class RecentPlacesMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model back;
    final private static String places;
    final private static int placesColor;
    final private static android.content.res.Resources resources;
    final private static com.navdy.service.library.log.Logger sLogger;
    private int backSelection;
    private com.squareup.otto.Bus bus;
    private java.util.List cachedList;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    private java.util.List returnToCacheList;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.RecentPlacesMenu.class);
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        placesColor = resources.getColor(R.color.mm_recent_places);
        places = resources.getString(R.string.carousel_menu_recent_place);
        String s = resources.getString(R.string.back);
        int i = resources.getColor(R.color.mm_back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, i, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i, s, (String)null);
    }
    
    public RecentPlacesMenu(com.squareup.otto.Bus a, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a0, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a1, com.navdy.hud.app.ui.component.mainmenu.IMenu a2) {
        this.bus = a;
        this.vscrollComponent = a0;
        this.presenter = a1;
        this.parent = a2;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        return null;
    }
    
    public int getInitialSelection() {
        int i = 0;
        java.util.List a = this.cachedList;
        label0: {
            label1: {
                if (a == null) {
                    break label1;
                }
                if (this.cachedList.size() > 1) {
                    i = 1;
                    break label0;
                }
            }
            i = 0;
        }
        return i;
    }
    
    public java.util.List getItems() {
        Object a = null;
        if (this.cachedList == null) {
            java.util.ArrayList a0 = new java.util.ArrayList();
            this.returnToCacheList = (java.util.List)new java.util.ArrayList();
            ((java.util.List)a0).add(back);
            java.util.List a1 = com.navdy.hud.app.framework.destinations.DestinationsManager.getInstance().getRecentDestinations();
            label2: {
                label0: {
                    label1: {
                        if (a1 == null) {
                            break label1;
                        }
                        if (a1.size() > 0) {
                            break label0;
                        }
                    }
                    sLogger.v("no recent destinations");
                    break label2;
                }
                sLogger.v(new StringBuilder().append("recent destinations:").append(a1.size()).toString());
                java.util.Iterator a2 = a1.iterator();
                int i = 0;
                Object a3 = a2;
                while(((java.util.Iterator)a3).hasNext()) {
                    com.navdy.hud.app.framework.destinations.Destination a4 = (com.navdy.hud.app.framework.destinations.Destination)((java.util.Iterator)a3).next();
                    com.navdy.hud.app.ui.component.vlist.VerticalList$Model a5 = com.navdy.hud.app.ui.component.mainmenu.PlacesMenu.buildModel(a4, i);
                    a5.state = a4;
                    ((java.util.List)a0).add(a5);
                    this.returnToCacheList.add(a5);
                    i = i + 1;
                }
            }
            this.cachedList = (java.util.List)a0;
            a = a0;
        } else {
            a = this.cachedList;
        }
        return (java.util.List)a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        java.util.List a0 = this.cachedList;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.cachedList.size() > i) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(i);
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.RECENT_PLACES;
    }
    
    public boolean isBindCallsEnabled() {
        return false;
    }
    
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    public boolean isItemClickable(int i, int i0) {
        return true;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
    }
    
    public void onScrollIdle() {
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
        if (com.navdy.hud.app.ui.component.mainmenu.RecentPlacesMenu$1.$SwitchMap$com$navdy$hud$app$ui$component$mainmenu$IMenu$MenuLevel[a.ordinal()] != 0 && this.returnToCacheList != null) {
            sLogger.v("rpm:unload add to cache");
            com.navdy.hud.app.ui.component.vlist.VerticalModelCache.addToCache(this.returnToCacheList);
            this.returnToCacheList = null;
        }
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        sLogger.v(new StringBuilder().append("select id:").append(a.id).append(" pos:").append(a.pos).toString());
        if (a.id == R.id.menu_back) {
            sLogger.v("back");
            com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
            this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
        } else {
            com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0 = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(a.pos);
            ((com.navdy.hud.app.ui.component.mainmenu.PlacesMenu)this.parent).launchDestination(a0);
        }
        return true;
    }
    
    public void setBackSelectionId(int i) {
    }
    
    public void setBackSelectionPos(int i) {
        this.backSelection = i;
    }
    
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_place_recent_2, placesColor, (android.graphics.Shader)null, 1f);
        this.vscrollComponent.selectedText.setText((CharSequence)places);
    }
    
    public void showToolTip() {
    }
}
