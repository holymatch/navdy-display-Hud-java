package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class SmartDashViewResourceValues {
    final public static int leftGaugeActiveX;
    final public static int leftGaugeActiveY;
    final public static int leftGaugeOpenX;
    final public static int leftGaugeOpenY;
    final public static int leftGaugeShrinkRightX;
    final public static int middleGaugeActiveHeight;
    final public static int middleGaugeActiveShrinkLeftX;
    final public static int middleGaugeActiveShrinkRightX;
    final public static int middleGaugeActiveWidth;
    final public static int middleGaugeActiveX;
    final public static int middleGaugeActiveY;
    final public static int middleGaugeLargeText;
    final public static int middleGaugeOpenHeight;
    final public static int middleGaugeOpenWidth;
    final public static int middleGaugeOpenX;
    final public static int middleGaugeOpenY;
    final public static int middleGaugeShrinkLeftX;
    final public static int middleGaugeShrinkRightX;
    final public static int middleGaugeSmallText;
    final public static int openAnimationDistance;
    final public static int rightGaugeActiveX;
    final public static int rightGaugeActiveY;
    final public static int rightGaugeOpenX;
    final public static int rightGaugeOpenY;
    final public static int rightGaugeShrinkLeftX;
    
    static {
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        leftGaugeOpenX = (int)a.getDimension(R.dimen.smart_dash_open_left_gauge_x);
        leftGaugeOpenY = (int)a.getDimension(R.dimen.smart_dash_open_left_gauge_y);
        middleGaugeOpenX = (int)a.getDimension(R.dimen.smart_dash_open_middle_gauge_x);
        middleGaugeOpenY = (int)a.getDimension(R.dimen.smart_dash_open_middle_gauge_y);
        rightGaugeOpenX = (int)a.getDimension(R.dimen.smart_dash_open_right_gauge_x);
        rightGaugeOpenY = (int)a.getDimension(R.dimen.smart_dash_open_right_gauge_y);
        leftGaugeActiveX = (int)a.getDimension(R.dimen.smart_dash_active_left_gauge_x);
        leftGaugeActiveY = (int)a.getDimension(R.dimen.smart_dash_active_left_gauge_y);
        middleGaugeActiveX = (int)a.getDimension(R.dimen.smart_dash_active_middle_gauge_x);
        middleGaugeActiveY = (int)a.getDimension(R.dimen.smart_dash_active_middle_gauge_y);
        rightGaugeActiveX = (int)a.getDimension(R.dimen.smart_dash_active_right_gauge_x);
        rightGaugeActiveY = (int)a.getDimension(R.dimen.smart_dash_active_right_gauge_y);
        middleGaugeShrinkLeftX = (int)a.getDimension(R.dimen.smart_dash_middle_gauge_shrink_left_x);
        middleGaugeShrinkRightX = (int)a.getDimension(R.dimen.smart_dash_middle_gauge_shrink_right_x);
        rightGaugeShrinkLeftX = (int)a.getDimension(R.dimen.smart_dash_right_gauge_shrink_left_x);
        leftGaugeShrinkRightX = (int)a.getDimension(R.dimen.smart_dash_left_gauge_shrink_right_x);
        middleGaugeActiveShrinkLeftX = (int)a.getDimension(R.dimen.smart_dash_middle_gauge_active_shrink_left_x);
        middleGaugeActiveShrinkRightX = (int)a.getDimension(R.dimen.smart_dash_middle_gauge_active_shrink_right_x);
        openAnimationDistance = (int)a.getDimension(R.dimen.smart_dash_open_top_animation_dist);
        middleGaugeOpenWidth = (int)a.getDimension(R.dimen.smart_dash_open_middle_gauge_w);
        middleGaugeOpenHeight = (int)a.getDimension(R.dimen.smart_dash_open_middle_gauge_h);
        middleGaugeActiveWidth = (int)a.getDimension(R.dimen.smart_dash_active_middle_gauge_w);
        middleGaugeActiveHeight = (int)a.getDimension(R.dimen.smart_dash_active_middle_gauge_h);
        middleGaugeSmallText = (int)a.getDimension(R.dimen.smart_dash_middle_gauge_small_text_size);
        middleGaugeLargeText = (int)a.getDimension(R.dimen.smart_dash_middle_gauge_large_text_size);
    }
    
    public SmartDashViewResourceValues() {
    }
}
