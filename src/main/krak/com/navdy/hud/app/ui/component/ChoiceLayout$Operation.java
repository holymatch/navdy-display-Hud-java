package com.navdy.hud.app.ui.component;


    public enum ChoiceLayout$Operation {
        MOVE_LEFT(0),
        MOVE_RIGHT(1),
        EXECUTE(2),
        KEY_DOWN(3),
        KEY_UP(4);

        private int value;
        ChoiceLayout$Operation(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    
//final class ChoiceLayout$Operation extends Enum {
//    final private static com.navdy.hud.app.ui.component.ChoiceLayout$Operation[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.ChoiceLayout$Operation EXECUTE;
//    final public static com.navdy.hud.app.ui.component.ChoiceLayout$Operation KEY_DOWN;
//    final public static com.navdy.hud.app.ui.component.ChoiceLayout$Operation KEY_UP;
//    final public static com.navdy.hud.app.ui.component.ChoiceLayout$Operation MOVE_LEFT;
//    final public static com.navdy.hud.app.ui.component.ChoiceLayout$Operation MOVE_RIGHT;
//    
//    static {
//        MOVE_LEFT = new com.navdy.hud.app.ui.component.ChoiceLayout$Operation("MOVE_LEFT", 0);
//        MOVE_RIGHT = new com.navdy.hud.app.ui.component.ChoiceLayout$Operation("MOVE_RIGHT", 1);
//        EXECUTE = new com.navdy.hud.app.ui.component.ChoiceLayout$Operation("EXECUTE", 2);
//        KEY_DOWN = new com.navdy.hud.app.ui.component.ChoiceLayout$Operation("KEY_DOWN", 3);
//        KEY_UP = new com.navdy.hud.app.ui.component.ChoiceLayout$Operation("KEY_UP", 4);
//        com.navdy.hud.app.ui.component.ChoiceLayout$Operation[] a = new com.navdy.hud.app.ui.component.ChoiceLayout$Operation[5];
//        a[0] = MOVE_LEFT;
//        a[1] = MOVE_RIGHT;
//        a[2] = EXECUTE;
//        a[3] = KEY_DOWN;
//        a[4] = KEY_UP;
//        $VALUES = a;
//    }
//    
//    private ChoiceLayout$Operation(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.ChoiceLayout$Operation valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.ChoiceLayout$Operation)Enum.valueOf(com.navdy.hud.app.ui.component.ChoiceLayout$Operation.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.ChoiceLayout$Operation[] values() {
//        return $VALUES.clone();
//    }
//}
//