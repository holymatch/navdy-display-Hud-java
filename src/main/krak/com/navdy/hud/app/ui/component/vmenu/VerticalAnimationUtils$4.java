package com.navdy.hud.app.ui.component.vmenu;

final class VerticalAnimationUtils$4 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final android.view.ViewGroup$MarginLayoutParams val$layoutParams;
    final android.view.View val$view;
    
    VerticalAnimationUtils$4(android.view.ViewGroup$MarginLayoutParams a, android.view.View a0) {
        super();
        this.val$layoutParams = a;
        this.val$view = a0;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        int i = ((Integer)a.getAnimatedValue()).intValue();
        this.val$layoutParams.width = i;
        this.val$layoutParams.height = i;
        this.val$view.requestLayout();
    }
}
