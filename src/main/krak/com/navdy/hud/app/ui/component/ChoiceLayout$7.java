package com.navdy.hud.app.ui.component;

class ChoiceLayout$7 implements android.animation.ValueAnimator$AnimatorUpdateListener {
    final com.navdy.hud.app.ui.component.ChoiceLayout this$0;
    
    ChoiceLayout$7(com.navdy.hud.app.ui.component.ChoiceLayout a) {
        super();
        this.this$0 = a;
    }
    
    public void onAnimationUpdate(android.animation.ValueAnimator a) {
        ((android.widget.FrameLayout$LayoutParams)this.this$0.highlightView.getLayoutParams()).width = ((Integer)a.getAnimatedValue()).intValue();
        this.this$0.highlightView.invalidate();
        this.this$0.highlightView.requestLayout();
    }
}
