package com.navdy.hud.app.ui.component.image;
import com.navdy.hud.app.R;

public class InitialsImageView extends android.widget.ImageView {
    private static int greyColor;
    private static int largeStyleTextSize;
    private static int mediumStyleTextSize;
    private static int smallStyleTextSize;
    private static int tinyStyleTextSize;
    private static android.graphics.Typeface typefaceLarge;
    private static android.graphics.Typeface typefaceSmall;
    private static boolean valuesSet;
    private static int whiteColor;
    private int bkColor;
    private String initials;
    private android.graphics.Paint paint;
    private boolean scaled;
    private com.navdy.hud.app.ui.component.image.InitialsImageView$Style style;
    
    public InitialsImageView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null, 0);
    }
    
    public InitialsImageView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public InitialsImageView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.style = com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT;
        this.bkColor = greyColor;
        this.init();
    }
    
    private void init() {
        if (!valuesSet) {
            valuesSet = true;
            android.content.res.Resources a = this.getResources();
            greyColor = a.getColor(R.color.grey_4a);
            whiteColor = a.getColor(17170443);
            tinyStyleTextSize = (int)a.getDimension(R.dimen.contact_image_tiny_text);
            smallStyleTextSize = (int)a.getDimension(R.dimen.contact_image_small_text);
            mediumStyleTextSize = (int)a.getDimension(R.dimen.contact_image_medium_text);
            largeStyleTextSize = (int)a.getDimension(R.dimen.contact_image_large_text);
            typefaceSmall = android.graphics.Typeface.create("sans-serif-medium", 0);
            typefaceLarge = android.graphics.Typeface.create("sans-serif", 1);
        }
        this.paint = new android.graphics.Paint();
        this.paint.setStrokeWidth(0.0f);
        this.paint.setAntiAlias(true);
    }
    
    public com.navdy.hud.app.ui.component.image.InitialsImageView$Style getStyle() {
        return this.style;
    }
    
    protected void onDraw(android.graphics.Canvas a) {
        super.onDraw(a);
        if (this.style != com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT) {
            com.navdy.hud.app.ui.component.image.InitialsImageView$Style a0 = null;
            boolean b = false;
            int i = this.getWidth();
            int i0 = this.getHeight();
            switch(com.navdy.hud.app.ui.component.image.InitialsImageView$1.$SwitchMap$com$navdy$hud$app$ui$component$image$InitialsImageView$Style[this.style.ordinal()]) {
                case 1: case 2: {
                    a.drawColor(0);
                    if (this.bkColor != 0) {
                        this.paint.setColor(this.bkColor);
                        a.drawCircle((float)(i / 2), (float)(i0 / 2), (float)(i / 2), this.paint);
                    }
                }
                default: {
                    a0 = this.style;
                    b = this.scaled;
                }
            }
            if (b) {
                a0 = com.navdy.hud.app.ui.component.image.InitialsImageView$Style.SMALL;
            }
            if (this.initials != null) {
                this.paint.setColor(whiteColor);
                switch(com.navdy.hud.app.ui.component.image.InitialsImageView$1.$SwitchMap$com$navdy$hud$app$ui$component$image$InitialsImageView$Style[a0.ordinal()]) {
                    case 4: {
                        this.paint.setTextSize((float)largeStyleTextSize);
                        this.paint.setTypeface(typefaceLarge);
                        break;
                    }
                    case 3: {
                        this.paint.setTextSize((float)mediumStyleTextSize);
                        this.paint.setTypeface(typefaceLarge);
                        break;
                    }
                    case 2: {
                        this.paint.setTextSize((float)tinyStyleTextSize);
                        this.paint.setTypeface(typefaceSmall);
                        break;
                    }
                    case 1: {
                        this.paint.setTextSize((float)smallStyleTextSize);
                        this.paint.setTypeface(typefaceSmall);
                        break;
                    }
                }
                this.paint.setTextAlign(android.graphics.Paint$Align.CENTER);
                int i1 = i / 2;
                int i2 = (int)((float)(i0 / 2) - (this.paint.descent() + this.paint.ascent()) / 2f);
                a.drawText(this.initials, (float)i1, (float)i2, this.paint);
            }
        }
    }
    
    public void setBkColor(int i) {
        this.bkColor = i;
    }
    
    public void setImage(int i, String s, com.navdy.hud.app.ui.component.image.InitialsImageView$Style a) {
        this.setImageResource(i);
        this.setInitials(s, a);
    }
    
    public void setInitials(String s, com.navdy.hud.app.ui.component.image.InitialsImageView$Style a) {
        this.initials = s;
        this.style = a;
        this.invalidate();
    }
    
    public void setScaled(boolean b) {
        this.scaled = b;
    }
}
