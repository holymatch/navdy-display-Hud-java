package com.navdy.hud.app.ui.component.tbt;

final public class TbtView$WhenMappings {
    final public static int[] $EnumSwitchMapping$0;
    final public static int[] $EnumSwitchMapping$1;
    final public static int[] $EnumSwitchMapping$2;
    
    static {
        $EnumSwitchMapping$0 = new int[com.navdy.hud.app.view.MainView$CustomAnimationMode.values().length];
        $EnumSwitchMapping$0[com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND.ordinal()] = 1;
        $EnumSwitchMapping$0[com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT.ordinal()] = 2;
        $EnumSwitchMapping$1 = new int[com.navdy.hud.app.view.MainView$CustomAnimationMode.values().length];
        $EnumSwitchMapping$1[com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND.ordinal()] = 1;
        $EnumSwitchMapping$1[com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT.ordinal()] = 2;
        $EnumSwitchMapping$2 = new int[com.navdy.hud.app.view.MainView$CustomAnimationMode.values().length];
        $EnumSwitchMapping$2[com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND.ordinal()] = 1;
    }
}
