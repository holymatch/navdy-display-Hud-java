package com.navdy.hud.app.ui.component.vlist.viewholder;
import com.navdy.hud.app.R;

public class ContentLoadingViewHolder extends com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder {
    final private static com.navdy.service.library.log.Logger sLogger;
    private android.animation.AnimatorSet$Builder animatorSetBuilder;
    private com.navdy.hud.app.ui.component.image.CrossFadeImageView crossFadeImageView;
    private android.view.ViewGroup iconContainer;
    private android.view.ViewGroup imageContainer;
    private android.graphics.drawable.AnimationDrawable loopAnimation;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener loopEnd;
    private android.widget.ImageView loopImageView;
    private com.navdy.hud.app.ui.framework.DefaultAnimationListener loopStart;
    private android.widget.ImageView unSelImageView;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder.class);
    }
    
    private ContentLoadingViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        super(a, a0, a1);
        this.loopStart = new com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder$1(this);
        this.loopEnd = new com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder$2(this);
        this.imageContainer = (android.view.ViewGroup)a.findViewById(R.id.imageContainer);
        this.iconContainer = (android.view.ViewGroup)a.findViewById(R.id.iconContainer);
        this.crossFadeImageView = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)a.findViewById(R.id.vlist_image);
        this.loopImageView = (android.widget.ImageView)a.findViewById(R.id.img_loop);
        this.unSelImageView = (android.widget.ImageView)a.findViewById(R.id.img);
        this.loopAnimation = (android.graphics.drawable.AnimationDrawable)this.loopImageView.getDrawable();
    }
    
    static android.widget.ImageView access$000(com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder a) {
        return a.loopImageView;
    }
    
    static android.widget.ImageView access$100(com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder a) {
        return a.unSelImageView;
    }
    
    static android.graphics.drawable.AnimationDrawable access$200(com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder a) {
        return a.loopAnimation;
    }
    
    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildModel(int i, int i0, int i1) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = com.navdy.hud.app.ui.component.vlist.VerticalModelCache.getFromCache(com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.LOADING_CONTENT);
        if (a == null) {
            a = new com.navdy.hud.app.ui.component.vlist.VerticalList$Model();
        }
        a.type = com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.LOADING_CONTENT;
        a.id = R.id.vlist_content_loading;
        a.icon = i;
        a.iconSelectedColor = i0;
        a.iconDeselectedColor = i1;
        return a;
    }
    
    public static com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder buildViewHolder(android.view.ViewGroup a, com.navdy.hud.app.ui.component.vlist.VerticalList a0, android.os.Handler a1) {
        android.view.ViewGroup a2 = (android.view.ViewGroup)android.view.LayoutInflater.from(a.getContext()).inflate(R.layout.vlist_content_loading, a, false);
        com.navdy.hud.app.ui.component.image.CrossFadeImageView a3 = (com.navdy.hud.app.ui.component.image.CrossFadeImageView)((android.view.ViewGroup)a2.findViewById(R.id.iconContainer)).findViewById(R.id.crossFadeImageView);
        a3.inject(com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL);
        a3.setId(R.id.vlist_image);
        return new com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder(a2, a0, a1);
    }
    
    private void setIcon(int i, int i0, int i1) {
        com.navdy.hud.app.ui.component.image.IconColorImageView a = (com.navdy.hud.app.ui.component.image.IconColorImageView)this.crossFadeImageView.getBig();
        a.setDraw(true);
        a.setIcon(i, i0, (android.graphics.Shader)null, 0.83f);
        com.navdy.hud.app.ui.component.image.IconColorImageView a0 = (com.navdy.hud.app.ui.component.image.IconColorImageView)this.crossFadeImageView.getSmall();
        a0.setDraw(true);
        a0.setIcon(i, i1, (android.graphics.Shader)null, 0.83f);
    }
    
    public void bind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        this.setIcon(a.icon, a.iconSelectedColor, a.iconDeselectedColor);
    }
    
    public void clearAnimation() {
        if (this.loopAnimation != null && this.loopAnimation.isRunning()) {
            this.loopAnimation.stop();
        }
        this.stopAnimation();
        this.currentState = null;
        this.layout.setVisibility(0);
        this.layout.setAlpha(1f);
    }
    
    public void copyAndPosition(android.widget.ImageView a, android.widget.TextView a0, android.widget.TextView a1, android.widget.TextView a2, boolean b) {
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType getModelType() {
        return com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.LOADING_CONTENT;
    }
    
    public void preBind(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a0) {
        this.crossFadeImageView.setSmallAlpha(-1f);
        ((com.navdy.hud.app.ui.component.image.IconColorImageView)this.crossFadeImageView.getBig()).setIconShape(a.iconShape);
        ((com.navdy.hud.app.ui.component.image.IconColorImageView)this.crossFadeImageView.getSmall()).setIconShape(a.iconShape);
    }
    
    public void select(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, int i, int i0) {
    }
    
    public void setItemState(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State a, com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$AnimationType a0, int i, boolean b) {
        float f = 0.0f;
        this.animatorSetBuilder = null;
        int i0 = com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder$3.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$State[a.ordinal()];
        com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode a1 = null;
        switch(i0) {
            case 2: {
                a1 = com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.SMALL;
                f = 0.6f;
                break;
            }
            case 1: {
                a1 = com.navdy.hud.app.ui.component.image.CrossFadeImageView$Mode.BIG;
                f = 1f;
                break;
            }
            default: {
                f = 0.0f;
            }
        }
        switch(com.navdy.hud.app.ui.component.vlist.viewholder.ContentLoadingViewHolder$3.$SwitchMap$com$navdy$hud$app$ui$component$vlist$viewholder$VerticalViewHolder$AnimationType[a0.ordinal()]) {
            case 3: {
                this.itemAnimatorSet = new android.animation.AnimatorSet();
                android.util.Property a2 = android.view.View.SCALE_X;
                float[] a3 = new float[1];
                a3[0] = f;
                android.animation.PropertyValuesHolder a4 = android.animation.PropertyValuesHolder.ofFloat(a2, a3);
                android.util.Property a5 = android.view.View.SCALE_Y;
                float[] a6 = new float[1];
                a6[0] = f;
                android.animation.PropertyValuesHolder a7 = android.animation.PropertyValuesHolder.ofFloat(a5, a6);
                android.animation.AnimatorSet a8 = this.itemAnimatorSet;
                android.view.ViewGroup a9 = this.imageContainer;
                android.animation.PropertyValuesHolder[] a10 = new android.animation.PropertyValuesHolder[2];
                a10[0] = a4;
                a10[1] = a7;
                this.animatorSetBuilder = a8.play((android.animation.Animator)android.animation.ObjectAnimator.ofPropertyValuesHolder(a9, a10));
                if (this.crossFadeImageView.getMode() != a1) {
                    this.animatorSetBuilder.with((android.animation.Animator)this.crossFadeImageView.getCrossFadeAnimator());
                }
                if (a != com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED) {
                    this.itemAnimatorSet.addListener((android.animation.Animator$AnimatorListener)this.loopEnd);
                    android.animation.AnimatorSet$Builder a11 = this.animatorSetBuilder;
                    android.widget.ImageView a12 = this.unSelImageView;
                    android.util.Property a13 = android.view.View.ALPHA;
                    float[] a14 = new float[1];
                    a14[0] = 1f;
                    a11.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a12, a13, a14));
                    android.animation.AnimatorSet$Builder a15 = this.animatorSetBuilder;
                    android.widget.ImageView a16 = this.loopImageView;
                    android.util.Property a17 = android.view.View.ALPHA;
                    float[] a18 = new float[1];
                    a18[0] = 0.0f;
                    a15.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a16, a17, a18));
                    break;
                } else {
                    this.itemAnimatorSet.addListener((android.animation.Animator$AnimatorListener)this.loopStart);
                    android.animation.AnimatorSet$Builder a19 = this.animatorSetBuilder;
                    android.widget.ImageView a20 = this.loopImageView;
                    android.util.Property a21 = android.view.View.ALPHA;
                    float[] a22 = new float[1];
                    a22[0] = 1f;
                    a19.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a20, a21, a22));
                    android.animation.AnimatorSet$Builder a23 = this.animatorSetBuilder;
                    android.widget.ImageView a24 = this.unSelImageView;
                    android.util.Property a25 = android.view.View.ALPHA;
                    float[] a26 = new float[1];
                    a26[0] = 0.0f;
                    a23.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a24, a25, a26));
                    break;
                }
            }
            case 1: case 2: {
                this.imageContainer.setScaleX(f);
                this.imageContainer.setScaleY(f);
                this.crossFadeImageView.setMode(a1);
                if (a != com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.SELECTED) {
                    this.loopImageView.setVisibility(8);
                    this.loopImageView.setAlpha(0.0f);
                    this.unSelImageView.setVisibility(0);
                    this.unSelImageView.setAlpha(1f);
                    if (this.loopAnimation == null) {
                        break;
                    }
                    if (!this.loopAnimation.isRunning()) {
                        break;
                    }
                    this.loopAnimation.stop();
                    break;
                } else {
                    this.loopImageView.setVisibility(0);
                    this.loopImageView.setAlpha(1f);
                    this.unSelImageView.setVisibility(8);
                    this.unSelImageView.setAlpha(0.0f);
                    if (this.loopAnimation == null) {
                        break;
                    }
                    if (this.loopAnimation.isRunning()) {
                        break;
                    }
                    this.loopAnimation.start();
                    break;
                }
            }
        }
    }
}
