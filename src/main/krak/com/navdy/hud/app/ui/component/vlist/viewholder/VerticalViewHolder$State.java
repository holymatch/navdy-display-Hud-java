package com.navdy.hud.app.ui.component.vlist.viewholder;


public enum VerticalViewHolder$State {
    SELECTED(0),
    UNSELECTED(1);

    private int value;
    VerticalViewHolder$State(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class VerticalViewHolder$State extends Enum {
//    final private static com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State SELECTED;
//    final public static com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State UNSELECTED;
//    
//    static {
//        SELECTED = new com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State("SELECTED", 0);
//        UNSELECTED = new com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State("UNSELECTED", 1);
//        com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State[] a = new com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State[2];
//        a[0] = SELECTED;
//        a[1] = UNSELECTED;
//        $VALUES = a;
//    }
//    
//    private VerticalViewHolder$State(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State)Enum.valueOf(com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.vlist.viewholder.VerticalViewHolder$State[] values() {
//        return $VALUES.clone();
//    }
//}
//