package com.navdy.hud.app.ui.component;

class ChoiceLayout$5 implements android.view.ViewTreeObserver$OnGlobalLayoutListener {
    final com.navdy.hud.app.ui.component.ChoiceLayout this$0;
    
    ChoiceLayout$5(com.navdy.hud.app.ui.component.ChoiceLayout a) {
        super();
        this.this$0 = a;
    }
    
    public void onGlobalLayout() {
        android.view.View a = this.this$0.choiceContainer.getChildAt(com.navdy.hud.app.ui.component.ChoiceLayout.access$800(this.this$0));
        if (a != null) {
            a.getViewTreeObserver().removeOnGlobalLayoutListener((android.view.ViewTreeObserver$OnGlobalLayoutListener)this);
            com.navdy.hud.app.ui.component.ChoiceLayout.access$1200(this.this$0, com.navdy.hud.app.ui.component.ChoiceLayout.access$800(this.this$0));
        }
    }
}
