package com.navdy.hud.app.ui.component.homescreen;

class TbtView$3 implements android.animation.Animator$AnimatorListener {
    final com.navdy.hud.app.ui.component.homescreen.TbtView this$0;
    final int val$imageResourceId;
    
    TbtView$3(com.navdy.hud.app.ui.component.homescreen.TbtView a, int i) {
        super();
        this.this$0 = a;
        this.val$imageResourceId = i;
    }
    
    public void onAnimationCancel(android.animation.Animator a) {
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        this.this$0.activeMapIcon.setImageResource(this.val$imageResourceId);
        this.this$0.activeMapIcon.setTranslationY(0.0f);
        this.this$0.nextMapIconContainer.setTranslationY(0.0f);
        this.this$0.nextMapIconContainer.setVisibility(8);
    }
    
    public void onAnimationRepeat(android.animation.Animator a) {
    }
    
    public void onAnimationStart(android.animation.Animator a) {
    }
}
