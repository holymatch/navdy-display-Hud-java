package com.navdy.hud.app.ui.activity;

final public class Main$Presenter$DeferredServices$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding ancsService;
    private dagger.internal.Binding gestureService;
    
    public Main$Presenter$DeferredServices$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices", false, com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.gestureService = a.requestBinding("com.navdy.hud.app.gesture.GestureServiceConnector", com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices.class, (this).getClass().getClassLoader());
        this.ancsService = a.requestBinding("com.navdy.hud.app.ancs.AncsServiceConnector", com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.gestureService);
        a0.add(this.ancsService);
    }
    
    public void injectMembers(com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices a) {
        a.gestureService = (com.navdy.hud.app.gesture.GestureServiceConnector)this.gestureService.get();
        a.ancsService = (com.navdy.hud.app.ancs.AncsServiceConnector)this.ancsService.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.ui.activity.Main$Presenter$DeferredServices)a);
    }
}
