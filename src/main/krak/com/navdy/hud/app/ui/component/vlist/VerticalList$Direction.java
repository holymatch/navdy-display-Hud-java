package com.navdy.hud.app.ui.component.vlist;


public enum VerticalList$Direction {
    UP(0),
    DOWN(1);

    private int value;
    VerticalList$Direction(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class VerticalList$Direction extends Enum {
//    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Direction[] $VALUES;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Direction DOWN;
//    final public static com.navdy.hud.app.ui.component.vlist.VerticalList$Direction UP;
//    
//    static {
//        UP = new com.navdy.hud.app.ui.component.vlist.VerticalList$Direction("UP", 0);
//        DOWN = new com.navdy.hud.app.ui.component.vlist.VerticalList$Direction("DOWN", 1);
//        com.navdy.hud.app.ui.component.vlist.VerticalList$Direction[] a = new com.navdy.hud.app.ui.component.vlist.VerticalList$Direction[2];
//        a[0] = UP;
//        a[1] = DOWN;
//        $VALUES = a;
//    }
//    
//    private VerticalList$Direction(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Direction valueOf(String s) {
//        return (com.navdy.hud.app.ui.component.vlist.VerticalList$Direction)Enum.valueOf(com.navdy.hud.app.ui.component.vlist.VerticalList$Direction.class, s);
//    }
//    
//    public static com.navdy.hud.app.ui.component.vlist.VerticalList$Direction[] values() {
//        return $VALUES.clone();
//    }
//}
//