package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

public class ActiveTripMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    final private static int ARRIVAL_EXPAND = 100;
    final private static int MIN_MANEUVER_DISTANCE = 100;
    final private static int TITLE_UPDATE_INTERVAL = 1000;
    final private static String TOWARDS_PATTERN;
    final private static String activeTripTitle;
    final private static String arriveTitle;
    final private static String arrivedDistance;
    final private static String arrivedTitle;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model back;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model endTrip;
    final private static String feetLabel;
    final private static android.os.Handler handler;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model infoModel;
    final private static String kiloMetersLabel;
    final public static int maneuverIconSize;
    final private static String metersLabel;
    final private static String milesLabel;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model muteTbtAudio;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model reportIssue;
    final private static android.content.res.Resources resources;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static int size_18;
    final private static int size_22;
    final private static StringBuilder timeBuilder;
    final private static com.navdy.hud.app.common.TimeHelper timeHelper;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model tripManeuvers;
    final private static int tripOptionsColor;
    final private static java.util.HashSet turnNotShown;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model unmuteTbtAudio;
    private android.widget.TextView activeTripDistance;
    private android.widget.TextView activeTripDuration;
    private android.widget.TextView activeTripEta;
    private android.view.View activeTripProgress;
    private android.widget.TextView activeTripSubTitleView;
    private android.widget.TextView activeTripTitleView;
    private boolean arrived;
    private int backSelection;
    private com.squareup.otto.Bus bus;
    private java.util.List cachedList;
    private java.util.List contactList;
    private com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu contactOptionsMenu;
    private int currentSelection;
    private com.navdy.hud.app.view.drawable.ETAProgressDrawable etaProgressDrawable;
    private boolean getItemsCalled;
    private String label;
    private boolean maneuverSelected;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    private com.navdy.service.library.events.contacts.PhoneNumber phoneNumber;
    private boolean positionOnFirstManeuver;
    private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    private boolean registered;
    private com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu reportIssueMenu;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu.class);
        timeBuilder = new StringBuilder();
        turnNotShown = new java.util.HashSet();
        TOWARDS_PATTERN = new StringBuilder().append(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.TOWARDS).append(" ").toString();
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        int i = resources.getColor(R.color.mm_back);
        tripOptionsColor = resources.getColor(R.color.mm_active_trip);
        size_22 = resources.getDimensionPixelSize(R.dimen.active_trip_22);
        size_18 = resources.getDimensionPixelSize(R.dimen.active_trip_18);
        maneuverIconSize = resources.getDimensionPixelSize(R.dimen.active_trip_maneuver_icon_size);
        activeTripTitle = resources.getString(R.string.mm_active_trip);
        arrivedTitle = resources.getString(R.string.mm_active_trip_arrived_no_label);
        arriveTitle = resources.getString(R.string.mm_active_trip_arrive);
        metersLabel = resources.getString(R.string.unit_meters);
        kiloMetersLabel = resources.getString(R.string.unit_kilometers);
        feetLabel = resources.getString(R.string.unit_feet);
        milesLabel = resources.getString(R.string.unit_miles);
        arrivedDistance = resources.getString(R.string.mm_active_trip_arrived_dist);
        String s = resources.getString(R.string.back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, i, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i, s, (String)null);
        infoModel = com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder.buildModel(R.layout.active_trip_menu_lyt);
        String s0 = resources.getString(R.string.end_trip);
        int i0 = resources.getColor(R.color.mm_options_end_trip);
        endTrip = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_end_trip, R.drawable.icon_active_nav_end_2, i0, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i0, s0, (String)null);
        String s1 = resources.getString(R.string.mute_tbt_2);
        int i1 = resources.getColor(R.color.mm_options_mute_tbt_audio);
        muteTbtAudio = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_mute_tbt, R.drawable.icon_active_nav_mute_2, i1, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i1, s1, (String)null);
        String s2 = resources.getString(R.string.unmute_tbt_2);
        int i2 = resources.getColor(R.color.mm_options_unmute_tbt_audio);
        unmuteTbtAudio = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_unmute_tbt, R.drawable.icon_active_nav_unmute_2, i2, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i2, s2, (String)null);
        String s3 = resources.getString(R.string.report_navigation_issue);
        int i3 = resources.getColor(R.color.mm_options_report_issue);
        reportIssue = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.main_menu_options_report_issue, R.drawable.icon_options_report_issue_2, i3, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i3, s3, (String)null);
        tripManeuvers = com.navdy.hud.app.ui.component.vlist.viewholder.TitleViewHolder.buildModel(resources.getString(R.string.trip_maneuvers));
        tripManeuvers.id = R.id.active_trip_maneuver_title;
        timeHelper = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTimeHelper();
        turnNotShown.add(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.START_TURN);
        turnNotShown.add(com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.ARRIVED);
    }
    
    ActiveTripMenu(com.squareup.otto.Bus a, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a0, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a1, com.navdy.hud.app.ui.component.mainmenu.IMenu a2) {
        this.currentSelection = -1;
        this.bus = a;
        this.vscrollComponent = a0;
        this.presenter = a1;
        this.parent = a2;
    }
    
    static boolean access$000(com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu a) {
        return a.getItemsCalled;
    }
    
    static com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter access$100(com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu a) {
        return a.presenter;
    }
    
    static com.navdy.service.library.events.contacts.PhoneNumber access$200(com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu a) {
        return a.phoneNumber;
    }
    
    static String access$300(com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu a) {
        return a.label;
    }
    
    private com.here.android.mpa.common.GeoBoundingBox buildArrivalBoundingBox(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        com.here.android.mpa.common.GeoBoundingBox a0 = null;
        label3: {
            label0: try {
                com.here.android.mpa.common.GeoCoordinate a1 = null;
                double d = a.destination.latitude.doubleValue();
                int i = (d > 0.0) ? 1 : (d == 0.0) ? 0 : -1;
                label2: {
                    label1: {
                        if (i == 0) {
                            break label1;
                        }
                        if (a.destination.longitude.doubleValue() == 0.0) {
                            break label1;
                        }
                        a1 = new com.here.android.mpa.common.GeoCoordinate(a.destination.latitude.doubleValue(), a.destination.longitude.doubleValue());
                        break label2;
                    }
                    com.navdy.service.library.events.location.Coordinate a2 = a.destinationDisplay;
                    a1 = null;
                    if (a2 != null) {
                        double d0 = a.destinationDisplay.latitude.doubleValue();
                        int i0 = (d0 > 0.0) ? 1 : (d0 == 0.0) ? 0 : -1;
                        a1 = null;
                        if (i0 != 0) {
                            double d1 = a.destinationDisplay.longitude.doubleValue();
                            int i1 = (d1 > 0.0) ? 1 : (d1 == 0.0) ? 0 : -1;
                            a1 = null;
                            if (i1 != 0) {
                                a1 = new com.here.android.mpa.common.GeoCoordinate(a.destinationDisplay.latitude.doubleValue(), a.destinationDisplay.longitude.doubleValue());
                            }
                        }
                    }
                }
                if (a1 == null) {
                    break label0;
                }
                a0 = new com.here.android.mpa.common.GeoBoundingBox(a1, a1);
                a0.expand(100f, 100f);
                break label3;
            } catch(Throwable a3) {
                sLogger.e(a3);
            }
            a0 = null;
        }
        return a0;
    }
    
    private void buildManeuvers(java.util.List a) {
        try {
            String s = null;
            long j = android.os.SystemClock.elapsedRealtime();
            com.navdy.hud.app.maps.here.HereNavigationManager a0 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            com.here.android.mpa.routing.Route a1 = a0.getCurrentRoute();
            String s0 = a0.getDestinationLabel();
            String s1 = a0.getFullStreetAddress();
            if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                if (android.text.TextUtils.isEmpty((CharSequence)s1)) {
                    s = s1;
                    s1 = null;
                } else {
                    s = null;
                }
            } else {
                s = s1;
                s1 = s0;
            }
            if (a1 != null) {
                java.util.ArrayList a2 = new java.util.ArrayList();
                java.util.List a3 = a1.getManeuvers();
                int i = a3.size();
                com.here.android.mpa.routing.Maneuver a4 = a0.getNavManeuver();
                if (a4 == null) {
                    sLogger.v("current maneuver not found");
                }
                com.navdy.service.library.events.navigation.NavigationRouteRequest a5 = a0.getCurrentNavigationRouteRequest();
                a.add(tripManeuvers);
                Object a6 = a;
                Object a7 = a3;
                boolean b = false;
                int i0 = 0;
                while(i0 < i) {
                    com.here.android.mpa.routing.Maneuver a8 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a7).get(i0);
                    if (!com.navdy.hud.app.maps.here.HereMapUtil.isStartManeuver(a8)) {
                        if (b) {
                            ((java.util.List)a2).add(a8);
                        } else if (a4 != null) {
                            if (com.navdy.hud.app.maps.here.HereMapUtil.areManeuverEqual(a4, a8)) {
                                ((java.util.List)a2).add(a8);
                                b = true;
                            }
                        } else {
                            ((java.util.List)a2).add(a8);
                        }
                    }
                    i0 = i0 + 1;
                }
                int i1 = ((java.util.List)a2).size();
                com.here.android.mpa.routing.Maneuver a9 = null;
                int i2 = 0;
                while(i2 < i1) {
                    String s2 = null;
                    com.here.android.mpa.routing.Maneuver a10 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a2).get(i2);
                    int i3 = i1 - 1;
                    com.here.android.mpa.routing.Maneuver a11 = null;
                    if (i2 < i3) {
                        a11 = (com.here.android.mpa.routing.Maneuver)((java.util.List)a2).get(i2 + 1);
                    }
                    com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a12 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getManeuverDisplay(a10, a11 == null, (long)a10.getDistanceToNextManeuver(), s, a9, a5, a11, true, false, (com.navdy.hud.app.maps.MapEvents$DestinationDirection)null);
                    boolean b0 = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.shouldShowTurnText(a12.pendingTurn);
                    label3: {
                        label2: {
                            if (!b0) {
                                break label2;
                            }
                            if (turnNotShown.contains(a12.pendingTurn)) {
                                break label2;
                            }
                            s2 = new StringBuilder().append(a12.pendingTurn).append(" ").append(a12.pendingRoad).toString();
                            break label3;
                        }
                        s2 = (a12.pendingRoad.indexOf(TOWARDS_PATTERN) != 0) ? a12.pendingRoad : a12.pendingRoad.substring(TOWARDS_PATTERN.length());
                    }
                    com.navdy.hud.app.ui.component.vlist.VerticalList$Model a13 = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(R.id.active_trip_maneuver, a12.turnIconNowId, a12.turnIconSoonId, 0, 0, s2, a12.distanceToPendingRoadText);
                    if (i2 != i1 - 1) {
                        int i4 = a10.getDistanceToNextManeuver();
                        if (i4 >= 100) {
                            a13.state = a10.getBoundingBox();
                        } else {
                            a13.state = this.expandBoundingBox(a10.getBoundingBox(), 100 - i4);
                        }
                    } else {
                        a13.state = this.buildArrivalBoundingBox(a5);
                    }
                    a13.noTextAnimation = true;
                    a13.noImageScaleAnimation = true;
                    label1: if (a12.navigationTurn != com.navdy.service.library.events.navigation.NavigationTurn.NAV_TURN_END) {
                        a13.iconSize = maneuverIconSize;
                    } else {
                        a13.title = arriveTitle;
                        label0: {
                            if (s1 == null) {
                                break label0;
                            }
                            if (s == null) {
                                break label0;
                            }
                            a13.title = arriveTitle;
                            a13.subTitle = s1;
                            a13.subTitle2 = s;
                            break label1;
                        }
                        if (s1 == null) {
                            if (s == null) {
                                a13.title = arriveTitle;
                            } else {
                                a13.title = arriveTitle;
                                a13.subTitle = s;
                            }
                        } else {
                            a13.title = arriveTitle;
                            a13.subTitle = s1;
                        }
                    }
                    ((java.util.List)a6).add(a13);
                    i2 = i2 + 1;
                    a9 = a10;
                }
            }
            sLogger.v(new StringBuilder().append("time to build maneuvers:").append(android.os.SystemClock.elapsedRealtime() - j).toString());
        } catch(Throwable a14) {
            sLogger.e("addManeuversToList", a14);
        }
    }
    
    private com.here.android.mpa.common.GeoBoundingBox expandBoundingBox(com.here.android.mpa.common.GeoBoundingBox a, int i) {
        try {
            a.expand((float)i, (float)i);
        } catch(Throwable a0) {
            sLogger.e(a0);
        }
        return a;
    }
    
    private void fillDurationVia(java.util.Date a) {
        String s = com.navdy.hud.app.maps.here.HereMapUtil.convertDateToEta(a);
        String s0 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentVia();
        if (s == null) {
            this.activeTripDuration.setText((CharSequence)"");
        } else {
            android.text.SpannableStringBuilder a0 = new android.text.SpannableStringBuilder();
            a0.append((CharSequence)s);
            int i = a0.length();
            a0.setSpan(new android.text.style.StyleSpan(1), 0, i, 34);
            a0.setSpan(new android.text.style.AbsoluteSizeSpan(size_22), 0, i, 34);
            if (s0 != null) {
                a0.append((CharSequence)" ");
                int i0 = s.length();
                android.content.res.Resources a1 = resources;
                Object[] a2 = new Object[1];
                a2[0] = s0;
                a0.append((CharSequence)a1.getString(R.string.via_desc, a2));
                a0.setSpan(new android.text.style.AbsoluteSizeSpan(size_18), i0, a0.length(), 34);
            }
            this.activeTripDuration.setText((CharSequence)a0);
        }
    }
    
    private int getFirstManeuverIndex() {
        int i = 0;
        int i0 = this.cachedList.indexOf(reportIssue);
        label2: {
            label4: {
                if (i0 < 0) {
                    break label4;
                }
                if (i0 + 2 >= this.cachedList.size()) {
                    break label4;
                }
                i = i0 + 2;
                break label2;
            }
            int i1 = this.cachedList.indexOf(unmuteTbtAudio);
            label3: {
                if (i1 < 0) {
                    break label3;
                }
                if (i1 + 2 >= this.cachedList.size()) {
                    break label3;
                }
                i = i1 + 2;
                break label2;
            }
            int i2 = this.cachedList.indexOf(muteTbtAudio);
            label0: {
                label1: {
                    if (i2 < 0) {
                        break label1;
                    }
                    if (i2 + 2 < this.cachedList.size()) {
                        break label0;
                    }
                }
                i = -1;
                break label2;
            }
            i = i2 + 2;
        }
        return i;
    }
    
    private boolean isMenuLoaded() {
        return this.activeTripTitleView != null;
    }
    
    private void setSpokenTurnByTurn(boolean b) {
        com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNavigationSessionPreference().spokenTurnByTurn = b;
        com.navdy.hud.app.profile.DriverProfile a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
        if (!a.isDefaultProfile()) {
            com.navdy.service.library.events.preferences.NavigationPreferences a0 = new com.navdy.service.library.events.preferences.NavigationPreferences$Builder(a.getNavigationPreferences()).spokenTurnByTurn(Boolean.valueOf(b)).build();
            com.navdy.service.library.events.preferences.NavigationPreferencesUpdate a1 = new com.navdy.service.library.events.preferences.NavigationPreferencesUpdate$Builder().status(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS).serial_number(a0.serial_number).preferences(a0).build();
            this.bus.post(a1);
            this.bus.post(new com.navdy.hud.app.event.RemoteEvent((com.squareup.wire.Message)a1));
        }
    }
    
    private void switchToArriveState() {
        if (!android.text.TextUtils.equals(this.activeTripDuration.getText(), (CharSequence)arrivedTitle)) {
            android.text.SpannableStringBuilder a = new android.text.SpannableStringBuilder();
            a.append((CharSequence)arrivedTitle);
            int i = a.length();
            a.setSpan(new android.text.style.StyleSpan(1), 0, i, 34);
            a.setSpan(new android.text.style.AbsoluteSizeSpan(size_22), 0, i, 34);
            this.activeTripDuration.setText((CharSequence)a);
            this.activeTripDistance.setText((CharSequence)arrivedDistance);
            this.activeTripEta.setText((CharSequence)"");
            this.etaProgressDrawable.setGaugeValue(100f);
        }
    }
    
    private void update() {
        if (this.isMenuLoaded()) {
            String s = null;
            com.navdy.hud.app.maps.here.HereNavigationManager a = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
            String s0 = a.getDestinationLabel();
            String s1 = a.getFullStreetAddress();
            if (android.text.TextUtils.isEmpty((CharSequence)s0)) {
                if (android.text.TextUtils.isEmpty((CharSequence)s1)) {
                    s = s1;
                    s1 = null;
                } else {
                    s = null;
                }
            } else {
                s = s1;
                s1 = s0;
            }
            if (s1 == null) {
                this.activeTripTitleView.setVisibility(8);
            } else {
                this.activeTripTitleView.setText((CharSequence)s1);
                this.activeTripTitleView.setVisibility(0);
            }
            if (s == null) {
                this.activeTripSubTitleView.setVisibility(8);
            } else {
                this.activeTripSubTitleView.setText((CharSequence)s);
                this.activeTripSubTitleView.setVisibility(0);
                this.activeTripSubTitleView.requestLayout();
            }
            if (a.hasArrived()) {
                this.switchToArriveState();
            }
        }
    }
    
    public void arrivalEvent(com.navdy.hud.app.maps.MapEvents$ArrivalEvent a) {
        if (this.arrived) {
            sLogger.v("arrival event");
        } else {
            this.arrived = true;
            this.currentSelection = -1;
            this.positionOnFirstManeuver = false;
            this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.REFRESH_CURRENT, 0, 0, true);
        }
    }
    
    com.navdy.hud.app.ui.component.vlist.VerticalList$Model buildContactModel(com.navdy.hud.app.framework.contacts.Contact a, int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0 = null;
        com.navdy.hud.app.framework.contacts.ContactImageHelper a1 = com.navdy.hud.app.framework.contacts.ContactImageHelper.getInstance();
        if (android.text.TextUtils.isEmpty((CharSequence)a.name)) {
            a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(R.id.active_trip_contact, R.drawable.icon_user_bg_4, R.drawable.icon_user_numberonly, resources.getColor(R.color.icon_user_bg_4), -1, a.formattedNumber, (String)null);
        } else {
            int i0 = a1.getResourceId(a.defaultImageIndex);
            int i1 = a1.getResourceColor(a.defaultImageIndex);
            String s = null;
            if (i == 1) {
                s = (a.numberType == com.navdy.hud.app.framework.contacts.NumberType.OTHER) ? a.formattedNumber : a.numberTypeStr;
            }
            android.content.res.Resources a2 = resources;
            Object[] a3 = new Object[1];
            a3[0] = a.name;
            a0 = com.navdy.hud.app.ui.component.vlist.viewholder.IconsTwoViewHolder.buildModel(R.id.active_trip_contact, i0, R.drawable.icon_user_grey, i1, -1, a2.getString(R.string.contact_person, a3), s);
        }
        a0.extras = new java.util.HashMap();
        a0.extras.put("INITIAL", a.initials);
        return a0;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        return null;
    }
    
    public int getInitialSelection() {
        int i = 0;
        boolean b = this.positionOnFirstManeuver;
        label1: {
            label0: {
                if (!b) {
                    break label0;
                }
                this.positionOnFirstManeuver = false;
                if (this.cachedList == null) {
                    break label0;
                }
                i = this.getFirstManeuverIndex();
                if (i <= 0) {
                    break label0;
                }
                sLogger.v(new StringBuilder().append("initialSelection-m:").append(i).toString());
                break label1;
            }
            if (this.currentSelection == -1) {
                i = 1;
            } else {
                sLogger.v(new StringBuilder().append("initialSelection:").append(this.currentSelection).toString());
                i = this.currentSelection;
                this.currentSelection = -1;
            }
        }
        return i;
    }
    
    public java.util.List getItems() {
        int i = 0;
        this.getItemsCalled = true;
        this.phoneNumber = null;
        this.contactList = null;
        this.label = null;
        java.util.ArrayList a = new java.util.ArrayList();
        ((java.util.List)a).add(back);
        ((java.util.List)a).add(infoModel);
        com.navdy.hud.app.maps.here.HereNavigationManager a0 = com.navdy.hud.app.maps.here.HereNavigationManager.getInstance();
        boolean b = a0.isNavigationModeOn();
        if (b) {
            com.navdy.service.library.events.navigation.NavigationRouteRequest a1 = a0.getCurrentNavigationRouteRequest();
            label0: if (a1 != null && a1.requestDestination != null) {
                java.util.List a2 = a1.requestDestination.phoneNumbers;
                label1: {
                    label2: {
                        if (a2 == null) {
                            break label2;
                        }
                        if (a2.size() > 0) {
                            break label1;
                        }
                    }
                    if (a1.requestDestination.contacts == null) {
                        break label0;
                    }
                    if (a1.requestDestination.contacts.size() <= 0) {
                        break label0;
                    }
                    this.contactList = (java.util.List)new java.util.ArrayList(a1.requestDestination.contacts.size());
                    Object a3 = a1.requestDestination.contacts.iterator();
                    while(((java.util.Iterator)a3).hasNext()) {
                        com.navdy.service.library.events.contacts.Contact a4 = (com.navdy.service.library.events.contacts.Contact)((java.util.Iterator)a3).next();
                        this.contactList.add(new com.navdy.hud.app.framework.contacts.Contact(a4));
                    }
                    ((java.util.List)a).add(this.buildContactModel((com.navdy.hud.app.framework.contacts.Contact)this.contactList.get(0), this.contactList.size()));
                    break label0;
                }
                com.navdy.service.library.events.contacts.PhoneNumber a5 = (com.navdy.service.library.events.contacts.PhoneNumber)a2.get(0);
                int i0 = resources.getColor(R.color.mm_places);
                if (android.text.TextUtils.isEmpty((CharSequence)a1.label)) {
                    if (android.text.TextUtils.isEmpty((CharSequence)a1.streetAddress)) {
                        this.label = "";
                    } else {
                        this.label = a1.streetAddress;
                    }
                } else {
                    this.label = a1.label;
                }
                int i1 = com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected;
                android.content.res.Resources a6 = resources;
                Object[] a7 = new Object[1];
                a7[0] = this.label;
                ((java.util.List)a).add(com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_call, R.drawable.icon_mm_contacts_2, i0, i1, i0, a6.getString(R.string.call_person, a7), com.navdy.hud.app.util.PhoneUtil.formatPhoneNumber(a5.number)));
                this.phoneNumber = a5;
            }
            ((java.util.List)a).add(endTrip);
            if (!com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().isDefaultProfile()) {
                if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getNavigationSessionPreference().spokenTurnByTurn) {
                    muteTbtAudio.title = resources.getString(R.string.mute_tbt_2);
                    ((java.util.List)a).add(muteTbtAudio);
                } else {
                    unmuteTbtAudio.title = resources.getString(R.string.unmute_tbt_2);
                    ((java.util.List)a).add(unmuteTbtAudio);
                }
            }
        }
        if (b && com.navdy.hud.app.util.ReportIssueService.canReportIssue()) {
            ((java.util.List)a).add(reportIssue);
        }
        if (b && !com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().hasArrived()) {
            this.buildManeuvers((java.util.List)a);
        }
        this.presenter.showScrimCover();
        this.cachedList = (java.util.List)a;
        sLogger.v(new StringBuilder().append("getItems:").append(((java.util.List)a).size()).toString());
        if (this.presenter.isMapShown()) {
            this.presenter.enableMapViews();
            i = 100;
        } else {
            this.presenter.showMap();
            i = 1000;
        }
        handler.postDelayed((Runnable)new com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu$1(this), (long)i);
        return (java.util.List)a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        java.util.List a0 = this.cachedList;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.cachedList.size() > i) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(i);
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.ACTIVE_TRIP;
    }
    
    public boolean isBindCallsEnabled() {
        return true;
    }
    
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    public boolean isItemClickable(int i, int i0) {
        boolean b = false;
        label0: if (i0 != 1) {
            com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = this.getModelfromPos(i0);
            label1: {
                if (a == null) {
                    break label1;
                }
                switch(a.id) {
                    default: {
                        break;
                    }
                    case R.id.active_trip_maneuver: case R.id.active_trip_maneuver_title: {
                        b = false;
                        break label0;
                    }
                }
            }
            b = true;
        } else {
            b = false;
        }
        return b;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
        if (a.type == com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.SCROLL_CONTENT) {
            android.view.ViewGroup a2 = (android.view.ViewGroup)((android.view.ViewGroup)a0).getChildAt(0);
            this.activeTripTitleView = (android.widget.TextView)a2.findViewById(R.id.active_trip_title);
            this.activeTripSubTitleView = (android.widget.TextView)a2.findViewById(R.id.active_trip_subtitle);
            this.activeTripDuration = (android.widget.TextView)a2.findViewById(R.id.active_trip_duration);
            this.activeTripDistance = (android.widget.TextView)a2.findViewById(R.id.active_trip_distance);
            this.activeTripEta = (android.widget.TextView)a2.findViewById(R.id.active_trip_eta);
            this.activeTripProgress = a2.findViewById(R.id.active_trip_progress);
            this.etaProgressDrawable = new com.navdy.hud.app.view.drawable.ETAProgressDrawable(this.activeTripProgress.getContext());
            this.etaProgressDrawable.setMinValue(0.0f);
            this.etaProgressDrawable.setMaxGaugeValue(100f);
            this.activeTripProgress.setBackground((android.graphics.drawable.Drawable)this.etaProgressDrawable);
            this.update();
            if (!this.registered) {
                this.bus.register(this);
                this.registered = true;
                sLogger.v("registered bus");
            }
            com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().postManeuverDisplay();
        }
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        label2: if (a.id != R.id.active_trip_maneuver) {
            if (this.maneuverSelected) {
                this.maneuverSelected = false;
                this.presenter.showBoundingBox(this.presenter.getCurrentRouteBoundingBox());
                sLogger.v("route bbox");
            }
        } else {
            this.maneuverSelected = true;
            com.navdy.hud.app.ui.component.vlist.VerticalList$Model a0 = this.getModelfromPos(a.pos);
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (a0.id != R.id.active_trip_maneuver) {
                        break label1;
                    }
                    if (a0.state instanceof com.here.android.mpa.common.GeoBoundingBox) {
                        break label0;
                    }
                }
                sLogger.v("leg - no bbox");
                break label2;
            }
            this.presenter.showBoundingBox((com.here.android.mpa.common.GeoBoundingBox)a0.state);
        }
    }
    
    public void onManeuverChangeEvent(com.navdy.hud.app.maps.MapEvents$ManeuverEvent a) {
        sLogger.v(new StringBuilder().append("onManeuverChangeEvent:").append(a.type).toString());
        if (com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu$6.$SwitchMap$com$navdy$hud$app$maps$MapEvents$ManeuverEvent$Type[a.type.ordinal()] != 0) {
            int i = this.presenter.getCurrentSelection();
            if (i > 1 && i >= this.getFirstManeuverIndex()) {
                this.positionOnFirstManeuver = true;
            }
            this.currentSelection = i;
            this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.REFRESH_CURRENT, 0, 0, true);
        }
    }
    
    public void onManeuverDisplayEvent(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        if (this.isMenuLoaded() && !a.isEmpty() && a.isNavigating()) {
            if (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().hasArrived()) {
                if (this.arrived) {
                    sLogger.v("already arrived");
                } else {
                    this.arrived = true;
                    this.currentSelection = -1;
                    this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.REFRESH_CURRENT, 0, 0, true);
                }
            } else if (a.etaDate != null) {
                String s = null;
                int i = 0;
                String s0 = null;
                this.fillDurationVia(a.etaDate);
                if (timeHelper.getFormat() != com.navdy.service.library.events.settings.DateTimeConfiguration$Clock.CLOCK_24_HOUR) {
                    timeBuilder.setLength(0);
                    String s1 = timeHelper.formatTime12Hour(a.etaDate, timeBuilder, false);
                    s = new StringBuilder().append(s1).append(" ").append(timeBuilder.toString()).toString();
                } else {
                    s = a.eta;
                }
                if (s == null) {
                    this.activeTripEta.setText((CharSequence)"");
                } else {
                    this.activeTripEta.setText((CharSequence)s);
                }
                float f = com.navdy.hud.app.maps.util.DistanceConverter.convertToMeters(a.totalDistance, a.totalDistanceUnit);
                float f0 = f - com.navdy.hud.app.maps.util.DistanceConverter.convertToMeters(a.totalDistanceRemaining, a.totalDistanceRemainingUnit);
                int i0 = (f > 0.0f) ? 1 : (f == 0.0f) ? 0 : -1;
                label2: {
                    label0: {
                        label1: {
                            if (i0 <= 0) {
                                break label1;
                            }
                            if (f > f0) {
                                break label0;
                            }
                        }
                        i = 0;
                        break label2;
                    }
                    i = (int)(f0 / f * 100f);
                }
                if (this.etaProgressDrawable != null) {
                    this.etaProgressDrawable.setGaugeValue((float)i);
                    this.activeTripProgress.invalidate();
                }
                switch(com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu$6.$SwitchMap$com$navdy$service$library$events$navigation$DistanceUnit[a.totalDistanceRemainingUnit.ordinal()]) {
                    case 4: {
                        s0 = feetLabel;
                        break;
                    }
                    case 3: {
                        s0 = milesLabel;
                        break;
                    }
                    case 2: {
                        s0 = kiloMetersLabel;
                        break;
                    }
                    case 1: {
                        s0 = metersLabel;
                        break;
                    }
                    default: {
                        s0 = "";
                    }
                }
                String s2 = new StringBuilder().append(Float.toString(a.totalDistanceRemaining)).append(" ").append(s0).toString();
                this.activeTripDistance.setText((CharSequence)s2);
            }
        }
    }
    
    public void onRouteChangeEvent(com.navdy.service.library.events.navigation.NavigationSessionRouteChange a) {
        int i = this.presenter.getCurrentSelection();
        sLogger.v(new StringBuilder().append("onRouteChangeEvent reason=").append(a.reason).append(" curPos=").append(i).toString());
        if (i > 1) {
            int i0 = this.getFirstManeuverIndex();
            if (i < i0) {
                sLogger.v(new StringBuilder().append("onRouteChangeEvent not showing maneuver curPos=").append(i).append(" maneuverPos=").append(i0).toString());
            } else {
                this.positionOnFirstManeuver = true;
                sLogger.v(new StringBuilder().append("onRouteChangeEvent showing maneuver curPos=").append(i).append(" maneuverPos=").append(i0).toString());
            }
        }
        this.currentSelection = i;
        this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.REFRESH_CURRENT, 0, 0, true);
    }
    
    public void onScrollIdle() {
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
        this.getItemsCalled = false;
        this.presenter.hideScrimCover();
        if (this.presenter.isMapShown()) {
            this.presenter.setViewBackgroundColor(-16777216);
            this.presenter.disableMapViews();
        }
        if (this.registered) {
            this.registered = false;
            this.bus.unregister(this);
            sLogger.v("unregistered bus");
        }
        this.activeTripTitleView = null;
        this.activeTripSubTitleView = null;
        this.activeTripDuration = null;
        this.activeTripDistance = null;
        this.activeTripEta = null;
        this.activeTripProgress = null;
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        sLogger.v(new StringBuilder().append("select id:").append(a.id).append(" pos:").append(a.pos).toString());
        switch(a.id) {
            case R.id.menu_call: {
                sLogger.v("call place");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu$5(this));
                break;
            }
            case R.id.menu_back: {
                sLogger.v("back");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
                int i = com.navdy.hud.app.ui.component.vlist.viewholder.IconOptionsViewHolder.getOffSetFromPos(((com.navdy.hud.app.ui.component.mainmenu.MainMenu)this.parent).getActivityTraySelection());
                this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, 0, false, i);
                break;
            }
            case R.id.main_menu_options_unmute_tbt: {
                sLogger.v("unmute tbt");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordOptionSelection("unmute_Turn_By_Turn");
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu$4(this), 1000);
                unmuteTbtAudio.title = resources.getString(R.string.navigation_unmuted);
                this.vscrollComponent.verticalList.unlock(false);
                this.presenter.refreshDataforPos(a.pos, false);
                this.vscrollComponent.verticalList.lock();
                this.setSpokenTurnByTurn(true);
                break;
            }
            case R.id.main_menu_options_report_issue: {
                sLogger.v("report-issue");
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("report-issue");
                if (this.reportIssueMenu == null) {
                    this.reportIssueMenu = new com.navdy.hud.app.ui.component.mainmenu.ReportIssueMenu(this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this);
                }
                this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.reportIssueMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                break;
            }
            case R.id.main_menu_options_mute_tbt: {
                sLogger.v("mute tbt");
                if (this.presenter.isMapShown()) {
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordOptionSelection("mute_Turn_By_Turn");
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu$3(this), 1000);
                muteTbtAudio.title = resources.getString(R.string.navigation_muted);
                this.vscrollComponent.verticalList.unlock(false);
                this.presenter.refreshDataforPos(a.pos, false);
                this.vscrollComponent.verticalList.lock();
                this.setSpokenTurnByTurn(false);
                break;
            }
            case R.id.main_menu_options_end_trip: {
                sLogger.v("end trip");
                if (this.presenter.isMapShown()) {
                    this.presenter.setNavEnded();
                    this.presenter.setViewBackgroundColor(-16777216);
                }
                com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("End_Trip");
                this.presenter.performSelectionAnimation((Runnable)new com.navdy.hud.app.ui.component.mainmenu.ActiveTripMenu$2(this));
                break;
            }
            case R.id.active_trip_contact: {
                sLogger.v("contact");
                if (this.contactOptionsMenu == null) {
                    this.contactOptionsMenu = new com.navdy.hud.app.ui.component.mainmenu.ContactOptionsMenu(this.contactList, this.vscrollComponent, this.presenter, (com.navdy.hud.app.ui.component.mainmenu.IMenu)this, this.bus);
                    this.contactOptionsMenu.setScrollModel(true);
                }
                this.presenter.loadMenu((com.navdy.hud.app.ui.component.mainmenu.IMenu)this.contactOptionsMenu, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.SUB_LEVEL, a.pos, 0);
                break;
            }
        }
        return true;
    }
    
    public void setBackSelectionId(int i) {
    }
    
    public void setBackSelectionPos(int i) {
        this.backSelection = i;
    }
    
    public void setSelectedIcon() {
        int i = tripOptionsColor;
        String s = activeTripTitle;
        this.vscrollComponent.setSelectedIconColorImage(R.drawable.icon_badge_active_trip, i, (android.graphics.Shader)null, 1f);
        this.vscrollComponent.selectedText.setText((CharSequence)s);
    }
    
    public void showToolTip() {
    }
}
