package com.navdy.hud.app.ui.component.mainmenu;

class MainOptionsMenu$2 implements Runnable {
    final com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu this$0;
    
    MainOptionsMenu$2(com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.access$000(this.this$0).close();
        com.navdy.service.library.events.preferences.LocalPreferences a = com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.access$100(this.this$0).getLocalPreferences();
        com.navdy.hud.app.analytics.AnalyticsSupport.recordOptionSelection("Auto_Zoom");
        com.navdy.service.library.events.preferences.LocalPreferences a0 = new com.navdy.service.library.events.preferences.LocalPreferences$Builder(a).manualZoom(Boolean.valueOf(false)).manualZoomLevel(Float.valueOf(-1f)).build();
        com.navdy.hud.app.ui.component.mainmenu.MainOptionsMenu.access$100(this.this$0).updateLocalPreferences(a0);
    }
}
