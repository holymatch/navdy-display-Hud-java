package com.navdy.hud.app.ui.component.vlist;

public class VerticalList$FontInfo {
    public float subTitle2FontSize;
    public float subTitle2FontTopMargin;
    public float subTitleFontSize;
    public float subTitleFontTopMargin;
    public float titleFontSize;
    public float titleFontTopMargin;
    public float titleScale;
    public boolean titleSingleLine;
    
    public VerticalList$FontInfo() {
        this.titleSingleLine = true;
    }
}
