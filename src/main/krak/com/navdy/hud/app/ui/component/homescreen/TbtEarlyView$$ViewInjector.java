package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class TbtEarlyView$$ViewInjector {
    public TbtEarlyView$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.homescreen.TbtEarlyView a0, Object a1) {
        a0.earlyManeuverImage = (android.widget.ImageView)a.findRequiredView(a1, R.id.earlyManeuverImage, "field 'earlyManeuverImage'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.homescreen.TbtEarlyView a) {
        a.earlyManeuverImage = null;
    }
}
