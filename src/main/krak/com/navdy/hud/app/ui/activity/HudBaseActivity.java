package com.navdy.hud.app.ui.activity;

abstract public class HudBaseActivity extends com.navdy.hud.app.common.BaseActivity {
    @Inject
    com.navdy.hud.app.manager.InputManager inputManager;
    
    public HudBaseActivity() {
    }
    
    protected void makeImmersive(boolean b) {
        android.view.Window a = this.getWindow();
        a.addFlags(b ? 4718720 : 4718592);
        a.getDecorView().setSystemUiVisibility(5894);
        this.logger.v("setting immersive mode");
    }
    
    public boolean onKeyDown(int i, android.view.KeyEvent a) {
        this.logger.v(new StringBuilder().append("keydown:").append(i).toString());
        return this.inputManager != null && this.inputManager.onKeyDown(i, a) || super.onKeyDown(i, a);
    }
    
    public boolean onKeyLongPress(int i, android.view.KeyEvent a) {
        return this.inputManager != null && this.inputManager.onKeyLongPress(i, a) || super.onKeyLongPress(i, a);
    }
    
    public boolean onKeyUp(int i, android.view.KeyEvent a) {
        return this.inputManager != null && this.inputManager.onKeyUp(i, a) || super.onKeyUp(i, a);
    }
}
