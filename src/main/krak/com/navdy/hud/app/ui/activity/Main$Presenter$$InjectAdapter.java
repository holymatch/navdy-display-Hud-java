package com.navdy.hud.app.ui.activity;

final public class Main$Presenter$$InjectAdapter extends dagger.internal.Binding implements javax.inject.Provider, dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding callManager;
    private dagger.internal.Binding connectionHandler;
    private dagger.internal.Binding dialSimulatorMessagesHandler;
    private dagger.internal.Binding driverProfileManager;
    private dagger.internal.Binding globalPreferences;
    private dagger.internal.Binding inputManager;
    private dagger.internal.Binding musicManager;
    private dagger.internal.Binding pandoraManager;
    private dagger.internal.Binding powerManager;
    private dagger.internal.Binding preferences;
    private dagger.internal.Binding settingsManager;
    private dagger.internal.Binding supertype;
    private dagger.internal.Binding tripManager;
    private dagger.internal.Binding uiStateManager;
    
    public Main$Presenter$$InjectAdapter() {
        super("com.navdy.hud.app.ui.activity.Main$Presenter", "members/com.navdy.hud.app.ui.activity.Main$Presenter", true, com.navdy.hud.app.ui.activity.Main$Presenter.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader());
        this.inputManager = a.requestBinding("com.navdy.hud.app.manager.InputManager", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader());
        this.connectionHandler = a.requestBinding("com.navdy.hud.app.service.ConnectionHandler", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader());
        this.uiStateManager = a.requestBinding("com.navdy.hud.app.ui.framework.UIStateManager", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader());
        this.callManager = a.requestBinding("com.navdy.hud.app.framework.phonecall.CallManager", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader());
        this.musicManager = a.requestBinding("com.navdy.hud.app.manager.MusicManager", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader());
        this.pandoraManager = a.requestBinding("com.navdy.hud.app.service.pandora.PandoraManager", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader());
        this.dialSimulatorMessagesHandler = a.requestBinding("com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader());
        this.globalPreferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader());
        this.settingsManager = a.requestBinding("com.navdy.hud.app.config.SettingsManager", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader());
        this.powerManager = a.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader());
        this.driverProfileManager = a.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader());
        this.tripManager = a.requestBinding("com.navdy.hud.app.framework.trips.TripManager", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader());
        this.preferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader());
        this.supertype = a.requestBinding("members/com.navdy.hud.app.ui.framework.BasePresenter", com.navdy.hud.app.ui.activity.Main$Presenter.class, (this).getClass().getClassLoader(), false, true);
    }
    
    public com.navdy.hud.app.ui.activity.Main$Presenter get() {
        com.navdy.hud.app.ui.activity.Main$Presenter a = new com.navdy.hud.app.ui.activity.Main$Presenter();
        this.injectMembers(a);
        return a;
    }
    
    public Object get() {
        return this.get();
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.bus);
        a0.add(this.inputManager);
        a0.add(this.connectionHandler);
        a0.add(this.uiStateManager);
        a0.add(this.callManager);
        a0.add(this.musicManager);
        a0.add(this.pandoraManager);
        a0.add(this.dialSimulatorMessagesHandler);
        a0.add(this.globalPreferences);
        a0.add(this.settingsManager);
        a0.add(this.powerManager);
        a0.add(this.driverProfileManager);
        a0.add(this.tripManager);
        a0.add(this.preferences);
        a0.add(this.supertype);
    }
    
    public void injectMembers(com.navdy.hud.app.ui.activity.Main$Presenter a) {
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        a.inputManager = (com.navdy.hud.app.manager.InputManager)this.inputManager.get();
        a.connectionHandler = (com.navdy.hud.app.service.ConnectionHandler)this.connectionHandler.get();
        a.uiStateManager = (com.navdy.hud.app.ui.framework.UIStateManager)this.uiStateManager.get();
        a.callManager = (com.navdy.hud.app.framework.phonecall.CallManager)this.callManager.get();
        a.musicManager = (com.navdy.hud.app.manager.MusicManager)this.musicManager.get();
        a.pandoraManager = (com.navdy.hud.app.service.pandora.PandoraManager)this.pandoraManager.get();
        a.dialSimulatorMessagesHandler = (com.navdy.hud.app.device.dial.DialSimulatorMessagesHandler)this.dialSimulatorMessagesHandler.get();
        a.globalPreferences = (android.content.SharedPreferences)this.globalPreferences.get();
        a.settingsManager = (com.navdy.hud.app.config.SettingsManager)this.settingsManager.get();
        a.powerManager = (com.navdy.hud.app.device.PowerManager)this.powerManager.get();
        a.driverProfileManager = (com.navdy.hud.app.profile.DriverProfileManager)this.driverProfileManager.get();
        a.tripManager = (com.navdy.hud.app.framework.trips.TripManager)this.tripManager.get();
        a.preferences = (android.content.SharedPreferences)this.preferences.get();
        this.supertype.injectMembers(a);
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.ui.activity.Main$Presenter)a);
    }
}
