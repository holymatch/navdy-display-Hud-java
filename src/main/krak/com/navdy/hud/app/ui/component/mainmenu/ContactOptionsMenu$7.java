package com.navdy.hud.app.ui.component.mainmenu;

class ContactOptionsMenu$7 {
    final static int[] $SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform;
    
    static {
        $SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform = new int[com.navdy.service.library.events.DeviceInfo$Platform.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform;
        com.navdy.service.library.events.DeviceInfo$Platform a0 = com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_Android;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$DeviceInfo$Platform[com.navdy.service.library.events.DeviceInfo$Platform.PLATFORM_iOS.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
    }
}
