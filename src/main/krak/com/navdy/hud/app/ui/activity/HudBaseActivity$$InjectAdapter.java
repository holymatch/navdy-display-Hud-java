package com.navdy.hud.app.ui.activity;

final public class HudBaseActivity$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding inputManager;
    private dagger.internal.Binding supertype;
    
    public HudBaseActivity$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.ui.activity.HudBaseActivity", false, com.navdy.hud.app.ui.activity.HudBaseActivity.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.inputManager = a.requestBinding("com.navdy.hud.app.manager.InputManager", com.navdy.hud.app.ui.activity.HudBaseActivity.class, (this).getClass().getClassLoader());
        this.supertype = a.requestBinding("members/com.navdy.hud.app.common.BaseActivity", com.navdy.hud.app.ui.activity.HudBaseActivity.class, (this).getClass().getClassLoader(), false, true);
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.inputManager);
        a0.add(this.supertype);
    }
    
    public void injectMembers(com.navdy.hud.app.ui.activity.HudBaseActivity a) {
        a.inputManager = (com.navdy.hud.app.manager.InputManager)this.inputManager.get();
        this.supertype.injectMembers(a);
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.ui.activity.HudBaseActivity)a);
    }
}
