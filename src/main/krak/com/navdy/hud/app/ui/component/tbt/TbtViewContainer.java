package com.navdy.hud.app.ui.component.tbt;
import com.navdy.hud.app.R;

final public class TbtViewContainer extends android.widget.FrameLayout implements com.navdy.hud.app.ui.component.homescreen.IHomeScreenLifecycle {
    final public static com.navdy.hud.app.ui.component.tbt.TbtViewContainer$Companion Companion;
    final private static long MANEUVER_PROGRESS_ANIMATION_DURATION = 250L;
    final private static long MANEUVER_SWAP_DELAY = 300L;
    final private static long MANEUVER_SWAP_DURATION = 500L;
    final private static com.squareup.otto.Bus bus;
    final private static float itemHeight;
    final private static com.navdy.service.library.log.Logger logger;
    final private static android.view.animation.AccelerateDecelerateInterpolator maneuverSwapInterpolator;
    final private static com.navdy.hud.app.manager.RemoteDeviceManager remoteDeviceManager;
    final private static android.content.res.Resources resources;
    final private static com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    private java.util.HashMap _$_findViewCache;
    private com.navdy.hud.app.ui.component.tbt.TbtView activeView;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreenView homeScreenView;
    private com.navdy.hud.app.ui.component.tbt.TbtView inactiveView;
    private com.navdy.hud.app.maps.MapEvents$ManeuverDisplay lastEvent;
    private com.navdy.hud.app.maps.MapEvents$ManeuverDisplay lastManeuverDisplay;
    private String lastManeuverId;
    private com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder$ManeuverState lastManeuverState;
    private com.navdy.hud.app.view.MainView$CustomAnimationMode lastMode;
    private String lastRoadText;
    private int lastTurnIconId;
    private String lastTurnText;
    private boolean paused;
    
    static {
        Companion = new com.navdy.hud.app.ui.component.tbt.TbtViewContainer$Companion((kotlin.jvm.internal.DefaultConstructorMarker)null);
        logger = new com.navdy.service.library.log.Logger("TbtViewContainer");
        android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a, "HudApplication.getAppContext().resources");
        resources = a;
        itemHeight = Companion.getResources().getDimension(R.dimen.tbt_view_ht);
        MANEUVER_PROGRESS_ANIMATION_DURATION = 250L;
        MANEUVER_SWAP_DURATION = 500L;
        MANEUVER_SWAP_DELAY = 300L;
        maneuverSwapInterpolator = new android.view.animation.AccelerateDecelerateInterpolator();
        com.navdy.hud.app.manager.RemoteDeviceManager a0 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a0, "RemoteDeviceManager.getInstance()");
        remoteDeviceManager = a0;
        com.squareup.otto.Bus a1 = Companion.getRemoteDeviceManager().getBus();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a1, "remoteDeviceManager.bus");
        bus = a1;
        com.navdy.hud.app.ui.framework.UIStateManager a2 = Companion.getRemoteDeviceManager().getUiStateManager();
        kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a2, "remoteDeviceManager.uiStateManager");
        uiStateManager = a2;
    }
    
    public TbtViewContainer(android.content.Context a) {
        super(a);
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        this.lastTurnIconId = -1;
    }
    
    public TbtViewContainer(android.content.Context a, android.util.AttributeSet a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "attrs");
        super(a, a0);
        this.lastTurnIconId = -1;
    }
    
    public TbtViewContainer(android.content.Context a, android.util.AttributeSet a0, int i) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "context");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "attrs");
        super(a, a0, i);
        this.lastTurnIconId = -1;
    }
    
    final public static com.squareup.otto.Bus access$getBus$cp() {
        return bus;
    }
    
    final public static com.navdy.hud.app.ui.component.tbt.TbtView access$getInactiveView$p(com.navdy.hud.app.ui.component.tbt.TbtViewContainer a) {
        return a.inactiveView;
    }
    
    final public static float access$getItemHeight$cp() {
        return itemHeight;
    }
    
    final public static com.navdy.service.library.log.Logger access$getLogger$cp() {
        return logger;
    }
    
    final public static long access$getMANEUVER_PROGRESS_ANIMATION_DURATION$cp() {
        return MANEUVER_PROGRESS_ANIMATION_DURATION;
    }
    
    final public static long access$getMANEUVER_SWAP_DELAY$cp() {
        return MANEUVER_SWAP_DELAY;
    }
    
    final public static long access$getMANEUVER_SWAP_DURATION$cp() {
        return MANEUVER_SWAP_DURATION;
    }
    
    final public static android.view.animation.AccelerateDecelerateInterpolator access$getManeuverSwapInterpolator$cp() {
        return maneuverSwapInterpolator;
    }
    
    final public static com.navdy.hud.app.manager.RemoteDeviceManager access$getRemoteDeviceManager$cp() {
        return remoteDeviceManager;
    }
    
    final public static android.content.res.Resources access$getResources$cp() {
        return resources;
    }
    
    final public static com.navdy.hud.app.ui.framework.UIStateManager access$getUiStateManager$cp() {
        return uiStateManager;
    }
    
    final public static void access$setInactiveView$p(com.navdy.hud.app.ui.component.tbt.TbtViewContainer a, com.navdy.hud.app.ui.component.tbt.TbtView a0) {
        a.inactiveView = a0;
    }
    
    public void _$_clearFindViewByIdCache() {
        if (this._$_findViewCache != null) {
            this._$_findViewCache.clear();
        }
    }
    
    public android.view.View _$_findCachedViewById(int i) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new java.util.HashMap();
        }
        android.view.View a = (android.view.View)this._$_findViewCache.get(Integer.valueOf(i));
        if (a == null) {
            a = this.findViewById(i);
            this._$_findViewCache.put(Integer.valueOf(i), a);
        }
        return a;
    }
    
    final public void clearState() {
        this.lastTurnIconId = -1;
        this.lastTurnText = null;
        this.lastRoadText = null;
        com.navdy.hud.app.ui.component.tbt.TbtView a = this.activeView;
        if (a != null) {
            a.clear();
        }
        com.navdy.hud.app.ui.component.tbt.TbtView a0 = this.inactiveView;
        if (a0 != null) {
            a0.clear();
        }
    }
    
    final public void getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a, android.animation.AnimatorSet$Builder a0) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "mode");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "mainBuilder");
        this.lastMode = a;
        com.navdy.hud.app.ui.component.tbt.TbtView a1 = this.activeView;
        if (a1 != null) {
            a1.getCustomAnimator(a, a0);
        }
        com.navdy.hud.app.ui.component.tbt.TbtView a2 = this.inactiveView;
        if (a2 != null) {
            a2.getCustomAnimator(a, a0);
        }
    }
    
    final public com.navdy.hud.app.maps.MapEvents$ManeuverDisplay getLastManeuverDisplay() {
        return this.lastManeuverDisplay;
    }
    
    final public void init(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "homeScreenView");
        this.homeScreenView = a;
        Companion.getBus().register(this);
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        android.view.View a = this.findViewById(R.id.view1);
        if (a == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtView");
        }
        this.activeView = (com.navdy.hud.app.ui.component.tbt.TbtView)a;
        android.view.View a0 = this.findViewById(R.id.view2);
        if (a0 == null) {
            throw new kotlin.TypeCastException("null cannot be cast to non-null type com.navdy.hud.app.ui.component.tbt.TbtView");
        }
        this.inactiveView = (com.navdy.hud.app.ui.component.tbt.TbtView)a0;
        com.navdy.hud.app.ui.component.tbt.TbtView a1 = this.activeView;
        if (a1 != null) {
            a1.setTranslationY(-Companion.getItemHeight());
        }
    }
    
    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            com.navdy.hud.app.ui.component.tbt.TbtViewContainer$Companion.access$getLogger$p(Companion).v("::onPause:tbt");
        }
    }
    
    public void onResume() {
        if (this.paused) {
            this.paused = false;
            com.navdy.hud.app.ui.component.tbt.TbtViewContainer$Companion.access$getLogger$p(Companion).v("::onResume:tbt");
            com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a = this.lastEvent;
            if (a != null) {
                this.lastEvent = null;
                com.navdy.hud.app.ui.component.tbt.TbtViewContainer$Companion.access$getLogger$p(Companion).v("::onResume:tbt updated last event");
                this.updateDisplay(a);
            }
        }
    }
    
    final public void setView(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "mode");
        this.lastMode = a;
        com.navdy.hud.app.ui.component.tbt.TbtView a0 = this.activeView;
        if (a0 != null) {
            a0.setView(a);
        }
        com.navdy.hud.app.ui.component.tbt.TbtView a1 = this.inactiveView;
        if (a1 != null) {
            a1.setView(a);
        }
    }
    
    final public void updateDisplay(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "event");
        this.lastManeuverDisplay = a;
        com.navdy.hud.app.ui.component.homescreen.HomeScreenView a0 = this.homeScreenView;
        if (a0 != null && a0.isNavigationActive()) {
            if (a.empty) {
                this.clearState();
            } else if (a.pendingTurn != null) {
                this.lastEvent = a;
                if (this.paused) {
                    this.lastManeuverId = a.maneuverId;
                } else {
                    if (android.text.TextUtils.equals((CharSequence)this.lastManeuverId, (CharSequence)a.maneuverId)) {
                        com.navdy.hud.app.ui.component.tbt.TbtView a1 = this.activeView;
                        if (a1 != null) {
                            a1.updateDisplay(a);
                        }
                    } else {
                        com.navdy.hud.app.ui.component.tbt.TbtView a2 = this.activeView;
                        this.activeView = this.inactiveView;
                        this.inactiveView = a2;
                        com.navdy.hud.app.ui.component.tbt.TbtView a3 = this.activeView;
                        if (a3 != null) {
                            a3.setTranslationY(-Companion.getItemHeight());
                        }
                        com.navdy.hud.app.ui.component.tbt.TbtView a4 = this.activeView;
                        if (a4 != null) {
                            a4.updateDisplay(a);
                        }
                        com.navdy.hud.app.ui.component.tbt.TbtView a5 = this.activeView;
                        if (a5 != null) {
                            android.view.ViewPropertyAnimator a6 = a5.animate();
                            if (a6 != null) {
                                android.view.ViewPropertyAnimator a7 = a6.translationY(0.0f);
                                if (a7 != null) {
                                    android.view.ViewPropertyAnimator a8 = a7.setDuration(Companion.getMANEUVER_SWAP_DURATION());
                                    if (a8 != null) {
                                        android.view.ViewPropertyAnimator a9 = a8.setStartDelay(Companion.getMANEUVER_SWAP_DELAY());
                                        if (a9 != null) {
                                            a9.start();
                                        }
                                    }
                                }
                            }
                        }
                        com.navdy.hud.app.ui.component.tbt.TbtView a10 = this.inactiveView;
                        if (a10 != null) {
                            android.view.ViewPropertyAnimator a11 = a10.animate();
                            if (a11 != null) {
                                android.view.ViewPropertyAnimator a12 = a11.translationY(Companion.getItemHeight());
                                if (a12 != null) {
                                    android.view.ViewPropertyAnimator a13 = a12.setDuration(Companion.getMANEUVER_SWAP_DURATION());
                                    if (a13 != null) {
                                        android.view.ViewPropertyAnimator a14 = a13.setStartDelay(Companion.getMANEUVER_SWAP_DELAY());
                                        if (a14 != null) {
                                            android.view.ViewPropertyAnimator a15 = a14.setListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.tbt.TbtViewContainer$updateDisplay$1(this));
                                            if (a15 != null) {
                                                a15.start();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    this.lastManeuverId = a.maneuverId;
                    this.lastManeuverState = a.maneuverState;
                }
            } else {
                com.navdy.hud.app.ui.component.tbt.TbtViewContainer$Companion.access$getLogger$p(Companion).v("null pending turn");
                this.lastEvent = null;
                this.lastManeuverId = null;
            }
        }
    }
}
