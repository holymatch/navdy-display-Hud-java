package com.navdy.hud.app.ui.component.homescreen;

public class SmartDashWidgetManager$SmartDashWidgetCache {
    private android.content.Context mContext;
    private java.util.List mGaugeIds;
    private java.util.HashMap mWidgetIndexMap;
    private java.util.HashMap mWidgetPresentersMap;
    final com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager this$0;
    
    public SmartDashWidgetManager$SmartDashWidgetCache(com.navdy.hud.app.ui.component.homescreen.SmartDashWidgetManager a, android.content.Context a0) {
        super();
        this.this$0 = a;
        this.mContext = a0;
        this.mGaugeIds = (java.util.List)new java.util.ArrayList();
        this.mWidgetPresentersMap = new java.util.HashMap();
        this.mWidgetIndexMap = new java.util.HashMap();
    }
    
    private void initializeWidget(String s) {
        int i = 0;
        switch(s.hashCode()) {
            case 1168400042: {
                i = (s.equals("FUEL_GAUGE_ID")) ? 0 : -1;
                break;
            }
            case 460083427: {
                i = (s.equals("DRIVE_SCORE_GAUGE_ID")) ? 1 : -1;
                break;
            }
            case -131933527: {
                i = (s.equals("ENGINE_TEMPERATURE_GAUGE_ID")) ? 2 : -1;
                break;
            }
            default: {
                i = -1;
            }
        }
        switch(i) {
            case 2: {
                com.navdy.hud.app.view.EngineTemperaturePresenter a = (com.navdy.hud.app.view.EngineTemperaturePresenter)this.getWidgetPresenter(s);
                double d = com.navdy.hud.app.obd.ObdManager.getInstance().getPidValue(5);
                if (a == null) {
                    break;
                }
                if (d == -1.0) {
                    d = com.navdy.hud.app.view.EngineTemperaturePresenter.Companion.getTEMPERATURE_GAUGE_MID_POINT_CELSIUS();
                }
                a.setEngineTemperature(d);
                break;
            }
            case 1: {
                com.navdy.hud.app.view.DriveScoreGaugePresenter a0 = (com.navdy.hud.app.view.DriveScoreGaugePresenter)this.getWidgetPresenter(s);
                if (a0 == null) {
                    break;
                }
                a0.setDriveScoreUpdated(com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTelemetryDataManager().getDriveScoreUpdatedEvent());
                break;
            }
            case 0: {
                com.navdy.hud.app.view.FuelGaugePresenter2 a1 = (com.navdy.hud.app.view.FuelGaugePresenter2)this.getWidgetPresenter(s);
                int i0 = com.navdy.hud.app.obd.ObdManager.getInstance().getFuelLevel();
                if (a1 == null) {
                    break;
                }
                a1.setFuelLevel(i0);
                break;
            }
        }
    }
    
    public void add(String s) {
        if (!this.mWidgetIndexMap.containsKey(s)) {
            this.mGaugeIds.add(s);
            this.mWidgetIndexMap.put(s, Integer.valueOf(this.mGaugeIds.size() - 1));
            com.navdy.hud.app.view.DashboardWidgetPresenter a = com.navdy.hud.app.ui.component.homescreen.DashWidgetPresenterFactory.createDashWidgetPresenter(this.mContext, s);
            this.mWidgetPresentersMap.put(s, a);
            this.initializeWidget(s);
        }
    }
    
    public void clear() {
        this.mGaugeIds.clear();
        this.mWidgetPresentersMap.clear();
        this.mWidgetIndexMap.clear();
    }
    
    public int getIndexForWidget(String s) {
        return (this.mWidgetIndexMap.containsKey(s)) ? ((Integer)this.mWidgetIndexMap.get(s)).intValue() : -1;
    }
    
    public com.navdy.hud.app.view.DashboardWidgetPresenter getWidgetPresenter(int i) {
        com.navdy.hud.app.view.DashboardWidgetPresenter a = null;
        label2: {
            label0: {
                label1: {
                    if (i < 0) {
                        break label1;
                    }
                    if (i < this.mGaugeIds.size()) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            String s = (String)this.mGaugeIds.get(i);
            a = (com.navdy.hud.app.view.DashboardWidgetPresenter)this.mWidgetPresentersMap.get(s);
        }
        return a;
    }
    
    public com.navdy.hud.app.view.DashboardWidgetPresenter getWidgetPresenter(String s) {
        return this.getWidgetPresenter(this.getIndexForWidget(s));
    }
    
    public java.util.HashMap getWidgetPresentersMap() {
        return this.mWidgetPresentersMap;
    }
    
    public int getWidgetsCount() {
        return this.mGaugeIds.size();
    }
    
    public void updateWidget(String s, Object a) {
        if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
            int i = 0;
            switch(s.hashCode()) {
                case 1168400042: {
                    i = (s.equals("FUEL_GAUGE_ID")) ? 0 : -1;
                    break;
                }
                case 1119776967: {
                    i = (s.equals("SPEED_LIMIT_SIGN_GAUGE_ID")) ? 3 : -1;
                    break;
                }
                case -131933527: {
                    i = (s.equals("ENGINE_TEMPERATURE_GAUGE_ID")) ? 4 : -1;
                    break;
                }
                case -1158963060: {
                    i = (s.equals("MPG_AVG_WIDGET")) ? 2 : -1;
                    break;
                }
                case -1874661839: {
                    i = (s.equals("COMPASS_WIDGET")) ? 1 : -1;
                    break;
                }
                default: {
                    i = -1;
                }
            }
            switch(i) {
                case 4: {
                    com.navdy.hud.app.view.EngineTemperaturePresenter a0 = (com.navdy.hud.app.view.EngineTemperaturePresenter)this.getWidgetPresenter(s);
                    if (a0 == null) {
                        break;
                    }
                    a0.setEngineTemperature((double)((Number)a).intValue());
                    break;
                }
                case 3: {
                    com.navdy.hud.app.view.SpeedLimitSignPresenter a1 = (com.navdy.hud.app.view.SpeedLimitSignPresenter)this.getWidgetPresenter(s);
                    if (a1 == null) {
                        break;
                    }
                    a1.setSpeedLimit(((Number)a).intValue());
                    break;
                }
                case 2: {
                    com.navdy.hud.app.view.MPGGaugePresenter a2 = (com.navdy.hud.app.view.MPGGaugePresenter)this.getWidgetPresenter(s);
                    if (a2 == null) {
                        break;
                    }
                    a2.setCurrentMPG(((Number)a).doubleValue());
                    break;
                }
                case 1: {
                    com.navdy.hud.app.view.CompassPresenter a3 = (com.navdy.hud.app.view.CompassPresenter)this.getWidgetPresenter(s);
                    if (a3 == null) {
                        break;
                    }
                    a3.setHeadingAngle(((Number)a).doubleValue());
                    break;
                }
                case 0: {
                    com.navdy.hud.app.view.FuelGaugePresenter2 a4 = (com.navdy.hud.app.view.FuelGaugePresenter2)this.getWidgetPresenter(s);
                    if (a4 == null) {
                        break;
                    }
                    if (!(a instanceof Double)) {
                        break;
                    }
                    a4.setFuelLevel(((Number)a).intValue());
                    break;
                }
            }
        }
    }
}
