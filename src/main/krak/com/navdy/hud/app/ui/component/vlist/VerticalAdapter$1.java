package com.navdy.hud.app.ui.component.vlist;

class VerticalAdapter$1 {
    final static int[] $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType;
    
    static {
        $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType = new int[com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.values().length];
        int[] a = $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType;
        com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType a0 = com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.BLANK;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType[com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.TITLE.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType[com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.TITLE_SUBTITLE.ordinal()] = 3;
        } catch(NoSuchFieldError ignoredException1) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType[com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_BKCOLOR.ordinal()] = 4;
        } catch(NoSuchFieldError ignoredException2) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType[com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.TWO_ICONS.ordinal()] = 5;
        } catch(NoSuchFieldError ignoredException3) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType[com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON.ordinal()] = 6;
        } catch(NoSuchFieldError ignoredException4) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType[com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.LOADING.ordinal()] = 7;
        } catch(NoSuchFieldError ignoredException5) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType[com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.ICON_OPTIONS.ordinal()] = 8;
        } catch(NoSuchFieldError ignoredException6) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType[com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.SCROLL_CONTENT.ordinal()] = 9;
        } catch(NoSuchFieldError ignoredException7) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType[com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.LOADING_CONTENT.ordinal()] = 10;
        } catch(NoSuchFieldError ignoredException8) {
        }
        try {
            $SwitchMap$com$navdy$hud$app$ui$component$vlist$VerticalList$ModelType[com.navdy.hud.app.ui.component.vlist.VerticalList$ModelType.SWITCH.ordinal()] = 11;
        } catch(NoSuchFieldError ignoredException9) {
        }
    }
}
