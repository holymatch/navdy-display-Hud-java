package com.navdy.hud.app.ui.component.mainmenu;
import com.navdy.hud.app.R;

public class SystemInfoMenu implements com.navdy.hud.app.ui.component.mainmenu.IMenu {
    final private static int UPDATE_FREQUENCY = 2000;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model back;
    final private static java.text.SimpleDateFormat dateFormat;
    private static String fuelUnitText;
    final private static com.navdy.hud.app.ui.component.vlist.VerticalList$Model infoModel;
    final private static String infoTitle;
    final private static String noData;
    final private static String noFixStr;
    final private static String off;
    final private static String on;
    private static String pressureUnitText;
    final private static android.content.res.Resources resources;
    final private static com.navdy.service.library.log.Logger sLogger;
    @InjectView(R.id.si_auto_on)
    protected android.widget.TextView autoOn;
    private int backSelection;
    private com.squareup.otto.Bus bus;
    private java.util.List cachedList;
    @InjectView(R.id.si_model)
    protected android.widget.TextView carModel;
    @InjectView(R.id.si_check_engine_light)
    protected android.widget.TextView checkEngineLight;
    @InjectView(R.id.si_device_mileage)
    protected android.widget.TextView deviceMileage;
    @InjectView(R.id.si_dial_bt)
    protected android.widget.TextView dialBt;
    @InjectView(R.id.si_dial_fw)
    protected android.widget.TextView dialFw;
    @InjectView(R.id.si_display_bt)
    protected android.widget.TextView displayBt;
    @InjectView(R.id.si_display_fw)
    protected android.widget.TextView displayFw;
    @InjectView(R.id.si_eng_oil_pressure)
    protected android.widget.TextView engineOilPressure;
    @InjectView(R.id.si_engine_oil_pressure_lyt)
    protected android.view.ViewGroup engineOilPressureLayout;
    @InjectView(R.id.si_eng_temp)
    protected android.widget.TextView engineTemp;
    @InjectView(R.id.si_engine_trouble_codes)
    protected android.widget.TextView engineTroubleCodes;
    @InjectView(R.id.si_fix)
    protected android.widget.TextView fix;
    @InjectView(R.id.si_fuel)
    protected android.widget.TextView fuel;
    @InjectView(R.id.si_gestures)
    protected android.widget.TextView gestures;
    @InjectView(R.id.si_gps)
    protected android.widget.TextView gps;
    @InjectView(R.id.si_gps_speed)
    protected android.widget.TextView gpsSpeed;
    private android.os.Handler handler;
    private com.navdy.hud.app.maps.here.HereLocationFixManager hereLocationFixManager;
    @InjectView(R.id.si_maps_data_verified)
    protected android.widget.TextView hereMapVerified;
    @InjectView(R.id.si_map_version)
    protected android.widget.TextView hereMapsVersion;
    @InjectView(R.id.si_offline_maps)
    protected android.widget.TextView hereOfflineMaps;
    @InjectView(R.id.si_here_sdk_version)
    protected android.widget.TextView hereSdk;
    @InjectView(R.id.si_updated)
    protected android.widget.TextView hereUpdated;
    @InjectView(R.id.si_maps_voices_verified)
    protected android.widget.TextView hereVoiceVerified;
    @InjectView(R.id.si_maf)
    protected android.widget.TextView maf;
    @InjectView(R.id.si_make)
    protected android.widget.TextView make;
    @InjectView(R.id.si_obd_data)
    protected android.widget.TextView obdData;
    @InjectView(R.id.obd_data_lyt)
    protected android.view.ViewGroup obdDataLayout;
    @InjectView(R.id.si_obd_fw)
    protected android.widget.TextView obdFw;
    private com.navdy.hud.app.obd.ObdManager obdManager;
    @InjectView(R.id.si_odometer)
    protected android.widget.TextView odo;
    @InjectView(R.id.si_odometer_lyt)
    protected android.view.ViewGroup odometerLayout;
    private com.navdy.hud.app.ui.component.mainmenu.IMenu parent;
    private com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter presenter;
    protected boolean registered;
    @InjectView(R.id.si_route_calc)
    protected android.widget.TextView routeCalc;
    @InjectView(R.id.si_route_pref)
    protected android.widget.TextView routePref;
    @InjectView(R.id.si_rpm)
    protected android.widget.TextView rpm;
    @InjectView(R.id.gps_signal)
    com.navdy.hud.app.ui.component.systeminfo.GpsSignalView satelliteView;
    @InjectView(R.id.si_satellites)
    protected android.widget.TextView satellites;
    @InjectView(R.id.si_special_pids_lyt)
    protected android.view.ViewGroup specialPidsLayout;
    @InjectView(R.id.si_speed)
    protected android.widget.TextView speed;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    @InjectView(R.id.si_temperature)
    protected android.widget.TextView temperature;
    @InjectView(R.id.si_eng_trip_fuel)
    protected android.widget.TextView tripFuel;
    @InjectView(R.id.si_trip_fuel_layout)
    protected android.view.ViewGroup tripFuelLayout;
    protected String ubloxFixType;
    @InjectView(R.id.si_ui_scaling)
    protected android.widget.TextView uiScaling;
    private Runnable updateRunnable;
    @InjectView(R.id.si_voltage)
    protected android.widget.TextView voltage;
    private com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent vscrollComponent;
    @InjectView(R.id.si_year)
    protected android.widget.TextView year;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu.class);
        dateFormat = new java.text.SimpleDateFormat("MM/dd/yyyy");
        resources = com.navdy.hud.app.HudApplication.getAppContext().getResources();
        noFixStr = resources.getString(R.string.gps_no_fix);
        noData = resources.getString(R.string.si_no_data);
        on = resources.getString(R.string.si_on);
        off = resources.getString(R.string.si_off);
        int i = resources.getColor(R.color.mm_back);
        infoTitle = resources.getString(R.string.carousel_menu_system_info_title);
        pressureUnitText = resources.getString(R.string.pressure_unit);
        fuelUnitText = resources.getString(R.string.litre);
        String s = resources.getString(R.string.back);
        back = com.navdy.hud.app.ui.component.vlist.viewholder.IconBkColorViewHolder.buildModel(R.id.menu_back, R.drawable.icon_mm_back, i, com.navdy.hud.app.ui.component.mainmenu.MainMenu.bkColorUnselected, i, s, (String)null);
        infoModel = com.navdy.hud.app.ui.component.vlist.viewholder.ScrollableViewHolder.buildModel(R.layout.system_info_lyt);
    }
    
    SystemInfoMenu(com.squareup.otto.Bus a, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a0, com.navdy.hud.app.ui.component.mainmenu.MainMenuScreen2$Presenter a1, com.navdy.hud.app.ui.component.mainmenu.IMenu a2) {
        this.updateRunnable = (Runnable)new com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu$1(this);
        this.bus = a;
        this.vscrollComponent = a0;
        this.presenter = a1;
        this.parent = a2;
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.obdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
    }
    
    static boolean access$000(com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu a) {
        return a.isMenuLoaded();
    }
    
    static void access$100(com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu a) {
        a.updateTemperature();
    }
    
    static void access$200(com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu a) {
        a.updateVoltage();
    }
    
    static void access$300(com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu a) {
        a.setFix();
    }
    
    static android.os.Handler access$400(com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu a) {
        return a.handler;
    }
    
    private void formatRoutePref(StringBuilder a, String s) {
        boolean b = false;
        if (a.length() <= 0) {
            b = true;
        } else {
            a.append(",");
            a.append(" ");
            b = false;
        }
        if (b) {
            StringBuilder a0 = new StringBuilder();
            int i = s.charAt(0);
            int i0 = Character.toUpperCase((char)i);
            s = a0.append((char)i0).append(s.substring(1)).toString();
        }
        a.append(s);
    }
    
    private static String getFixTypeDescription(int i) {
        String s = null;
        switch(i) {
            case 6: {
                s = "DR Fix";
                break;
            }
            case 5: {
                s = "RTK Float";
                break;
            }
            case 4: {
                s = "RTK Fixed";
                break;
            }
            case 2: {
                s = "Differential";
                break;
            }
            case 1: {
                s = "Standard 2D/3D";
                break;
            }
            case 0: {
                s = "No Fix";
                break;
            }
            default: {
                s = "Unknown";
            }
        }
        return s;
    }
    
    private boolean isMenuLoaded() {
        return this.dialFw != null;
    }
    
    private String removeBrackets(String s) {
        return s.replace((CharSequence)"(", (CharSequence)"").replace((CharSequence)")", (CharSequence)"");
    }
    
    private void setFix() {
        if (this.hereLocationFixManager != null) {
            android.content.res.Resources a = com.navdy.hud.app.HudApplication.getAppContext().getResources();
            com.navdy.hud.app.maps.MapEvents$LocationFix a0 = this.hereLocationFixManager.getLocationFixEvent();
            sLogger.v(new StringBuilder().append("fix [").append(a0.locationAvailable).append("] phone[").append(a0.usingPhoneLocation).append("] u-blox[").append(a0.usingLocalGpsLocation).append("] type[").append(this.ubloxFixType).append("]").toString());
            if (a0.locationAvailable) {
                if (a0.usingPhoneLocation) {
                    this.fix.setText(a.getText(R.string.gps_phone));
                } else if (a0.usingLocalGpsLocation) {
                    android.widget.TextView a1 = this.fix;
                    Object a2 = (this.ubloxFixType == null) ? a.getText(R.string.gps_ublox_navdy) : this.ubloxFixType;
                    a1.setText((CharSequence)a2);
                } else {
                    this.fix.setText((CharSequence)noFixStr);
                }
            } else {
                this.fix.setText((CharSequence)noFixStr);
            }
        }
    }
    
    private void update() {
        this.updateVersions();
        this.updateStats();
        this.updateHere();
        this.updateVehicleInfo();
        this.updateGenericPref();
        this.updateNavPref();
    }
    
    private void updateDeviceMileage() {
        if (this.presenter != null) {
            long j = this.presenter.tripManager.getTotalDistanceTravelledWithNavdy();
            sLogger.v(new StringBuilder().append("navdy distance travelled:").append(j).toString());
            if (j > 0L) {
                com.navdy.hud.app.maps.util.DistanceConverter$Distance a = new com.navdy.hud.app.maps.util.DistanceConverter$Distance();
                com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit(), (float)j, a);
                String s = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getFormattedDistance(a.value, a.unit, false);
                this.deviceMileage.setText((CharSequence)s);
            }
        }
    }
    
    private void updateDynamicVehicleInfo() {
        boolean b = this.obdManager.isConnected();
        com.navdy.hud.app.obd.ObdManager$ConnectionType a = this.obdManager.getConnectionType();
        this.obdDataLayout.setVisibility((a != com.navdy.hud.app.obd.ObdManager$ConnectionType.POWER_ONLY) ? 0 : 8);
        label2: if (b) {
            boolean b0 = false;
            this.updateFuel();
            this.updateRPM();
            this.updateSpeed();
            this.updateMAF();
            this.updateEngineTemperature();
            this.updateVoltage();
            this.updateOdometer();
            com.navdy.obd.PidSet a0 = this.obdManager.getSupportedPids();
            boolean b1 = a0.contains(258);
            label5: {
                label3: {
                    label4: {
                        if (b1) {
                            break label4;
                        }
                        if (!a0.contains(259)) {
                            break label3;
                        }
                    }
                    b0 = true;
                    break label5;
                }
                b0 = false;
            }
            if (b0) {
                this.specialPidsLayout.setVisibility(0);
                this.updateEngineFuelPressure();
                this.updateEngineTripFuel();
            } else {
                this.specialPidsLayout.setVisibility(8);
            }
            this.checkEngineLight.setText((CharSequence)((this.obdManager.isCheckEngineLightOn()) ? on : off));
            java.util.List a1 = this.obdManager.getTroubleCodes();
            label0: {
                label1: {
                    if (a1 == null) {
                        break label1;
                    }
                    if (a1.size() > 0) {
                        break label0;
                    }
                }
                this.engineTroubleCodes.setText((CharSequence)noData);
                break label2;
            }
            StringBuilder a2 = new StringBuilder();
            Object a3 = a1.iterator();
            while(((java.util.Iterator)a3).hasNext()) {
                String s = (String)((java.util.Iterator)a3).next();
                a2.append(new StringBuilder().append(s).append(",").toString());
            }
            a2.deleteCharAt(a2.length() - 1);
            this.engineTroubleCodes.setText((CharSequence)a2.toString());
        } else {
            this.specialPidsLayout.setVisibility(8);
            this.odometerLayout.setVisibility(8);
            this.checkEngineLight.setText((CharSequence)noData);
            this.engineTroubleCodes.setText((CharSequence)noData);
        }
    }
    
    private void updateEngineFuelPressure() {
        int i = (int)this.obdManager.getPidValue(258);
        if (i <= 0) {
            this.engineOilPressure.setText((CharSequence)noData);
        } else {
            this.engineOilPressure.setText((CharSequence)new StringBuilder().append(i).append(" ").append(pressureUnitText).toString());
        }
    }
    
    private void updateEngineTemperature() {
        com.navdy.hud.app.profile.DriverProfile a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        double d = this.obdManager.getPidValue(5);
        if (d > 0.0) {
            int i = 0;
            int i0 = 0;
            if (com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu$2.$SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$UnitSystem[a.getUnitSystem().ordinal()] != 0) {
                i = (int)Math.round(d);
                i0 = R.string.si_celsius;
            } else {
                i = (int)Math.round(1.8 * d + 32.0);
                i0 = R.string.si_fahrenheit;
            }
            StringBuilder a0 = new StringBuilder();
            Object[] a1 = new Object[1];
            a1[0] = Integer.valueOf(i);
            String s = a0.append(String.format("%d", a1)).append(" ").append((char)176).append(resources.getString(i0)).toString();
            this.engineTemp.setText((CharSequence)s);
        } else {
            this.engineTemp.setText((CharSequence)noData);
        }
    }
    
    private void updateEngineTripFuel() {
        int i = (int)this.obdManager.getPidValue(259);
        if (i <= 0) {
            this.tripFuel.setText((CharSequence)noData);
        } else {
            this.tripFuel.setText((CharSequence)new StringBuilder().append(i).append(" ").append(fuelUnitText).toString());
        }
    }
    
    private void updateFuel() {
        int i = this.obdManager.getFuelLevel();
        if (i == -1) {
            this.fuel.setText((CharSequence)noData);
        } else {
            android.widget.TextView a = this.fuel;
            android.content.res.Resources a0 = resources;
            Object[] a1 = new Object[1];
            a1[0] = Integer.valueOf(i);
            a.setText((CharSequence)a0.getString(R.string.si_fuel_level, a1));
        }
    }
    
    private void updateGPSSpeed() {
        int i = this.speedManager.getGpsSpeed();
        if (i == -1) {
            this.gpsSpeed.setText((CharSequence)noData);
        } else {
            this.gpsSpeed.setText((CharSequence)String.valueOf(i));
        }
    }
    
    private void updateGenericPref() {
        com.navdy.hud.app.profile.DriverProfile a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        com.navdy.service.library.events.preferences.InputPreferences a0 = a.getInputPreferences();
        if (a0 != null) {
            if (Boolean.TRUE.equals(a0.use_gestures)) {
                this.gestures.setText((CharSequence)resources.getString(R.string.si_enabled));
            } else {
                this.gestures.setText((CharSequence)resources.getString(R.string.si_disabled));
            }
        }
        com.navdy.hud.app.obd.ObdDeviceConfigurationManager a1 = this.obdManager.getObdDeviceConfigurationManager();
        if (a1 != null) {
            if (a1.isAutoOnEnabled()) {
                this.autoOn.setText((CharSequence)resources.getString(R.string.si_enabled));
            } else {
                this.autoOn.setText((CharSequence)resources.getString(R.string.si_disabled));
            }
        }
        com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting a2 = a.getObdScanSetting();
        if (a2 != null) {
            switch(com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu$2.$SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$ObdScanSetting[a2.ordinal()]) {
                case 1: case 2: {
                    this.obdData.setText((CharSequence)resources.getString(R.string.si_on));
                    break;
                }
                default: {
                    this.obdData.setText((CharSequence)resources.getString(R.string.si_off));
                }
            }
        }
        com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat a3 = a.getDisplayFormat();
        if (a3 != null) {
            switch(com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu$2.$SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$DisplayFormat[a3.ordinal()]) {
                case 2: {
                    this.uiScaling.setText((CharSequence)resources.getString(R.string.si_normal));
                    break;
                }
                case 1: {
                    this.uiScaling.setText((CharSequence)resources.getString(R.string.si_compact));
                    break;
                }
            }
        }
    }
    
    private void updateHere() {
        com.navdy.hud.app.maps.here.HereMapsManager a = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        if (!a.isInitializing()) {
            if (a.isInitialized()) {
                this.hereSdk.setText((CharSequence)a.getVersion());
                if (a.isVoiceSkinsLoaded()) {
                    this.hereVoiceVerified.setText((CharSequence)resources.getString(R.string.si_yes));
                } else {
                    this.hereVoiceVerified.setText((CharSequence)resources.getString(R.string.si_no));
                }
                if (a.isMapDataVerified()) {
                    sLogger.v(new StringBuilder().append("map package installed count:").append(a.getMapPackageCount()).toString());
                    this.hereMapVerified.setText((CharSequence)resources.getString(R.string.si_yes));
                } else {
                    this.hereMapVerified.setText((CharSequence)resources.getString(R.string.si_no));
                }
                com.navdy.hud.app.maps.here.HereOfflineMapsVersion a0 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance().getOfflineMapsVersion();
                if (a0 != null) {
                    java.util.List a1 = a0.getPackages();
                    String s = null;
                    if (a1 != null) {
                        int i = a1.size();
                        StringBuilder a2 = new StringBuilder();
                        Object a3 = a1;
                        int i0 = 0;
                        while(i0 < i) {
                            if (i0 != 0) {
                                a2.append(",");
                                a2.append(" ");
                            }
                            a2.append((String)((java.util.List)a3).get(i0));
                            i0 = i0 + 1;
                        }
                        s = a2.toString();
                    }
                    String s0 = (a0.getDate() == null) ? null : dateFormat.format(a0.getDate());
                    String s1 = a0.getVersion();
                    if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                        this.hereUpdated.setText((CharSequence)s0);
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
                        this.hereOfflineMaps.setText((CharSequence)s);
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                        this.hereMapsVersion.setText((CharSequence)s1);
                    }
                }
            } else {
                com.here.android.mpa.common.OnEngineInitListener$Error a4 = a.getError();
                this.hereSdk.setText((CharSequence)a4.name());
            }
        }
    }
    
    private void updateMAF() {
        int i = (int)this.obdManager.getInstantFuelConsumption();
        if (i <= 0) {
            this.maf.setText((CharSequence)noData);
        } else {
            this.maf.setText((CharSequence)String.valueOf(i));
        }
    }
    
    private void updateNavPref() {
        com.navdy.service.library.events.preferences.NavigationPreferences a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile().getNavigationPreferences();
        if (a != null) {
            switch(com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu$2.$SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RoutingType[a.routingType.ordinal()]) {
                case 2: {
                    this.routeCalc.setText((CharSequence)resources.getString(R.string.si_shortest));
                    break;
                }
                case 1: {
                    this.routeCalc.setText((CharSequence)resources.getString(R.string.si_fastest));
                    break;
                }
            }
            StringBuilder a0 = new StringBuilder();
            if (Boolean.TRUE.equals(a.allowHighways)) {
                a0.append(resources.getString(R.string.si_highways));
            }
            if (Boolean.TRUE.equals(a.allowTollRoads)) {
                this.formatRoutePref(a0, resources.getString(R.string.si_tollroads));
            }
            if (Boolean.TRUE.equals(a.allowFerries)) {
                this.formatRoutePref(a0, resources.getString(R.string.si_ferries));
            }
            if (Boolean.TRUE.equals(a.allowTunnels)) {
                this.formatRoutePref(a0, resources.getString(R.string.si_tunnels));
            }
            if (Boolean.TRUE.equals(a.allowUnpavedRoads)) {
                this.formatRoutePref(a0, resources.getString(R.string.si_unpaved_roads));
            }
            if (Boolean.TRUE.equals(a.allowAutoTrains)) {
                this.formatRoutePref(a0, resources.getString(R.string.si_auto_trains));
            }
            a0.append("\n");
            this.routePref.setText((CharSequence)a0.toString());
        }
    }
    
    private void updateOdometer() {
        if (this.obdManager.getSupportedPids().contains(260)) {
            this.odometerLayout.setVisibility(0);
            int i = (int)this.obdManager.getPidValue(260);
            if (i <= 0) {
                this.odo.setText((CharSequence)noData);
            } else {
                com.navdy.hud.app.maps.util.DistanceConverter$Distance a = new com.navdy.hud.app.maps.util.DistanceConverter$Distance();
                com.navdy.hud.app.maps.util.DistanceConverter.convertToDistance(this.speedManager.getSpeedUnit(), (float)i, a);
                String s = com.navdy.hud.app.maps.here.HereManeuverDisplayBuilder.getFormattedDistance(a.value, a.unit, false);
                this.odo.setText((CharSequence)s);
            }
        } else {
            this.odometerLayout.setVisibility(8);
        }
    }
    
    private void updateRPM() {
        int i = this.obdManager.getEngineRpm();
        if (i == -1) {
            this.rpm.setText((CharSequence)noData);
        } else {
            this.rpm.setText((CharSequence)String.valueOf(i));
        }
    }
    
    private void updateSpeed() {
        int i = this.speedManager.getCurrentSpeed();
        if (i == -1) {
            this.speed.setText((CharSequence)noData);
        } else {
            this.speed.setText((CharSequence)String.valueOf(i));
        }
    }
    
    private void updateStats() {
        this.updateTemperature();
        this.updateDeviceMileage();
        this.updateGPSSpeed();
        com.navdy.hud.app.maps.here.HereMapsManager a = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        if (a.isInitialized()) {
            this.hereLocationFixManager = a.getLocationFixManager();
        }
        this.setFix();
    }
    
    private void updateTemperature() {
        int i = com.navdy.hud.app.analytics.AnalyticsSupport.getCurrentTemperature();
        if (i != -1) {
            String s = new StringBuilder().append(String.valueOf(i)).append(" ").append((char)176).append(resources.getString(R.string.si_celsius)).toString();
            this.temperature.setText((CharSequence)s);
        }
    }
    
    private void updateVehicleInfo() {
        com.navdy.hud.app.profile.DriverProfile a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        String s = a.getCarMake();
        if (!android.text.TextUtils.isEmpty((CharSequence)s)) {
            this.make.setText((CharSequence)s);
        }
        String s0 = a.getCarModel();
        if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
            this.carModel.setText((CharSequence)s0);
        }
        String s1 = a.getCarYear();
        if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
            this.year.setText((CharSequence)s1);
        }
        this.updateDynamicVehicleInfo();
    }
    
    private void updateVersions() {
        String s = com.navdy.hud.app.util.DeviceUtil.isUserBuild() ? com.navdy.hud.app.util.OTAUpdateService.shortVersion("1.3.3051-corona") : "1.3.3051-corona";
        this.displayFw.setText((CharSequence)s);
        com.navdy.hud.app.device.dial.DialManager a = com.navdy.hud.app.device.dial.DialManager.getInstance();
        if (a.isDialConnected()) {
            try {
                com.navdy.hud.app.device.dial.DialFirmwareUpdater a0 = a.getDialFirmwareUpdater();
                if (a0 != null) {
                    int i = a0.getVersions().dial.incrementalVersion;
                    String s0 = null;
                    if (i != -1) {
                        s0 = String.valueOf(i);
                    }
                    if (!android.text.TextUtils.isEmpty((CharSequence)s0)) {
                        this.dialFw.setText((CharSequence)s0);
                    }
                }
            } catch(Throwable a1) {
                sLogger.e(a1);
            }
            String s1 = a.getDialName();
            if (!android.text.TextUtils.isEmpty((CharSequence)s1)) {
                this.dialBt.setText((CharSequence)this.removeBrackets(s1));
            }
        }
        String s2 = android.bluetooth.BluetoothAdapter.getDefaultAdapter().getName();
        if (!android.text.TextUtils.isEmpty((CharSequence)s2)) {
            this.displayBt.setText((CharSequence)this.removeBrackets(s2));
        }
        String s3 = this.obdManager.getObdChipFirmwareVersion();
        if (!android.text.TextUtils.isEmpty((CharSequence)s3)) {
            this.obdFw.setText((CharSequence)s3);
        }
    }
    
    private void updateVoltage() {
        double d = this.obdManager.getBatteryVoltage();
        if (d > 0.0) {
            StringBuilder a = new StringBuilder();
            Object[] a0 = new Object[1];
            a0[0] = Double.valueOf(d);
            String s = a.append(String.format("%.1f", a0)).append(resources.getString(R.string.si_volt)).toString();
            this.voltage.setText((CharSequence)s);
        } else {
            this.voltage.setText((CharSequence)noData);
        }
    }
    
    public void GPSSpeedChangeEvent(com.navdy.hud.app.maps.MapEvents$GPSSpeedEvent a) {
        if (this.isMenuLoaded()) {
            this.updateSpeed();
            this.updateGPSSpeed();
        }
    }
    
    public void ObdStateChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdConnectionStatusEvent a) {
        if (this.isMenuLoaded()) {
            this.updateSpeed();
            this.updateDynamicVehicleInfo();
        }
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu getChildMenu(com.navdy.hud.app.ui.component.mainmenu.IMenu a, String s, String s0) {
        return null;
    }
    
    public int getInitialSelection() {
        return 1;
    }
    
    public java.util.List getItems() {
        java.util.ArrayList a = new java.util.ArrayList();
        ((java.util.List)a).add(back);
        ((java.util.List)a).add(infoModel);
        this.cachedList = (java.util.List)a;
        return (java.util.List)a;
    }
    
    public com.navdy.hud.app.ui.component.vlist.VerticalList$Model getModelfromPos(int i) {
        com.navdy.hud.app.ui.component.vlist.VerticalList$Model a = null;
        java.util.List a0 = this.cachedList;
        label2: {
            label0: {
                label1: {
                    if (a0 == null) {
                        break label1;
                    }
                    if (this.cachedList.size() > i) {
                        break label0;
                    }
                }
                a = null;
                break label2;
            }
            a = (com.navdy.hud.app.ui.component.vlist.VerticalList$Model)this.cachedList.get(i);
        }
        return a;
    }
    
    public com.navdy.hud.app.ui.component.vmenu.VerticalFastScrollIndex getScrollIndex() {
        return null;
    }
    
    public com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu getType() {
        return com.navdy.hud.app.ui.component.mainmenu.IMenu$Menu.SYSTEM_INFO;
    }
    
    public boolean isBindCallsEnabled() {
        return true;
    }
    
    public boolean isFirstItemEmpty() {
        return true;
    }
    
    public boolean isItemClickable(int i, int i0) {
        return i0 == 0;
    }
    
    public void onBindToView(com.navdy.hud.app.ui.component.vlist.VerticalList$Model a, android.view.View a0, int i, com.navdy.hud.app.ui.component.vlist.VerticalList$ModelState a1) {
        if (i == 1) {
            android.view.ViewGroup a2 = (android.view.ViewGroup)a0;
            android.view.View a3 = a2;
            if (a2.getChildCount() > 0) {
                android.view.ViewGroup a4 = (android.view.ViewGroup)a3;
                android.view.View a5 = a4;
                android.view.ViewGroup a6 = (android.view.ViewGroup)a4.getChildAt(0);
                butterknife.ButterKnife.inject(this, a5);
                this.satelliteView.setVisibility(com.navdy.hud.app.ui.component.UISettings.advancedGpsStatsEnabled() ? 0 : 8);
                this.update();
                if (!this.registered) {
                    this.bus.register(this);
                    this.registered = true;
                    com.navdy.hud.app.obd.ObdManager.getInstance().enableInstantaneousMode(true);
                    sLogger.v("registered, enabled instantaneous mode");
                }
                this.handler.removeCallbacks(this.updateRunnable);
                this.handler.postDelayed(this.updateRunnable, 2000L);
            }
        }
    }
    
    public void onDriverProfilePrefChangeEvent(com.navdy.hud.app.event.DriverProfileUpdated a) {
        if (this.isMenuLoaded() && a.state == com.navdy.hud.app.event.DriverProfileUpdated$State.UPDATED) {
            this.updateGenericPref();
        }
    }
    
    public void onFastScrollEnd() {
    }
    
    public void onFastScrollStart() {
    }
    
    public void onGpsSatelliteData(com.navdy.hud.app.device.gps.GpsUtils$GpsSatelliteData a) {
        try {
            if (this.isMenuLoaded()) {
                int i = a.data.getInt("SAT_SEEN", 0);
                int i0 = a.data.getInt("SAT_USED", 0);
                label2: {
                    label0: {
                        label1: {
                            if (i > 0) {
                                break label1;
                            }
                            if (i0 <= 0) {
                                break label0;
                            }
                        }
                        android.widget.TextView a0 = this.satellites;
                        android.content.res.Resources a1 = resources;
                        Object[] a2 = new Object[2];
                        a2[0] = Integer.valueOf(i0);
                        a2[1] = Integer.valueOf(i);
                        a0.setText((CharSequence)a1.getString(R.string.si_seen, a2));
                        break label2;
                    }
                    this.satellites.setText((CharSequence)noData);
                }
                this.ubloxFixType = com.navdy.hud.app.ui.component.mainmenu.SystemInfoMenu.getFixTypeDescription(a.data.getInt("SAT_FIX_TYPE", 0));
                int i1 = a.data.getInt("SAT_MAX_DB", 0);
                if (i1 <= 0) {
                    this.gps.setText((CharSequence)noData);
                } else {
                    android.widget.TextView a3 = this.gps;
                    android.content.res.Resources a4 = resources;
                    Object[] a5 = new Object[1];
                    a5[0] = Integer.valueOf(i1);
                    a3.setText((CharSequence)a4.getString(R.string.si_gps_signal, a5));
                }
                if (com.navdy.hud.app.ui.component.UISettings.advancedGpsStatsEnabled()) {
                    com.navdy.hud.app.ui.component.systeminfo.GpsSignalView$SatelliteData[] a6 = new com.navdy.hud.app.ui.component.systeminfo.GpsSignalView$SatelliteData[i];
                    int i2 = 0;
                    while(i2 < i) {
                        String s = a.data.getString(new StringBuilder().append("SAT_PROVIDER_").append(i2 + 1).toString());
                        a6[i2] = new com.navdy.hud.app.ui.component.systeminfo.GpsSignalView$SatelliteData(a.data.getInt(new StringBuilder().append("SAT_ID_").append(i2 + 1).toString()), a.data.getInt(new StringBuilder().append("SAT_DB_").append(i2 + 1).toString()), s);
                        i2 = i2 + 1;
                    }
                    java.util.Arrays.sort((Object[])a6);
                    this.satelliteView.setSatelliteData(a6);
                }
                sLogger.v(new StringBuilder().append("satellite data { seen=").append(i).append(" used=").append(i0).append(" fixType=").append(this.ubloxFixType).append(" maxDB=").append(i1).toString());
            }
        } catch(Throwable a7) {
            sLogger.e(a7);
        }
    }
    
    public void onItemSelected(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
    }
    
    public void onNavigationPrefChangeEvent(com.navdy.service.library.events.preferences.NavigationPreferences a) {
        if (this.isMenuLoaded()) {
            this.updateNavPref();
        }
    }
    
    public void onPidChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdPidChangeEvent a) {
        if (this.isMenuLoaded()) {
            switch(a.pid.getId()) {
                case 260: {
                    this.updateOdometer();
                    break;
                }
                case 259: {
                    this.updateEngineTripFuel();
                    break;
                }
                case 258: {
                    this.updateEngineFuelPressure();
                    break;
                }
                case 256: {
                    this.updateMAF();
                    break;
                }
                case 47: {
                    this.updateFuel();
                    break;
                }
                case 13: {
                    this.updateSpeed();
                    break;
                }
                case 12: {
                    this.updateRPM();
                    break;
                }
                case 5: {
                    this.updateEngineTemperature();
                    break;
                }
            }
        }
    }
    
    public void onScrollIdle() {
    }
    
    public void onSpeedDataExpired(com.navdy.hud.app.manager.SpeedManager$SpeedDataExpired a) {
        if (this.isMenuLoaded()) {
            this.updateSpeed();
            this.updateGPSSpeed();
        }
    }
    
    public void onSpeedUnitChanged(com.navdy.hud.app.manager.SpeedManager$SpeedUnitChanged a) {
        if (this.isMenuLoaded()) {
            this.updateSpeed();
            this.updateGPSSpeed();
        }
    }
    
    public void onSupportedPidsChanged(com.navdy.hud.app.obd.ObdManager$ObdSupportedPidsChangedEvent a) {
        if (this.isMenuLoaded()) {
            this.updateSpeed();
            this.updateGPSSpeed();
            this.updateDynamicVehicleInfo();
        }
    }
    
    public void onUnload(com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel a) {
        if (this.registered) {
            if (com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager().getHomescreenView().getDisplayMode() == com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.SMART_DASH) {
                sLogger.v("onUnload: instantaneous mode still enabled");
            } else {
                sLogger.v("onUnload: disable instantaneous mode");
                com.navdy.hud.app.obd.ObdManager.getInstance().enableInstantaneousMode(false);
            }
            this.bus.unregister(this);
            this.registered = false;
            sLogger.v("unregistered");
        }
        this.displayFw = null;
        this.dialFw = null;
        this.obdFw = null;
        this.dialBt = null;
        this.displayBt = null;
        this.gps = null;
        this.satellites = null;
        this.fix = null;
        this.temperature = null;
        this.deviceMileage = null;
        this.hereSdk = null;
        this.hereUpdated = null;
        this.hereMapsVersion = null;
        this.hereOfflineMaps = null;
        this.hereMapVerified = null;
        this.hereVoiceVerified = null;
        this.make = null;
        this.carModel = null;
        this.year = null;
        this.fuel = null;
        this.rpm = null;
        this.speed = null;
        this.odo = null;
        this.maf = null;
        this.engineTemp = null;
        this.voltage = null;
        this.gestures = null;
        this.autoOn = null;
        this.obdData = null;
        this.uiScaling = null;
        this.routeCalc = null;
        this.routePref = null;
    }
    
    public boolean selectItem(com.navdy.hud.app.ui.component.vlist.VerticalList$ItemSelectionState a) {
        sLogger.v(new StringBuilder().append("select id:").append(a.id).append(" pos:").append(a.pos).toString());
        if (a.id == R.id.menu_back) {
            sLogger.v("back");
            com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
            this.presenter.loadMenu(this.parent, com.navdy.hud.app.ui.component.mainmenu.IMenu$MenuLevel.BACK_TO_PARENT, this.backSelection, 0);
        }
        return true;
    }
    
    public void setBackSelectionId(int i) {
    }
    
    public void setBackSelectionPos(int i) {
        this.backSelection = i;
    }
    
    public void setSelectedIcon() {
        this.vscrollComponent.setSelectedIconImage(R.drawable.icon_settings_navdy_data, (String)null, com.navdy.hud.app.ui.component.image.InitialsImageView$Style.DEFAULT);
        this.vscrollComponent.selectedText.setText((CharSequence)infoTitle);
    }
    
    public void showToolTip() {
    }
}
