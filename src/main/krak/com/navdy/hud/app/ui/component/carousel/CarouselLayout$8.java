package com.navdy.hud.app.ui.component.carousel;

class CarouselLayout$8 extends com.navdy.hud.app.ui.framework.DefaultAnimationListener {
    final com.navdy.hud.app.ui.component.carousel.CarouselLayout this$0;
    final android.animation.Animator$AnimatorListener val$listener;
    final int val$newPos;
    final boolean val$next;
    final int val$oldPos;
    
    CarouselLayout$8(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, android.animation.Animator$AnimatorListener a0, int i, int i0, boolean b) {
        super();
        this.this$0 = a;
        this.val$listener = a0;
        this.val$oldPos = i;
        this.val$newPos = i0;
        this.val$next = b;
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
        this.this$0.currentItem = this.val$newPos;
        if (this.val$listener != null) {
            this.val$listener.onAnimationEnd(a);
        }
        com.navdy.hud.app.ui.component.carousel.CarouselLayout.access$800(this.this$0, this.val$next, true);
        if (this.val$next) {
            if (this.this$0.leftView != null) {
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.access$900(this.this$0, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.SIDE, this.this$0.leftView);
                this.this$0.leftView = null;
            }
            this.this$0.leftView = this.this$0.middleLeftView;
            this.this$0.middleLeftView = this.this$0.rightView;
            this.this$0.rightView = this.this$0.newRightView;
        } else {
            if (this.this$0.rightView != null) {
                com.navdy.hud.app.ui.component.carousel.CarouselLayout.access$900(this.this$0, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.SIDE, this.this$0.rightView);
                this.this$0.rightView = null;
            }
            this.this$0.rightView = this.this$0.middleLeftView;
            this.this$0.middleLeftView = (this.this$0.newMiddleLeftView == null) ? this.this$0.leftView : this.this$0.newMiddleLeftView;
            this.this$0.leftView = this.this$0.newLeftView;
        }
        if (this.this$0.newMiddleRightView != null) {
            com.navdy.hud.app.ui.component.carousel.CarouselLayout.access$900(this.this$0, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_RIGHT, this.this$0.middleRightView);
            this.this$0.middleRightView = this.this$0.newMiddleRightView;
            this.this$0.newMiddleRightView = null;
        }
        this.this$0.newMiddleLeftView = null;
        this.this$0.newLeftView = null;
        this.this$0.newRightView = null;
        if (this.this$0.leftView == null) {
            this.this$0.leftView = this.this$0.addSideView(this.val$newPos - 1, this.this$0.viewPadding, false);
        }
        if (this.this$0.rightView == null) {
            this.this$0.rightView = this.this$0.addSideView(this.val$newPos + 1, this.this$0.viewPadding + this.this$0.sideImageSize + this.this$0.rightImageStart, false);
        }
        if (this.this$0.carouselIndicator != null) {
            this.this$0.carouselIndicator.setCurrentItem(this.this$0.currentItem);
        }
        if (this.this$0.itemChangeListener != null) {
            this.this$0.itemChangeListener.onCurrentItemChanged(this.this$0.currentItem, ((com.navdy.hud.app.ui.component.carousel.Carousel$Model)this.this$0.model.get(this.this$0.currentItem)).id);
        }
        this.this$0.runQueuedOperation();
    }
    
    public void onAnimationStart(android.animation.Animator a) {
        if (this.val$listener != null) {
            this.val$listener.onAnimationStart(a);
        }
        if (this.this$0.newMiddleRightView != null) {
            this.this$0.newMiddleRightView.setVisibility(0);
        }
        if (this.this$0.itemChangeListener != null && this.val$oldPos != this.val$newPos) {
            this.this$0.itemChangeListener.onCurrentItemChanging(this.val$oldPos, this.val$newPos, ((com.navdy.hud.app.ui.component.carousel.Carousel$Model)this.this$0.model.get(this.val$newPos)).id);
        }
    }
}
