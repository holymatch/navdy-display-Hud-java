package com.navdy.hud.app.ui.component.carousel;
import com.navdy.hud.app.R;

public class FastScrollAnimator implements com.navdy.hud.app.ui.component.carousel.AnimationStrategy {
    final public static int ANIMATION_HERO = 50;
    final public static int ANIMATION_HERO_SINGLE = 133;
    final public static int ANIMATION_HERO_TEXT = 165;
    final public static int ANIMATION_HERO_TRANSLATE_X;
    final private static com.navdy.service.library.log.Logger sLogger;
    private com.navdy.hud.app.ui.component.carousel.CarouselLayout carouselLayout;
    private boolean endPending;
    private android.view.View hiddenView;
    private long lastScrollAnimationFinishTime;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator.class);
        ANIMATION_HERO_TRANSLATE_X = (int)com.navdy.hud.app.HudApplication.getAppContext().getResources().getDimension(R.dimen.carousel_fastscroll_hero_translate_x);
    }
    
    FastScrollAnimator(com.navdy.hud.app.ui.component.carousel.CarouselLayout a) {
        this.carouselLayout = a;
        this.hiddenView = a.buildView(0, 0, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_LEFT, false);
    }
    
    static boolean access$002(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator a, boolean b) {
        a.endPending = b;
        return b;
    }
    
    static android.view.View access$100(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator a) {
        return a.hiddenView;
    }
    
    static android.view.View access$102(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator a, android.view.View a0) {
        a.hiddenView = a0;
        return a0;
    }
    
    static com.navdy.hud.app.ui.component.carousel.CarouselLayout access$200(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator a) {
        return a.carouselLayout;
    }
    
    static long access$302(com.navdy.hud.app.ui.component.carousel.FastScrollAnimator a, long j) {
        a.lastScrollAnimationFinishTime = j;
        return j;
    }
    
    public android.animation.AnimatorSet buildLayoutAnimation(android.animation.Animator$AnimatorListener a, com.navdy.hud.app.ui.component.carousel.CarouselLayout a0, int i, int i0) {
        int i1 = 0;
        int i2 = 0;
        int i3 = 0;
        android.animation.AnimatorSet a1 = new android.animation.AnimatorSet();
        if (((i0 > i) ? com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.LEFT : com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.RIGHT) != com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction.LEFT) {
            i1 = (int)(a0.middleLeftView.getX() + (float)ANIMATION_HERO_TRANSLATE_X);
            i2 = (int)(a0.middleLeftView.getX() - (float)ANIMATION_HERO_TRANSLATE_X);
        } else {
            i1 = (int)(a0.middleLeftView.getX() - (float)ANIMATION_HERO_TRANSLATE_X);
            i2 = (int)(a0.middleLeftView.getX() + (float)ANIMATION_HERO_TRANSLATE_X);
        }
        this.hiddenView.setX((float)i2);
        this.hiddenView.setAlpha(0.0f);
        android.view.View a2 = a0.middleLeftView;
        android.util.Property a3 = android.view.View.X;
        float[] a4 = new float[1];
        a4[0] = (float)i1;
        android.animation.AnimatorSet$Builder a5 = a1.play((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a2, a3, a4));
        android.view.View a6 = a0.middleLeftView;
        android.util.Property a7 = android.view.View.ALPHA;
        float[] a8 = new float[1];
        a8[0] = 0.0f;
        a5.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a6, a7, a8));
        android.view.View a9 = this.hiddenView;
        android.util.Property a10 = android.view.View.X;
        float[] a11 = new float[1];
        a11[0] = this.carouselLayout.middleLeftView.getX();
        a5.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a9, a10, a11));
        android.view.View a12 = this.hiddenView;
        android.util.Property a13 = android.view.View.ALPHA;
        float[] a14 = new float[1];
        a14[0] = 1f;
        a5.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a12, a13, a14));
        this.carouselLayout.carouselAdapter.getView(i0, this.hiddenView, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_LEFT, this.carouselLayout.imageLytResourceId, this.carouselLayout.mainImageSize);
        if (this.carouselLayout.carouselIndicator != null) {
            android.animation.AnimatorSet a15 = this.carouselLayout.carouselIndicator.getItemMoveAnimator(i0, -1);
            if (a15 != null) {
                a5.with((android.animation.Animator)a15);
            }
        }
        a1.setInterpolator((android.animation.TimeInterpolator)this.carouselLayout.interpolator);
        a1.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.carousel.FastScrollAnimator$1(this, a, i0));
        if (this.carouselLayout.isAnimationPending()) {
            i3 = 50;
        } else {
            i3 = (android.os.SystemClock.elapsedRealtime() - this.lastScrollAnimationFinishTime <= 50L) ? 50 : 133;
        }
        a1.setDuration((long)i3);
        if (sLogger.isLoggable(3)) {
            sLogger.v(new StringBuilder().append("dur=").append(i3).toString());
        }
        return a1;
    }
    
    public android.animation.AnimatorSet createHiddenViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a0) {
        return null;
    }
    
    public android.animation.AnimatorSet createMiddleLeftViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a0) {
        return null;
    }
    
    public android.animation.AnimatorSet createMiddleRightViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a0) {
        return null;
    }
    
    public android.animation.AnimatorSet createNewMiddleRightViewAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a0) {
        return null;
    }
    
    public android.animation.AnimatorSet createSideViewToMiddleAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a0) {
        return null;
    }
    
    public android.animation.Animator createViewOutAnimation(com.navdy.hud.app.ui.component.carousel.CarouselLayout a, com.navdy.hud.app.ui.component.carousel.AnimationStrategy$Direction a0) {
        return null;
    }
    
    void endAnimation() {
        if (this.endPending) {
            this.endPending = false;
            this.carouselLayout.carouselAdapter.getView(this.carouselLayout.currentItem - 1, this.carouselLayout.leftView, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.SIDE, this.carouselLayout.imageLytResourceId, this.carouselLayout.sideImageSize);
            this.carouselLayout.carouselAdapter.getView(this.carouselLayout.currentItem + 1, this.carouselLayout.rightView, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.SIDE, this.carouselLayout.imageLytResourceId, this.carouselLayout.sideImageSize);
            this.carouselLayout.carouselAdapter.getView(this.carouselLayout.currentItem, this.carouselLayout.middleRightView, com.navdy.hud.app.ui.component.carousel.Carousel$ViewType.MIDDLE_RIGHT, this.carouselLayout.imageLytResourceId, this.carouselLayout.sideImageSize);
            this.endAnimation((android.animation.Animator$AnimatorListener)null, this.carouselLayout.currentItem, true);
        }
    }
    
    void endAnimation(android.animation.Animator$AnimatorListener a, int i, boolean b) {
        android.animation.AnimatorSet a0 = new android.animation.AnimatorSet();
        android.view.View a1 = this.carouselLayout.leftView;
        android.util.Property a2 = android.view.View.ALPHA;
        float[] a3 = new float[1];
        a3[0] = 1f;
        android.animation.ObjectAnimator a4 = android.animation.ObjectAnimator.ofFloat(a1, a2, a3);
        android.view.View a5 = this.carouselLayout.rightView;
        android.util.Property a6 = android.view.View.ALPHA;
        float[] a7 = new float[1];
        a7[0] = 1f;
        android.animation.ObjectAnimator a8 = android.animation.ObjectAnimator.ofFloat(a5, a6, a7);
        android.view.View a9 = this.carouselLayout.middleRightView;
        android.util.Property a10 = android.view.View.ALPHA;
        float[] a11 = new float[1];
        a11[0] = 1f;
        android.animation.ObjectAnimator a12 = android.animation.ObjectAnimator.ofFloat(a9, a10, a11);
        a0.setDuration(165L);
        android.animation.Animator[] a13 = new android.animation.Animator[3];
        a13[0] = a4;
        a13[1] = a8;
        a13[2] = a12;
        a0.playTogether(a13);
        a0.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.carousel.FastScrollAnimator$2(this, b, i, a));
        a0.start();
    }
    
    boolean isEndPending() {
        return this.endPending;
    }
}
