package com.navdy.hud.app.ui.component.homescreen;

public class SpeedView extends android.widget.LinearLayout {
    private com.squareup.otto.Bus bus;
    private int lastSpeed;
    private com.navdy.hud.app.manager.SpeedManager$SpeedUnit lastSpeedUnit;
    private com.navdy.service.library.log.Logger logger;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    @InjectView(R.id.speedUnitView)
    android.widget.TextView speedUnitView;
    @InjectView(R.id.speedView)
    android.widget.TextView speedView;
    
    public SpeedView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public SpeedView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public SpeedView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.lastSpeed = -1;
    }
    
    private void setTrackingSpeed() {
        int i = this.speedManager.getCurrentSpeed();
        if (i < 0) {
            i = 0;
        }
        com.navdy.hud.app.manager.SpeedManager$SpeedUnit a = this.speedManager.getSpeedUnit();
        if (i != this.lastSpeed) {
            this.lastSpeed = i;
            this.speedView.setText((CharSequence)String.valueOf(i));
        }
        if (a != this.lastSpeedUnit) {
            String s = null;
            this.lastSpeedUnit = a;
            switch(com.navdy.hud.app.ui.component.homescreen.SpeedView$1.$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[a.ordinal()]) {
                case 3: {
                    s = com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.SPEED_METERS;
                    break;
                }
                case 2: {
                    s = com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.SPEED_KM;
                    break;
                }
                case 1: {
                    s = com.navdy.hud.app.ui.component.homescreen.HomeScreenConstants.SPEED_MPH;
                    break;
                }
                default: {
                    s = "";
                }
            }
            this.speedUnitView.setText((CharSequence)s);
        }
    }
    
    public void GPSSpeedChangeEvent(com.navdy.hud.app.maps.MapEvents$GPSSpeedEvent a) {
        this.setTrackingSpeed();
    }
    
    public void ObdPidChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdPidChangeEvent a) {
        if (a.pid.getId() == 13) {
            this.setTrackingSpeed();
        }
    }
    
    public void ObdStateChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdConnectionStatusEvent a) {
        this.lastSpeed = -1;
        this.lastSpeedUnit = null;
        this.setTrackingSpeed();
    }
    
    public void clearState() {
        this.lastSpeed = -1;
        this.lastSpeedUnit = null;
        this.setTrackingSpeed();
    }
    
    public void getTopAnimator(android.animation.AnimatorSet$Builder a, boolean b) {
        if (b) {
            a.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getTranslationXPositionAnimator((android.view.View)this.speedUnitView, com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.topViewSpeedOut));
            android.widget.TextView a0 = this.speedUnitView;
            android.util.Property a1 = android.view.View.ALPHA;
            float[] a2 = new float[2];
            a2[0] = 1f;
            a2[1] = 0.0f;
            a.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a0, a1, a2));
        } else {
            a.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getTranslationXPositionAnimator((android.view.View)this.speedUnitView, 0));
            android.widget.TextView a3 = this.speedUnitView;
            android.util.Property a4 = android.view.View.ALPHA;
            float[] a5 = new float[2];
            a5[0] = 0.0f;
            a5[1] = 1f;
            a.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a3, a4, a5));
            this.speedView.setVisibility(0);
        }
    }
    
    protected void onFinishInflate() {
        this.logger = com.navdy.hud.app.ui.component.homescreen.HomeScreenView.sLogger;
        super.onFinishInflate();
        butterknife.ButterKnife.inject((android.view.View)this);
        if (!this.isInEditMode()) {
            this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
            this.bus = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getBus();
            this.bus.register(this);
        }
    }
    
    public void onSpeedDataExpired(com.navdy.hud.app.manager.SpeedManager$SpeedDataExpired a) {
        this.setTrackingSpeed();
    }
    
    public void onSpeedUnitChanged(com.navdy.hud.app.manager.SpeedManager$SpeedUnitChanged a) {
        this.setTrackingSpeed();
    }
    
    public void onSpeedWarningEvent(com.navdy.hud.app.maps.MapEvents$SpeedWarning a) {
        if (a.exceed) {
            this.speedView.setTextColor(-16711681);
        } else {
            this.speedView.setTextColor(-1);
        }
    }
    
    public void resetTopViewsAnimator() {
        this.speedUnitView.setAlpha(1f);
        this.speedUnitView.setTranslationX(0.0f);
        this.speedView.setVisibility(0);
    }
}
