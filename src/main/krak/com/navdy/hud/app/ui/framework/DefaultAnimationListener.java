package com.navdy.hud.app.ui.framework;

public class DefaultAnimationListener implements android.animation.Animator$AnimatorListener {
    public DefaultAnimationListener() {
    }
    
    public void onAnimationCancel(android.animation.Animator a) {
    }
    
    public void onAnimationEnd(android.animation.Animator a) {
    }
    
    public void onAnimationRepeat(android.animation.Animator a) {
    }
    
    public void onAnimationStart(android.animation.Animator a) {
    }
}
