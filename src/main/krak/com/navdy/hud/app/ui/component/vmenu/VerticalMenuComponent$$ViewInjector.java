package com.navdy.hud.app.ui.component.vmenu;
import com.navdy.hud.app.R;

public class VerticalMenuComponent$$ViewInjector {
    public VerticalMenuComponent$$ViewInjector() {
    }
    
    public static void inject(butterknife.ButterKnife$Finder a, com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a0, Object a1) {
        a0.leftContainer = (android.view.ViewGroup)a.findRequiredView(a1, R.id.leftContainer, "field 'leftContainer'");
        a0.selectedImage = (android.view.ViewGroup)a.findRequiredView(a1, R.id.selectedImage, "field 'selectedImage'");
        a0.selectedIconColorImage = (com.navdy.hud.app.ui.component.image.IconColorImageView)a.findRequiredView(a1, R.id.selectedIconColorImage, "field 'selectedIconColorImage'");
        a0.selectedIconImage = (com.navdy.hud.app.ui.component.image.InitialsImageView)a.findRequiredView(a1, R.id.selectedIconImage, "field 'selectedIconImage'");
        a0.selectedText = (android.widget.TextView)a.findRequiredView(a1, R.id.selectedText, "field 'selectedText'");
        a0.rightContainer = (android.view.ViewGroup)a.findRequiredView(a1, R.id.rightContainer, "field 'rightContainer'");
        a0.indicator = (com.navdy.hud.app.ui.component.carousel.CarouselIndicator)a.findRequiredView(a1, R.id.indicator, "field 'indicator'");
        a0.recyclerView = (com.navdy.hud.app.ui.component.vlist.VerticalRecyclerView)a.findRequiredView(a1, R.id.recyclerView, "field 'recyclerView'");
        a0.toolTip = (com.navdy.hud.app.view.ToolTipView)a.findRequiredView(a1, R.id.tooltip, "field 'toolTip'");
        a0.closeContainerScrim = a.findRequiredView(a1, R.id.closeContainerScrim, "field 'closeContainerScrim'");
        a0.closeIconContainer = (android.view.ViewGroup)a.findRequiredView(a1, R.id.closeIconContainer, "field 'closeIconContainer'");
        a0.closeContainer = (android.view.ViewGroup)a.findRequiredView(a1, R.id.closeContainer, "field 'closeContainer'");
        a0.closeHalo = (com.navdy.hud.app.ui.component.HaloView)a.findRequiredView(a1, R.id.closeHalo, "field 'closeHalo'");
        a0.fastScrollContainer = (android.view.ViewGroup)a.findRequiredView(a1, R.id.fastScrollView, "field 'fastScrollContainer'");
        a0.fastScrollText = (android.widget.TextView)a.findRequiredView(a1, R.id.fastScrollText, "field 'fastScrollText'");
        a0.selectedCustomView = (android.widget.FrameLayout)a.findRequiredView(a1, R.id.selectedCustomView, "field 'selectedCustomView'");
    }
    
    public static void reset(com.navdy.hud.app.ui.component.vmenu.VerticalMenuComponent a) {
        a.leftContainer = null;
        a.selectedImage = null;
        a.selectedIconColorImage = null;
        a.selectedIconImage = null;
        a.selectedText = null;
        a.rightContainer = null;
        a.indicator = null;
        a.recyclerView = null;
        a.toolTip = null;
        a.closeContainerScrim = null;
        a.closeIconContainer = null;
        a.closeContainer = null;
        a.closeHalo = null;
        a.fastScrollContainer = null;
        a.fastScrollText = null;
        a.selectedCustomView = null;
    }
}
