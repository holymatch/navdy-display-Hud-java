package com.navdy.hud.app.ui.component;
import com.navdy.hud.app.R;

public class ConfirmationLayout extends android.widget.RelativeLayout {
    public com.navdy.hud.app.ui.component.ChoiceLayout choiceLayout;
    private com.navdy.hud.app.ui.component.ChoiceLayout$IListener choiceListener;
    private com.navdy.hud.app.ui.component.ChoiceLayout$IListener defaultChoiceListener;
    private boolean finished;
    public com.navdy.hud.app.ui.component.FluctuatorAnimatorView fluctuatorView;
    private android.os.Handler handler;
    public android.widget.ImageView leftSwipe;
    private com.navdy.hud.app.ui.component.ConfirmationLayout$Listener listener;
    public android.view.ViewGroup mainSection;
    public android.widget.ImageView rightSwipe;
    public com.navdy.hud.app.ui.component.image.InitialsImageView screenImage;
    public com.navdy.hud.app.ui.component.image.IconColorImageView screenImageIconBkColor;
    public android.widget.TextView screenTitle;
    public android.widget.ImageView sideImage;
    private int timeout;
    private Runnable timeoutRunnable;
    public android.widget.TextView title1;
    public android.widget.TextView title2;
    public android.widget.TextView title3;
    public android.widget.TextView title4;
    public android.view.ViewGroup titleContainer;
    
    public ConfirmationLayout(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public ConfirmationLayout(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public ConfirmationLayout(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.defaultChoiceListener = (com.navdy.hud.app.ui.component.ChoiceLayout$IListener)new com.navdy.hud.app.ui.component.ConfirmationLayout$1(this);
        this.timeoutRunnable = (Runnable)new com.navdy.hud.app.ui.component.ConfirmationLayout$2(this);
        this.init();
    }
    
    static void access$000(com.navdy.hud.app.ui.component.ConfirmationLayout a) {
        a.clearTimeout();
    }
    
    static com.navdy.hud.app.ui.component.ChoiceLayout$IListener access$100(com.navdy.hud.app.ui.component.ConfirmationLayout a) {
        return a.choiceListener;
    }
    
    static boolean access$202(com.navdy.hud.app.ui.component.ConfirmationLayout a, boolean b) {
        a.finished = b;
        return b;
    }
    
    static com.navdy.hud.app.ui.component.ConfirmationLayout$Listener access$300(com.navdy.hud.app.ui.component.ConfirmationLayout a) {
        return a.listener;
    }
    
    private void clearTimeout() {
        this.handler.removeCallbacks(this.timeoutRunnable);
    }
    
    private void init() {
        android.view.LayoutInflater.from(this.getContext()).inflate(R.layout.confirmation_lyt, (android.view.ViewGroup)this, true);
        this.screenTitle = (android.widget.TextView)this.findViewById(R.id.mainTitle);
        this.screenImage = (com.navdy.hud.app.ui.component.image.InitialsImageView)this.findViewById(R.id.image);
        this.screenImageIconBkColor = (com.navdy.hud.app.ui.component.image.IconColorImageView)this.findViewById(R.id.imageIconBkColor);
        this.sideImage = (android.widget.ImageView)this.findViewById(R.id.sideImage);
        this.titleContainer = (android.view.ViewGroup)this.findViewById(R.id.infoContainer);
        this.title1 = (android.widget.TextView)this.findViewById(R.id.title1);
        this.title2 = (android.widget.TextView)this.findViewById(R.id.title2);
        this.title3 = (android.widget.TextView)this.findViewById(R.id.title3);
        this.title4 = (android.widget.TextView)this.findViewById(R.id.title4);
        this.choiceLayout = (com.navdy.hud.app.ui.component.ChoiceLayout)this.findViewById(R.id.choiceLayout);
        this.fluctuatorView = (com.navdy.hud.app.ui.component.FluctuatorAnimatorView)this.findViewById(R.id.fluctuator);
        this.leftSwipe = (android.widget.ImageView)this.findViewById(R.id.leftSwipe);
        this.rightSwipe = (android.widget.ImageView)this.findViewById(R.id.rightSwipe);
        this.mainSection = (android.view.ViewGroup)this.findViewById(R.id.mainSection);
    }
    
    private void resetTimeout() {
        if (this.timeout > 0) {
            this.handler.removeCallbacks(this.timeoutRunnable);
            this.handler.postDelayed(this.timeoutRunnable, (long)this.timeout);
        }
    }
    
    public void executeSelectedItem(boolean b) {
        if (!this.finished) {
            this.resetTimeout();
            this.choiceLayout.executeSelectedItem(b);
        }
    }
    
    public boolean handleKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        switch(com.navdy.hud.app.ui.component.ConfirmationLayout$3.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
            case 3: {
                this.executeSelectedItem(true);
                b = true;
                break;
            }
            case 2: {
                this.moveSelectionRight();
                b = true;
                break;
            }
            case 1: {
                this.moveSelectionLeft();
                b = true;
                break;
            }
            default: {
                b = false;
            }
        }
        return b;
    }
    
    public void moveSelectionLeft() {
        if (!this.finished) {
            this.resetTimeout();
            this.choiceLayout.moveSelectionLeft();
        }
    }
    
    public void moveSelectionRight() {
        if (!this.finished) {
            this.resetTimeout();
            this.choiceLayout.moveSelectionRight();
        }
    }
    
    public void setChoices(java.util.List a, int i, com.navdy.hud.app.ui.component.ChoiceLayout$IListener a0) {
        this.finished = false;
        this.clearTimeout();
        if (a != null && a.size() != 0 && a0 != null) {
            this.choiceListener = a0;
            java.util.ArrayList a1 = new java.util.ArrayList();
            Object a2 = a.iterator();
            while(((java.util.Iterator)a2).hasNext()) {
                ((java.util.List)a1).add(new com.navdy.hud.app.ui.component.ChoiceLayout$Choice((String)((java.util.Iterator)a2).next(), 0));
            }
            this.choiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, (java.util.List)a1, i, this.defaultChoiceListener);
            return;
        }
        throw new IllegalArgumentException();
    }
    
    public void setChoicesList(java.util.List a, int i, com.navdy.hud.app.ui.component.ChoiceLayout$IListener a0) {
        this.finished = false;
        this.clearTimeout();
        if (a != null && a.size() != 0 && a0 != null) {
            this.choiceListener = a0;
            this.choiceLayout.setChoices(com.navdy.hud.app.ui.component.ChoiceLayout$Mode.LABEL, a, i, this.defaultChoiceListener);
            return;
        }
        throw new IllegalArgumentException();
    }
    
    public void setTimeout(int i, com.navdy.hud.app.ui.component.ConfirmationLayout$Listener a) {
        if (!this.finished) {
            label0: {
                label1: {
                    if (a == null) {
                        break label1;
                    }
                    if (i > 0) {
                        break label0;
                    }
                }
                throw new IllegalArgumentException();
            }
            this.timeout = i;
            this.listener = a;
            this.resetTimeout();
        }
    }
}
