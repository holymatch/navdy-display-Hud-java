package com.navdy.hud.app.ui.activity;
import com.navdy.hud.app.R;

public class MainActivity extends com.navdy.hud.app.ui.activity.HudBaseActivity {
    final private static String ACTION_PAIRING_CANCEL = "android.bluetooth.device.action.PAIRING_CANCEL";
    private android.content.BroadcastReceiver bluetoothReceiver;
    @InjectView(R.id.container)
    com.navdy.hud.app.view.ContainerView container;
    android.widget.TextView debugText;
    @Inject
    com.navdy.hud.app.ui.activity.Main$Presenter mainPresenter;
    @Inject
    com.navdy.hud.app.manager.PairingManager pairingManager;
    @Inject
    com.navdy.hud.app.device.PowerManager powerManager;
    
    public MainActivity() {
        this.bluetoothReceiver = new com.navdy.hud.app.ui.activity.MainActivity$2(this);
    }
    
    static com.navdy.service.library.log.Logger access$000(com.navdy.hud.app.ui.activity.MainActivity a) {
        return a.logger;
    }
    
    static com.navdy.service.library.log.Logger access$100(com.navdy.hud.app.ui.activity.MainActivity a) {
        return a.logger;
    }
    
    static com.navdy.service.library.log.Logger access$200(com.navdy.hud.app.ui.activity.MainActivity a) {
        return a.logger;
    }
    
    protected mortar.Blueprint getBlueprint() {
        return (mortar.Blueprint)new com.navdy.hud.app.ui.activity.Main();
    }
    
    public void onConfigurationChanged(android.content.res.Configuration a) {
        this.logger.v(new StringBuilder().append("onConfigurationChanged:").append(a).toString());
        super.onConfigurationChanged(a);
    }
    
    protected void onCreate(android.os.Bundle a) {
        super.onCreate(a);
        if (com.navdy.hud.app.util.DeviceUtil.isNavdyDevice()) {
            this.setContentView(R.layout.main_view_lyt);
        } else {
            this.setContentView(R.layout.activity_fullscreen);
            this.debugText = (android.widget.TextView)this.findViewById(R.id.debug_text);
            android.content.res.Resources a0 = this.getResources();
            int i = (int)a0.getDimension(R.dimen.dashboard_width);
            int i0 = (int)a0.getDimension(R.dimen.dashboard_height);
            String s = new StringBuilder().append("Device:  ").append(android.os.Build.DEVICE).append(":").append(android.os.Build.MODEL).append(" width = ").append(i).append(" height = ").append(i0).append(" scale factor = ").append(i / 640).toString();
            this.debugText.setText((CharSequence)s);
        }
        butterknife.ButterKnife.inject((android.app.Activity)this);
        android.preference.PreferenceManager.setDefaultValues((android.content.Context)this, "HUD", 0, R.xml.developer_preferences, false);
        this.makeImmersive(!this.powerManager.inQuietMode());
        android.content.IntentFilter a1 = new android.content.IntentFilter();
        a1.addAction("android.bluetooth.device.action.PAIRING_REQUEST");
        a1.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
        a1.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        a1.addAction("android.bluetooth.device.action.PAIRING_CANCEL");
        this.registerReceiver(this.bluetoothReceiver, a1);
        this.mainPresenter.bus.register(this);
    }
    
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(this.bluetoothReceiver);
    }
    
    public void onInitEvent(com.navdy.hud.app.event.InitEvents$InitPhase a) {
        if (a.phase == com.navdy.hud.app.event.InitEvents$Phase.POST_START) {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.ui.activity.MainActivity$1(this), 1);
        }
    }
    
    public void onWakeup(com.navdy.hud.app.event.Wakeup a) {
        this.logger.v("enableNormalMode(): FLAG_KEEP_SCREEN_ON set");
        this.getWindow().addFlags(128);
    }
    
    public void onWindowFocusChanged(boolean b) {
        super.onWindowFocusChanged(b);
        if (b) {
            this.makeImmersive(!this.powerManager.inQuietMode());
        }
    }
}
