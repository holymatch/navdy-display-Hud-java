package com.navdy.hud.app.ui.component.homescreen;
import com.navdy.hud.app.R;

public class HomeScreenView extends android.widget.RelativeLayout implements com.navdy.hud.app.view.ICustomAnimator, com.navdy.hud.app.manager.InputManager$IInputHandler, com.navdy.hud.app.gesture.GestureDetector$GestureListener {
    final private static int MIN_SPEED_LIMIT_VISIBLE_DURATION;
    final static String SYSPROP_ALWAYS_SHOW_SPEED_LIMIT_SIGN = "persist.sys.speedlimit.always";
    final static com.navdy.service.library.log.Logger sLogger;
    @InjectView(R.id.activeEtaContainer)
    com.navdy.hud.app.ui.component.homescreen.EtaView activeEtaContainer;
    private boolean alwaysShowSpeedLimitSign;
    @Inject
    com.squareup.otto.Bus bus;
    private com.navdy.hud.app.maps.NavigationMode currentNavigationMode;
    @InjectView(R.id.drive_score_events)
    com.navdy.hud.app.view.DashboardWidgetView dashboardWidgetView;
    com.navdy.hud.app.view.DriveScoreGaugePresenter driveScoreGaugePresenter;
    private android.content.SharedPreferences driverPreferences;
    @Inject
    android.content.SharedPreferences globalPreferences;
    private android.os.Handler handler;
    private Runnable hideSpeedLimitRunnable;
    private boolean isRecalculating;
    private boolean isTopInit;
    private boolean isTopViewAnimationOut;
    private boolean isTopViewAnimationRunning;
    @InjectView(R.id.lane_guidance_map_icon_indicator)
    android.widget.ImageView laneGuidanceIconIndicator;
    @InjectView(R.id.laneGuidance)
    com.navdy.hud.app.ui.component.homescreen.LaneGuidanceView laneGuidanceView;
    private long lastSpeedLimitShown;
    @InjectView(R.id.mainscreenRightSection)
    com.navdy.hud.app.ui.component.homescreen.HomeScreenRightSectionView mainscreenRightSection;
    @InjectView(R.id.map_container)
    com.navdy.hud.app.ui.component.homescreen.NavigationView mapContainer;
    @InjectView(R.id.map_mask)
    android.widget.ImageView mapMask;
    @InjectView(R.id.map_view_speed_container)
    android.view.ViewGroup mapViewSpeedContainer;
    private com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode mode;
    @InjectView(R.id.navigation_views_container)
    protected android.widget.RelativeLayout navigationViewsContainer;
    private com.navdy.hud.app.framework.notifications.NotificationManager notificationManager;
    @InjectView(R.id.openMapRoadInfoContainer)
    com.navdy.hud.app.ui.component.homescreen.OpenRoadView openMapRoadInfoContainer;
    private boolean paused;
    @Inject
    com.navdy.hud.app.ui.component.homescreen.HomeScreen$Presenter presenter;
    @InjectView(R.id.recalcRouteContainer)
    com.navdy.hud.app.ui.component.homescreen.RecalculatingView recalcRouteContainer;
    private android.animation.AnimatorSet rightScreenAnimator;
    private com.navdy.hud.app.ui.framework.IScreenAnimationListener screenAnimationListener;
    private boolean showCollapsedNotif;
    @InjectView(R.id.smartDashContainer)
    com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView smartDashContainer;
    private int speedLimit;
    @InjectView(R.id.home_screen_speed_limit_sign)
    com.navdy.hud.app.view.SpeedLimitSignView speedLimitSignView;
    private com.navdy.hud.app.manager.SpeedManager speedManager;
    @InjectView(R.id.speedContainer)
    com.navdy.hud.app.ui.component.homescreen.SpeedView speedView;
    @InjectView(R.id.activeMapRoadInfoContainer)
    com.navdy.hud.app.ui.component.tbt.TbtViewContainer tbtView;
    @InjectView(R.id.time)
    com.navdy.hud.app.ui.component.homescreen.TimeView timeContainer;
    private Runnable topViewAnimationRunnable;
    private android.animation.AnimatorSet topViewAnimator;
    private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.ui.component.homescreen.HomeScreenView.class);
        MIN_SPEED_LIMIT_VISIBLE_DURATION = (int)java.util.concurrent.TimeUnit.SECONDS.toMillis(3L);
    }
    
    public HomeScreenView(android.content.Context a) {
        this(a, (android.util.AttributeSet)null);
    }
    
    public HomeScreenView(android.content.Context a, android.util.AttributeSet a0) {
        this(a, a0, 0);
    }
    
    public HomeScreenView(android.content.Context a, android.util.AttributeSet a0, int i) {
        super(a, a0, i);
        this.isTopInit = true;
        this.speedManager = com.navdy.hud.app.manager.SpeedManager.getInstance();
        this.speedLimit = 0;
        this.alwaysShowSpeedLimitSign = false;
        this.topViewAnimationRunnable = (Runnable)new com.navdy.hud.app.ui.component.homescreen.HomeScreenView$1(this);
        this.handler = new android.os.Handler();
        this.screenAnimationListener = (com.navdy.hud.app.ui.framework.IScreenAnimationListener)new com.navdy.hud.app.ui.component.homescreen.HomeScreenView$2(this);
        this.mode = com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP;
        this.hideSpeedLimitRunnable = (Runnable)new com.navdy.hud.app.ui.component.homescreen.HomeScreenView$3(this);
        this.uiStateManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getUiStateManager();
        this.notificationManager = com.navdy.hud.app.framework.notifications.NotificationManager.getInstance();
        this.alwaysShowSpeedLimitSign = com.navdy.hud.app.util.os.SystemProperties.getBoolean("persist.sys.speedlimit.always", false);
        if (!this.isInEditMode()) {
            this.driveScoreGaugePresenter = new com.navdy.hud.app.view.DriveScoreGaugePresenter(a, R.layout.drive_score_map_layout, false);
            mortar.Mortar.inject(a, this);
        }
    }
    
    static boolean access$000(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        return a.showCollapsedNotif;
    }
    
    static boolean access$002(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a, boolean b) {
        a.showCollapsedNotif = b;
        return b;
    }
    
    static void access$100(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        a.hideSpeedLimit();
    }
    
    static boolean access$200(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        return a.isTopViewAnimationOut;
    }
    
    static boolean access$202(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a, boolean b) {
        a.isTopViewAnimationOut = b;
        return b;
    }
    
    static com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode access$300(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        return a.mode;
    }
    
    static boolean access$400(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        return a.isRecalculating;
    }
    
    static boolean access$502(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a, boolean b) {
        a.isTopViewAnimationRunning = b;
        return b;
    }
    
    static Runnable access$600(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        return a.topViewAnimationRunnable;
    }
    
    static android.os.Handler access$700(com.navdy.hud.app.ui.component.homescreen.HomeScreenView a) {
        return a.handler;
    }
    
    private void clearRightSectionAnimation() {
        if (this.rightScreenAnimator != null && this.rightScreenAnimator.isRunning()) {
            this.rightScreenAnimator.removeAllListeners();
            this.rightScreenAnimator.cancel();
            sLogger.v("stopped rightscreen animation");
        }
        this.rightScreenAnimator = null;
    }
    
    private android.animation.Animator getExpandAnimator() {
        boolean b = this.isNavigationActive();
        android.animation.AnimatorSet a = new android.animation.AnimatorSet();
        float[] a0 = new float[2];
        a0[0] = 1f;
        a0[1] = 100f;
        android.animation.AnimatorSet$Builder a1 = a.play((android.animation.Animator)android.animation.ValueAnimator.ofFloat(a0));
        if (!this.isTopViewAnimationOut) {
            if (this.timeContainer.getAlpha() == 0.0f) {
                com.navdy.hud.app.ui.component.homescreen.TimeView a2 = this.timeContainer;
                android.util.Property a3 = ALPHA;
                float[] a4 = new float[2];
                a4[0] = 0.0f;
                a4[1] = 1f;
                a1.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a2, a3, a4));
            }
            if (this.timeContainer.getX() != (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.timeX) {
                a1.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.timeContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.timeX));
            }
        }
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
            if (this.activeEtaContainer.getX() != (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.etaX) {
                a1.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.activeEtaContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.etaX));
            }
            if (this.activeEtaContainer.getAlpha() == 0.0f) {
                com.navdy.hud.app.ui.component.homescreen.EtaView a5 = this.activeEtaContainer;
                android.util.Property a6 = ALPHA;
                float[] a7 = new float[2];
                a7[0] = 0.0f;
                a7[1] = 1f;
                a1.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a5, a6, a7));
            }
        }
        this.mapContainer.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND, a1);
        if (this.mode == com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
            this.mapViewSpeedContainer.setVisibility(0);
        }
        this.smartDashContainer.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND, a1);
        if (b) {
            this.openMapRoadInfoContainer.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND);
            this.tbtView.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND, a1);
            this.laneGuidanceView.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND, a1);
        } else {
            this.tbtView.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND);
            this.openMapRoadInfoContainer.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND, a1);
        }
        if (this.isRecalculating) {
            a1.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.recalcRouteContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.recalcX));
        } else {
            this.recalcRouteContainer.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND);
        }
        a.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.homescreen.HomeScreenView$6(this));
        this.animateTopViewsIn();
        return a;
    }
    
    private android.animation.Animator getShrinkLeftAnimator() {
        boolean b = this.isNavigationActive();
        android.animation.AnimatorSet a = new android.animation.AnimatorSet();
        float[] a0 = new float[2];
        a0[0] = 1f;
        a0[1] = 100f;
        android.animation.AnimatorSet$Builder a1 = a.play((android.animation.Animator)android.animation.ValueAnimator.ofFloat(a0));
        this.mapContainer.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT, a1);
        android.animation.AnimatorSet a2 = new android.animation.AnimatorSet();
        android.animation.Animator[] a3 = new android.animation.Animator[1];
        android.view.ViewGroup a4 = this.mapViewSpeedContainer;
        android.util.Property a5 = android.view.View.ALPHA;
        float[] a6 = new float[2];
        a6[0] = 1f;
        a6[1] = 0.0f;
        a3[0] = android.animation.ObjectAnimator.ofFloat(a4, a5, a6);
        a2.playTogether(a3);
        a2.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.homescreen.HomeScreenView$4(this));
        a1.with((android.animation.Animator)a2);
        this.smartDashContainer.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT, a1);
        if (b) {
            this.openMapRoadInfoContainer.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT);
            if (this.timeContainer.getVisibility() != 0) {
                this.timeContainer.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT);
                if (this.mode != com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
                    this.activeEtaContainer.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT);
                } else {
                    a1.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.activeEtaContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.etaShrinkLeftX));
                }
            } else {
                a1.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.timeContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.timeShrinkLeftX));
                this.activeEtaContainer.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT);
            }
            this.tbtView.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT, a1);
            this.laneGuidanceView.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT, a1);
        } else {
            this.tbtView.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT);
            this.activeEtaContainer.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT);
            this.openMapRoadInfoContainer.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT, a1);
            a1.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.timeContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.timeShrinkLeftX));
        }
        if (this.isRecalculating) {
            a1.with((android.animation.Animator)com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.recalcRouteContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.recalcShrinkLeftX));
        } else {
            this.recalcRouteContainer.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_LEFT);
        }
        a.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.homescreen.HomeScreenView$5(this));
        this.animateTopViewsOut();
        return a;
    }
    
    private void hideSpeedLimit() {
        this.handler.removeCallbacks(this.hideSpeedLimitRunnable);
        long j = android.os.SystemClock.elapsedRealtime() - this.lastSpeedLimitShown;
        if (j <= (long)MIN_SPEED_LIMIT_VISIBLE_DURATION) {
            this.handler.postDelayed(this.hideSpeedLimitRunnable, j);
        } else {
            this.speedLimitSignView.setVisibility(8);
            this.speedLimitSignView.setSpeedLimit(0);
            this.lastSpeedLimitShown = 0L;
        }
    }
    
    private void pauseNavigationViewContainer() {
        sLogger.v("::onPause:pauseNavigationViewContainer");
        this.tbtView.onPause();
        this.recalcRouteContainer.onPause();
    }
    
    private void resetTopViewsAnimator() {
        sLogger.v("resetTopViewsAnimator");
        if (this.topViewAnimator != null && this.topViewAnimator.isRunning()) {
            this.topViewAnimator.removeAllListeners();
            this.topViewAnimator.cancel();
        }
        this.mapContainer.resetTopViewsAnimator();
        this.speedView.resetTopViewsAnimator();
        this.smartDashContainer.resetTopViewsAnimator();
        if (this.shouldAnimateTopViews()) {
            this.timeContainer.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND);
            this.timeContainer.setAlpha(1f);
            this.activeEtaContainer.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND);
            this.activeEtaContainer.setAlpha(1f);
            this.isTopViewAnimationOut = false;
            this.isTopViewAnimationRunning = false;
            this.handler.removeCallbacks(this.topViewAnimationRunnable);
            this.handler.postDelayed(this.topViewAnimationRunnable, 10000L);
        } else {
            this.handler.removeCallbacks(this.topViewAnimationRunnable);
            this.animateTopViewsOut();
        }
    }
    
    private void resumeNavigationViewContainer() {
        sLogger.v("::onResume:resumeNavigationViewContainer");
        this.tbtView.onResume();
        this.recalcRouteContainer.onResume();
    }
    
    private void setDisplayMode(boolean b) {
        sLogger.v(new StringBuilder().append("setDisplayMode:").append(this.mode).toString());
        switch(com.navdy.hud.app.ui.component.homescreen.HomeScreenView$10.$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode[this.mode.ordinal()]) {
            case 2: {
                sLogger.v("dash visible");
                this.setViewsBackground(0);
                if (com.navdy.hud.app.ui.component.homescreen.HomeScreenView.showDriveScoreEventsOnMap()) {
                    this.dashboardWidgetView.setVisibility(8);
                    this.driveScoreGaugePresenter.onPause();
                }
                com.navdy.hud.app.obd.ObdManager.getInstance().enableInstantaneousMode(true);
                this.smartDashContainer.onResume();
                this.smartDashContainer.setVisibility(0);
                this.mapViewSpeedContainer.setVisibility(8);
                this.activeEtaContainer.setVisibility(8);
                this.timeContainer.setVisibility(8);
                this.laneGuidanceView.setVisibility(8);
                this.laneGuidanceIconIndicator.setVisibility(8);
                this.mapContainer.onPause();
                this.laneGuidanceView.onPause();
                this.navigationViewsContainer.setVisibility(0);
                this.resumeNavigationViewContainer();
                break;
            }
            case 1: {
                sLogger.v("dash invisible");
                this.setViewsBackground(-16777216);
                this.mapContainer.onResume();
                this.laneGuidanceView.onResume();
                com.navdy.hud.app.obd.ObdManager.getInstance().enableInstantaneousMode(false);
                this.smartDashContainer.setVisibility(4);
                this.smartDashContainer.onPause();
                if (com.navdy.hud.app.ui.component.homescreen.HomeScreenView.showDriveScoreEventsOnMap()) {
                    this.dashboardWidgetView.setVisibility(0);
                    this.driveScoreGaugePresenter.onResume();
                }
                if (this.isNavigationActive()) {
                    this.timeContainer.setVisibility(8);
                    this.activeEtaContainer.setVisibility(0);
                } else {
                    this.timeContainer.setVisibility(0);
                    this.activeEtaContainer.setVisibility(8);
                }
                if (this.uiStateManager.isMainUIShrunk()) {
                    this.mapViewSpeedContainer.setVisibility(8);
                } else {
                    this.mapViewSpeedContainer.setVisibility(0);
                }
                this.navigationViewsContainer.setVisibility(0);
                this.laneGuidanceView.showLastEvent();
                this.resumeNavigationViewContainer();
                break;
            }
        }
        this.updateRoadInfoView();
        if (b) {
            this.resetTopViewsAnimator();
        }
    }
    
    private void setViewSpeed(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        switch(com.navdy.hud.app.ui.component.homescreen.HomeScreenView$10.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                this.mapViewSpeedContainer.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.speedX);
                break;
            }
            case 1: {
                this.mapViewSpeedContainer.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.speedShrinkLeftX);
                break;
            }
        }
    }
    
    private void setViews(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        this.activeEtaContainer.setView(a);
        this.openMapRoadInfoContainer.setView(a);
        com.navdy.hud.app.view.MainView$CustomAnimationMode a0 = (a != com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND) ? a : (this.isModeVisible()) ? com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_MODE : a;
        this.mapContainer.setView(a0);
        this.smartDashContainer.setView(a0);
        this.setViewSpeed(a);
        this.tbtView.setView(a);
        this.recalcRouteContainer.setView(a);
        this.timeContainer.setView(a);
        this.laneGuidanceView.setView(a);
    }
    
    private void setViewsBackground(int i) {
        sLogger.v(new StringBuilder().append("setViewsBackground:").append(i).toString());
        this.tbtView.setBackgroundColor(i);
        this.recalcRouteContainer.setBackgroundColor(i);
    }
    
    public static boolean showDriveScoreEventsOnMap() {
        return !com.navdy.hud.app.util.DeviceUtil.isUserBuild();
    }
    
    private void updateLayoutForMode(com.navdy.hud.app.maps.NavigationMode a) {
        int i = 0;
        this.handler.removeCallbacks(this.topViewAnimationRunnable);
        this.isRecalculating = false;
        boolean b = this.isNavigationActive();
        this.mapContainer.updateLayoutForMode(a);
        this.smartDashContainer.updateLayoutForMode(a, false);
        this.mainscreenRightSection.updateLayoutForMode(a, this);
        if (b) {
            if (this.mode != com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
                this.mapViewSpeedContainer.setVisibility(8);
            } else if (this.uiStateManager.isMainUIShrunk()) {
                this.mapViewSpeedContainer.setVisibility(8);
            } else {
                this.mapViewSpeedContainer.setVisibility(0);
            }
            this.timeContainer.setVisibility(8);
            this.tbtView.clearState();
            this.tbtView.setVisibility(0);
            this.activeEtaContainer.clearState();
            if (this.mode == com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
                this.activeEtaContainer.setVisibility(0);
                this.timeContainer.setVisibility(8);
            }
            this.mapContainer.clearState();
            this.speedView.clearState();
        } else {
            this.recalcRouteContainer.hideRecalculating();
            this.activeEtaContainer.clearState();
            this.activeEtaContainer.setVisibility(8);
            this.tbtView.clearState();
            this.tbtView.setVisibility(8);
            if (this.mode != com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
                this.mapViewSpeedContainer.setVisibility(8);
            } else {
                this.timeContainer.setVisibility(0);
                if (this.uiStateManager.isMainUIShrunk()) {
                    this.mapViewSpeedContainer.setVisibility(8);
                } else {
                    this.mapViewSpeedContainer.setVisibility(0);
                }
            }
            this.mapContainer.clearState();
            this.speedView.clearState();
        }
        this.updateRoadInfoView();
        this.resetTopViewsAnimator();
        if (this.isTopInit) {
            this.isTopInit = false;
            i = 30000;
        } else {
            i = 10000;
        }
        this.handler.postDelayed(this.topViewAnimationRunnable, (long)i);
    }
    
    private void updateRoadInfoView() {
        if (this.isNavigationActive()) {
            this.openMapRoadInfoContainer.setVisibility(8);
        } else {
            this.openMapRoadInfoContainer.setRoad();
            this.openMapRoadInfoContainer.setVisibility(0);
        }
    }
    
    private void updateSpeedLimitSign() {
        label2: if (this.speedLimit <= 0) {
            this.hideSpeedLimit();
        } else {
            int i = this.speedManager.getCurrentSpeed();
            int i0 = this.speedLimit;
            label0: {
                label1: {
                    if (i > i0) {
                        break label1;
                    }
                    if (!this.alwaysShowSpeedLimitSign) {
                        break label0;
                    }
                }
                this.handler.removeCallbacks(this.hideSpeedLimitRunnable);
                com.navdy.hud.app.manager.SpeedManager$SpeedUnit a = this.speedManager.getSpeedUnit();
                this.speedLimitSignView.setSpeedLimitUnit(a);
                this.speedLimitSignView.setSpeedLimit(this.speedLimit);
                this.speedLimitSignView.setVisibility(0);
                this.lastSpeedLimitShown = android.os.SystemClock.elapsedRealtime();
                break label2;
            }
            this.hideSpeedLimit();
        }
    }
    
    public void GPSSpeedChangeEvent(com.navdy.hud.app.maps.MapEvents$GPSSpeedEvent a) {
        this.updateSpeedLimitSign();
    }
    
    public void ObdPidChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdPidChangeEvent a) {
        if (a.pid.getId() == 13) {
            this.updateSpeedLimitSign();
        }
    }
    
    public void animateInModeView() {
        if (this.hasModeView() && !this.isModeVisible()) {
            sLogger.v("mode-view animateInModeView");
            this.rightScreenAnimator = new android.animation.AnimatorSet();
            android.animation.AnimatorSet a = this.rightScreenAnimator;
            float[] a0 = new float[2];
            a0[0] = 0.0f;
            a0[1] = 0.0f;
            android.animation.AnimatorSet$Builder a1 = a.play((android.animation.Animator)android.animation.ValueAnimator.ofFloat(a0));
            this.mapContainer.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_MODE, a1);
            this.smartDashContainer.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_MODE, a1);
            this.mainscreenRightSection.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.SHRINK_MODE, a1);
            this.rightScreenAnimator.start();
        }
    }
    
    public void animateOutModeView(boolean b) {
        if (this.isModeVisible()) {
            sLogger.v(new StringBuilder().append("mode-view animateOutModeView:").append(b).toString());
            this.rightScreenAnimator = new android.animation.AnimatorSet();
            android.animation.AnimatorSet a = this.rightScreenAnimator;
            float[] a0 = new float[2];
            a0[0] = 0.0f;
            a0[1] = 0.0f;
            android.animation.AnimatorSet$Builder a1 = a.play((android.animation.Animator)android.animation.ValueAnimator.ofFloat(a0));
            this.mapContainer.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND, a1);
            this.smartDashContainer.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND, a1);
            this.mainscreenRightSection.getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND, a1);
            if (b) {
                this.rightScreenAnimator.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.homescreen.HomeScreenView$9(this));
            }
            this.rightScreenAnimator.start();
        }
    }
    
    void animateTopViewsIn() {
        if (!this.isTopViewAnimationRunning && this.shouldAnimateTopViews() && this.isTopViewAnimationOut) {
            android.animation.AnimatorSet$Builder a = null;
            this.topViewAnimator = new android.animation.AnimatorSet();
            if (this.timeContainer.getVisibility() != 0) {
                android.animation.AnimatorSet a0 = this.topViewAnimator;
                int[] a1 = new int[2];
                a1[0] = 0;
                a1[1] = 0;
                a = a0.play((android.animation.Animator)android.animation.ValueAnimator.ofInt(a1));
            } else {
                android.animation.ObjectAnimator a2 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.timeContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.topViewLeftAnimationIn);
                a = this.topViewAnimator.play((android.animation.Animator)a2);
                com.navdy.hud.app.ui.component.homescreen.TimeView a3 = this.timeContainer;
                android.util.Property a4 = android.view.View.ALPHA;
                float[] a5 = new float[2];
                a5[0] = 0.0f;
                a5[1] = 1f;
                a.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a3, a4, a5));
            }
            switch(com.navdy.hud.app.ui.component.homescreen.HomeScreenView$10.$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode[this.mode.ordinal()]) {
                case 2: {
                    this.smartDashContainer.getTopAnimator(a, false);
                    break;
                }
                case 1: {
                    this.mapContainer.getTopAnimator(a, false);
                    this.speedView.getTopAnimator(a, false);
                    break;
                }
            }
            this.isTopViewAnimationRunning = true;
            this.topViewAnimator.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.homescreen.HomeScreenView$8(this));
            this.topViewAnimator.start();
            sLogger.v("started in animation");
        }
    }
    
    void animateTopViewsOut() {
        this.handler.removeCallbacks(this.topViewAnimationRunnable);
        if (!this.isTopViewAnimationRunning && !this.isTopViewAnimationOut) {
            android.animation.AnimatorSet$Builder a = null;
            this.topViewAnimator = new android.animation.AnimatorSet();
            if (this.timeContainer.getVisibility() != 0) {
                android.animation.AnimatorSet a0 = this.topViewAnimator;
                int[] a1 = new int[2];
                a1[0] = 0;
                a1[1] = 0;
                a = a0.play((android.animation.Animator)android.animation.ValueAnimator.ofInt(a1));
            } else {
                android.animation.ObjectAnimator a2 = com.navdy.hud.app.ui.component.homescreen.HomeScreenUtils.getXPositionAnimator((android.view.View)this.timeContainer, (float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.topViewLeftAnimationOut);
                a = this.topViewAnimator.play((android.animation.Animator)a2);
                com.navdy.hud.app.ui.component.homescreen.TimeView a3 = this.timeContainer;
                android.util.Property a4 = android.view.View.ALPHA;
                float[] a5 = new float[2];
                a5[0] = 1f;
                a5[1] = 0.0f;
                a.with((android.animation.Animator)android.animation.ObjectAnimator.ofFloat(a3, a4, a5));
            }
            switch(com.navdy.hud.app.ui.component.homescreen.HomeScreenView$10.$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode[this.mode.ordinal()]) {
                case 2: {
                    this.smartDashContainer.getTopAnimator(a, true);
                    break;
                }
                case 1: {
                    this.mapContainer.getTopAnimator(a, true);
                    this.speedView.getTopAnimator(a, true);
                    break;
                }
            }
            this.isTopViewAnimationRunning = true;
            this.topViewAnimator.addListener((android.animation.Animator$AnimatorListener)new com.navdy.hud.app.ui.component.homescreen.HomeScreenView$7(this));
            this.topViewAnimator.start();
            sLogger.v("started out animation");
        }
    }
    
    public void ejectRightSection() {
        sLogger.v("ejectRightSection");
        this.clearRightSectionAnimation();
        label2: if (this.mainscreenRightSection.isViewVisible()) {
            com.navdy.hud.app.ui.activity.Main a = this.uiStateManager.getRootScreen();
            boolean b = a.isNotificationExpanding();
            label0: {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (!a.isNotificationViewShowing()) {
                        break label0;
                    }
                }
                sLogger.v("ejectRightSection: cannot animate");
                this.mainscreenRightSection.ejectRightSection();
                break label2;
            }
            this.animateOutModeView(true);
        } else {
            this.mainscreenRightSection.ejectRightSection();
        }
    }
    
    public android.animation.Animator getCustomAnimator(com.navdy.hud.app.view.MainView$CustomAnimationMode a) {
        android.animation.Animator a0 = null;
        switch(com.navdy.hud.app.ui.component.homescreen.HomeScreenView$10.$SwitchMap$com$navdy$hud$app$view$MainView$CustomAnimationMode[a.ordinal()]) {
            case 2: {
                a0 = this.getExpandAnimator();
                break;
            }
            case 1: {
                a0 = this.getShrinkLeftAnimator();
                break;
            }
            default: {
                a0 = null;
            }
        }
        return a0;
    }
    
    public com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode getDisplayMode() {
        return this.mode;
    }
    
    public android.content.SharedPreferences getDriverPreferences() {
        return this.driverPreferences;
    }
    
    public com.navdy.hud.app.ui.component.homescreen.EtaView getEtaView() {
        return this.activeEtaContainer;
    }
    
    public android.widget.ImageView getLaneGuidanceIconIndicator() {
        return this.laneGuidanceIconIndicator;
    }
    
    public com.navdy.hud.app.ui.component.homescreen.NavigationView getNavigationView() {
        return this.mapContainer;
    }
    
    public com.navdy.hud.app.ui.component.homescreen.OpenRoadView getOpenRoadView() {
        return this.openMapRoadInfoContainer;
    }
    
    public com.navdy.hud.app.ui.component.homescreen.RecalculatingView getRecalculatingView() {
        return this.recalcRouteContainer;
    }
    
    public com.navdy.hud.app.ui.component.homescreen.BaseSmartDashView getSmartDashView() {
        return this.smartDashContainer;
    }
    
    public com.navdy.hud.app.ui.component.tbt.TbtViewContainer getTbtView() {
        return this.tbtView;
    }
    
    public boolean hasModeView() {
        return this.mainscreenRightSection.hasView();
    }
    
    public void injectRightSection(android.view.View a) {
        sLogger.v("injectRightSection");
        this.mainscreenRightSection.injectRightSection(a);
        this.clearRightSectionAnimation();
        com.navdy.hud.app.ui.activity.Main a0 = this.uiStateManager.getRootScreen();
        label2: if (this.mainscreenRightSection.isViewVisible()) {
            sLogger.v("injectRightSection: right section view already visible");
        } else {
            boolean b = a0.isNotificationExpanding();
            label0: {
                label1: {
                    if (b) {
                        break label1;
                    }
                    if (!a0.isNotificationViewShowing()) {
                        break label0;
                    }
                }
                sLogger.v("injectRightSection: cannot animate");
                break label2;
            }
            this.animateInModeView();
        }
    }
    
    public boolean isModeVisible() {
        return this.mainscreenRightSection.isViewVisible();
    }
    
    public boolean isNavigationActive() {
        boolean b = false;
        com.navdy.hud.app.maps.NavigationMode a = this.currentNavigationMode;
        com.navdy.hud.app.maps.NavigationMode a0 = com.navdy.hud.app.maps.NavigationMode.MAP_ON_ROUTE;
        label2: {
            label0: {
                label1: {
                    if (a == a0) {
                        break label1;
                    }
                    if (this.currentNavigationMode != com.navdy.hud.app.maps.NavigationMode.TBT_ON_ROUTE) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public boolean isRecalculating() {
        return this.isRecalculating;
    }
    
    public boolean isShowCollapsedNotification() {
        return this.showCollapsedNotif;
    }
    
    boolean isTopViewAnimationOut() {
        return this.isTopViewAnimationOut;
    }
    
    public com.navdy.hud.app.manager.InputManager$IInputHandler nextHandler() {
        return com.navdy.hud.app.manager.InputManager.nextContainingHandler((android.view.View)this);
    }
    
    public void onArrivalEvent(com.navdy.hud.app.maps.MapEvents$ArrivalEvent a) {
        this.activeEtaContainer.setVisibility(8);
        if (this.getDisplayMode() == com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.MAP) {
            this.timeContainer.setVisibility(0);
        }
        this.resetTopViewsAnimator();
    }
    
    public void onClick() {
    }
    
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        this.updateDriverPrefs();
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        android.view.ViewGroup a = (android.view.ViewGroup)this.findViewById(R.id.smart_dash_place_holder);
        android.view.LayoutInflater.from(this.getContext()).inflate(R.layout.screen_home_smartdash, a, true);
        butterknife.ButterKnife.inject((android.view.View)this);
        this.updateDriverPrefs();
        this.setDisplayMode(false);
        this.mapMask.setImageResource(com.navdy.hud.app.ui.component.homescreen.HomeScreenView.showDriveScoreEventsOnMap() ? R.drawable.map_mask : R.drawable.navigation_map_mask);
        this.mapContainer.init(this);
        this.smartDashContainer.init(this);
        this.recalcRouteContainer.init(this);
        this.tbtView.init(this);
        this.openMapRoadInfoContainer.init(this);
        this.activeEtaContainer.init(this);
        this.laneGuidanceView.init(this);
        this.mainscreenRightSection.setX((float)com.navdy.hud.app.ui.component.homescreen.HomeScreenResourceValues.mainScreenRightSectionX);
        this.setViews(this.uiStateManager.getCustomAnimateMode());
        sLogger.v("setting navigation mode to MAP");
        this.setMode(com.navdy.hud.app.maps.NavigationMode.MAP);
        if (this.presenter != null) {
            this.presenter.takeView(this);
        }
        if (com.navdy.hud.app.ui.component.homescreen.HomeScreenView.showDriveScoreEventsOnMap()) {
            android.os.Bundle a0 = new android.os.Bundle();
            a0.putInt("EXTRA_GRAVITY", 0);
            a0.putBoolean("EXTRA_IS_ACTIVE", true);
            this.driveScoreGaugePresenter.setView(this.dashboardWidgetView, a0);
            this.driveScoreGaugePresenter.setWidgetVisibleToUser(true);
        }
        this.uiStateManager.addScreenAnimationListener(this.screenAnimationListener);
        this.bus.register(this);
    }
    
    public boolean onGesture(com.navdy.service.library.events.input.GestureEvent a) {
        boolean b = false;
        com.navdy.service.library.events.input.Gesture a0 = a.gesture;
        com.navdy.service.library.events.input.Gesture a1 = com.navdy.service.library.events.input.Gesture.GESTURE_SWIPE_RIGHT;
        label2: {
            label0: {
                if (a0 != a1) {
                    break label0;
                }
                if (!this.isModeVisible()) {
                    break label0;
                }
                if (!this.hasModeView()) {
                    break label0;
                }
                android.animation.AnimatorSet a2 = this.rightScreenAnimator;
                label1: {
                    if (a2 == null) {
                        break label1;
                    }
                    if (this.rightScreenAnimator.isRunning()) {
                        break label0;
                    }
                }
                sLogger.v("swipe_right animateOutModeView");
                this.animateOutModeView(true);
                b = true;
                break label2;
            }
            switch(com.navdy.hud.app.ui.component.homescreen.HomeScreenView$10.$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode[this.mode.ordinal()]) {
                case 2: {
                    b = this.smartDashContainer.onGesture(a);
                    break;
                }
                case 1: {
                    b = this.mapContainer.onGesture(a);
                    break;
                }
                default: {
                    b = false;
                }
            }
        }
        return b;
    }
    
    public boolean onKey(com.navdy.hud.app.manager.InputManager$CustomKeyEvent a) {
        boolean b = false;
        if (this.shouldAnimateTopViews()) {
            switch(com.navdy.hud.app.ui.component.homescreen.HomeScreenView$10.$SwitchMap$com$navdy$hud$app$manager$InputManager$CustomKeyEvent[a.ordinal()]) {
                case 2: {
                    this.animateTopViewsOut();
                    break;
                }
                case 1: {
                    this.animateTopViewsIn();
                    break;
                }
            }
        }
        switch(com.navdy.hud.app.ui.component.homescreen.HomeScreenView$10.$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode[this.mode.ordinal()]) {
            case 2: {
                b = this.smartDashContainer.onKey(a);
                break;
            }
            case 1: {
                b = this.mapContainer.onKey(a);
                break;
            }
            default: {
                b = false;
            }
        }
        return b;
    }
    
    public void onMapEvent(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        com.navdy.hud.app.manager.SpeedManager$SpeedUnit a0 = com.navdy.hud.app.manager.SpeedManager.getInstance().getSpeedUnit();
        this.speedLimit = Math.round((float)com.navdy.hud.app.manager.SpeedManager.convert((double)a.currentSpeedLimit, com.navdy.hud.app.manager.SpeedManager$SpeedUnit.METERS_PER_SECOND, a0));
        this.updateSpeedLimitSign();
    }
    
    public void onNavigationModeChange(com.navdy.hud.app.maps.MapEvents$NavigationModeChange a) {
        this.setMode(com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().getCurrentNavigationMode());
    }
    
    public void onPause() {
        if (!this.paused) {
            this.paused = true;
            this.smartDashContainer.onPause();
            this.mapContainer.onPause();
            this.pauseNavigationViewContainer();
            sLogger.v("::onPause");
        }
    }
    
    public void onResume() {
        if (this.paused) {
            this.paused = false;
            switch(com.navdy.hud.app.ui.component.homescreen.HomeScreenView$10.$SwitchMap$com$navdy$hud$app$ui$component$homescreen$HomeScreen$DisplayMode[this.mode.ordinal()]) {
                case 2: {
                    this.mapContainer.onPause();
                    this.laneGuidanceView.onPause();
                    this.smartDashContainer.onResume();
                    break;
                }
                case 1: {
                    this.mapContainer.onResume();
                    this.laneGuidanceView.onResume();
                    this.smartDashContainer.onPause();
                    break;
                }
            }
            this.resumeNavigationViewContainer();
            sLogger.v("::onResume");
        }
    }
    
    public void onSpeedDataExpired(com.navdy.hud.app.manager.SpeedManager$SpeedDataExpired a) {
        this.updateSpeedLimitSign();
    }
    
    public void onSpeedUnitChanged(com.navdy.hud.app.manager.SpeedManager$SpeedUnitChanged a) {
        this.updateSpeedLimitSign();
    }
    
    public void onTrackHand(float f) {
    }
    
    public void resetModeView() {
        if (this.isModeVisible()) {
            sLogger.v("mode-view resetModeView");
            this.mapContainer.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND);
            this.smartDashContainer.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND);
            this.mainscreenRightSection.setView(com.navdy.hud.app.view.MainView$CustomAnimationMode.EXPAND);
        }
    }
    
    public void setDisplayMode(com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode a) {
        if (this.mode != a) {
            this.mode = a;
            this.setDisplayMode(true);
        }
    }
    
    public void setMode(com.navdy.hud.app.maps.NavigationMode a) {
        com.navdy.hud.app.maps.NavigationMode a0 = this.currentNavigationMode;
        label0: {
            label1: {
                if (a0 != a) {
                    break label1;
                }
                if (!this.mapContainer.isOverviewMapMode()) {
                    break label0;
                }
            }
            sLogger.v(new StringBuilder().append("navigation mode changed to ").append(a).toString());
            this.currentNavigationMode = a;
            this.updateLayoutForMode(a);
            this.mapContainer.layoutMap();
            this.bus.post(a);
        }
    }
    
    public void setRecalculating(boolean b) {
        this.isRecalculating = b;
    }
    
    public void setShowCollapsedNotification(boolean b) {
        this.showCollapsedNotif = b;
    }
    
    public boolean shouldAnimateTopViews() {
        boolean b = false;
        com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode a = this.mode;
        com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode a0 = com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode.SMART_DASH;
        label2: {
            label0: {
                label1: {
                    if (a != a0) {
                        break label1;
                    }
                    if (!this.smartDashContainer.shouldShowTopViews()) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    void updateDriverPrefs() {
        this.driverPreferences = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getLocalPreferencesForCurrentDriverProfile(com.navdy.hud.app.HudApplication.getAppContext());
    }
}
