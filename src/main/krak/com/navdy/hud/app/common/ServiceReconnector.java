package com.navdy.hud.app.common;

abstract public class ServiceReconnector {
    final private static int RECONNECT_INTERVAL_MS = 15000;
    final private static int RETRY_INTERVAL_MS = 60000;
    private String action;
    private Runnable connectRunnable;
    private android.content.Context context;
    private android.os.Handler handler;
    final private com.navdy.service.library.log.Logger logger;
    private android.content.ServiceConnection serviceConnection;
    protected android.content.Intent serviceIntent;
    boolean shuttingDown;
    
    public ServiceReconnector(android.content.Context a, android.content.Intent a0, String s) {
        this.logger = new com.navdy.service.library.log.Logger((this).getClass());
        this.connectRunnable = (Runnable)new com.navdy.hud.app.common.ServiceReconnector$1(this);
        this.serviceConnection = (android.content.ServiceConnection)new com.navdy.hud.app.common.ServiceReconnector$2(this);
        this.logger.i("Establishing service connection");
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.context = a;
        this.serviceIntent = a0;
        this.action = s;
        this.handler.post(this.connectRunnable);
    }
    
    static android.content.ComponentName access$000(com.navdy.hud.app.common.ServiceReconnector a, android.content.Intent a0) {
        return a.getServiceComponent(a0);
    }
    
    static android.os.Handler access$100(com.navdy.hud.app.common.ServiceReconnector a) {
        return a.handler;
    }
    
    static com.navdy.service.library.log.Logger access$200(com.navdy.hud.app.common.ServiceReconnector a) {
        return a.logger;
    }
    
    static android.content.Context access$300(com.navdy.hud.app.common.ServiceReconnector a) {
        return a.context;
    }
    
    static String access$400(com.navdy.hud.app.common.ServiceReconnector a) {
        return a.action;
    }
    
    static android.content.ServiceConnection access$500(com.navdy.hud.app.common.ServiceReconnector a) {
        return a.serviceConnection;
    }
    
    static Runnable access$600(com.navdy.hud.app.common.ServiceReconnector a) {
        return a.connectRunnable;
    }
    
    private android.content.ComponentName getServiceComponent(android.content.Intent a) {
        android.content.ComponentName a0 = null;
        java.util.List a1 = this.context.getPackageManager().queryIntentServices(a, 0);
        if (a1.isEmpty()) {
            a0 = null;
        } else {
            android.content.pm.ResolveInfo a2 = (android.content.pm.ResolveInfo)a1.get(0);
            a0 = new android.content.ComponentName(a2.serviceInfo.applicationInfo.packageName, a2.serviceInfo.name);
        }
        return a0;
    }
    
    abstract protected void onConnected(android.content.ComponentName arg, android.os.IBinder arg0);
    
    
    abstract protected void onDisconnected(android.content.ComponentName arg);
    
    
    public void restart() {
        this.context.stopService(this.serviceIntent);
    }
    
    public void shutdown() {
        this.logger.i("shutting down service");
        this.shuttingDown = true;
        this.handler.removeCallbacks(this.connectRunnable);
        this.context.stopService(this.serviceIntent);
    }
}
