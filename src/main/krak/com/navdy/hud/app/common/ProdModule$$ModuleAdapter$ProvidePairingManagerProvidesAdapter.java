package com.navdy.hud.app.common;

final public class ProdModule$$ModuleAdapter$ProvidePairingManagerProvidesAdapter extends dagger.internal.ProvidesBinding implements javax.inject.Provider {
    final private com.navdy.hud.app.common.ProdModule module;
    
    public ProdModule$$ModuleAdapter$ProvidePairingManagerProvidesAdapter(com.navdy.hud.app.common.ProdModule a) {
        super("com.navdy.hud.app.manager.PairingManager", true, "com.navdy.hud.app.common.ProdModule", "providePairingManager");
        this.module = a;
        this.setLibrary(true);
    }
    
    public com.navdy.hud.app.manager.PairingManager get() {
        return this.module.providePairingManager();
    }
    
    public Object get() {
        return this.get();
    }
}
