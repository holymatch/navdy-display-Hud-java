package com.navdy.hud.app.common;

class TimeHelper$1 {
    final static int[] $SwitchMap$com$navdy$service$library$events$settings$DateTimeConfiguration$Clock;
    
    static {
        $SwitchMap$com$navdy$service$library$events$settings$DateTimeConfiguration$Clock = new int[com.navdy.service.library.events.settings.DateTimeConfiguration$Clock.values().length];
        int[] a = $SwitchMap$com$navdy$service$library$events$settings$DateTimeConfiguration$Clock;
        com.navdy.service.library.events.settings.DateTimeConfiguration$Clock a0 = com.navdy.service.library.events.settings.DateTimeConfiguration$Clock.CLOCK_24_HOUR;
        try {
            a[a0.ordinal()] = 1;
        } catch(NoSuchFieldError ignoredException) {
        }
        try {
            $SwitchMap$com$navdy$service$library$events$settings$DateTimeConfiguration$Clock[com.navdy.service.library.events.settings.DateTimeConfiguration$Clock.CLOCK_12_HOUR.ordinal()] = 2;
        } catch(NoSuchFieldError ignoredException0) {
        }
    }
}
