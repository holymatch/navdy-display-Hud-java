package com.navdy.hud.app.common;

abstract public class BaseActivity extends android.app.Activity {
    private mortar.MortarActivityScope activityScope;
    protected com.navdy.service.library.log.Logger logger;
    
    public BaseActivity() {
        this.logger = new com.navdy.service.library.log.Logger((this).getClass());
    }
    
    protected void attachBaseContext(android.content.Context a) {
        super.attachBaseContext(com.navdy.hud.app.profile.HudLocale.onAttach(a));
    }
    
    protected mortar.Blueprint getBlueprint() {
        return null;
    }
    
    public Object getSystemService(String s) {
        Object a = null;
        if (mortar.Mortar.isScopeSystemService(s)) {
            if (this.activityScope == null) {
                throw new IllegalArgumentException("BaseActivity must override getBlueprint to use Mortar internally");
            }
            a = this.activityScope;
        } else {
            a = super.getSystemService(s);
        }
        return a;
    }
    
    public boolean isActivityDestroyed() {
        boolean b = false;
        boolean b0 = this.isFinishing();
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (!this.isDestroyed()) {
                        break label0;
                    }
                }
                b = true;
                break label2;
            }
            b = false;
        }
        return b;
    }
    
    public void onConfigurationChanged(android.content.res.Configuration a) {
        this.logger.v("::onConfigurationChanged");
        super.onConfigurationChanged(a);
        com.navdy.hud.app.profile.HudLocale.onAttach((android.content.Context)this);
    }
    
    protected void onCreate(android.os.Bundle a) {
        this.logger.v("::OnCreate");
        super.onCreate(a);
        mortar.Blueprint a0 = this.getBlueprint();
        if (a0 != null) {
            this.activityScope = mortar.Mortar.requireActivityScope(mortar.Mortar.getScope((android.content.Context)this.getApplication()), a0);
            mortar.Mortar.inject((android.content.Context)this, this);
            this.activityScope.onCreate(a);
        }
    }
    
    protected void onDestroy() {
        this.logger.v("::onDestroy");
        super.onDestroy();
        if (this.isFinishing() && this.activityScope != null) {
            mortar.Mortar.getScope((android.content.Context)this.getApplication()).destroyChild((mortar.MortarScope)this.activityScope);
            this.activityScope = null;
        }
    }
    
    protected void onPause() {
        this.logger.v("::onPause");
        super.onPause();
    }
    
    public void onResume() {
        this.logger.v("::onResume");
        super.onResume();
    }
    
    public void onSaveInstanceState(android.os.Bundle a) {
        this.logger.v("::onSaveInstanceState");
        super.onSaveInstanceState(a);
        if (this.activityScope != null) {
            this.activityScope.onSaveInstanceState(a);
        }
    }
}
