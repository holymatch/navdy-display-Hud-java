package com.navdy.hud.app.config;

public class Setting {
    private static int MAX_PROP_LEN;
    private String description;
    private String name;
    private com.navdy.hud.app.config.Setting$IObserver observer;
    private String path;
    private String property;
    
    static {
        MAX_PROP_LEN = 31;
    }
    
    public Setting(String s, String s0, String s1) {
        this(s, s0, s1, new StringBuilder().append("persist.sys.").append(s0).toString());
    }
    
    public Setting(String s, String s0, String s1, String s2) {
        this.name = s;
        this.path = s0;
        this.description = s1;
        if (s2 != null && s2.length() > MAX_PROP_LEN) {
            throw new IllegalArgumentException(new StringBuilder().append("property name (").append(s2).append(") too long").toString());
        }
        this.property = s2;
    }
    
    protected void changed() {
        if (this.observer != null) {
            this.observer.onChanged(this);
        }
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getPath() {
        return this.path;
    }
    
    public String getProperty() {
        return this.property;
    }
    
    public boolean isEnabled() {
        return true;
    }
    
    public void setObserver(com.navdy.hud.app.config.Setting$IObserver a) {
        this.observer = a;
    }
}
