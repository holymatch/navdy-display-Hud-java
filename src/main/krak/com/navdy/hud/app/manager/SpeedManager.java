package com.navdy.hud.app.manager;

public class SpeedManager {
    final private static double EPSILON = 1e-05;
    final private static long GPS_SPEED_EXPIRY_INTERVAL = 5000L;
    final private static double MAX_VALID_METERS_PER_SEC = 89.41333333333333;
    final private static double METERS_PER_KILOMETER = 1000.0;
    final private static double METERS_PER_MILE = 1609.44;
    final private static int SECONDS_PER_HOUR = 3600;
    final public static int SPEED_NOT_AVAILABLE = -1;
    final private static com.navdy.service.library.log.Logger sLogger;
    final private static com.navdy.hud.app.manager.SpeedManager singleton;
    @Inject
    com.squareup.otto.Bus bus;
    @Inject
    com.navdy.hud.app.profile.DriverProfileManager driverProfileManager;
    private volatile int gpsSpeed;
    private Runnable gpsSpeedExpiryRunnable;
    private android.os.Handler handler;
    private volatile int obdSpeed;
    private volatile float rawGpsSpeed;
    private volatile long rawGpsSpeedTimeStamp;
    private volatile int rawObdSpeed;
    private volatile long rawObdSpeedTimeStamp;
    private com.navdy.hud.app.manager.SpeedManager$SpeedUnit speedUnit;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.manager.SpeedManager.class);
        singleton = new com.navdy.hud.app.manager.SpeedManager();
    }
    
    public SpeedManager() {
        this.obdSpeed = -1;
        this.rawObdSpeed = -1;
        this.rawObdSpeedTimeStamp = 0L;
        this.rawGpsSpeed = -1f;
        this.rawGpsSpeedTimeStamp = 0L;
        this.speedUnit = com.navdy.hud.app.manager.SpeedManager$SpeedUnit.MILES_PER_HOUR;
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.bus.register(this);
        this.setUnit();
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static int access$102(com.navdy.hud.app.manager.SpeedManager a, int i) {
        a.gpsSpeed = i;
        return i;
    }
    
    private static boolean almostEqual(float f, float f0) {
        return (double)Math.abs(f - f0) < 1e-05;
    }
    
    public static int convert(double d, com.navdy.hud.app.manager.SpeedManager$SpeedUnit a) {
        return com.navdy.hud.app.manager.SpeedManager.convert(d, com.navdy.hud.app.manager.SpeedManager$SpeedUnit.METERS_PER_SECOND, a);
    }
    
    public static int convert(double d, com.navdy.hud.app.manager.SpeedManager$SpeedUnit a, com.navdy.hud.app.manager.SpeedManager$SpeedUnit a0) {
        return Math.round(com.navdy.hud.app.manager.SpeedManager.convertWithPrecision(d, a, a0));
    }
    
    public static float convertWithPrecision(double d, com.navdy.hud.app.manager.SpeedManager$SpeedUnit a, com.navdy.hud.app.manager.SpeedManager$SpeedUnit a0) {
        float f = 0.0f;
        int i = (d > 0.0) ? 1 : (d == 0.0) ? 0 : -1;
        label2: {
            label0: {
                label1: {
                    if (i == 0) {
                        break label1;
                    }
                    if (d != -1.0) {
                        break label0;
                    }
                }
                f = (float)d;
                break label2;
            }
            if (a != a0) {
                switch(com.navdy.hud.app.manager.SpeedManager$2.$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[a.ordinal()]) {
                    case 3: {
                        break;
                    }
                    case 2: {
                        d = d * 1000.0 / 3600.0;
                        break;
                    }
                    case 1: {
                        d = d * 1609.44 / 3600.0;
                        break;
                    }
                    default: {
                        d = 0.0;
                    }
                }
                switch(com.navdy.hud.app.manager.SpeedManager$2.$SwitchMap$com$navdy$hud$app$manager$SpeedManager$SpeedUnit[a0.ordinal()]) {
                    case 3: {
                        f = (float)d;
                        break;
                    }
                    case 2: {
                        f = (float)(d * 3600.0 / 1000.0);
                        break;
                    }
                    case 1: {
                        f = (float)(d * 3600.0 / 1609.44);
                        break;
                    }
                    default: {
                        f = 0.0f;
                    }
                }
            } else {
                f = (float)d;
            }
        }
        return f;
    }
    
    public static com.navdy.hud.app.manager.SpeedManager getInstance() {
        return singleton;
    }
    
    private void setUnit() {
        com.navdy.hud.app.manager.SpeedManager$SpeedUnit a = (this.driverProfileManager.getCurrentProfile().getUnitSystem() != com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem.UNIT_SYSTEM_METRIC) ? com.navdy.hud.app.manager.SpeedManager$SpeedUnit.MILES_PER_HOUR : com.navdy.hud.app.manager.SpeedManager$SpeedUnit.KILOMETERS_PER_HOUR;
        if (this.speedUnit == a) {
            sLogger.v(new StringBuilder().append("ondriverprofileupdated speed unit is still ").append(a.name()).toString());
        } else {
            sLogger.i(new StringBuilder().append("ondriverprofileupdate speed unit has changed to ").append(a.name()).toString());
            this.setSpeedUnit(a);
            this.bus.post(new com.navdy.hud.app.manager.SpeedManager$SpeedUnitChanged());
        }
    }
    
    private boolean updateSpeed(float f, long j) {
        boolean b = false;
        synchronized(this) {
            if (com.navdy.hud.app.manager.SpeedManager.almostEqual(this.rawGpsSpeed, f)) {
                this.rawGpsSpeedTimeStamp = j;
                b = false;
            } else if (f < 0.0f) {
                b = false;
            } else if ((double)f > 89.41333333333333) {
                b = false;
            } else {
                this.rawGpsSpeedTimeStamp = j;
                this.rawGpsSpeed = f;
                this.gpsSpeed = com.navdy.hud.app.manager.SpeedManager.convert((double)f, com.navdy.hud.app.manager.SpeedManager$SpeedUnit.METERS_PER_SECOND, this.speedUnit);
                b = true;
            }
        }
        /*monexit(this)*/;
        return b;
    }
    
    public int getCurrentSpeed() {
        int i = 0;
        synchronized(this) {
            i = this.getObdSpeed();
            if (i == -1) {
                i = this.getGpsSpeed();
            }
        }
        /*monexit(this)*/;
        return i;
    }
    
    public com.navdy.hud.app.analytics.RawSpeed getCurrentSpeedInMetersPerSecond() {
        com.navdy.hud.app.analytics.RawSpeed a = null;
        synchronized(this) {
            a = (this.rawObdSpeed == -1) ? new com.navdy.hud.app.analytics.RawSpeed(this.rawGpsSpeed, this.rawGpsSpeedTimeStamp) : new com.navdy.hud.app.analytics.RawSpeed(com.navdy.hud.app.manager.SpeedManager.convertWithPrecision((double)this.rawObdSpeed, com.navdy.hud.app.manager.SpeedManager$SpeedUnit.KILOMETERS_PER_HOUR, com.navdy.hud.app.manager.SpeedManager$SpeedUnit.METERS_PER_SECOND), this.rawObdSpeedTimeStamp);
        }
        /*monexit(this)*/;
        return a;
    }
    
    public int getGpsSpeed() {
        int i = 0;
        synchronized(this) {
            i = this.gpsSpeed;
        }
        /*monexit(this)*/;
        return i;
    }
    
    public float getGpsSpeedInMetersPerSecond() {
        float f = 0.0f;
        synchronized(this) {
            f = this.rawGpsSpeed;
        }
        /*monexit(this)*/;
        return f;
    }
    
    public int getObdSpeed() {
        return this.obdSpeed;
    }
    
    public int getRawObdSpeed() {
        return this.rawObdSpeed;
    }
    
    public com.navdy.hud.app.manager.SpeedManager$SpeedUnit getSpeedUnit() {
        return this.speedUnit;
    }
    
    public void onDriverProfileChanged(com.navdy.hud.app.event.DriverProfileChanged a) {
        this.setUnit();
    }
    
    public void onDriverProfileUpdated(com.navdy.hud.app.event.DriverProfileUpdated a) {
        if (a.state == com.navdy.hud.app.event.DriverProfileUpdated$State.UPDATED) {
            this.setUnit();
        }
    }
    
    public boolean setGpsSpeed(float f, long j) {
        boolean b = false;
        synchronized(this) {
            if (this.gpsSpeedExpiryRunnable == null) {
                this.gpsSpeedExpiryRunnable = (Runnable)new com.navdy.hud.app.manager.SpeedManager$1(this);
            }
            this.handler.removeCallbacks(this.gpsSpeedExpiryRunnable);
            this.handler.postDelayed(this.gpsSpeedExpiryRunnable, 5000L);
            b = this.updateSpeed(f, j);
        }
        /*monexit(this)*/;
        return b;
    }
    
    public void setObdSpeed(int i, long j) {
        label2: synchronized(this) {
            label0: {
                label1: {
                    if (i < -1) {
                        break label1;
                    }
                    if (i <= 256) {
                        break label0;
                    }
                }
                sLogger.w(new StringBuilder().append("Invalid OBD speed ignored:").append(i).toString());
                break label2;
            }
            this.rawObdSpeed = i;
            this.rawObdSpeedTimeStamp = j;
            this.obdSpeed = com.navdy.hud.app.manager.SpeedManager.convert((double)i, com.navdy.hud.app.manager.SpeedManager$SpeedUnit.KILOMETERS_PER_HOUR, this.speedUnit);
        }
        /*monexit(this)*/;
    }
    
    public void setSpeedUnit(com.navdy.hud.app.manager.SpeedManager$SpeedUnit a) {
        if (a != this.speedUnit) {
            this.gpsSpeed = com.navdy.hud.app.manager.SpeedManager.convert((double)this.gpsSpeed, this.speedUnit, a);
            this.obdSpeed = com.navdy.hud.app.manager.SpeedManager.convert((double)this.obdSpeed, this.speedUnit, a);
            this.speedUnit = a;
        }
    }
}
