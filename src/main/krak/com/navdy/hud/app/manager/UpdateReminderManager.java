package com.navdy.hud.app.manager;

public class UpdateReminderManager {
    private static long DIAL_CONNECT_CHECK_INTERVAL = 0L;
    final public static String DIAL_REMINDER_TIME = "dial_reminder_time";
    final public static String EXTRA_UPDATE_REMINDER = "UPDATE_REMINDER";
    private static long INITIAL_REMINDER_INTERVAL = 0L;
    final public static String OTA_REMINDER_TIME = "ota_reminder_time";
    private static long REMINDER_INTERVAL;
    final private static com.navdy.service.library.log.Logger sLogger;
    @Inject
    com.squareup.otto.Bus bus;
    private boolean dialUpdateAvailable;
    private android.os.Handler handler;
    private com.navdy.hud.app.manager.UpdateReminderManager$ReminderRunnable reminderRunnable;
    @Inject
    android.content.SharedPreferences sharedPreferences;
    private boolean timeAvailable;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.manager.UpdateReminderManager.class);
        INITIAL_REMINDER_INTERVAL = java.util.concurrent.TimeUnit.DAYS.toMillis(10L);
        REMINDER_INTERVAL = java.util.concurrent.TimeUnit.DAYS.toMillis(1L);
        DIAL_CONNECT_CHECK_INTERVAL = java.util.concurrent.TimeUnit.MINUTES.toMillis(15L);
    }
    
    public UpdateReminderManager(android.content.Context a) {
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.reminderRunnable = null;
        this.timeAvailable = false;
        this.dialUpdateAvailable = false;
        mortar.Mortar.inject(a, this);
        this.bus.register(this);
    }
    
    static long access$000() {
        return REMINDER_INTERVAL;
    }
    
    static long access$100() {
        return DIAL_CONNECT_CHECK_INTERVAL;
    }
    
    static boolean access$200(com.navdy.hud.app.manager.UpdateReminderManager a) {
        return a.isOTAUpdateRequired();
    }
    
    static void access$300(com.navdy.hud.app.manager.UpdateReminderManager a) {
        a.scheduleReminder();
    }
    
    private void clearReminder() {
        if (this.reminderRunnable != null) {
            this.handler.removeCallbacks((Runnable)this.reminderRunnable);
            this.reminderRunnable = null;
        }
    }
    
    private boolean isOTAUpdateRequired() {
        return com.navdy.hud.app.util.OTAUpdateService.isUpdateAvailable();
    }
    
    private void scheduleReminder() {
        if (this.timeAvailable) {
            boolean b = false;
            this.clearReminder();
            long j = this.sharedPreferences.getLong("ota_reminder_time", 0L);
            long j0 = (this.dialUpdateAvailable) ? this.sharedPreferences.getLong("dial_reminder_time", 0L) : 0L;
            int i = (j < 0L) ? -1 : (j == 0L) ? 0 : 1;
            label2: {
                label3: {
                    if (i == 0) {
                        break label3;
                    }
                    if (j0 == 0L) {
                        b = false;
                        break label2;
                    }
                    if (j <= j0) {
                        b = false;
                        break label2;
                    }
                }
                if (j0 == 0L) {
                    j = 0L;
                    b = false;
                } else {
                    j = j0;
                    b = true;
                }
            }
            long j1 = System.currentTimeMillis();
            if (j != 0L) {
                long j2 = j - j1;
                label1: {
                    label0: {
                        if (!b) {
                            break label0;
                        }
                        if (j2 >= java.util.concurrent.TimeUnit.SECONDS.toMillis(1L)) {
                            break label0;
                        }
                        j2 = java.util.concurrent.TimeUnit.SECONDS.toMillis(1L);
                        break label1;
                    }
                    if (j2 < 0L) {
                        j2 = 0L;
                    }
                }
                com.navdy.service.library.log.Logger a = sLogger;
                String s = b ? "dial" : "OTA";
                Object[] a0 = new Object[2];
                a0[0] = s;
                a0[1] = Long.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(j2));
                a.v(String.format("new scheduling %s reminder for +%d seconds.", a0));
                this.reminderRunnable = new com.navdy.hud.app.manager.UpdateReminderManager$ReminderRunnable(this, b);
                this.handler.postDelayed((Runnable)this.reminderRunnable, j2);
            }
        }
    }
    
    public void handleDialUpdate(com.navdy.hud.app.device.dial.DialManager$DialUpdateStatus a) {
        long j = this.sharedPreferences.getLong("dial_reminder_time", 0L);
        if (a.available) {
            long j0 = System.currentTimeMillis();
            if (j != 0L) {
                com.navdy.service.library.log.Logger a0 = sLogger;
                Object[] a1 = new Object[1];
                a1[0] = Long.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(j - j0));
                a0.v(String.format("handleDialUpdate(): dial update reminder time already set to +%d seconds", a1));
            } else {
                long j1 = INITIAL_REMINDER_INTERVAL;
                this.sharedPreferences.edit().putLong("dial_reminder_time", j0 + j1).apply();
                com.navdy.service.library.log.Logger a2 = sLogger;
                Object[] a3 = new Object[1];
                a3[0] = Long.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(INITIAL_REMINDER_INTERVAL));
                a2.v(String.format("handleDialUpdate(): setting dial update reminder time to +%d seconds", a3));
            }
            this.dialUpdateAvailable = true;
            this.scheduleReminder();
        } else if (j != 0L) {
            this.sharedPreferences.edit().remove("dial_reminder_time").apply();
            sLogger.v("handleDialUpdate(): clearing dial reminder");
        }
    }
    
    public void handleOTAUpdate(com.navdy.hud.app.util.OTAUpdateService$UpdateVerified a) {
        long j = this.sharedPreferences.getLong("ota_reminder_time", 0L);
        long j0 = System.currentTimeMillis();
        if (j != 0L) {
            com.navdy.service.library.log.Logger a0 = sLogger;
            Object[] a1 = new Object[1];
            a1[0] = Long.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(j - j0));
            a0.v(String.format("handleOTAUpdate() OTA reminder already set for +%d seconds", a1));
        } else {
            long j1 = INITIAL_REMINDER_INTERVAL;
            this.sharedPreferences.edit().putLong("ota_reminder_time", j1 + j0).apply();
            com.navdy.service.library.log.Logger a2 = sLogger;
            Object[] a3 = new Object[1];
            a3[0] = Long.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(INITIAL_REMINDER_INTERVAL));
            a2.v(String.format("handleOTAUpdate() setting OTA reminder for +%d seconds ", a3));
        }
        this.scheduleReminder();
    }
    
    public void onDateTimeAvailable(com.navdy.hud.app.common.TimeHelper$DateTimeAvailableEvent a) {
        this.timeAvailable = true;
        long j = this.sharedPreferences.getLong("ota_reminder_time", 0L);
        if (this.isOTAUpdateRequired()) {
            long j0 = System.currentTimeMillis();
            if (j != 0L) {
                com.navdy.service.library.log.Logger a0 = sLogger;
                Object[] a1 = new Object[1];
                a1[0] = Long.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(j - j0));
                a0.v(String.format("onDateTimeAvailable() OTA reminder time already set to +%d seconds", a1));
            } else {
                long j1 = INITIAL_REMINDER_INTERVAL;
                this.sharedPreferences.edit().putLong("ota_reminder_time", j1 + j0).apply();
                com.navdy.service.library.log.Logger a2 = sLogger;
                Object[] a3 = new Object[1];
                a3[0] = Long.valueOf(java.util.concurrent.TimeUnit.MILLISECONDS.toSeconds(INITIAL_REMINDER_INTERVAL));
                a2.v(String.format("onDateTimeAvailable(): setting OTA reminder time to +%d seconds ", a3));
            }
        } else if (j != 0L) {
            this.sharedPreferences.edit().remove("ota_reminder_time").apply();
            sLogger.v("onDateTimeAvailable(): clearing ota reminder");
        }
        this.scheduleReminder();
    }
}
