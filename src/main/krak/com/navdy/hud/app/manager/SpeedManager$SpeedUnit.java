package com.navdy.hud.app.manager;


public enum SpeedManager$SpeedUnit {
    MILES_PER_HOUR(0),
    KILOMETERS_PER_HOUR(1),
    METERS_PER_SECOND(2);

    private int value;
    SpeedManager$SpeedUnit(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class SpeedManager$SpeedUnit extends Enum {
//    final private static com.navdy.hud.app.manager.SpeedManager$SpeedUnit[] $VALUES;
//    final public static com.navdy.hud.app.manager.SpeedManager$SpeedUnit KILOMETERS_PER_HOUR;
//    final public static com.navdy.hud.app.manager.SpeedManager$SpeedUnit METERS_PER_SECOND;
//    final public static com.navdy.hud.app.manager.SpeedManager$SpeedUnit MILES_PER_HOUR;
//    
//    static {
//        MILES_PER_HOUR = new com.navdy.hud.app.manager.SpeedManager$SpeedUnit("MILES_PER_HOUR", 0);
//        KILOMETERS_PER_HOUR = new com.navdy.hud.app.manager.SpeedManager$SpeedUnit("KILOMETERS_PER_HOUR", 1);
//        METERS_PER_SECOND = new com.navdy.hud.app.manager.SpeedManager$SpeedUnit("METERS_PER_SECOND", 2);
//        com.navdy.hud.app.manager.SpeedManager$SpeedUnit[] a = new com.navdy.hud.app.manager.SpeedManager$SpeedUnit[3];
//        a[0] = MILES_PER_HOUR;
//        a[1] = KILOMETERS_PER_HOUR;
//        a[2] = METERS_PER_SECOND;
//        $VALUES = a;
//    }
//    
//    private SpeedManager$SpeedUnit(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.hud.app.manager.SpeedManager$SpeedUnit valueOf(String s) {
//        return (com.navdy.hud.app.manager.SpeedManager$SpeedUnit)Enum.valueOf(com.navdy.hud.app.manager.SpeedManager$SpeedUnit.class, s);
//    }
//    
//    public static com.navdy.hud.app.manager.SpeedManager$SpeedUnit[] values() {
//        return $VALUES.clone();
//    }
//}
//