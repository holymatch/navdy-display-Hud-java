package com.navdy.hud.app.manager;

class SpeedManager$1 implements Runnable {
    final com.navdy.hud.app.manager.SpeedManager this$0;
    
    SpeedManager$1(com.navdy.hud.app.manager.SpeedManager a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.manager.SpeedManager.access$000().d("Gps RawSpeed data expired, resetting to zero");
        com.navdy.hud.app.manager.SpeedManager.access$102(this.this$0, -1);
        com.navdy.hud.app.HudApplication.getApplication().getBus().post(new com.navdy.hud.app.manager.SpeedManager$SpeedDataExpired());
    }
}
