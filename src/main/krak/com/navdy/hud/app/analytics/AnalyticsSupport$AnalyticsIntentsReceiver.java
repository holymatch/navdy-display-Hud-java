package com.navdy.hud.app.analytics;

public class AnalyticsSupport$AnalyticsIntentsReceiver extends android.content.BroadcastReceiver {
    public AnalyticsSupport$AnalyticsIntentsReceiver() {
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        String s = a0.getStringExtra("tag");
        boolean b = a0.getBooleanExtra("include_device_info", false);
        if (s != null) {
            String[] a1 = a0.getStringArrayExtra("arguments");
            if (a1 == null) {
                a1 = new String[0];
            }
            if (s.equals("Analytics_New_Device")) {
                com.navdy.hud.app.analytics.AnalyticsSupport.access$4600();
            } else {
                com.navdy.hud.app.analytics.AnalyticsSupport.access$4700(s, b, a1);
            }
        }
    }
}
