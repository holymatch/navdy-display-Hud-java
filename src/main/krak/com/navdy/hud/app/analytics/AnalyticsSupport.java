package com.navdy.hud.app.analytics;

public class AnalyticsSupport {
    final private static String ANALYTICS_EVENT_ADAPTIVE_BRIGHTNESS_ADJUSTMENT_CHANGE = "Adaptive_Brightness_Adjustment_Change";
    final private static String ANALYTICS_EVENT_ATTEMPT_TO_CONNECT = "Attempt_To_Connect";
    final private static String ANALYTICS_EVENT_BAD_ROUTE_POSITION = "Route_Bad_Pos";
    final private static String ANALYTICS_EVENT_BRIGHTNESS_ADJUSTMENT_CHANGE = "Brightness_Adjustment_Change";
    final private static String ANALYTICS_EVENT_BRIGHTNESS_CHANGE = "Brightness_Change";
    final private static String ANALYTICS_EVENT_CPU_OVERHEAT = "Cpu_Overheat";
    final private static String ANALYTICS_EVENT_DASH_GAUGE_SETTING = "Dash_Gauge_Setting";
    final private static String ANALYTICS_EVENT_DESTINATION_SUGGESTION = "Destination_Suggestion_Show";
    final private static String ANALYTICS_EVENT_DIAL_UPDATE_PROMPT = "Dial_Update_Prompt";
    final private static String ANALYTICS_EVENT_DISPLAY_UPDATE_PROMPT = "Display_Update_Prompt";
    final private static String ANALYTICS_EVENT_DISTANCE_TRAVELLED = "Distance_Travelled";
    final private static String ANALYTICS_EVENT_DRIVER_PREFERENCES = "Driver_Preferences";
    final private static String ANALYTICS_EVENT_FUEL_LEVEL = "Fuel_Level";
    final public static String ANALYTICS_EVENT_GLANCE_ADVANCE = "Glance_Advance";
    final public static String ANALYTICS_EVENT_GLANCE_DELETE_ALL = "Glance_Delete_All";
    final public static String ANALYTICS_EVENT_GLANCE_DISMISS = "Glance_Dismiss";
    final public static String ANALYTICS_EVENT_GLANCE_OPEN_FULL = "Glance_Open_Full";
    final public static String ANALYTICS_EVENT_GLANCE_OPEN_MINI = "Glance_Open_Mini";
    final private static String ANALYTICS_EVENT_GLYMPSE_SENT = "Glympse_Sent";
    final private static String ANALYTICS_EVENT_GLYMPSE_VIEWED = "Glympse_Viewed";
    final public static String ANALYTICS_EVENT_GPS_ACCURACY_STATISTICS = "GPS_Accuracy";
    final public static String ANALYTICS_EVENT_GPS_ACQUIRE_LOCATION = "GPS_Acquired_Location";
    final public static String ANALYTICS_EVENT_GPS_ATTEMPT_ACQUIRE_LOCATION = "GPS_Attempt_Acquire_Location";
    final public static String ANALYTICS_EVENT_GPS_CALIBRATION_IMU_DONE = "GPS_Calibration_IMU_Done";
    final public static String ANALYTICS_EVENT_GPS_CALIBRATION_LOST = "GPS_Calibration_Lost";
    final public static String ANALYTICS_EVENT_GPS_CALIBRATION_SENSOR_DONE = "GPS_Calibration_Sensor_Done";
    final public static String ANALYTICS_EVENT_GPS_CALIBRATION_START = "GPS_Calibration_Start";
    final public static String ANALYTICS_EVENT_GPS_CALIBRATION_VIN_SWITCH = "GPS_Calibration_VinSwitch";
    final public static String ANALYTICS_EVENT_GPS_LOST_LOCATION = "GPS_Lost_Signal";
    final public static String ANALYTICS_EVENT_IAP_FAILURE = "Mobile_iAP_Failure";
    final public static String ANALYTICS_EVENT_IAP_RETRY_SUCCESS = "Mobile_iAP_Retry_Success";
    final private static String ANALYTICS_EVENT_KEY_FAILED = "Key_Failed";
    final private static String ANALYTICS_EVENT_MENU_SELECTION = "Menu_Selection";
    final private static String ANALYTICS_EVENT_MILEAGE = "Navdy_Mileage";
    final private static String ANALYTICS_EVENT_MOBILE_APP_CONNECT = "Mobile_App_Connect";
    final private static String ANALYTICS_EVENT_MOBILE_APP_DISCONNECT = "Mobile_App_Disconnect";
    final private static String ANALYTICS_EVENT_MUSIC_ACTION = "Music_Action";
    final private static String ANALYTICS_EVENT_NAVIGATION_ARRIVED = "Navigation_Arrived";
    final private static String ANALYTICS_EVENT_NAVIGATION_DESTINATION_SET = "Navigation_Destination_Set";
    final private static String ANALYTICS_EVENT_NAVIGATION_MANEUVER_MISSED = "Navigation_Maneuver_Missed";
    final private static String ANALYTICS_EVENT_NAVIGATION_MANEUVER_REQUIRED = "Navigation_Maneuver_Required";
    final private static String ANALYTICS_EVENT_NAVIGATION_MANEUVER_WARNING = "Navigation_Maneuver_Warning";
    final private static String ANALYTICS_EVENT_NAVIGATION_PREFERENCES = "Navigation_Preferences";
    final private static String ANALYTICS_EVENT_NAVIGATION_REGION_CHANGE = "Map_Region_Change";
    final private static String ANALYTICS_EVENT_NEARBY_SEARCH_ARRIVED = "Nearby_Search_Arrived";
    final private static String ANALYTICS_EVENT_NEARBY_SEARCH_CLOSED = "Nearby_Search_Closed";
    final private static String ANALYTICS_EVENT_NEARBY_SEARCH_DISMISS = "Nearby_Search_Dismiss";
    final private static String ANALYTICS_EVENT_NEARBY_SEARCH_ENDED = "Nearby_Search_Ended";
    final private static String ANALYTICS_EVENT_NEARBY_SEARCH_NO_RESULTS = "Nearby_Search_No_Results";
    final private static String ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_CLOSE = "Nearby_Search_Results_Close";
    final private static String ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_DISMISS = "Nearby_Search_Results_Dismiss";
    final private static String ANALYTICS_EVENT_NEARBY_SEARCH_RESULTS_VIEW = "Nearby_Search_Results_View";
    final private static String ANALYTICS_EVENT_NEARBY_SEARCH_RETURN_MM = "Nearby_Search_Return_Main_Menu";
    final private static String ANALYTICS_EVENT_NEARBY_SEARCH_SELECTION = "Nearby_Search_Selection";
    final private static String ANALYTICS_EVENT_NEARBY_SEARCH_TRIP_CANCELLED = "Nearby_Search_Trip_Cancelled";
    final private static String ANALYTICS_EVENT_NEARBY_SEARCH_TRIP_INITIATED = "Nearby_Search_Trip_Initiated";
    final public static String ANALYTICS_EVENT_NEW_DEVICE = "Analytics_New_Device";
    final private static String ANALYTICS_EVENT_OBD_CONNECTED = "OBD_Connected";
    final private static String ANALYTICS_EVENT_OBD_LISTENING_POSTED = "OBD_Listening_Posted";
    final private static String ANALYTICS_EVENT_OBD_LISTENING_STATUS = "OBD_Listening_Status";
    final private static String ANALYTICS_EVENT_OFFLINE_MAPS_UPDATED = "Offline_Maps_Updated";
    final private static String ANALYTICS_EVENT_OPTION_SELECTED = "Option_Selected";
    final private static String ANALYTICS_EVENT_OTADOWNLOAD = "OTA_Download";
    final private static String ANALYTICS_EVENT_OTAINSTALL = "OTA_Install";
    final private static String ANALYTICS_EVENT_OTA_INSTALL_RESULT = "OTA_Install_Result";
    final private static String ANALYTICS_EVENT_PHONE_DISCONNECTED = "Phone_Disconnected";
    final private static String ANALYTICS_EVENT_ROUTE_COMPLETED = "Route_Completed";
    final private static String ANALYTICS_EVENT_SHUTDOWN = "Shutdown";
    final private static String ANALYTICS_EVENT_SMS_SENT = "sms_sent";
    final private static String ANALYTICS_EVENT_SWIPED_PREFIX = "Swiped_";
    final private static String ANALYTICS_EVENT_TEMPERATURE_CHANGE = "Temperature_Change";
    final private static String ANALYTICS_EVENT_VEHICLE_TELEMETRY_DATA = "Vehicle_Telemetry_Data";
    final private static String ANALYTICS_EVENT_VOICE_SEARCH_ANDROID = "Voice_Search_Android";
    final private static String ANALYTICS_EVENT_VOICE_SEARCH_IOS = "Voice_Search_iOS";
    final private static String ANALYTICS_EVENT_WAKEUP = "Wakeup";
    final private static String ANALYTICS_EVENT_ZERO_FUEL_DATA = "OBD_Zero_Fuel_Level";
    final public static String ANALYTICS_INTENT = "com.navdy.hud.app.analytics.AnalyticsEvent";
    final public static String ANALYTICS_INTENT_EXTRA_ARGUMENTS = "arguments";
    final public static String ANALYTICS_INTENT_EXTRA_INCLUDE_REMOTE_DEVICE_INFO = "include_device_info";
    final public static String ANALYTICS_INTENT_EXTRA_TAG_NAME = "tag";
    final private static String ATTR_BRIGHTNESS_ADJ_NEW = "New";
    final private static String ATTR_BRIGHTNESS_ADJ_PREVIOUS = "Previous";
    final private static String ATTR_BRIGHTNESS_BRIGHTNESS = "Brightness";
    final private static String ATTR_BRIGHTNESS_LIGHT_SENSOR = "Light_Sensor";
    final private static String ATTR_BRIGHTNESS_NEW = "New";
    final private static String ATTR_BRIGHTNESS_PREVIOUS = "Previous";
    final private static String ATTR_BRIGHTNESS_RAW1_SENSOR = "Raw1_Sensor";
    final private static String ATTR_BRIGHTNESS_RAW_SENSOR = "Raw_Sensor";
    final private static String ATTR_CONN_ATTEMPT_NEW_DEVICE = "Was_New_Device";
    final private static String ATTR_CONN_ATTEMPT_TIME = "Time_To_Connect";
    final private static String ATTR_CONN_CAR_MAKE = "Car_Make";
    final private static String ATTR_CONN_CAR_MODEL = "Car_Model";
    final private static String ATTR_CONN_CAR_YEAR = "Car_Year";
    final private static String ATTR_CONN_DEVICE_ID = "Device_Id";
    final private static String ATTR_CONN_REMOTE_CLIENT_VERSION = "Remote_Client_Version";
    final private static String ATTR_CONN_REMOTE_DEVICE_ID = "Remote_Device_Id";
    final private static String ATTR_CONN_REMOTE_DEVICE_NAME = "Remote_Device_Name";
    final private static String ATTR_CONN_REMOTE_PLATFORM = "Remote_Platform";
    final private static String ATTR_CONN_VIN = "VIN";
    final private static String ATTR_CPU_OVERHEAT_STATE = "State";
    final private static String ATTR_CPU_OVERHEAT_UPTIME = "Uptime";
    final private static String ATTR_DESTINATION_SUGGESTION_ACCEPTED = "Accepted";
    final private static String ATTR_DESTINATION_SUGGESTION_TYPE = "Type";
    final private static String ATTR_DISC_CAR_MAKE = "Car_Make";
    final private static String ATTR_DISC_CAR_MODEL = "Car_Model";
    final private static String ATTR_DISC_CAR_YEAR = "Car_Year";
    final private static String ATTR_DISC_ELAPSED_TIME = "Elapsed_Time";
    final private static String ATTR_DISC_FORCE_RECONNECT = "Force_Reconnect";
    final private static String ATTR_DISC_FORCE_RECONNECT_REASON = "Force_Reconnect_Reason";
    final private static String ATTR_DISC_MOVING = "While_Moving";
    final private static String ATTR_DISC_PHONE_MAKE = "Phone_Make";
    final private static String ATTR_DISC_PHONE_MODEL = "Phone_Model";
    final private static String ATTR_DISC_PHONE_OS = "Phone_OS";
    final private static String ATTR_DISC_PHONE_TYPE = "Phone_Type";
    final private static String ATTR_DISC_REASON = "Reason";
    final private static String ATTR_DISC_STATUS = "Status";
    final private static String ATTR_DISC_VIN = "VIN";
    final private static String ATTR_DISTANCE = "Distance";
    final private static String ATTR_DRIVER_PREFERENCES_AUTO_ON = "Auto_On";
    final private static String ATTR_DRIVER_PREFERENCES_DISPLAY_FORMAT = "Display_Format";
    final private static String ATTR_DRIVER_PREFERENCES_FEATURE_MODE = "Feature_Mode";
    final private static String ATTR_DRIVER_PREFERENCES_GESTURES = "Gestures";
    final private static String ATTR_DRIVER_PREFERENCES_GLANCES = "Glances";
    final private static String ATTR_DRIVER_PREFERENCES_LIMIT_BANDWIDTH = "Limit_Bandwidth";
    final private static String ATTR_DRIVER_PREFERENCES_LOCALE = "Locale";
    final private static String ATTR_DRIVER_PREFERENCES_MANUAL_ZOOM = "Manual_Zoom";
    final private static String ATTR_DRIVER_PREFERENCES_OBD = "OBD";
    final private static String ATTR_DRIVER_PREFERENCES_UNITS = "Units";
    final private static String ATTR_DURATION = "Duration";
    final private static String ATTR_EVENT_AVERAGE_MPG = "Event_Average_Mpg";
    final private static String ATTR_EVENT_AVERAGE_RPM = "Event_Average_Rpm";
    final private static String ATTR_EVENT_ENGINE_TEMPERATURE = "Event_Engine_Temperature";
    final private static String ATTR_EVENT_FUEL_LEVEL = "Event_Fuel_Level";
    final private static String ATTR_EVENT_MAX_G = "Event_Max_G";
    final private static String ATTR_EVENT_MAX_G_ANGLE = "Event_Max_G_Angle";
    final private static String ATTR_FUEL_CONSUMPTION = "Fuel_Consumption";
    final private static String ATTR_FUEL_DISTANCE = "Distance";
    final private static String ATTR_FUEL_LEVEL = "Fuel_Level";
    final private static String ATTR_FUEL_VIN = "VIN";
    final private static String ATTR_GAUGE_ID = "Gauge_name";
    final private static String ATTR_GAUGE_SIDE = "Side";
    final private static String ATTR_GAUGE_SIDE_LEFT = "Left";
    final private static String ATTR_GAUGE_SIDE_RIGHT = "Right";
    final private static String ATTR_GLANCE_NAME = "Glance_Name";
    final private static String ATTR_GLANCE_NAV_METHOD = "Nav_Method";
    final private static String ATTR_GLYMPSE_ID = "Id";
    final private static String ATTR_GLYMPSE_MESSAGE = "Message";
    final private static String ATTR_GLYMPSE_SHARE = "Share";
    final private static String ATTR_GLYMPSE_SUCCESS = "Success";
    final private static String ATTR_KEY_FAIL_ERROR = "Error";
    final private static String ATTR_KEY_FAIL_KEY_TYPE = "Key_Type";
    final private static String ATTR_MENU_NAME = "Name";
    final private static String ATTR_MUSIC_ACTION_TYPE = "Type";
    final private static String ATTR_NAVDY_KILOMETERS = "Navdy_Kilometers";
    final private static String ATTR_NAVDY_USAGE_RATE = "Navdy_Usage_Rate";
    final private static String ATTR_NAVIGATING = "Navigating";
    final private static String ATTR_NAVIGATION_PREFERENCES_ALLOW_AUTO_TRAINS = "Allow_Auto_Trains";
    final private static String ATTR_NAVIGATION_PREFERENCES_ALLOW_FERRIES = "Allow_Ferries";
    final private static String ATTR_NAVIGATION_PREFERENCES_ALLOW_HIGHWAYS = "Allow_Highways";
    final private static String ATTR_NAVIGATION_PREFERENCES_ALLOW_HOV_LANES = "Allow_HOV_Lanes";
    final private static String ATTR_NAVIGATION_PREFERENCES_ALLOW_TOLL_ROADS = "Allow_Toll_Roads";
    final private static String ATTR_NAVIGATION_PREFERENCES_ALLOW_TUNNELS = "Allow_Tunnels";
    final private static String ATTR_NAVIGATION_PREFERENCES_ALLOW_UNPAVED_ROADS = "Allow_Unpaved_Roads";
    final private static String ATTR_NAVIGATION_PREFERENCES_REROUTE_FOR_TRAFFIC = "Reroute_For_Traffic";
    final private static String ATTR_NAVIGATION_PREFERENCES_ROUTING_TYPE = "Routing_Type";
    final private static String ATTR_NAVIGATION_PREFERENCES_SHOW_TRAFFIC_NAVIGATING = "Show_Traffic_Navigating";
    final private static String ATTR_NAVIGATION_PREFERENCES_SHOW_TRAFFIC_OPEN_MAP = "Show_Traffic_Open_Map";
    final private static String ATTR_NAVIGATION_PREFERENCES_SPOKEN_SPEED_WARNINGS = "Spoken_Speed_Warnings";
    final private static String ATTR_NAVIGATION_PREFERENCES_SPOKEN_TBT = "Spoken_Turn_By_Turn";
    final private static String ATTR_NEARBY_SEARCH_DEST_SCROLLED = "Scrolled";
    final private static String ATTR_NEARBY_SEARCH_DEST_SELECTED_POSITION = "Selected_Position";
    final private static String ATTR_NEARBY_SEARCH_NO_RESULTS_ACTION = "Action";
    final private static String ATTR_NEARBY_SEARCH_PLACE_TYPE = "Place_Type";
    final private static String ATTR_OBD_CONN_ECU = "ECU";
    final private static String ATTR_OBD_CONN_FIRMWARE_VERSION = "Obd_Chip_Firmware";
    final private static String ATTR_OBD_CONN_PROTOCOL = "Protocol";
    final private static String ATTR_OBD_CONN_VIN = "VIN";
    final private static String ATTR_OBD_DATA_CAR_MAKE = "Car_Make";
    final private static String ATTR_OBD_DATA_CAR_MODEL = "Car_Model";
    final private static String ATTR_OBD_DATA_CAR_VIN = "Vin";
    final private static String ATTR_OBD_DATA_CAR_YEAR = "Car_Year";
    final private static String ATTR_OBD_DATA_MONITORING_DATA_SIZE = "Data_Size";
    final private static String ATTR_OBD_DATA_MONITORING_SUCCESS = "Success";
    final private static String ATTR_OBD_DATA_VERSION = "Version";
    final private static String ATTR_OFFLINE_MAPS_UPDATED_NEW_DATE = "New_Date";
    final private static String ATTR_OFFLINE_MAPS_UPDATED_NEW_PACKAGES = "New_Packages";
    final private static String ATTR_OFFLINE_MAPS_UPDATED_NEW_VERSION = "New_Version";
    final private static String ATTR_OFFLINE_MAPS_UPDATED_OLD_DATE = "Old_Date";
    final private static String ATTR_OFFLINE_MAPS_UPDATED_OLD_PACKAGES = "Old_Packages";
    final private static String ATTR_OFFLINE_MAPS_UPDATED_OLD_VERSION = "Old_Version";
    final private static String ATTR_OFFLINE_MAPS_UPDATED_UPDATE_COUNT = "Update_Count";
    final private static String ATTR_OPTION_NAME = "Name";
    final private static String ATTR_OTA_CURRENT_VERSION = "Current_Version";
    final private static String ATTR_OTA_INSTALL_INCREMENTAL = "Incremental";
    final private static String ATTR_OTA_INSTALL_PRIOR_VERSION = "Prior_Version";
    final private static String ATTR_OTA_INSTALL_SUCCESS = "Success";
    final private static String ATTR_OTA_UPDATE_VERSION = "Update_Version";
    final private static String ATTR_PHONE_CONNECTION = "Connection";
    final public static String ATTR_PLATFORM_ANDROID = "Android";
    final public static String ATTR_PLATFORM_IOS = "iOS";
    final private static String ATTR_PROFILE_BUILD_TYPE = "Build_Type";
    final private static String ATTR_PROFILE_CAR_MAKE = "Car_Make";
    final private static String ATTR_PROFILE_CAR_MODEL = "Car_Model";
    final private static String ATTR_PROFILE_CAR_YEAR = "Car_Year";
    final private static String ATTR_PROFILE_HUD_VERSION = "Hud_Version_Number";
    final private static String ATTR_PROFILE_SERIAL_NUMBER = "Hud_Serial_Number";
    final private static String ATTR_REGION_COUNTRY = "Country";
    final private static String ATTR_REGION_STATE = "State";
    final private static String ATTR_REMOTE_DEVICE_HARDWARE = "Remote_Device_Hardware";
    final private static String ATTR_REMOTE_DEVICE_PLATFORM = "Remote_Device_Platform";
    final private static String ATTR_REMOTE_DEVICE_SW_VERSION = "Remote_Device_SW_Version";
    final private static String ATTR_ROUTE_COMPLETED_ACTUAL_DISTANCE = "Actual_Distance";
    final private static String ATTR_ROUTE_COMPLETED_ACTUAL_DURATION = "Actual_Duration";
    final private static String ATTR_ROUTE_COMPLETED_DISTANCE_VARIANCE = "Distance_PCT_Diff";
    final private static String ATTR_ROUTE_COMPLETED_DURATION_VARIANCE = "Duration_PCT_Diff";
    final private static String ATTR_ROUTE_COMPLETED_EXPECTED_DISTANCE = "Expected_Distance";
    final private static String ATTR_ROUTE_COMPLETED_EXPECTED_DURATION = "Expected_Duration";
    final private static String ATTR_ROUTE_COMPLETED_RECALCULATIONS = "Recalculations";
    final private static String ATTR_ROUTE_COMPLETED_TRAFFIC_LEVEL = "Traffic_Level";
    final private static String ATTR_ROUTE_DISPLAY_POS = "Display_Pos";
    final private static String ATTR_ROUTE_NAV_POS = "Nav_Pos";
    final private static String ATTR_ROUTE_STREET_ADDRESS = "Street_Addr";
    final private static String ATTR_SESSION_ACCELERATION_COUNT = "Session_Hard_Acceleration_Count";
    final private static String ATTR_SESSION_AVERAGE_MPG = "Session_Average_Mpg";
    final private static String ATTR_SESSION_AVERAGE_ROLLING_SPEED = "Session_Average_Rolling_Speed";
    final private static String ATTR_SESSION_AVERAGE_SPEED = "Session_Average_Speed";
    final private static String ATTR_SESSION_DISTANCE = "Session_Distance";
    final private static String ATTR_SESSION_DURATION = "Session_Duration";
    final private static String ATTR_SESSION_EXCESSIVE_SPEEDING_FREQUENCY = "Session_Excessive_Speeding_Frequency";
    final private static String ATTR_SESSION_HARD_BRAKING_COUNT = "Session_Hard_Braking_Count";
    final private static String ATTR_SESSION_HIGH_G_COUNT = "Session_High_G_Count";
    final private static String ATTR_SESSION_MAX_RPM = "Session_Max_RPM";
    final private static String ATTR_SESSION_MAX_SPEED = "Session_Max_Speed";
    final private static String ATTR_SESSION_ROLLING_DURATION = "Session_Rolling_Duration";
    final private static String ATTR_SESSION_SPEEDING_FREQUENCY = "Session_Speeding_Frequency";
    final private static String ATTR_SESSION_TROUBLE_CODE_COUNT = "Session_Trouble_Code_Count";
    final private static String ATTR_SESSION_TROUBLE_CODE_LAST = "Session_Trouble_Code_Last";
    final private static String ATTR_SHUTDOWN_BATTERY_LEVEL = "Battery_Level";
    final private static String ATTR_SHUTDOWN_REASON = "Reason";
    final private static String ATTR_SHUTDOWN_SLEEP_TIME = "Sleep_Time";
    final private static String ATTR_SHUTDOWN_TYPE = "Type";
    final private static String ATTR_SHUTDOWN_UPTIME = "Uptime";
    final private static String ATTR_SMS_MESSAGE_TYPE = "Message_Type";
    final private static String ATTR_SMS_MESSAGE_TYPE_CANNED = "Canned";
    final private static String ATTR_SMS_MESSAGE_TYPE_CUSTOM = "Custom";
    final private static String ATTR_SMS_PLATFORM = "Platform";
    final private static String ATTR_SMS_SUCCESS = "Success";
    final private static String ATTR_SPEEDING_AMOUNT = "Session_Speeding_Amount";
    final private static String ATTR_TEMP_CPU_AVERAGE = "CPU_Average";
    final private static String ATTR_TEMP_CPU_MAX = "CPU_Max";
    final private static String ATTR_TEMP_CPU_TEMP = "CPU_Temperature";
    final private static String ATTR_TEMP_DMD_AVERAGE = "DMD_Average";
    final private static String ATTR_TEMP_DMD_MAX = "DMD_Max";
    final private static String ATTR_TEMP_DMD_TEMP = "DMD_Temperature";
    final private static String ATTR_TEMP_FAN_SPEED = "Fan_Speed";
    final private static String ATTR_TEMP_INLET_AVERAGE = "Inlet_Average";
    final private static String ATTR_TEMP_INLET_MAX = "Inlet_Max";
    final private static String ATTR_TEMP_INLET_TEMP = "Inlet_Temperature";
    final private static String ATTR_TEMP_LED_AVERAGE = "LED_Average";
    final private static String ATTR_TEMP_LED_MAX = "LED_Max";
    final private static String ATTR_TEMP_LED_TEMP = "LED_Temperature";
    final private static String ATTR_TEMP_WARNING = "Warning";
    final private static String ATTR_UPDATE_ACCEPTED = "Accepted";
    final private static String ATTR_UPDATE_TO_VERSION = "Update_To_Version";
    final private static String ATTR_VEHICLE_KILO_METERS = "Vehicle_Kilo_Meters";
    final private static String ATTR_VIN = "Vin";
    final private static String ATTR_VOICE_SEARCH_ADDITIONAL_RESULTS_GLANCE = "Additional_Results_Glance";
    final private static String ATTR_VOICE_SEARCH_CANCEL_TYPE = "Cancel_Type";
    final private static String ATTR_VOICE_SEARCH_CLIENT_VERSION = "Client_Version";
    final private static String ATTR_VOICE_SEARCH_CONFIDENCE_LEVEL = "Confidence_Level";
    final private static String ATTR_VOICE_SEARCH_DESTINATION_TYPE = "Destination_Type";
    final private static String ATTR_VOICE_SEARCH_EXPLICIT_RETRY = "Explicit_Retry";
    final private static String ATTR_VOICE_SEARCH_LIST_SELECTION = "Additional_Results_List_Item_Selected";
    final private static String ATTR_VOICE_SEARCH_MICROPHONE_USED = "Microphone_Used";
    final private static String ATTR_VOICE_SEARCH_NAVIGATION_INITIATED = "Navigation_Initiated";
    final private static String ATTR_VOICE_SEARCH_NAV_CANCELLED_QUICKLY = "Navigation_Cancelled_Quickly";
    final private static String ATTR_VOICE_SEARCH_NUMBER_OF_WORDS = "Number_Of_Words";
    final private static String ATTR_VOICE_SEARCH_NUM_RESULTS = "Num_Total_Results";
    final private static String ATTR_VOICE_SEARCH_PREFIX = "Prefix";
    final private static String ATTR_VOICE_SEARCH_RESPONSE = "Response";
    final private static String ATTR_VOICE_SEARCH_RETRY_COUNT = "Retry_Count";
    final private static String ATTR_VOICE_SEARCH_SUGGESTION = "Suggestion";
    final private static String ATTR_WAKEUP_BATTERY_DRAIN = "Battery_Drain";
    final private static String ATTR_WAKEUP_BATTERY_LEVEL = "Battery_Level";
    final private static String ATTR_WAKEUP_MAX_BATTERY = "Max_Battery";
    final private static String ATTR_WAKEUP_REASON = "Reason";
    final private static String ATTR_WAKEUP_SLEEP_TIME = "Sleep_Time";
    final private static String ATTR_WAKEUP_VIN = "VIN";
    final private static int CUSTOM_DIMENSION_HW_VERSION = 0;
    final private static String DESTINATION_SUGGESTION_TYPE_CALENDAR = "Calendar";
    final private static String DESTINATION_SUGGESTION_TYPE_SMART_SUGGESTION = "SmartSuggestion";
    final private static String FALSE = "false";
    final private static long FUEL_REPORTING_INTERVAL = 180000L;
    final private static String LIGHT_SENSOR_DEVICE = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_illuminance0_input";
    final private static String LIGHT_SENSOR_RAW1_DEVICE = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw1";
    final private static String LIGHT_SENSOR_RAW_DEVICE = "/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw";
    final private static int MAX_CPU_TEMP = 93;
    final private static long MAX_DMD_HIGH_TIME_MS;
    final private static int MAX_DMD_TEMP = 65;
    final private static long MAX_INLET_HIGH_TIME_MS;
    final private static int MAX_INLET_TEMP = 72;
    final private static long NAVIGATION_CANCEL_TIMEOUT;
    final private static int NOTEMP = -1000;
    final private static String OFFLINE_MAPS_UPDATE_COUNT = "offlineMapsUpdateCount";
    final private static int PREF_CHANGE_DELAY_MS = 15000;
    final private static String PREVIOUS_OFFLINE_MAPS_DATE = "previousOfflineMapsDate";
    final private static String PREVIOUS_OFFLINE_MAPS_PACKAGES = "previousOfflineMapsPackages";
    final private static String PREVIOUS_OFFLINE_MAPS_VERSION = "previousOfflineMapsVersion";
    final private static long TEMP_WARNING_BOOT_DELAY_S;
    final private static long TEMP_WARNING_INTERVAL_MS;
    final private static String TRUE = "true";
    final private static long UPLOAD_INTERVAL;
    final private static long UPLOAD_INTERVAL_BOOT;
    final private static long UPLOAD_INTERVAL_LIMITED_BANDWIDTH;
    final private static boolean VERBOSE = false;
    private static int currentCpuAvgTemperature;
    private static java.util.ArrayList deferredEvents;
    private static com.navdy.hud.app.analytics.AnalyticsSupport$DeferredWakeup deferredWakeup;
    private static boolean destinationSet;
    private static boolean disconnectDueToForceReconnect;
    private static String glanceNavigationSource;
    private static android.os.Handler handler;
    private static int lastDiscReason;
    private static String lastForceReconnectReason;
    private static volatile boolean localyticsInitialized;
    private static int nearbySearchDestScrollPosition;
    private static com.navdy.service.library.events.places.PlaceType nearbySearchPlaceType;
    private static boolean newDevicePaired;
    private static boolean phonePaired;
    private static boolean quietMode;
    private static com.navdy.service.library.events.DeviceInfo remoteDevice;
    private static com.navdy.hud.app.analytics.AnalyticsSupport$FuelMonitor sFuelMonitor;
    private static com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceWatcher sLastNavigationPreferences;
    private static com.navdy.hud.app.profile.DriverProfile sLastProfile;
    private static com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceWatcher sLastProfilePreferences;
    private static String sLastVIN;
    private static com.navdy.service.library.log.Logger sLogger;
    private static com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver sNotificationReceiver;
    private static com.navdy.hud.app.obd.ObdManager sObdManager;
    private static String sRemoteDeviceId;
    private static com.navdy.hud.app.manager.RemoteDeviceManager sRemoteDeviceManager;
    private static Runnable sUploader;
    private static long startTime;
    private static double startingBatteryVoltage;
    private static int startingDistance;
    private static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceResultsRunnable voiceResultsRunnable;
    private static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction voiceSearchAdditionalResultsAction;
    private static Integer voiceSearchConfidence;
    private static int voiceSearchCount;
    private static boolean voiceSearchExplicitRetry;
    private static Integer voiceSearchListSelection;
    private static boolean voiceSearchListeningOverBluetooth;
    private static String voiceSearchPrefix;
    private static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress voiceSearchProgress;
    private static java.util.List voiceSearchResults;
    private static String voiceSearchWords;
    private static long welcomeScreenTime;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.analytics.AnalyticsSupport.class);
        sRemoteDeviceManager = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance();
        remoteDevice = null;
        sLastProfile = null;
        lastDiscReason = 0;
        lastForceReconnectReason = "";
        disconnectDueToForceReconnect = false;
        sLastProfilePreferences = null;
        sLastNavigationPreferences = null;
        sLastVIN = null;
        sRemoteDeviceId = null;
        handler = new android.os.Handler(android.os.Looper.getMainLooper());
        localyticsInitialized = false;
        quietMode = false;
        startingBatteryVoltage = -1.0;
        UPLOAD_INTERVAL_BOOT = java.util.concurrent.TimeUnit.MINUTES.toMillis(5L);
        UPLOAD_INTERVAL = java.util.concurrent.TimeUnit.MINUTES.toMillis(5L);
        UPLOAD_INTERVAL_LIMITED_BANDWIDTH = java.util.concurrent.TimeUnit.MINUTES.toMillis(20L);
        sUploader = (Runnable)new com.navdy.hud.app.analytics.AnalyticsSupport$1();
        glanceNavigationSource = null;
        TEMP_WARNING_BOOT_DELAY_S = java.util.concurrent.TimeUnit.MINUTES.toSeconds(3L);
        TEMP_WARNING_INTERVAL_MS = java.util.concurrent.TimeUnit.MINUTES.toMillis(10L);
        MAX_INLET_HIGH_TIME_MS = java.util.concurrent.TimeUnit.MINUTES.toMillis(2L);
        MAX_DMD_HIGH_TIME_MS = java.util.concurrent.TimeUnit.MINUTES.toMillis(10L);
        currentCpuAvgTemperature = -1;
        nearbySearchDestScrollPosition = -1;
        nearbySearchPlaceType = null;
        welcomeScreenTime = -1L;
        newDevicePaired = false;
        phonePaired = false;
        NAVIGATION_CANCEL_TIMEOUT = java.util.concurrent.TimeUnit.MINUTES.toMillis(1L);
        voiceSearchProgress = null;
        voiceSearchWords = null;
        voiceSearchConfidence = null;
        voiceSearchCount = 0;
        voiceSearchListeningOverBluetooth = false;
        voiceSearchResults = null;
        voiceSearchAdditionalResultsAction = null;
        voiceSearchListSelection = null;
        voiceSearchExplicitRetry = false;
        voiceSearchPrefix = null;
        voiceResultsRunnable = null;
        destinationSet = false;
        deferredWakeup = null;
        deferredEvents = null;
    }
    
    public AnalyticsSupport() {
    }
    
    static com.navdy.service.library.log.Logger access$000() {
        return sLogger;
    }
    
    static long access$100() {
        return com.navdy.hud.app.analytics.AnalyticsSupport.getUpdateInterval();
    }
    
    static com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceWatcher access$1002(com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceWatcher a) {
        sLastProfilePreferences = a;
        return a;
    }
    
    static com.navdy.hud.app.analytics.Event access$1100(com.navdy.hud.app.profile.DriverProfile a) {
        return com.navdy.hud.app.analytics.AnalyticsSupport.recordPreference(a);
    }
    
    static com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceWatcher access$1202(com.navdy.hud.app.analytics.AnalyticsSupport$PreferenceWatcher a) {
        sLastNavigationPreferences = a;
        return a;
    }
    
    static com.navdy.hud.app.analytics.Event access$1300(com.navdy.hud.app.profile.DriverProfile a) {
        return com.navdy.hud.app.analytics.AnalyticsSupport.recordNavigationPreference(a);
    }
    
    static String access$1400() {
        return sRemoteDeviceId;
    }
    
    static void access$1500() {
        com.navdy.hud.app.analytics.AnalyticsSupport.clearVoiceSearch();
    }
    
    static void access$1600(com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason a) {
        com.navdy.hud.app.analytics.AnalyticsSupport.recordWakeup(a);
    }
    
    static boolean access$1702(boolean b) {
        quietMode = b;
        return b;
    }
    
    static Runnable access$1800() {
        return sUploader;
    }
    
    static void access$1900() {
        com.navdy.hud.app.analytics.AnalyticsSupport.reportObdState();
    }
    
    static android.os.Handler access$200() {
        return handler;
    }
    
    static void access$2000(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        com.navdy.hud.app.analytics.AnalyticsSupport.processNavigationRouteRequest(a);
    }
    
    static void access$2100(com.navdy.hud.app.maps.MapEvents$ArrivalEvent a) {
        com.navdy.hud.app.analytics.AnalyticsSupport.processNavigationArrivalEvent(a);
    }
    
    static void access$2200(com.navdy.hud.app.analytics.Event a, boolean b) {
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(a, b);
    }
    
    static long access$2300() {
        return MAX_INLET_HIGH_TIME_MS;
    }
    
    static long access$2400() {
        return MAX_DMD_HIGH_TIME_MS;
    }
    
    static int access$2500(String s) {
        return com.navdy.hud.app.analytics.AnalyticsSupport.parseTemp(s);
    }
    
    static int access$2602(int i) {
        currentCpuAvgTemperature = i;
        return i;
    }
    
    static long access$2700() {
        return TEMP_WARNING_INTERVAL_MS;
    }
    
    static long access$2800() {
        return com.navdy.hud.app.analytics.AnalyticsSupport.uptime();
    }
    
    static long access$2900() {
        return TEMP_WARNING_BOOT_DELAY_S;
    }
    
    static String access$300() {
        return glanceNavigationSource;
    }
    
    static String access$302(String s) {
        glanceNavigationSource = s;
        return s;
    }
    
    static int access$3100() {
        return lastDiscReason;
    }
    
    static int access$3102(int i) {
        lastDiscReason = i;
        return i;
    }
    
    static boolean access$3202(boolean b) {
        disconnectDueToForceReconnect = b;
        return b;
    }
    
    static String access$3300() {
        return lastForceReconnectReason;
    }
    
    static String access$3302(String s) {
        lastForceReconnectReason = s;
        return s;
    }
    
    static com.navdy.hud.app.obd.ObdManager access$3400() {
        return sObdManager;
    }
    
    static String access$3500() {
        return voiceSearchWords;
    }
    
    static Integer access$3600() {
        return voiceSearchConfidence;
    }
    
    static int access$3700() {
        return voiceSearchCount;
    }
    
    static boolean access$3800() {
        return voiceSearchListeningOverBluetooth;
    }
    
    static java.util.List access$3900() {
        return voiceSearchResults;
    }
    
    static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction access$4000() {
        return voiceSearchAdditionalResultsAction;
    }
    
    static com.navdy.service.library.events.DeviceInfo access$402(com.navdy.service.library.events.DeviceInfo a) {
        remoteDevice = a;
        return a;
    }
    
    static boolean access$4100() {
        return voiceSearchExplicitRetry;
    }
    
    static Integer access$4200() {
        return voiceSearchListSelection;
    }
    
    static String access$4300() {
        return voiceSearchPrefix;
    }
    
    static void access$4400(com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason a, com.navdy.service.library.events.navigation.NavigationRouteRequest a0, String s, Integer a1, int i, String s0, java.util.List a2, com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction a3, boolean b, Integer a4, String s1) {
        com.navdy.hud.app.analytics.AnalyticsSupport.sendVoiceSearchEvent(a, a0, s, a1, i, s0, a2, a3, b, a4, s1);
    }
    
    static com.navdy.hud.app.analytics.AnalyticsSupport$VoiceResultsRunnable access$4502(com.navdy.hud.app.analytics.AnalyticsSupport$VoiceResultsRunnable a) {
        voiceResultsRunnable = a;
        return a;
    }
    
    static void access$4600() {
        com.navdy.hud.app.analytics.AnalyticsSupport.recordNewDevicePaired();
    }
    
    static void access$4700(String s, boolean b, String[] a) {
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(s, b, a);
    }
    
    static void access$500() {
        com.navdy.hud.app.analytics.AnalyticsSupport.setupProfile();
    }
    
    static void access$600(android.content.SharedPreferences a) {
        com.navdy.hud.app.analytics.AnalyticsSupport.recordOfflineMapsChange(a);
    }
    
    static void access$700() {
        com.navdy.hud.app.analytics.AnalyticsSupport.maybeOpenSession();
    }
    
    static void access$800() {
        com.navdy.hud.app.analytics.AnalyticsSupport.closeSession();
    }
    
    static com.navdy.hud.app.profile.DriverProfile access$902(com.navdy.hud.app.profile.DriverProfile a) {
        sLastProfile = a;
        return a;
    }
    
    public static void analyticsApplicationInit(android.app.Application a) {
        a.registerActivityLifecycleCallbacks((android.app.Application$ActivityLifecycleCallbacks)new com.localytics.android.LocalyticsActivityLifecycleCallbacks((android.content.Context)a));
        com.localytics.android.Localytics.setPushDisabled(true);
        com.navdy.hud.app.analytics.AnalyticsSupport.setupCustomDimensions();
        sNotificationReceiver = new com.navdy.hud.app.analytics.AnalyticsSupport$NotificationReceiver();
        startTime = java.util.concurrent.TimeUnit.NANOSECONDS.toSeconds(System.nanoTime());
        com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter a0 = new com.navdy.hud.app.analytics.AnalyticsSupport$TemperatureReporter();
        a0.setName("TemperatureReporter");
        a0.start();
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), a0);
        sObdManager = com.navdy.hud.app.obd.ObdManager.getInstance();
        sFuelMonitor = new com.navdy.hud.app.analytics.AnalyticsSupport$FuelMonitor((com.navdy.hud.app.analytics.AnalyticsSupport$1)null);
        quietMode = sNotificationReceiver.powerManager.inQuietMode();
        com.navdy.hud.app.analytics.AnalyticsSupport.maybeOpenSession();
        android.content.IntentFilter a1 = new android.content.IntentFilter("android.bluetooth.device.action.ACL_DISCONNECTED");
        a1.addAction("com.navdy.hud.app.force_reconnect");
        com.navdy.hud.app.HudApplication.getAppContext().registerReceiver((android.content.BroadcastReceiver)new com.navdy.hud.app.analytics.AnalyticsSupport$2(), a1);
    }
    
    private static void clearVoiceSearch() {
        voiceSearchProgress = null;
        voiceSearchWords = null;
        voiceSearchConfidence = null;
        voiceSearchListeningOverBluetooth = false;
        voiceSearchResults = null;
        voiceSearchAdditionalResultsAction = null;
        voiceSearchExplicitRetry = false;
        voiceSearchListSelection = null;
        voiceSearchPrefix = null;
    }
    
    private static void closeSession() {
        boolean b = localyticsInitialized;
        synchronized(com.navdy.hud.app.analytics.AnalyticsSupport.class) {
            if (!b) {
                break label1;
            }
            label0: {
                try {
                    com.navdy.hud.app.analytics.AnalyticsSupport.sendDistance();
                    sLogger.v("closeSession()");
                    com.localytics.android.Localytics.closeSession();
                    sRemoteDeviceId = "unknown";
                } catch(Throwable a0) {
                    a = a0;
                    break label0;
                }
                break label1;
            }
            /*monexit(com.navdy.hud.app.analytics.AnalyticsSupport.class)*/;
            throw a;
        }
        /*monexit(com.navdy.hud.app.analytics.AnalyticsSupport.class)*/;
    }
    
    private static int countWords(String s) {
        int i = 0;
        label1: if (s == null) {
            i = 0;
        } else {
            String s0 = s.trim();
            int i0 = s0.length();
            i = 0;
            int i1 = 0;
            while(true) {
                int i2 = i1;
                if (i1 >= i0) {
                    break label1;
                }
                i = i + 1;
                label0: while(true) {
                    i1 = i2;
                    if (i2 < i0) {
                        int i3 = s0.charAt(i2);
                        i1 = i2;
                        if (i3 != 32) {
                            i2 = i2 + 1;
                            continue label0;
                        }
                    }
                    while(i1 < i0) {
                        int i4 = s0.charAt(i1);
                        if (i4 != 32) {
                            break;
                        }
                        i1 = i1 + 1;
                    }
                    break;
                }
            }
        }
        return i;
    }
    
    private static boolean deferLocalyticsEvent(String s, String[] a) {
        boolean b = false;
        boolean b0 = localyticsInitialized;
        synchronized(com.navdy.hud.app.analytics.AnalyticsSupport.class) {
            if (b0) {
                b = false;
                break label0;
            } else {
                try {
                    if (deferredEvents == null) {
                        deferredEvents = new java.util.ArrayList(5);
                    }
                    deferredEvents.add(new com.navdy.hud.app.analytics.AnalyticsSupport$DeferredLocalyticsEvent(s, a));
                    b = true;
                    break label0;
                } catch(Throwable a1) {
                    a0 = a1;
                }
            }
            /*monexit(com.navdy.hud.app.analytics.AnalyticsSupport.class)*/;
            throw a0;
        }
        /*monexit(com.navdy.hud.app.analytics.AnalyticsSupport.class)*/;
        return b;
    }
    
    private static String getClientVersion() {
        com.navdy.service.library.events.DeviceInfo a = sRemoteDeviceManager.getRemoteDeviceInfo();
        String s = (a != null) ? a.clientVersion : null;
        if (s == null) {
            sLogger.e("failed to get client version");
            s = "";
        }
        return s;
    }
    
    public static int getCurrentTemperature() {
        return currentCpuAvgTemperature;
    }
    
    private static String getHwVersion() {
        return new StringBuilder().append(com.navdy.hud.app.util.SerialNumber.instance.configurationCode).append(com.navdy.hud.app.util.SerialNumber.instance.revisionCode).toString();
    }
    
    private static long getUpdateInterval() {
        return (com.navdy.hud.app.framework.network.NetworkBandwidthController.getInstance().isLimitBandwidthModeOn()) ? UPLOAD_INTERVAL_LIMITED_BANDWIDTH : UPLOAD_INTERVAL;
    }
    
    private static String getVoiceSearchEvent() {
        String s = (remoteDevice != null) ? remoteDevice.platform.toString() : null;
        String s0 = null;
        if (s != null) {
            s0 = (s.equals("PLATFORM_Android")) ? "Voice_Search_Android" : "Voice_Search_iOS";
        }
        return s0;
    }
    
    private static String isEnabled(boolean b) {
        return b ? "Enabled" : "Disabled";
    }
    
    private static boolean isTimeValid() {
        return java.util.Calendar.getInstance().get(1) >= 2016;
    }
    
    private static void localyticsSendEvent(com.navdy.hud.app.analytics.Event a, boolean b) {
        if (b) {
            com.localytics.android.Localytics.tagEvent(a.tag, a.argMap);
        } else {
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.analytics.AnalyticsSupport$EventSender(a), 1);
        }
    }
    
    private static void localyticsSendEvent(String s, boolean b, boolean b0, String[] a) {
        label0: {
            label1: {
                if (b0) {
                    break label1;
                }
                if (com.navdy.hud.app.analytics.AnalyticsSupport.deferLocalyticsEvent(s, a)) {
                    break label0;
                }
            }
            com.navdy.hud.app.analytics.Event a0 = new com.navdy.hud.app.analytics.Event(s, a);
            java.util.Map a1 = a0.argMap;
            if (b) {
                com.navdy.service.library.events.DeviceInfo a2 = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getRemoteDeviceInfo();
                if (a2 != null) {
                    if (a2.platform != null) {
                        a1.put("Remote_Device_Platform", a2.platform.name());
                    }
                    a1.put("Remote_Device_Hardware", new StringBuilder().append(a2.deviceMake).append(" ").append(a2.model).toString());
                    a1.put("Remote_Device_SW_Version", new StringBuilder().append(a2.deviceMake).append(" ").append(a2.systemVersion).toString());
                }
            }
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(a0, b0);
        }
    }
    
    private static void localyticsSendEvent(String s, boolean b, String[] a) {
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(s, b, false, a);
    }
    
    public static void localyticsSendEvent(String s, String[] a) {
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(s, false, false, a);
    }
    
    private static void maybeOpenSession() {
        boolean b = com.navdy.hud.app.analytics.AnalyticsSupport.isTimeValid();
        if (!quietMode & b) {
            com.navdy.hud.app.analytics.AnalyticsSupport.openSession();
        }
    }
    
    private static String normalizeName(String s) {
        char[] a = s.toCharArray();
        boolean b = true;
        int i = 0;
        while(i < a.length) {
            int i0 = a[i];
            if (b) {
                i0 = Character.toUpperCase((char)i0);
            }
            b = i0 == 95;
            int i1 = (char)i0;
            a[i] = (char)i1;
            i = i + 1;
        }
        return new String(a);
    }
    
    private static void openSession() {
        boolean b = localyticsInitialized;
        synchronized(com.navdy.hud.app.analytics.AnalyticsSupport.class) {
            if (b) {
                break label0;
            }
            try {
                sLogger.i("openSession()");
                com.localytics.android.Localytics.openSession();
                localyticsInitialized = true;
                com.navdy.hud.app.analytics.AnalyticsSupport.sendDeferredLocalyticsEvents();
                handler.postDelayed((Runnable)sFuelMonitor, 180000L);
                handler.postDelayed(sUploader, UPLOAD_INTERVAL_BOOT);
                break label0;
            } catch(Throwable a0) {
                a = a0;
            }
            /*monexit(com.navdy.hud.app.analytics.AnalyticsSupport.class)*/;
            throw a;
        }
        /*monexit(com.navdy.hud.app.analytics.AnalyticsSupport.class)*/;
    }
    
    private static int parseTemp(String s) {
        int i = 0;
        try {
            i = Integer.parseInt(s);
        } catch(NumberFormatException ignoredException) {
            sLogger.e(new StringBuilder().append("invalid temperature: ").append(s).toString());
            i = -1000;
        }
        return i;
    }
    
    private static String pidListToHexString(java.util.List a) {
        long[] a0 = new long[5];
        Object a1 = a.iterator();
        while(((java.util.Iterator)a1).hasNext()) {
            int i = ((com.navdy.obd.Pid)((java.util.Iterator)a1).next()).getId();
            int i0 = i / 64;
            a0[i0] = a0[i0] | 1L << i % 64;
        }
        int i1 = a0.length - 1;
        String s = "";
        String s0 = "";
        while(i1 >= 0) {
            StringBuilder a2 = new StringBuilder().append(s0).append(s);
            java.util.Locale a3 = java.util.Locale.US;
            Object[] a4 = new Object[1];
            a4[0] = Long.valueOf(a0[i1]);
            s0 = a2.append(String.format(a3, "%08x", a4)).toString();
            i1 = i1 + -1;
            s = " ";
        }
        return s0;
    }
    
    private static void processNavigationArrivalEvent(com.navdy.hud.app.maps.MapEvents$ArrivalEvent a) {
        if (nearbySearchPlaceType != null) {
            String[] a0 = new String[2];
            a0[0] = "Place_Type";
            a0[1] = nearbySearchPlaceType.toString();
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Nearby_Search_Arrived", a0);
            nearbySearchPlaceType = null;
        }
    }
    
    private static void processNavigationRouteRequest(com.navdy.service.library.events.navigation.NavigationRouteRequest a) {
        if (nearbySearchPlaceType == null) {
            boolean b = false;
            com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress a0 = voiceSearchProgress;
            com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress a1 = com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress.PROGRESS_ROUTE_SELECTED;
            label2: {
                label0: {
                    label1: {
                        if (a0 == a1) {
                            break label1;
                        }
                        if (voiceSearchProgress != com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress.PROGRESS_DESTINATION_FOUND) {
                            break label0;
                        }
                    }
                    b = true;
                    break label2;
                }
                b = false;
            }
            if (voiceResultsRunnable != null) {
                voiceResultsRunnable.cancel(b ? com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason.new_voice_search : com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason.new_non_voice_search);
            }
            if (b) {
                voiceResultsRunnable = new com.navdy.hud.app.analytics.AnalyticsSupport$VoiceResultsRunnable(a);
                handler.postDelayed((Runnable)voiceResultsRunnable, NAVIGATION_CANCEL_TIMEOUT);
                com.navdy.hud.app.analytics.AnalyticsSupport.clearVoiceSearch();
            } else if (voiceSearchProgress != null) {
                sLogger.w(new StringBuilder().append("Received NavigationRouteRequest with unexpected voice progress state: ").append(voiceSearchProgress).toString());
            }
            voiceSearchCount = 0;
        } else {
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Nearby_Search_Trip_Initiated", new String[0]);
        }
    }
    
    private static String readLightSensor(String s) {
        String s0 = null;
        try {
            s0 = com.navdy.service.library.util.IOUtils.convertFileToString(s);
        } catch(Exception a) {
            sLogger.e(new StringBuilder().append("exception reading light sensor device: ").append(s).toString(), (Throwable)a);
            s0 = "";
        }
        return s0.trim();
    }
    
    public static void recordAdaptiveAutobrightnessAdjustmentChanged(int i) {
        String s = com.navdy.hud.app.analytics.AnalyticsSupport.readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_illuminance0_input");
        String s0 = com.navdy.hud.app.analytics.AnalyticsSupport.readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw");
        String s1 = com.navdy.hud.app.analytics.AnalyticsSupport.readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw1");
        String[] a = new String[8];
        a[0] = "Brightness";
        a[1] = Integer.toString(i);
        a[2] = "Light_Sensor";
        a[3] = s;
        a[4] = "Raw_Sensor";
        a[5] = s0;
        a[6] = "Raw1_Sensor";
        a[7] = s1;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Adaptive_Brightness_Adjustment_Change", a);
    }
    
    public static void recordAutobrightnessAdjustmentChanged(int i, int i0, int i1) {
        if (i0 != i1) {
            String s = com.navdy.hud.app.analytics.AnalyticsSupport.readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_illuminance0_input");
            String s0 = com.navdy.hud.app.analytics.AnalyticsSupport.readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw");
            String s1 = com.navdy.hud.app.analytics.AnalyticsSupport.readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw1");
            String[] a = new String[12];
            a[0] = "Brightness";
            a[1] = Integer.toString(i);
            a[2] = "Previous";
            a[3] = Integer.toString(i0);
            a[4] = "New";
            a[5] = Integer.toString(i1);
            a[6] = "Light_Sensor";
            a[7] = s;
            a[8] = "Raw_Sensor";
            a[9] = s0;
            a[10] = "Raw1_Sensor";
            a[11] = s1;
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Brightness_Adjustment_Change", a);
        }
    }
    
    public static void recordBadRoutePosition(String s, String s0, String s1) {
        String[] a = new String[6];
        a[0] = "Display_Pos";
        a[1] = s;
        a[2] = "Nav_Pos";
        a[3] = s0;
        a[4] = "Street_Addr";
        a[5] = s1;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Route_Bad_Pos", a);
    }
    
    public static void recordBrightnessChanged(int i, int i0) {
        if (i != i0) {
            String s = com.navdy.hud.app.analytics.AnalyticsSupport.readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_illuminance0_input");
            String s0 = com.navdy.hud.app.analytics.AnalyticsSupport.readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw");
            String s1 = com.navdy.hud.app.analytics.AnalyticsSupport.readLightSensor("/sys/bus/i2c/drivers/tsl2x7x/2-0039/iio:device0/in_intensity0_raw1");
            String[] a = new String[10];
            a[0] = "Previous";
            a[1] = Integer.toString(i);
            a[2] = "New";
            a[3] = Integer.toString(i0);
            a[4] = "Light_Sensor";
            a[5] = s;
            a[6] = "Raw_Sensor";
            a[7] = s0;
            a[8] = "Raw1_Sensor";
            a[9] = s1;
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Brightness_Change", a);
        }
    }
    
    public static void recordCpuOverheat(String s) {
        String s0 = Long.toString(com.navdy.hud.app.analytics.AnalyticsSupport.uptime());
        sLogger.i(new StringBuilder().append("Recording CPU overheat event uptime:").append(s0).append(" state:").append(s).toString());
        String[] a = new String[4];
        a[0] = "Uptime";
        a[1] = s0;
        a[2] = "State";
        a[3] = s;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Cpu_Overheat", a);
    }
    
    public static void recordDashPreference(String s, boolean b) {
        String s0 = b ? "Left" : "Right";
        String[] a = new String[4];
        a[0] = "Side";
        a[1] = s0;
        a[2] = "Gauge_name";
        a[3] = s;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Dash_Gauge_Setting", a);
    }
    
    public static void recordDestinationSuggestion(boolean b, boolean b0) {
        String s = b ? "true" : "false";
        String s0 = b0 ? "Calendar" : "SmartSuggestion";
        String[] a = new String[4];
        a[0] = "Accepted";
        a[1] = s;
        a[2] = "Type";
        a[3] = s0;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Destination_Suggestion_Show", a);
    }
    
    public static void recordDownloadOTAUpdate(android.content.SharedPreferences a) {
        String[] a0 = new String[4];
        a0[0] = "Current_Version";
        a0[1] = android.os.Build.VERSION.INCREMENTAL;
        a0[2] = "Update_Version";
        a0[3] = com.navdy.hud.app.util.OTAUpdateService.getIncrementalUpdateVersion(a);
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("OTA_Download", a0);
    }
    
    public static void recordGlanceAction(String s, com.navdy.hud.app.framework.notifications.INotification a, String s0) {
        if (s0 == null) {
            s0 = (glanceNavigationSource == null) ? "unknown" : glanceNavigationSource;
        }
        glanceNavigationSource = null;
        if (a == null) {
            String[] a0 = new String[2];
            a0[0] = "Nav_Method";
            a0[1] = s0;
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(s, a0);
        } else {
            com.navdy.hud.app.framework.notifications.NotificationType a1 = a.getType();
            String s1 = (a instanceof com.navdy.hud.app.framework.glance.GlanceNotification) ? ((com.navdy.hud.app.framework.glance.GlanceNotification)a).getGlanceApp().toString() : a1.toString();
            String[] a2 = new String[4];
            a2[0] = "Glance_Name";
            a2[1] = s1;
            a2[2] = "Nav_Method";
            a2[3] = s0;
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(s, a2);
        }
    }
    
    public static void recordGlympseSent(boolean b, String s, String s0, String s1) {
        sLogger.v(new StringBuilder().append("glympse sent id=").append(s1).append(" sucess=").append(b).append(" share=").append(s).append(" msg=").append(s0).toString());
        String[] a = new String[8];
        a[0] = "Success";
        a[1] = String.valueOf(b);
        a[2] = "Share";
        a[3] = s;
        a[4] = "Message";
        a[5] = s0;
        a[6] = "Id";
        a[7] = s1;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Glympse_Sent", a);
    }
    
    public static void recordGlympseViewed(String s) {
        sLogger.v(new StringBuilder().append("glympse viewed id=").append(s).toString());
        String[] a = new String[2];
        a[0] = "Id";
        a[1] = s;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Glympse_Viewed", a);
    }
    
    public static void recordIncorrectFuelData() {
        com.navdy.hud.app.profile.DriverProfile a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        String s = null;
        String s0 = null;
        String s1 = null;
        if (a != null) {
            s = a.getCarMake();
            s0 = a.getCarModel();
            s1 = a.getCarYear();
        }
        String[] a0 = new String[6];
        a0[0] = "Car_Make";
        a0[1] = s;
        a0[2] = "Car_Model";
        a0[3] = s0;
        a0[4] = "Car_Year";
        a0[5] = s1;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("OBD_Zero_Fuel_Level", a0);
    }
    
    public static void recordInstallOTAUpdate(android.content.SharedPreferences a) {
        String[] a0 = new String[4];
        a0[0] = "Current_Version";
        a0[1] = android.os.Build.VERSION.INCREMENTAL;
        a0[2] = "Update_Version";
        a0[3] = com.navdy.hud.app.util.OTAUpdateService.getIncrementalUpdateVersion(a);
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("OTA_Install", a0);
    }
    
    public static void recordKeyFailure(String s, String s0) {
        String[] a = new String[4];
        a[0] = "Key_Type";
        a[1] = s;
        a[2] = "Error";
        a[3] = s0;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Key_Failed", a);
    }
    
    public static void recordMenuSelection(String s) {
        String[] a = new String[2];
        a[0] = "Name";
        a[1] = s;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Menu_Selection", a);
    }
    
    public static void recordMusicAction(String s) {
        String[] a = new String[2];
        a[0] = "Type";
        a[1] = s;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Music_Action", a);
    }
    
    public static void recordMusicBrowsePlayAction(String s) {
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(new StringBuilder().append(s).append("_played").toString(), true, new String[0]);
    }
    
    public static void recordNavdyMileageEvent(double d, double d0, double d1) {
        String[] a = new String[6];
        a[0] = "Vehicle_Kilo_Meters";
        a[1] = Double.toString(d);
        a[2] = "Navdy_Kilometers";
        a[3] = Double.toString(d0);
        a[4] = "Navdy_Usage_Rate";
        a[5] = Double.toString(d1);
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Navdy_Mileage", a);
    }
    
    public static void recordNavigation(boolean b) {
        if (b) {
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Navigation_Destination_Set", new String[0]);
            destinationSet = true;
        }
    }
    
    public static void recordNavigationCancelled() {
        if (nearbySearchPlaceType == null) {
            if (voiceResultsRunnable != null) {
                sLogger.v("navigation ended, cancelling voice search callback");
                voiceResultsRunnable.cancel(com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason.end_trip);
            }
        } else {
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Nearby_Search_Ended", new String[0]);
            sLogger.v("navigation ended for nearby search trip");
            nearbySearchPlaceType = null;
        }
    }
    
    private static com.navdy.hud.app.analytics.Event recordNavigationPreference(com.navdy.hud.app.profile.DriverProfile a) {
        String s = null;
        com.navdy.service.library.events.preferences.NavigationPreferences a0 = a.getNavigationPreferences();
        String s0 = (a0.routingType != com.navdy.service.library.events.preferences.NavigationPreferences$RoutingType.ROUTING_FASTEST) ? "Shortest" : "Fastest";
        switch(com.navdy.hud.app.analytics.AnalyticsSupport$3.$SwitchMap$com$navdy$service$library$events$preferences$NavigationPreferences$RerouteForTraffic[a0.rerouteForTraffic.ordinal()]) {
            case 3: {
                s = "Never";
                break;
            }
            case 2: {
                s = "Confirm";
                break;
            }
            case 1: {
                s = "Automatic";
                break;
            }
            default: {
                s = "Unknown";
            }
        }
        String[] a1 = new String[26];
        a1[0] = "Reroute_For_Traffic";
        a1[1] = s;
        a1[2] = "Routing_Type";
        a1[3] = s0;
        a1[4] = "Spoken_Turn_By_Turn";
        a1[5] = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a0.spokenTurnByTurn.booleanValue());
        a1[6] = "Spoken_Speed_Warnings";
        a1[7] = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a0.spokenSpeedLimitWarnings.booleanValue());
        a1[8] = "Show_Traffic_Open_Map";
        a1[9] = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a0.showTrafficInOpenMap.booleanValue());
        a1[10] = "Show_Traffic_Navigating";
        a1[11] = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a0.showTrafficWhileNavigating.booleanValue());
        a1[12] = "Allow_Highways";
        a1[13] = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a0.allowHighways.booleanValue());
        a1[14] = "Allow_Toll_Roads";
        a1[15] = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a0.allowTollRoads.booleanValue());
        a1[16] = "Allow_Ferries";
        a1[17] = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a0.allowFerries.booleanValue());
        a1[18] = "Allow_Tunnels";
        a1[19] = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a0.allowTunnels.booleanValue());
        a1[20] = "Allow_Unpaved_Roads";
        a1[21] = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a0.allowUnpavedRoads.booleanValue());
        a1[22] = "Allow_Auto_Trains";
        a1[23] = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a0.allowAutoTrains.booleanValue());
        a1[24] = "Allow_HOV_Lanes";
        a1[25] = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a0.allowHOVLanes.booleanValue());
        return new com.navdy.hud.app.analytics.Event("Navigation_Preferences", a1);
    }
    
    public static void recordNavigationPreferenceChange(com.navdy.hud.app.profile.DriverProfile a) {
        if (sLastNavigationPreferences != null) {
            sLastNavigationPreferences.enqueue(a);
        }
    }
    
    public static void recordNearbySearchClosed() {
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Nearby_Search_Closed", new String[0]);
    }
    
    public static void recordNearbySearchDestinationScroll(int i) {
        if (i > nearbySearchDestScrollPosition) {
            nearbySearchDestScrollPosition = i;
        }
    }
    
    public static void recordNearbySearchDismiss() {
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Nearby_Search_Dismiss", new String[0]);
    }
    
    public static void recordNearbySearchNoResult(boolean b) {
        String s = b ? "Retry" : "Dismiss";
        String[] a = new String[2];
        a[0] = "Action";
        a[1] = s;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Nearby_Search_No_Results", a);
    }
    
    public static void recordNearbySearchResultsClose() {
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Nearby_Search_Results_Close", new String[0]);
    }
    
    public static void recordNearbySearchResultsDismiss() {
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Nearby_Search_Results_Dismiss", new String[0]);
    }
    
    public static void recordNearbySearchResultsView() {
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Nearby_Search_Results_View", new String[0]);
        nearbySearchDestScrollPosition = -1;
        nearbySearchPlaceType = null;
    }
    
    public static void recordNearbySearchReturnMainMenu() {
        com.navdy.hud.app.analytics.AnalyticsSupport.recordMenuSelection("back");
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Nearby_Search_Return_Main_Menu", new String[0]);
    }
    
    public static void recordNearbySearchSelection(com.navdy.service.library.events.places.PlaceType a, int i) {
        String[] a0 = new String[2];
        a0[0] = "Name";
        a0[1] = "nearby_places_results";
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Menu_Selection", a0);
        String s = a.toString();
        String s0 = Integer.toString(i);
        String s1 = (nearbySearchDestScrollPosition <= 0) ? "false" : "true";
        String[] a1 = new String[6];
        a1[0] = "Place_Type";
        a1[1] = s;
        a1[2] = "Selected_Position";
        a1[3] = s0;
        a1[4] = "Scrolled";
        a1[5] = s1;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Nearby_Search_Selection", a1);
        nearbySearchPlaceType = a;
    }
    
    private static void recordNewDevicePaired() {
        newDevicePaired = true;
    }
    
    public static void recordOTAInstallResult(boolean b, String s, boolean b0) {
        String s0 = b ? "true" : "false";
        String s1 = b0 ? "true" : "false";
        String[] a = new String[6];
        a[0] = "Incremental";
        a[1] = s0;
        a[2] = "Prior_Version";
        a[3] = s;
        a[4] = "Success";
        a[5] = s1;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("OTA_Install_Result", a);
    }
    
    public static void recordObdCanBusDataSent(String s) {
        if (sLastProfile == null) {
            String[] a = new String[2];
            a[0] = "Version";
            a[1] = s;
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("OBD_Listening_Posted", a);
        } else {
            String[] a0 = new String[8];
            a0[0] = "Car_Make";
            a0[1] = sLastProfile.getCarMake();
            a0[2] = "Car_Model";
            a0[3] = sLastProfile.getCarModel();
            a0[4] = "Car_Year";
            a0[5] = sLastProfile.getCarYear();
            a0[6] = "Version";
            a0[7] = s;
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("OBD_Listening_Posted", a0);
        }
    }
    
    public static void recordObdCanBusMonitoringState(String s, boolean b, int i, int i0) {
        if (sLastProfile == null) {
            String s0 = b ? "true" : "false";
            String[] a = new String[8];
            a[0] = "Vin";
            a[1] = s;
            a[2] = "Success";
            a[3] = s0;
            a[4] = "Version";
            a[5] = Integer.toString(i0);
            a[6] = "Data_Size";
            a[7] = Integer.toString(i);
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("OBD_Listening_Status", a);
        } else {
            String s1 = sLastProfile.getCarMake();
            String s2 = sLastProfile.getCarModel();
            String s3 = sLastProfile.getCarYear();
            String s4 = b ? "true" : "false";
            String[] a0 = new String[14];
            a0[0] = "Vin";
            a0[1] = s;
            a0[2] = "Car_Make";
            a0[3] = s1;
            a0[4] = "Car_Model";
            a0[5] = s2;
            a0[6] = "Car_Year";
            a0[7] = s3;
            a0[8] = "Success";
            a0[9] = s4;
            a0[10] = "Version";
            a0[11] = Integer.toString(i0);
            a0[12] = "Data_Size";
            a0[13] = Integer.toString(i);
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("OBD_Listening_Status", a0);
        }
    }
    
    private static void recordOfflineMapsChange(android.content.SharedPreferences a) {
        String s = a.getString("previousOfflineMapsVersion", (String)null);
        String s0 = a.getString("previousOfflineMapsPackages", (String)null);
        String s1 = a.getString("previousOfflineMapsDate", (String)null);
        com.navdy.hud.app.maps.here.HereMapsManager a0 = com.navdy.hud.app.maps.here.HereMapsManager.getInstance();
        label0: if (a0.isInitialized()) {
            com.navdy.hud.app.maps.here.HereOfflineMapsVersion a1 = a0.getOfflineMapsVersion();
            if (a1 != null) {
                java.util.List a2 = a1.getPackages();
                String s2 = (a2 == null) ? "" : android.text.TextUtils.join((CharSequence)",", (Iterable)a2);
                String s3 = a1.getVersion();
                String s4 = a1.getRawDate();
                if (s3 == null) {
                    s3 = "";
                }
                if (s4 == null) {
                    s4 = "";
                }
                boolean b = s2.equals(s0);
                label1: {
                    if (!b) {
                        break label1;
                    }
                    if (!s4.equals(s1)) {
                        break label1;
                    }
                    if (s3.equals(s)) {
                        break label0;
                    }
                }
                int i = a.getInt("offlineMapsUpdateCount", 0) + 1;
                String[] a3 = new String[14];
                a3[0] = "Old_Date";
                a3[1] = s1;
                a3[2] = "Old_Version";
                a3[3] = s;
                a3[4] = "Old_Packages";
                a3[5] = s0;
                a3[6] = "New_Version";
                a3[7] = s3;
                a3[8] = "New_Packages";
                a3[9] = s2;
                a3[10] = "New_Date";
                a3[11] = s4;
                a3[12] = "Update_Count";
                a3[13] = String.valueOf(i);
                com.navdy.hud.app.analytics.Event a4 = new com.navdy.hud.app.analytics.Event("Offline_Maps_Updated", a3);
                sLogger.i(new StringBuilder().append("Recording offline maps change:").append(a4).toString());
                com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(a4, false);
                a.edit().putString("previousOfflineMapsDate", s4).putString("previousOfflineMapsPackages", s2).putString("previousOfflineMapsVersion", s3).putInt("offlineMapsUpdateCount", i).apply();
            }
        }
    }
    
    public static void recordOptionSelection(String s) {
        String[] a = new String[2];
        a[0] = "Name";
        a[1] = s;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Option_Selected", a);
    }
    
    public static void recordPhoneDisconnect(boolean b, String s) {
        boolean b0 = com.navdy.hud.app.manager.SpeedManager.getInstance().getCurrentSpeed() > 0;
        if (remoteDevice != null) {
            com.navdy.hud.app.profile.DriverProfile a = sLastProfile;
            String s0 = null;
            String s1 = null;
            String s2 = null;
            if (a != null) {
                s0 = sLastProfile.getCarMake();
                s1 = sLastProfile.getCarModel();
                s2 = sLastProfile.getCarYear();
            }
            String s3 = b ? "reconnected" : "failed";
            String[] a0 = new String[28];
            a0[0] = "Status";
            a0[1] = s3;
            a0[2] = "Reason";
            a0[3] = Integer.toString(lastDiscReason);
            a0[4] = "Force_Reconnect";
            a0[5] = Boolean.toString(disconnectDueToForceReconnect);
            a0[6] = "Force_Reconnect_Reason";
            a0[7] = lastForceReconnectReason;
            a0[8] = "While_Moving";
            a0[9] = Boolean.toString(b0);
            a0[10] = "Phone_Type";
            a0[11] = remoteDevice.platform.toString();
            a0[12] = "Phone_OS";
            a0[13] = remoteDevice.systemVersion;
            a0[14] = "Phone_Make";
            a0[15] = remoteDevice.deviceMake;
            a0[16] = "Phone_Model";
            a0[17] = remoteDevice.model;
            a0[18] = "Elapsed_Time";
            a0[19] = s;
            a0[20] = "Car_Make";
            a0[21] = s0;
            a0[22] = "Car_Model";
            a0[23] = s1;
            a0[24] = "Car_Year";
            a0[25] = s2;
            a0[26] = "VIN";
            a0[27] = sLastVIN;
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Phone_Disconnected", a0);
            sLastVIN = null;
            sLastProfile = null;
            remoteDevice = null;
            lastDiscReason = 0;
            lastForceReconnectReason = "";
            disconnectDueToForceReconnect = false;
        }
    }
    
    private static com.navdy.hud.app.analytics.Event recordPreference(com.navdy.hud.app.profile.DriverProfile a) {
        String s = null;
        String s0 = null;
        com.navdy.service.library.events.preferences.InputPreferences a0 = a.getInputPreferences();
        com.navdy.service.library.events.preferences.NotificationPreferences a1 = a.getNotificationPreferences();
        com.navdy.service.library.events.preferences.LocalPreferences a2 = a.getLocalPreferences();
        String s1 = (a.getUnitSystem() != com.navdy.service.library.events.preferences.DriverProfilePreferences$UnitSystem.UNIT_SYSTEM_IMPERIAL) ? "Metric" : "Imperial";
        String s2 = (a.getDisplayFormat() != com.navdy.service.library.events.preferences.DriverProfilePreferences$DisplayFormat.DISPLAY_FORMAT_COMPACT) ? "Normal" : "Compact";
        switch(com.navdy.hud.app.analytics.AnalyticsSupport$3.$SwitchMap$com$navdy$service$library$events$preferences$DriverProfilePreferences$FeatureMode[a.getFeatureMode().ordinal()]) {
            case 3: {
                s = "Experimental";
                break;
            }
            case 2: {
                s = "Release";
                break;
            }
            case 1: {
                s = "Beta";
                break;
            }
            default: {
                s = "Unknown";
            }
        }
        label2: if (a1.enabled.booleanValue()) {
            boolean b = a1.readAloud.booleanValue();
            label3: {
                if (!b) {
                    break label3;
                }
                if (a1.showContent.booleanValue()) {
                    break label3;
                }
                s0 = "Read_Aloud";
                break label2;
            }
            boolean b0 = a1.readAloud.booleanValue();
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (a1.showContent.booleanValue()) {
                        break label0;
                    }
                }
                s0 = "Read_and_Show";
                break label2;
            }
            s0 = "Show_Content";
        } else {
            s0 = "Disabled";
        }
        com.navdy.service.library.events.preferences.DriverProfilePreferences$ObdScanSetting a3 = a.getObdScanSetting();
        String s3 = a.getLocale().toString();
        String s4 = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a.isAutoOnEnabled());
        String s5 = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a0.use_gestures.booleanValue());
        String s6 = (a3 == null) ? "null" : a3.name();
        String[] a4 = new String[20];
        a4[0] = "Locale";
        a4[1] = s3;
        a4[2] = "Units";
        a4[3] = s1;
        a4[4] = "Auto_On";
        a4[5] = s4;
        a4[6] = "Display_Format";
        a4[7] = s2;
        a4[8] = "Feature_Mode";
        a4[9] = s;
        a4[10] = "Gestures";
        a4[11] = s5;
        a4[12] = "Glances";
        a4[13] = s0;
        a4[14] = "OBD";
        a4[15] = s6;
        a4[16] = "Limit_Bandwidth";
        a4[17] = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a.isLimitBandwidthModeOn());
        a4[18] = "Manual_Zoom";
        a4[19] = com.navdy.hud.app.analytics.AnalyticsSupport.isEnabled(a2.manualZoom.booleanValue());
        return new com.navdy.hud.app.analytics.Event("Driver_Preferences", a4);
    }
    
    public static void recordPreferenceChange(com.navdy.hud.app.profile.DriverProfile a) {
        if (sLastProfilePreferences != null) {
            sLastProfilePreferences.enqueue(a);
        }
    }
    
    public static void recordRouteSelectionCancelled() {
        if (nearbySearchPlaceType != null) {
            sLogger.v("nearby search route calculation cancelled");
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Nearby_Search_Trip_Cancelled", new String[0]);
            nearbySearchPlaceType = null;
        }
        if (voiceSearchProgress != null) {
            com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason a = null;
            if (voiceSearchProgress != com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress.PROGRESS_SEARCHING) {
                com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress a0 = voiceSearchProgress;
                com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress a1 = com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress.PROGRESS_LIST_DISPLAYED;
                a = null;
                if (a0 == a1) {
                    a = com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason.cancel_list;
                }
            } else {
                a = com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason.cancel_search;
            }
            if (a != null) {
                sLogger.v(new StringBuilder().append("route selection cancelled, reason = ").append(a).toString());
                com.navdy.hud.app.analytics.AnalyticsSupport.sendVoiceSearchEvent(a, (com.navdy.service.library.events.navigation.NavigationRouteRequest)null, (String)null, voiceSearchConfidence, voiceSearchCount - 1, voiceSearchListeningOverBluetooth ? "bluetooth" : "phone", voiceSearchResults, voiceSearchAdditionalResultsAction, voiceSearchExplicitRetry, (Integer)null, voiceSearchPrefix);
            } else {
                sLogger.e(new StringBuilder().append("unexpected progress ").append(voiceSearchProgress).append(" in recordRouteSelectionCancelled()").toString());
                com.navdy.hud.app.analytics.AnalyticsSupport.clearVoiceSearch();
            }
        } else if (voiceResultsRunnable != null) {
            sLogger.v("route calculation cancelled");
            voiceResultsRunnable.cancel(com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason.cancel_route_calculation);
        }
    }
    
    public static void recordScreen(com.navdy.hud.app.screen.BaseScreen a) {
        if (a.getScreen() != com.navdy.service.library.events.ui.Screen.SCREEN_WELCOME) {
            if (a.getScreen() == com.navdy.service.library.events.ui.Screen.SCREEN_HOME && !phonePaired && welcomeScreenTime != -1L && sRemoteDeviceManager.isRemoteDeviceConnected()) {
                long j = android.os.SystemClock.elapsedRealtime();
                String s = newDevicePaired ? "true" : "false";
                String[] a0 = new String[4];
                a0[0] = "Was_New_Device";
                a0[1] = s;
                a0[2] = "Time_To_Connect";
                a0[3] = Long.toString(j - welcomeScreenTime);
                com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Attempt_To_Connect", a0);
                welcomeScreenTime = -1L;
                newDevicePaired = false;
                phonePaired = true;
            }
        } else {
            welcomeScreenTime = android.os.SystemClock.elapsedRealtime();
        }
        if (localyticsInitialized) {
            com.localytics.android.Localytics.tagScreen(com.navdy.hud.app.analytics.AnalyticsSupport.screenName(a));
        }
    }
    
    public static void recordShutdown(com.navdy.hud.app.event.Shutdown$Reason a, boolean b) {
        String s = b ? "quiet" : "full";
        double d = sObdManager.getBatteryVoltage();
        String s0 = quietMode ? "Sleep_Time" : "Uptime";
        String[] a0 = new String[8];
        a0[0] = "Type";
        a0[1] = s;
        a0[2] = "Reason";
        a0[3] = a.attr;
        a0[4] = "Battery_Level";
        java.util.Locale a1 = java.util.Locale.US;
        Object[] a2 = new Object[1];
        a2[0] = Double.valueOf(d);
        a0[5] = String.format(a1, "%6.1f", a2);
        a0[6] = s0;
        a0[7] = Long.toString(com.navdy.hud.app.analytics.AnalyticsSupport.uptime());
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Shutdown", false, true, a0);
    }
    
    public static void recordSmsSent(boolean b, String s, boolean b0) {
        sLogger.v(new StringBuilder().append("sms-sent success=").append(b).append(" plaform=").append(s).append(" canned=").append(b0).toString());
        String s0 = b0 ? "Canned" : "Custom";
        String[] a = new String[6];
        a[0] = "Success";
        a[1] = String.valueOf(b);
        a[2] = "Platform";
        a[3] = s;
        a[4] = "Message_Type";
        a[5] = s0;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("sms_sent", a);
    }
    
    public static void recordStartingBatteryVoltage(double d) {
        if (startingBatteryVoltage == -1.0) {
            startingBatteryVoltage = d;
            com.navdy.service.library.log.Logger a = sLogger;
            java.util.Locale a0 = java.util.Locale.US;
            Object[] a1 = new Object[1];
            a1[0] = Double.valueOf(d);
            a.i(String.format(a0, "starting battery voltage = %6.1f", a1));
        }
    }
    
    public static void recordSwipedCalibration(String s, String[] a) {
        String s0 = new StringBuilder().append("Swiped_").append(com.navdy.hud.app.analytics.AnalyticsSupport.normalizeName(s)).toString();
        int i = 0;
        while(i < a.length - 1) {
            a[i] = com.navdy.hud.app.analytics.AnalyticsSupport.normalizeName(a[i]);
            i = i + 2;
        }
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(s0, a);
    }
    
    public static void recordUpdatePrompt(boolean b, boolean b0, String s) {
        String s0 = b0 ? "Dial_Update_Prompt" : "Display_Update_Prompt";
        String s1 = b ? "true" : "false";
        String[] a = new String[4];
        a[0] = "Accepted";
        a[1] = s1;
        a[2] = "Update_To_Version";
        a[3] = s;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(s0, a);
    }
    
    public static void recordVehicleTelemetryData(long j, long j0, long j1, float f, float f0, float f1, int i, int i0, float f2, float f3, int i1, String s, int i2, int i3, int i4, int i5, int i6, float f4, float f5, float f6, int i7) {
        String s0 = null;
        String s1 = null;
        String s2 = null;
        com.navdy.hud.app.profile.DriverProfile a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getDriverProfileManager().getCurrentProfile();
        if (a == null) {
            s0 = "";
            s1 = "";
            s2 = "";
        } else {
            s0 = a.getCarMake();
            s1 = a.getCarModel();
            s2 = a.getCarYear();
        }
        String[] a0 = new String[48];
        a0[0] = "Session_Distance";
        a0[1] = Long.toString(j);
        a0[2] = "Session_Duration";
        a0[3] = Long.toString(j0);
        a0[4] = "Session_Rolling_Duration";
        a0[5] = Long.toString(j1);
        a0[6] = "Session_Average_Speed";
        a0[7] = Float.toString(f);
        a0[8] = "Session_Average_Rolling_Speed";
        a0[9] = Float.toString(f0);
        a0[10] = "Session_Max_Speed";
        a0[11] = Float.toString(f1);
        a0[12] = "Session_Hard_Braking_Count";
        a0[13] = Integer.toString(i);
        a0[14] = "Session_Hard_Acceleration_Count";
        a0[15] = Integer.toString(i0);
        a0[16] = "Session_Speeding_Frequency";
        a0[17] = Float.toString(f2);
        a0[18] = "Session_Excessive_Speeding_Frequency";
        a0[19] = Float.toString(f3);
        a0[20] = "Session_Trouble_Code_Count";
        a0[21] = Integer.toString(i1);
        a0[22] = "Session_Trouble_Code_Last";
        a0[23] = s;
        a0[24] = "Session_Average_Mpg";
        a0[25] = Integer.toString(i2);
        a0[26] = "Session_Max_RPM";
        a0[27] = Integer.toString(i3);
        a0[28] = "Event_Average_Mpg";
        a0[29] = Integer.toString(i4);
        a0[30] = "Event_Average_Rpm";
        a0[31] = Integer.toString(i5);
        a0[32] = "Event_Fuel_Level";
        a0[33] = Integer.toString(i6);
        a0[34] = "Event_Engine_Temperature";
        a0[35] = Float.toString(f4);
        a0[36] = "Event_Max_G";
        a0[37] = Float.toString(f5);
        a0[38] = "Event_Max_G_Angle";
        a0[39] = Float.toString(f6);
        a0[40] = "Session_High_G_Count";
        a0[41] = Integer.toString(i7);
        a0[42] = "Car_Make";
        a0[43] = s0;
        a0[44] = "Car_Model";
        a0[45] = s1;
        a0[46] = "Car_Year";
        a0[47] = s2;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Vehicle_Telemetry_Data", a0);
    }
    
    public static void recordVoiceSearchAdditionalResultsAction(com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction a) {
        voiceSearchAdditionalResultsAction = a;
        if (a != com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction.ADDITIONAL_RESULTS_LIST) {
            voiceSearchProgress = com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress.PROGRESS_ROUTE_SELECTED;
        } else {
            voiceSearchProgress = com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress.PROGRESS_LIST_DISPLAYED;
        }
    }
    
    public static void recordVoiceSearchEntered() {
        if (voiceSearchProgress == com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress.PROGRESS_LIST_DISPLAYED) {
            com.navdy.hud.app.analytics.AnalyticsSupport.clearVoiceSearch();
            voiceSearchExplicitRetry = true;
        }
    }
    
    public static void recordVoiceSearchListItemSelection(int i) {
        label2: {
            label0: {
                label1: {
                    if (i == -1) {
                        break label1;
                    }
                    if (i != -2) {
                        break label0;
                    }
                }
                voiceSearchExplicitRetry = true;
                break label2;
            }
            voiceSearchListSelection = Integer.valueOf(i);
            voiceSearchProgress = com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress.PROGRESS_ROUTE_SELECTED;
        }
    }
    
    public static void recordVoiceSearchResult(com.navdy.service.library.events.audio.VoiceSearchResponse a) {
        switch(com.navdy.hud.app.analytics.AnalyticsSupport$3.$SwitchMap$com$navdy$service$library$events$audio$VoiceSearchResponse$VoiceSearchState[a.state.ordinal()]) {
            case 5: {
                sLogger.v(new StringBuilder().append("voice search error: ").append(a.error).toString());
                com.navdy.hud.app.analytics.AnalyticsSupport.clearVoiceSearch();
                com.navdy.hud.app.analytics.AnalyticsSupport.sendVoiceSearchErrorEvent(a.error, voiceSearchCount - 1);
                break;
            }
            case 3: {
                sLogger.v("voice search successful");
                voiceSearchProgress = com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress.PROGRESS_DESTINATION_FOUND;
                if (a.listeningOverBluetooth != null) {
                    voiceSearchListeningOverBluetooth = a.listeningOverBluetooth.booleanValue();
                }
                voiceSearchResults = a.results;
            }
            case 4: {
                if (a.recognizedWords != null) {
                    voiceSearchWords = a.recognizedWords;
                }
                if (a.confidenceLevel != null) {
                    voiceSearchConfidence = a.confidenceLevel;
                }
                if (a.prefix == null) {
                    break;
                }
                voiceSearchPrefix = a.prefix;
                break;
            }
            case 2: {
                if (a.listeningOverBluetooth == null) {
                    break;
                }
                voiceSearchListeningOverBluetooth = a.listeningOverBluetooth.booleanValue();
                break;
            }
            case 1: {
                voiceSearchCount = voiceSearchCount + 1;
                if (voiceSearchProgress != null) {
                    sLogger.w(new StringBuilder().append("starting voice search with unexpected progress state ").append(voiceSearchProgress).toString());
                    com.navdy.hud.app.analytics.AnalyticsSupport.clearVoiceSearch();
                }
                sLogger.v("starting voice search");
                voiceSearchProgress = com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchProgress.PROGRESS_SEARCHING;
                break;
            }
        }
    }
    
    public static void recordVoiceSearchRetry() {
        voiceSearchExplicitRetry = true;
    }
    
    private static void recordWakeup(com.navdy.hud.app.analytics.AnalyticsSupport$WakeupReason a) {
        String s = Long.toString(com.navdy.hud.app.analytics.AnalyticsSupport.uptime());
        java.util.Locale a0 = java.util.Locale.US;
        Object[] a1 = new Object[1];
        a1[0] = Double.valueOf(sObdManager.getBatteryVoltage());
        String s0 = String.format(a0, "%6.1f", a1);
        java.util.Locale a2 = java.util.Locale.US;
        Object[] a3 = new Object[1];
        a3[0] = Double.valueOf(sObdManager.getMaxBatteryVoltage());
        String s1 = String.format(a2, "%6.1f", a3);
        String s2 = sObdManager.getVin();
        double d = (startingBatteryVoltage == -1.0) ? 0.0 : startingBatteryVoltage - sObdManager.getMinBatteryVoltage();
        java.util.Locale a4 = java.util.Locale.US;
        Object[] a5 = new Object[1];
        a5[0] = Double.valueOf(d);
        String s3 = String.format(a4, "%6.1f", a5);
        if (s2 == null) {
            deferredWakeup = new com.navdy.hud.app.analytics.AnalyticsSupport$DeferredWakeup(a, s0, s3, s, s1);
        } else {
            String[] a6 = new String[12];
            a6[0] = "Reason";
            a6[1] = a.attr;
            a6[2] = "Battery_Level";
            a6[3] = s0;
            a6[4] = "Battery_Drain";
            a6[5] = s3;
            a6[6] = "Sleep_Time";
            a6[7] = s;
            a6[8] = "Max_Battery";
            a6[9] = s1;
            a6[10] = "VIN";
            a6[11] = s2;
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Wakeup", false, true, a6);
        }
        startTime = java.util.concurrent.TimeUnit.NANOSECONDS.toSeconds(System.nanoTime());
        startingDistance = sRemoteDeviceManager.getTripManager().getTotalDistanceTravelled();
    }
    
    private static void reportObdState() {
        java.util.List a = sObdManager.getEcus();
        int i = (a != null) ? a.size() : 0;
        if (i == 0) {
            sLogger.e("Obd connection with no ECUs");
        }
        sLastVIN = sObdManager.getVin();
        String s = sObdManager.getObdChipFirmwareVersion();
        String[] a0 = new String[i * 2 + 6];
        a0[0] = "Obd_Chip_Firmware";
        if (s == null) {
            s = "";
        }
        a0[1] = s;
        a0[2] = "VIN";
        a0[3] = sLastVIN;
        a0[4] = "Protocol";
        a0[5] = sObdManager.getProtocol();
        Object a1 = a;
        int i0 = 6;
        int i1 = 0;
        while(i1 < i) {
            com.navdy.obd.ECU a2 = (com.navdy.obd.ECU)((java.util.List)a1).get(i1);
            java.util.List a3 = a2.supportedPids.asList();
            int i2 = i0 + 1;
            java.util.Locale a4 = java.util.Locale.US;
            Object[] a5 = new Object[2];
            a5[0] = "ECU";
            a5[1] = Integer.valueOf(i1 + 1);
            a0[i0] = String.format(a4, "%s%d", a5);
            i0 = i2 + 1;
            java.util.Locale a6 = java.util.Locale.US;
            Object[] a7 = new Object[2];
            a7[0] = Integer.valueOf(a2.address);
            a7[1] = com.navdy.hud.app.analytics.AnalyticsSupport.pidListToHexString(a3);
            a0[i2] = String.format(a6, "Address: 0x%x, Pids: %s", a7);
            i1 = i1 + 1;
        }
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("OBD_Connected", a0);
        if (deferredWakeup != null) {
            String[] a8 = new String[12];
            a8[0] = "Reason";
            a8[1] = deferredWakeup.reason.attr;
            a8[2] = "Battery_Level";
            a8[3] = deferredWakeup.batteryLevel;
            a8[4] = "Battery_Drain";
            a8[5] = deferredWakeup.batteryDrain;
            a8[6] = "Sleep_Time";
            a8[7] = deferredWakeup.sleepTime;
            a8[8] = "Max_Battery";
            a8[9] = deferredWakeup.maxBattery;
            a8[10] = "VIN";
            a8[11] = sLastVIN;
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Wakeup", false, true, a8);
            deferredWakeup = null;
        }
    }
    
    private static String screenName(com.navdy.hud.app.screen.BaseScreen a) {
        com.navdy.service.library.events.ui.Screen a0 = a.getScreen();
        String s = a0.toString();
        if (a0 == com.navdy.service.library.events.ui.Screen.SCREEN_HOME) {
            com.navdy.hud.app.ui.component.homescreen.HomeScreen$DisplayMode a1 = ((com.navdy.hud.app.ui.component.homescreen.HomeScreen)a).getDisplayMode();
            if (a1 != null && com.navdy.hud.app.maps.here.HereMapsManager.getInstance().isInitialized()) {
                String s0 = (com.navdy.hud.app.maps.here.HereNavigationManager.getInstance().isNavigationModeOn()) ? "_ROUTE" : "";
                s = new StringBuilder().append(a1.toString()).append(s0).toString();
            }
        }
        return s;
    }
    
    private static void sendDeferredLocalyticsEvents() {
        boolean b = localyticsInitialized;
        synchronized(com.navdy.hud.app.analytics.AnalyticsSupport.class) {
            if (b) {
                if (deferredEvents != null) {
                    sLogger.v(new StringBuilder().append("sending ").append(deferredEvents.size()).append(" deferred localytics events").toString());
                    Object a = deferredEvents.iterator();
                    while(((java.util.Iterator)a).hasNext()) {
                        com.navdy.hud.app.analytics.AnalyticsSupport$DeferredLocalyticsEvent a0 = (com.navdy.hud.app.analytics.AnalyticsSupport$DeferredLocalyticsEvent)((java.util.Iterator)a).next();
                        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(a0.tag, a0.args);
                    }
                    deferredEvents = null;
                }
            } else {
                sLogger.e("sendDeferredLocalyticsEvents() called before Localytics was initialized");
            }
        }
        /*monexit(com.navdy.hud.app.analytics.AnalyticsSupport.class)*/;
    }
    
    private static void sendDistance() {
        int i = com.navdy.hud.app.manager.RemoteDeviceManager.getInstance().getTripManager().getTotalDistanceTravelled() - startingDistance;
        if (i > 10 && !quietMode) {
            long j = com.navdy.hud.app.analytics.AnalyticsSupport.uptime();
            String s = Long.toString((long)i);
            String s0 = Long.toString(j);
            String s1 = destinationSet ? "true" : "false";
            String s2 = phonePaired ? "true" : "false";
            String[] a = new String[10];
            a[0] = "Distance";
            a[1] = s;
            a[2] = "Duration";
            a[3] = s0;
            a[4] = "Navigating";
            a[5] = s1;
            a[6] = "Connection";
            a[7] = s2;
            a[8] = "Vin";
            a[9] = sLastVIN;
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Distance_Travelled", false, true, a);
        }
    }
    
    private static void sendVoiceSearchErrorEvent(com.navdy.service.library.events.audio.VoiceSearchResponse$VoiceSearchError a, int i) {
        String s = com.navdy.hud.app.analytics.AnalyticsSupport.getVoiceSearchEvent();
        if (s != null) {
            String s0 = (a != null) ? a.toString() : "";
            String[] a0 = new String[6];
            a0[0] = "Response";
            a0[1] = s0;
            a0[2] = "Retry_Count";
            a0[3] = Integer.toString(i);
            a0[4] = "Client_Version";
            a0[5] = com.navdy.hud.app.analytics.AnalyticsSupport.getClientVersion();
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(s, a0);
        } else {
            sLogger.e("cannot determine phone_type for voice search");
        }
    }
    
    private static void sendVoiceSearchEvent(com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason a, com.navdy.service.library.events.navigation.NavigationRouteRequest a0, String s, Integer a1, int i, String s0, java.util.List a2, com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchAdditionalResultsAction a3, boolean b, Integer a4, String s1) {
        String s2 = com.navdy.hud.app.analytics.AnalyticsSupport.getVoiceSearchEvent();
        if (s2 != null) {
            boolean b0 = false;
            String s3 = null;
            boolean b1 = false;
            java.util.HashMap a5 = new java.util.HashMap();
            ((java.util.Map)a5).put("Response", "Success");
            ((java.util.Map)a5).put("Client_Version", com.navdy.hud.app.analytics.AnalyticsSupport.getClientVersion());
            if (a1 != null) {
                ((java.util.Map)a5).put("Confidence_Level", a1.toString());
            }
            if (s != null) {
                ((java.util.Map)a5).put("Number_Of_Words", Integer.toString(com.navdy.hud.app.analytics.AnalyticsSupport.countWords(s)));
            }
            com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason a6 = com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason.end_trip;
            label10: {
                label8: {
                    label9: {
                        if (a == a6) {
                            break label9;
                        }
                        if (a == com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason.new_voice_search) {
                            break label9;
                        }
                        if (a != com.navdy.hud.app.analytics.AnalyticsSupport$VoiceSearchCancelReason.new_non_voice_search) {
                            break label8;
                        }
                    }
                    b0 = true;
                    break label10;
                }
                b0 = false;
            }
            label7: {
                label5: {
                    label6: {
                        if (a == null) {
                            break label6;
                        }
                        if (!b0) {
                            break label5;
                        }
                    }
                    s3 = "true";
                    break label7;
                }
                s3 = "false";
            }
            ((java.util.Map)a5).put("Navigation_Initiated", s3);
            ((java.util.Map)a5).put("Navigation_Cancelled_Quickly", (b0 ? "true" : "false"));
            ((java.util.Map)a5).put("Retry_Count", Integer.toString(i));
            ((java.util.Map)a5).put("Microphone_Used", s0);
            ((java.util.Map)a5).put("Num_Total_Results", Integer.toString((a2 != null) ? a2.size() : 0));
            if (a3 != null) {
                ((java.util.Map)a5).put("Additional_Results_Glance", a3.tag);
            }
            ((java.util.Map)a5).put("Explicit_Retry", (b ? "true" : "false"));
            if (a != null) {
                ((java.util.Map)a5).put("Cancel_Type", a.toString());
            }
            if (a4 != null) {
                ((java.util.Map)a5).put("Additional_Results_List_Item_Selected", a4.toString());
            }
            if (a0 == null) {
                b1 = false;
            } else {
                String s4 = null;
                com.navdy.service.library.events.destination.Destination a7 = a0.requestDestination;
                label4: {
                    label2: {
                        label3: {
                            if (a7 == null) {
                                break label3;
                            }
                            if (a7.suggestion_type == null) {
                                break label3;
                            }
                            if (a7.suggestion_type != com.navdy.service.library.events.destination.Destination$SuggestionType.SUGGESTION_NONE) {
                                break label2;
                            }
                        }
                        b1 = false;
                        break label4;
                    }
                    b1 = true;
                }
                com.navdy.service.library.events.destination.Destination$FavoriteType a8 = a0.destinationType;
                label1: {
                    label0: {
                        if (a8 == null) {
                            break label0;
                        }
                        if (a0.destinationType == com.navdy.service.library.events.destination.Destination$FavoriteType.FAVORITE_NONE) {
                            break label0;
                        }
                        s4 = a0.destinationType.toString();
                        break label1;
                    }
                    s4 = (a0.label == null) ? "ADDRESS" : "PLACE";
                }
                ((java.util.Map)a5).put("Destination_Type", s4);
            }
            ((java.util.Map)a5).put("Suggestion", (b1 ? "true" : "false"));
            if (s1 != null) {
                ((java.util.Map)a5).put("Prefix", s1);
            }
            com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent(new com.navdy.hud.app.analytics.Event(s2, (java.util.Map)a5), false);
        } else {
            sLogger.e("cannot determine phone_type for voice search");
        }
    }
    
    public static void setGlanceNavigationSource(String s) {
        glanceNavigationSource = s;
    }
    
    private static void setupCustomDimensions() {
        com.localytics.android.Localytics.setCustomDimension(0, com.navdy.hud.app.analytics.AnalyticsSupport.getHwVersion());
    }
    
    private static void setupProfile() {
        String s = null;
        String s0 = null;
        String s1 = null;
        com.navdy.hud.app.profile.DriverProfile a = com.navdy.hud.app.framework.DriverProfileHelper.getInstance().getCurrentProfile();
        boolean b = a.isDefaultProfile();
        String s2 = null;
        String s3 = null;
        String s4 = null;
        if (!b) {
            com.localytics.android.Localytics.setCustomerFullName(a.getDriverName());
            com.localytics.android.Localytics.setCustomerEmail(a.getDriverEmail());
            s2 = a.getCarMake();
            s3 = a.getCarModel();
            s4 = a.getCarYear();
            com.localytics.android.Localytics.setProfileAttribute("Hud_Serial_Number", android.os.Build.SERIAL);
            com.localytics.android.Localytics.setProfileAttribute("Car_Make", s2);
            com.localytics.android.Localytics.setProfileAttribute("Car_Model", s3);
            com.localytics.android.Localytics.setProfileAttribute("Car_Year", s4);
            com.localytics.android.Localytics.setProfileAttribute("Hud_Version_Number", com.navdy.hud.app.util.os.SystemProperties.get("ro.build.description", ""));
            com.localytics.android.Localytics.setProfileAttribute("Build_Type", android.os.Build.TYPE);
        }
        com.navdy.service.library.events.DeviceInfo a0 = sRemoteDeviceManager.getRemoteDeviceInfo();
        if (a0 == null) {
            sRemoteDeviceId = "unknown";
            s = "unknown";
            s0 = "unknown";
            s1 = "unknown";
        } else {
            s = a0.platform.toString();
            s0 = a0.deviceName;
            s1 = a0.clientVersion;
            sRemoteDeviceId = a0.deviceId;
        }
        String s5 = sObdManager.getVin();
        String[] a1 = new String[18];
        a1[0] = "Device_Id";
        a1[1] = android.os.Build.SERIAL;
        a1[2] = "Remote_Device_Id";
        a1[3] = sRemoteDeviceId;
        a1[4] = "Remote_Device_Name";
        a1[5] = s0;
        a1[6] = "Remote_Platform";
        a1[7] = s;
        a1[8] = "Remote_Client_Version";
        a1[9] = s1;
        a1[10] = "Car_Make";
        a1[11] = s2;
        a1[12] = "Car_Model";
        a1[13] = s3;
        a1[14] = "Car_Year";
        a1[15] = s4;
        a1[16] = "VIN";
        a1[17] = s5;
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Mobile_App_Connect", a1);
        sLogger.i(new StringBuilder().append("setupProfile: DeviceId = ").append(android.os.Build.SERIAL).append(", RemoteDeviceId = ").append(sRemoteDeviceId).append(", remotePlatform = ").append(", RemoteDeviceName = ").append(s0).append(", RemoteClientVersion = ").append(s1).append(" vin = ").append(s5).toString());
    }
    
    static void submitNavigationQualityReport(com.navdy.hud.app.analytics.NavigationQualityTracker$Report a) {
        String[] a0 = new String[16];
        a0[0] = "Expected_Duration";
        a0[1] = String.valueOf(a.expectedDuration);
        a0[2] = "Actual_Duration";
        a0[3] = String.valueOf(a.actualDuration);
        a0[4] = "Expected_Distance";
        a0[5] = String.valueOf(a.expectedDistance);
        a0[6] = "Actual_Distance";
        a0[7] = String.valueOf(a.actualDistance);
        a0[8] = "Recalculations";
        a0[9] = String.valueOf(a.nRecalculations);
        a0[10] = "Duration_PCT_Diff";
        a0[11] = String.valueOf(a.getDurationVariance());
        a0[12] = "Distance_PCT_Diff";
        a0[13] = String.valueOf(a.getDistanceVariance());
        a0[14] = "Traffic_Level";
        a0[15] = a.trafficLevel.name();
        com.navdy.hud.app.analytics.AnalyticsSupport.localyticsSendEvent("Route_Completed", a0);
    }
    
    private static long uptime() {
        return java.util.concurrent.TimeUnit.NANOSECONDS.toSeconds(System.nanoTime()) - startTime;
    }
}
