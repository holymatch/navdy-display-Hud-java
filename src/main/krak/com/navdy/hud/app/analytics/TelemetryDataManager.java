package com.navdy.hud.app.analytics;

final public class TelemetryDataManager {
    final private static long ANALYTICS_REPORTING_INTERVAL = 300000L;
    final public static com.navdy.hud.app.analytics.TelemetryDataManager$Companion Companion;
    final private static long DRIVE_SCORE_PUBLISH_INTERVAL = 3000L;
    final private static boolean LOG_TELEMETRY_DATA;
    final private static float MAX_ACCEL = 2f;
    final private static String PREFERENCE_TROUBLE_CODES = "PREFRENCE_TROUBLE_CODES";
    final private static com.navdy.service.library.log.Logger sLogger;
    final private com.squareup.otto.Bus bus;
    private com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated driveScoreUpdatedEvent;
    private android.os.Handler handler;
    private boolean highAccuracySpeedAvailable;
    private int lastDriveScore;
    final private com.navdy.hud.app.device.PowerManager powerManager;
    final private kotlin.jvm.functions.Function0 reportRunnable;
    final private android.content.SharedPreferences sharedPreferences;
    final private com.navdy.hud.app.framework.trips.TripManager tripManager;
    final private com.navdy.hud.app.ui.framework.UIStateManager uiStateManager;
    final private Runnable updateDriveScoreRunnable;
    
    static {
        Companion = new com.navdy.hud.app.analytics.TelemetryDataManager$Companion((kotlin.jvm.internal.DefaultConstructorMarker)null);
        sLogger = new com.navdy.service.library.log.Logger("TelemetryDataManager");
        PREFERENCE_TROUBLE_CODES = "PREFRENCE_TROUBLE_CODES";
        ANALYTICS_REPORTING_INTERVAL = 300000L;
        DRIVE_SCORE_PUBLISH_INTERVAL = 3000L;
        MAX_ACCEL = 2f;
        LOG_TELEMETRY_DATA = com.navdy.hud.app.util.os.SystemProperties.getBoolean("persist.sys.prop.log.telemetry", false);
    }
    
    public TelemetryDataManager(com.navdy.hud.app.ui.framework.UIStateManager a, com.navdy.hud.app.device.PowerManager a0, com.squareup.otto.Bus a1, android.content.SharedPreferences a2, com.navdy.hud.app.framework.trips.TripManager a3) {
        super();
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "uiStateManager");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a0, "powerManager");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a1, "bus");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a2, "sharedPreferences");
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a3, "tripManager");
        this.uiStateManager = a;
        this.powerManager = a0;
        this.bus = a1;
        this.sharedPreferences = a2;
        this.tripManager = a3;
        this.reportRunnable = (kotlin.jvm.functions.Function0)new com.navdy.hud.app.analytics.TelemetryDataManager$reportRunnable$1(this);
        com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setDataSource((com.navdy.hud.app.analytics.TelemetrySession$DataSource)new com.navdy.hud.app.analytics.TelemetryDataManager$1(this));
        this.bus.register(this);
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.updateDriveScoreRunnable = (Runnable)new com.navdy.hud.app.analytics.TelemetryDataManager$updateDriveScoreRunnable$1(this);
        this.driveScoreUpdatedEvent = new com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.NONE, false, false, false, 100);
    }
    
    final public static long access$getANALYTICS_REPORTING_INTERVAL$cp() {
        return ANALYTICS_REPORTING_INTERVAL;
    }
    
    final public static long access$getDRIVE_SCORE_PUBLISH_INTERVAL$cp() {
        return DRIVE_SCORE_PUBLISH_INTERVAL;
    }
    
    final public static boolean access$getLOG_TELEMETRY_DATA$cp() {
        return LOG_TELEMETRY_DATA;
    }
    
    final public static float access$getMAX_ACCEL$cp() {
        return MAX_ACCEL;
    }
    
    final public static String access$getPREFERENCE_TROUBLE_CODES$cp() {
        return PREFERENCE_TROUBLE_CODES;
    }
    
    final public static com.navdy.service.library.log.Logger access$getSLogger$cp() {
        return sLogger;
    }
    
    final public static com.navdy.hud.app.framework.trips.TripManager access$getTripManager$p(com.navdy.hud.app.analytics.TelemetryDataManager a) {
        return a.tripManager;
    }
    
    final public static com.navdy.hud.app.ui.framework.UIStateManager access$getUiStateManager$p(com.navdy.hud.app.analytics.TelemetryDataManager a) {
        return a.uiStateManager;
    }
    
    final public static void access$reportTelemetryData(com.navdy.hud.app.analytics.TelemetryDataManager a) {
        a.reportTelemetryData();
    }
    
    final private void reportTelemetryData() {
        if (com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getLOG_TELEMETRY_DATA$p(Companion)) {
            com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getSLogger$p(Companion).d(new StringBuilder().append(new StringBuilder().append("Distance = ").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionDistance()).append(",").toString()).append(new StringBuilder().append("Duration = ").append(com.navdy.hud.app.analytics.TelemetrySessionKt.milliSecondsToSeconds(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionDuration())).append(", ").toString()).append(new StringBuilder().append("RollingDuration = ").append(com.navdy.hud.app.analytics.TelemetrySessionKt.milliSecondsToSeconds(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionRollingDuration())).append(", ").toString()).append(new StringBuilder().append("AverageSpeed = ").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionAverageSpeed()).append(", ").toString()).append(new StringBuilder().append("AverageRollingSpeed = ").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionAverageRollingSpeed()).append(", ").toString()).append(new StringBuilder().append("MaxSpeed = ").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionMaxSpeed()).append(", ").toString()).append(new StringBuilder().append("HardBrakingCount = ").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHardBrakingCount()).append(", ").toString()).append(new StringBuilder().append("HardAccelerationCount = ").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHardAccelerationCount()).append(", ").toString()).append(new StringBuilder().append("SpeedingPercentage = ").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionSpeedingPercentage()).append(",").toString()).append(new StringBuilder().append("TroubleCodeCount = ").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionTroubleCodeCount()).append(",").toString()).append(new StringBuilder().append("TroubleCodesReport = ").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionTroubleCodesReport()).toString()).append(new StringBuilder().append("MaxRPM = ").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getMaxRpm()).append(",").toString()).toString());
            com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getSLogger$p(Companion).d(new StringBuilder().append(new StringBuilder().append("EVENT , averageMpg = ").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getEventAverageMpg()).append(",").toString()).append(new StringBuilder().append("averageRPM = ").append(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getEventAverageRpm()).append(", ").toString()).append(new StringBuilder().append("Fuel level = ").append(com.navdy.hud.app.obd.ObdManager.getInstance().getFuelLevel()).append(", ").toString()).append(new StringBuilder().append("Engine Temperature = ").append(com.navdy.hud.app.obd.ObdManager.getInstance().getPidValue(5)).append(", ").toString()).toString());
        }
        com.navdy.hud.app.analytics.AnalyticsSupport.recordVehicleTelemetryData(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionDistance(), com.navdy.hud.app.analytics.TelemetrySessionKt.milliSecondsToSeconds(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionDuration()), com.navdy.hud.app.analytics.TelemetrySessionKt.milliSecondsToSeconds(com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionRollingDuration()), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionAverageSpeed(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionAverageRollingSpeed(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionMaxSpeed(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHardBrakingCount(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHardAccelerationCount(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionSpeedingPercentage(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionExcessiveSpeedingPercentage(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionTroubleCodeCount(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionTroubleCodesReport(), (int)com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionAverageMpg(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getMaxRpm(), (int)com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getEventAverageMpg(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getEventAverageRpm(), com.navdy.hud.app.obd.ObdManager.getInstance().getFuelLevel(), (float)com.navdy.hud.app.obd.ObdManager.getInstance().getPidValue(5), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getMaxG(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getMaxGAngle(), com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionHighGCount());
        com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.eventReported();
        android.os.Handler a = this.handler;
        kotlin.jvm.functions.Function0 a0 = this.reportRunnable;
        a.postDelayed((Runnable)((a0 != null) ? new com.navdy.hud.app.analytics.TelemetryDataManagerKt$sam$Runnable$cc6381d8(a0) : null), com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getANALYTICS_REPORTING_INTERVAL$p(Companion));
    }
    
    public static void updateDriveScore$default(com.navdy.hud.app.analytics.TelemetryDataManager a, com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a0, int i, Object a1) {
        if ((i & 1) != 0) {
            a0 = com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.NONE;
        }
        a.updateDriveScore(a0);
    }
    
    final private void updateSpeed() {
        if (com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionStarted()) {
            com.navdy.hud.app.analytics.TelemetrySession a = com.navdy.hud.app.analytics.TelemetrySession.INSTANCE;
            com.navdy.hud.app.analytics.RawSpeed a0 = com.navdy.hud.app.manager.SpeedManager.getInstance().getCurrentSpeedInMetersPerSecond();
            kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(a0, "SpeedManager.getInstance\u2026entSpeedInMetersPerSecond");
            a.setCurrentSpeed(a0);
        }
    }
    
    final public void GPSSpeedChangeEvent(com.navdy.hud.app.maps.MapEvents$GPSSpeedEvent a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "event");
        this.updateSpeed();
    }
    
    final public void ObdPidChangeEvent(com.navdy.hud.app.obd.ObdManager$ObdPidChangeEvent a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "event");
        switch(a.pid.getId()) {
            case 256: {
                com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setCurrentMpg(com.navdy.hud.app.util.ConversionUtil.convertLpHundredKmToMPG(a.pid.getValue()));
                break;
            }
            case 13: {
                this.updateSpeed();
                break;
            }
            case 12: {
                com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setCurrentRpm((int)a.pid.getValue());
                break;
            }
        }
    }
    
    final public com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated getDriveScoreUpdatedEvent() {
        return this.driveScoreUpdatedEvent;
    }
    
    final public boolean getHighAccuracySpeedAvailable() {
        return this.highAccuracySpeedAvailable;
    }
    
    final public int getLastDriveScore() {
        return this.lastDriveScore;
    }
    
    final public void onCalibratedGForceData(com.navdy.hud.app.device.gps.CalibratedGForceData a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "calibratedGForceData");
        if (com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionStarted()) {
            com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setGForce(a.getXAccel(), a.getYAccel(), a.getZAccel());
        }
    }
    
    final public void onGpsSwitched(com.navdy.hud.app.device.gps.GpsUtils$GpsSwitch a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "gpsSwitch");
        com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getSLogger$p(Companion).d(new StringBuilder().append("onGpsSwitched ").append(a).toString());
        com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.speedSourceChanged();
    }
    
    final public void onInit(com.navdy.hud.app.event.InitEvents$InitPhase a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "event");
        com.navdy.hud.app.event.InitEvents$Phase a0 = a.phase;
        if (a0 != null && com.navdy.hud.app.analytics.TelemetryDataManager$WhenMappings.$EnumSwitchMapping$0[a0.ordinal()] != 0) {
            com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getSLogger$p(Companion).i(new StringBuilder().append("Post Start , Quiet mode : ").append(this.powerManager.inQuietMode()).toString());
            if (!this.powerManager.inQuietMode()) {
                this.startTelemetrySession();
            }
        }
    }
    
    final public void onMapEvent(com.navdy.hud.app.maps.MapEvents$ManeuverDisplay a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "event");
        com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setCurrentSpeedLimit(a.currentSpeedLimit);
    }
    
    final public void onObdConnectionStateChanged(com.navdy.hud.app.obd.ObdManager$ObdConnectionStatusEvent a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "connectionStateChangeEvent");
        com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.speedSourceChanged();
        if (a.connected) {
            com.navdy.hud.app.obd.ObdManager.getInstance().getSupportedPids();
            this.highAccuracySpeedAvailable = true;
            String s = this.sharedPreferences.getString(com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getPREFERENCE_TROUBLE_CODES$p(Companion), "");
            java.util.List a0 = com.navdy.hud.app.obd.ObdManager.getInstance().getTroubleCodes();
            if (kotlin.jvm.internal.Intrinsics.areEqual(a0, null)) {
                this.sharedPreferences.edit().putString(com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getPREFERENCE_TROUBLE_CODES$p(Companion), "").apply();
                com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setSessionTroubleCodesReport("");
            } else {
                com.navdy.hud.app.analytics.TelemetryDataManager$Companion a1 = Companion;
                kotlin.jvm.internal.Intrinsics.checkExpressionValueIsNotNull(s, "troubleCodesString");
                kotlin.Pair a2 = a1.serializeTroubleCodes(a0, s);
                String s0 = (String)a2.component1();
                String s1 = (String)a2.component2();
                this.sharedPreferences.edit().putString(com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getPREFERENCE_TROUBLE_CODES$p(Companion), s0).apply();
                com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.setSessionTroubleCodesReport(s1);
            }
        } else {
            this.highAccuracySpeedAvailable = false;
        }
    }
    
    final public void onSpeedDataExpired(com.navdy.hud.app.manager.SpeedManager$SpeedDataExpired a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "speedDataExpired");
        com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.speedSourceChanged();
        this.updateSpeed();
    }
    
    final public void onWakeup(com.navdy.hud.app.event.Wakeup a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "wakeUpEvent");
        this.startTelemetrySession();
    }
    
    final public void setDriveScoreUpdatedEvent(com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "<set-?>");
        this.driveScoreUpdatedEvent = a;
    }
    
    final public void setHighAccuracySpeedAvailable(boolean b) {
        this.highAccuracySpeedAvailable = b;
    }
    
    final public void setLastDriveScore(int i) {
        this.lastDriveScore = i;
    }
    
    final public void startTelemetrySession() {
        if (com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.getSessionStarted()) {
            com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getSLogger$p(Companion).d("Telemetry session is already started");
        } else {
            com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getSLogger$p(Companion).i("Starting telemetry session");
            com.navdy.hud.app.analytics.TelemetrySession.INSTANCE.startSession();
            android.os.Handler a = this.handler;
            kotlin.jvm.functions.Function0 a0 = this.reportRunnable;
            a.removeCallbacks((Runnable)((a0 != null) ? new com.navdy.hud.app.analytics.TelemetryDataManagerKt$sam$Runnable$cc6381d8(a0) : null));
            android.os.Handler a1 = this.handler;
            kotlin.jvm.functions.Function0 a2 = this.reportRunnable;
            a1.postDelayed((Runnable)((a2 != null) ? new com.navdy.hud.app.analytics.TelemetryDataManagerKt$sam$Runnable$cc6381d8(a2) : null), com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getANALYTICS_REPORTING_INTERVAL$p(Companion));
            this.handler.postDelayed(this.updateDriveScoreRunnable, com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getDRIVE_SCORE_PUBLISH_INTERVAL$p(Companion));
        }
    }
    
    final public void updateDriveScore(com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent a) {
        kotlin.jvm.internal.Intrinsics.checkParameterIsNotNull(a, "interestingEvent");
        com.navdy.hud.app.analytics.TelemetrySession a0 = com.navdy.hud.app.analytics.TelemetrySession.INSTANCE;
        double d = (a0.getSessionRollingDuration() <= 0L) ? 0.0 : (double)(a0.getSessionHardAccelerationCount() + a0.getSessionHardBrakingCount() + a0.getSessionHighGCount()) * 540.0 / Math.sqrt((double)a0.getSessionRollingDuration() / 1000.0 + (double)60);
        float f = a0.getSessionSpeedingPercentage() * (float)50;
        float f0 = a0.getSessionExcessiveSpeedingPercentage() * (float)50;
        int i = (int)Math.ceil(Math.max(0.0, (double)100 - ((double)f + d + (double)f0)));
        this.driveScoreUpdatedEvent = new com.navdy.hud.app.analytics.TelemetryDataManager$DriveScoreUpdated(a, a0.isSpeeding(), a0.isExcessiveSpeeding(), a0.isDoingHighGManeuver(), i);
        boolean b = kotlin.jvm.internal.Intrinsics.areEqual(a, com.navdy.hud.app.analytics.TelemetrySession$InterestingEvent.NONE) ^ true;
        label0: {
            label1: {
                if (b) {
                    break label1;
                }
                if (i == this.lastDriveScore) {
                    break label0;
                }
            }
            com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getSLogger$p(Companion).d(new StringBuilder().append("HA : ").append(a0.getSessionHardAccelerationCount()).append(" , HB : ").append(a0.getSessionHardBrakingCount()).append(" , HG : ").append(a0.getSessionHighGCount()).toString());
            com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getSLogger$p(Companion).d(new StringBuilder().append("Speeding percentage : ").append(a0.getSessionSpeedingPercentage()).toString());
            com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getSLogger$p(Companion).d(new StringBuilder().append("Excessive Speeding percentage : ").append(a0.getSessionExcessiveSpeedingPercentage()).toString());
            com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getSLogger$p(Companion).d(new StringBuilder().append("Hard Acceleration Factor : ").append(d).append(" , Speeding Factor : ").append(f).append(" , excessive Speeding factor ").append(f0).toString());
            com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getSLogger$p(Companion).d(new StringBuilder().append("DriveScoreUpdate : ").append(this.driveScoreUpdatedEvent).toString());
            this.bus.post(this.driveScoreUpdatedEvent);
        }
        this.lastDriveScore = i;
        this.handler.removeCallbacks(this.updateDriveScoreRunnable);
        this.handler.postDelayed(this.updateDriveScoreRunnable, com.navdy.hud.app.analytics.TelemetryDataManager$Companion.access$getDRIVE_SCORE_PUBLISH_INTERVAL$p(Companion));
    }
}
