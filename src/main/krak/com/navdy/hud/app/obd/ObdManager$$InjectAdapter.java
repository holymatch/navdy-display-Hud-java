package com.navdy.hud.app.obd;

final public class ObdManager$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding driverProfileManager;
    private dagger.internal.Binding powerManager;
    private dagger.internal.Binding sharedPreferences;
    private dagger.internal.Binding telemetryDataManager;
    private dagger.internal.Binding tripManager;
    
    public ObdManager$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.obd.ObdManager", false, com.navdy.hud.app.obd.ObdManager.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.obd.ObdManager.class, (this).getClass().getClassLoader());
        this.driverProfileManager = a.requestBinding("com.navdy.hud.app.profile.DriverProfileManager", com.navdy.hud.app.obd.ObdManager.class, (this).getClass().getClassLoader());
        this.powerManager = a.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.obd.ObdManager.class, (this).getClass().getClassLoader());
        this.tripManager = a.requestBinding("com.navdy.hud.app.framework.trips.TripManager", com.navdy.hud.app.obd.ObdManager.class, (this).getClass().getClassLoader());
        this.sharedPreferences = a.requestBinding("android.content.SharedPreferences", com.navdy.hud.app.obd.ObdManager.class, (this).getClass().getClassLoader());
        this.telemetryDataManager = a.requestBinding("com.navdy.hud.app.analytics.TelemetryDataManager", com.navdy.hud.app.obd.ObdManager.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.bus);
        a0.add(this.driverProfileManager);
        a0.add(this.powerManager);
        a0.add(this.tripManager);
        a0.add(this.sharedPreferences);
        a0.add(this.telemetryDataManager);
    }
    
    public void injectMembers(com.navdy.hud.app.obd.ObdManager a) {
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        a.driverProfileManager = (com.navdy.hud.app.profile.DriverProfileManager)this.driverProfileManager.get();
        a.powerManager = (com.navdy.hud.app.device.PowerManager)this.powerManager.get();
        a.tripManager = (com.navdy.hud.app.framework.trips.TripManager)this.tripManager.get();
        a.sharedPreferences = (android.content.SharedPreferences)this.sharedPreferences.get();
        a.telemetryDataManager = (com.navdy.hud.app.analytics.TelemetryDataManager)this.telemetryDataManager.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.obd.ObdManager)a);
    }
}
