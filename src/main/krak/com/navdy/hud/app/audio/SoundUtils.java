package com.navdy.hud.app.audio;
import com.navdy.hud.app.R;

public class SoundUtils {
    private static boolean initialized;
    final private static com.navdy.service.library.log.Logger sLogger;
    private static java.util.HashMap soundIdMap;
    private static android.media.SoundPool soundPool;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.audio.SoundUtils.class);
        soundIdMap = new java.util.HashMap();
    }
    
    public SoundUtils() {
    }
    
    public static void init() {
        if (!initialized) {
            com.navdy.hud.app.util.GenericUtil.checkNotOnMainThread();
            initialized = true;
            android.content.Context a = com.navdy.hud.app.HudApplication.getAppContext();
            android.media.AudioAttributes a0 = new android.media.AudioAttributes$Builder().setUsage(13).setContentType(4).build();
            soundPool = new android.media.SoundPool$Builder().setAudioAttributes(a0).build();
            int i = soundPool.load(a, R.raw.sound_menu_move, 1);
            soundIdMap.put(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_MOVE, Integer.valueOf(i));
            int i0 = soundPool.load(a, R.raw.sound_menu_select, 1);
            soundIdMap.put(com.navdy.hud.app.audio.SoundUtils$Sound.MENU_SELECT, Integer.valueOf(i0));
            int i1 = soundPool.load(a, R.raw.sound_startup, 1);
            soundIdMap.put(com.navdy.hud.app.audio.SoundUtils$Sound.STARTUP, Integer.valueOf(i1));
            int i2 = soundPool.load(a, R.raw.sound_shutdown, 1);
            soundIdMap.put(com.navdy.hud.app.audio.SoundUtils$Sound.SHUTDOWN, Integer.valueOf(i2));
            int i3 = soundPool.load(a, R.raw.sound_alert_positive, 1);
            soundIdMap.put(com.navdy.hud.app.audio.SoundUtils$Sound.ALERT_POSITIVE, Integer.valueOf(i3));
            int i4 = soundPool.load(a, R.raw.sound_alert_negative, 1);
            soundIdMap.put(com.navdy.hud.app.audio.SoundUtils$Sound.ALERT_NEGATIVE, Integer.valueOf(i4));
            sLogger.v("sound pool created");
        }
    }
    
    public static void playSound(com.navdy.hud.app.audio.SoundUtils$Sound a) {
        boolean b = initialized;
        label0: {
            Throwable a0 = null;
            if (!b) {
                break label0;
            }
            try {
                Integer a1 = (Integer)soundIdMap.get(a);
                if (a1 != null) {
                    soundPool.play(a1.intValue(), 1f, 1f, 0, 0, 1f);
                    break label0;
                } else {
                    sLogger.v(new StringBuilder().append("soundId not found:").append(a).toString());
                    break label0;
                }
            } catch(Throwable a2) {
                a0 = a2;
            }
            sLogger.e(a0);
        }
    }
}
