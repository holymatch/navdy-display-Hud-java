package com.navdy.hud.app.service;

final public class ShutdownMonitor$$InjectAdapter extends dagger.internal.Binding implements dagger.MembersInjector {
    private dagger.internal.Binding bus;
    private dagger.internal.Binding mInputManager;
    private dagger.internal.Binding powerManager;
    
    public ShutdownMonitor$$InjectAdapter() {
        super((String)null, "members/com.navdy.hud.app.service.ShutdownMonitor", false, com.navdy.hud.app.service.ShutdownMonitor.class);
    }
    
    public void attach(dagger.internal.Linker a) {
        this.bus = a.requestBinding("com.squareup.otto.Bus", com.navdy.hud.app.service.ShutdownMonitor.class, (this).getClass().getClassLoader());
        this.powerManager = a.requestBinding("com.navdy.hud.app.device.PowerManager", com.navdy.hud.app.service.ShutdownMonitor.class, (this).getClass().getClassLoader());
        this.mInputManager = a.requestBinding("com.navdy.hud.app.manager.InputManager", com.navdy.hud.app.service.ShutdownMonitor.class, (this).getClass().getClassLoader());
    }
    
    public void getDependencies(java.util.Set a, java.util.Set a0) {
        a0.add(this.bus);
        a0.add(this.powerManager);
        a0.add(this.mInputManager);
    }
    
    public void injectMembers(com.navdy.hud.app.service.ShutdownMonitor a) {
        a.bus = (com.squareup.otto.Bus)this.bus.get();
        a.powerManager = (com.navdy.hud.app.device.PowerManager)this.powerManager.get();
        a.mInputManager = (com.navdy.hud.app.manager.InputManager)this.mInputManager.get();
    }
    
    public void injectMembers(Object a) {
        this.injectMembers((com.navdy.hud.app.service.ShutdownMonitor)a);
    }
}
