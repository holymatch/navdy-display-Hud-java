package com.navdy.hud.app.service;

class HudConnectionService$3 extends android.content.BroadcastReceiver {
    private android.bluetooth.BluetoothDevice cancelledDevice;
    private long cancelledTime;
    final com.navdy.hud.app.service.HudConnectionService this$0;
    
    HudConnectionService$3(com.navdy.hud.app.service.HudConnectionService a) {
        super();
        this.this$0 = a;
    }
    
    public void onReceive(android.content.Context a, android.content.Intent a0) {
        String s = a0.getAction();
        android.bluetooth.BluetoothDevice a1 = (android.bluetooth.BluetoothDevice)a0.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
        label0: if (s.equals("android.bluetooth.device.action.BOND_STATE_CHANGED")) {
            int i = a0.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -2147483648);
            com.navdy.hud.app.service.HudConnectionService.access$100(this.this$0).d(new StringBuilder().append("Bond state change - device:").append(a1).append(" state:").append(i).toString());
            if (i == 11) {
                com.navdy.hud.app.service.DeviceSearch a2 = com.navdy.hud.app.service.HudConnectionService.access$200(this.this$0);
                label1: {
                    label2: {
                        if (a2 == null) {
                            break label2;
                        }
                        if (com.navdy.hud.app.service.HudConnectionService.access$200(this.this$0).forgetDevice(a1)) {
                            break label1;
                        }
                    }
                    if (com.navdy.hud.app.service.HudConnectionService.access$300(this.this$0) == null) {
                        break label0;
                    }
                    if (!com.navdy.hud.app.service.HudConnectionService.access$400(this.this$0).getDeviceId().getBluetoothAddress().equals(a1.getAddress())) {
                        break label0;
                    }
                }
                com.navdy.hud.app.service.HudConnectionService.access$500(this.this$0).i("Unexpected bonding with known device - removing bond");
                this.this$0.forgetPairedDevice(a1);
                com.navdy.hud.app.util.BluetoothUtil.cancelBondProcess(a1);
                com.navdy.hud.app.util.BluetoothUtil.removeBond(a1);
                this.cancelledDevice = a1;
                this.cancelledTime = android.os.SystemClock.elapsedRealtime();
                com.navdy.hud.app.service.HudConnectionService.access$700(this.this$0).removeCallbacks(com.navdy.hud.app.service.HudConnectionService.access$600(this.this$0));
                this.this$0.setActiveDevice((com.navdy.service.library.device.RemoteDevice)null);
            }
        } else if (s.equals("android.bluetooth.device.action.PAIRING_REQUEST")) {
            if (a1.equals(this.cancelledDevice) && android.os.SystemClock.elapsedRealtime() - this.cancelledTime < 250L) {
                com.navdy.hud.app.service.HudConnectionService.access$800(this.this$0).d("Aborting pairing request for cancelled bonding");
                this.abortBroadcast();
            }
            this.cancelledDevice = null;
        } else if (s.equals("android.bluetooth.device.action.ACL_DISCONNECTED") && com.navdy.hud.app.service.HudConnectionService.access$200(this.this$0) != null) {
            com.navdy.hud.app.service.HudConnectionService.access$200(this.this$0).handleDisconnect(a1);
        }
    }
}
