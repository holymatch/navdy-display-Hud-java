package com.navdy.hud.app.service;

public class ConnectionServiceAnalyticsSupport {
    public ConnectionServiceAnalyticsSupport() {
    }
    
    static void access$000(String s, boolean b, String[] a) {
        com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.sendEvent(s, b, a);
    }
    
    public static void recordGpsAccuracy(String s, String s0, String s1) {
        String[] a = new String[6];
        a[0] = "Min_Accuracy";
        a[1] = s;
        a[2] = "Max_Accuracy";
        a[3] = s0;
        a[4] = "Average_Accuracy";
        a[5] = s1;
        com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.sendEvent("GPS_Accuracy", a);
    }
    
    public static void recordGpsAcquireLocation(String s, String s0) {
        String[] a = new String[4];
        a[0] = "time";
        a[1] = s;
        a[2] = "accuracy";
        a[3] = s0;
        com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.sendEvent("GPS_Acquired_Location", a);
    }
    
    public static void recordGpsAttemptAcquireLocation() {
        com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.sendEvent("GPS_Attempt_Acquire_Location", new String[0]);
    }
    
    public static void recordGpsLostLocation(String s) {
        String[] a = new String[2];
        a[0] = "time";
        a[1] = s;
        com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.sendEvent("GPS_Lost_Signal", a);
    }
    
    public static void recordNewDevice() {
        com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.sendEvent("Analytics_New_Device", new String[0]);
    }
    
    private static void sendEvent(String s, boolean b, String[] a) {
        android.content.Intent a0 = new android.content.Intent("com.navdy.hud.app.analytics.AnalyticsEvent");
        a0.putExtra("tag", s);
        a0.putExtra("arguments", a);
        if (b) {
            a0.putExtra("include_device_info", true);
        }
        com.navdy.hud.app.HudApplication.getAppContext().sendBroadcastAsUser(a0, android.os.Process.myUserHandle());
    }
    
    private static void sendEvent(String s, String[] a) {
        com.navdy.hud.app.service.ConnectionServiceAnalyticsSupport.sendEvent(s, false, a);
    }
}
