package com.navdy.hud.app.service.pandora.messages;

public class ReturnTrackInfoExtended extends com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage {
    public String album;
    public int albumArtLength;
    public String artist;
    public short duration;
    public short elapsed;
    public byte identityFlags;
    public byte permissionFlags;
    public byte rating;
    public String title;
    public int trackToken;
    
    public ReturnTrackInfoExtended() {
    }
    
    protected static com.navdy.hud.app.service.pandora.messages.BaseIncomingMessage innerBuildFromPayload(byte[] a) {
        java.nio.ByteBuffer a0 = java.nio.ByteBuffer.wrap(a);
        a0.get();
        com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended a1 = new com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended();
        a1.trackToken = a0.getInt();
        a1.albumArtLength = a0.getInt();
        int i = a0.getShort();
        a1.duration = (short)i;
        int i0 = a0.getShort();
        a1.elapsed = (short)i0;
        int i1 = a0.get();
        a1.rating = (byte)i1;
        int i2 = a0.get();
        a1.permissionFlags = (byte)i2;
        int i3 = a0.get();
        a1.identityFlags = (byte)i3;
        try {
            try {
                a1.title = com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended.getString(a0);
                a1.artist = com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended.getString(a0);
                a1.album = com.navdy.hud.app.service.pandora.messages.ReturnTrackInfoExtended.getString(a0);
                return a1;
            } catch(com.navdy.hud.app.service.pandora.exceptions.UnterminatedStringException ignoredException) {
            }
        } catch(com.navdy.hud.app.service.pandora.exceptions.StringOverflowException ignoredException0) {
        }
        throw new com.navdy.hud.app.service.pandora.exceptions.CorruptedPayloadException();
    }
    
    public String toString() {
        return new StringBuilder().append("Got extended track's info for track with token: ").append(this.trackToken).toString();
    }
}
