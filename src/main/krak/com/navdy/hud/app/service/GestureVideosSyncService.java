package com.navdy.hud.app.service;

public class GestureVideosSyncService extends com.navdy.hud.app.service.S3FileUploadService {
    final private static int MAX_GESTURE_VIDEOS_TO_UPLOAD = 100;
    final public static String NAVDY_GESTURE_VIDEOS_BUCKET = "navdy-gesture-videos";
    private static java.util.concurrent.atomic.AtomicBoolean isUploading;
    private static boolean mIsInitialized;
    private static com.navdy.hud.app.service.S3FileUploadService$Request sCurrentRequest;
    private static String sGestureVideosFolder;
    final private static com.navdy.hud.app.service.S3FileUploadService$UploadQueue sGestureVideosUploadQueue;
    private static com.navdy.service.library.log.Logger sLogger;
    @Inject
    com.squareup.otto.Bus bus;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.service.GestureVideosSyncService.class);
        sGestureVideosUploadQueue = new com.navdy.hud.app.service.S3FileUploadService$UploadQueue();
        isUploading = new java.util.concurrent.atomic.AtomicBoolean(false);
        mIsInitialized = false;
    }
    
    public GestureVideosSyncService() {
        super("GESTURE_VIDEOS_SYNC");
    }
    
    public static void addGestureVideoToUploadQueue(java.io.File a, String s) {
        com.navdy.hud.app.service.GestureVideosSyncService.initializeIfNecessary(a);
        synchronized(sGestureVideosUploadQueue) {
            com.navdy.service.library.log.Logger a1 = sLogger;
            a1.d(new StringBuilder().append("Add gesture video to upload ").append(a.getName()).toString());
            sGestureVideosUploadQueue.add(new com.navdy.hud.app.service.S3FileUploadService$Request(a, s));
            sLogger.d(new StringBuilder().append("Queue size : ").append(sGestureVideosUploadQueue.size()).toString());
            if (!isUploading.get() && sGestureVideosUploadQueue.size() > 100) {
                sLogger.d("Number of videos to upload exceeded ");
                sGestureVideosUploadQueue.pop();
            }
            /*monexit(a0)*/;
        }
    }
    
    private static void initializeIfNecessary() {
        com.navdy.hud.app.service.GestureVideosSyncService.initializeIfNecessary((java.io.File)null);
    }
    
    private static void initializeIfNecessary(java.io.File a) {
        Throwable a0 = null;
        label0: synchronized(sGestureVideosUploadQueue) {
        boolean b = mIsInitialized;
            label4: if (!b) {
                java.io.File[] a2 = null;
                java.util.ArrayList a3 = null;
                int i = 0;
                int i0 = 0;
                //try {
                    sLogger.d("Not initialized , initializing now");
                    if (mIsInitialized) {
                        break label4;
                    }
                    sGestureVideosFolder = com.navdy.hud.app.storage.PathManager.getInstance().getGestureVideosSyncFolder();
                    a2 = new java.io.File(sGestureVideosFolder).listFiles();
                    a3 = new java.util.ArrayList();
                    i = a2.length;
                    i0 = 0;
                //} catch(Throwable a4) {
                //    a0 = a4;
                //    break label0;
                //}
                while(i0 < i) {
                    java.io.File a5 = a2[i0];
                    com.navdy.service.library.log.Logger a6 = sLogger;
                    label3: {
                        java.io.File[] a7 = null;
                        int i1 = 0;
                        int i2 = 0;
                        //try {
                            a6.d(new StringBuilder().append("Session directory :").append(a5).toString());
                            if (!a5.isDirectory()) {
                                break label3;
                            }
                            a7 = a5.listFiles();
                            if (a7.length == 0) {
                                sLogger.d(new StringBuilder().append("Remove empty directory:").append(a5).toString());
                                com.navdy.service.library.util.IOUtils.deleteDirectory(com.navdy.hud.app.HudApplication.getAppContext(), a5);
                            }
                            i1 = a7.length;
                            i2 = 0;
                        //} catch(Throwable a8) {
                        //    a0 = a8;
                        //    break label0;
                        //}
                        while(i2 < i1) {
                            java.io.File a9 = a7[i2];
                            com.navdy.service.library.log.Logger a10 = sLogger;
                            //try {
                                a10.d(new StringBuilder().append("Gesture video :").append(a9).toString());
                                label1: if (a9.isFile()) {
                                    label2: {
                                        boolean b0 = false;
                                        if (a == null) {
                                            break label2;
                                        }
                                        try {
                                            b0 = a9.getCanonicalPath().equals(a.getCanonicalPath());
                                        } catch(java.io.IOException ignoredException) {
                                            break label1;
                                        }
                                        if (b0) {
                                            break label1;
                                        }
                                    }
                                    a3.add(a9);
                                }
                                i2 = i2 + 1;
                            //} catch(Throwable a11) {
                            //    a0 = a11;
                            //    break label0;
                            //}
                        }
                    }
                    i0 = i0 + 1;
                }
                //try {
                    sLogger.d(new StringBuilder().append("Number of Gesture videos :").append(a3.size()).toString());
                    com.navdy.hud.app.service.GestureVideosSyncService.populateFilesQueue(a3, sGestureVideosUploadQueue, 100);
                    mIsInitialized = true;
                //} catch(Throwable a12) {
                //    a0 = a12;
                //    break label0;
                //}
            }
            return;
        }
    }
    
    public static void scheduleWithDelay(long j) {
        android.content.Intent a = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.service.GestureVideosSyncService.class);
        a.setAction("SYNC");
        android.app.PendingIntent a0 = android.app.PendingIntent.getService(com.navdy.hud.app.HudApplication.getAppContext(), 123, a, 268435456);
        ((android.app.AlarmManager)com.navdy.hud.app.HudApplication.getAppContext().getSystemService("alarm")).setExact(3, android.os.SystemClock.elapsedRealtime() + j, a0);
    }
    
    public static void syncNow() {
        sLogger.d("synNow");
        android.content.Intent a = new android.content.Intent(com.navdy.hud.app.HudApplication.getAppContext(), com.navdy.hud.app.service.GestureVideosSyncService.class);
        a.setAction("SYNC");
        com.navdy.hud.app.HudApplication.getAppContext().startService(a);
    }
    
    public boolean canCompleteRequest(com.navdy.hud.app.service.S3FileUploadService$Request a) {
        return false;
    }
    
    protected String getAWSBucket() {
        return "navdy-gesture-videos";
    }
    
    protected com.navdy.hud.app.service.S3FileUploadService$Request getCurrentRequest() {
        return sCurrentRequest;
    }
    
    protected java.util.concurrent.atomic.AtomicBoolean getIsUploading() {
        return isUploading;
    }
    
    protected String getKeyPrefix(java.io.File a) {
        String s = a.getParentFile().getName();
        return new StringBuilder().append("archives").append(java.io.File.separator).append(s).toString();
    }
    
    protected com.navdy.service.library.log.Logger getLogger() {
        return sLogger;
    }
    
    protected com.navdy.hud.app.service.S3FileUploadService$UploadQueue getUploadQueue() {
        return sGestureVideosUploadQueue;
    }
    
    protected void initialize() {
        com.navdy.hud.app.service.GestureVideosSyncService.initializeIfNecessary();
    }
    
    public void onCreate() {
        mortar.Mortar.inject(com.navdy.hud.app.HudApplication.getAppContext(), this);
        super.onCreate();
    }
    
    public void reSchedule() {
        sLogger.d("reschedule");
        com.navdy.hud.app.service.GestureVideosSyncService.scheduleWithDelay(10000L);
    }
    
    protected void setCurrentRequest(com.navdy.hud.app.service.S3FileUploadService$Request a) {
        sCurrentRequest = a;
    }
    
    public void sync() {
        com.navdy.hud.app.service.GestureVideosSyncService.syncNow();
    }
    
    protected void uploadFinished(boolean b, String s, String s0) {
        this.bus.post(new com.navdy.hud.app.service.S3FileUploadService$UploadFinished(b, s, s0));
    }
}
