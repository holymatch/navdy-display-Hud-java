package com.navdy.hud.app.service.pandora;

abstract class FrameMessage {
    final protected static byte[] ACK_PAYLOAD;
    final protected static int ADDITIONAL_BYTES_LENGTH = 6;
    final protected static int CRC_BYTES_LENGTH = 2;
    final protected static int FRAME_TYPE_POSITION = 1;
    final protected static byte PNDR_FRAME_END = (byte)124;
    final private static byte PNDR_FRAME_ESCAPE = (byte)125;
    final private static java.util.Map PNDR_FRAME_ESCAPE_MAPPING;
    final protected static byte PNDR_FRAME_SEQUENCE_0 = (byte)0;
    final protected static byte PNDR_FRAME_SEQUENCE_1 = (byte)1;
    final protected static byte PNDR_FRAME_START = (byte)126;
    final protected static byte PNDR_FRAME_TYPE_ACK = (byte)1;
    final protected static byte PNDR_FRAME_TYPE_DATA = (byte)0;
    final private static java.util.Map PNDR_FRAME_UNESCAPE_MAPPING;
    protected boolean isSequence0;
    protected byte[] payload;
    
    static {
        ACK_PAYLOAD = new byte[0];
        PNDR_FRAME_ESCAPE_MAPPING = (java.util.Map)new com.navdy.hud.app.service.pandora.FrameMessage$1();
        PNDR_FRAME_UNESCAPE_MAPPING = (java.util.Map)new java.util.HashMap(PNDR_FRAME_ESCAPE_MAPPING.size());
        Object a = PNDR_FRAME_ESCAPE_MAPPING.entrySet().iterator();
        while(((java.util.Iterator)a).hasNext()) {
            Object a0 = ((java.util.Iterator)a).next();
            PNDR_FRAME_UNESCAPE_MAPPING.put(((java.util.Map.Entry)a0).getValue(), ((java.util.Map.Entry)a0).getKey());
        }
    }
    
    protected FrameMessage(boolean b, byte[] a) {
        this.isSequence0 = b;
        this.payload = a;
    }
    
    protected static byte[] buildFrameFromCRCPart(java.nio.ByteBuffer a) {
        java.nio.ByteBuffer a0 = com.navdy.hud.app.service.pandora.CRC16CCITT.calculate(a);
        byte[] a1 = com.navdy.hud.app.service.pandora.FrameMessage.escapeBytes(a);
        byte[] a2 = com.navdy.hud.app.service.pandora.FrameMessage.escapeBytes(a0);
        return java.nio.ByteBuffer.allocate(a1.length + a2.length + 2).put((byte)126).put(a1).put(a2).put((byte)124).array();
    }
    
    protected static byte[] escapeBytes(java.nio.ByteBuffer a) {
        java.io.ByteArrayOutputStream a0 = new java.io.ByteArrayOutputStream();
        int i = 0;
        while(i < a.limit()) {
            int i0 = a.get(i);
            Byte a1 = (Byte)PNDR_FRAME_ESCAPE_MAPPING.get(Byte.valueOf((byte)i0));
            if (a1 == null) {
                a0.write(i0);
            } else {
                a0.write(125);
                int i1 = a1.byteValue();
                a0.write(i1);
            }
            i = i + 1;
        }
        return a0.toByteArray();
    }
    
    public static com.navdy.hud.app.service.pandora.FrameMessage parseFrame(byte[] a) {
        com.navdy.hud.app.service.pandora.FrameMessage a0 = null;
        int i = a[1];
        if (i != 1) {
            if (i != 0) {
                StringBuilder a1 = new StringBuilder().append("Unknown message frame type: ");
                Object[] a2 = new Object[1];
                a2[0] = Byte.valueOf((byte)i);
                throw new IllegalArgumentException(a1.append(String.format("%02X", a2)).toString());
            }
            a0 = com.navdy.hud.app.service.pandora.DataFrameMessage.parseDataFrame(a);
        } else {
            a0 = com.navdy.hud.app.service.pandora.AckFrameMessage.parseAckFrame(a);
        }
        return a0;
    }
    
    protected static java.nio.ByteBuffer unescapeBytes(byte[] a) {
        java.io.ByteArrayOutputStream a0 = new java.io.ByteArrayOutputStream();
        int i = a.length;
        int i0 = 0;
        while(true) {
            if (i0 >= i) {
                return java.nio.ByteBuffer.wrap(a0.toByteArray());
            }
            int i1 = a[i0];
            label0: {
                label1: {
                    if (i1 == 125) {
                        try {
                            try {
                                i0 = i0 + 1;
                                java.util.Map a1 = PNDR_FRAME_UNESCAPE_MAPPING;
                                int i2 = a[i0];
                                i1 = ((Byte)a1.get(Byte.valueOf((byte)i2))).byteValue();
                            } catch(ArrayIndexOutOfBoundsException ignoredException) {
                                break label1;
                            }
                        } catch(NullPointerException ignoredException0) {
                            break label0;
                        }
                    }
                    a0.write(i1);
                    i0 = i0 + 1;
                    continue;
                }
                throw new IllegalArgumentException("Escape byte not followed by a byte");
            }
            throw new IllegalArgumentException("Escape byte not followed by a proper byte");
        }
    }
    
    abstract public byte[] buildFrame();
}
