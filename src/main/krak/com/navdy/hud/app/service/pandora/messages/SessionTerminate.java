package com.navdy.hud.app.service.pandora.messages;

public class SessionTerminate extends com.navdy.hud.app.service.pandora.messages.BaseOutgoingEmptyMessage {
    final public static com.navdy.hud.app.service.pandora.messages.SessionTerminate INSTANCE;
    
    static {
        INSTANCE = new com.navdy.hud.app.service.pandora.messages.SessionTerminate();
    }
    
    private SessionTerminate() {
    }
    
    public byte[] buildPayload() {
        return super.buildPayload();
    }
    
    protected byte getMessageType() {
        return (byte)5;
    }
    
    public String toString() {
        return "Session Terminate";
    }
}
