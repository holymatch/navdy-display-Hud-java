package com.navdy.hud.app.settings;

final class MainScreenSettings$ScreenConfiguration$1 implements android.os.Parcelable$Creator {
    MainScreenSettings$ScreenConfiguration$1() {
    }
    
    public com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration createFromParcel(android.os.Parcel a) {
        return new com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration(a);
    }
    
    public Object createFromParcel(android.os.Parcel a) {
        return this.createFromParcel(a);
    }
    
    public com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration[] newArray(int i) {
        return new com.navdy.hud.app.settings.MainScreenSettings$ScreenConfiguration[i];
    }
    
    public Object[] newArray(int i) {
        return this.newArray(i);
    }
}
