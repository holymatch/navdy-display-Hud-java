package com.navdy.hud.app.debug;

class RouteRecorder$2 implements Runnable {
    final com.navdy.hud.app.debug.RouteRecorder this$0;
    
    RouteRecorder$2(com.navdy.hud.app.debug.RouteRecorder a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        java.util.List a = com.navdy.hud.app.debug.RouteRecorder.access$200(this.this$0);
        label2: {
            label3: {
                if (a == null) {
                    break label3;
                }
                if (com.navdy.hud.app.debug.RouteRecorder.access$200(this.this$0).size() <= com.navdy.hud.app.debug.RouteRecorder.access$300(this.this$0)) {
                    break label3;
                }
                com.navdy.service.library.events.location.Coordinate a0 = new com.navdy.service.library.events.location.Coordinate$Builder((com.navdy.service.library.events.location.Coordinate)com.navdy.hud.app.debug.RouteRecorder.access$200(this.this$0).get(com.navdy.hud.app.debug.RouteRecorder.access$300(this.this$0))).timestamp(Long.valueOf(System.currentTimeMillis())).build();
                com.navdy.hud.app.debug.RouteRecorder.access$400(this.this$0).feedLocation(a0);
                if (com.navdy.hud.app.debug.RouteRecorder.access$200(this.this$0).size() <= com.navdy.hud.app.debug.RouteRecorder.access$300(this.this$0) + 1) {
                    if (com.navdy.hud.app.debug.RouteRecorder.access$600(this.this$0)) {
                        com.navdy.hud.app.debug.RouteRecorder.access$302(this.this$0, 0);
                        com.navdy.hud.app.debug.RouteRecorder.access$500(this.this$0).post(com.navdy.hud.app.debug.RouteRecorder.access$700(this.this$0));
                        break label2;
                    } else {
                        this.this$0.stopPlayback();
                        break label2;
                    }
                } else {
                    long j = ((com.navdy.service.library.events.location.Coordinate)com.navdy.hud.app.debug.RouteRecorder.access$200(this.this$0).get(com.navdy.hud.app.debug.RouteRecorder.access$300(this.this$0) + 1)).timestamp.longValue();
                    long j0 = ((com.navdy.service.library.events.location.Coordinate)com.navdy.hud.app.debug.RouteRecorder.access$200(this.this$0).get(com.navdy.hud.app.debug.RouteRecorder.access$300(this.this$0))).timestamp.longValue();
                    com.navdy.hud.app.debug.RouteRecorder.access$308(this.this$0);
                    com.navdy.hud.app.debug.RouteRecorder.access$500(this.this$0).postDelayed((Runnable)this, j - j0);
                    break label2;
                }
            }
            boolean b = com.navdy.hud.app.debug.RouteRecorder.access$600(this.this$0);
            label0: {
                label1: {
                    if (!b) {
                        break label1;
                    }
                    if (com.navdy.hud.app.debug.RouteRecorder.access$200(this.this$0) != null) {
                        break label0;
                    }
                }
                this.this$0.stopPlayback();
                break label2;
            }
            com.navdy.hud.app.debug.RouteRecorder.access$302(this.this$0, 0);
            com.navdy.hud.app.debug.RouteRecorder.access$500(this.this$0).post(com.navdy.hud.app.debug.RouteRecorder.access$700(this.this$0));
        }
    }
}
