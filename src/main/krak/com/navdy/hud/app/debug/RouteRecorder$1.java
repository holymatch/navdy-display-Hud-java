package com.navdy.hud.app.debug;

class RouteRecorder$1 implements Runnable {
    final com.navdy.hud.app.debug.RouteRecorder this$0;
    
    RouteRecorder$1(com.navdy.hud.app.debug.RouteRecorder a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        if (com.navdy.hud.app.debug.RouteRecorder.access$000(this.this$0).isProviderEnabled("NAVDY_GPS_PROVIDER")) {
            com.navdy.hud.app.debug.RouteRecorder.access$000(this.this$0).requestLocationUpdates("NAVDY_GPS_PROVIDER", 0L, 0.0f, com.navdy.hud.app.debug.RouteRecorder.access$100(this.this$0));
        }
        if (com.navdy.hud.app.debug.RouteRecorder.access$000(this.this$0).isProviderEnabled("network")) {
            com.navdy.hud.app.debug.RouteRecorder.access$000(this.this$0).requestLocationUpdates("network", 0L, 0.0f, com.navdy.hud.app.debug.RouteRecorder.access$100(this.this$0));
        }
    }
}
