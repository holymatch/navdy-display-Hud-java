package com.navdy.hud.app.debug;

class DriveRecorder$9 implements Runnable {
    final com.navdy.hud.app.debug.DriveRecorder this$0;
    
    DriveRecorder$9(com.navdy.hud.app.debug.DriveRecorder a) {
        super();
        this.this$0 = a;
    }
    
    public void run() {
        com.navdy.hud.app.debug.DriveRecorder.sLogger.d("Auto recording is enabled so starting the auto record");
        java.io.File[] a = new java.io.File(new StringBuilder().append(com.navdy.hud.app.storage.PathManager.getInstance().getMapsPartitionPath()).append(java.io.File.separator).append("drive_logs").toString()).listFiles();
        if (a != null && a.length > 20) {
            java.util.Arrays.sort((Object[])a, (java.util.Comparator)new com.navdy.hud.app.debug.DriveRecorder$FilesModifiedTimeComparator());
            int i = 0;
            while(i < a.length - 20) {
                java.io.File a0 = a[i];
                com.navdy.hud.app.debug.DriveRecorder.sLogger.d(new StringBuilder().append("Deleting the drive record ").append(a0.getAbsolutePath()).append(" as its old ").toString());
                com.navdy.service.library.util.IOUtils.deleteFile(com.navdy.hud.app.HudApplication.getAppContext(), a0.getAbsolutePath());
                i = i + 1;
            }
        }
        com.navdy.hud.app.debug.DriveRecorder.access$1600(this.this$0).sendLocalMessage((com.squareup.wire.Message)new com.navdy.service.library.events.debug.StartDriveRecordingEvent("auto"));
    }
}
