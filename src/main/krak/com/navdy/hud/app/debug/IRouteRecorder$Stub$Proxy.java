package com.navdy.hud.app.debug;

class IRouteRecorder$Stub$Proxy implements com.navdy.hud.app.debug.IRouteRecorder {
    private android.os.IBinder mRemote;
    
    IRouteRecorder$Stub$Proxy(android.os.IBinder a) {
        this.mRemote = a;
    }
    
    public android.os.IBinder asBinder() {
        return this.mRemote;
    }
    
    public String getInterfaceDescriptor() {
        return "com.navdy.hud.app.debug.IRouteRecorder";
    }
    
    public boolean isPaused() {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
            this.mRemote.transact(11, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        boolean b = i != 0;
        a0.recycle();
        a.recycle();
        return b;
    }
    
    public boolean isPlaying() {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
            this.mRemote.transact(10, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        boolean b = i != 0;
        a0.recycle();
        a.recycle();
        return b;
    }
    
    public boolean isRecording() {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
            this.mRemote.transact(4, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        boolean b = i != 0;
        a0.recycle();
        a.recycle();
        return b;
    }
    
    public boolean isStopped() {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
            this.mRemote.transact(12, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        boolean b = i != 0;
        a0.recycle();
        a.recycle();
        return b;
    }
    
    public void pausePlayback() {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
            this.mRemote.transact(6, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void prepare(String s, boolean b) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
            a.writeString(s);
            a.writeInt(b ? 1 : 0);
            this.mRemote.transact(1, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public boolean restartPlayback() {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
            this.mRemote.transact(9, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        boolean b = i != 0;
        a0.recycle();
        a.recycle();
        return b;
    }
    
    public void resumePlayback() {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
            this.mRemote.transact(7, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void startPlayback(String s, boolean b, boolean b0) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
            a.writeString(s);
            a.writeInt(b ? 1 : 0);
            a.writeInt(b0 ? 1 : 0);
            this.mRemote.transact(5, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public String startRecording(String s) {
        String s0 = null;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
            a.writeString(s);
            this.mRemote.transact(2, a, a0, 0);
            a0.readException();
            s0 = a0.readString();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return s0;
    }
    
    public void stopPlayback() {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
            this.mRemote.transact(8, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void stopRecording() {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.hud.app.debug.IRouteRecorder");
            this.mRemote.transact(3, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
}
