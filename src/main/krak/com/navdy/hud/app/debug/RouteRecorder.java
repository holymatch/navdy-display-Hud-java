package com.navdy.hud.app.debug;

public class RouteRecorder {
    final private static java.text.SimpleDateFormat DATE_FORMAT;
    final public static String SECONDARY_LOCATION_TAG = "[secondary] ";
    final private static com.navdy.hud.app.debug.RouteRecorder sInstance;
    final public static com.navdy.service.library.log.Logger sLogger;
    private com.navdy.hud.app.service.HudConnectionService connectionService;
    final private android.content.Context context;
    private volatile int currentInjectionIndex;
    private java.util.List currentLocations;
    private java.io.BufferedWriter driveLogWriter;
    final private com.navdy.hud.app.device.gps.GpsManager gpsManager;
    final private android.os.Handler handler;
    final private Runnable injectFakeLocationRunnable;
    private volatile boolean isLooping;
    private volatile boolean isRecording;
    boolean lastReportedDST;
    private java.util.TimeZone lastReportedTimeZone;
    final private android.location.LocationListener locationListener;
    final private android.location.LocationManager locationManager;
    final private com.navdy.hud.app.storage.PathManager pathManager;
    private com.navdy.hud.app.debug.DriveRecorder$State playbackState;
    private volatile boolean prepared;
    private volatile long preparedFileLastModifiedTime;
    private volatile String preparedFileName;
    private String recordingLabel;
    private Runnable startLocationUpdates;
    
    static {
        sLogger = new com.navdy.service.library.log.Logger(com.navdy.hud.app.debug.RouteRecorder.class);
        DATE_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd'_'HH_mm_ss.SSS", java.util.Locale.US);
        sInstance = new com.navdy.hud.app.debug.RouteRecorder();
    }
    
    private RouteRecorder() {
        this.lastReportedTimeZone = null;
        this.lastReportedDST = false;
        this.playbackState = com.navdy.hud.app.debug.DriveRecorder$State.STOPPED;
        this.startLocationUpdates = (Runnable)new com.navdy.hud.app.debug.RouteRecorder$1(this);
        this.injectFakeLocationRunnable = (Runnable)new com.navdy.hud.app.debug.RouteRecorder$2(this);
        this.locationListener = (android.location.LocationListener)new com.navdy.hud.app.debug.RouteRecorder$3(this);
        this.context = com.navdy.hud.app.HudApplication.getAppContext();
        this.locationManager = (android.location.LocationManager)this.context.getSystemService("location");
        this.gpsManager = com.navdy.hud.app.device.gps.GpsManager.getInstance();
        this.handler = new android.os.Handler(android.os.Looper.getMainLooper());
        this.pathManager = com.navdy.hud.app.storage.PathManager.getInstance();
        this.isRecording = false;
        this.currentInjectionIndex = 0;
    }
    
    static android.location.LocationManager access$000(com.navdy.hud.app.debug.RouteRecorder a) {
        return a.locationManager;
    }
    
    static android.location.LocationListener access$100(com.navdy.hud.app.debug.RouteRecorder a) {
        return a.locationListener;
    }
    
    static com.navdy.hud.app.storage.PathManager access$1000(com.navdy.hud.app.debug.RouteRecorder a) {
        return a.pathManager;
    }
    
    static com.navdy.hud.app.service.HudConnectionService access$1100(com.navdy.hud.app.debug.RouteRecorder a) {
        return a.connectionService;
    }
    
    static java.io.BufferedWriter access$1200(com.navdy.hud.app.debug.RouteRecorder a) {
        return a.driveLogWriter;
    }
    
    static java.io.BufferedWriter access$1202(com.navdy.hud.app.debug.RouteRecorder a, java.io.BufferedWriter a0) {
        a.driveLogWriter = a0;
        return a0;
    }
    
    static Runnable access$1300(com.navdy.hud.app.debug.RouteRecorder a) {
        return a.startLocationUpdates;
    }
    
    static boolean access$1400(com.navdy.hud.app.debug.RouteRecorder a) {
        return a.isRecording;
    }
    
    static java.util.List access$200(com.navdy.hud.app.debug.RouteRecorder a) {
        return a.currentLocations;
    }
    
    static int access$300(com.navdy.hud.app.debug.RouteRecorder a) {
        return a.currentInjectionIndex;
    }
    
    static int access$302(com.navdy.hud.app.debug.RouteRecorder a, int i) {
        a.currentInjectionIndex = i;
        return i;
    }
    
    static int access$308(com.navdy.hud.app.debug.RouteRecorder a) {
        int i = a.currentInjectionIndex;
        a.currentInjectionIndex = i + 1;
        return i;
    }
    
    static com.navdy.hud.app.device.gps.GpsManager access$400(com.navdy.hud.app.debug.RouteRecorder a) {
        return a.gpsManager;
    }
    
    static android.os.Handler access$500(com.navdy.hud.app.debug.RouteRecorder a) {
        return a.handler;
    }
    
    static boolean access$600(com.navdy.hud.app.debug.RouteRecorder a) {
        return a.isLooping;
    }
    
    static Runnable access$700(com.navdy.hud.app.debug.RouteRecorder a) {
        return a.injectFakeLocationRunnable;
    }
    
    static void access$800(com.navdy.hud.app.debug.RouteRecorder a, android.location.Location a0, boolean b) {
        a.persistLocationToFile(a0, b);
    }
    
    static void access$900(com.navdy.hud.app.debug.RouteRecorder a, boolean b) {
        a.bStopRecording(b);
    }
    
    private void bStopRecording(boolean b) {
        com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)this.driveLogWriter);
        this.isRecording = false;
        this.recordingLabel = null;
        if (!b) {
            this.connectionService.sendMessage((com.squareup.wire.Message)new com.navdy.service.library.events.debug.StopDriveRecordingResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS));
        }
        sLogger.v("recording stopped");
    }
    
    private Runnable createDriveRecord(String s, boolean b) {
        return (Runnable)new com.navdy.hud.app.debug.RouteRecorder$8(this, s, b);
    }
    
    public static com.navdy.hud.app.debug.RouteRecorder getInstance() {
        return sInstance;
    }
    
    private void persistLocationToFile(android.location.Location a, boolean b) {
        if (this.isRecording) {
            String s = null;
            String s0 = a.getProvider();
            if ("NAVDY_GPS_PROVIDER".equals(s0)) {
                s0 = "gps";
            }
            java.util.TimeZone a0 = java.util.TimeZone.getDefault();
            boolean b0 = a0.inDaylightTime(new java.util.Date());
            boolean b1 = (a0).equals(this.lastReportedTimeZone);
            label0: {
                label1: {
                    if (!b1) {
                        break label1;
                    }
                    if (b0 == this.lastReportedDST) {
                        s = "";
                        break label0;
                    }
                }
                String s1 = a0.getID();
                String s2 = b0 ? "T" : "F";
                Object[] a1 = new Object[2];
                a1[0] = s1;
                a1[1] = s2;
                s = String.format("#zone=\"%s\", dst=%s\n", a1);
                this.lastReportedTimeZone = a0;
                this.lastReportedDST = b0;
                com.navdy.service.library.log.Logger a2 = sLogger;
                Object[] a3 = new Object[2];
                a3[0] = a0.getID();
                a3[1] = Boolean.valueOf(b0);
                a2.v(String.format("setting zoneinfo:  %s dst=%b", a3));
            }
            String s3 = new StringBuilder().append(b ? "[secondary] " : "").append(a.getLatitude()).append(",").append(a.getLongitude()).append(",").append(a.getBearing()).append(",").append(a.getSpeed()).append(",").append(a.getAccuracy()).append(",").append(a.getAltitude()).append(",").append(a.getTime()).append(",").append(s0).toString();
            com.navdy.service.library.task.TaskManager.getInstance().execute(this.writeLocation(new StringBuilder().append(s).append(s3).toString()), 9);
        }
    }
    
    private Runnable writeLocation(String s) {
        return (Runnable)new com.navdy.hud.app.debug.RouteRecorder$9(this, s);
    }
    
    private Runnable writeMarker(String s) {
        return (Runnable)new com.navdy.hud.app.debug.RouteRecorder$10(this, s);
    }
    
    public boolean bPrepare(String s, boolean b) {
        boolean b0 = false;
        label5: synchronized(this) {
            java.io.BufferedReader a = null;
            java.io.File a0 = new java.io.File(com.navdy.hud.app.debug.DriveRecorder.getDriveLogsDir(s), s);
            long j = a0.lastModified();
            boolean b1 = android.text.TextUtils.equals((CharSequence)s, (CharSequence)this.preparedFileName);
            label6: {
                if (!b1) {
                    break label6;
                }
                if (this.preparedFileLastModifiedTime != j) {
                    break label6;
                }
                sLogger.d("Already prepared, ready for playback");
                b0 = true;
                break label5;
            }
            sLogger.d(new StringBuilder().append("Preparing for playback from file : ").append(s).toString());
            this.preparedFileName = s;
            this.preparedFileLastModifiedTime = j;
            label1: {
                label4: {
                    java.io.BufferedReader a1 = null;
                    Throwable a2 = null;
                    try {
                        a1 = null;
                        a1 = null;
                        a1 = null;
                        label0: {
                            Exception a3 = null;
                            label3: {
                                label2: {
                                    try {
                                        a1 = null;
                                        java.io.FileInputStream a4 = new java.io.FileInputStream(a0);
                                        a1 = null;
                                        java.io.InputStreamReader a5 = new java.io.InputStreamReader((java.io.InputStream)a4, "utf-8");
                                        a1 = null;
                                        a = new java.io.BufferedReader((java.io.Reader)a5);
                                        break label2;
                                    } catch(Exception a6) {
                                        a3 = a6;
                                    }
                                    a = null;
                                    break label3;
                                }
                                try {
                                    try {
                                        this.currentLocations = (java.util.List)new java.util.ArrayList();
                                        while(true) {
                                            String s0 = a.readLine();
                                            if (s0 == null) {
                                                break label1;
                                            }
                                            if (!s0.startsWith("#")) {
                                                int i = s0.indexOf("[secondary] ");
                                                boolean b2 = i >= 0;
                                                if (b2) {
                                                    s0 = s0.substring("[secondary] ".length() + i);
                                                }
                                                if (b) {
                                                    if (!b2) {
                                                        continue;
                                                    }
                                                } else if (b2) {
                                                    continue;
                                                }
                                                String[] a7 = s0.split(",");
                                                if (a7.length != 8) {
                                                    if (a7.length == 6) {
                                                        com.navdy.service.library.events.location.Coordinate a8 = new com.navdy.service.library.events.location.Coordinate$Builder().latitude(Double.valueOf(Double.parseDouble(a7[0]))).longitude(Double.valueOf(Double.parseDouble(a7[1]))).bearing(Float.valueOf(Float.parseFloat(a7[2]))).speed(Float.valueOf(Float.parseFloat(a7[3]))).accuracy(Float.valueOf(1f)).altitude(Double.valueOf(0.0)).timestamp(Long.valueOf(Long.parseLong(a7[4]))).build();
                                                        this.currentLocations.add(a8);
                                                    }
                                                } else {
                                                    com.navdy.service.library.events.location.Coordinate a9 = new com.navdy.service.library.events.location.Coordinate$Builder().latitude(Double.valueOf(Double.parseDouble(a7[0]))).longitude(Double.valueOf(Double.parseDouble(a7[1]))).bearing(Float.valueOf(Float.parseFloat(a7[2]))).speed(Float.valueOf(Float.parseFloat(a7[3]))).accuracy(Float.valueOf(Float.parseFloat(a7[4]))).altitude(Double.valueOf(Double.parseDouble(a7[5]))).timestamp(Long.valueOf(Long.parseLong(a7[6]))).build();
                                                    this.currentLocations.add(a9);
                                                }
                                            }
                                        }
                                    } catch(Exception a10) {
                                        a3 = a10;
                                    }
                                } catch(Throwable a11) {
                                    a2 = a11;
                                    break label0;
                                }
                            }
                            com.navdy.service.library.log.Logger a12 = sLogger;
                            a1 = a;
                            a12.e("Error parsing the file, prepare failed", (Throwable)a3);
                            break label4;
                        }
                        a1 = a;
                    } catch(Throwable a13) {
                        a2 = a13;
                    }
                    com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a1);
                    throw a2;
                }
                com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
                b0 = false;
                break label5;
            }
            com.navdy.service.library.util.IOUtils.closeStream((java.io.Closeable)a);
            b0 = true;
        }
        /*monexit(this)*/;
        return b0;
    }
    
    public String getLabel() {
        return this.recordingLabel;
    }
    
    public void injectLocation(android.location.Location a, boolean b) {
        this.persistLocationToFile(a, b);
    }
    
    public void injectMarker(String s) {
        if (this.isRecording) {
            com.navdy.service.library.task.TaskManager.getInstance().execute(this.writeMarker(s), 9);
        }
    }
    
    public boolean isPaused() {
        return this.playbackState == com.navdy.hud.app.debug.DriveRecorder$State.PAUSED;
    }
    
    public boolean isPlaying() {
        return this.playbackState == com.navdy.hud.app.debug.DriveRecorder$State.PLAYING;
    }
    
    public boolean isRecording() {
        return this.isRecording;
    }
    
    public boolean isStopped() {
        return this.playbackState == com.navdy.hud.app.debug.DriveRecorder$State.STOPPED;
    }
    
    public void pausePlayback() {
        if (this.isPlaying()) {
            this.handler.removeCallbacks(this.injectFakeLocationRunnable);
            this.playbackState = com.navdy.hud.app.debug.DriveRecorder$State.PAUSED;
            this.gpsManager.startUbloxReporting();
            this.connectionService.setSimulatingGpsCoordinates(false);
        } else {
            sLogger.v("already stopped, no-op");
        }
    }
    
    public void prepare(String s, boolean b) {
        boolean b0 = this.isPlaying();
        label2: {
            label0: {
                label1: {
                    if (b0) {
                        break label1;
                    }
                    if (!this.isPaused()) {
                        break label0;
                    }
                }
                sLogger.e(new StringBuilder().append("Playback is not stopped , current state ").append(this.playbackState.name()).toString());
                break label2;
            }
            com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.debug.RouteRecorder$6(this, s, b), 9);
        }
    }
    
    public void release() {
        synchronized(this) {
            if (this.currentLocations != null) {
                this.currentLocations.clear();
                this.currentLocations = null;
            }
            this.preparedFileName = null;
            this.preparedFileLastModifiedTime = -1L;
        }
        /*monexit(this)*/;
    }
    
    public void requestRecordings() {
        com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.debug.RouteRecorder$5(this), 1);
    }
    
    public boolean restartPlayback() {
        boolean b = false;
        boolean b0 = this.isPlaying();
        label0: {
            label1: {
                if (b0) {
                    break label1;
                }
                if (!this.isPaused()) {
                    b = false;
                    break label0;
                }
            }
            if (this.isPaused()) {
                this.gpsManager.stopUbloxReporting();
                this.connectionService.setSimulatingGpsCoordinates(false);
            }
            this.playbackState = com.navdy.hud.app.debug.DriveRecorder$State.PLAYING;
            this.currentInjectionIndex = 0;
            this.handler.removeCallbacks(this.injectFakeLocationRunnable);
            this.handler.post(this.injectFakeLocationRunnable);
            b = true;
        }
        return b;
    }
    
    public void resumePlayback() {
        if (this.isPaused()) {
            this.gpsManager.stopUbloxReporting();
            this.playbackState = com.navdy.hud.app.debug.DriveRecorder$State.PLAYING;
            this.connectionService.setSimulatingGpsCoordinates(true);
            this.handler.post(this.injectFakeLocationRunnable);
        }
    }
    
    public void setConnectionService(com.navdy.hud.app.service.HudConnectionService a) {
        this.connectionService = a;
    }
    
    public void startPlayback(String s, boolean b, boolean b0) {
        this.startPlayback(s, b, false, b0);
    }
    
    public void startPlayback(String s, boolean b, boolean b0, boolean b1) {
        boolean b2 = this.isPlaying();
        label1: {
            label0: {
                if (b2) {
                    break label0;
                }
                if (this.isRecording) {
                    break label0;
                }
                this.playbackState = com.navdy.hud.app.debug.DriveRecorder$State.PLAYING;
                this.isLooping = b0;
                this.gpsManager.stopUbloxReporting();
                this.connectionService.setSimulatingGpsCoordinates(true);
                com.navdy.service.library.task.TaskManager.getInstance().execute((Runnable)new com.navdy.hud.app.debug.RouteRecorder$7(this, s, b, b1), 1);
                break label1;
            }
            sLogger.v("already busy, no-op");
            if (!b1) {
                this.connectionService.sendMessage((com.squareup.wire.Message)new com.navdy.service.library.events.debug.StartDrivePlaybackResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_STATE));
            }
        }
    }
    
    public String startRecording(String s, boolean b) {
        String s0 = null;
        boolean b0 = this.isRecording;
        label1: {
            label0: {
                if (b0) {
                    break label0;
                }
                if (this.isPlaying()) {
                    break label0;
                }
                sLogger.d("Starting the drive recording");
                this.isRecording = true;
                this.recordingLabel = s;
                String s1 = DATE_FORMAT.format(new java.util.Date());
                s0 = new StringBuilder().append(com.navdy.hud.app.util.GenericUtil.normalizeToFilename(s)).append("_").append(s1).append(".log").toString();
                com.navdy.service.library.task.TaskManager.getInstance().execute(this.createDriveRecord(s0, b), 9);
                break label1;
            }
            sLogger.v("already busy");
            if (!b) {
                this.connectionService.sendMessage((com.squareup.wire.Message)new com.navdy.service.library.events.debug.StartDriveRecordingResponse(com.navdy.service.library.events.RequestStatus.REQUEST_INVALID_STATE));
            }
            s0 = null;
        }
        return s0;
    }
    
    public void stopPlayback() {
        if (this.isStopped()) {
            sLogger.v("already stopped, no-op");
        } else {
            this.handler.removeCallbacks(this.injectFakeLocationRunnable);
            this.currentInjectionIndex = 0;
            this.playbackState = com.navdy.hud.app.debug.DriveRecorder$State.STOPPED;
            this.gpsManager.startUbloxReporting();
            this.isLooping = false;
            this.connectionService.setSimulatingGpsCoordinates(false);
        }
    }
    
    public void stopRecording(boolean b) {
        if (this.isRecording) {
            sLogger.d("Stopping the drive recording");
            this.handler.post((Runnable)new com.navdy.hud.app.debug.RouteRecorder$4(this, b));
        } else {
            sLogger.v("already stopped, no-op");
        }
    }
}
