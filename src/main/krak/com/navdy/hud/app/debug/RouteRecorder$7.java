package com.navdy.hud.app.debug;

class RouteRecorder$7 implements Runnable {
    final com.navdy.hud.app.debug.RouteRecorder this$0;
    final String val$fileName;
    final boolean val$local;
    final boolean val$secondary;
    
    RouteRecorder$7(com.navdy.hud.app.debug.RouteRecorder a, String s, boolean b, boolean b0) {
        super();
        this.this$0 = a;
        this.val$fileName = s;
        this.val$secondary = b;
        this.val$local = b0;
    }
    
    public void run() {
        try {
            if (this.this$0.bPrepare(this.val$fileName, this.val$secondary)) {
                com.navdy.hud.app.debug.RouteRecorder.sLogger.d("Prepare succeeded");
                if (!this.val$local) {
                    com.navdy.hud.app.debug.RouteRecorder.access$1100(this.this$0).sendMessage((com.squareup.wire.Message)new com.navdy.service.library.events.debug.StartDrivePlaybackResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS));
                }
                com.navdy.hud.app.debug.RouteRecorder.sLogger.d("Starting playback");
                com.navdy.hud.app.debug.RouteRecorder.access$302(this.this$0, 0);
                com.navdy.hud.app.debug.RouteRecorder.access$500(this.this$0).post(com.navdy.hud.app.debug.RouteRecorder.access$700(this.this$0));
            } else {
                if (!this.val$local) {
                    com.navdy.hud.app.debug.RouteRecorder.access$1100(this.this$0).sendMessage((com.squareup.wire.Message)new com.navdy.service.library.events.debug.StartDrivePlaybackResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR));
                }
                this.this$0.stopPlayback();
            }
        } catch(Throwable a) {
            if (!this.val$local) {
                com.navdy.hud.app.debug.RouteRecorder.access$1100(this.this$0).sendMessage((com.squareup.wire.Message)new com.navdy.service.library.events.debug.StartDrivePlaybackResponse(com.navdy.service.library.events.RequestStatus.REQUEST_SERVICE_ERROR));
            }
            this.this$0.stopPlayback();
            com.navdy.hud.app.debug.RouteRecorder.sLogger.e(a);
        }
    }
}
