package com.navdy.hud.app.debug;

abstract public class IRouteRecorder$Stub extends android.os.Binder implements com.navdy.hud.app.debug.IRouteRecorder {
    final private static String DESCRIPTOR = "com.navdy.hud.app.debug.IRouteRecorder";
    final static int TRANSACTION_isPaused = 11;
    final static int TRANSACTION_isPlaying = 10;
    final static int TRANSACTION_isRecording = 4;
    final static int TRANSACTION_isStopped = 12;
    final static int TRANSACTION_pausePlayback = 6;
    final static int TRANSACTION_prepare = 1;
    final static int TRANSACTION_restartPlayback = 9;
    final static int TRANSACTION_resumePlayback = 7;
    final static int TRANSACTION_startPlayback = 5;
    final static int TRANSACTION_startRecording = 2;
    final static int TRANSACTION_stopPlayback = 8;
    final static int TRANSACTION_stopRecording = 3;
    
    public IRouteRecorder$Stub() {
        this.attachInterface((android.os.IInterface)this, "com.navdy.hud.app.debug.IRouteRecorder");
    }
    
    public static com.navdy.hud.app.debug.IRouteRecorder asInterface(android.os.IBinder a) {
        Object a0 = null;
        label1: if (a != null) {
            android.os.IInterface a1 = a.queryLocalInterface("com.navdy.hud.app.debug.IRouteRecorder");
            label0: {
                if (a1 == null) {
                    break label0;
                }
                if (!(a1 instanceof com.navdy.hud.app.debug.IRouteRecorder)) {
                    break label0;
                }
                a0 = a1;
                break label1;
            }
            a0 = new com.navdy.hud.app.debug.IRouteRecorder$Stub$Proxy(a);
        } else {
            a0 = null;
        }
        return (com.navdy.hud.app.debug.IRouteRecorder)a0;
    }
    
    public android.os.IBinder asBinder() {
        return (android.os.IBinder)this;
    }
    
    public boolean onTransact(int i, android.os.Parcel a, android.os.Parcel a0, int i0) {
        boolean b = false;
        switch(i) {
            case 1598968902: {
                a0.writeString("com.navdy.hud.app.debug.IRouteRecorder");
                b = true;
                break;
            }
            case 12: {
                a.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                boolean b0 = this.isStopped();
                a0.writeNoException();
                a0.writeInt(b0 ? 1 : 0);
                b = true;
                break;
            }
            case 11: {
                a.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                boolean b1 = this.isPaused();
                a0.writeNoException();
                a0.writeInt(b1 ? 1 : 0);
                b = true;
                break;
            }
            case 10: {
                a.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                boolean b2 = this.isPlaying();
                a0.writeNoException();
                a0.writeInt(b2 ? 1 : 0);
                b = true;
                break;
            }
            case 9: {
                a.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                boolean b3 = this.restartPlayback();
                a0.writeNoException();
                a0.writeInt(b3 ? 1 : 0);
                b = true;
                break;
            }
            case 8: {
                a.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                this.stopPlayback();
                a0.writeNoException();
                b = true;
                break;
            }
            case 7: {
                a.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                this.resumePlayback();
                a0.writeNoException();
                b = true;
                break;
            }
            case 6: {
                a.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                this.pausePlayback();
                a0.writeNoException();
                b = true;
                break;
            }
            case 5: {
                a.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                this.startPlayback(a.readString(), a.readInt() != 0, a.readInt() != 0);
                a0.writeNoException();
                b = true;
                break;
            }
            case 4: {
                a.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                boolean b4 = this.isRecording();
                a0.writeNoException();
                a0.writeInt(b4 ? 1 : 0);
                b = true;
                break;
            }
            case 3: {
                a.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                this.stopRecording();
                a0.writeNoException();
                b = true;
                break;
            }
            case 2: {
                a.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                String s = this.startRecording(a.readString());
                a0.writeNoException();
                a0.writeString(s);
                b = true;
                break;
            }
            case 1: {
                a.enforceInterface("com.navdy.hud.app.debug.IRouteRecorder");
                this.prepare(a.readString(), a.readInt() != 0);
                a0.writeNoException();
                b = true;
                break;
            }
            default: {
                b = super.onTransact(i, a, a0, i0);
            }
        }
        return b;
    }
}
