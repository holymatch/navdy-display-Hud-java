package com.navdy.hud.device.connection;

class iAP2Link$2 implements com.navdy.hud.device.connection.iAP2Link$NavdyEventProcessor {
    final com.navdy.hud.device.connection.iAP2Link this$0;
    
    iAP2Link$2(com.navdy.hud.device.connection.iAP2Link a) {
        super();
        this.this$0 = a;
    }
    
    public boolean processNavdyEvent(com.navdy.service.library.events.NavdyEvent a) {
        com.navdy.service.library.events.callcontrol.TelephonyRequest a0 = (com.navdy.service.library.events.callcontrol.TelephonyRequest)a.getExtension(com.navdy.service.library.events.Ext_NavdyEvent.telephonyRequest);
        com.navdy.hud.device.connection.iAP2Link.access$302(this.this$0, a0.action);
        boolean b = com.navdy.hud.device.connection.IAPMessageUtility.performActionForTelephonyRequest(a0, com.navdy.hud.device.connection.iAP2Link.access$400(this.this$0));
        this.this$0.sendMessageAsNavdyEvent((com.squareup.wire.Message)new com.navdy.service.library.events.callcontrol.TelephonyResponse(a0.action, b ? com.navdy.service.library.events.RequestStatus.REQUEST_SUCCESS : com.navdy.service.library.events.RequestStatus.REQUEST_NOT_AVAILABLE, (String)null));
        return true;
    }
}
