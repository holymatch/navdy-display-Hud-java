package com.navdy.hud.device.connection;

abstract interface iAP2Link$NavdyEventProcessor {
    abstract public boolean processNavdyEvent(com.navdy.service.library.events.NavdyEvent arg);
}
