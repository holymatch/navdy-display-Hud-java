package com.navdy.hud.device.connection;

final class IAPMessageUtility$2 extends java.util.HashMap {
    IAPMessageUtility$2() {
        this.put(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle.Off, com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF);
        this.put(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle.Songs, com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS);
        this.put(com.navdy.hud.mfi.NowPlayingUpdate$PlaybackShuffle.Albums, com.navdy.service.library.events.audio.MusicShuffleMode.MUSIC_SHUFFLE_MODE_ALBUMS);
    }
}
