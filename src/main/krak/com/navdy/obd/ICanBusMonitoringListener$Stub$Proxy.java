package com.navdy.obd;

class ICanBusMonitoringListener$Stub$Proxy implements com.navdy.obd.ICanBusMonitoringListener {
    private android.os.IBinder mRemote;
    
    ICanBusMonitoringListener$Stub$Proxy(android.os.IBinder a) {
        this.mRemote = a;
    }
    
    public android.os.IBinder asBinder() {
        return this.mRemote;
    }
    
    public int getGpsSpeed() {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
            this.mRemote.transact(4, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return i;
    }
    
    public String getInterfaceDescriptor() {
        return "com.navdy.obd.ICanBusMonitoringListener";
    }
    
    public double getLatitude() {
        double d = 0.0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
            this.mRemote.transact(5, a, a0, 0);
            a0.readException();
            d = a0.readDouble();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return d;
    }
    
    public double getLongitude() {
        double d = 0.0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
            this.mRemote.transact(6, a, a0, 0);
            a0.readException();
            d = a0.readDouble();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return d;
    }
    
    public String getMake() {
        String s = null;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
            this.mRemote.transact(8, a, a0, 0);
            a0.readException();
            s = a0.readString();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return s;
    }
    
    public String getModel() {
        String s = null;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
            this.mRemote.transact(9, a, a0, 0);
            a0.readException();
            s = a0.readString();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return s;
    }
    
    public String getYear() {
        String s = null;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
            this.mRemote.transact(10, a, a0, 0);
            a0.readException();
            s = a0.readString();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return s;
    }
    
    public boolean isMonitoringLimitReached() {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
            this.mRemote.transact(7, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        boolean b = i != 0;
        a0.recycle();
        a.recycle();
        return b;
    }
    
    public void onCanBusMonitorSuccess(int i) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
            a.writeInt(i);
            this.mRemote.transact(3, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void onCanBusMonitoringError(String s) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
            a.writeString(s);
            this.mRemote.transact(2, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void onNewDataAvailable(String s) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.obd.ICanBusMonitoringListener");
            a.writeString(s);
            this.mRemote.transact(1, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
}
