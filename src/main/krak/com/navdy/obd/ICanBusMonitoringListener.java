package com.navdy.obd;

abstract public interface ICanBusMonitoringListener extends android.os.IInterface {
    abstract public int getGpsSpeed();
    
    
    abstract public double getLatitude();
    
    
    abstract public double getLongitude();
    
    
    abstract public String getMake();
    
    
    abstract public String getModel();
    
    
    abstract public String getYear();
    
    
    abstract public boolean isMonitoringLimitReached();
    
    
    abstract public void onCanBusMonitorSuccess(int arg);
    
    
    abstract public void onCanBusMonitoringError(String arg);
    
    
    abstract public void onNewDataAvailable(String arg);
}
