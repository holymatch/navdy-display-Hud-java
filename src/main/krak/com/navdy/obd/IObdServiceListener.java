package com.navdy.obd;

abstract public interface IObdServiceListener extends android.os.IInterface {
    abstract public void onConnectionStateChange(int arg);
    
    
    abstract public void onRawData(String arg);
    
    
    abstract public void onStatusMessage(String arg);
    
    
    abstract public void scannedPids(java.util.List arg);
    
    
    abstract public void scannedVIN(String arg);
    
    
    abstract public void supportedPids(java.util.List arg);
}
