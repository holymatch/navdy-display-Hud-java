package com.navdy.obd;

public class ECU implements android.os.Parcelable {
    final public static android.os.Parcelable$Creator CREATOR;
    final public int address;
    final public com.navdy.obd.PidSet supportedPids;
    
    static {
        CREATOR = (android.os.Parcelable$Creator)new com.navdy.obd.ECU$1();
    }
    
    public ECU(int i, com.navdy.obd.PidSet a) {
        this.address = i;
        this.supportedPids = a;
    }
    
    public ECU(android.os.Parcel a) {
        this.address = a.readInt();
        this.supportedPids = (com.navdy.obd.PidSet)a.readParcelable(com.navdy.obd.PidSet.class.getClassLoader());
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void writeToParcel(android.os.Parcel a, int i) {
        a.writeInt(this.address);
        a.writeParcelable((android.os.Parcelable)this.supportedPids, 0);
    }
}
