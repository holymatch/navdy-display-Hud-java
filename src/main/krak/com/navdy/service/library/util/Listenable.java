package com.navdy.service.library.util;

abstract public class Listenable {
    final private Object listenerLock;
    final private com.navdy.service.library.log.Logger logger;
    protected java.util.HashSet mListeners;
    
    public Listenable() {
        this.logger = new com.navdy.service.library.log.Logger((this).getClass());
        this.listenerLock = new Object();
        this.mListeners = new java.util.HashSet();
    }
    
    public boolean addListener(com.navdy.service.library.util.Listenable$Listener a) {
        {
            boolean b = false;
            if (a != null) {
                synchronized (this.listenerLock) {
                    java.util.Iterator a2 = this.mListeners.iterator();
                    Object a3 = a;
                    Object a4 = a2;
                    while (true) {
                        if (((java.util.Iterator) a4).hasNext()) {
                            Object a5 = ((java.lang.ref.WeakReference) ((java.util.Iterator) a4).next()).get();
                            if (a5 == null) {
                                ((java.util.Iterator) a4).remove();
                                continue;
                            }
                            if (!a5.equals(a3)) {
                                continue;
                            }
                            /*monexit(a0)*/
                            b = false;
                            break;
                        } else {
                            this.mListeners.add( new java.lang.ref.WeakReference(a3));
                            /*monexit(a0)*/
                            b = true;
                            break;
                        }
                    }
                }
            } else {
                this.logger.e("attempted to add null listener");
                b = false;
            }
            return b;
        }
    }
    
    protected void dispatchToListeners(com.navdy.service.library.util.Listenable$EventDispatcher a) {
        java.util.HashSet a0 = null;
        synchronized(this.listenerLock) {
            a0 = (java.util.HashSet)this.mListeners.clone();
            /*monexit(a1)*/;
        }
        java.util.Iterator a6 = a0.iterator();
        Object a7 = a;
        Object a8 = a6;
        while(((java.util.Iterator)a8).hasNext()) {
            Object a9 = ((java.lang.ref.WeakReference)((java.util.Iterator)a8).next()).get();
            if (a9 != null) {
                ((com.navdy.service.library.util.Listenable$EventDispatcher)a7).dispatchEvent(this, (com.navdy.service.library.util.Listenable$Listener)a9);
            } else {
                ((java.util.Iterator)a8).remove();
            }
        }
    }
    
    public boolean removeListener(com.navdy.service.library.util.Listenable$Listener a) {
        Throwable a1 = null;
        {
            boolean b = false;
            if (a != null) {
                synchronized (this.listenerLock) {
                    java.util.Iterator a2 = this.mListeners.iterator();
                    Object a3 = a;
                    Object a4 = a2;
                    while (true) {
                        if (((java.util.Iterator) a4).hasNext()) {
                            Object a5 = ((java.lang.ref.WeakReference) ((java.util.Iterator) a4).next()).get();
                            if (a5 == null) {
                                ((java.util.Iterator) a4).remove();
                                continue;
                            }
                            if (!a5.equals(a3)) {
                                continue;
                            }
                            ((java.util.Iterator) a4).remove();
                            /*monexit(a0)*/
                            b = true;
                            break;
                        } else {
                            /*monexit(a0)*/
                            b = false;
                            break;
                        }
                    }
                }
            } else {
                this.logger.e("attempted to remove null listener");
                b = false;
            }
            return b;
        }
    }
}
