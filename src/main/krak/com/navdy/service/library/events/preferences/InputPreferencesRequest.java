package com.navdy.service.library.events.preferences;

final public class InputPreferencesRequest extends com.squareup.wire.Message {
    final public static Long DEFAULT_SERIAL_NUMBER;
    final private static long serialVersionUID = 0L;
    final public Long serial_number;
    
    static {
        DEFAULT_SERIAL_NUMBER = Long.valueOf(0L);
    }
    
    private InputPreferencesRequest(com.navdy.service.library.events.preferences.InputPreferencesRequest$Builder a) {
        this(a.serial_number);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    InputPreferencesRequest(com.navdy.service.library.events.preferences.InputPreferencesRequest$Builder a, com.navdy.service.library.events.preferences.InputPreferencesRequest$1 a0) {
        this(a);
    }
    
    public InputPreferencesRequest(Long a) {
        this.serial_number = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.preferences.InputPreferencesRequest && this.equals(this.serial_number, ((com.navdy.service.library.events.preferences.InputPreferencesRequest)a).serial_number);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.serial_number == null) ? 0 : this.serial_number.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
