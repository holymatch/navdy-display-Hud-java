package com.navdy.service.library.events.audio;

final public class MusicCollectionSourceUpdate extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.audio.MusicCollectionSource DEFAULT_COLLECTIONSOURCE;
    final public static Long DEFAULT_SERIAL_NUMBER;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    final public Long serial_number;
    
    static {
        DEFAULT_COLLECTIONSOURCE = com.navdy.service.library.events.audio.MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
        DEFAULT_SERIAL_NUMBER = Long.valueOf(0L);
    }
    
    public MusicCollectionSourceUpdate(com.navdy.service.library.events.audio.MusicCollectionSource a, Long a0) {
        this.collectionSource = a;
        this.serial_number = a0;
    }
    
    private MusicCollectionSourceUpdate(com.navdy.service.library.events.audio.MusicCollectionSourceUpdate$Builder a) {
        this(a.collectionSource, a.serial_number);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    MusicCollectionSourceUpdate(com.navdy.service.library.events.audio.MusicCollectionSourceUpdate$Builder a, com.navdy.service.library.events.audio.MusicCollectionSourceUpdate$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.audio.MusicCollectionSourceUpdate) {
                com.navdy.service.library.events.audio.MusicCollectionSourceUpdate a0 = (com.navdy.service.library.events.audio.MusicCollectionSourceUpdate)a;
                boolean b0 = this.equals(this.collectionSource, a0.collectionSource);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.serial_number, a0.serial_number)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.collectionSource == null) ? 0 : this.collectionSource.hashCode()) * 37 + ((this.serial_number == null) ? 0 : this.serial_number.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
