package com.navdy.service.library.events.destination;

final public class Destination$SuggestionType extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.destination.Destination$SuggestionType[] $VALUES;
    final public static com.navdy.service.library.events.destination.Destination$SuggestionType SUGGESTION_CALENDAR;
    final public static com.navdy.service.library.events.destination.Destination$SuggestionType SUGGESTION_NONE;
    final public static com.navdy.service.library.events.destination.Destination$SuggestionType SUGGESTION_RECENT;
    final private int value;
    
    static {
        SUGGESTION_NONE = new com.navdy.service.library.events.destination.Destination$SuggestionType("SUGGESTION_NONE", 0, 0);
        SUGGESTION_CALENDAR = new com.navdy.service.library.events.destination.Destination$SuggestionType("SUGGESTION_CALENDAR", 1, 1);
        SUGGESTION_RECENT = new com.navdy.service.library.events.destination.Destination$SuggestionType("SUGGESTION_RECENT", 2, 2);
        com.navdy.service.library.events.destination.Destination$SuggestionType[] a = new com.navdy.service.library.events.destination.Destination$SuggestionType[3];
        a[0] = SUGGESTION_NONE;
        a[1] = SUGGESTION_CALENDAR;
        a[2] = SUGGESTION_RECENT;
        $VALUES = a;
    }
    
    private Destination$SuggestionType(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.destination.Destination$SuggestionType valueOf(String s) {
        return (com.navdy.service.library.events.destination.Destination$SuggestionType)Enum.valueOf(com.navdy.service.library.events.destination.Destination$SuggestionType.class, s);
    }
    
    public static com.navdy.service.library.events.destination.Destination$SuggestionType[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
