package com.navdy.service.library.events.callcontrol;

final public class TelephonyRequest extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.callcontrol.CallAction DEFAULT_ACTION;
    final public static String DEFAULT_CALLUUID = "";
    final public static String DEFAULT_NUMBER = "";
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.callcontrol.CallAction action;
    final public String callUUID;
    final public String number;
    
    static {
        DEFAULT_ACTION = com.navdy.service.library.events.callcontrol.CallAction.CALL_ACCEPT;
    }
    
    public TelephonyRequest(com.navdy.service.library.events.callcontrol.CallAction a, String s, String s0) {
        this.action = a;
        this.number = s;
        this.callUUID = s0;
    }
    
    private TelephonyRequest(com.navdy.service.library.events.callcontrol.TelephonyRequest$Builder a) {
        this(a.action, a.number, a.callUUID);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    TelephonyRequest(com.navdy.service.library.events.callcontrol.TelephonyRequest$Builder a, com.navdy.service.library.events.callcontrol.TelephonyRequest$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.callcontrol.TelephonyRequest) {
                com.navdy.service.library.events.callcontrol.TelephonyRequest a0 = (com.navdy.service.library.events.callcontrol.TelephonyRequest)a;
                boolean b0 = this.equals(this.action, a0.action);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.number, a0.number)) {
                        break label1;
                    }
                    if (this.equals(this.callUUID, a0.callUUID)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((this.action == null) ? 0 : this.action.hashCode()) * 37 + ((this.number == null) ? 0 : this.number.hashCode())) * 37 + ((this.callUUID == null) ? 0 : this.callUUID.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
