package com.navdy.service.library.events;

final public class NavdyEvent extends com.squareup.wire.ExtendableMessage {
    final public static com.navdy.service.library.events.NavdyEvent$MessageType DEFAULT_TYPE;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.NavdyEvent$MessageType type;
    
    static {
        DEFAULT_TYPE = com.navdy.service.library.events.NavdyEvent$MessageType.Coordinate;
    }
    
    private NavdyEvent(com.navdy.service.library.events.NavdyEvent$Builder a) {
        this(a.type);
        this.setBuilder((com.squareup.wire.ExtendableMessage$ExtendableBuilder)a);
    }
    
    NavdyEvent(com.navdy.service.library.events.NavdyEvent$Builder a, com.navdy.service.library.events.NavdyEvent$1 a0) {
        this(a);
    }
    
    public NavdyEvent(com.navdy.service.library.events.NavdyEvent$MessageType a) {
        this.type = a;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        if (a != this) {
            if (a instanceof com.navdy.service.library.events.NavdyEvent) {
                com.navdy.service.library.events.NavdyEvent a0 = (com.navdy.service.library.events.NavdyEvent)a;
                b = this.extensionsEqual((com.squareup.wire.ExtendableMessage)a0) && this.equals(this.type, a0.type);
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = this.extensionsHashCode() * 37 + ((this.type == null) ? 0 : this.type.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
