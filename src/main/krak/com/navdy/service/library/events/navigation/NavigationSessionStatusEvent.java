package com.navdy.service.library.events.navigation;

final public class NavigationSessionStatusEvent extends com.squareup.wire.Message {
    final public static String DEFAULT_DESTINATION_IDENTIFIER = "";
    final public static String DEFAULT_LABEL = "";
    final public static String DEFAULT_ROUTEID = "";
    final public static com.navdy.service.library.events.navigation.NavigationSessionState DEFAULT_SESSIONSTATE;
    final private static long serialVersionUID = 0L;
    final public String destination_identifier;
    final public String label;
    final public com.navdy.service.library.events.navigation.NavigationRouteResult route;
    final public String routeId;
    final public com.navdy.service.library.events.navigation.NavigationSessionState sessionState;
    
    static {
        DEFAULT_SESSIONSTATE = com.navdy.service.library.events.navigation.NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    }
    
    public NavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionState a, String s, String s0, com.navdy.service.library.events.navigation.NavigationRouteResult a0, String s1) {
        this.sessionState = a;
        this.label = s;
        this.routeId = s0;
        this.route = a0;
        this.destination_identifier = s1;
    }
    
    private NavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionStatusEvent$Builder a) {
        this(a.sessionState, a.label, a.routeId, a.route, a.destination_identifier);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    NavigationSessionStatusEvent(com.navdy.service.library.events.navigation.NavigationSessionStatusEvent$Builder a, com.navdy.service.library.events.navigation.NavigationSessionStatusEvent$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.navigation.NavigationSessionStatusEvent) {
                com.navdy.service.library.events.navigation.NavigationSessionStatusEvent a0 = (com.navdy.service.library.events.navigation.NavigationSessionStatusEvent)a;
                boolean b0 = this.equals(this.sessionState, a0.sessionState);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.label, a0.label)) {
                        break label1;
                    }
                    if (!this.equals(this.routeId, a0.routeId)) {
                        break label1;
                    }
                    if (!this.equals(this.route, a0.route)) {
                        break label1;
                    }
                    if (this.equals(this.destination_identifier, a0.destination_identifier)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((this.sessionState == null) ? 0 : this.sessionState.hashCode()) * 37 + ((this.label == null) ? 0 : this.label.hashCode())) * 37 + ((this.routeId == null) ? 0 : this.routeId.hashCode())) * 37 + ((this.route == null) ? 0 : this.route.hashCode())) * 37 + ((this.destination_identifier == null) ? 0 : this.destination_identifier.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
