package com.navdy.service.library.events.notification;

final public class ServiceType extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.notification.ServiceType[] $VALUES;
    final public static com.navdy.service.library.events.notification.ServiceType SERVICE_ANCS;
    final public static com.navdy.service.library.events.notification.ServiceType SERVICE_PANDORA;
    final private int value;
    
    static {
        SERVICE_ANCS = new com.navdy.service.library.events.notification.ServiceType("SERVICE_ANCS", 0, 1);
        SERVICE_PANDORA = new com.navdy.service.library.events.notification.ServiceType("SERVICE_PANDORA", 1, 2);
        com.navdy.service.library.events.notification.ServiceType[] a = new com.navdy.service.library.events.notification.ServiceType[2];
        a[0] = SERVICE_ANCS;
        a[1] = SERVICE_PANDORA;
        $VALUES = a;
    }
    
    private ServiceType(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.notification.ServiceType valueOf(String s) {
        return (com.navdy.service.library.events.notification.ServiceType)Enum.valueOf(com.navdy.service.library.events.notification.ServiceType.class, s);
    }
    
    public static com.navdy.service.library.events.notification.ServiceType[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
