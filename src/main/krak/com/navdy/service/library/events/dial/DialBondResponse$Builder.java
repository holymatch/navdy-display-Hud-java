package com.navdy.service.library.events.dial;

final public class DialBondResponse$Builder extends com.squareup.wire.Message.Builder {
    public String macAddress;
    public String name;
    public com.navdy.service.library.events.dial.DialError status;
    
    public DialBondResponse$Builder() {
    }
    
    public DialBondResponse$Builder(com.navdy.service.library.events.dial.DialBondResponse a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.status = a.status;
            this.name = a.name;
            this.macAddress = a.macAddress;
        }
    }
    
    public com.navdy.service.library.events.dial.DialBondResponse build() {
        return new com.navdy.service.library.events.dial.DialBondResponse(this, (com.navdy.service.library.events.dial.DialBondResponse$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.dial.DialBondResponse$Builder macAddress(String s) {
        this.macAddress = s;
        return this;
    }
    
    public com.navdy.service.library.events.dial.DialBondResponse$Builder name(String s) {
        this.name = s;
        return this;
    }
    
    public com.navdy.service.library.events.dial.DialBondResponse$Builder status(com.navdy.service.library.events.dial.DialError a) {
        this.status = a;
        return this;
    }
}
