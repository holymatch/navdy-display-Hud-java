package com.navdy.service.library.events.audio;

final public class MusicCollectionRequest$Builder extends com.squareup.wire.Message.Builder {
    public String collectionId;
    public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    public java.util.List filters;
    public com.navdy.service.library.events.audio.MusicCollectionType groupBy;
    public Boolean includeCharacterMap;
    public Integer limit;
    public Integer offset;
    
    public MusicCollectionRequest$Builder() {
    }
    
    public MusicCollectionRequest$Builder(com.navdy.service.library.events.audio.MusicCollectionRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.collectionSource = a.collectionSource;
            this.collectionType = a.collectionType;
            this.filters = com.navdy.service.library.events.audio.MusicCollectionRequest.access$000(a.filters);
            this.collectionId = a.collectionId;
            this.limit = a.limit;
            this.offset = a.offset;
            this.includeCharacterMap = a.includeCharacterMap;
            this.groupBy = a.groupBy;
        }
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionRequest build() {
        return new com.navdy.service.library.events.audio.MusicCollectionRequest(this, (com.navdy.service.library.events.audio.MusicCollectionRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionRequest$Builder collectionId(String s) {
        this.collectionId = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionRequest$Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource a) {
        this.collectionSource = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionRequest$Builder collectionType(com.navdy.service.library.events.audio.MusicCollectionType a) {
        this.collectionType = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionRequest$Builder filters(java.util.List a) {
        this.filters = com.navdy.service.library.events.audio.MusicCollectionRequest$Builder.checkForNulls(a);
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionRequest$Builder groupBy(com.navdy.service.library.events.audio.MusicCollectionType a) {
        this.groupBy = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionRequest$Builder includeCharacterMap(Boolean a) {
        this.includeCharacterMap = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionRequest$Builder limit(Integer a) {
        this.limit = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicCollectionRequest$Builder offset(Integer a) {
        this.offset = a;
        return this;
    }
}
