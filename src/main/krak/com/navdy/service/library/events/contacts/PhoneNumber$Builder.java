package com.navdy.service.library.events.contacts;

final public class PhoneNumber$Builder extends com.squareup.wire.Message.Builder {
    public String customType;
    public String number;
    public com.navdy.service.library.events.contacts.PhoneNumberType numberType;
    
    public PhoneNumber$Builder() {
    }
    
    public PhoneNumber$Builder(com.navdy.service.library.events.contacts.PhoneNumber a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.number = a.number;
            this.numberType = a.numberType;
            this.customType = a.customType;
        }
    }
    
    public com.navdy.service.library.events.contacts.PhoneNumber build() {
        return new com.navdy.service.library.events.contacts.PhoneNumber(this, (com.navdy.service.library.events.contacts.PhoneNumber$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.contacts.PhoneNumber$Builder customType(String s) {
        this.customType = s;
        return this;
    }
    
    public com.navdy.service.library.events.contacts.PhoneNumber$Builder number(String s) {
        this.number = s;
        return this;
    }
    
    public com.navdy.service.library.events.contacts.PhoneNumber$Builder numberType(com.navdy.service.library.events.contacts.PhoneNumberType a) {
        this.numberType = a;
        return this;
    }
}
