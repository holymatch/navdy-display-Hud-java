package com.navdy.service.library.events.dial;

final public class DialStatusResponse extends com.squareup.wire.Message {
    final public static Integer DEFAULT_BATTERYLEVEL;
    final public static Boolean DEFAULT_ISCONNECTED;
    final public static Boolean DEFAULT_ISPAIRED;
    final public static String DEFAULT_MACADDRESS = "";
    final public static String DEFAULT_NAME = "";
    final private static long serialVersionUID = 0L;
    final public Integer batteryLevel;
    final public Boolean isConnected;
    final public Boolean isPaired;
    final public String macAddress;
    final public String name;
    
    static {
        DEFAULT_ISPAIRED = Boolean.valueOf(false);
        DEFAULT_ISCONNECTED = Boolean.valueOf(false);
        DEFAULT_BATTERYLEVEL = Integer.valueOf(0);
    }
    
    private DialStatusResponse(com.navdy.service.library.events.dial.DialStatusResponse$Builder a) {
        this(a.isPaired, a.isConnected, a.name, a.macAddress, a.batteryLevel);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DialStatusResponse(com.navdy.service.library.events.dial.DialStatusResponse$Builder a, com.navdy.service.library.events.dial.DialStatusResponse$1 a0) {
        this(a);
    }
    
    public DialStatusResponse(Boolean a, Boolean a0, String s, String s0, Integer a1) {
        this.isPaired = a;
        this.isConnected = a0;
        this.name = s;
        this.macAddress = s0;
        this.batteryLevel = a1;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.dial.DialStatusResponse) {
                com.navdy.service.library.events.dial.DialStatusResponse a0 = (com.navdy.service.library.events.dial.DialStatusResponse)a;
                boolean b0 = this.equals(this.isPaired, a0.isPaired);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.isConnected, a0.isConnected)) {
                        break label1;
                    }
                    if (!this.equals(this.name, a0.name)) {
                        break label1;
                    }
                    if (!this.equals(this.macAddress, a0.macAddress)) {
                        break label1;
                    }
                    if (this.equals(this.batteryLevel, a0.batteryLevel)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((this.isPaired == null) ? 0 : this.isPaired.hashCode()) * 37 + ((this.isConnected == null) ? 0 : this.isConnected.hashCode())) * 37 + ((this.name == null) ? 0 : this.name.hashCode())) * 37 + ((this.macAddress == null) ? 0 : this.macAddress.hashCode())) * 37 + ((this.batteryLevel == null) ? 0 : this.batteryLevel.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
