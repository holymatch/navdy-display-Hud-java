package com.navdy.service.library.events.audio;

final public class MusicTrackInfo$Builder extends com.squareup.wire.Message.Builder {
    public String album;
    public String author;
    public String collectionId;
    public com.navdy.service.library.events.audio.MusicCollectionSource collectionSource;
    public com.navdy.service.library.events.audio.MusicCollectionType collectionType;
    public Integer currentPosition;
    public com.navdy.service.library.events.audio.MusicDataSource dataSource;
    public Integer duration;
    public Long index;
    public Boolean isNextAllowed;
    public Boolean isOnlineStream;
    public Boolean isPreviousAllowed;
    public String name;
    public Integer playCount;
    public com.navdy.service.library.events.audio.MusicPlaybackState playbackState;
    public com.navdy.service.library.events.audio.MusicRepeatMode repeatMode;
    public com.navdy.service.library.events.audio.MusicShuffleMode shuffleMode;
    public String trackId;
    
    public MusicTrackInfo$Builder() {
    }
    
    public MusicTrackInfo$Builder(com.navdy.service.library.events.audio.MusicTrackInfo a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.playbackState = a.playbackState;
            this.index = a.index;
            this.name = a.name;
            this.author = a.author;
            this.album = a.album;
            this.duration = a.duration;
            this.currentPosition = a.currentPosition;
            this.isPreviousAllowed = a.isPreviousAllowed;
            this.isNextAllowed = a.isNextAllowed;
            this.dataSource = a.dataSource;
            this.isOnlineStream = a.isOnlineStream;
            this.collectionSource = a.collectionSource;
            this.collectionType = a.collectionType;
            this.collectionId = a.collectionId;
            this.trackId = a.trackId;
            this.playCount = a.playCount;
            this.shuffleMode = a.shuffleMode;
            this.repeatMode = a.repeatMode;
        }
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder album(String s) {
        this.album = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder author(String s) {
        this.author = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.audio.MusicTrackInfo(this, (com.navdy.service.library.events.audio.MusicTrackInfo$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder collectionId(String s) {
        this.collectionId = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder collectionSource(com.navdy.service.library.events.audio.MusicCollectionSource a) {
        this.collectionSource = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder collectionType(com.navdy.service.library.events.audio.MusicCollectionType a) {
        this.collectionType = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder currentPosition(Integer a) {
        this.currentPosition = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder dataSource(com.navdy.service.library.events.audio.MusicDataSource a) {
        this.dataSource = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder duration(Integer a) {
        this.duration = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder index(Long a) {
        this.index = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder isNextAllowed(Boolean a) {
        this.isNextAllowed = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder isOnlineStream(Boolean a) {
        this.isOnlineStream = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder isPreviousAllowed(Boolean a) {
        this.isPreviousAllowed = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder name(String s) {
        this.name = s;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder playCount(Integer a) {
        this.playCount = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder playbackState(com.navdy.service.library.events.audio.MusicPlaybackState a) {
        this.playbackState = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder repeatMode(com.navdy.service.library.events.audio.MusicRepeatMode a) {
        this.repeatMode = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder shuffleMode(com.navdy.service.library.events.audio.MusicShuffleMode a) {
        this.shuffleMode = a;
        return this;
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfo$Builder trackId(String s) {
        this.trackId = s;
        return this;
    }
}
