package com.navdy.service.library.events.glances;

final public class EmailConstants extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.glances.EmailConstants[] $VALUES;
    final public static com.navdy.service.library.events.glances.EmailConstants EMAIL_BODY;
    final public static com.navdy.service.library.events.glances.EmailConstants EMAIL_FROM_EMAIL;
    final public static com.navdy.service.library.events.glances.EmailConstants EMAIL_FROM_NAME;
    final public static com.navdy.service.library.events.glances.EmailConstants EMAIL_SUBJECT;
    final public static com.navdy.service.library.events.glances.EmailConstants EMAIL_TO_EMAIL;
    final public static com.navdy.service.library.events.glances.EmailConstants EMAIL_TO_NAME;
    final private int value;
    
    static {
        EMAIL_FROM_EMAIL = new com.navdy.service.library.events.glances.EmailConstants("EMAIL_FROM_EMAIL", 0, 0);
        EMAIL_FROM_NAME = new com.navdy.service.library.events.glances.EmailConstants("EMAIL_FROM_NAME", 1, 1);
        EMAIL_TO_EMAIL = new com.navdy.service.library.events.glances.EmailConstants("EMAIL_TO_EMAIL", 2, 2);
        EMAIL_TO_NAME = new com.navdy.service.library.events.glances.EmailConstants("EMAIL_TO_NAME", 3, 3);
        EMAIL_SUBJECT = new com.navdy.service.library.events.glances.EmailConstants("EMAIL_SUBJECT", 4, 4);
        EMAIL_BODY = new com.navdy.service.library.events.glances.EmailConstants("EMAIL_BODY", 5, 5);
        com.navdy.service.library.events.glances.EmailConstants[] a = new com.navdy.service.library.events.glances.EmailConstants[6];
        a[0] = EMAIL_FROM_EMAIL;
        a[1] = EMAIL_FROM_NAME;
        a[2] = EMAIL_TO_EMAIL;
        a[3] = EMAIL_TO_NAME;
        a[4] = EMAIL_SUBJECT;
        a[5] = EMAIL_BODY;
        $VALUES = a;
    }
    
    private EmailConstants(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.glances.EmailConstants valueOf(String s) {
        return (com.navdy.service.library.events.glances.EmailConstants)Enum.valueOf(com.navdy.service.library.events.glances.EmailConstants.class, s);
    }
    
    public static com.navdy.service.library.events.glances.EmailConstants[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
