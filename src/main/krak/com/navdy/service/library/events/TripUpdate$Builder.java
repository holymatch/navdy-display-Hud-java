package com.navdy.service.library.events;

final public class TripUpdate$Builder extends com.squareup.wire.Message.Builder {
    public String arrived_at_destination_id;
    public Float bearing;
    public String chosen_destination_id;
    public com.navdy.service.library.events.location.LatLong current_position;
    public Integer distance_to_destination;
    public Integer distance_traveled;
    public Double elevation;
    public Float elevation_accuracy;
    public Integer estimated_time_remaining;
    public Double excessive_speeding_ratio;
    public Float gps_speed;
    public Integer hard_acceleration_count;
    public Integer hard_breaking_count;
    public Integer high_g_count;
    public Float horizontal_accuracy;
    public com.navdy.service.library.events.location.Coordinate last_raw_coordinate;
    public Integer meters_traveled_since_boot;
    public Integer obd_speed;
    public String road_element;
    public Integer sequence_number;
    public Double speeding_ratio;
    public Long timestamp;
    public Long trip_number;
    
    public TripUpdate$Builder() {
    }
    
    public TripUpdate$Builder(com.navdy.service.library.events.TripUpdate a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.trip_number = a.trip_number;
            this.sequence_number = a.sequence_number;
            this.timestamp = a.timestamp;
            this.distance_traveled = a.distance_traveled;
            this.current_position = a.current_position;
            this.elevation = a.elevation;
            this.bearing = a.bearing;
            this.gps_speed = a.gps_speed;
            this.obd_speed = a.obd_speed;
            this.road_element = a.road_element;
            this.chosen_destination_id = a.chosen_destination_id;
            this.arrived_at_destination_id = a.arrived_at_destination_id;
            this.estimated_time_remaining = a.estimated_time_remaining;
            this.distance_to_destination = a.distance_to_destination;
            this.high_g_count = a.high_g_count;
            this.hard_breaking_count = a.hard_breaking_count;
            this.hard_acceleration_count = a.hard_acceleration_count;
            this.speeding_ratio = a.speeding_ratio;
            this.excessive_speeding_ratio = a.excessive_speeding_ratio;
            this.meters_traveled_since_boot = a.meters_traveled_since_boot;
            this.horizontal_accuracy = a.horizontal_accuracy;
            this.elevation_accuracy = a.elevation_accuracy;
            this.last_raw_coordinate = a.last_raw_coordinate;
        }
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder arrived_at_destination_id(String s) {
        this.arrived_at_destination_id = s;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder bearing(Float a) {
        this.bearing = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.TripUpdate(this, (com.navdy.service.library.events.TripUpdate$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder chosen_destination_id(String s) {
        this.chosen_destination_id = s;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder current_position(com.navdy.service.library.events.location.LatLong a) {
        this.current_position = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder distance_to_destination(Integer a) {
        this.distance_to_destination = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder distance_traveled(Integer a) {
        this.distance_traveled = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder elevation(Double a) {
        this.elevation = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder elevation_accuracy(Float a) {
        this.elevation_accuracy = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder estimated_time_remaining(Integer a) {
        this.estimated_time_remaining = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder excessive_speeding_ratio(Double a) {
        this.excessive_speeding_ratio = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder gps_speed(Float a) {
        this.gps_speed = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder hard_acceleration_count(Integer a) {
        this.hard_acceleration_count = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder hard_breaking_count(Integer a) {
        this.hard_breaking_count = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder high_g_count(Integer a) {
        this.high_g_count = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder horizontal_accuracy(Float a) {
        this.horizontal_accuracy = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder last_raw_coordinate(com.navdy.service.library.events.location.Coordinate a) {
        this.last_raw_coordinate = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder meters_traveled_since_boot(Integer a) {
        this.meters_traveled_since_boot = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder obd_speed(Integer a) {
        this.obd_speed = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder road_element(String s) {
        this.road_element = s;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder sequence_number(Integer a) {
        this.sequence_number = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder speeding_ratio(Double a) {
        this.speeding_ratio = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder timestamp(Long a) {
        this.timestamp = a;
        return this;
    }
    
    public com.navdy.service.library.events.TripUpdate$Builder trip_number(Long a) {
        this.trip_number = a;
        return this;
    }
}
