package com.navdy.service.library.events.connection;

final public class ConnectionStatus extends com.squareup.wire.Message {
    final public static String DEFAULT_REMOTEDEVICEID = "";
    final public static com.navdy.service.library.events.connection.ConnectionStatus$Status DEFAULT_STATUS;
    final private static long serialVersionUID = 0L;
    final public String remoteDeviceId;
    final public com.navdy.service.library.events.connection.ConnectionStatus$Status status;
    
    static {
        DEFAULT_STATUS = com.navdy.service.library.events.connection.ConnectionStatus$Status.CONNECTION_PAIRING;
    }
    
    private ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus$Builder a) {
        this(a.status, a.remoteDeviceId);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus$Builder a, com.navdy.service.library.events.connection.ConnectionStatus$1 a0) {
        this(a);
    }
    
    public ConnectionStatus(com.navdy.service.library.events.connection.ConnectionStatus$Status a, String s) {
        this.status = a;
        this.remoteDeviceId = s;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.connection.ConnectionStatus) {
                com.navdy.service.library.events.connection.ConnectionStatus a0 = (com.navdy.service.library.events.connection.ConnectionStatus)a;
                boolean b0 = this.equals(this.status, a0.status);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.remoteDeviceId, a0.remoteDeviceId)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.status == null) ? 0 : this.status.hashCode()) * 37 + ((this.remoteDeviceId == null) ? 0 : this.remoteDeviceId.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
