package com.navdy.service.library.events.places;

final public class PlacesSearchResult$Builder extends com.squareup.wire.Message.Builder {
    public String address;
    public String category;
    public com.navdy.service.library.events.location.Coordinate destinationLocation;
    public Double distance;
    public String label;
    public com.navdy.service.library.events.location.Coordinate navigationPosition;
    
    public PlacesSearchResult$Builder() {
    }
    
    public PlacesSearchResult$Builder(com.navdy.service.library.events.places.PlacesSearchResult a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.label = a.label;
            this.navigationPosition = a.navigationPosition;
            this.destinationLocation = a.destinationLocation;
            this.address = a.address;
            this.category = a.category;
            this.distance = a.distance;
        }
    }
    
    public com.navdy.service.library.events.places.PlacesSearchResult$Builder address(String s) {
        this.address = s;
        return this;
    }
    
    public com.navdy.service.library.events.places.PlacesSearchResult build() {
        return new com.navdy.service.library.events.places.PlacesSearchResult(this, (com.navdy.service.library.events.places.PlacesSearchResult$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.places.PlacesSearchResult$Builder category(String s) {
        this.category = s;
        return this;
    }
    
    public com.navdy.service.library.events.places.PlacesSearchResult$Builder destinationLocation(com.navdy.service.library.events.location.Coordinate a) {
        this.destinationLocation = a;
        return this;
    }
    
    public com.navdy.service.library.events.places.PlacesSearchResult$Builder distance(Double a) {
        this.distance = a;
        return this;
    }
    
    public com.navdy.service.library.events.places.PlacesSearchResult$Builder label(String s) {
        this.label = s;
        return this;
    }
    
    public com.navdy.service.library.events.places.PlacesSearchResult$Builder navigationPosition(com.navdy.service.library.events.location.Coordinate a) {
        this.navigationPosition = a;
        return this;
    }
}
