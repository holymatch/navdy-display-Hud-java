package com.navdy.service.library.events.audio;

final public class MusicCollectionSource extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.MusicCollectionSource[] $VALUES;
    final public static com.navdy.service.library.events.audio.MusicCollectionSource COLLECTION_SOURCE_ANDROID_GOOGLE_PLAY_MUSIC;
    final public static com.navdy.service.library.events.audio.MusicCollectionSource COLLECTION_SOURCE_ANDROID_LOCAL;
    final public static com.navdy.service.library.events.audio.MusicCollectionSource COLLECTION_SOURCE_ANDROID_SPOTIFY;
    final public static com.navdy.service.library.events.audio.MusicCollectionSource COLLECTION_SOURCE_IOS_MEDIA_PLAYER;
    final public static com.navdy.service.library.events.audio.MusicCollectionSource COLLECTION_SOURCE_UNKNOWN;
    final private int value;
    
    static {
        COLLECTION_SOURCE_UNKNOWN = new com.navdy.service.library.events.audio.MusicCollectionSource("COLLECTION_SOURCE_UNKNOWN", 0, 0);
        COLLECTION_SOURCE_ANDROID_LOCAL = new com.navdy.service.library.events.audio.MusicCollectionSource("COLLECTION_SOURCE_ANDROID_LOCAL", 1, 1);
        COLLECTION_SOURCE_IOS_MEDIA_PLAYER = new com.navdy.service.library.events.audio.MusicCollectionSource("COLLECTION_SOURCE_IOS_MEDIA_PLAYER", 2, 2);
        COLLECTION_SOURCE_ANDROID_GOOGLE_PLAY_MUSIC = new com.navdy.service.library.events.audio.MusicCollectionSource("COLLECTION_SOURCE_ANDROID_GOOGLE_PLAY_MUSIC", 3, 3);
        COLLECTION_SOURCE_ANDROID_SPOTIFY = new com.navdy.service.library.events.audio.MusicCollectionSource("COLLECTION_SOURCE_ANDROID_SPOTIFY", 4, 4);
        com.navdy.service.library.events.audio.MusicCollectionSource[] a = new com.navdy.service.library.events.audio.MusicCollectionSource[5];
        a[0] = COLLECTION_SOURCE_UNKNOWN;
        a[1] = COLLECTION_SOURCE_ANDROID_LOCAL;
        a[2] = COLLECTION_SOURCE_IOS_MEDIA_PLAYER;
        a[3] = COLLECTION_SOURCE_ANDROID_GOOGLE_PLAY_MUSIC;
        a[4] = COLLECTION_SOURCE_ANDROID_SPOTIFY;
        $VALUES = a;
    }
    
    private MusicCollectionSource(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.MusicCollectionSource valueOf(String s) {
        return (com.navdy.service.library.events.audio.MusicCollectionSource)Enum.valueOf(com.navdy.service.library.events.audio.MusicCollectionSource.class, s);
    }
    
    public static com.navdy.service.library.events.audio.MusicCollectionSource[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
