package com.navdy.service.library.events.audio;

final public class MusicCharacterMap extends com.squareup.wire.Message {
    final public static String DEFAULT_CHARACTER = "";
    final public static Integer DEFAULT_OFFSET;
    final private static long serialVersionUID = 0L;
    final public String character;
    final public Integer offset;
    
    static {
        DEFAULT_OFFSET = Integer.valueOf(0);
    }
    
    private MusicCharacterMap(com.navdy.service.library.events.audio.MusicCharacterMap$Builder a) {
        this(a.character, a.offset);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    MusicCharacterMap(com.navdy.service.library.events.audio.MusicCharacterMap$Builder a, com.navdy.service.library.events.audio.MusicCharacterMap$1 a0) {
        this(a);
    }
    
    public MusicCharacterMap(String s, Integer a) {
        this.character = s;
        this.offset = a;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.audio.MusicCharacterMap) {
                com.navdy.service.library.events.audio.MusicCharacterMap a0 = (com.navdy.service.library.events.audio.MusicCharacterMap)a;
                boolean b0 = this.equals(this.character, a0.character);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (this.equals(this.offset, a0.offset)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = ((this.character == null) ? 0 : this.character.hashCode()) * 37 + ((this.offset == null) ? 0 : this.offset.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
