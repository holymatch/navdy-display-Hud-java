package com.navdy.service.library.events.audio;

final public class CancelSpeechRequest$Builder extends com.squareup.wire.Message.Builder {
    public String id;
    
    public CancelSpeechRequest$Builder() {
    }
    
    public CancelSpeechRequest$Builder(com.navdy.service.library.events.audio.CancelSpeechRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.id = a.id;
        }
    }
    
    public com.navdy.service.library.events.audio.CancelSpeechRequest build() {
        return new com.navdy.service.library.events.audio.CancelSpeechRequest(this, (com.navdy.service.library.events.audio.CancelSpeechRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.audio.CancelSpeechRequest$Builder id(String s) {
        this.id = s;
        return this;
    }
}
