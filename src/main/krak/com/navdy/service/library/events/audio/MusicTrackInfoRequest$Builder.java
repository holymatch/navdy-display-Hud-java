package com.navdy.service.library.events.audio;

final public class MusicTrackInfoRequest$Builder extends com.squareup.wire.Message.Builder {
    public MusicTrackInfoRequest$Builder() {
    }
    
    public MusicTrackInfoRequest$Builder(com.navdy.service.library.events.audio.MusicTrackInfoRequest a) {
        super((com.squareup.wire.Message)a);
    }
    
    public com.navdy.service.library.events.audio.MusicTrackInfoRequest build() {
        return new com.navdy.service.library.events.audio.MusicTrackInfoRequest(this, (com.navdy.service.library.events.audio.MusicTrackInfoRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
