package com.navdy.service.library.events.places;

final public class SuggestedDestination$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.destination.Destination destination;
    public Integer duration_traffic;
    public com.navdy.service.library.events.places.SuggestedDestination$SuggestionType type;
    
    public SuggestedDestination$Builder() {
    }
    
    public SuggestedDestination$Builder(com.navdy.service.library.events.places.SuggestedDestination a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.destination = a.destination;
            this.duration_traffic = a.duration_traffic;
            this.type = a.type;
        }
    }
    
    public com.navdy.service.library.events.places.SuggestedDestination build() {
        this.checkRequiredFields();
        return new com.navdy.service.library.events.places.SuggestedDestination(this, (com.navdy.service.library.events.places.SuggestedDestination$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.places.SuggestedDestination$Builder destination(com.navdy.service.library.events.destination.Destination a) {
        this.destination = a;
        return this;
    }
    
    public com.navdy.service.library.events.places.SuggestedDestination$Builder duration_traffic(Integer a) {
        this.duration_traffic = a;
        return this;
    }
    
    public com.navdy.service.library.events.places.SuggestedDestination$Builder type(com.navdy.service.library.events.places.SuggestedDestination$SuggestionType a) {
        this.type = a;
        return this;
    }
}
