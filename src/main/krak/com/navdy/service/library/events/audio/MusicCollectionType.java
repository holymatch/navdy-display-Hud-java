package com.navdy.service.library.events.audio;

final public class MusicCollectionType extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.audio.MusicCollectionType[] $VALUES;
    final public static com.navdy.service.library.events.audio.MusicCollectionType COLLECTION_TYPE_ALBUMS;
    final public static com.navdy.service.library.events.audio.MusicCollectionType COLLECTION_TYPE_ARTISTS;
    final public static com.navdy.service.library.events.audio.MusicCollectionType COLLECTION_TYPE_AUDIOBOOKS;
    final public static com.navdy.service.library.events.audio.MusicCollectionType COLLECTION_TYPE_PLAYLISTS;
    final public static com.navdy.service.library.events.audio.MusicCollectionType COLLECTION_TYPE_PODCASTS;
    final public static com.navdy.service.library.events.audio.MusicCollectionType COLLECTION_TYPE_UNKNOWN;
    final private int value;
    
    static {
        COLLECTION_TYPE_UNKNOWN = new com.navdy.service.library.events.audio.MusicCollectionType("COLLECTION_TYPE_UNKNOWN", 0, 0);
        COLLECTION_TYPE_PLAYLISTS = new com.navdy.service.library.events.audio.MusicCollectionType("COLLECTION_TYPE_PLAYLISTS", 1, 1);
        COLLECTION_TYPE_ARTISTS = new com.navdy.service.library.events.audio.MusicCollectionType("COLLECTION_TYPE_ARTISTS", 2, 2);
        COLLECTION_TYPE_ALBUMS = new com.navdy.service.library.events.audio.MusicCollectionType("COLLECTION_TYPE_ALBUMS", 3, 3);
        COLLECTION_TYPE_PODCASTS = new com.navdy.service.library.events.audio.MusicCollectionType("COLLECTION_TYPE_PODCASTS", 4, 4);
        COLLECTION_TYPE_AUDIOBOOKS = new com.navdy.service.library.events.audio.MusicCollectionType("COLLECTION_TYPE_AUDIOBOOKS", 5, 5);
        com.navdy.service.library.events.audio.MusicCollectionType[] a = new com.navdy.service.library.events.audio.MusicCollectionType[6];
        a[0] = COLLECTION_TYPE_UNKNOWN;
        a[1] = COLLECTION_TYPE_PLAYLISTS;
        a[2] = COLLECTION_TYPE_ARTISTS;
        a[3] = COLLECTION_TYPE_ALBUMS;
        a[4] = COLLECTION_TYPE_PODCASTS;
        a[5] = COLLECTION_TYPE_AUDIOBOOKS;
        $VALUES = a;
    }
    
    private MusicCollectionType(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.audio.MusicCollectionType valueOf(String s) {
        return (com.navdy.service.library.events.audio.MusicCollectionType)Enum.valueOf(com.navdy.service.library.events.audio.MusicCollectionType.class, s);
    }
    
    public static com.navdy.service.library.events.audio.MusicCollectionType[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
