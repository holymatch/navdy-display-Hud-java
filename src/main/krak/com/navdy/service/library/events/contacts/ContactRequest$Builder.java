package com.navdy.service.library.events.contacts;

final public class ContactRequest$Builder extends com.squareup.wire.Message.Builder {
    public String identifier;
    
    public ContactRequest$Builder() {
    }
    
    public ContactRequest$Builder(com.navdy.service.library.events.contacts.ContactRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.identifier = a.identifier;
        }
    }
    
    public com.navdy.service.library.events.contacts.ContactRequest build() {
        return new com.navdy.service.library.events.contacts.ContactRequest(this, (com.navdy.service.library.events.contacts.ContactRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
    
    public com.navdy.service.library.events.contacts.ContactRequest$Builder identifier(String s) {
        this.identifier = s;
        return this;
    }
}
