package com.navdy.service.library.events.ui;

final public class DismissScreen extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.ui.Screen DEFAULT_SCREEN;
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.ui.Screen screen;
    
    static {
        DEFAULT_SCREEN = com.navdy.service.library.events.ui.Screen.SCREEN_DASHBOARD;
    }
    
    private DismissScreen(com.navdy.service.library.events.ui.DismissScreen$Builder a) {
        this(a.screen);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    DismissScreen(com.navdy.service.library.events.ui.DismissScreen$Builder a, com.navdy.service.library.events.ui.DismissScreen$1 a0) {
        this(a);
    }
    
    public DismissScreen(com.navdy.service.library.events.ui.Screen a) {
        this.screen = a;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.ui.DismissScreen && this.equals(this.screen, ((com.navdy.service.library.events.ui.DismissScreen)a).screen);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.screen == null) ? 0 : this.screen.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
