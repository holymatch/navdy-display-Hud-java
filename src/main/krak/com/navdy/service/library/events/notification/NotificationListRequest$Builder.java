package com.navdy.service.library.events.notification;

final public class NotificationListRequest$Builder extends com.squareup.wire.Message.Builder {
    public NotificationListRequest$Builder() {
    }
    
    public NotificationListRequest$Builder(com.navdy.service.library.events.notification.NotificationListRequest a) {
        super((com.squareup.wire.Message)a);
    }
    
    public com.navdy.service.library.events.notification.NotificationListRequest build() {
        return new com.navdy.service.library.events.notification.NotificationListRequest(this, (com.navdy.service.library.events.notification.NotificationListRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
