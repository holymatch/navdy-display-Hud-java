package com.navdy.service.library.events.audio;

final public class SpeechRequest extends com.squareup.wire.Message {
    final public static com.navdy.service.library.events.audio.SpeechRequest$Category DEFAULT_CATEGORY;
    final public static String DEFAULT_ID = "";
    final public static String DEFAULT_LANGUAGE = "";
    final public static Boolean DEFAULT_SENDSTATUS;
    final public static String DEFAULT_WORDS = "";
    final private static long serialVersionUID = 0L;
    final public com.navdy.service.library.events.audio.SpeechRequest$Category category;
    final public String id;
    final public String language;
    final public Boolean sendStatus;
    final public String words;
    
    static {
        DEFAULT_CATEGORY = com.navdy.service.library.events.audio.SpeechRequest$Category.SPEECH_TURN_BY_TURN;
        DEFAULT_SENDSTATUS = Boolean.valueOf(false);
    }
    
    private SpeechRequest(com.navdy.service.library.events.audio.SpeechRequest$Builder a) {
        this(a.words, a.category, a.id, a.sendStatus, a.language);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    SpeechRequest(com.navdy.service.library.events.audio.SpeechRequest$Builder a, com.navdy.service.library.events.audio.SpeechRequest$1 a0) {
        this(a);
    }
    
    public SpeechRequest(String s, com.navdy.service.library.events.audio.SpeechRequest$Category a, String s0, Boolean a0, String s1) {
        this.words = s;
        this.category = a;
        this.id = s0;
        this.sendStatus = a0;
        this.language = s1;
    }
    
    public boolean equals(Object a) {
        boolean b = false;
        label0: if (a != this) {
            if (a instanceof com.navdy.service.library.events.audio.SpeechRequest) {
                com.navdy.service.library.events.audio.SpeechRequest a0 = (com.navdy.service.library.events.audio.SpeechRequest)a;
                boolean b0 = this.equals(this.words, a0.words);
                label1: {
                    if (!b0) {
                        break label1;
                    }
                    if (!this.equals(this.category, a0.category)) {
                        break label1;
                    }
                    if (!this.equals(this.id, a0.id)) {
                        break label1;
                    }
                    if (!this.equals(this.sendStatus, a0.sendStatus)) {
                        break label1;
                    }
                    if (this.equals(this.language, a0.language)) {
                        b = true;
                        break label0;
                    }
                }
                b = false;
            } else {
                b = false;
            }
        } else {
            b = true;
        }
        return b;
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (((((this.words == null) ? 0 : this.words.hashCode()) * 37 + ((this.category == null) ? 0 : this.category.hashCode())) * 37 + ((this.id == null) ? 0 : this.id.hashCode())) * 37 + ((this.sendStatus == null) ? 0 : this.sendStatus.hashCode())) * 37 + ((this.language == null) ? 0 : this.language.hashCode());
            this.hashCode = i;
        }
        return i;
    }
}
