package com.navdy.service.library.events.navigation;

final public class RouteManeuverRequest extends com.squareup.wire.Message {
    final public static String DEFAULT_ROUTEID = "";
    final private static long serialVersionUID = 0L;
    final public String routeId;
    
    private RouteManeuverRequest(com.navdy.service.library.events.navigation.RouteManeuverRequest$Builder a) {
        this(a.routeId);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    RouteManeuverRequest(com.navdy.service.library.events.navigation.RouteManeuverRequest$Builder a, com.navdy.service.library.events.navigation.RouteManeuverRequest$1 a0) {
        this(a);
    }
    
    public RouteManeuverRequest(String s) {
        this.routeId = s;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.navigation.RouteManeuverRequest && this.equals(this.routeId, ((com.navdy.service.library.events.navigation.RouteManeuverRequest)a).routeId);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.routeId == null) ? 0 : this.routeId.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
