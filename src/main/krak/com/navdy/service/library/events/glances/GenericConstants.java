package com.navdy.service.library.events.glances;

final public class GenericConstants extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.glances.GenericConstants[] $VALUES;
    final public static com.navdy.service.library.events.glances.GenericConstants GENERIC_MAIN_ICON;
    final public static com.navdy.service.library.events.glances.GenericConstants GENERIC_MESSAGE;
    final public static com.navdy.service.library.events.glances.GenericConstants GENERIC_SIDE_ICON;
    final public static com.navdy.service.library.events.glances.GenericConstants GENERIC_TITLE;
    final private int value;
    
    static {
        GENERIC_TITLE = new com.navdy.service.library.events.glances.GenericConstants("GENERIC_TITLE", 0, 0);
        GENERIC_MESSAGE = new com.navdy.service.library.events.glances.GenericConstants("GENERIC_MESSAGE", 1, 1);
        GENERIC_MAIN_ICON = new com.navdy.service.library.events.glances.GenericConstants("GENERIC_MAIN_ICON", 2, 2);
        GENERIC_SIDE_ICON = new com.navdy.service.library.events.glances.GenericConstants("GENERIC_SIDE_ICON", 3, 3);
        com.navdy.service.library.events.glances.GenericConstants[] a = new com.navdy.service.library.events.glances.GenericConstants[4];
        a[0] = GENERIC_TITLE;
        a[1] = GENERIC_MESSAGE;
        a[2] = GENERIC_MAIN_ICON;
        a[3] = GENERIC_SIDE_ICON;
        $VALUES = a;
    }
    
    private GenericConstants(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.glances.GenericConstants valueOf(String s) {
        return (com.navdy.service.library.events.glances.GenericConstants)Enum.valueOf(com.navdy.service.library.events.glances.GenericConstants.class, s);
    }
    
    public static com.navdy.service.library.events.glances.GenericConstants[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
