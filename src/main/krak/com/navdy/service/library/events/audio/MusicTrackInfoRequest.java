package com.navdy.service.library.events.audio;

final public class MusicTrackInfoRequest extends com.squareup.wire.Message {
    final private static long serialVersionUID = 0L;
    
    public MusicTrackInfoRequest() {
    }
    
    private MusicTrackInfoRequest(com.navdy.service.library.events.audio.MusicTrackInfoRequest$Builder a) {
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    MusicTrackInfoRequest(com.navdy.service.library.events.audio.MusicTrackInfoRequest$Builder a, com.navdy.service.library.events.audio.MusicTrackInfoRequest$1 a0) {
        this(a);
    }
    
    public boolean equals(Object a) {
        return a instanceof com.navdy.service.library.events.audio.MusicTrackInfoRequest;
    }
    
    public int hashCode() {
        return 0;
    }
}
