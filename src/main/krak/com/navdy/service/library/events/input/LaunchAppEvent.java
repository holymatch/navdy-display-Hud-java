package com.navdy.service.library.events.input;

final public class LaunchAppEvent extends com.squareup.wire.Message {
    final public static String DEFAULT_APPBUNDLEID = "";
    final private static long serialVersionUID = 0L;
    final public String appBundleID;
    
    private LaunchAppEvent(com.navdy.service.library.events.input.LaunchAppEvent$Builder a) {
        this(a.appBundleID);
        this.setBuilder((com.squareup.wire.Message.Builder)a);
    }
    
    LaunchAppEvent(com.navdy.service.library.events.input.LaunchAppEvent$Builder a, com.navdy.service.library.events.input.LaunchAppEvent$1 a0) {
        this(a);
    }
    
    public LaunchAppEvent(String s) {
        this.appBundleID = s;
    }
    
    public boolean equals(Object a) {
        return a == this || a instanceof com.navdy.service.library.events.input.LaunchAppEvent && this.equals(this.appBundleID, ((com.navdy.service.library.events.input.LaunchAppEvent)a).appBundleID);
    }
    
    public int hashCode() {
        int i = this.hashCode;
        if (i == 0) {
            i = (this.appBundleID == null) ? 0 : this.appBundleID.hashCode();
            this.hashCode = i;
        }
        return i;
    }
}
