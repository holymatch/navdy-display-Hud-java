package com.navdy.service.library.events.dial;

final public class DialBondRequest$Builder extends com.squareup.wire.Message.Builder {
    public com.navdy.service.library.events.dial.DialBondRequest$DialAction action;
    
    public DialBondRequest$Builder() {
    }
    
    public DialBondRequest$Builder(com.navdy.service.library.events.dial.DialBondRequest a) {
        super((com.squareup.wire.Message)a);
        if (a != null) {
            this.action = a.action;
        }
    }
    
    public com.navdy.service.library.events.dial.DialBondRequest$Builder action(com.navdy.service.library.events.dial.DialBondRequest$DialAction a) {
        this.action = a;
        return this;
    }
    
    public com.navdy.service.library.events.dial.DialBondRequest build() {
        return new com.navdy.service.library.events.dial.DialBondRequest(this, (com.navdy.service.library.events.dial.DialBondRequest$1)null);
    }
    
    public com.squareup.wire.Message build() {
        return this.build();
    }
}
