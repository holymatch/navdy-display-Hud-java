package com.navdy.service.library.events.preferences;

final public class InputPreferences$DialSensitivity extends Enum implements com.squareup.wire.ProtoEnum {
    final private static com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity[] $VALUES;
    final public static com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity DIAL_SENSITIVITY_LOW;
    final public static com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity DIAL_SENSITIVITY_STANDARD;
    final private int value;
    
    static {
        DIAL_SENSITIVITY_STANDARD = new com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity("DIAL_SENSITIVITY_STANDARD", 0, 0);
        DIAL_SENSITIVITY_LOW = new com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity("DIAL_SENSITIVITY_LOW", 1, 1);
        com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity[] a = new com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity[2];
        a[0] = DIAL_SENSITIVITY_STANDARD;
        a[1] = DIAL_SENSITIVITY_LOW;
        $VALUES = a;
    }
    
    private InputPreferences$DialSensitivity(String s, int i, int i0) {
        super(s, i);
        this.value = i0;
    }
    
    public static com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity valueOf(String s) {
        return (com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity)Enum.valueOf(com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity.class, s);
    }
    
    public static com.navdy.service.library.events.preferences.InputPreferences$DialSensitivity[] values() {
        return $VALUES.clone();
    }
    
    public int getValue() {
        return this.value;
    }
}
