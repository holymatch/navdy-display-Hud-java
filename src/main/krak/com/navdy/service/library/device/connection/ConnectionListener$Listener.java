package com.navdy.service.library.device.connection;

abstract public interface ConnectionListener$Listener extends com.navdy.service.library.util.Listenable$Listener {
    abstract public void onConnected(com.navdy.service.library.device.connection.ConnectionListener arg, com.navdy.service.library.device.connection.Connection arg0);
    
    
    abstract public void onConnectionFailed(com.navdy.service.library.device.connection.ConnectionListener arg);
    
    
    abstract public void onStartFailure(com.navdy.service.library.device.connection.ConnectionListener arg);
    
    
    abstract public void onStarted(com.navdy.service.library.device.connection.ConnectionListener arg);
    
    
    abstract public void onStopped(com.navdy.service.library.device.connection.ConnectionListener arg);
}
