package com.navdy.service.library.device;


public enum RemoteDevice$LinkStatus {
    DISCONNECTED(0),
    LINK_ESTABLISHED(1),
    CONNECTED(2);

    private int value;
    RemoteDevice$LinkStatus(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

//final public class RemoteDevice$LinkStatus extends Enum {
//    final private static com.navdy.service.library.device.RemoteDevice$LinkStatus[] $VALUES;
//    final public static com.navdy.service.library.device.RemoteDevice$LinkStatus CONNECTED;
//    final public static com.navdy.service.library.device.RemoteDevice$LinkStatus DISCONNECTED;
//    final public static com.navdy.service.library.device.RemoteDevice$LinkStatus LINK_ESTABLISHED;
//    
//    static {
//        DISCONNECTED = new com.navdy.service.library.device.RemoteDevice$LinkStatus("DISCONNECTED", 0);
//        LINK_ESTABLISHED = new com.navdy.service.library.device.RemoteDevice$LinkStatus("LINK_ESTABLISHED", 1);
//        CONNECTED = new com.navdy.service.library.device.RemoteDevice$LinkStatus("CONNECTED", 2);
//        com.navdy.service.library.device.RemoteDevice$LinkStatus[] a = new com.navdy.service.library.device.RemoteDevice$LinkStatus[3];
//        a[0] = DISCONNECTED;
//        a[1] = LINK_ESTABLISHED;
//        a[2] = CONNECTED;
//        $VALUES = a;
//    }
//    
//    private RemoteDevice$LinkStatus(String s, int i) {
//        super(s, i);
//    }
//    
//    public static com.navdy.service.library.device.RemoteDevice$LinkStatus valueOf(String s) {
//        return (com.navdy.service.library.device.RemoteDevice$LinkStatus)Enum.valueOf(com.navdy.service.library.device.RemoteDevice$LinkStatus.class, s);
//    }
//    
//    public static com.navdy.service.library.device.RemoteDevice$LinkStatus[] values() {
//        return $VALUES.clone();
//    }
//}
//