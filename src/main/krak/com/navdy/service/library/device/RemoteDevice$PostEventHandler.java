package com.navdy.service.library.device;

abstract public interface RemoteDevice$PostEventHandler {
    abstract public void onComplete(com.navdy.service.library.device.RemoteDevice$PostEventStatus arg);
}
