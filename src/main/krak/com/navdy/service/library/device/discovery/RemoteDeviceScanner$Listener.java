package com.navdy.service.library.device.discovery;

abstract public interface RemoteDeviceScanner$Listener extends com.navdy.service.library.util.Listenable$Listener {
    abstract public void onDiscovered(com.navdy.service.library.device.discovery.RemoteDeviceScanner arg, java.util.List arg0);
    
    
    abstract public void onLost(com.navdy.service.library.device.discovery.RemoteDeviceScanner arg, java.util.List arg0);
    
    
    abstract public void onScanStarted(com.navdy.service.library.device.discovery.RemoteDeviceScanner arg);
    
    
    abstract public void onScanStopped(com.navdy.service.library.device.discovery.RemoteDeviceScanner arg);
}
