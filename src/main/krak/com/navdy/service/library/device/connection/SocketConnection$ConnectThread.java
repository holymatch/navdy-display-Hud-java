package com.navdy.service.library.device.connection;

class SocketConnection$ConnectThread extends Thread {
    private com.navdy.service.library.network.SocketAdapter mmSocket;
    final com.navdy.service.library.device.connection.SocketConnection this$0;
    
    public SocketConnection$ConnectThread(com.navdy.service.library.device.connection.SocketConnection a) {
        super();
        this.this$0 = a;
        this.setName("ConnectThread");
    }
    
    public void cancel() {
        this.this$0.logger.d("[SOCK]Cancelling connect, closing socket");
        com.navdy.service.library.device.connection.SocketConnection.access$100(this.this$0, this.mmSocket);
        this.mmSocket = null;
    }
    
    public void run() {
        this.this$0.logger.i("BEGIN mConnectThread");
        com.navdy.service.library.device.connection.Connection$ConnectionFailureCause a = com.navdy.service.library.device.connection.Connection$ConnectionFailureCause.UNKNOWN;
        label1: {
            Exception a0 = null;
            com.navdy.service.library.device.connection.Connection$Status a1 = null;
            int[] a2 = null;
            label0: {
                try {
                    this.mmSocket = com.navdy.service.library.device.connection.SocketConnection.access$000(this.this$0).build();
                    this.mmSocket.connect();
                } catch(Exception a3) {
                    a0 = a3;
                    break label0;
                }
                synchronized(this.this$0) {
                    this.this$0.mConnectThread = null;
                    /*monexit(a4)*/;
                }
                this.this$0.connected(this.mmSocket);
                break label1;
            }
            this.this$0.logger.e(new StringBuilder().append("Unable to connect - ").append(a0.getMessage()).toString());
            if (a0 instanceof java.net.SocketException) {
                String s = a0.getMessage();
                if (s.contains((CharSequence)"ETIMEDOUT")) {
                    a = com.navdy.service.library.device.connection.Connection$ConnectionFailureCause.CONNECTION_TIMED_OUT;
                } else if (s.contains((CharSequence)"ECONNREFUSED")) {
                    a = com.navdy.service.library.device.connection.Connection$ConnectionFailureCause.CONNECTION_REFUSED;
                }
            }
            synchronized(this.this$0) {
                this.this$0.logger.d("Connect failed, closing socket");
                com.navdy.service.library.device.connection.SocketConnection.access$100(this.this$0, this.mmSocket);
                this.mmSocket = null;
                a1 = this.this$0.mStatus;
                this.this$0.mStatus = com.navdy.service.library.device.connection.Connection$Status.DISCONNECTED;
                /*monexit(a9)*/;
                a2 = com.navdy.service.library.device.connection.SocketConnection$1.$SwitchMap$com$navdy$service$library$device$connection$Connection$Status;
            }
            switch(a2[a1.ordinal()]) {
                case 2: {
                    this.this$0.dispatchDisconnectEvent(com.navdy.service.library.device.connection.Connection$DisconnectCause.NORMAL);
                    break;
                }
                case 1: {
                    com.navdy.service.library.device.connection.SocketConnection.access$200(this.this$0, a);
                    break;
                }
                default: {
                    this.this$0.logger.w(new StringBuilder().append("unexpected state during connection failure: ").append(a1).toString());
                }
            }
        }
    }
}
