package com.navdy.service.library.task;

class TaskManager$1 implements java.util.concurrent.ThreadFactory {
    final com.navdy.service.library.task.TaskManager this$0;
    
    TaskManager$1(com.navdy.service.library.task.TaskManager a) {
        super();
        this.this$0 = a;
    }
    
    public Thread newThread(Runnable a) {
        Thread a0 = new Thread(a);
        a0.setName(new StringBuilder().append("navdy_").append(com.navdy.service.library.task.TaskManager.access$200().incrementAndGet()).toString());
        a0.setDaemon(true);
        return a0;
    }
}
