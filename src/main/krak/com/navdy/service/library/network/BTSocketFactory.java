package com.navdy.service.library.network;

public class BTSocketFactory implements com.navdy.service.library.network.SocketFactory {
    final private String address;
    final private boolean secure;
    final private java.util.UUID serviceUUID;
    
    public BTSocketFactory(com.navdy.service.library.device.connection.ServiceAddress a) {
        this(a.getAddress(), java.util.UUID.fromString(a.getService()));
    }
    
    public BTSocketFactory(String s, java.util.UUID a) {
        this(s, a, true);
    }
    
    public BTSocketFactory(String s, java.util.UUID a, boolean b) {
        this.address = s;
        this.serviceUUID = a;
        this.secure = b;
    }
    
    public com.navdy.service.library.network.SocketAdapter build() {
        android.bluetooth.BluetoothDevice a = android.bluetooth.BluetoothAdapter.getDefaultAdapter().getRemoteDevice(this.address);
        return (com.navdy.service.library.network.SocketAdapter)((this.secure) ? new com.navdy.service.library.network.BTSocketAdapter(a.createRfcommSocketToServiceRecord(this.serviceUUID)) : new com.navdy.service.library.network.BTSocketAdapter(a.createInsecureRfcommSocketToServiceRecord(this.serviceUUID)));
    }
}
