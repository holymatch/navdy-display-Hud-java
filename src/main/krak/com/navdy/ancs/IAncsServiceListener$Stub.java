package com.navdy.ancs;

abstract public class IAncsServiceListener$Stub extends android.os.Binder implements com.navdy.ancs.IAncsServiceListener {
    final private static String DESCRIPTOR = "com.navdy.ancs.IAncsServiceListener";
    final static int TRANSACTION_onConnectionStateChange = 1;
    final static int TRANSACTION_onNotification = 2;
    
    public IAncsServiceListener$Stub() {
        this.attachInterface((android.os.IInterface)this, "com.navdy.ancs.IAncsServiceListener");
    }
    
    public static com.navdy.ancs.IAncsServiceListener asInterface(android.os.IBinder a) {
        Object a0 = null;
        label1: if (a != null) {
            android.os.IInterface a1 = a.queryLocalInterface("com.navdy.ancs.IAncsServiceListener");
            label0: {
                if (a1 == null) {
                    break label0;
                }
                if (!(a1 instanceof com.navdy.ancs.IAncsServiceListener)) {
                    break label0;
                }
                a0 = a1;
                break label1;
            }
            a0 = new com.navdy.ancs.IAncsServiceListener$Stub$Proxy(a);
        } else {
            a0 = null;
        }
        return (com.navdy.ancs.IAncsServiceListener)a0;
    }
    
    public android.os.IBinder asBinder() {
        return (android.os.IBinder)this;
    }
    
    public boolean onTransact(int i, android.os.Parcel a, android.os.Parcel a0, int i0) {
        boolean b = false;
        switch(i) {
            case 1598968902: {
                a0.writeString("com.navdy.ancs.IAncsServiceListener");
                b = true;
                break;
            }
            case 2: {
                a.enforceInterface("com.navdy.ancs.IAncsServiceListener");
                com.navdy.ancs.AppleNotification a1 = (a.readInt() == 0) ? null : (com.navdy.ancs.AppleNotification)com.navdy.ancs.AppleNotification.CREATOR.createFromParcel(a);
                this.onNotification(a1);
                a0.writeNoException();
                b = true;
                break;
            }
            case 1: {
                a.enforceInterface("com.navdy.ancs.IAncsServiceListener");
                this.onConnectionStateChange(a.readInt());
                a0.writeNoException();
                b = true;
                break;
            }
            default: {
                b = super.onTransact(i, a, a0, i0);
            }
        }
        return b;
    }
}
