package com.navdy.ancs;

class IAncsService$Stub$Proxy implements com.navdy.ancs.IAncsService {
    private android.os.IBinder mRemote;
    
    IAncsService$Stub$Proxy(android.os.IBinder a) {
        this.mRemote = a;
    }
    
    public void addListener(com.navdy.ancs.IAncsServiceListener a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.ancs.IAncsService");
            android.os.IBinder a2 = (a == null) ? null : a.asBinder();
            a0.writeStrongBinder(a2);
            this.mRemote.transact(1, a0, a1, 0);
            a1.readException();
        } catch(Throwable a3) {
            a1.recycle();
            a0.recycle();
            throw a3;
        }
        a1.recycle();
        a0.recycle();
    }
    
    public android.os.IBinder asBinder() {
        return this.mRemote;
    }
    
    public void connect(String s) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.ancs.IAncsService");
            a.writeString(s);
            this.mRemote.transact(3, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void connectToDevice(android.os.ParcelUuid a, String s) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.ancs.IAncsService");
            if (a == null) {
                a0.writeInt(0);
            } else {
                a0.writeInt(1);
                a.writeToParcel(a0, 0);
            }
            a0.writeString(s);
            this.mRemote.transact(9, a0, a1, 0);
            a1.readException();
        } catch(Throwable a2) {
            a1.recycle();
            a0.recycle();
            throw a2;
        }
        a1.recycle();
        a0.recycle();
    }
    
    public void connectToService(android.os.ParcelUuid a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.ancs.IAncsService");
            if (a == null) {
                a0.writeInt(0);
            } else {
                a0.writeInt(1);
                a.writeToParcel(a0, 0);
            }
            this.mRemote.transact(4, a0, a1, 0);
            a1.readException();
        } catch(Throwable a2) {
            a1.recycle();
            a0.recycle();
            throw a2;
        }
        a1.recycle();
        a0.recycle();
    }
    
    public void disconnect() {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.ancs.IAncsService");
            this.mRemote.transact(5, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public String getInterfaceDescriptor() {
        return "com.navdy.ancs.IAncsService";
    }
    
    public int getState() {
        int i = 0;
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.ancs.IAncsService");
            this.mRemote.transact(7, a, a0, 0);
            a0.readException();
            i = a0.readInt();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
        return i;
    }
    
    public void performNotificationAction(int i, int i0) {
        android.os.Parcel a = android.os.Parcel.obtain();
        android.os.Parcel a0 = android.os.Parcel.obtain();
        try {
            a.writeInterfaceToken("com.navdy.ancs.IAncsService");
            a.writeInt(i);
            a.writeInt(i0);
            this.mRemote.transact(6, a, a0, 0);
            a0.readException();
        } catch(Throwable a1) {
            a0.recycle();
            a.recycle();
            throw a1;
        }
        a0.recycle();
        a.recycle();
    }
    
    public void removeListener(com.navdy.ancs.IAncsServiceListener a) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.ancs.IAncsService");
            android.os.IBinder a2 = (a == null) ? null : a.asBinder();
            a0.writeStrongBinder(a2);
            this.mRemote.transact(2, a0, a1, 0);
            a1.readException();
        } catch(Throwable a3) {
            a1.recycle();
            a0.recycle();
            throw a3;
        }
        a1.recycle();
        a0.recycle();
    }
    
    public void setNotificationFilter(java.util.List a, long j) {
        android.os.Parcel a0 = android.os.Parcel.obtain();
        android.os.Parcel a1 = android.os.Parcel.obtain();
        try {
            a0.writeInterfaceToken("com.navdy.ancs.IAncsService");
            a0.writeStringList(a);
            a0.writeLong(j);
            this.mRemote.transact(8, a0, a1, 0);
            a1.readException();
        } catch(Throwable a2) {
            a1.recycle();
            a0.recycle();
            throw a2;
        }
        a1.recycle();
        a0.recycle();
    }
}
